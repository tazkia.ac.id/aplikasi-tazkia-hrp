#!/bin/bash

APPNAME=aplikasihris
APPUSER=hris

if [[ $# -eq 0 ]] ; then
    echo 'Cara pakai : deploy.sh <namafile.jar>'
    exit 1
fi

service aplikasi-hris stop
rm /var/lib/aplikasi-hris/$APPNAME.jar
ln -s /var/lib/aplikasi-hris/$1 /var/lib/aplikasi-hris/$APPNAME.jar
chown -R $APPUSER:$APPUSER /var/lib/aplikasi-hris
chmod -R 775 /var/lib/aplikasi-hris
service aplikasi-hris start
