package id.ac.tazkia.aplikasihr;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

//@SpringBootTest
class AplikasiHrApplicationTests {

	@Test
	void contextLoads() {

		String tanggal = "12/31/2022";
		try {
			LocalDate dt = LocalDate.parse(tanggal, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
			System.out.println(dt.format(DateTimeFormatter.ofPattern("EEE, dd-MMM-yyyy")));
		} catch (DateTimeParseException er){

		}
		String waktu = "12:01 PM";
		LocalTime time = LocalTime.parse(waktu, DateTimeFormatter.ofPattern("hh:mm a"));
		System.out.println(time.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
	}

}
