package id.ac.tazkia.aplikasihr.dao.masterdata;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JabatanFungsionalDosen;

public interface JabatanFungsionalDosenDao extends CrudRepository<JabatanFungsionalDosen, String> {
    List<JabatanFungsionalDosen> findByStatus(StatusRecord aktif);
    
}
