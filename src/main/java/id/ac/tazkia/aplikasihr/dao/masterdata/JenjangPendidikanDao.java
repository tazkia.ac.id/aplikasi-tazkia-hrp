package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JenjangPendidikan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JenjangPendidikanDao extends PagingAndSortingRepository<JenjangPendidikan, String>, CrudRepository<JenjangPendidikan, String> {

    List<JenjangPendidikan> findByStatusOrderByNamaPendidikan(StatusRecord statusRecord);

}
