package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ApplicantDao extends PagingAndSortingRepository<Applicant, String>, CrudRepository<Applicant, String> {

    Page<Applicant> findByStatus(StatusRecord status, Pageable page);

    Applicant findByUser(User user);

    Applicant findByStatusAndEmailActive(StatusRecord status, String email);

    @Query(value = "select a.*, b.id as idSchedule, b.status_kirim_email as statusEmail, b.tanggal as tanggal, b.status_wawancara as wawancara from (select b.*, c.id as idRecruitment, d.position_name, e.agama from applicant_recruitment_request as a inner join applicant as b on a.id_applicant=b.id inner join recruitment_request as c on a.id_recruitment_request=c.id inner join job_position as d on c.for_job_position=d.id inner join agama as e on b.religion_id=e.id where a.status='AKTIF' and b.status='AKTIF')a left join (select * from applicant_schedule where status='AKTIF')b on a.id=b.id_applicant", nativeQuery = true,
            countQuery = "select count(*) from (select b.*, c.id as idRecruitment, d.position_name, e.agama from applicant_recruitment_request as a inner join applicant as b on a.id_applicant=b.id inner join recruitment_request as c on a.id_recruitment_request=c.id inner join job_position as d on c.for_job_position=d.id inner join agama as e on b.religion_id=e.id where a.status='AKTIF' and b.status='AKTIF')a left join (select * from applicant_schedule where status='AKTIF')b on a.id=b.id_applicant;")
    Page<Object[]> listApplicant(Pageable page);

}
