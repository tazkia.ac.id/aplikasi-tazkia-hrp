package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.payroll.HitungPajakDto;
import id.ac.tazkia.aplikasihr.entity.masterdata.HitungPajak;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface HitungPajakDao extends PagingAndSortingRepository<HitungPajak, String>, CrudRepository<HitungPajak, String> {

    @Query(value = "select persentase from hitung_pajak where syarat_dari < ?1 and syarat_sampai >= ?1 and status = 'AKTIF'", nativeQuery = true)
    BigDecimal cariHitungPajak(BigDecimal nominal);

    @Query(value = "select persentase_non_npwp from hitung_pajak where syarat_dari < ?1 and syarat_sampai >= ?1 and status = 'AKTIF'", nativeQuery = true)
    BigDecimal cariHitungPajakNonNpwp(BigDecimal nominal);

    @Query(value = "select persentase,syarat_dari as syaratDari, syarat_sampai as syaratSampai from hitung_pajak where syarat_dari < ?1 and status = 'AKTIF' order by syarat_dari", nativeQuery = true)
    List<HitungPajakDto> listHitungPajak(BigDecimal nominal);

}
