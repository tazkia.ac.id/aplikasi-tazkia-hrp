package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengisianKpiDosenDao extends PagingAndSortingRepository<PengisianKpiDosen, String>, CrudRepository<PengisianKpiDosen, String> {


    @Modifying
    @Query(value = "update pengisian_kpi set status = 'HAPUS' where id = ?1"
            ,nativeQuery = true)
    void hapusKpi(String id);

    Page<PengisianKpiDosen>findByStatusAndStatusKpiAndLecturerAndBulanAndTahunOrderByTanggal(StatusRecord statusRecord, StatusRecord statusRecord2, Lecturer lecturer, String bulan, String tahun, Pageable pageable);

    List<PengisianKpiDosen> findByStatusAndStatusKpiAndLecturerAndBulanAndTahunAndLecturerClassKpiOrderByTanggal(
            StatusRecord statusRecord, StatusRecord statusRecord2, Lecturer lecturer, String bulan, String tahun, LecturerClassKpi lecturerClassKpi
    );


    @Query("SELECT p FROM PengisianKpiDosen p " +
            "WHERE p.lecturerClassKpi.id = :idPositionKpi " +
            "AND p.lecturer.id = :idEmployee " +
            "AND p.bulan = :bulan " +
            "AND p.tahun = :tahun " +
            "AND p.status = 'AKTIF'")
    List<PengisianKpiDosen> findByPositionKpiAndEmployeeAndBulanAndTahun(
            String idPositionKpi,
            String idEmployee,
            String bulan,
            String tahun);
}
