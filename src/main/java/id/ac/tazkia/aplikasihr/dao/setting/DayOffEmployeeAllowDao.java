package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.dto.DayOffEmployeeAllowDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffEmployeeAllow;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DayOffEmployeeAllowDao extends PagingAndSortingRepository<DayOffEmployeeAllow, String>, CrudRepository<DayOffEmployeeAllow, String> {

    @Query(value = "select a.id, a.number as idEmployee, a.full_name as  namaEmployee,  coalesce(b.status,'FALSE')as statusAktif from\n" +
            "(select * from employes where status = 'AKTIF') as a left join\n" +
            "(select * from day_off_employee_allow where status = 'AKTIF' and id_day_off = ?1) as b \n" +
            "on a.id = b.id_employee order by a.full_name", nativeQuery = true)
    List<DayOffEmployeeAllowDto> listDayOffEmployee(String idDayOff);

    DayOffEmployeeAllow findByStatusAndDayOffAndEmployes(StatusRecord statusRecord, DayOff dayOff, Employes employes);

    DayOffEmployeeAllow findByDayOffAndEmployes(DayOff dayOff, Employes employes);

}
