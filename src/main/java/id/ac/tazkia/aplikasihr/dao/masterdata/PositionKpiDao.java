package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.ListPositionKpiDto;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PositionKpiDao extends PagingAndSortingRepository<PositionKpi, String>, CrudRepository<PositionKpi, String> {

    @Query(value = "SELECT a.id as id,b.department_name as departemen, a.position_name as position, \n" +
            "       COALESCE(GROUP_CONCAT('-',c.kpi SEPARATOR ',\\n'),'-') AS kpi\n" +
            "FROM job_position AS a\n" +
            "INNER JOIN department AS b ON a.id_department = b.id and a.status = 'AKTIF'\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where b.id_company in (?1) and a.position_name like %?2% \n" +
            "GROUP BY a.id order by b.department_name, a.position_name", countQuery = "SELECT count(a.id) as nomor\n" +
            "FROM job_position AS a\n" +
            "INNER JOIN department AS b ON a.id_department = b.id\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where b.id_company in (?1) and a.position_name like %?2%", nativeQuery = true)
    Page<ListPositionKpiDto> listPositionKpiSearch(List<String> idCompany,String search, Pageable pageable);


    @Query(value = "SELECT a.id as id,b.department_name as departemen, a.position_name as position, \n" +
            "       COALESCE(GROUP_CONCAT('-',c.kpi SEPARATOR ',\\n'),'-') AS kpi\n" +
            "FROM job_position AS a\n" +
            "INNER JOIN department AS b ON a.id_department = b.id and a.status = 'AKTIF'\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where b.id_company in (?1) \n" +
            "GROUP BY a.id order by b.department_name, a.position_name", countQuery = "SELECT count(a.id) as nomor\n" +
            "FROM job_position AS a\n" +
            "INNER JOIN department AS b ON a.id_department = b.id\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where b.id_company in (?1)", nativeQuery = true)
    Page<ListPositionKpiDto> listPositionKpi(List<String> idCompany, Pageable pageable);

    List<PositionKpi> findByStatusAndJobPosition(StatusRecord statusRecord, JobPosition jobPosition);

    List<PositionKpi> findByStatusAndJobPositionIdInOrderByKpi(StatusRecord statusRecord, List<String> idJobPosition);

    List<PositionKpi> findByStatusAndJobPositionIdInAndPeriodeEvaluasiInOrderByKpi(StatusRecord statusRecord, List<String> idJobPosition, List<PeriodeEvaluasiKpi> periodeEvaluasi);

}
