package id.ac.tazkia.aplikasihr.dao.config;

import id.ac.tazkia.aplikasihr.entity.config.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RoleDao extends PagingAndSortingRepository<Role, String>, CrudRepository<Role, String> {


    @Query(value = "select a.id,a.name, group_concat(c.permission_label separator' - ')as permission \n" +
            "from s_role as a\n" +
            "left join s_role_permission as b on b.id_role = a.id\n" +
            "left join s_permission as c on b.id_permission = c.id where a.id <> 'superadmin' \n" +
            "group by a.id\n" +
            "order by a.name", nativeQuery = true)
    List<Object> listRolePermission();

    Role findByName(String dosen);


}
