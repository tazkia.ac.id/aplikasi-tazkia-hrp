package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Vendor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface VendorDao extends PagingAndSortingRepository<Vendor, String>, CrudRepository<Vendor, String> {

    List<Vendor> findByStatusOrderByVendorName(StatusRecord statusRecord);

}
