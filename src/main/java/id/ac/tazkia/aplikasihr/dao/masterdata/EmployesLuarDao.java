package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployesLuar;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployesLuarDao extends PagingAndSortingRepository<EmployesLuar, String>, CrudRepository<EmployesLuar, String> {

    EmployesLuar findByStatusAndUser(StatusRecord statusRecord, User user);

}
