package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeSaudara;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeSaudaraDao extends PagingAndSortingRepository<EmployeeSaudara, String>, CrudRepository<EmployeeSaudara, String> {

    List<EmployeeSaudara> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
