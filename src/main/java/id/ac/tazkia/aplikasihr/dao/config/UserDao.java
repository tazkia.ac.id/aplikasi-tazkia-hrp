package id.ac.tazkia.aplikasihr.dao.config;

import id.ac.tazkia.aplikasihr.entity.config.User;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String>, CrudRepository<User, String> {
    User findByUsername(String username);

    User findByUsernameAndActiveAndIdNot(String username, Boolean aktif, String idU);

    User findByUsernameAndActive(String username, Boolean aktif);

}
