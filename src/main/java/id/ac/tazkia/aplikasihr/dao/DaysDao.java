package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.entity.Days;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DaysDao extends PagingAndSortingRepository<Days, String>, CrudRepository<Days, String> {

    List<Days> findByStatusOrderByNumberDay(StatusRecord statusRecord);

}
