package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengisianKpiEvidanceDao extends PagingAndSortingRepository<PengisianKpiEvidance, String>, CrudRepository<PengisianKpiEvidance, String> {

    List<PengisianKpiEvidance> findByPengisianKpiAndStatus(PengisianKpi kpi, StatusRecord record);
}
