package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.ListPositionKpiDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LecturerClassDao extends PagingAndSortingRepository<LecturerClass, String>, CrudRepository<LecturerClass, String> {

    List<LecturerClass> findByStatusOrderByClasss(StatusRecord statusRecord);

    LecturerClass findByStatusAndId(StatusRecord statusRecord, String id);

    Page<LecturerClass> findByStatusOrderByDescription (StatusRecord statusRecord, Pageable pageable);


    @Query(value = "SELECT a.id as id,a.description as departemen, a.classs as position, \n" +
            "       COALESCE(GROUP_CONCAT('-',c.kpi SEPARATOR ',\\n'),'-') AS kpi\n" +
            "FROM lecturer_class AS a\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where a.classs like %?1% GROUP BY a.id order by a.classs", countQuery = "SELECT count(a.id) as nomor\n" +
            "FROM lecturer_class AS a\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "where a.classs like %?1%", nativeQuery = true)
    Page<ListPositionKpiDto> listLecturerClassKpiSearch(String search, Pageable pageable);


    @Query(value = "SELECT a.id as id,a.description as departemen, a.classs as position, \n" +
            "       COALESCE(GROUP_CONCAT('-',c.kpi SEPARATOR ',\\n'),'-') AS kpi\n" +
            "FROM lecturer_class AS a\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'\n" +
            "GROUP BY a.id order by a.classs", countQuery = "SELECT count(a.id) as nomor\n" +
            "FROM lecturer_class AS a\n" +
            "LEFT JOIN position_kpi AS c ON a.id = c.id_position AND c.status = 'AKTIF'", nativeQuery = true)
    Page<ListPositionKpiDto> listLecturerClassKpi(Pageable pageable);

}
