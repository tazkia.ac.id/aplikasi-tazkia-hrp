package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKartuKeluarga;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeKartuKeluargaDao extends PagingAndSortingRepository<EmployeeKartuKeluarga, String>, CrudRepository<EmployeeKartuKeluarga, String> {

    EmployeeKartuKeluarga findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    List<EmployeeKartuKeluarga> findByStatusAndEmployesOrderById(StatusRecord statusRecord, Employes employes);

}
