package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantCertificate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApplicantCertificateDao extends PagingAndSortingRepository<ApplicantCertificate, String>, CrudRepository<ApplicantCertificate, String> {

    ApplicantCertificate findByApplicantAndStatus(Applicant applicant, StatusRecord status);

    List<ApplicantCertificate> findByStatus(StatusRecord status);

}
