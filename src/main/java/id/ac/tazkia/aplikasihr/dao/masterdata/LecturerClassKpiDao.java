package id.ac.tazkia.aplikasihr.dao.masterdata;


import id.ac.tazkia.aplikasihr.dto.ListPositionKpiDto;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LecturerClassKpiDao extends PagingAndSortingRepository<LecturerClassKpi, String>, CrudRepository<LecturerClassKpi, String> {

    List<LecturerClassKpi> findByStatusAndLecturerClass(StatusRecord status, LecturerClass lecturerClass);

    List<LecturerClassKpi> findByStatusAndLecturerClassAndPeriodeEvaluasiIn(StatusRecord status, LecturerClass lecturerClass, List<PeriodeEvaluasiKpi> periodeEvaluasi);


}
