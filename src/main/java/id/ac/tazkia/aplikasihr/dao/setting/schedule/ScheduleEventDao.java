package id.ac.tazkia.aplikasihr.dao.setting.schedule;

import id.ac.tazkia.aplikasihr.dto.setting.schedule.ScheduleEventDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleDaily;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ScheduleEventDao extends PagingAndSortingRepository<ScheduleEvent, String>, CrudRepository<ScheduleEvent, String> {

    Page<ScheduleEvent> findByStatusOrderByScheduleDateDesc(StatusRecord statusRecord, Pageable pageable);

    @Query(value = "select a.id as id, id_company as idCompany, a.schedule_date as scheduleDate, a.schedule_time_start as scheduleTimeStart, a.schedule_time_end as scheduleTimeEnd, a.schedule_name as scheduleName,\n" +
            "schedule_description as scheduleDescription, a.status_aktif as statusAktif, a.status, concat(a.schedule_date,' ',a.schedule_time_end) as selesai, now() as sekarang,\n" +
            "if(DATE_ADD(concat(a.schedule_date,' ',a.schedule_time_start), INTERVAL 7 HOUR) > now(), 'BELUM MULAI', if(DATE_ADD(concat(a.schedule_date,' ',a.schedule_time_end), INTERVAL 7 HOUR) >= now(), 'MULAI','SELESAI')) as statusMulai,\n" +
            "if(b.id is not null,'ABSEN','BELUM') as absen, b.id as idAbsen from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event and b.id_employee = ?1 where DATE_ADD(concat(a.schedule_date,' ',a.schedule_time_end), INTERVAL 7 HOUR) > now() and a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and id_company = 'eb5c2e2a-b711-4037-8f60-1e1aa0415958' and coalesce(b.id_employee,?1) = ?1 order by concat(a.schedule_date,' ',a.schedule_time_end) asc limit 3", nativeQuery = true)
    List<ScheduleEventDto> listScheduleEvent(String idEmployee);

    @Query(value = "select count(id) as jml from schedule_event where DATE_ADD(concat(schedule_date,' ',schedule_time_end), INTERVAL 7 HOUR) > now() and status = 'AKTIF' and status_aktif = 'AKTIF'\n" +
            "and id_company = 'eb5c2e2a-b711-4037-8f60-1e1aa0415958' order by concat(schedule_date,' ',schedule_time_end) asc limit 3", nativeQuery = true)
    Integer scheduleEvent(String idEmployee);

    @Query(value = "select a.id as id, id_company as idCompany, schedule_date as scheduleDate, schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd, schedule_name as scheduleName,\n" +
            "schedule_description as scheduleDescription, a.status_aktif as statusAktif, a.status, concat(schedule_date,' ',schedule_time_end) as selesai, now() as sekarang,\n" +
            "if(DATE_ADD(concat(schedule_date,' ',schedule_time_start), INTERVAL 7 HOUR) > now(), 'BELUM MULAI', if(DATE_ADD(concat(schedule_date,' ',schedule_time_end), INTERVAL 7 HOUR) >= now(), 'MULAI','SELESAI')) as statusMulai,\n" +
            "if(b.id is not null,'ABSEN','BELUM') as absen, b.id as idAbsen from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event and b.id_employee = ?1 where DATE_ADD(concat(schedule_date,' ',schedule_time_end), INTERVAL 7 HOUR) > now() and a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and id_company = 'eb5c2e2a-b711-4037-8f60-1e1aa0415958' and coalesce(b.id_employee,?1) = ?1 order by concat(schedule_date,' ',schedule_time_end) asc limit 1", nativeQuery = true)
    ScheduleEventDto dataScheduleEvent(String idEmployee);

    @Query(value = "select a.id as id, schedule_date as scheduleDate, schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd,\n" +
            "schedule_name as scheduleName, schedule_description as scheduleDescription, count(b.id_schedule_event) as jmlHadir from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event where a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and coalesce(b.status,'AKTIF') = 'AKTIF' group by a.id order by schedule_date desc, schedule_time_end desc", countQuery = "\n" +
            "select count(a.id) as jumlah from\n" +
            "(select a.id as id, schedule_date as scheduleDate, schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd,\n" +
            "schedule_name as scheduleName, schedule_description as scheduleDescription, count(b.id_schedule_event) as jmlHadir from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event where a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and coalesce(b.status,'AKTIF') = 'AKTIF' group by a.id order by schedule_date desc, schedule_time_end desc) as a", nativeQuery = true)

    Page<ScheduleEventReportDto> listScheduleEventReport(Pageable pageable);


    @Query(value = "select a.id as id, schedule_date as scheduleDate, schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd,\n" +
            "schedule_name as scheduleName, schedule_description as scheduleDescription, count(b.id_schedule_event) as jmlHadir from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event where a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and coalesce(b.status,'AKTIF') = 'AKTIF' and a.schedule_name like ?1 group by a.id order by schedule_date desc, schedule_time_end desc", countQuery = "\n" +
            "select count(a.id) as jumlah from\n" +
            "(select a.id as id, schedule_date as scheduleDate, schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd,\n" +
            "schedule_name as scheduleName, schedule_description as scheduleDescription, count(b.id_schedule_event) as jmlHadir from schedule_event as a\n" +
            "left join attendance_schedule_event as b on a.id = b.id_schedule_event where a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and coalesce(b.status,'AKTIF') = 'AKTIF' and a.schedule_name like ?1 group by a.id order by schedule_date desc, schedule_time_end desc) as a", nativeQuery = true)

    Page<ScheduleEventReportDto> listScheduleEventReportWithSearch(String search, Pageable pageable);

}
