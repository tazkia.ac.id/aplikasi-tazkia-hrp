package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantIdentityFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApplicantIdentityFileDao extends PagingAndSortingRepository<ApplicantIdentityFile, String>, CrudRepository<ApplicantIdentityFile, String> {

    ApplicantIdentityFile findByApplicantAndStatus(Applicant applicant, StatusRecord status);

    List<ApplicantIdentityFile> findByStatus(StatusRecord status);

}
