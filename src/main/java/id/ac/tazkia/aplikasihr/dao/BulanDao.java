package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.entity.Bulan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BulanDao extends PagingAndSortingRepository<Bulan, String>, CrudRepository<Bulan, String> {

    List<Bulan> findByStatusOrderById(StatusRecord statusRecord);

    Bulan findByStatusAndNomor(StatusRecord statusRecord, String nomor);

    Bulan findByStatusAndId(StatusRecord statusRecord, String id);

    List<Bulan> findByStatusOrderByNomor(StatusRecord statusRecord);



}
