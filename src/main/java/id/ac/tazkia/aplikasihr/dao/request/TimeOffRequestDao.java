package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.TimeOffHariIniDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public interface TimeOffRequestDao extends PagingAndSortingRepository<TimeOffRequest, String>, CrudRepository<TimeOffRequest, String> {

    Page<TimeOffRequest> findByStatusAndEmployesOrderByTanggalPengajuan(StatusRecord statusRecord, Employes employes, Pageable pageable);

    Page<TimeOffRequest> findByStatusAndEmployesOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Employes employes, Pageable pageable);

    TimeOffRequest findByStatusAndId(StatusRecord statusRecord, String id);

    @Query(value = "select b.full_name from time_off_request as a inner join employes as b on a.id_employee = b.id where a.id = ?1", nativeQuery = true)
    String cariEmployee(String id);

    @Query(value = "select date(now()) as tanggal, full_name as nama, status_approve as status, time_off_name as timeOff from time_off_request as a \n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join time_off_jenis as c on a.id_time_off_jenis = c.id\n" +
            "where date(now()) >= a.tanggal_time_off and date(now()) <= tanggal_time_off_sampai and a.status = 'AKTIF' and a.status_approve not in ('REJECTED','CANCELED')", nativeQuery = true)
    List<TimeOffHariIniDto> timeOffHariIni();

    @Query(value = "select date(now()) as tanggal, full_name as nama, status_approve as status, time_off_name as timeOff from time_off_request as a \n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join time_off_jenis as c on a.id_time_off_jenis = c.id\n" +
            "where b.id_company in (?1) and date(now()) >= a.tanggal_time_off and date(now()) <= tanggal_time_off_sampai and a.status = 'AKTIF' and a.status_approve not in ('REJECTED','CANCELED')", nativeQuery = true)
    List<TimeOffHariIniDto> timeOffHariIniCompanies(List<String> idCompanies);


    @Query(value = "select ?1 as tanggal, full_name as nama, status_approve as status, time_off_name as timeOff from time_off_request as a \n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join time_off_jenis as c on a.id_time_off_jenis = c.id\n" +
            "where a.tanggal_time_off <= ?1 and tanggal_time_off_sampai >= ?1 and a.status = 'AKTIF' and a.status_approve not in ('REJECTED','CANCELED')", nativeQuery = true)
    List<TimeOffHariIniDto> timeOffMonitoring(String tanggal);


    @Query(value = "select sum(datediff(tanggal_time_off_sampai, tanggal_time_off) + 1) as hari from time_off_request \n" +
            "where id_employee = ?1 and id_time_off_jenis = ?2 \n" +
            "and year(tanggal_time_off) = ?3 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED')", nativeQuery = true)
    Integer sumTimeOffJenisBlocking(String idEmployee, String idTimeOffJenis, String tahun);

    @Query(value = "select sum(datediff(tanggal_time_off_sampai, tanggal_time_off) + 1) as hari from time_off_request \n" +
            "where id_employee = ?1 and id_time_off_jenis = ?2 \n" +
            "and month(tanggal_time_off) = ?3 and year(tanggal_time_off) = ?4 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED')", nativeQuery = true)
    Integer sumTimeOffJenisBlockingMonth(String idEmployee, String idTimeOffJenis, Integer bulan, Integer tahun);

}
