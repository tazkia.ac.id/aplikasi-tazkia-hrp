package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Jenjang;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JenjangDao extends PagingAndSortingRepository<Jenjang, String>, CrudRepository<Jenjang, String> {

    List<Jenjang> findByStatusOrderByNamaJenjangAsc(StatusRecord statusRecord, Pageable pageable);

}
