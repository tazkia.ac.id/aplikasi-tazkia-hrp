package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKeahlian;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeKeahlianDao extends PagingAndSortingRepository<EmployeeKeahlian, String>, CrudRepository<EmployeeKeahlian, String> {

    List<EmployeeKeahlian> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
