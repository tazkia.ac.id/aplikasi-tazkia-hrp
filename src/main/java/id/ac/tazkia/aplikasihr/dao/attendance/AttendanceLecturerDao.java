package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AttendanceLecturerDao extends PagingAndSortingRepository<AttendanceLecturer, String>, CrudRepository<AttendanceLecturer, String> {

    @Query(value = "update attendance_lecturer set status = 'HAPUS' where status = 'AKTIF' and bulan_date = ?1 and tahun = ?2", nativeQuery = true)
    @Modifying
    void hapusAbsenDosenDiBulanYangSama(String bulan, String tahun, String idCompany);


    @Query(value = "update attendance_lecturer set status = 'HAPUS' where status = 'AKTIF' and bulan_date = ?1 and tahun = ?2 and id_company = ?3", nativeQuery = true)
    @Modifying
    void hapusAbsenDosenDiBulanYangSama2(String bulan, String tahun, String idCompany);


}
