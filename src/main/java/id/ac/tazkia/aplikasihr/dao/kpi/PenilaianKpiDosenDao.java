package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PenilaianKpiDosenDao extends PagingAndSortingRepository<PenilaianKpiDosen, String>, CrudRepository<PenilaianKpiDosen, String> {


    PenilaianKpiDosen findByStatusAndEmployesAndBulanAndTahunAndPositionKpi (StatusRecord statusRecord, Lecturer employee, String bulan, String tahun, LecturerClassKpi lecturerClassKpi);
}
