package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.request.DetailApprovalListDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequestApproval;
import id.ac.tazkia.aplikasihr.entity.setting.AttendanceApprovalSetting;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface AttendanceRequestApprovalDao extends PagingAndSortingRepository<AttendanceRequestApproval, String>, CrudRepository<AttendanceRequestApproval, String> {


    @Query(value = "SELECT a.id,b.id AS idAttendanceRequest, a.number AS number,e.full_name AS fullName,a.status_approve AS statusApprove,a.tanggal_approve AS tanggalApprove,a.keterangan AS  keterangan \n" +
            "FROM attendance_request_approval AS a INNER JOIN\n" +
            "attendance_request AS b ON a.id_attendance_request = b.id INNER JOIN\n" +
            "attendance_approval_setting AS c ON a.id_attendance_approval_setting = c.id INNER JOIN\n" +
            "employee_job_position AS d ON c.id_position_approve = d.id_position INNER JOIN\n" +
            "employes AS e ON d.id_employee = e.id\n" +
            "WHERE b.id_employee = ?1 AND b.status = 'AKTIF' AND b.status_approve = ?2\n" +
            "AND a.status='AKTIF' AND c.status = 'AKTIF' AND d.status='AKTIF' AND e.status = 'AKTIF'\n" +
            "ORDER BY b.id, number, full_name", nativeQuery = true)
    List<DetailApprovalListDto> listApproval(String idEmployee, String statusApprove);

    List<AttendanceRequestApproval> findByStatusAndAttendanceRequest(StatusRecord statusRecord, AttendanceRequest attendanceRequest);

    Integer countByStatusAndAttendanceApprovalSettingAndStatusApprove(StatusRecord statusRecord, AttendanceApprovalSetting attendanceApprovalSetting, StatusRecord statusRecord2);

}
