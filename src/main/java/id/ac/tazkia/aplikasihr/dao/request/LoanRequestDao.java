package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.finance.ListLoanRequestApprovalDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LoanRequestDao extends PagingAndSortingRepository<LoanRequest, String>, CrudRepository<LoanRequest, String> {

    Page<LoanRequest> findByStatusOrderByDateUpdate(StatusRecord statusRecord, Pageable pageable);
    Page<LoanRequest> findByStatusOrderByRequestDate (StatusRecord statusRecord, Pageable pageable);
    Page<LoanRequest> findByStatusAndEmployesFullNameContainingIgnoreCaseOrderByRequestDate (StatusRecord statusRecord, String search, Pageable pageable);

    @Query(value = "select aa.id as id, aa.nominal_estimate as nominalEstimasi, aa.installment as installment,aa.loan_for as loanFor, aa.deskripsi as deskripsi,\n" +
            "aa.estimate_persentase as estimatePersentase, aa.status as status, aa.status_approve as statusApprove, aa.request_date as requestDate,\n" +
            "nominal_cicilan as nominalCicilan,'' as ada, cc.number as number, cc.full_name as fullName, dd.name as name \n" +
            "from loan_request as aa\n" +
            "inner join loan_approval_setting as bb on aa.sequence = bb.sequence\n" +
            "inner join employes as cc on aa.id_employee = cc.id\n" +
            "inner join loan as dd on aa.id_loan = dd.id\n" +
            "where bb.id_employee = ?1 and aa.status = 'AKTIF' and aa.status_approve = 'WAITING'", nativeQuery = true)
    List<ListLoanRequestApprovalDto> listLoanRequest(String idEmployee);

}
