package id.ac.tazkia.aplikasihr.dao.setting.schedule;

import java.time.LocalDate;
import java.time.LocalTime;

public interface ScheduleEventReportDto {

    String getId();
    LocalDate getScheduleDate();
    LocalTime getScheduleTimeStart();
    LocalTime getScheduleTimeEnd();
    String getScheduleName();
    String getScheduleDescription();
    Integer getJmlHadir();

}
