package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.OvertimeApprovalSetting;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OvertimeApprovalSettingDao extends PagingAndSortingRepository<OvertimeApprovalSetting, String>, CrudRepository<OvertimeApprovalSetting, String> {

    List<OvertimeApprovalSetting> findByStatusOrderBySequence(StatusRecord statusRecord);

    List<OvertimeApprovalSetting> findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord statusRecord, String search, StatusRecord statusRecord2, String search2);

    @Query(value = "select count(id) as ada from overtime_approval_setting where status = 'AKTIF' and id_position = ?1", nativeQuery = true)
    Integer cariNumberOvertimeApproval(String idPosition);

    OvertimeApprovalSetting findByStatusAndPositionAndPositionApprove(StatusRecord statusRecord, JobPosition position, JobPosition position1);

}
