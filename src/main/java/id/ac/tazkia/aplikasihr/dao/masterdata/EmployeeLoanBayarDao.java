package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.loan.HistoryLoanDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.transaction.EmployeeLoanBayar;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface EmployeeLoanBayarDao extends PagingAndSortingRepository<EmployeeLoanBayar, String>, CrudRepository<EmployeeLoanBayar, String> {

    EmployeeLoanBayar findByStatusAndEmployeeLoanAndBulanAndTahun(StatusRecord statusRecord, EmployeeLoan employeeLoan, String bulan, String tahun);

    @Query(value = "select sum(payment)as jml from employee_loan_bayar where status = 'AKTIF' and id_employee_loan = ?1",nativeQuery = true)
    BigDecimal jumlahPembayaran(String idLoan);

    @Query(value = "select c.id as idEmployeeLoanBayar,b.id as idEmployeeLoan,c.number,c.full_name as employeeName,d.name as loan,b.descriptions,installment,cicilan_ke as cicilanKe,payment\n" +
            "from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan = b.id\n" +
            "inner join employes as c on b.id_employee = c.id \n" +
            "inner join loan as d on b.id_loan = d.id\n" +
            "where a.bulan = ?1 and a.tahun = ?2 and c.id_company = ?3 and a.status = 'AKTIF' order by c.number", nativeQuery = true)
    List<HistoryLoanDto> historyLoanBayar(String bulan, String tahun, String idCompanies);

    @Query(value = "select b.id, cicilan_ke, if(coalesce(remaining,0) > 0,remaining,amount) as amount, tahun, c.nama_english as bulan, a.payment, if(coalesce(remaining,0) > 0,remaining,amount)-(select sum(payment) from employee_loan_bayar where status='AKTIF' and cicilan_ke <= a.cicilan_ke and id_employee_loan = b.id group by id_employee_loan ) as sisa from employee_loan_bayar as a\n" +
            "inner join employee_loan as b on a.id_employee_loan = b.id inner join bulan as c on a.bulan = c.nomor \n" +
            "where id_employee_loan = ?1 and a.status = 'AKTIF' and b.status <> 'HAPUS' \n" +
            "order by tahun, a.bulan", nativeQuery = true)
    List<Object> historyEmployeeLoanBayar(String idEmployeeLoan);

}
