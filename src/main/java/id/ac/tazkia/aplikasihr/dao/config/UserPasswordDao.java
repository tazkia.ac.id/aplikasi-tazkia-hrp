package id.ac.tazkia.aplikasihr.dao.config;

import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserPassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>, CrudRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}
