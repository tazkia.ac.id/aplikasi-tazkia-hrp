package id.ac.tazkia.aplikasihr.dao.setting.time_management;

import id.ac.tazkia.aplikasihr.dto.CekShiftEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.api.EmployeeScheduleDailyDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.EmployeeScheduleDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.ListHeaderShiftDto;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.EmployeeSchedule;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface EmployeeScheduleDao extends PagingAndSortingRepository<EmployeeSchedule, String>, CrudRepository<EmployeeSchedule, String> {

    @Query(value = "select aa.id,date(aa.tanggal) as tanggal, aa.hari as hari, aa.shift_name as shiftName, (aa.shift_in) as shiftIn, (aa.shift_out) as shiftOut from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, shift_name, shift_in, shift_out,efective_date FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date, id_day) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date, id_day) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal", nativeQuery = true)
    List<EmployeeScheduleDto> listEmployeeSchedule(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);

    @Query(value = "select aa.id,aa.tanggal as tanggal, aa.hari as hari, aa.shift_name as shiftName, (aa.shift_in + INTERVAL 7 HOUR) as shiftIn, (aa.shift_out + INTERVAL 7 HOUR) as shiftOut from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, shift_name, shift_in, shift_out,efective_date FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date, id_day) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date, id_day) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal", nativeQuery = true)
    List<Object> listEmployeeSchedule2(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);

    @Query(value = "SELECT 'id_number' as number, 'nama' as name,gen_date as tanggal FROM \n" +
            "(SELECT ADDDATE('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2 order by gen_date", nativeQuery = true)
    List<ListHeaderShiftDto> listHeaderShift(LocalDate tanggalDari, LocalDate tanggalSampai);

    @Query(value = "select * from employee_schedule where id_employee = ?1 and efective_date <= ?2 order by efective_date desc limit 1", nativeQuery = true)
    EmployeeSchedule cariScheduleEmployee(String idEmployee, LocalDate tanggalMulai);

    @Query(value = "select a.*, coalesce(b.keterangan,workDayStatys) as keterangan, coalesce(libur,workDayStatys) as status from\n" +
            "(select a.id, work_day_status as workDayStatys, (shift_in) as shiftIn, (shift_out) as shiftOut, \n" +
            "(overtime_before) as overtimeBefore, (overtime_after) as overtimeAfter, ?1 as tanggals,\n" +
            "name_day as hari, b.overtime_working_day as overtimeWorkingDay, b.overtime_holiday as overtimeHoliday from\n" +
            "(select * from employee_schedule where id_employee = ?2 and efective_date <= ?1 and status = 'AKTIF' order by efective_date desc limit 1)as a\n" +
            "inner join pattern_schedule as b on a.id_pattern_schedule = b.id\n" +
            "inner join pattern as c on b.id_pattern = c.id\n" +
            "inner join shift_pattern as d on c.id = d.id_pattern\n" +
            "inner join shift_detail as e on d.id_shift_detail = e.id\n" +
            "inner join days as f on d.id_day = f.id\n" +
            "where b.status = 'AKTIF' and c.status = 'AKTIF' and d.status='AKTIF' and e.status = 'AKTIF' and f.status = 'AKTIF' and number_day + 1 = dayofweek(?1)) as a\n" +
            "left join\n" +
            "(select *,'OFF' as libur from day_off where date_day_off = ?1 and status = 'AKTIF') as b on a.tanggals = b.date_day_off\n", nativeQuery = true)
    CekShiftEmployeeDto cariShiftEmployee(LocalDate tanggal, String idEmployee);

    @Query(value = "update employee_schedule set status = 'HAPUS' where id_employee = ?1 and efective_date >= ?2", nativeQuery = true)
    @Modifying
    void deleteEmployeeSchedule(String idEmployee, LocalDate efectiveDate);

    @Query(value = "select ?2 as tanggalMasuk, shift_in + INTERVAL 7 HOUR as shiftMasuk, " +
            "if((shift_out + INTERVAL 7 HOUR) < (shift_in + INTERVAL 7 HOUR) ,date(DATE_ADD(?2, INTERVAL 1 DAY)),date(?2)) as tanggalKeluar, " +
            "shift_out + INTERVAL 7 HOUR as shiftKeluar \n" +
            "from employee_schedule as a\n" +
            "inner join pattern_schedule as b on a.id_pattern_schedule = b.id\n" +
            "inner join shift_pattern as c on b.id_pattern = c.id_pattern\n" +
            "inner join shift_detail as d on c.id_shift_detail = d.id\n" +
            "where a.efective_date <= ?2 and a.id_employee = ?1 and a.status = 'AKTIF' \n" +
            "and b.status = 'AKTIF' and c.status = 'AKTIF' and work_day_status = 'ON' order by a.efective_date desc limit 1", nativeQuery = true)
    EmployeeScheduleDailyDto cariShiftHarianPegawai(String idEmployes, LocalDate tanggal);

}
