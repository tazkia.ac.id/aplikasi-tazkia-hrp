package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeRiwayatPekerjaan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeRiwayatPekerjaanDao extends PagingAndSortingRepository<EmployeeRiwayatPekerjaan, String>, CrudRepository<EmployeeRiwayatPekerjaan, String> {

    List<EmployeeRiwayatPekerjaan> findByStatusAndEmployesOrderByTahunMasuk(StatusRecord statusRecord, Employes employes);

}
