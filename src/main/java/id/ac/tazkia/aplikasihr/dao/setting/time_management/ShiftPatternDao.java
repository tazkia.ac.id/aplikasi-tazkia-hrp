package id.ac.tazkia.aplikasihr.dao.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftDetail;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftPattern;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface ShiftPatternDao extends PagingAndSortingRepository<ShiftPattern, String>, CrudRepository<ShiftPattern, String> {

    List<ShiftPattern> findByStatusOrderByDaysNumberDay(StatusRecord statusRecord);

    Page<ShiftPattern> findByStatusOrderByDaysNumberDay(StatusRecord statusRecord, Pageable pageable);

    List<ShiftPattern> findByStatusAndPatternOrderByDaysNumberDay(StatusRecord statusRecord, Pattern pattern);

//    @Query(value = "SELECT (COUNT(id)) AS number FROM shift_pattern WHERE id_pattern = ?1 AND STATUS = 'AKTIF'", nativeQuery = true)
//    Integer cariNomorUrutShift(String idPattern);

    List<ShiftPattern> findByStatusAndDaysNumberDayGreaterThanOrderByDaysNumberDay(StatusRecord statusRecord, Integer nomor);

    Integer countByStatusAndShiftDetail(StatusRecord statusRecord, ShiftDetail shiftDetail);

}
