package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.payroll.LectureSalaryDto;
import id.ac.tazkia.aplikasihr.dto.payroll.LecturerSalaryReportDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerClass;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerSalary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

public interface LecturerSalaryDao extends PagingAndSortingRepository<LecturerSalary, String>, CrudRepository<LecturerSalary, String> {
    Page<LecturerSalary> findByStatus(StatusRecord status, Pageable pageable);

    LecturerSalary findByStatusAndId(StatusRecord statusRecord, String id);

    @Query(value = "SELECT a.id as id, b.name as nama, a.salary_per_sks as salaryPerSks, a.status as status, max(a.tanggal_berlaku) as tanggalBerlaku FROM lecturer_salary as a inner join lecturer as b on a.id_lecturer = b.id where a.status = 'AKTIF' group by b.id order by b.name asc", nativeQuery = true)
    Page<LectureSalaryDto> listData(Pageable pageable);

    Page<LecturerSalary> findByStatusOrderByTanggalBerlakuDesc(StatusRecord statusRecord, Pageable pageable);

    Page<LecturerSalary> findByStatusAndLecturerCompaniesIdInOrderByTanggalBerlakuDesc(StatusRecord statusRecord,List<String> idCompany, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "UPDATE lecturer_salary SET status = 'HAPUS' WHERE id = ?1", nativeQuery = true)
    void deleteSalaryLecturer(String id);


    @Query(value = "select a.id_dosen as id, nama,classs, matkul, kelas, total_sesi as totalSesi, tanggal, total_sks as totalSks,coalesce(salary_per_sks,0) as tarif, total_sks * coalesce(salary_per_sks,0) as total \n" +
            "from attendance_lecturer as a\n" +
            "inner join lecturer as d on a.id_dosen = d.id\n" +
            "inner join lecturer_class as c on d.id_lecturer_class = c.id\n" +
            "left join lecturer_salary as b on a.id_dosen = b.id_lecturer \n" +
            "where tahun = ?1 and bulan_date = ?2 and a.status = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and \n" +
            "coalesce(tanggal_berlaku,?3) <= ?3 and a.id_jenjang = ?4 group by nama, kelas, matkul order by nama", nativeQuery = true)
    List<LecturerSalaryReportDto> lecturerSalaryReport(String tahun, String bulan, LocalDate tanggal, String idJenjang);

    @Query(value = "select a.id_dosen as id, nama,classs, matkul, kelas, total_sesi as totalSesi, tanggal, total_sks as totalSks,coalesce(salary_per_sks,0) as tarif, total_sks * coalesce(salary_per_sks,0) as total \n" +
            "from attendance_lecturer as a\n" +
            "inner join lecturer as d on a.id_dosen = d.id\n" +
            "inner join lecturer_class as c on d.id_lecturer_class = c.id\n" +
            "left join lecturer_salary as b on a.id_dosen = b.id_lecturer \n" +
            "where tahun = ?1 and bulan_date = ?2 and a.status = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and \n" +
            "coalesce(tanggal_berlaku,?3) <= ?3 and a.id_jenjang = ?4 and d.id_company in (?5) group by nama, kelas, matkul order by nama", nativeQuery = true)
    List<LecturerSalaryReportDto> lecturerSalaryReportCompany(String tahun, String bulan, LocalDate tanggal, String idJenjang, List<String> idCompany);


    @Query(value = "select a.id_dosen as id, nama,classs, matkul, kelas, total_sesi as totalSesi, tanggal, total_sks as totalSks,coalesce(salary_per_sks,0) as tarif, total_sks * coalesce(salary_per_sks,0) as total \n" +
            "from attendance_lecturer as a\n" +
            "inner join lecturer as d on a.id_dosen = d.id\n" +
            "inner join lecturer_class as c on d.id_lecturer_class = c.id\n" +
            "left join lecturer_salary as b on a.id_dosen = b.id_lecturer \n" +
            "where tahun = ?1 and bulan_date = ?2 and a.status = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and \n" +
            "coalesce(tanggal_berlaku,?3) <= ?3 and a.id_jenjang = ?4 and d.id = ?5", nativeQuery = true)
    LecturerSalaryReportDto lecturerSalaryReportDosen(String tahun, String bulan, LocalDate tanggal, String idJenjang, String idDosen);


    @Query(value = "select id_jenjang, jenjang from attendance_lecturer where id_jenjang is not null group by id_jenjang", nativeQuery = true)
    List<Object> listJenjang();

    @Query(value = "select id_jenjang, jenjang from attendance_lecturer where id_jenjang = ?1 group by id_jenjang", nativeQuery = true)
    Object listJenjangSelected(String idJenjang);

    @Query(value = "select jenjang from attendance_lecturer where id_jenjang = ?1 group by id_jenjang", nativeQuery = true)
    String listJenjangSelectedS(String idJenjang);
}
