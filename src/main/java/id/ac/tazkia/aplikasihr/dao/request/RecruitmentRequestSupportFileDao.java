package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentFileSupport;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RecruitmentRequestSupportFileDao extends PagingAndSortingRepository<RecruitmentFileSupport, String>, CrudRepository<RecruitmentFileSupport, String> {

    RecruitmentFileSupport findByRequestAndNameAndStatus(RecruitmentRequest request, String name, StatusRecord status);

    List<RecruitmentFileSupport> findByRequestAndStatus(RecruitmentRequest request, StatusRecord status);

}
