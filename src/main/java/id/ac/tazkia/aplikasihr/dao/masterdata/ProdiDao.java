package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Prodi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProdiDao extends PagingAndSortingRepository<Prodi, String>, CrudRepository<Prodi, String> {

    Page<Prodi> findByStatusAndFakultasCompaniesIdInOrderByNamaProdiAsc(StatusRecord status, List<String> idCompany, Pageable pageable);
    List<Prodi> findByStatusAndFakultasCompaniesIdInOrderByNamaProdiAsc(StatusRecord status, List<String> idCompany);
    List<Prodi> findByStatusOrderByNamaProdiAsc(StatusRecord status);

    Page<Prodi> findByStatusAndFakultasCompaniesIdInAndNamaProdiContainingIgnoreCaseOrStatusAndFakultasCompaniesIdInAndNamaProdiEnglishContainingIgnoreCaseOrderByNamaProdiAsc(StatusRecord status,List<String> idCompany, String search, StatusRecord statusRecord2,List<String> idCompany2, String search2, Pageable pageable);


}
