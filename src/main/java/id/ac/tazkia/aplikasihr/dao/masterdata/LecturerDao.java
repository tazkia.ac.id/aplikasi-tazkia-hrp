package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.transaction.Transactional;
import java.util.List;

public interface LecturerDao extends PagingAndSortingRepository<Lecturer, String>, CrudRepository<Lecturer, String> {

    Page<Lecturer> findByStatusOrderByEmployesFullNameAsc(StatusRecord statusRecord, Pageable pageable);
    List<Lecturer> findByStatusOrderByEmployesFullNameAsc(StatusRecord statusRecord);


    @Query(value = "select a.id, a.nidn, a.name, b.description, c.department_name, a.status from lecturer as a \n" +
            "inner join lecturer_class as b on a.id_lecturer_class = b.id \n" +
            "inner join department as c on a.id_departemen = c.id where a.status = 'AKTIF'", nativeQuery = true)
    Page<Object[]> listData(Pageable pageable);

//    Page<Lecturer> findByStatusOrderByNameAsc(StatusRecord statusRecord, Pageable pageable);

    Page<Lecturer> findByStatusAndProdiDepartmentCompaniesIdInOrderByEmployesFullNameAsc(StatusRecord statusRecord, List<String> idCompanies, Pageable pageable);


    Page<Lecturer> findByStatusAndProdiDepartmentCompaniesOrderByEmployesFullNameAsc(StatusRecord statusRecord, Companies companies, Pageable pageable);

    Page<Lecturer> findByStatusAndProdiDepartmentCompaniesAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullNameAsc(StatusRecord statusRecord, Companies companies,String nama, Pageable pageable);

    Page<Lecturer> findByStatusAndEmployesFullNameContainingIgnoreCaseOrderByNameAsc(StatusRecord statusRecord,String nama, Pageable pageable);

    Page<Lecturer> findByStatusAndProdiDepartmentCompaniesIdInAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullNameAsc(StatusRecord statusRecord,List<String> idCompany ,String nama, Pageable pageable);

    Lecturer findByStatusAndId(StatusRecord statusRecord, String id);
    @Transactional
    @Modifying
    @Query(value = "UPDATE lecturer SET status = 'HAPUS' WHERE id = ?1", nativeQuery = true)
    void deleteLecturer(String id);

    @Query(value = "select id, name from lecturer where status = 'AKTIF' order by name asc", nativeQuery = true)
    Object[] selectDosen();

    @Query(value = "select id, name from lecturer where status = 'AKTIF' and id_company in (?1) order by name asc", nativeQuery = true)
    Object[] selectDosenCompany(List<String> idCompany);


    Lecturer findByStatusAndEmployesCompaniesAndEmployesEmailActive(StatusRecord statusRecord, Companies companies, String email);

    Lecturer findByStatusAndCompaniesAndEmail(StatusRecord statusRecord, Companies companies, String email);

    Lecturer findByStatusAndEmail(StatusRecord statusRecord, String email);


    Lecturer findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    @Query(value = "select id from lecturer where id_lecturer_class in ('TETAP','TETAPM') and id_employee = ?1 group by id_employee", nativeQuery = true)
    String ambilIdDosen(String idEmployee);



}
