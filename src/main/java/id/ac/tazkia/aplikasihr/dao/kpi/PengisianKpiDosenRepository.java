package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PengisianKpiDosenRepository extends JpaRepository<PengisianKpiDosen, String> {
    @Query("SELECT p FROM PengisianKpiDosen p " +
            "WHERE p.lecturerClassKpi.id = :idPositionKpi " +
            "AND p.lecturer.id = :idEmployee " +
            "AND p.bulan = :bulan " +
            "AND p.tahun = :tahun " +
            "AND p.status = 'AKTIF'")
    List<PengisianKpiDosen> findByPositionKpiAndEmployeeAndBulanAndTahun(
            String idPositionKpi,
            String idEmployee,
            String bulan,
            String tahun);

}
