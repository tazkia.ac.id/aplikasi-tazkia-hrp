package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.approval.ApprovalListDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalOvertimeRequestListDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface AttendanceRequestDao extends PagingAndSortingRepository<AttendanceRequest, String>, CrudRepository<AttendanceRequest, String> {

    Page<AttendanceRequest> findByStatusAndEmployesOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Employes employes, Pageable pageable);

    AttendanceRequest findByStatusAndEmployesAndTanggalAndKeteranganAndStatusApproveAndJamMasukAndJamKeluarAndNomor(StatusRecord statusRecord, Employes employes, LocalDate tanggal, String keterangan, StatusRecord statusRecord1, LocalTime jamMasuk, LocalTime jamKeluar, Integer nomor);

    AttendanceRequest findByStatusAndId(StatusRecord statusRecord, String id);

    @Query(value = "select b.full_name from attendance_request as a inner join employes as b on a.id_employee = b.id where a.id = ?1", nativeQuery = true)
    String cariEmployee(String id);
    @Query(value = "select a.*, coalesce(b.approve,'-') as approve from\n" +
            "(select a.* from\n" +
            "(select a.id, e.full_name as fullName, a.tanggal_pengajuan as tanggalPengajuan, tanggal,sampai as sampai, request, a.keterangan as keterangan, a.file as file, jenis, nomor from\n" +
            "(select id,nomor, id_employee, tanggal_pengajuan, tanggal, tanggal as sampai, concat('Attendance In : ',time(CONVERT_TZ(`jam_masuk`,'+00:00','+07:00')), ', Out : ', time(CONVERT_TZ(`jam_keluar`,'+00:00','+07:00'))) as request, keterangan, file, 'attendance' as jenis from attendance_request where status = 'AKTIF' and status_approve = 'WAITING'\n" +
            "union\n" +
            "select id,nomor, id_employee, tanggal as tanggal_pengajuan, tanggal_overtime as tanggal, tanggal_overtime as sampai, concat('Overtime from : ',time(CONVERT_TZ(`overtime_from`,'+00:00','+07:00')) ,' to : ', time(CONVERT_TZ(`overtime_to`,'+00:00','+07:00'))) as request, keterangan, file, 'overtime' as jenis from overtime_request where status = 'AKTIF' and status_approve = 'WAITING'\n" +
            "union\n" +
            "select a.id,a.nomor, a.id_employee, a.tanggal_pengajuan, a.tanggal_time_off as tanggal, a.tanggal_time_off_sampai as sampai, b.time_off_name as request, keterangan, file, 'time_off' as jenis from time_off_request as a inner join time_off_jenis as b on a.id_time_off_jenis = b.id where a.status = 'AKTIF' and a.status_approve = 'WAITING')a\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now())) as b on a.id_employee = b.id_employee\n" +
            "inner join attendance_approval_setting as c on b.id_position = c.id_position and a.nomor = c.sequence\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now())) as d on c.id_position_approve = d.id_position \n" +
            "inner join employes as e on a.id_employee = e.id\n" +
            "where d.id_employee = ?1 and b.status='AKTIF' and b.status_aktif = 'AKTIF' and c.status = 'AKTIF' and d.status='AKTIF' and d.status_aktif = 'AKTIF'\n" +
            "group by a.id)a\n" +
            "left join \n" +
            "(select id, id_attendance_request as id_request, status_approve, number from attendance_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1 \n" +
            "union\n" +
            "select id, id_overtime_request as id_request, status_approve, number from overtime_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1\n" +
            "union\n" +
            "select id, id_time_off_request as id_request, status_approve, number from time_off_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1\n" +
            ")b on a.id = b.id_request and a.nomor = b.number\n" +
            "where b.status_approve is null group by id \n" +
            "order by tanggalPengajuan)as a\n" +
            "left join\n" +
            "(select a.id, id_attendance_request as id_request, status_approve, group_concat(a.number,', ',b.full_name,'-(', DATE_FORMAT(a.tanggal_approve, '%d-%M-%Y %H:%i:%s'),'). ' order by id_attendance_request, a.number  SEPARATOR '  ')as approve from attendance_request_approval as a inner join employes as b on a.id_employee = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' group by id_attendance_request\n" +
            "union\n" +
            "select a.id, a.id_overtime_request as id_request, status_approve, group_concat(a.number,', ',b.full_name,'-(', DATE_FORMAT(a.tanggal_approve, '%d-%M-%Y %H:%i:%s'),'). ' order by id_overtime_request, a.number  SEPARATOR '  ')as approve from overtime_request_approval as a inner join employes as b on a.id_employee = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' group by a.id_overtime_request\n" +
            "union\n" +
            "select a.id, a.id_time_off_request as id_request, status_approve, group_concat(a.number,', ',b.full_name,'-(', DATE_FORMAT(a.tanggal_approve, '%d-%M-%Y %H:%i:%s'),'). ' order by id_time_off_request, a.number  SEPARATOR '  ')as approve from time_off_request_approval as a inner join employes as b on a.id_employee = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' group by a.id_time_off_request\n" +
            ")b on a.id = b.id_request order by tanggalPengajuan", countQuery = "select count(a.id) as page from\n" +
            "(select a.id, e.full_name as fullName, a.tanggal_pengajuan as tanggalPengajuan, tanggal, request, a.keterangan as keterangan, a.file as file, jenis, nomor from\n" +
            "(select id,nomor, id_employee, tanggal_pengajuan, tanggal, concat('Attendance In : ',time(CONVERT_TZ(`jam_masuk`,'+00:00','+07:00')), ', Out : ', time(CONVERT_TZ(`jam_keluar`,'+00:00','+07:00'))) as request, keterangan, file, 'attendance' as jenis from attendance_request where status = 'AKTIF' and status_approve = 'WAITING'\n" +
            "union\n" +
            "select id,nomor, id_employee, tanggal as tanggal_pengajuan, tanggal_overtime as tanggal, concat('Overtime from : ', time(CONVERT_TZ(`overtime_from`,'+00:00','+07:00')) ,' to : ', time(CONVERT_TZ(`overtime_to`,'+00:00','+07:00'))) as request, keterangan, file, 'overtime' as jenis from overtime_request where status = 'AKTIF' and status_approve = 'WAITING'\n" +
            "union\n" +
            "select a.id,a.nomor, a.id_employee, a.tanggal_pengajuan, a.tanggal_time_off as tanggal,b.time_off_name as request, a.keterangan, a.file, 'time_off' as jenis from time_off_request as a inner join time_off_jenis as b on a.id_time_off_jenis = b.id where a.status = 'AKTIF' and a.status_approve = 'WAITING')a\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now())) as b on a.id_employee = b.id_employee\n" +
            "inner join attendance_approval_setting as c on b.id_position = c.id_position and a.nomor = c.sequence\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now())) as d on c.id_position_approve = d.id_position \n" +
            "inner join employes as e on a.id_employee = e.id\n" +
            "where d.id_employee = ?1 and b.status='AKTIF' and b.status_aktif = 'AKTIF' and c.status = 'AKTIF' and d.status='AKTIF' and d.status_aktif = 'AKTIF'\n" +
            "group by a.id)a\n" +
            "left join \n" +
            "(select id, id_attendance_request as id_request, status_approve, number from attendance_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1 \n" +
            "union\n" +
            "select id, id_overtime_request as id_request, status_approve, number from overtime_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1\n" +
            "union\n" +
            "select id, id_time_off_request as id_request, status_approve, number from time_off_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1\n" +
            ")b on a.id = b.id_request and a.nomor = b.number\n" +
            "where b.status_approve is null",
            nativeQuery = true)
    Page<ApprovalListDto> listApprovalProcess(String idEmployee, Pageable pageable);

}
