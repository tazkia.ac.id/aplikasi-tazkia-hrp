package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Agama;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AgamaDao extends PagingAndSortingRepository<Agama, String>, CrudRepository<Agama, String> {

    List<Agama> findByStatusOrderByAgama(StatusRecord statusRecord);

}
