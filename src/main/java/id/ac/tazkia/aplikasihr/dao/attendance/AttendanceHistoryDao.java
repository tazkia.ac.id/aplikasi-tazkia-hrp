package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AttendanceHistoryDao extends PagingAndSortingRepository<AttendanceHistory, String>, CrudRepository<AttendanceHistory, String> {

    AttendanceHistory findByStatusAndId(StatusRecord statusRecord, String id);

}
