package id.ac.tazkia.aplikasihr.dao.document;

import id.ac.tazkia.aplikasihr.entity.document.Document;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DocumentDao extends PagingAndSortingRepository<Document, String>, CrudRepository<Document, String> {

    Document findTopByIdTahunOrderByNomorDesc(String idTahun);

    Document findByIdTahunAndIdDosen(String idTahun, String idDosen);

}
