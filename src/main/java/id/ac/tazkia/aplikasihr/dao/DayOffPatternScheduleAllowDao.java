package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.dto.DayOffPatternScheduleAllowDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffPatternScheduleAllow;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DayOffPatternScheduleAllowDao extends PagingAndSortingRepository<DayOffPatternScheduleAllow, String>, CrudRepository<DayOffPatternScheduleAllow, String> {

    @Query(value = "select a.id, a.schedule_name as  patternSchedule, coalesce(b.status,'FALSE')as statusAktif from\n" +
            "(select * from pattern_schedule where status = 'AKTIF') as a left join\n" +
            "(select * from day_off_pattern_schedule_allow where status = 'AKTIF' and id_day_off = ?1) as b \n" +
            "on a.id = b.id_pattern_schedule order by a.schedule_name ", nativeQuery = true)
    List<DayOffPatternScheduleAllowDto> listDarOffPatternScheduleAllow(String idDayOff);


    DayOffPatternScheduleAllow findByStatusAndDayOffAndPatternSchedule(StatusRecord statusRecord, DayOff dayOff, PatternSchedule patternSchedule);

    DayOffPatternScheduleAllow findByDayOffAndPatternSchedule(DayOff dayOff, PatternSchedule patternSchedule);

}
