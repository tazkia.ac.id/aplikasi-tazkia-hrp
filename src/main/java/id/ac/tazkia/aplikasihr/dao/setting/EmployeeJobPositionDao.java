package id.ac.tazkia.aplikasihr.dao.setting;

import java.time.LocalDate;
import java.util.List;

import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;

public interface EmployeeJobPositionDao extends PagingAndSortingRepository<EmployeeJobPosition, String>, CrudRepository<EmployeeJobPosition, String> {

    Page<EmployeeJobPosition> findByStatusAndEmployesOrderByStatusAktif(StatusRecord statusRecord, Employes employes, Pageable pageable);

    Page<EmployeeJobPosition> findByStatusAndEmployesAndPositionPositionNameContainingIgnoreCaseOrderByStatusAktif(StatusRecord statusRecord, Employes employes, String search, Pageable pageable);

    List<EmployeeJobPosition> findByStatusAndStatusAktifAndEmployes(StatusRecord statusRecord, StatusRecord statusRecord1, Employes employes);

    @Query(value = "select d.id as idCompanies from employee_job_position as a\n" +
            "inner join job_position as b on a.id_position = b.id\n" +
            "inner join department as c on b.id_department = c.id\n" +
            "inner join companies as d on c.id_company = d.id inner join companies as e on d.id_main_company = e.id\n" +
            "where a.id_employee = ?1 and a.status = 'AKTIF' and a.start_date <= now() and a.end_date >= now() group by d.id;", nativeQuery = true)
    List<String> cariIdCompaniesKaryawan2(String idEmployee);

    @Query(value = "select d.id as idCompanies from employee_job_position as a\n" +
            "inner join job_position as b on a.id_position = b.id\n" +
            "inner join department as c on b.id_department = c.id\n" +
            "inner join companies as d on c.id_company = d.id_main_company\n" +
            "where a.id_employee = ?1 and a.status = 'AKTIF' and d.status = 'AKTIF' and a.start_date <= now() and a.end_date >= now() group by d.id", nativeQuery = true)
    List<String> cariIdCompaniesKaryawan(String idEmployee);

    EmployeeJobPosition findByStatusAndId(StatusRecord statusRecord, String idEmployeJobPosition);

    @Query(value = "SELECT id_position FROM employee_job_position WHERE id_employee = ?1 AND start_date <= DATE(NOW()) AND end_date >= DATE(NOW()) AND STATUS='AKTIF'", nativeQuery = true)
    List<String> listEmployeeJobPosition(String idEmployee);

    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF'\n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "group by a.id", countQuery = "SELECT COUNT(id)AS number FROM\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, GROUP_CONCAT(COALESCE(c.position_name,'None')) AS job_position FROM employes AS a\n" +
            "LEFT JOIN employee_job_position AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' AND COALESCE(b.status, 'AKTIF') = 'AKTIF' AND COALESCE(c.status, 'AKTIF') = 'AKTIF'\n" +
            "GROUP BY a.id,c.position_name)a ", nativeQuery = true)
    Page<Object> listEmployeeJobPositionTanpaSearch(String status, Pageable pageable);


    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id where last_schedule = ?2 \n" +
            "group by a.id", countQuery = "SELECT COUNT(id)AS number FROM\n" +
            "(select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from \n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a \n" +
            "left join  \n" +
            "(select b.*,c.schedule_name from \n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a \n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update \n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d \n" +
            "on a.id = d.id_employee \n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF'  \n" +
            "GROUP BY a.id) as a \n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee \n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id where last_schedule = ?2 \n" +
            "group by a.id)a ", nativeQuery = true)
    Page<Object> listEmployeeJobPositionTanpaSearchSchedule(String status,String patternSchedule, Pageable pageable);

    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.id_company in (?2) and a.status_aktif = 'AKTIF'\n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "group by a.id", countQuery = "SELECT COUNT(id)AS number FROM\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, GROUP_CONCAT(COALESCE(c.position_name,'None')) AS job_position, id_company FROM employes AS a\n" +
            "LEFT JOIN employee_job_position AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "WHERE a.status = ?1 and a.id_company in (?2) and a.status_aktif = 'AKTIF' AND COALESCE(b.status, 'AKTIF') = 'AKTIF' AND COALESCE(c.status, 'AKTIF') = 'AKTIF'\n" +
            "GROUP BY a.id,c.position_name)a ", nativeQuery = true)
    Page<Object> listEmployeeJobPositionCompaniesTanpaSearch(String status,List<String> idCompanies, Pageable pageable);


    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.id_company in (?2) and a.status_aktif = 'AKTIF'\n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id where last_schedule = ?3 \n" +
            "group by a.id", countQuery = "SELECT COUNT(id)AS number FROM\n" +
            "(select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from \n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a \n" +
            "left join  \n" +
            "(select b.*,c.schedule_name from \n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a \n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update \n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d \n" +
            "on a.id = d.id_employee \n" +
            "WHERE a.status = ?1 and a.id_company in (?2) and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a \n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee \n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id where last_schedule = ?3 \n" +
            "group by a.id)a ", nativeQuery = true)
    Page<Object> listEmployeeJobPositionCompaniesTanpaSearchSchedule(String status,List<String> idCompanies, String patternSchedule, Pageable pageable);

    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "where (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%)\n" +
            "group by a.id",
    countQuery = "SELECT COUNT(id)AS number FROM\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, GROUP_CONCAT(COALESCE(c.position_name,'None')) AS job_position FROM employes AS a\n" +
            "LEFT JOIN employee_job_position AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' AND COALESCE(b.status, 'AKTIF') = 'AKTIF' AND COALESCE(c.status, 'AKTIF') = 'AKTIF'\n" +
            "AND (a.full_name LIKE %?2% OR a.number LIKE %?2% OR a.full_name LIKE %?2% OR COALESCE(c.position_name,'') LIKE %?2%)\n" +
            "GROUP BY a.id,c.position_name)a",
    nativeQuery = true)
    Page<Object> listEmployeeJobPositionDenganSearch(String status, String search, Pageable pageable);


    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "where last_schedule = ?3 and (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%)\n" +
            "group by a.id",
            countQuery = "SELECT COUNT(id)AS number FROM\n" +
                    "(select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from \n" +
                    "SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule FROM employes AS a \n" +
                    "left join \n" +
                    "(select b.*,c.schedule_name from \n" +
                    "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a \n" +
                    "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update \n" +
                    "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d \n" +
                    "on a.id = d.id_employee \n" +
                    "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
                    "GROUP BY a.id) as a \n" +
                    "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee \n" +
                    "LEFT JOIN job_position AS c ON b.id_position = c.id \n" +
                    "where last_schedule = ?3 and (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%) \n" +
                    "group by a.id)a",
            nativeQuery = true)
    Page<Object> listEmployeeJobPositionDenganSearchSchedule(String status, String search,String schedule, Pageable pageable);

    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "where a.id_company in (?3) and (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%)\n" +
            "group by a.id",
            countQuery = "SELECT COUNT(id)AS number FROM\n" +
                    "(SELECT a.id, a.number, a.nick_name, a.full_name, GROUP_CONCAT(COALESCE(c.position_name,'None')) AS job_position, id_company FROM employes AS a\n" +
                    "LEFT JOIN employee_job_position AS b ON a.id = b.id_employee\n" +
                    "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
                    "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' AND COALESCE(b.status, 'AKTIF') = 'AKTIF' AND COALESCE(c.status, 'AKTIF') = 'AKTIF'\n" +
                    "AND a.id_company in (?3) and (a.full_name LIKE %?2% OR a.number LIKE %?2% OR a.full_name LIKE %?2% OR COALESCE(c.position_name,'') LIKE %?2%)\n" +
                    "GROUP BY a.id,c.position_name)a",
            nativeQuery = true)
    Page<Object> listEmployeeJobPositionCompaniesDenganSearch(String status, String search, List<String> idCompany, Pageable pageable);

    @Query(value = "select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from\n" +
            "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a\n" +
            "left join \n" +
            "(select b.*,c.schedule_name from\n" +
            "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a\n" +
            "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update\n" +
            "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d\n" +
            "on a.id = d.id_employee\n" +
            "WHERE a.status = ?1 and a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id) as a\n" +
            "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee\n" +
            "LEFT JOIN job_position AS c ON b.id_position = c.id\n" +
            "where a.id_company in (?3) and last_schedule = ?4 and (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%)\n" +
            "group by a.id",
            countQuery = "SELECT COUNT(id)AS number FROM\n" +
                    "(select a.id, number, nick_name, full_name, GROUP_CONCAT(COALESCE(position_name,'None')) AS job_positio, last_schedule from \n" +
                    "(SELECT a.id, a.number, a.nick_name, a.full_name, coalesce(d.schedule_name,'None') as last_schedule, id_company FROM employes AS a \n" +
                    "left join  \n" +
                    "(select b.*,c.schedule_name from \n" +
                    "(select id_employee, max(efective_date) as efective_date, max(date_update) as date_update from employee_schedule where status = 'AKTIF' group by id_employee)a \n" +
                    "inner join employee_schedule as b on a.id_employee = b.id_employee and b.efective_date = b.efective_date and a.date_update = b.date_update \n" +
                    "inner join pattern_schedule as c on b.id_pattern_schedule = c.id)d \n" +
                    "on a.id = d.id_employee \n" +
                    "WHERE a.status = ?1 and a.status_aktif = 'AKTIF'  \n" +
                    "GROUP BY a.id) as a \n" +
                    "LEFT JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id = b.id_employee \n" +
                    "LEFT JOIN job_position AS c ON b.id_position = c.id \n" +
                    "where a.id_company in (?3) and last_schedule = ?4 and (full_name LIKE %?2% OR number LIKE %?2% OR full_name LIKE %?2% OR COALESCE(position_name,'') LIKE %?2%) \n" +
                    "group by a.id)a",
            nativeQuery = true)
    Page<Object> listEmployeeJobPositionCompaniesDenganSearchSchedule(String status, String search, List<String> idCompany, String patternSchedule, Pageable pageable);

    @Query(value = "SELECT 'AKTIF' AS STATUS, 'ACTIVE' AS nama FROM employes \n" +
            "UNION\n" +
            "SELECT 'NONAKTIF' AS STATUS,'NONACTIVE' AS nama FROM employes ", nativeQuery = true)
    List<Object> listStatus();

    @Query(value = "SELECT * FROM\n" +
            "(SELECT 'AKTIF' AS STATUS, 'ACTIVE' AS nama FROM employes \n" +
            "UNION\n" +
            "SELECT 'NONAKTIF' AS STATUS,'NONACTIVE' AS nama FROM employes)a \n" +
            "WHERE STATUS = ?1 ", nativeQuery = true)
    Object lisStatusSelected(String status);

    @Query(value = "select id_position from employee_job_position where status = 'AKTIF' and status_aktif = 'AKTIF' and id_employee = ?1 and start_date <= date(now()) and end_date >= date(now())", nativeQuery = true)
    List<String> listJobPosition(String idEmployee);


    @Query(value = "select group_concat(b.position_name separator ', ') as position from employee_job_position as a \n" +
            "inner join job_position as b on a.id_position = b.id\n" +
            "where a.id_employee = ?1 and a.status = 'AKTIF' and b.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and a.end_date >= ?2 and start_date <= ?3", nativeQuery = true)
    String employeeJobPositonAda(String idEmployee, LocalDate tanggalMulai, LocalDate tanggalSelesai);


    @Query(value = "select * from\n" +
            "(select id_employee, full_name, position_name, department_name , \n" +
            "max(end_date) as masa_jabatan, if(date_sub(max(end_date),interval 30 day) < date(now()), 'EXPIRED', 'ACTIVE') as status from employee_job_position as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join job_position as c on a.id_position = c.id \n" +
            "inner join department as d on c.id_department = d.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and b.status_aktif = 'AKTIF'\n" +
            "group by a.id_employee)as a where status = 'EXPIRED'", nativeQuery = true)
    List<Object> employeeJobPositionExpired();


    @Query(value = "select id_position from employee_job_position \n" +
            "where id_employee = ?1 and status = 'AKTIF' and status_aktif = 'AKTIF'\n" +
            "and start_date <= date(now()) and end_date >= date(now())", nativeQuery = true)
    List<String> cariIdEmployeeJobPosition(String idEmployee);

    // Untuk mengambil warek 1
    EmployeeJobPosition findByStatusAndPosition(StatusRecord status, JobPosition positionName);

}
