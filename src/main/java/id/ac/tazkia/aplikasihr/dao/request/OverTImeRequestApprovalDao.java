package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.request.OvertimeRequest;
import id.ac.tazkia.aplikasihr.entity.request.OvertimeRequestApproval;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequest;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequestApproval;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OverTImeRequestApprovalDao extends PagingAndSortingRepository<OvertimeRequestApproval, String>, CrudRepository<OvertimeRequestApproval, String> {

    List<OvertimeRequestApproval> findByStatusAndOvertimeRequest(StatusRecord statusRecord, OvertimeRequest overtimeRequest);


}
