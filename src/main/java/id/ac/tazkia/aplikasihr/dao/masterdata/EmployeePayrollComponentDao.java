package id.ac.tazkia.aplikasihr.dao.masterdata;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentEndDateDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentHistoryDetailDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;

public interface EmployeePayrollComponentDao extends PagingAndSortingRepository<EmployeePayrollComponent, String>, CrudRepository<EmployeePayrollComponent, String> {

    List<EmployeePayrollComponent> findByStatusAndEmployesOrderByJenisComponent(StatusRecord statusRecord, Employes employes);

    EmployeePayrollComponent findByStatusAndEmployesAndPayrollComponent(StatusRecord statusRecord, Employes employes, PayrollComponent payrollComponent);

    EmployeePayrollComponent findByStatusNotAndEmployesAndPayrollComponent(StatusRecord statusRecord, Employes employes, PayrollComponent payrollComponent);

    EmployeePayrollComponent findByStatusAndEmployesAndPayrollComponentStatus(StatusRecord statusRecord, Employes employes, StatusRecord statusRecord2);

    List<EmployeePayrollComponent> findByStatusNotAndEmployesOrderByPayrollComponent(StatusRecord statusRecord, Employes employes);

    List<EmployeePayrollComponent> findByStatusAndEmployesAndPayrollComponentJenisComponent(StatusRecord statusRecord, Employes employes, String jenisComponent);

    List<EmployeePayrollComponent> findByStatusNotAndEmployesAndPayrollComponentPotonganAbsen(StatusRecord statusRecord, Employes employes, StatusRecord potonganAbsen);

    List<EmployeePayrollComponent> findByStatusNotAndEmployesAndPayrollComponentPotonganTerlambat(StatusRecord statusRecord, Employes employes, StatusRecord potonganAbsen);

    @Query(value = "select a.id as id, a.number as number, a.full_name as fullName,coalesce(b.nominal,0) as currrentAmmount,\n" +
            "coalesce(b.nominal,0) as newAmmount from employes as a left join\n" +
            "(select * from employee_payroll_component where id_component = ?1 and status in ('AKTIF','BASIC'))b \n" +
            "on a.id = b.id_employee where a.status_aktif = 'AKTIF' and a.status = 'AKTIF'", nativeQuery = true)
    List<ExportPayrollComponentToExcelDto> listPayrollComponentExportExcel(String idPayrollComponent);


    @Query(value = "select a.id as id, a.number as number, a.full_name as fullName,coalesce(b.nominal,0) as currrentAmmount,\n" +
            "coalesce(b.nominal,0) as newAmmount from (select * from employes where id_company in (?2) and status = 'AKTIF' and status_aktif = 'AKTIF') as a left join\n" +
            "(select * from employee_payroll_component where id_component = ?1 and status in ('AKTIF','BASIC'))b \n" +
            "on a.id = b.id_employee where a.status_aktif = 'AKTIF' and a.status = 'AKTIF'", nativeQuery = true)
    List<ExportPayrollComponentToExcelDto> listPayrollComponentExportExcelCompanies(String idPayrollComponent, List<String> idCompany);

    @Query(value = "select a.id, a.number as employeeId, a.full_name as fullName, coalesce (b.current_amount,a.nominal,0) as currentAmmount, coalesce (b.new_amount,a.nominal,0) as newAmmount, b.id as idHistory, b.effective_date as effectiveDate, b.end_date as endDate from\n" +
            "(select a.*, b.number, b.full_name from employee_payroll_component as a \n" +
            "inner join employes as b on a.id_employee = b.id where a.status in ('AKTIF','BASIC') and b.status = 'AKTIF' and id_component= ?1) as a\n" +
            "inner join\n" +
            "(select a.*,b.effective_date,b.end_date from employee_payroll_component_history_detail as a\n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "where a.status = 'AKTIF') as b on a.id = b.id_employee_payroll_component order by b.effective_date desc,b.end_date desc",nativeQuery = true)
    List<EmployeePayrollComponentHistoryDetailDto> listEmployeePayrollComponenHistoryDetail(String idComponent);

    @Query(value = "select a.id, a.number as employeeId, a.full_name as fullName, coalesce (b.current_amount,a.nominal,0) as currentAmmount, coalesce (b.new_amount,a.nominal,0) as newAmmount, b.id as idHistory, b.effective_date as effectiveDate, b.end_date as endDate from\n" +
            "(select a.*, b.number, b.full_name from employee_payroll_component as a \n" +
            "inner join (select * from employes where id_company in (?2)) as b on a.id_employee = b.id where a.status in ('AKTIF','BASIC') and b.status = 'AKTIF' and id_component= ?1) as a\n" +
            "inner join\n" +
            "(select a.*,b.effective_date,b.end_date from employee_payroll_component_history_detail as a\n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "where a.status = 'AKTIF') as b on a.id = b.id_employee_payroll_component order by b.effective_date desc,b.end_date desc",nativeQuery = true)
    List<EmployeePayrollComponentHistoryDetailDto> listEmployeePayrollComponenHistoryDetailCompanies(String idComponent, List<String> idCompanies);

    @Query(value = "select a.id, a.number as employeeId, a.full_name as fullName, coalesce (b.current_amount,a.nominal,0) as currentAmmount, \n" +
            "coalesce (b.new_amount,a.nominal,0) as newAmmount, b.id as idHistory from\n" +
            "(select a.*, b.number, b.full_name from employee_payroll_component as a \n" +
            "inner join employes as b on a.id_employee = b.id where a.status in ('AKTIF','BASIC') and b.status = 'AKTIF' and id_component= ?1 ) as a\n" +
            "inner join\n" +
            "(select a.*,b.effective_date from employee_payroll_component_history_detail as a \n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id where a.status = 'AKTIF') as b \n" +
            "on a.id = b.id_employee_payroll_component\n" +
            "where b.effective_date = ?2", nativeQuery = true)
    List<EmployeePayrollComponentHistoryDetailDto> listEmployeePayrollComponentHistoryDetailDate(String idComponent, LocalDate tanggal);

    @Query(value = "select a.id, a.number as employeeId, a.full_name as fullName, coalesce (b.current_amount,a.nominal,0) as currentAmmount, coalesce (b.new_amount,a.nominal,0) as newAmmount, b.id as idHistory from\n" +
            "(select a.*, b.number, b.full_name from employee_payroll_component as a \n" +
            "inner join employes as b on a.id_employee = b.id where a.status in ('AKTIF','BASIC') and b.status = 'AKTIF') as a\n" +
            "left join\n" +
            "(select * from employee_payroll_component_history_detail where status = 'AKTIF') as b on a.id = b.id_employee_payroll_component",nativeQuery = true)
    List<EmployeePayrollComponentHistoryDetailDto> listEmployeePayrollComponenHistoryDetailAll();

    EmployeePayrollComponent findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    @Query(value = "select sum(nominal)as total_allowance from employee_payroll_component as a\n" +
            "inner join payroll_component as b on a.id_component = b.id\n" +
            "where a.status in ('AKTIF','BASIC') and a.id_employee = ?1 and a.nominal > 0\n" +
            "and b.jenis_component in ('ALLOWANCE','BASIC') ",nativeQuery = true)
    BigDecimal totalAllowance(String idEmployee);

    @Query(value = "select c.id as id, c.id_employee as idEmployee, c.id_component as idComponent, c.jenis_component as jenisComponent,a.new_amount as nominal,\n" +
            "c.nominal as nominalAsli, c.jenis_nominal as jenisNominal, c.status as status, d.jenis_component as jenisComponentMaster, d.code as code,\n" +
            "d.name as componentName, d.rumus as rumus, d.status as statusComponent, d.taxable as taxable, d.kode_rumus_default as kodeRumusDefault,\n" +
            "d.potongan_absen as potonganAbsen, d.potongan_terlambat as potonganTerlambat, d.zakat as zakat\n" +
            "from employee_payroll_component_history_detail as a\n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join employee_payroll_component as c on a.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where c.status <> 'HAPUS' and b.effective_date <= ?1 and b.end_date >= ?2 and c.id_employee is not null\n" +
            "and c.id_employee = ?3\n" +
            "group by c.id_employee,c.id_component", nativeQuery = true)
    List<EmployeePayrollComponentDto> listEmployeePayrollComponent1(LocalDate tanggalMulai, LocalDate tanggalSelesai, String idEmployee);

    @Query(value = "select b.* from\n" +
            "(select c.id_employee,c.id_component,max(b.date_update)as terbaru from employee_payroll_component_history_detail as a\n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join employee_payroll_component as c on a.id_employee_payroll_component = c.id\n" +
            "where c.status <> 'HAPUS' and b.effective_date <= ?1 and b.end_date >= ?2 and c.id_employee is not null\n" +
            "and c.id_employee = ?3 group by c.id_employee,c.id_component) as a\n" +
            "inner join\n" +
            "(select c.id as id, c.id_employee as idEmployee, c.id_component as idComponent, c.jenis_component as jenisComponent,a.new_amount as nominal,\n" +
            "c.nominal as nominalAsli, c.jenis_nominal as jenisNominal, c.status as status, d.jenis_component as jenisComponentMaster, d.code as code,\n" +
            "d.name as componentName, d.rumus as rumus, d.status as statusComponent, d.taxable as taxable, d.kode_rumus_default as kodeRumusDefault,\n" +
            "d.potongan_absen as potonganAbsen, d.potongan_terlambat as potonganTerlambat, d.zakat as zakat, b.date_update, d.thr, d.thr_pajak as thrPajak, d.persentase, d.perhitungan as perhitungan, d.maksimal_hari as maksimalHari \n" +
            "from employee_payroll_component_history_detail as a\n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join employee_payroll_component as c on a.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where c.status <> 'HAPUS' and b.effective_date <= ?1 and b.end_date >= ?2 and c.id_employee is not null\n" +
            "and c.id_employee = ?3)as b \n" +
            "on a.id_employee = b.idEmployee and a.id_component = b.idComponent and a.terbaru = b.date_update", nativeQuery = true)
    List<EmployeePayrollComponentDto> listEmployeePayrollComponent(LocalDate tanggalMulai, LocalDate tanggalSelesai, String idEmployee);

    @Query(value = "select id,idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal, effectiveDate,endDate, date(concat(year(now()),'-',if(length(month(now()))=1,concat('0',month(now())),month(now())),'-',sampai_tanggal))as sampaiTanggal from\n" +
            "(select '1' as satu,id,id_component as idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal,\n" +
            "coalesce(effective_date,date('1945-01-01')) as effectiveDate,coalesce(end_date,date('1945-01-01'))as endDate from\n" +
            "(select a.id,a.id_component,c.code as codeComponent, c.name as nameComponent, b.id as idEmployee, b.full_name as employeeName,\n" +
            "a.jenis_component as jenisComponent,b.number as nip, a.nominal as nominal, a.jenis_nominal as jenisNominal from employee_payroll_component as a\n" +
            "inner join employes as b on a.id_employee = b.id \n" +
            "inner join payroll_component as c on a.id_component = c.id\n" +
            "where a.id_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and b.status_aktif in ('AKTIF','BASIC')\n" +
            "order by b.number) as a\n" +
            "left join\n" +
            "(select a.*,b.effective_date,b.end_date from\n" +
            "(select max(b.date_update)as terakhir_update,a.id_employee_payroll_component from employee_payroll_component_history_detail as a \n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join payroll_component as c on b.id_payroll_component = c.id \n" +
            "where b.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and c.status in ('AKTIF','BASIC')\n" +
            "group by a.id_employee_payroll_component) as a\n" +
            "inner join \n" +
            "(select a.date_update, b.id_employee_payroll_component, a.effective_date , a.end_date from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history\n" +
            "where a.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC')) as b \n" +
            "on a.terakhir_update = b.date_update and a.id_employee_payroll_component = b.id_employee_payroll_component) as b\n" +
            "on a.id = b.id_employee_payroll_component\n" +
            "order by end_date,nip) as a\n" +
            "left join\n" +
            "(SELECT '1' as satu, sampai_tanggal FROM payroll_periode where status = 'AKTIF') b on a.satu = b.satu order by endDate, nip\n", nativeQuery = true)
    List<EmployeePayrollComponentEndDateDto> listEmployeePayrollComponentEndDate(String idComponent);


    @Query(value = "select id,idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal, effectiveDate,endDate, date(concat(year(now()),'-',if(length(month(now()))=1,concat('0',month(now())),month(now())),'-',sampai_tanggal))as sampaiTanggal from\n" +
            "(select '1' as satu,id,id_component as idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal,\n" +
            "coalesce(effective_date,date('1945-01-01')) as effectiveDate,coalesce(end_date,date('1945-01-01'))as endDate from\n" +
            "(select a.id,a.id_component,c.code as codeComponent, c.name as nameComponent, b.id as idEmployee, b.full_name as employeeName,\n" +
            "a.jenis_component as jenisComponent,b.number as nip, a.nominal as nominal, a.jenis_nominal as jenisNominal from employee_payroll_component as a\n" +
            "inner join (select * from employes where id in (?2) and status = 'AKTIF') as b on a.id_employee = b.id \n" +
            "inner join payroll_component as c on a.id_component = c.id\n" +
            "where a.id_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and b.status_aktif in ('AKTIF','BASIC')\n" +
            "order by b.number) as a\n" +
            "left join\n" +
            "(select a.*,b.effective_date,b.end_date from\n" +
            "(select max(b.date_update)as terakhir_update,a.id_employee_payroll_component from employee_payroll_component_history_detail as a \n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join payroll_component as c on b.id_payroll_component = c.id \n" +
            "where b.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and c.status in ('AKTIF','BASIC')\n" +
            "group by a.id_employee_payroll_component) as a\n" +
            "inner join \n" +
            "(select a.date_update, b.id_employee_payroll_component, a.effective_date , a.end_date from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history\n" +
            "where a.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC')) as b \n" +
            "on a.terakhir_update = b.date_update and a.id_employee_payroll_component = b.id_employee_payroll_component) as b\n" +
            "on a.id = b.id_employee_payroll_component\n" +
            "order by end_date,nip) as a\n" +
            "left join\n" +
            "(SELECT '1' as satu, sampai_tanggal FROM payroll_periode where status = 'AKTIF') b on a.satu = b.satu order by endDate, nip\n", nativeQuery = true)
    List<EmployeePayrollComponentEndDateDto> listEmployeePayrollComponentEndDateWithCompanies(String idComponent, List<String> idCompany);



        @Query(value = "select id,idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal, effectiveDate,endDate, date(concat(year(now()),'-',if(length(month(now()))=1,concat('0',month(now())),month(now())),'-',sampai_tanggal))as sampaiTanggal from\n" +
            "(select '1' as satu,id,id_component as idComponent,codeComponent,nameComponent,idEmployee,nip,employeeName,jenisComponent,nominal,jenisNominal,\n" +
            "coalesce(effective_date,date('1945-01-01')) as effectiveDate,coalesce(end_date,date('1945-01-01'))as endDate from\n" +
            "(select a.id,a.id_component,c.code as codeComponent, c.name as nameComponent, b.id as idEmployee, b.full_name as employeeName,\n" +
            "a.jenis_component as jenisComponent,b.number as nip, a.nominal as nominal, a.jenis_nominal as jenisNominal from employee_payroll_component as a\n" +
            "inner join (select * from employes where id_company in (?2) and status = 'AKTIF') as b on a.id_employee = b.id \n" +
            "inner join payroll_component as c on a.id_component = c.id\n" +
            "where a.id_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and b.status_aktif in ('AKTIF','BASIC')\n" +
            "order by b.number) as a\n" +
            "left join\n" +
            "(select a.*,b.effective_date,b.end_date from\n" +
            "(select max(b.date_update)as terakhir_update,a.id_employee_payroll_component from employee_payroll_component_history_detail as a \n" +
            "inner join employee_payroll_component_history as b on a.id_employee_payroll_component_history = b.id\n" +
            "inner join payroll_component as c on b.id_payroll_component = c.id \n" +
            "where b.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC') and c.status in ('AKTIF','BASIC')\n" +
            "group by a.id_employee_payroll_component) as a\n" +
            "inner join \n" +
            "(select a.date_update, b.id_employee_payroll_component, a.effective_date , a.end_date from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history\n" +
            "where a.id_payroll_component = ?1 and a.status in ('AKTIF','BASIC') and b.status in ('AKTIF','BASIC')) as b \n" +
            "on a.terakhir_update = b.date_update and a.id_employee_payroll_component = b.id_employee_payroll_component) as b\n" +
            "on a.id = b.id_employee_payroll_component\n" +
            "order by end_date,nip) as a\n" +
            "left join\n" +
            "(SELECT '1' as satu, sampai_tanggal FROM payroll_periode where status = 'AKTIF') b on a.satu = b.satu order by endDate, nip\n", nativeQuery = true)
    List<EmployeePayrollComponentEndDateDto> listEmployeePayrollComponentEndDateWithCompaniesMainCompanies(String idComponent, List<String> idCompany);

}