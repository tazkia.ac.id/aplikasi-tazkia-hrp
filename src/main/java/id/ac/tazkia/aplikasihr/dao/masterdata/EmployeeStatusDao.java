package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.EmployeeStatusAktifDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeStatus;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeStatusDao extends PagingAndSortingRepository<EmployeeStatus, String>, CrudRepository<EmployeeStatus, String> {

        EmployeeStatus findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);
        List<EmployeeStatus> findByStatusAndEmployesOrderByDariTanggalString(StatusRecord statusRecord, Employes employes);

        @Query(value = "select coalesce(status_employee,'KONTRAK') as statusEmployee from employee_status where id_employee = ?1 \n" +
                "and dari_tanggal <= date(now()) and status = 'AKTIF' order by dari_tanggal desc limit 1", nativeQuery = true)
        String statusEmployee(String idEmployee);

}
