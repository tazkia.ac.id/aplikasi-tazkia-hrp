package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JobLevelDao extends PagingAndSortingRepository<JobLevel, String>, CrudRepository<JobLevel, String> {

    Page<JobLevel> findByStatusOrderByJobLevel(StatusRecord statusRecord, Pageable pageable);

    Page<JobLevel> findByStatusAndJobLevelOrderByJobLevel(StatusRecord statusRecord, String search, Pageable page);

    List<JobLevel> findByStatusOrderByJobLevel(StatusRecord statusRecord);

    JobLevel findByJobLevelAndStatus(String jobLevel, StatusRecord statusRecord);

}
