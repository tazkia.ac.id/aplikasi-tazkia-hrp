package id.ac.tazkia.aplikasihr.dao.setting.timeoff;

import id.ac.tazkia.aplikasihr.dto.TimeOffJensiDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.timeoff.TimeOffJenis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TimeOffJenisDao extends PagingAndSortingRepository<TimeOffJenis, String>, CrudRepository<TimeOffJenis, String> {

    Page<TimeOffJenis> findByStatusAndTahunOrderByTimeOffName(StatusRecord statusRecord, String tahun, Pageable pageable);

    List<TimeOffJenis> findByStatusOrderByTimeOffName(StatusRecord statusRecord);

    @Query(value = "select a.id, if(status_quota = 'AKTIF', concat(time_off_name,' Tahun ', tahun ,', Sisa Quota : ',(count-coalesce(diambil,0)),' hari'), time_off_name) as nama from\n" +
            "(select * from time_off_jenis where tahun = year(now())-1 and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now())-1 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis\n" +
            "union\n" +
            "select a.id, if(status_quota = 'AKTIF', concat(time_off_name,' Tahun ', tahun ,', Sisa Quota : ',(count-coalesce(diambil,0)),' hari'), time_off_name) as nama from\n" +
            "(select * from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now()) and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis\n" +
            "union\n" +
            "select id, time_off_name as nama from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'NONAKTIF'", nativeQuery = true)
    List<TimeOffJensiDto> listTimeOffJenisSebelumBulanTujuh(String idEmployee);

    @Query(value = "select a.id, if(status_quota = 'AKTIF', concat(time_off_name,' Tahun ', tahun ,', Sisa Quota : ',(count-coalesce(diambil,0)),' hari'), time_off_name) as nama from\n" +
            "(select * from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now()) and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis\n" +
            "union\n" +
            "select id, time_off_name as nama from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota in ('NONAKTIF')", nativeQuery = true)
    List<TimeOffJensiDto> listTimeOffJenisSetelahBulanTujuh(String idEmployee);


    @Query(value = "select id, time_off_name as nama from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota in ('NONAKTIF')", nativeQuery = true)
    List<TimeOffJensiDto> listTImeOffKaryawanKontrak(String idEmployee);

    Page<TimeOffJenis> findByStatusAndTahunAndTimeOffNameContainingIgnoreCaseOrderByTimeOffName(StatusRecord statusRecord, String tahun, String search, Pageable pageable);

    @Query(value = "select count, coalesce(b.diambil + d.diambil + d.diambil,0) as diambil from\n" +
            "(select '1' as aa, sum(count) as count from time_off_jenis where status = 'AKTIF' and status_quota = 'AKTIF')a\n" +
            "left join \n" +
            "(select '1' as aa,sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) = year(now())) b \n" +
            "on a.aa = b.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(tanggal_time_off_sampai, concat(year(now()),'-01-01') )+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off_sampai) = year(now()) and year(tanggal_time_off) < year(now())) as c\n" +
            "on a.aa = c.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(concat(year(now()),'-12-31'), tanggal_time_off)+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) > year(now()))d\n" +
            "on a.aa = d.aa", nativeQuery = true)
    Object dasboardTimeOff2(String Karyawan);


    @Query(value = "select count, (coalesce(b.diambil + c.diambil + d.diambil,0)) as diambil from\n" +
            "(select '1' as aa, sum(count) as count from time_off_jenis where status = 'AKTIF' and status_quota = 'AKTIF')a\n" +
            "left join \n" +
            "(select '1' as aa,sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) = year(now())) b \n" +
            "on a.aa = b.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(tanggal_time_off_sampai, concat(year(now()),'-01-01') )+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off_sampai) = year(now()) and year(tanggal_time_off) < year(now())) as c\n" +
            "on a.aa = c.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(concat(year(now()),'-12-31'), tanggal_time_off)+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) > year(now()))d\n" +
            "on a.aa = d.aa", nativeQuery = true)
    Object dasboardTimeOff(String Karyawan);


    @Query(value = "select concat(count, ' + ', coalesce(sisa,0), ' Sisa tahun sebelumnya') as counts, count, coalesce(diambil,0) as diambil  from \n" +
            "(select '1' as ada, count, diambil from\n" +
            "(select * from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now()) and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by tahun,id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis) as a\n" +
            "left join\n" +
            "(select '1' as ada, coalesce(count) - coalesce(diambil) as sisa from\n" +
            "(select * from time_off_jenis where tahun = year(now())-1 and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now())-1 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by tahun,id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis) as b on a.ada = b.ada", nativeQuery = true)
    Object dashboardTimeOffSebelumBulanTujuh1(String karyawan);


    @Query(value = "select concat(coalesce(count,0), ' + ', coalesce(sisa,0), ' Sisa tahun sebelumnya') as counts, coalesce(count,0) as count, coalesce(diambil,0) as diambil  from \n" +
            "(select '1' as ada, count, diambil from\n" +
            "(select * from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now()) and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by tahun,id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis) as a\n" +
            "left join\n" +
            "(select '1' as ada, coalesce(count,0) - coalesce(diambil,0) as sisa from\n" +
            "(select * from time_off_jenis where tahun = year(now())-1 and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now())-1 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by tahun,id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis) as b on a.ada = b.ada", nativeQuery = true)
    Object dashboardTimeOffSebelumBulanTujuh(String karyawan);

    @Query(value = "select coalesce(count,0) as counts, coalesce(count,0) as count, coalesce(diambil,0) as diambil from\n" +
            "(select * from time_off_jenis where tahun = year(now()) and status = 'AKTIF' and status_quota = 'AKTIF')as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where tahun = year(now()) and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 group by tahun,id_time_off_jenis) as b\n" +
            "on a.id = b.id_time_off_jenis", nativeQuery = true)
    Object dashboardTimeOffSetelahBulanTujuh(String karyawan);

    @Query(value = "select count - (coalesce(b.diambil + c.diambil + d.diambil,0)) as sisa from\n" +
            "(select '1' as aa, sum(count) as count from time_off_jenis where status = 'AKTIF' and status_quota = 'AKTIF')a\n" +
            "left join \n" +
            "(select '1' as aa,sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) = year(now())) b \n" +
            "on a.aa = b.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(tanggal_time_off_sampai, concat(year(now()),'-01-01') )+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off_sampai) = year(now()) and year(tanggal_time_off) < year(now())) as c\n" +
            "on a.aa = c.aa\n" +
            "left join\n" +
            "(select '1' as aa,coalesce(sum(datediff(concat(year(now()),'-12-31'), tanggal_time_off)+1),0) as diambil,tanggal_time_off, tanggal_time_off_sampai from time_off_request as a \n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve in ('WAITING','APPROVED') and a.id_employee = ?1 \n" +
            "and b.status = 'AKTIF' and b.status_quota = 'AKTIF' and year(tanggal_time_off) = year(now()) and year(tanggal_time_off_sampai) > year(now()))d\n" +
            "on a.aa = d.aa", nativeQuery = true)
    Long sisaTimeOff1(String Karyawan);

    @Query(value = "select coalesce(count,0) - coalesce(diambil,0) as sisa from\n" +
            "(select * from time_off_jenis where tahun = ?1 and status = 'AKTIF' and status_quota = 'AKTIF' and id = ?2)as a\n" +
            "left join\n" +
            "(select id_time_off_jenis, sum(datediff(tanggal_time_off_sampai, tanggal_time_off)+1) as diambil from time_off_request \n" +
            "where id_time_off_jenis = ?2 and tahun = ?1 and id_employee = ?3 and status = 'AKTIF' and status_approve in ('WAITING','APPROVED') ) as b\n" +
            "on a.id = b.id_time_off_jenis", nativeQuery = true)
    Long sisaTimeOff(String tahun, String idTimeOffJenis, String karyawan);



}
