package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.entity.AnnouncementCompanies;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AnnouncementCompaniesDao extends PagingAndSortingRepository<AnnouncementCompanies, String>, CrudRepository<AnnouncementCompanies, String> {


}
