package id.ac.tazkia.aplikasihr.dao.setting.payroll;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollPeriodeDao extends PagingAndSortingRepository<PayrollPeriode, String>, CrudRepository<PayrollPeriode, String> {

    PayrollPeriode findByStatus(StatusRecord statusRecord);

}
