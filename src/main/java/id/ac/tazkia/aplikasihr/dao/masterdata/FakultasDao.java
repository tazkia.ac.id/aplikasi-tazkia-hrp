package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Fakultas;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FakultasDao extends PagingAndSortingRepository<Fakultas, String>, CrudRepository<Fakultas, String> {

    Page<Fakultas> findByStatusAndCompaniesIdInAndKodeFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasEnglishContainingIgnoreCaseOrderByNamaFakultas(StatusRecord status, List<String> companies, String search, StatusRecord status2, List<String> companies2, String search2,StatusRecord status3, List<String> companies3, String search3, Pageable pageable);

    Page<Fakultas> findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord status, List<String> companies, Pageable pageable);

    List<Fakultas> findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord status, List<String> companies);

}
