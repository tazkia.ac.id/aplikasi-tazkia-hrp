package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollInfo;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface EmployeePayrollInfoDao extends PagingAndSortingRepository<EmployeePayrollInfo, String>, CrudRepository<EmployeePayrollInfo, String> {

    EmployeePayrollInfo findByStatusAndEmployes (StatusRecord statusRecord, Employes employes);

    List<EmployeePayrollInfo> findByStatusOrderByEmployes(StatusRecord statusRecord);


    @Query(value = "select persentase from rumus_pajak where rumus = ?1 and \n" +
            "dari <= ?2 and sampai >= ?2", nativeQuery = true)
    BigDecimal cariRumusPajak(String rumus, BigDecimal totalKenaPajak);

}
