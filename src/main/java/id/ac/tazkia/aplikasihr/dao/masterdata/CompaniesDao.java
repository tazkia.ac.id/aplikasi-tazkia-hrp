package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CompaniesDao extends PagingAndSortingRepository<Companies, String>, CrudRepository<Companies, String> {

    Page<Companies> findByStatusNotOrderByCompanyName(StatusRecord statusRecord, Pageable pageable);

    Page<Companies> findByStatusNotAndCompanyNameContainingIgnoreCaseOrStatusNotAndCompanyTypeContainingIgnoreCaseOrStatusNotAndRegencyContainingIgnoreCaseOrderByCompanyName(StatusRecord statusRecord, String search1, StatusRecord statusRecord2, String search2, StatusRecord statusRecord3, String search3, Pageable pageable);

    List<Companies> findByStatusNotOrderByCompanyName(StatusRecord statusRecord);


    List<Companies> findByStatusNotAndIdInOrderByCompanyName(StatusRecord statusRecord, List<String> id);


    Companies findByStatusAndId(StatusRecord statusRecord, String id);

    List<Companies> findByStatusAndIdInOrderById (StatusRecord statusRecord, List<String> id);

    List<Companies> findByStatusAndCompanyTypeOrderByCompanyName(StatusRecord statusRecord, String type);

    List<Companies> findByStatusAndCompanyTypeAndIdInOrderByCompanyName(StatusRecord statusRecord, String type, List<String> id);


    List<Companies> findByStatusOrderByCompanyName(StatusRecord statusRecord);

    List<Companies> findByStatusAndIdInOrderByCompanyName(StatusRecord statusRecord, List<String> id);


    List<Companies> findByStatusAndIdIn(StatusRecord statusRecord, List<String> id);

    Companies findByStatusNotAndCompanyName(StatusRecord statusRecord, String company);

}
