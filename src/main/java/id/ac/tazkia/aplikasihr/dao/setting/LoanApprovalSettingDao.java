package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import id.ac.tazkia.aplikasihr.entity.setting.LoanApprovalSetting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LoanApprovalSettingDao extends PagingAndSortingRepository<LoanApprovalSetting, String>, CrudRepository<LoanApprovalSetting, String> {

    List<LoanApprovalSetting> findByStatusOrderBySequence(StatusRecord statusRecord);

    Page<LoanApprovalSetting> findByStatusOrderBySequence(StatusRecord statusRecord, Pageable pageable);

    Page<LoanApprovalSetting> findByStatusAndCompaniesIdInOrderBySequence(StatusRecord statusRecord, List<String> idCompanies, Pageable pageable);


    LoanApprovalSetting findByStatusAndEmployesAndSequence(StatusRecord statusRecord, Employes employes, Integer sequence);

    LoanApprovalSetting findByStatusNotAndEmployes(StatusRecord statusRecord, Employes employes);

    LoanApprovalSetting findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);


    Integer countByStatus(StatusRecord statusRecord);

}
