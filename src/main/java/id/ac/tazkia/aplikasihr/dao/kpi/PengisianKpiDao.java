package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.dto.kpi.RekapPengisianKpiDto;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.hibernate.annotations.Where;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengisianKpiDao extends PagingAndSortingRepository<PengisianKpi, String>, CrudRepository<PengisianKpi, String> {

    @Modifying
    @Query(value = "update pengisian_kpi set status = 'HAPUS' where id = ?1"
            ,nativeQuery = true)
    void hapusKpi(String id);

    Page<PengisianKpi> findByStatusAndStatusKpiAndEmployesAndBulanAndTahunOrderByTanggal(StatusRecord statusRecord, StatusRecord statusRecord1, Employes employes, String bulan, String tahun, Pageable pageable);

    List<PengisianKpi> findByStatusAndStatusKpiAndEmployesAndPositionKpiAndBulanAndTahunOrderByTanggal(
            StatusRecord statusRecord, StatusRecord statusRecord1,
            Employes employes, PositionKpi positionKpi, String bulan, String tahun
    );


    @Query(value = "select id as positionKpi, id_position as idPosition, kpi,indikator,coalesce(id_kpi,'-') as idKpi, id_employee as employee, coalesce(jml,0) as jml, syarat_aa as syaratAa, syarat_bb as syaratBb, syarat_cc as syaratCc, bulan as bulan, tahun as tahun, grade as grade, COALESCE(nilai, '-') as nilai from\n" +
            "            (select id, kpi, indikator,id_position, syarat_aa, syarat_bb, syarat_cc from position_kpi where id_position = ?1\n" +
            "            and status = 'AKTIF') as a\n" +
            "            left join\n" +
            "            (select id_kpi,count(id_kpi) as jml, id_employee, bulan, tahun from pengisian_kpi where status = 'AKTIF' and id_employee = ?2 and bulan = ?3 and tahun = ?4 and status_kpi = 'OPEN' group by id_kpi) as b\n" +
            "            on a.id = b.id_kpi\n" +
            "            left join\n" +
            "            (select id_position_kpi, grade, nilai from penilaian_kpi where id_employee = ?2 and bulan = ?3 and tahun = ?4) as c\n" +
            "            on a.id = c.id_position_kpi", nativeQuery = true)
    List<RekapPengisianKpiDto> rekapPengisianKpi(String idPosition, String idEmployee, String bulan, String tahun);

    @Query(value = "select id as positionKpi, id_position as idPosition, kpi,indikator,coalesce(id_kpi,'-') as idKpi, coalesce(jml,0) as jml from\n" +
            "(select id, kpi, indikator,id_position from position_kpi where id_position = ?1\n" +
            "and status = 'AKTIF') as a\n" +
            "left join\n" +
            "(select id_kpi,count(id_kpi) as jml from pengisian_kpi where status = 'AKTIF' and id_employee = ?2 and bulan = ?3 and tahun = ?4 and status_kpi = 'OPEN' group by id_kpi) as b\n" +
            "on a.id = b.id_kpi", nativeQuery = true)
    List<RekapPengisianKpiDto> rekapPengisianKpiDosen(String idPosition, String idEmployee, String bulan, String tahun);


}
