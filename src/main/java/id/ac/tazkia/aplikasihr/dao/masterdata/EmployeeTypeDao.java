package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeType;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeTypeDao extends PagingAndSortingRepository<EmployeeType, String>, CrudRepository<EmployeeType, String> {

    List<EmployeeType> findByStatus(StatusRecord statusRecord);

    EmployeeType findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
