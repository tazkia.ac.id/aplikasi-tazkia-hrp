package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.dto.AnnouncementDto;
import id.ac.tazkia.aplikasihr.entity.Announcement;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AnnouncementDao extends PagingAndSortingRepository<Announcement, String>, CrudRepository<Announcement, String> {

    Page<Announcement> findByStatusOrderByDateUpdateDesc(StatusRecord statusRecord, Pageable pageable);

    @Query(value = "select a.id as id, a.id_employee as idEmployee, a.isi as isi, \n" +
            "a.file_upload as fileUpload, a.date_update as dateUpdate, c.full_name as fullName from announcement as a\n" +
            "inner join announcement_companies as b on a.id = b.id_announcement\n" +
            "inner join employes as c on a.id_employee = c.id\n" +
            "where a.status = 'AKTIF' and b.id_companies = ?1\n" +
            "group by a.id\n" +
            "order by a.date_update desc",
            countQuery = "select count(id) as jml from\n" +
                    "(select a.* from announcement as a\n" +
                    "inner join announcement_companies as b on a.id = b.id_announcement\n" +
                    "where a.status = 'AKTIF' and b.id_companies = ?1\n" +
                    "group by a.id)a\n" +
                    "group by id" ,
            nativeQuery = true)
    Page<AnnouncementDto> pagePengumuman(String idCompanies, Pageable pageable);

    @Query(value = "select a.id as id, a.id_employee as idEmployee, a.isi as isi, \n" +
            "a.file_upload as fileUpload, a.date_update as dateUpdate, c.full_name as fullName from announcement as a\n" +
            "inner join announcement_companies as b on a.id = b.id_announcement\n" +
            "inner join employes as c on a.id_employee = c.id\n" +
            "where a.status = 'AKTIF'\n" +
            "group by a.id\n" +
            "order by a.date_update desc",
            countQuery = "select count(id) as jml from\n" +
                    "(select a.* from announcement as a\n" +
                    "inner join announcement_companies as b on a.id = b.id_announcement\n" +
                    "where a.status = 'AKTIF'\n" +
                    "group by a.id)a\n" +
                    "group by id" ,
            nativeQuery = true)
    Page<AnnouncementDto> pagePengumumanAdmin(Pageable pageable);


    @Query(value = "select a.id as id, a.id_employee as idEmployee, a.isi as isi, \n" +
            "a.file_upload as fileUpload, a.date_update as dateUpdate, c.full_name as fullName from announcement as a\n" +
            "inner join announcement_companies as b on a.id = b.id_announcement\n" +
            "inner join employes as c on a.id_employee = c.id\n" +
            "where a.status = 'AKTIF' and b.id_companies in (?1) \n" +
            "group by a.id\n" +
            "order by a.date_update desc",
            countQuery = "select count(id) as jml from\n" +
                    "(select a.* from announcement as a\n" +
                    "inner join announcement_companies as b on a.id = b.id_announcement\n" +
                    "where a.status = 'AKTIF' and b.id_companies in (?1) \n" +
                    "group by a.id)a\n" +
                    "group by id" ,
            nativeQuery = true)
    Page<AnnouncementDto> pagePengumumanAdminHrd(List<String> idCompanies, Pageable pageable);
}
