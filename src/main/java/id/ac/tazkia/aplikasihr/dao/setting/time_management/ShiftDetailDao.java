package id.ac.tazkia.aplikasihr.dao.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ShiftDetailDao extends PagingAndSortingRepository<ShiftDetail, String>, CrudRepository<ShiftDetail, String> {

    List<ShiftDetail> findByStatusOrderByShiftName(StatusRecord statusRecord);

}
