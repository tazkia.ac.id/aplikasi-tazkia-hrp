package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.reports.OvertimeSumaryDto;
import id.ac.tazkia.aplikasihr.dto.request.TarikOvertimeDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.OvertimeRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface OverTimeRequestDao extends PagingAndSortingRepository<OvertimeRequest, String>, CrudRepository<OvertimeRequest, String> {

    Page<OvertimeRequest> findByStatusAndEmployesOrderByTanggal(StatusRecord statusRecord, Employes employes, Pageable pageable);


    @Query(value = "select b.full_name from overtime_request as a inner join employes as b on a.id_employee = b.id where a.id = ?1", nativeQuery = true)
    String cariEmployee(String id);

    Page<OvertimeRequest> findByStatusAndEmployesOrderByTanggalDesc(StatusRecord statusRecord, Employes employes, Pageable pageable);

    OvertimeRequest findByStatusAndId(StatusRecord statusRecord, String id);

    @Query(value = "select coalesce(sum(jam),0) as terpakai from overtime_request where status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 and week(tanggal_overtime) = week(?2) and year(tanggal_overtime) = year(?2) \n", nativeQuery = true)
    Integer cekOvertimeTerpakaiSeminggu(String idEmployee, LocalDate tanggal);

    @Query(value = "select coalesce(sum(jam),0) as terpakai from overtime_request where status = 'AKTIF' and status_approve in ('WAITING','APPROVED') and id_employee = ?1 and tanggal_overtime = ?2 ", nativeQuery = true)
    Integer cekOvertimeTerpakaiSehari(String idEmployee, LocalDate tanggal);

    @Query(value = "select count(id) as ada from overtime_request where id_employee = ?4 and tanggal_overtime = ?1 and status_approve in ('WAITING','APPROVED') and ((overtime_from <= ?2 and overtime_to >= ?2) or \n" +
            "(overtime_from <= ?3 and overtime_to >= ?3) or (overtime_from >= ?2 and overtime_to <= ?3))", nativeQuery = true)
    Integer cariDataOvertimeSerupa(LocalDate tanggal, LocalTime overtimeFrom, LocalTime overtimeTo, String idEmployee);

    @Query(value = "select id_employee as idEmployee, tanggal_overtime as tanggalOvertime,jam,status_overtime as statusOvertime \n" +
            "from overtime_request where id_employee = ?1 and status = 'AKTIF' \n" +
            "and status_approve = 'APPROVED' and tanggal_overtime >= ?2 and tanggal_overtime <= ?3 and status_overtime = ?4", nativeQuery = true)
    List<TarikOvertimeDto> tarikDataOvertimeEmployee(String idEmployee, LocalDate tanggalMulai, LocalDate tanggalSelesai, String status);

    @Query(value = "select id,number,full_name as fullName, group_concat(Date_format(tanggal, \"%d-%M-%Y\") order by tanggal asc SEPARATOR '\\n') as tanggal, group_concat(keterangan order by tanggal asc SEPARATOR '\\n') as keterangan, \n" +
            "group_concat(jam order by tanggal asc SEPARATOR '\\n') as jam, group_concat(if(status_overtime = 'OFF', 'Hari Libur','Hari Kerja') order by tanggal asc SEPARATOR '\\n') as status,\n" +
            "sum(jam) as totalJam, sum(nominalPerHari) as totalNominal from\n" +
            "(select a.*, onJamPertama + onJamBerikutnya + offJamPertama + offJamKedua + offJamBerikutnya as nominalPerHari from\n" +
            "(select a.*,if(status_overtime = 'ON',if(jam >= 1,hitungan_lembur * 1.5,0),0)as onJamPertama,\n" +
            "if(status_overtime = 'ON',if(jam >= 2,(hitungan_lembur * 2) * (jam - 1),0),0)as onJamBerikutnya,\n" +
            "if(status_overtime = 'OFF',if(jam >= 1,hitungan_lembur * 2,0),0)as offJamPertama,\n" +
            "if(status_overtime = 'OFF',if(jam >= 2,hitungan_lembur * 3,0),0)as offJamKedua,\n" +
            "if(status_overtime = 'OFF',if(jam >= 3,(hitungan_lembur * 4) * (jam - 2),0),0)as offJamBerikutnya from\n" +
            "(select a.*, round((b.new_amount/173),2) hitungan_lembur  from\n" +
            "(select b.id,b.number,b.full_name, tanggal_overtime  as tanggal, keterangan, \n" +
            "jam, status_overtime\n" +
            "from overtime_request as a \n" +
            "inner join employes as b on a.id_employee = b.id \n" +
            "where a.status = 'AKTIF' and status_approve = 'APPROVED'\n" +
            "and tanggal_overtime >= ?1 and tanggal_overtime <= ?2) as a \n" +
            "left join\n" +
            "(select id_employee, max(a.date_update) as terbaru, new_amount from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history \n" +
            "inner join employee_payroll_component as c on b.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where a.effective_date <= ?1 and a.end_date >= ?2 and d.status = 'BASIC' \n" +
            "and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'BASIC' group by id_employee) as b on a.id = b.id_employee) as a) as a)as a \n" +
            "group by id order by number", nativeQuery = true)
    List<OvertimeSumaryDto> summaryDataOvertime(LocalDate tanggalMulai, LocalDate tanggalSelesai);


    @Query(value = "select id,number,full_name as fullName, group_concat(Date_format(tanggal, \"%d-%M-%Y\") order by tanggal asc SEPARATOR '\\n') as tanggal, group_concat(keterangan order by tanggal asc SEPARATOR '\\n') as keterangan, \n" +
            "group_concat(jam order by tanggal asc SEPARATOR '\\n') as jam, group_concat(if(status_overtime = 'OFF', 'Hari Libur','Hari Kerja') order by tanggal asc SEPARATOR '\\n') as status,\n" +
            "sum(jam) as totalJam, sum(nominalPerHari) as totalNominal from\n" +
            "(select a.*, onJamPertama + onJamBerikutnya + offJamPertama + offJamKedua + offJamBerikutnya as nominalPerHari from\n" +
            "(select a.*,if(status_overtime = 'ON',if(jam >= 1,hitungan_lembur,0),0)as onJamPertama,\n" +
            "if(status_overtime = 'ON',if(jam >= 2,(hitungan_lembur) * (jam - 1),0),0)as onJamBerikutnya,\n" +
            "if(status_overtime = 'OFF',if(jam >= 1,hitungan_lembur,0),0)as offJamPertama,\n" +
            "if(status_overtime = 'OFF',if(jam >= 2,hitungan_lembur,0),0)as offJamKedua,\n" +
            "if(status_overtime = 'OFF',if(jam >= 3,(hitungan_lembur) * (jam - 2),0),0)as offJamBerikutnya from\n" +
            "(select a.*, if(type = 'TENDIK',20000,13000) hitungan_lembur  from\n" +
            "(select b.id,b.number,b.full_name, tanggal_overtime  as tanggal, keterangan, \n" +
            "jam, status_overtime,c.type \n" +
            "from overtime_request as a \n" +
            "inner join employes as b on a.id_employee = b.id inner join employee_type as c on b.id = c.id_employee \n" +
            "where a.status = 'AKTIF' and status_approve = 'APPROVED'\n" +
            "and tanggal_overtime >= ?1 and tanggal_overtime <= ?2) as a \n" +
            "left join\n" +
            "(select id_employee, max(a.date_update) as terbaru, new_amount from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history \n" +
            "inner join employee_payroll_component as c on b.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where a.effective_date <= ?1 and a.end_date >= ?2 and d.status = 'BASIC' \n" +
            "and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'BASIC' group by id_employee) as b on a.id = b.id_employee) as a) as a)as a \n" +
            "group by id order by number", nativeQuery = true)
    List<OvertimeSumaryDto> summaryDataOvertimeFlat(LocalDate tanggalMulai, LocalDate tanggalSelesai);

    @Query(value = "select id,number,full_name as fullName, group_concat(Date_format(tanggal, \"%d-%M-%Y\") order by tanggal asc SEPARATOR '\\n') as tanggal, group_concat(keterangan order by tanggal asc SEPARATOR '\\n') as keterangan, \n" +
            "group_concat(jam order by tanggal asc SEPARATOR '\\n') as jam, group_concat(if(status_overtime = 'OFF', 'Hari Libur','Hari Kerja') order by tanggal asc SEPARATOR '\\n') as status,\n" +
            "sum(jam) as totalJam, sum(nominalPerHari) as totalNominal from\n" +
            "(select a.*, onJamPertama + onJamBerikutnya + offJamPertama + offJamKedua + offJamBerikutnya as nominalPerHari from\n" +
            "(select a.*,if(status_overtime = 'ON',if(jam >= 1,hitungan_lembur,0),0)as onJamPertama,\n" +
            "if(status_overtime = 'ON',if(jam >= 2,(hitungan_lembur) * (jam - 1),0),0)as onJamBerikutnya,\n" +
            "if(status_overtime = 'OFF',if(jam >= 1,hitungan_lembur,0),0)as offJamPertama,\n" +
            "if(status_overtime = 'OFF',if(jam >= 2,hitungan_lembur,0),0)as offJamKedua,\n" +
            "if(status_overtime = 'OFF',if(jam >= 3,(hitungan_lembur) * (jam - 2),0),0)as offJamBerikutnya from\n" +
            "(select a.*, if(type = 'TENDIK',20000,13000) hitungan_lembur  from\n" +
            "(select b.id,b.number,b.full_name, tanggal_overtime  as tanggal, keterangan, \n" +
            "jam, status_overtime\n" +
            "from overtime_request as a \n" +
            "inner join (select * from employes where status = 'AKTIF' and id_company in (?3)) as b on a.id_employee = b.id inner join employee_type as c on b.id = c.id_employee \n" +
            "where a.status = 'AKTIF' and status_approve = 'APPROVED'\n" +
            "and tanggal_overtime >= ?1 and tanggal_overtime <= ?2) as a \n" +
            "left join\n" +
            "(select id_employee, max(a.date_update) as terbaru, new_amount from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history \n" +
            "inner join employee_payroll_component as c on b.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where a.effective_date <= ?1 and a.end_date >= ?2 and d.status = 'BASIC' \n" +
            "and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'BASIC' group by id_employee) as b on a.id = b.id_employee) as a) as a)as a \n" +
            "group by id order by number", nativeQuery = true)
    List<OvertimeSumaryDto> summaryDataOvertimeCompanyFlat(LocalDate tanggalMulai, LocalDate tanggalSelesai, List<String> idCompany);
    @Query(value = "select id,number,full_name as fullName, group_concat(Date_format(tanggal, \"%d-%M-%Y\") order by tanggal asc SEPARATOR '\\n') as tanggal, group_concat(keterangan order by tanggal asc SEPARATOR '\\n') as keterangan, \n" +
            "group_concat(jam order by tanggal asc SEPARATOR '\\n') as jam, group_concat(if(status_overtime = 'OFF', 'Hari Libur','Hari Kerja') order by tanggal asc SEPARATOR '\\n') as status,\n" +
            "sum(jam) as totalJam, sum(nominalPerHari) as totalNominal from\n" +
            "(select a.*, onJamPertama + onJamBerikutnya + offJamPertama + offJamKedua + offJamBerikutnya as nominalPerHari from\n" +
            "(select a.*,if(status_overtime = 'ON',if(jam >= 1,hitungan_lembur * 1.5,0),0)as onJamPertama,\n" +
            "if(status_overtime = 'ON',if(jam >= 2,(hitungan_lembur * 2) * (jam - 1),0),0)as onJamBerikutnya,\n" +
            "if(status_overtime = 'OFF',if(jam >= 1,hitungan_lembur * 2,0),0)as offJamPertama,\n" +
            "if(status_overtime = 'OFF',if(jam >= 2,hitungan_lembur * 3,0),0)as offJamKedua,\n" +
            "if(status_overtime = 'OFF',if(jam >= 3,(hitungan_lembur * 4) * (jam - 2),0),0)as offJamBerikutnya from\n" +
            "(select a.*, round((b.new_amount/173),2) hitungan_lembur  from\n" +
            "(select b.id,b.number,b.full_name, tanggal_overtime  as tanggal, keterangan, \n" +
            "jam, status_overtime\n" +
            "from overtime_request as a \n" +
            "inner join (select * from employes where status = 'AKTIF' and id_company in (?3)) as b on a.id_employee = b.id \n" +
            "where a.status = 'AKTIF' and status_approve = 'APPROVED'\n" +
            "and tanggal_overtime >= ?1 and tanggal_overtime <= ?2) as a \n" +
            "left join\n" +
            "(select id_employee, max(a.date_update) as terbaru, new_amount from employee_payroll_component_history as a\n" +
            "inner join employee_payroll_component_history_detail as b on a.id = b.id_employee_payroll_component_history \n" +
            "inner join employee_payroll_component as c on b.id_employee_payroll_component = c.id\n" +
            "inner join payroll_component as d on c.id_component = d.id\n" +
            "where a.effective_date <= ?1 and a.end_date >= ?2 and d.status = 'BASIC' \n" +
            "and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'BASIC' group by id_employee) as b on a.id = b.id_employee) as a) as a)as a \n" +
            "group by id order by number", nativeQuery = true)
    List<OvertimeSumaryDto> summaryDataOvertimeCompany(LocalDate tanggalMulai, LocalDate tanggalSelesai, List<String> idCompany);

}
