package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcessDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttendanceLecturerImporProcessDetailDao extends PagingAndSortingRepository<AttendanceLecturerImporProcessDetail, String>, CrudRepository<AttendanceLecturerImporProcessDetail, String> {

    List<AttendanceLecturerImporProcessDetail> findByStatusAndAttendanceLecturerImporProcessOrderByTopik(StatusRecord statusRecord, AttendanceLecturerImporProcess attendanceLecturerImporProcess);

}
