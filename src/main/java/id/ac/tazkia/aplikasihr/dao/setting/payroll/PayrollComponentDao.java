package id.ac.tazkia.aplikasihr.dao.setting.payroll;

import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollBenefit;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PayrollComponentDao extends PagingAndSortingRepository<PayrollComponent, String>, CrudRepository<PayrollComponent, String> {

    Page<PayrollComponent> findByStatusAndJenisComponentOrderByName(StatusRecord statusRecord, String jenisKomponen, Pageable pageable);

    PayrollComponent findByIdAndJenisComponentAndStatus(String id, String jenisKomponen, StatusRecord statusRecord);

    List<PayrollComponent> findByStatusOrderByJenisComponent(StatusRecord statusRecord);

    PayrollComponent findByStatus(StatusRecord statusRecord);

    List<PayrollComponent> findByStatusNotOrderByJenisComponent(StatusRecord statusRecord);

    PayrollComponent findByStatusInAndId(List<StatusRecord> statusRecord, String idComponent);

}
