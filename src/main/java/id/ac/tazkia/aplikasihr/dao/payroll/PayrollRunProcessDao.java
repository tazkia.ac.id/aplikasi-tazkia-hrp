package id.ac.tazkia.aplikasihr.dao.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PayrollRunProcessDao extends PagingAndSortingRepository<PayrollRunProcess, String>, CrudRepository<PayrollRunProcess, String> {

    PayrollRunProcess findFirstByStatusOrderByTanggalInput(StatusRecord statusRecord);

    List<PayrollRunProcess> findByStatusNotOrderByTanggalInputDesc(StatusRecord statusRecord);

}
