package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.dto.attendance.AttendanceRunProcessDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttendanceLecturerImporProcessDao extends PagingAndSortingRepository<AttendanceLecturerImporProcess, String>, CrudRepository<AttendanceLecturerImporProcess, String> {

    AttendanceLecturerImporProcess findFirstByStatusOrderByTanggalInputAsc(StatusRecord statusRecord);

    List<AttendanceLecturerImporProcess> findByStatusNotOrderByTanggalInputDesc(StatusRecord statusRecord);

    @Query(value = "select a.id,company_name as companyName, tahun, bulan, tanggal_input as tanggalInput, tanggal_mulai as tanggalMulai, tanggal_selesai as tanggalSelesai, user_input as user, a.status as status, coalesce(done,0) as done, coalesce(errorr,0)as errorr from attendance_lecturer_impor_process as a\n" +
            "inner join companies as b on a.id_company = b.id\n" +
            "left join\n" +
            "(select count(id) as done, id_attendance_lecturer_impor_process from attendance_lecturer_impor_process_detail where status = 'DONE' group by id_attendance_lecturer_impor_process) as c on a.id = c.id_attendance_lecturer_impor_process\n" +
            "left join\n" +
            "(select count(id) as errorr, id_attendance_lecturer_impor_process from attendance_lecturer_impor_process_detail where status = 'ERROR' group by id_attendance_lecturer_impor_process) as d on a.id = d.id_attendance_lecturer_impor_process\n" +
            " where a.status <> 'HAPUS' order by tanggal_input desc", nativeQuery = true)
    List<AttendanceRunProcessDto> listAttendanceLecturerImporProcess();

}
