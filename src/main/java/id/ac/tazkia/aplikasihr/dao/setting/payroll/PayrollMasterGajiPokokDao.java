package id.ac.tazkia.aplikasihr.dao.setting.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollMasterGajiPokok;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollMasterGajiPokokDao extends PagingAndSortingRepository<PayrollMasterGajiPokok, String>, CrudRepository<PayrollMasterGajiPokok, String> {

    Page<PayrollMasterGajiPokok> findByStatusOrderByDescription(StatusRecord statusRecord, Pageable page);

    Page<PayrollMasterGajiPokok> findByStatusAndDescriptionContainingIgnoreCaseOrStatusAndJenjangPendidikanKodePendidikanContainingIgnoreCaseOrStatusAndJenjangPendidikanNamaPendidikanContainingIgnoreCaseOrStatusAndJobLevelJobLevelContainingIgnoreCaseOrderByDescription(StatusRecord statusRecord, String search1, StatusRecord statusRecord1, String search2, StatusRecord statusRecord2, String search3, StatusRecord statusRecord3, String search4, Pageable pageable);

    PayrollMasterGajiPokok findByStatusAndId(StatusRecord statusRecord, String id);
}
