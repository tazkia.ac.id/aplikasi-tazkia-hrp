package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeBahasa;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeBahasaDao extends PagingAndSortingRepository<EmployeeBahasa, String>, CrudRepository<EmployeeBahasa, String> {

    List<EmployeeBahasa> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
