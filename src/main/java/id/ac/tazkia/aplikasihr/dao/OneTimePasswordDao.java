package id.ac.tazkia.aplikasihr.dao;

import id.ac.tazkia.aplikasihr.entity.OneTimePassword;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface OneTimePasswordDao extends PagingAndSortingRepository<OneTimePassword, String>, CrudRepository<OneTimePassword, String> {

    List<OneTimePassword> findByStatusAndExpires(StatusRecord status, LocalDateTime expires);

}
