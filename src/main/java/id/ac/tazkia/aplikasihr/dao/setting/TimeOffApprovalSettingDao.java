package id.ac.tazkia.aplikasihr.dao.setting;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.TimeOffApprovalSetting;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TimeOffApprovalSettingDao extends PagingAndSortingRepository<TimeOffApprovalSetting, String>, CrudRepository<TimeOffApprovalSetting, String> {

    List<TimeOffApprovalSetting> findByStatusOrderBySequence(StatusRecord statusRecord);

    List<TimeOffApprovalSetting> findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord statusRecord, String search, StatusRecord statusRecord2, String search2);

    @Query(value = "select count(id) as ada from time_off_approval_setting where status = 'AKTIF' and id_position = ?1", nativeQuery = true)
    Integer cariNumberTimeOffApproval(String idPosition);

    TimeOffApprovalSetting findByStatusAndPositionAndPositionApprove(StatusRecord statusRecord, JobPosition position, JobPosition position1);

}
