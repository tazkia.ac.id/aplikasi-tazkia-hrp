package id.ac.tazkia.aplikasihr.dao.setting.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollBenefit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollBenefitDao extends PagingAndSortingRepository<PayrollBenefit, String>, CrudRepository<PayrollBenefit, String> {

    Page<PayrollBenefit> findByStatusOrderByName(StatusRecord statusRecord, Pageable pageable);

    PayrollBenefit findByIdAndStatus(String id, StatusRecord statusRecord);

}
