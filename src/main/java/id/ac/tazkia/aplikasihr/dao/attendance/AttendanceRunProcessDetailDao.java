package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcessDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttendanceRunProcessDetailDao extends PagingAndSortingRepository<AttendanceRunProcessDetail, String>, CrudRepository<AttendanceRunProcessDetail, String> {

    List<AttendanceRunProcessDetail> findByStatusAndAndAttendanceRunProcessOrderByEmployesNumber(StatusRecord statusRecord, AttendanceRunProcess attendanceRunProcess);

    @Query(value = "select count(id) as id, id_attendance_run_process from attendance_run_process_detail where status = ?1 group by id_attendance_run_process", nativeQuery = true)
    List<Object> statusAttendaceRun(StatusRecord statusRecord);

}
