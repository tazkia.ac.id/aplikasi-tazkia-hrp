package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.ApprovalRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApprovalRequestDao extends PagingAndSortingRepository<ApprovalRequest, String>, CrudRepository<ApprovalRequest, String> {

    List<ApprovalRequest> findByStatusAndPositionApproveDepartmentCompaniesIdInOrderBySequence(StatusRecord statusRecord, List<String> idCompanies);

    List<ApprovalRequest> findByStatusOrderBySequence(StatusRecord statusRecord);


    List<ApprovalRequest> findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord statusRecord, String search, StatusRecord status, String search2);

    List<ApprovalRequest> findByStatusAndPositionApproveDepartmentCompaniesIdInAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord statusRecord, List<String> idCompanies, String search, StatusRecord status, String search2);

    @Query(value = "select count(id) as ada from approval_request where status = 'AKTIF' and id_position = ?1", nativeQuery = true)
    Integer cariNumberApprovalRequest(String idPosition);

    ApprovalRequest findByStatusAndPositionAndPositionApprove(StatusRecord statusRecord, JobPosition position, JobPosition positoinApprove);

    List<ApprovalRequest> findByStatusAndPositionAndSequenceGreaterThanOrderBySequence(StatusRecord status, JobPosition position, Integer number);

    List<ApprovalRequest> findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord status, List<String> jobPosition, List<String> jobPosition2, Integer sequence);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from approval_request \n" +
            "where id_position in ?1 \n" +
            "and status = 'AKTIF' and sequence = ?2)a\n" +
            "left join\n" +
            "(select * from recruitment_request_approval where status = 'AKTIF' and id_recruitment_request = ?3) b\n" +
            "on a.id = b.id_recruitment_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalRecruitment(List<String> idPosition, Integer number, String idRequest);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from approval_request \n" +
            "where id_position in ?1 \n" +
            "and status = 'AKTIF')a\n" +
            "left join\n" +
            "(select * from recruitment_request_approval where status = 'AKTIF' and id_recruitment_request = ?2) b\n" +
            "on a.id = b.id_recruitment_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaAppprovalRecruitmentTotal(List<String> idPosition, String idRequest);

}
