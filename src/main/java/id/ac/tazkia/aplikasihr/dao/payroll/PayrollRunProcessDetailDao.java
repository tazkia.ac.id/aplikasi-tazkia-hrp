package id.ac.tazkia.aplikasihr.dao.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcess;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcessDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PayrollRunProcessDetailDao extends PagingAndSortingRepository<PayrollRunProcessDetail, String>, CrudRepository<PayrollRunProcessDetail, String> {

    List<PayrollRunProcessDetail> findByStatusAndPayrollRunProcess(StatusRecord statusRecord, PayrollRunProcess payrollRunProcess);

}
