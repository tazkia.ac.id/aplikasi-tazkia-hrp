package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.DepartmentClass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DepartmentClassDao extends PagingAndSortingRepository<DepartmentClass, String>, CrudRepository<DepartmentClass, String> {

    List<DepartmentClass> findByStatusOrderByClassName(StatusRecord status);

}
