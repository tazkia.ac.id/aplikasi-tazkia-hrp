package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequestApproval;
import id.ac.tazkia.aplikasihr.entity.setting.ApprovalRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RecruitmentRequestApprovalDao extends PagingAndSortingRepository<RecruitmentRequestApproval, String>, CrudRepository<RecruitmentRequestApproval, String> {

    Integer countByStatusAndApprovalRequestAndStatusApprove(StatusRecord status, ApprovalRequest approvalRequest, StatusRecord statusApprove);

    List<RecruitmentRequestApproval> findByStatusAndRecruitmentRequest(StatusRecord statusRecord, RecruitmentRequest recruitmentRequest);

}
