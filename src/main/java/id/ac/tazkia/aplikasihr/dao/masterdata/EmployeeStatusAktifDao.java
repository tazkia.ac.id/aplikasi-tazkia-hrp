package id.ac.tazkia.aplikasihr.dao.masterdata;

import java.time.LocalDate;
import java.util.List;

import id.ac.tazkia.aplikasihr.dto.EmployeeStatusAktifDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeStatusKontrakDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasihr.dto.StatusEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployesAktifThrDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeStatusAktif;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;

public interface EmployeeStatusAktifDao extends PagingAndSortingRepository<EmployeeStatusAktif, String>, CrudRepository<EmployeeStatusAktif, String> {

    EmployeeStatusAktif findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    List<EmployeeStatusAktif> findByStatusAndEmployesOrderByDateUpdate(StatusRecord statusRecord, Employes employes);

    List<EmployeeStatusAktif> findByStatusAndStatusAktifAndEmployesFullNameAndEmployesNumberAndTanggalOrderByTanggal(StatusRecord statusRecord, String statusAktif, String fullName, String nik, LocalDate tanggal);
    EmployeeStatusAktif findByStatusAndStatusAktifAndEmployesFullNameAndTanggal(StatusRecord statusRecord, String statusAktif, String fullName, LocalDate tanggal);


    @Query(value = "select * from employee_status_aktif where status = 'AKTIF' and status_aktif in ('AKTIF','RESIGN') and id_employee = ?1 \n" +
            "and tanggal between ?2 and ?3 order by tanggal desc", nativeQuery = true)
    List<Object> listStatusEmployee(String idEmployee, LocalDate dari, LocalDate sampai);


    @Query(value = "select id, number, nick_name as nickName, full_name as fullName, tanggal_masuk as tanggalMasuk, tanggal_keluar as tanggalKeluar,bulan, if (bulan > 11, 'OK', 'NO') as status \n" +
            "from (select a.id, a.number, a.nick_name, a.full_name, max(b.tanggal) as  tanggal_masuk, TIMESTAMPDIFF(MONTH, max(b.tanggal),DATE_ADD(date(?2), INTERVAL 1 DAY)) as bulan from employes as a\n" +
            "left join employee_status_aktif as b on a.id = b.id_employee \n" +
            "where  a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and a.status_employee = 'KONTRAK' and b.status = 'AKTIF' and a.id_company = ?1 group by a.id) as a\n" +
            "left join\n" +
            "(select id_employee, max(tanggal) as tanggal_keluar from employee_status_aktif where status <> 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where bulan > 0\n" +
            "union\n" +
            "select a.id,number,nick_name as nickName, full_name as fullName, coalesce(b.tanggal, null) as tanggalMasuk, null as tanggalKeluar, 12 as bulan, 'YES' as status from employes as a\n" +
            "left join employee_status_aktif as b on a.id = b.id_employee\n" +
            "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and a.status_employee = 'TETAP' and a.id_company = ?1\n" +
            "and coalesce(b.status_aktif,'AKTIF') = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' order by number", nativeQuery = true)
    List<EmployesAktifThrDto> listKaryawanAktifThr(String company, LocalDate tanggalSelesai);


    @Query(value = "select if(datediff(date(now()),min(b.tanggal)) > 365,'OK','NO') as status, if(datediff(date(now()),min(b.tanggal)) > 548,'OK','NO') as statusdua from employes as a \n" +
    "inner join employee_status_aktif as b on a.id = b.id_employee \n" +
    "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and b.status = 'AKTIF' and b.status_aktif = 'AKTIF' \n" +
    "and a.id = ?1", nativeQuery = true)
    StatusEmployeeDto cariStatusTimeOff(String employeeId);

    @Query(value = "select c.id_employee as idEmployee, c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, coalesce(max(c.sampai_tanggal),c.dari_tanggal) as sampaiTanggal,\n" +
            "if(coalesce(max(c.sampai_tanggal),c.dari_tanggal) < date(now()),if(c.status_employee = 'TETAP','YES','NO'),'YES') as status from\n" +
            "(SELECT id_employee, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "FROM employee_status where status = 'AKTIF' and id_employee = ?1\n" +
            "     GROUP BY id_employee) AS latest_status \n" +
            "LEFT JOIN employee_status AS c ON c.id_employee = ?1 AND latest_status.max_tanggal_dari = c.dari_tanggal", nativeQuery = true)
    EmployeeStatusKontrakDto cariStatusKontrak(String idEmployee);


    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee order by b.tanggal_masuk,number", countQuery = "select count(id) as aaa from employes as a\n" +
            "left join\n" +
            "(select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee", nativeQuery = true)
    Page<EmployeeStatusAktifDto> listEmployeeStatusAktif(Pageable pageable);

    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee order by b.tanggal_masuk,number", nativeQuery = true)
    List<EmployeeStatusAktifDto> listEmployeeStatusAktifList();

    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1 order by b.tanggal_masuk,number", countQuery = "select count(id) as aaa from employes as a\n" +
            "left join\n" +
            "(select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1", nativeQuery = true)
    Page<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompany(String idCompanies, Pageable pageable);


//    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
//            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
//            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
//            "on a.id = b.id_employee where id_company = ?1 order by b.tanggal_masuk,number", nativeQuery = true)
//    List<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompanyList(String idCompanies);

    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1 order by b.tanggal_masuk,number", nativeQuery = true)
    List<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompanyList(String idCompanies);


    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1 and full_name like %?2% order by b.tanggal_masuk,number", countQuery = "select count(id) as aaa from employes as a\n" +
            "left join\n" +
            "(select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1 and full_name like %?2%", nativeQuery = true)
    Page<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompanyAndNama(String idCompanies, String search, Pageable pageable);

    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company = ?1 and full_name like %?2% order by b.tanggal_masuk,number", nativeQuery = true)
    List<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompanyAndNamaList(String idCompanies, String search);
    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company in (?1) order by b.tanggal_masuk,number", countQuery = "select count(id) as aaa from employes as a\n" +
            "left join\n" +
            "(select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company in (?1)", nativeQuery = true)
    Page<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompanies(List<String> idCompanies, Pageable pageable);

    @Query(value = "select id,number as nikAda,full_name as nama,b.tanggal_masuk as tanggalAda,b.status_aktif as statusAda from employes as a \n" +
            "left join (select id_employee,max(tanggal) as tanggal_masuk, status_aktif from employee_status_aktif \n" +
            "where status = 'AKTIF' and status_aktif = 'AKTIF' group by id_employee) as b\n" +
            "on a.id = b.id_employee where id_company in (?1) order by b.tanggal_masuk,number", nativeQuery = true)
    List<EmployeeStatusAktifDto> listEmployeeStatusAktifWithCompaniesList(List<String> idCompanies);

}
