package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeIbu;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeIbuDao extends PagingAndSortingRepository<EmployeeIbu, String>, CrudRepository<EmployeeIbu, String> {

    List<EmployeeIbu> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
