package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestSupportFileDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantFileSupport;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentFileSupport;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApplicantFileSupportDao extends PagingAndSortingRepository<ApplicantFileSupport, String>, CrudRepository<ApplicantFileSupport, String> {

    List<ApplicantFileSupport> findBySupportRequestAndApplicantAndStatus(RecruitmentRequest request, Applicant applicant, StatusRecord status);

    List<ApplicantFileSupport> findByStatus(StatusRecord status);

}
