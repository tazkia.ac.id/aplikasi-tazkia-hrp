package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpi;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;

public interface PenilaianKpiDao extends PagingAndSortingRepository<PenilaianKpi, String>, CrudRepository<PenilaianKpi, String> {


    PenilaianKpi findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(StatusRecord statusRecord, Employes employes,String bulan, String tahun, PositionKpi positionKpi);

    @Query(value = "SELECT sum((a.nilai/100)*b.nominal) as nominalKpi FROM dbhris.penilaian_kpi as a\n" +
            "inner join position_kpi as b on a.id_position_kpi = b.id and a.bulan = ?1 and a.tahun\t= ?2 and a.id_employee = ?3", nativeQuery = true)
    BigDecimal totalKpi(String bulan, String tahun, String idEmployee);
}
