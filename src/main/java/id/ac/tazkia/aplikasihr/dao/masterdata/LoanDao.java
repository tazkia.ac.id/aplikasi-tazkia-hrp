package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Vendor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LoanDao extends PagingAndSortingRepository<Loan, String>, CrudRepository<Loan, String> {

        Loan findByStatusAndName(StatusRecord statusRecord, String name);

        List<Loan> findByStatusOrderByName(StatusRecord statusRecord);


        List<Loan> findByStatusAndVendorOrderByName(StatusRecord statusRecord, Vendor vendor);

}
