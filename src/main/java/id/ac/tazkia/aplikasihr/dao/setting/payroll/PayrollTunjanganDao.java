package id.ac.tazkia.aplikasihr.dao.setting.payroll;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollTunjangan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollTunjanganDao extends PagingAndSortingRepository<PayrollTunjangan, String>, CrudRepository<PayrollTunjangan, String> {

    Page<PayrollTunjangan> findByStatusOrderByName(StatusRecord statusRecord, Pageable pageable);

    PayrollTunjangan findByIdAndStatus(String id, StatusRecord statusRecord);

}
