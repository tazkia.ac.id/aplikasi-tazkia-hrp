package id.ac.tazkia.aplikasihr.dao.config;

import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRoleDao extends PagingAndSortingRepository<UserRole, String>, CrudRepository<UserRole, String> {

    List<UserRole> findByUser(User user);

    UserRole findByUserAndRoleId(User user, String role);

}
