package id.ac.tazkia.aplikasihr.dao.config;

import id.ac.tazkia.aplikasihr.entity.config.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PermissionDao extends PagingAndSortingRepository<Permission, String>, CrudRepository<Permission, String> {



}
