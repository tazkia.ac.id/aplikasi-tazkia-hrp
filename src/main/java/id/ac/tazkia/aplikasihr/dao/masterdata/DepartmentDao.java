package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusClass;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DepartmentDao extends PagingAndSortingRepository<Department, String>, CrudRepository<Department, String> {

    Page<Department> findByStatusOrderByDepartmentCode(StatusRecord statusRecord, Pageable pageable);

    Page<Department> findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord statusRecord, List<String> companies, Pageable pageable);

    Page<Department> findByStatusAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord statusRecord, String search, StatusRecord statusRecord1, String search1, Pageable pageable);

    Page<Department> findByStatusAndCompaniesIdInAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord statusRecord, List<String> companies, String search, StatusRecord statusRecord1, String search1, Pageable pageable);

    List<Department> findByStatusOrderByDepartmentCode(StatusRecord statusRecord);

    List<Department> findByStatusAndDepartmentClassJenis(StatusRecord statusRecord, StatusClass statusClass);

    List<Department> findByStatusAndCompaniesOrderByDepartmentCode(StatusRecord statusRecord, Companies companies);
    List<Department> findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord statusRecord, List<String> idCompanies);

    Department findByStatusAndDepartmentCode(StatusRecord statusRecord, String code);

}
