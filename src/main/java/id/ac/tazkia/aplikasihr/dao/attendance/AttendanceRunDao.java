package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttendanceRunDao extends PagingAndSortingRepository<AttendanceRun, String>, CrudRepository<AttendanceRun, String> {

    AttendanceRun findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord statusRecord, String tahun, String bulan, String idEmployee);

    @Query(value = "select b.number, b.full_name, c.company_name, a.total_shift, a.total_attendance, a.total_time_off, a.total_absen, a.total_late, b.id, coalesce(a.overtime_working_day_first_hour,0) + coalesce(a.overtime_working_day_next_hour,0) as overtime_working_day,coalesce(a.overtime_holiday_first_hour,0) + coalesce(a.overtime_holiday_second_hour,0) + coalesce(a.overtime_working_day_next_hour,0) + coalesce(a.overtime_holiday_next_hour,0)  as overtime_holiday,a.id as ida from attendance_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join companies as c on b.id_company = c.id\n" +
            "where b.id_company = ?1 and a.tahun = ?2 and bulan = ?3 order by b.number", nativeQuery = true)
    List<Object> listAttendanceRun(String idCompany, String tahun, String bulan);

    @Query(value = "select b.number, b.full_name, c.company_name, a.total_shift, a.total_attendance, a.total_time_off, a.total_absen, a.total_late, b.id, coalesce(a.overtime_working_day_first_hour,0) + coalesce(a.overtime_working_day_next_hour,0) as overtime_working_day,coalesce(a.overtime_holiday_first_hour,0) + coalesce(a.overtime_holiday_second_hour,0) + coalesce(a.overtime_working_day_next_hour,0) + coalesce(a.overtime_holiday_next_hour,0) as overtime_holiday,a.id as ida from attendance_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join companies as c on b.id_company = c.id\n" +
            "where a.tahun = ?1 and bulan = ?2 order by b.number", nativeQuery = true)
    List<Object> listAttendanceRunAll(String tahun, String bulan);

}
