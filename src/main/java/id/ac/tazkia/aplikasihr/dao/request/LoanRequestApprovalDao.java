package id.ac.tazkia.aplikasihr.dao.request;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequest;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequestApproval;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LoanRequestApprovalDao extends PagingAndSortingRepository<LoanRequestApproval, String>, CrudRepository<LoanRequestApproval, String> {

    List<LoanRequestApproval> findByStatusOrderByLoanApprovalSettingSequence(StatusRecord statusRecord);

    Integer countByStatusAndStatusApproveAndLoanRequest(StatusRecord statusRecord, StatusRecord statusRecord1, LoanRequest loanRequest);

    List<LoanRequestApproval> findByStatusOrderByLoanRequest(StatusRecord statusRecord);

    List<LoanRequestApproval> findByStatusAndLoanRequest(StatusRecord statusRecord, LoanRequest loanRequest);

}
