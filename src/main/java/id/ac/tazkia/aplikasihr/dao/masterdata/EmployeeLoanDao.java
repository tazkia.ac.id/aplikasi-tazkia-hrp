package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.dto.LunasDto;
import id.ac.tazkia.aplikasihr.dto.payroll.BayarLoanDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface EmployeeLoanDao extends PagingAndSortingRepository<EmployeeLoan, String>, CrudRepository<EmployeeLoan, String> {

    Page<EmployeeLoan> findByStatusOrderByEffectiveDateDesc(StatusRecord statusRecord, Pageable pageable);

    Page<EmployeeLoan> findByStatusNotOrderByEffectiveDateDesc(StatusRecord statusRecord, Pageable pageable);


    Page<EmployeeLoan> findByStatusAndEmployesFullNameContainingIgnoreCaseOrStatusAndLoanNameContainingIgnoreCaseOrderByEffectiveDateDesc(StatusRecord statusRecord, String employee, StatusRecord statusRecord2, String loanName, Pageable pageable);

    Page<EmployeeLoan> findByStatusNotAndEmployesFullNameContainingIgnoreCaseOrStatusAndLoanNameContainingIgnoreCaseOrderByEffectiveDateDesc(StatusRecord statusRecord, String employee, StatusRecord statusRecord2, String loanName, Pageable pageable);

    List<EmployeeLoan> findByStatusOrderByEffectiveDateDesc(StatusRecord statusRecord);

    EmployeeLoan findByStatusAndId(StatusRecord statusRecord, String id);

    EmployeeLoan findByStatusNotAndId(StatusRecord statusRecord, String id);


    EmployeeLoan findByStatusAndEmployesNumberAndLoanNameAndAmountAndInstallmentAndInterest(StatusRecord statusRecord, String employeeNumber, String loanName, BigDecimal ammount, BigDecimal installment, BigDecimal interest);

    EmployeeLoan findByStatusNotAndEmployesNumberAndLoanNameAndAmountAndInstallmentAndInterest(StatusRecord statusRecord, String employeeNumber, String loanName, BigDecimal ammount, BigDecimal installment, BigDecimal interest);

    List<EmployeeLoan> findByStatusAndEmployesAndEffectiveDateBefore(StatusRecord statusRecord, Employes employes, LocalDate efectiveDate);


    @Query(value = "select id,effective_date as effectiveDate, amount, current_payment as currentPayment, sisa_bayar as sisaBayar ,cicilan_ke as cicilanKe from\n" +
            "(select *, amount-total_payment-total_bayar as sisa_bayar, installment_paid + jml + 1 as cicilan_ke from \n" +
            "(select a.*,coalesce(b.jml_bayar,0)as jml, coalesce(b.total_bayar,0)as total_bayar from\n" +
            "(select * from employee_loan where id_employee = ?1 and status = 'AKTIF' and effective_date <= ?2)a left join\n" +
            "(select a.id_employee_loan,count(a.id)as jml_bayar, sum(a.payment)as total_bayar from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan = b.id where a.status = 'AKTIF' and b.id_employee = ?1 and bulan <> ?3 and tahun <> ?4)b on a.id = b.id_employee_loan) as a) as aa", nativeQuery = true)
    List<BayarLoanDto> listLoanEmployee2(String idEmployee, LocalDate tanggal, String bulan, String tahun);

    @Query(value = "select id,effectiveDate,amount,currentPayment,sisaBayar,cicilanKe from \n" +
            "(select id,effective_date as effectiveDate, amount, current_payment as currentPayment, coalesce(sisa_bayar,0) as sisaBayar ,cicilan_ke as cicilanKe from\n" +
            "(select *, amount-total_payment-total_bayar as sisa_bayar, installment_paid + jml + 1 as cicilan_ke from \n" +
            "(select a.*,coalesce(b.jml_bayar,0)as jml, coalesce(b.total_bayar,0)as total_bayar from\n" +
            "(select * from employee_loan where id_employee = ?1 and status = 'AKTIF' and effective_date <= ?2)a left join\n" +
            "(select a.id_employee_loan,count(a.id)as jml_bayar, sum(a.payment)as total_bayar from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan = b.id where a.status = 'AKTIF' and b.id_employee = ?1 \n" +
            "and bulan <> ?3 and tahun <= ?4 group by a.id_employee_loan)b on a.id = b.id_employee_loan) as a) as aa)aaa\n" +
            "where sisaBayar > 0", nativeQuery = true)
    List<BayarLoanDto> listLoanEmployee1(String idEmployeee, LocalDate tanggal, String bulan, String tahun);

    @Query(value = "select id,effectiveDate,amount,currentPayment,sisaBayar,cicilanKe from \n" +
            "(select id,effective_date as effectiveDate, amount, current_payment as currentPayment, coalesce(sisa_bayar,0) as sisaBayar ,cicilan_ke as cicilanKe from\n" +
            "(select *, amount-total_payment-total_bayar as sisa_bayar, installment_paid + jml + 1 as cicilan_ke from \n" +
            "(select a.*,coalesce(b.jml_bayar,0)as jml, coalesce(b.total_bayar,0)as total_bayar from\n" +
            "(select * from employee_loan where id_employee = ?1 and status in ('AKTIF','LUNAS') and effective_date < ?2)a left join\n" +
            "(select a.id_employee_loan,count(a.id)as jml_bayar, sum(a.payment)as total_bayar from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan = b.id where a.status = 'AKTIF' and b.id_employee = ?1 \n" +
            "and date(concat(tahun,'-',bulan,'-','20')) < ?2 group by a.id_employee_loan)b on a.id = b.id_employee_loan) as a) as aa)aaa\n" +
            "where sisaBayar > 0", nativeQuery = true)
    List<BayarLoanDto> listLoanEmployee(String idEmployee, LocalDate tanggal, String bulan, String tahun);


    List<EmployeeLoan> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    List<EmployeeLoan> findByStatusNotAndEmployes(StatusRecord statusRecord, Employes employes);


    @Query(value = "select id from\n" +
            "(select a.id, a.amount, coalesce(sum(b.payment),0) as dibayar, if(a.amount > coalesce(sum(b.payment),0),'AKTIF','LUNAS') as status  from employee_loan as a \n" +
            "left join employee_loan_bayar as b on a.id = b.id_employee_loan\n" +
            "where a.status = ('AKTIF') group by a.id) as a where status = 'LUNAS'", nativeQuery = true)
    List<String> cekStatusLoanLunas();

    @Query(value = "select id, status from\n" +
            "(select a.id, a.amount, coalesce(sum(b.payment),0) as dibayar, if(a.amount > coalesce(sum(b.payment),0),'AKTIF','LUNAS') as status  \n" +
            "from employee_loan as a \n" +
            "left join employee_loan_bayar as b on a.id = b.id_employee_loan\n" +
            "where a.status in ('AKTIF','LUNAS') group by a.id) as a where status in ('LUNAS','AKTIF')", nativeQuery = true)
    List<LunasDto> cekStatusLoanLunasAktif();

}
