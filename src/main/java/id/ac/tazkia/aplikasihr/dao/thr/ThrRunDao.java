package id.ac.tazkia.aplikasihr.dao.thr;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRun;
import org.hibernate.sql.Delete;
import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ThrRunDao extends PagingAndSortingRepository<ThrRun, String>, CrudRepository<ThrRun, String> {

    ThrRun findByStatusAndTahunAndBulanAndEmployesId(StatusRecord statusRecord, String tahun, String bulan, String idEmployee);

    @Query(value = "delete from thr_run where tahun = ?1 and bulan = ?2 and id_employee in ?3", nativeQuery = true)
    @Modifying
    void deleteThrRun(String tahun, String bulan, List<String> idEmployee);

    @Query(value = "update thr_run set pay_slip = 'AKTIF' where tahun = ?1 and bulan = ?2 and id_employee in ?3 and status = 'AKTIF'", nativeQuery = true)
    @Modifying
    void updatePaySlipThr(String tahun, String bulan, List<String> idEmployee);

    List<ThrRun> findByStatusAndBulanAndTahunAndEmployesCompaniesOrderByEmployesNumber(StatusRecord statusRecord, String bulan, String tahun, Companies companies);

    List<ThrRun> findByStatusAndBulanAndTahunAndEmployesCompaniesIdOrderByEmployesNumber(StatusRecord statusRecord, String bulan, String tahun, String companies);

    List<ThrRun> findByStatusAndEmployesOrderByTahun(StatusRecord statusRecord, Employes employes);

}
