package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeJobLevelDao extends PagingAndSortingRepository<EmployeeJobLevel, String>, CrudRepository<EmployeeJobLevel, String> {

    Page<EmployeeJobLevel> findByStatusAndEmployesOrderByJobLevelJobLevel(StatusRecord statusRecord, Employes employes, Pageable pageable);

    Page<EmployeeJobLevel> findByStatusAndJobLevelOrderByEmployesNumber(StatusRecord statusRecord, JobLevel jobLevel, Pageable page);

}
