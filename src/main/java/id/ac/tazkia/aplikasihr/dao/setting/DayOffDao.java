package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DayOffDao extends PagingAndSortingRepository<DayOff, String>, CrudRepository<DayOff, String> {

    Page<DayOff> findByStatusOrderByDateDayOffDesc(StatusRecord statusRecord, Pageable pageable);

    Page<DayOff> findByStatusAndKeteranganContainingIgnoreCaseOrderByDateDayOffDesc(StatusRecord statusRecord, String search, Pageable pageable);

}
