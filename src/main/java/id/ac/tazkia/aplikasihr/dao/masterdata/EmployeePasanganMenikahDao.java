package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePasanganMenikah;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeePasanganMenikahDao extends PagingAndSortingRepository<EmployeePasanganMenikah, String>, CrudRepository<EmployeePasanganMenikah, String> {

    List<EmployeePasanganMenikah> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
