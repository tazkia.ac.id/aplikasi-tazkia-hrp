package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Bank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BankDao extends PagingAndSortingRepository<Bank, String>, CrudRepository<Bank, String> {

    List<Bank> findByStatusOrderByNamaBank(StatusRecord statusRecord);

    Page<Bank> findByStatusOrderByNamaBank(StatusRecord statusRecord, Pageable pageable);

    Page<Bank> findByStatusAndKodeBankContainingOrStatusAndNamaBankContainingIgnoreCaseOrderByNamaBank(StatusRecord statusRecord, String search, StatusRecord statusRecord2, String search2, Pageable pageable);

}
