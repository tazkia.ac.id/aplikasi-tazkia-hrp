package id.ac.tazkia.aplikasihr.dao.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceScheduleEvent;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AttendanceScheduleEventDao extends PagingAndSortingRepository<AttendanceScheduleEvent, String>, CrudRepository<AttendanceScheduleEvent, String> {

    List<AttendanceScheduleEvent> findByStatusAndScheduleEventOrderByEmployesFullName(StatusRecord statusRecord, ScheduleEvent scheduleEvent);

    List<AttendanceScheduleEvent> findByStatusAndScheduleEventAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullName(StatusRecord statusRecord, ScheduleEvent scheduleEvent, String search);

    List<AttendanceScheduleEvent> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
