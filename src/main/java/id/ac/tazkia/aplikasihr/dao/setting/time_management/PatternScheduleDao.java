package id.ac.tazkia.aplikasihr.dao.setting.time_management;

import id.ac.tazkia.aplikasihr.dto.setting.time_management.PatternScheduleListDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PatternScheduleDao extends PagingAndSortingRepository<PatternSchedule, String>, CrudRepository<PatternSchedule, String> {

    List<PatternSchedule> findByStatusOrderByScheduleName(StatusRecord statusRecord);

    PatternSchedule findByStatusAndId(StatusRecord statusRecord, String id);
    PatternSchedule findOneByStatusAndScheduleName(StatusRecord statusRecord, String name);

    Page<PatternSchedule> findByStatusOrderByScheduleName(StatusRecord statusRecord, Pageable page);

    Page<PatternSchedule> findByStatusAndScheduleNameContainingIgnoreCase(StatusRecord statusRecord, String nama, Pageable pageable);

    Integer countByStatusAndPattern(StatusRecord statusRecord, Pattern pattern);

    @Query(value = "select a.id, a.schedule_name as scheduleName, concat(b.name_day,':',time(CONVERT_TZ(b.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(b.shift_out,'+00:00','+07:00')))as minggu,\n" +
            "concat(c.name_day,':',time(CONVERT_TZ(c.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(c.shift_out,'+00:00','+07:00')))as senin,\n" +
            "concat(d.name_day,':',time(CONVERT_TZ(d.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(d.shift_out,'+00:00','+07:00')))as selasa,\n" +
            "concat(e.name_day,':',time(CONVERT_TZ(e.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(e.shift_out,'+00:00','+07:00')))as rabu,\n" +
            "concat(f.name_day,':',time(CONVERT_TZ(f.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(f.shift_out,'+00:00','+07:00')))as kamis,\n" +
            "concat(g.name_day,':',time(CONVERT_TZ(g.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(g.shift_out,'+00:00','+07:00')))as jumat,\n" +
            "concat(h.name_day,':',time(CONVERT_TZ(h.shift_in,'+00:00','+07:00')),'-',time(CONVERT_TZ(h.shift_out,'+00:00','+07:00')))as sabtu from pattern_schedule as a left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '1' and a.status='AKTIF')b on a.id = b.id \n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '2' and a.status='AKTIF')c on a.id = c.id \n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '3' and a.status='AKTIF')d on a.id = d.id\n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '4' and a.status='AKTIF')e on a.id = e.id\n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '5' and a.status='AKTIF')f on a.id = f.id\n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '6' and a.status='AKTIF')g on a.id = g.id\n" +
            "left join\n" +
            "(select a.id,schedule_name,d.shift_in,d.shift_out,c.id_day, name_day from pattern_schedule as a\n" +
            "left join pattern as b on a.id_pattern = b.id\n" +
            "left join shift_pattern as c on b.id = c.id_pattern\n" +
            "left join shift_detail as d on c.id_shift_detail = d.id \n" +
            "left join days as e on c.id_day = e.id where c.id_day = '7' and a.status='AKTIF')h on a.id = h.id\n" +
            "where a.status='AKTIF' group by a.id order by a.schedule_name", nativeQuery = true)
    List<PatternScheduleListDto> listPatternSchedule();

}
