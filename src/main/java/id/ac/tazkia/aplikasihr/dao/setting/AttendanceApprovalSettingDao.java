package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.dto.kpi.ListEmployeeKpiDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalAttendanceRequestListDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalOvertimeRequestListDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalTimeOffRequestListDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.AttendanceApprovalSetting;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.swing.text.Position;
import java.util.List;

public interface AttendanceApprovalSettingDao extends PagingAndSortingRepository<AttendanceApprovalSetting, String>, CrudRepository<AttendanceApprovalSetting, String> {

    List<AttendanceApprovalSetting> findByStatusOrderBySequence(StatusRecord statusRecord);

    AttendanceApprovalSetting findByStatusAndId(StatusRecord statusRecord, String id);

    List<AttendanceApprovalSetting> findByStatusAndPositionOrderBySequence(StatusRecord statusRecord, String position);

    List<AttendanceApprovalSetting> findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord statusRecord, String search, StatusRecord statusRecord2, String search2);

    @Query("select a from AttendanceApprovalSetting a where a.status = ?1")
    List<AttendanceApprovalSetting> listAttendanceApprovalSetting(StatusRecord statusRecord);

    @Query(value = "select count(id) as ada from attendance_approval_setting where status = 'AKTIF' and id_position = ?1", nativeQuery = true)
    Integer cariNumberAttendanceApproval(String idPosition);

    AttendanceApprovalSetting findByStatusAndPositionAndPositionApprove(StatusRecord statusRecord, JobPosition position, JobPosition position1);

    @Query(value = "SELECT number FROM\n" +
            "(SELECT b.id_employee,a.sequence AS number,'WAITING' AS status_approve,'' AS keterangan,'AKTIF' AS STATUS FROM attendance_approval_setting AS a\n" +
            "INNER JOIN employee_job_position AS b ON a.id_position_approve = b.id_position\n" +
            "INNER JOIN employes AS c ON b.id_employee = c.id \n" +
            "WHERE a.id_position IN ?1 AND a.status = 'AKTIF'\n" +
            "AND b.start_date <= DATE(NOW()) AND b.end_date >= DATE(NOW()) AND b.status='AKTIF'\n" +
            "AND c.status = 'AKTIF' ORDER BY sequence)a\n" +
            "ORDER BY number\n" +
            "LIMIT 1", nativeQuery = true)
    Integer getNomorAwal(List<String> position);

    @Query(value = "SELECT coalesce(number,0) as number FROM\n" +
            "(SELECT b.id_employee,a.sequence AS number,'WAITING' AS status_approve,'' AS keterangan,'AKTIF' AS STATUS FROM attendance_approval_setting AS a\n" +
            "INNER JOIN employee_job_position AS b ON a.id_position_approve = b.id_position\n" +
            "INNER JOIN employes AS c ON b.id_employee = c.id \n" +
            "WHERE a.id_position IN ?1 AND a.status = 'AKTIF'\n" +
            "AND b.start_date <= DATE(NOW()) AND b.end_date >= DATE(NOW()) AND b.status='AKTIF'\n" +
            "AND c.status = 'AKTIF')a\n" +
            "ORDER BY number desc limit 1", nativeQuery = true)
    Integer getJumlahApproval(List<String> position);

    @Query(value = "SELECT id_employee AS idEmployee, full_name as employee, number, idAttendanceApprovalSetting FROM\n" +
            "(SELECT b.id_employee,c.full_name,a.sequence AS number,'WAITING' AS status_approve,'' AS keterangan,'AKTIF' AS STATUS, a.id AS idAttendanceApprovalSetting FROM attendance_approval_setting AS a\n" +
            "INNER JOIN employee_job_position AS b ON a.id_position_approve = b.id_position\n" +
            "INNER JOIN employes AS c ON b.id_employee = c.id \n" +
            "WHERE a.id_position IN ?1 AND a.status = 'AKTIF'\n" +
            "AND b.start_date <= DATE(NOW()) AND b.end_date >= DATE(NOW()) AND b.status='AKTIF'\n" +
            "AND c.status = 'AKTIF')a\n" +
            "GROUP BY id_employee, number\n" +
            "ORDER BY number", nativeQuery = true)
    List<ApprovalAttendanceRequestListDto> listAttendanceApproval(List<String> position);

    @Query(value = "SELECT number, full_name as employee, FROM\n" +
            "(SELECT b.id_employee,c.full_name,a.sequence AS number,'WAITING' AS status_approve,'' AS keterangan,'AKTIF' AS STATUS, a.id AS idAttendanceApprovalSetting " +
            "FROM attendance_approval_setting AS a\n" +
            "INNER JOIN employee_job_position AS b ON a.id_position_approve = b.id_position\n" +
            "INNER JOIN employes AS c ON b.id_employee = c.id \n" +
            "WHERE a.id_position IN ?1 AND a.secuence = ?2 AND a.status = 'AKTIF'\n" +
            "AND b.start_date <= DATE(NOW()) AND b.end_date >= DATE(NOW()) AND b.status='AKTIF'\n" +
            "AND c.status = 'AKTIF')a\n" +
            "GROUP BY id_employee, number\n" +
            "ORDER BY number", nativeQuery = true)
    List<ApprovalAttendanceRequestListDto> listAttendanceApprovalAktif(List<String> position, Integer sequence);

    @Query(value = "SELECT * FROM\n" +
            "(SELECT c.sequence AS nomor, a.id AS attendanceRequest, coalesce(h.full_name,e.full_name) AS employee, \n" +
            "coalesce(j.position_name,u.position_name) as position , COALESCE(g.status_approve,f.status_approve,'WAITING') AS approve,coalesce(g.tanggal_approve,f.tanggal_approve) AS tanggalApprove, coalesce(g.keterangan,f.keterangan) as keterangan \n" +
            "FROM attendance_request AS a\n" +
            "INNER JOIN (select * from employee_job_position where id_employee = ?1 and status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id_employee = b.id_employee\n" +
            "INNER JOIN attendance_approval_setting AS c ON b.id_position = c.id_position\n" +
            "INNER JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS d ON c.id_position_approve = d.id_position\n" +
            "INNER JOIN employes AS e ON d.id_employee = e.id\n" +
            "inner join job_position as u on d.id_position = u.id\n" +
            "LEFT JOIN attendance_request_approval AS f ON a.id = f.id_attendance_request  AND c.sequence = f.number AND e.id = f.id_employee\n" +
            "LEFT JOIN attendance_request_approval AS g ON a.id = g.id_attendance_request  AND c.sequence = g.number\n" +
            "left join employes as h on g.id_employee = h.id\n" +
            "left join employee_job_position as i on h.id = i.id_employee and i.start_date <= date(g.tanggal_approve) and i.end_date >= date(g.tanggal_approve) and i.status = 'AKTIF'\n" +
            "left join job_position as j on i.id_position = j.id\n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND (e.status = 'AKTIF' OR e.status IS NULL)\n" +
            "AND b.id_employee = ?1 \n" +
            "ORDER BY a.id)a group by attendanceRequest,nomor,employee, tanggalApprove ORDER BY attendanceRequest, nomor", nativeQuery = true)
    List<ApprovalAttendanceRequestListDto> listAttendanceRequestApproval(String idEmployee);

    @Query(value="SELECT * FROM\n" +
            "(SELECT c.sequence AS nomor, a.id AS timeOffRequest, coalesce(h.full_name,e.full_name) AS employee, \n" +
            "coalesce(j.position_name,u.position_name) as position , COALESCE(g.status_approve,f.status_approve,'WAITING') AS approve,coalesce(g.tanggal_approve,f.tanggal_approve) AS tanggalApprove, coalesce(g.keterangan,f.keterangan) as keterangan \n" +
            "FROM time_off_request AS a\n" +
            "INNER JOIN (select * from employee_job_position where id_employee = ?1 and status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id_employee = b.id_employee\n" +
            "INNER JOIN attendance_approval_setting AS c ON b.id_position = c.id_position\n" +
            "INNER JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS d ON c.id_position_approve = d.id_position\n" +
            "INNER JOIN employes AS e ON d.id_employee = e.id\n" +
            "inner join job_position as u on d.id_position = u.id\n" +
            "LEFT JOIN time_off_request_approval AS f ON a.id = f.id_time_off_request  AND c.sequence = f.number AND e.id = f.id_employee\n" +
            "LEFT JOIN time_off_request_approval AS g ON a.id = g.id_time_off_request  AND c.sequence = g.number\n" +
            "left join employes as h on g.id_employee = h.id\n" +
            "left join employee_job_position as i on h.id = i.id_employee and i.start_date <= date(g.tanggal_approve) and i.end_date >= date(g.tanggal_approve) and i.status = 'AKTIF'\n" +
            "left join job_position as j on i.id_position = j.id\n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND (e.status = 'AKTIF' OR e.status IS NULL)\n" +
            "AND b.id_employee = ?1 \n" +
            "ORDER BY a.id)a group by timeOffRequest,nomor,employee, tanggalApprove ORDER BY timeOffRequest, nomor", nativeQuery = true)
    List<ApprovalTimeOffRequestListDto> listTimeOffRequestAttendance(String idEmployee);

    @Query(value="SELECT * FROM\n" +
            "(SELECT c.sequence AS nomor, a.id AS overTimeRequest, coalesce(h.full_name,e.full_name) AS employee, \n" +
            "coalesce(j.position_name,u.position_name) as position , COALESCE(g.status_approve,f.status_approve,'WAITING') AS approve,coalesce(g.tanggal_approve,f.tanggal_approve) AS tanggalApprove, coalesce(g.keterangan,f.keterangan) as keterangan \n" +
            "FROM overtime_request AS a\n" +
            "INNER JOIN (select * from employee_job_position where id_employee = ?1 and status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS b ON a.id_employee = b.id_employee\n" +
            "INNER JOIN attendance_approval_setting AS c ON b.id_position = c.id_position\n" +
            "INNER JOIN (select * from employee_job_position where status = 'AKTIF' and status_aktif='AKTIF' and start_date <= date(now()) and end_date >= date(now()) group by id_employee, id_position) AS d ON c.id_position_approve = d.id_position\n" +
            "INNER JOIN employes AS e ON d.id_employee = e.id\n" +
            "inner join job_position as u on d.id_position = u.id\n" +
            "LEFT JOIN overtime_request_approval AS f ON a.id = f.id_overtime_request  AND c.sequence = f.number AND e.id = f.id_employee\n" +
            "LEFT JOIN overtime_request_approval AS g ON a.id = g.id_overtime_request  AND c.sequence = g.number\n" +
            "left join employes as h on g.id_employee = h.id\n" +
            "left join employee_job_position as i on h.id = i.id_employee and i.start_date <= date(g.tanggal_approve) and i.end_date >= date(g.tanggal_approve) and i.status = 'AKTIF'\n" +
            "left join job_position as j on i.id_position = j.id\n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND (e.status = 'AKTIF' OR e.status IS NULL)\n" +
            "AND b.id_employee = ?1 \n" +
            "ORDER BY a.id)a group by overTimeRequest,nomor,employee, tanggalApprove ORDER BY overTimeRequest, nomor", nativeQuery = true)
    List<ApprovalOvertimeRequestListDto> listOvertimeRequestAttendance(String idEmployee);

    List<AttendanceApprovalSetting> findByStatusAndPositionAndSequenceGreaterThanOrderBySequence(StatusRecord statusRecord, JobPosition position, Integer number1);

    AttendanceApprovalSetting findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord statusRecord, List<String> jobPositions, List<String> jobPositions2, Integer sequence);

    List<AttendanceApprovalSetting> findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord statusRecord, List<String> jobPositions, List<String> jobPositions2, Integer sequence);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF' and sequence = ?3)a\n" +
            "left join\n" +
            "(select * from attendance_request_approval where status = 'AKTIF' and id_attendance_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalAttendance(List<String> id_position, String idAttendanceRequest, Integer nomor);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position = ?1\n" +
            "and status = 'AKTIF' and sequence = ?3)a\n" +
            "left join\n" +
            "(select * from attendance_request_approval where status = 'AKTIF' and id_attendance_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalAttendance2(String id_position, String idAttendanceRequest, Integer nomor);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF')a\n" +
            "left join\n" +
            "(select * from attendance_request_approval where status = 'AKTIF' and id_attendance_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalAttendanceTotal(List<String> idPosition, String idAttendaceRequest);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF' and sequence = ?3)a\n" +
            "left join\n" +
            "(select * from overtime_request_approval where status = 'AKTIF' and id_overtime_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalOvertime(List<String> id_position, String idOvertimeRequest, Integer nomor);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF')a\n" +
            "left join\n" +
            "(select * from overtime_request_approval where status = 'AKTIF' and id_overtime_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalOvertimeTotal(List<String> idPosition, String idOvertimeRequest);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF' and sequence = ?3)a\n" +
            "left join\n" +
            "(select * from time_off_request_approval where status = 'AKTIF' and id_time_off_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalTimeOff(List<String> id_position, String idTimeOffRequest, Integer nomor);

    @Query(value = "select count(ids)as sisa from\n" +
            "(select a.id as ids, b.id from\n" +
            "(select * from attendance_approval_setting \n" +
            "where id_position in ?1\n" +
            "and status = 'AKTIF')a\n" +
            "left join\n" +
            "(select * from time_off_request_approval where status = 'AKTIF' and id_time_off_request = ?2) b\n" +
            "on a.id = b.id_attendance_approval_setting)aa\n" +
            "where id is null", nativeQuery = true)
    Integer sisaApprovalTimeOffTotal(List<String> idPosition, String idTimeOffRequest);


    @Query(value = "select a.id,b.position_name as positionApprove, d.full_name as employeeApprove, coalesce(d.email_active,d.email_tazkia,'suprayogi20@gmail.com') as email, d.id as idEmployee \n" +
            "from attendance_approval_setting as a\n" +
            "inner join job_position as b on a.id_position_approve = b.id\n" +
            "inner join employee_job_position as c on a.id_position_approve = c.id_position\n" +
            "inner join employes as d on c.id_employee = d.id\n" +
            "where a.id_position in ?1 and a.status = 'AKTIF' and a.sequence = ?2\n" +
            "AND c.start_date <= DATE(NOW()) AND c.end_date >= DATE(NOW()) AND c.status='AKTIF' and c.status_aktif = 'AKTIF' order by c.end_date, c.start_date limit 1", nativeQuery = true)
    List<ApprovalListDetailDto> cariApprovalEmail(List<String> idPosition, Integer sequence);


    @Query(value = "select id_position as idPosition,id_employee as idEmployee,coalesce(number,'-') as number,full_name as fullName, position_name as positionName, jenis from\n" +
            "(select b.id_position,c.id_employee, d.number, d.full_name,e.position_name,'TENDIK' as jenis from employee_job_position as a\n" +
            "inner join attendance_approval_setting as b\n" +
            "on a.id_position = b.id_position_approve and a.id_employee = ?1 and a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and a.start_date <= date(now()) and a.end_date >= date(now()) and b.status = 'AKTIF'\n" +
            "inner join employee_job_position as c on b.id_position = c.id_position and c.status = 'AKTIF' and c.status_aktif = 'AKTIF'\n" +
            "and c.start_date <= date(now()) and c.end_date >= date(now())\n" +
            "inner join employes as d on c.id_employee = d.id\n" +
            "inner join job_position as e on c.id_position = e.id\n" +
            "union\n" +
            "select e.id_lecturer_class as id_position,e.id_employee,f.number, f.full_name,g.description as position_name,'DOSEN' as jenis from employee_job_position as a\n" +
            "inner join job_position as b on a.id_position = b.id and a.id_employee = ?1\n" +
            "and a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and a.start_date <= date(now()) and a.end_date >= date(now()) and b.status = 'AKTIF'\n" +
            "inner join department as c on b.id_department = c.id\n" +
            "inner join department_prodi as d on c.id = d.id_department\n" +
            "inner join lecturer as e on d.id = e.id_prodi\n" +
            "inner join employes as f on e.id_employee = f.id and f.id <> ?1\n" +
            "inner join lecturer_class as g on e.id_lecturer_class = g.id and kpi = 'AKTIF'\n" +
            "union\n" +
            "select i.id as id_position,c.id_employee, d.number, d.full_name,i.description as position_name,'DOSEN' as jenis from employee_job_position as a\n" +
            "inner join attendance_approval_setting as b\n" +
            "on a.id_position = b.id_position_approve and a.id_employee = ?1 and a.status = 'AKTIF' and a.status_aktif = 'AKTIF'\n" +
            "and a.start_date <= date(now()) and a.end_date >= date(now()) and b.status = 'AKTIF'\n" +
            "inner join employee_job_position as c on b.id_position = c.id_position and c.status = 'AKTIF' and c.status_aktif = 'AKTIF'\n" +
            "and c.start_date <= date(now()) and c.end_date >= date(now())\n" +
            "inner join employes as d on c.id_employee = d.id\n" +
            "inner join job_position as e on c.id_position = e.id\n" +
            "inner join department as f on e.id_department = f.id\n" +
            "inner join department_class as g on f.id_department_class = g.id and g.jenis = 'PRODI'\n" +
            "inner join lecturer as h on d.id = h.id_employee\n" +
            "inner join lecturer_class as i on h.id_lecturer_class = i.id) as a\n" +
            "order by jenis,position_name,full_name",nativeQuery = true)
    List<Object>listPenilaianKpi(String idEmployee);

}
