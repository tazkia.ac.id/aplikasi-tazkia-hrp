package id.ac.tazkia.aplikasihr.dao.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistory;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface EmployeePayrollComponentHistoryDao extends PagingAndSortingRepository<EmployeePayrollComponentHistory, String>, CrudRepository<EmployeePayrollComponentHistory, String> {

    Page<EmployeePayrollComponentHistory> findByStatusOrderByEffectiveDateDesc(StatusRecord statusRecord, Pageable pageable);

    Page<EmployeePayrollComponentHistory> findByStatusAndCompaniesIdInOrderByEffectiveDateDesc(StatusRecord statusRecord, List<String> idCompanies, Pageable pageable);


    EmployeePayrollComponentHistory findByStatusAndPayrollComponentAndEffectiveDate(StatusRecord statusRecord, PayrollComponent payrollComponent, LocalDate effectiveDate);

    EmployeePayrollComponentHistory findByStatusAndPayrollComponentAndEffectiveDateAndEndDate(StatusRecord statusRecord, PayrollComponent payrollComponent, LocalDate effectiveDate, LocalDate endDate);

    EmployeePayrollComponentHistory findByStatusAndId(StatusRecord statusRecord, String id);

    EmployeePayrollComponentHistory findFirstByStatusAndEffectiveDateAndEndDateOrderByDateUpdateDesc(StatusRecord statusRecord, LocalDate effektif, LocalDate end);

}
