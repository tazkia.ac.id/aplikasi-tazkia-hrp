package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.TaxStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TaxStatusDao extends PagingAndSortingRepository<TaxStatus, String>, CrudRepository<TaxStatus, String> {

    List<TaxStatus> findByStatusOrderById(StatusRecord statusRecord);

}
