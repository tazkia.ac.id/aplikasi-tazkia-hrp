package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface ApplicantScheduleDao extends PagingAndSortingRepository<ApplicantSchedule, String>, CrudRepository<ApplicantSchedule, String> {

    List<ApplicantSchedule> findByStatus(StatusRecord status);

    @Query(value = "select a.id, b.email_active, REGEXP_REPLACE(replace(replace(concat( 'Kepada ', b.full_name,'\\n',\n" +
            "'Perihal : ',replace(jenis_undangan, '_', ' '),'\\n', deskripsi,'\\n', 'Tanggal ', replace(jenis_undangan, '_', ' '),' : ', DATE_FORMAT(tanggal, \"%d-%M-%Y\"),'\\n', \n" +
            "'Waktu ', replace(jenis_undangan, '_', ' '),' : ', DATE_FORMAT(waktu,\"%H:%i\"),' WIB','\\n', 'Bertemu dengan : ', c.full_name ),'&nbsp;',' '),'<br>','\\n'), '<.+?>', '') as isi from applicant_schedule as a\n" +
            "inner join applicant as b on a.id_applicant = b.id\n" +
            "inner join employes as c on a.bertemu_dengan = c.id where status_kirim_email = 'WAITING'", nativeQuery = true)
    List<Object[]> listSentEmail();

    @Query(value = "select a.id, d.full_name, a.date_update, concat( 'Kepada ', b.full_name,'\\n',\n" +
            "'Perihal : ',replace(jenis_undangan, '_', ' '),'\\n', deskripsi,'\\n', 'Tanggal ', replace(jenis_undangan, '_', ' '),' : ', DATE_FORMAT(tanggal, \"%d-%M-%Y\"),'\\n', \n" +
            "'Waktu ', replace(jenis_undangan, '_', ' '),' : ', DATE_FORMAT(waktu,\"%H:%i\"),' WIB','\\n', 'Bertemu dengan : ', c.full_name) as isi, c.id as idEmployes from applicant_schedule as a\n" +
            "inner join applicant as b on a.id_applicant = b.id\n" +
            "inner join employes as c on a.bertemu_dengan = c.id inner join employes as d on a.id_employes=d.id where a.status='AKTIF' and b.id=?1", nativeQuery = true,
            countQuery = "select count(a.id) from applicant_schedule as a\n" +
                    "inner join applicant as b on a.id_applicant = b.id\n" +
                    "inner join employes as c on a.id_employes = c.id where a.status='AKTIF' and b.id=?1")
    Page<Object[]> listAnnouncmentApplicant(String idApplicant, Pageable page);

    List<ApplicantSchedule> findByStatusAndTanggal(StatusRecord status, LocalDate tanggal);

}
