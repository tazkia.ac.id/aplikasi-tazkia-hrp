package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKartuIdentitas;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeKartuIdentitasDao extends PagingAndSortingRepository<EmployeeKartuIdentitas, String>, CrudRepository<EmployeeKartuIdentitas, String> {

    EmployeeKartuIdentitas findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

    List<EmployeeKartuIdentitas> findByStatusAndEmployesOrderById(StatusRecord statusRecord, Employes employes);

}
