package id.ac.tazkia.aplikasihr.dao.masterdata;

import java.time.LocalDate;
import java.util.List;

import id.ac.tazkia.aplikasihr.dto.employes.EmployeeAllDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployesDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.ac.tazkia.aplikasihr.dto.EmployeeDto;
import id.ac.tazkia.aplikasihr.dto.api.JumlahEmployeeDto2;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;

public interface EmployesDao extends JpaRepository<Employes, String> {

    Employes findByUser(User user);

    Employes findByIdAbsen(Integer idAbsen);

    Employes findByStatusAndIdAbsen(StatusRecord statusRecord, Integer idAbsen);

    Integer countByStatusAndIdAbsen(StatusRecord statusRecord, Integer idAbsen);

    Page<Employes> findByStatusOrderByNumber(StatusRecord statusRecord, Pageable page);

    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF'\n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal  and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF' ) as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF'", nativeQuery = true)
    Page<EmployesDto> listEmployeeAll(Pageable pageable);


    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF' and a.id_company = ?1 \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal  and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF') as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF' and id_company = ?1", nativeQuery = true)
    Page<EmployesDto> listEmployeeAllCompany(String idCompany, Pageable pageable);


    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF' and a.id_company in (?1) \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF') as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF' and id_company in (?1)", nativeQuery = true)
    Page<EmployesDto> listEmployeeAllCompanies(List<String> idCompany, Pageable pageable);

    Page<Employes> findByStatusAndCompaniesIdInOrderByNumber(StatusRecord statusRecord, List<String> idCompany , Pageable page);


    Employes findByEmailActiveAndStatus(String email, StatusRecord statusRecord);

    Page<Employes> findByStatusAndCompaniesOrderByNumber(StatusRecord statusRecord, Companies companies, Pageable pageable);

    @Query(value = "select a.id from\n" +
            "(select a.id,full_name,b.id as ids from employes as a\n" +
            "left join\n" +
            "(select * from employee_payroll_component as a \n" +
            "where id_component = ?1) as b on a.id = b.id_employee\n" +
            "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF')as a\n" +
            "where ids is null\n", nativeQuery = true)
    List<String> karyawanBelumTunjangan(String idComponent);

    List<Employes> findByStatusAndStatusAktifAndIdInOrderByNumber(StatusRecord statusRecord, String statusRecord2, List<String> idEmployee);

    List<Employes> findByStatusAndStatusAktifAndCompaniesAndIdInOrderByNumber(StatusRecord statusRecord, String statusRecord2, Companies companies, List<String> idEmployee);

    List<Employes> findByStatusAndStatusAktifAndCompaniesMainCompanyAndIdInOrderByNumber(StatusRecord statusRecord, String statusRecord2, Companies companies, List<String> idEmployee);

    List<Employes> findByStatusAndStatusAktifAndCompaniesOrderByNumber(StatusRecord statusRecord, String statusRecord1, Companies companies);

    List<Employes> findByStatusAndStatusAktifAndCompaniesIdInOrderByNumber(StatusRecord statusRecord, String statusRecord1, List<String> companies);

    List<Employes> findByStatusAndStatusAktifOrderByNumber(StatusRecord statusRecord, String statusRecord1);

    List<Employes> findByStatusAndStatusAktif(StatusRecord statusRecord, String status);

    List<Employes> findByStatusAndStatusAktifAndCompaniesIdIn(StatusRecord statusRecord, String status, List<String> idCompanies);
    List<Employes> findByStatusAndStatusAktifAndId(StatusRecord statusRecord, String status, String id);


    List<Employes> findByStatusOrderByNumber(StatusRecord statusRecord);

    List<Employes> findByStatusAndCompaniesIdInOrderByNumber(StatusRecord statusRecord, List<String> idCompanies);

    List<Employes> findByStatusAndCompaniesMainCompanyIdInOrderByNumber(StatusRecord statusRecord, List<String> idCompanies);



    Employes findByStatusAndId(StatusRecord statusRecord, String id);

    List<Employes> findByStatusAndStatusAktifAndIdIn(StatusRecord statusRecord, String statusRecord2, List<String> idEmployee);

    List<Employes> findByStatusAndStatusAktifAndCompanies(StatusRecord statusRecord, String status, Companies companies);

    List<Employes> findByStatusAndCompaniesId(StatusRecord statusRecord, String companies);

    @Query(value = "select id from employes where status = 'AKTIF' and id_company = ?1", nativeQuery = true)
    List<String> cariEmployeeCompanyAktif(String idCompany);

    @Query(value = "select id from employes where status = 'AKTIF' and id_company in ?1", nativeQuery = true)
    List<String> cariEmployeeCompanyAktif1(List<String> idCompany);

    @Query(value = "select id from employes where status = 'AKTIF' and id_company = ?1", nativeQuery = true)
    List<String> cariEmployeeCompanyAktif2(String idCompany);


    Page<Employes> findByStatusAndNickNameContainingIgnoreCaseOrStatusAndFullNameContainingIgnoreCaseOrderByNumber(StatusRecord statusRecord, String number, StatusRecord statusRecord1, String nikName, Pageable page);

    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF'\n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF') as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "where fullName like %?1% or nickName like %?1% or number like %?1% or idAbsen like %?1% \n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF' and (number like %?1% or full_name like %?1% or nick_name like %?1% or id_absen like %?1%)", nativeQuery = true)
    Page<EmployesDto> listEmployeeSearch(String search, Pageable pageable);


    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF' and a.id_company = ?2 \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF') as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "where fullName like %?1% or nickName like %?1% or number like %?1% or idAbsen like %?1% \n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF' and id_company = ?2 and (number like %?1% or full_name like %?1% or nick_name like %?1% or id_absen like %?1%)", nativeQuery = true)
    Page<EmployesDto> listEmployeeSearchCompany(String search, String idCompany, Pageable pageable);

    @Query(value = "select a.*, b.type, coalesce(concat('LECTURER ',c.id_lecturer_class), 'STAFF') as classs from\n" +
            "(SELECT a.id, a.number, a.id_absen as idAbsen, a.nick_name as nickName, a.full_name as fullName, company_name as company, a.gender, d.status_aktif as status, d.tanggal,\n" +
            "c.status_employee as statusEmployee, c.dari_tanggal as dariTanggal, c.sampai_tanggal as sampaiTanggal\n" +
            " FROM employes AS a\n" +
            "INNER JOIN companies AS b ON a.id_company = b.id and a.status = 'AKTIF' and a.id_company in (?2) \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee, MAX(date_update) AS max_tanggal,MAX(tanggal) as max FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max_tanggal = d.date_update and d.status = 'AKTIF') as a\n" +
            "left join employee_type as b on a.id = b.id_employee and b.status = 'AKTIF'\n" +
            "left join lecturer as c on a.id = c.id_employee and c.status = 'AKTIF'\n" +
            "where fullName like %?1% or nickName like %?1% or number like %?1% or idAbsen like %?1% \n" +
            "group by a.id order by fullName", countQuery = "select count(id) as id from employes where status = 'AKTIF' and id_company in (?2) and (number like %?1% or full_name like %?1% or nick_name like %?1% or id_absen like %?1%)", nativeQuery = true)
    Page<EmployesDto> listEmployeeSearchCompanies(String search, List<String> idCompany, Pageable pageable);

    Page<Employes> findByStatusAndCompaniesIdInAndNickNameContainingIgnoreCaseOrStatusAndFullNameContainingIgnoreCaseOrderByNumber(StatusRecord statusRecord, List<String> idCompany, String number, StatusRecord statusRecord1, String nikName, Pageable page);

    Page<Employes> findByStatusAndCompaniesAndNickNameContainingIgnoreCaseOrStatusAndFullNameContainingIgnoreCaseOrderByNumber(StatusRecord statusRecord, Companies companies, String number, StatusRecord statusRecord1, String nikName, Pageable page);

    Page<Employes> findByStatusOrderByFullName(StatusRecord statusRecord, Pageable pageable);

    @Query(value = "SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
            "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
            "    INNER JOIN employes AS b ON a.id = b.id_user\n" +
            "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
            "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a GROUP BY id ORDER BY fullname",
            countQuery = "select count(id) as nomor from\n" +
                    "    (SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
                    "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
                    "    INNER JOIN employes AS b ON a.id = b.id_user\n" +
                    "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
                    "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a GROUP BY id ORDER BY fullname) as a", nativeQuery = true)
    Page<Object> semuaUser(Pageable pageable);

    Page<Employes> findByStatusAndCompaniesIdInOrderByFullName(StatusRecord statusRecord, List<String> idCompanies, Pageable pageable);

    @Query(value = "SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
            "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
            "    INNER JOIN employes AS b ON a.id = b.id_user\n" +
            "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
            "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a where fullname like %?1% GROUP BY id ORDER BY fullname",
            countQuery = "select count(id) as nomor from\n" +
                    "    (SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
                    "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
                    "    INNER JOIN employes AS b ON a.id = b.id_user\n" +
                    "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
                    "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a where fullname like %?1% GROUP BY id ORDER BY fullname) as a", nativeQuery = true)
    Page<Object> semuaUserSearch(String search, Pageable pageable);

    @Query(value = "SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
            "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
            "    INNER JOIN employes AS b ON a.id = b.id_user and b.id_company in (?1) \n" +
            "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
            "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a GROUP BY id ORDER BY fullname",
            countQuery = "select count(id) as nomor from\n" +
                    "    (SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
                    "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
                    "    INNER JOIN employes AS b ON a.id = b.id_user and b.id_company in (?1)\n" +
                    "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
                    "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a GROUP BY id ORDER BY fullname) as a", nativeQuery = true)
    Page<Object> semuaUserCompany(List<String> idCompanies, Pageable pageable);

    @Query(value = "SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
            "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
            "    INNER JOIN employes AS b ON a.id = b.id_user and b.id_company in (?1) \n" +
            "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
            "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a where fullname like %?1%  GROUP BY id ORDER BY fullname",
            countQuery = "select count(id) as nomor from\n" +
                    "    (SELECT id,fullname,username,active,GROUP_CONCAT(name SEPARATOR '\\n') AS role\n" +
                    "FROM (SELECT a.id,full_name AS fullname,a.username,a.active,d.name FROM s_user AS a\n" +
                    "    INNER JOIN employes AS b ON a.id = b.id_user and b.id_company in (?1)\n" +
                    "    LEFT JOIN s_user_role AS c ON a.id = c.id_user\n" +
                    "    LEFT JOIN s_role AS d ON c.id_role = d.id) AS a where fullname like %?1%  GROUP BY id ORDER BY fullname) as a", nativeQuery = true)
    Page<Object> semuaUserCompanySearch(List<String> idCompanies, String search, Pageable pageable);

    List<Employes> findByStatusOrderByFullName(StatusRecord statusRecord);



    Page<Employes> findByStatusAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord statusRecord, String search, Pageable pageable);

    Page<Employes> findByStatusAndCompaniesIdInAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord statusRecord, List<String> idCompanies, String search, Pageable pageable);

    Integer countByStatusAndStatusAktif(StatusRecord statusRecord, String status);

    Integer countByStatusAndStatusAktifAndCompaniesIdIn(StatusRecord statusRecord, String status, List<String> idCompanies);


    Integer countByStatusAndStatusAktifAndGenderAndCompaniesIdIn(StatusRecord statusRecord, String status, String gender, List<String> idCompanies);

    Integer countByStatusAndStatusAktifAndGender(StatusRecord statusRecord, String status, String gender);

    List<Employes> findByStatusAndCompaniesOrderByNumber(StatusRecord statusRecord, Companies companies);

    Employes findByStatusAndNumber(StatusRecord statusRecord, String number);

    Employes findByStatusAndFullName(StatusRecord statusRecord, String fullname);


    @Query(value = "select id from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and job_status not in ('HB','LB') order by number", nativeQuery = true)
    List<String> allEmployes2();

    @Query(value = "select id,max(tanggalMulai) as tanggalMulai,min(tanggalSelesai)as tanggalSelesai from\n" +
            "(select id,?1 as tanggalMulai,?2 as tanggalSelesai from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and job_status not in ('HB','LB') \n" +
            "union\n" +
            "select a.id,?1 as tanggalMulai,b.tanggal as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' \n" +
            "and a.status_aktif <> 'AKTIF' and b.status_aktif <> 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee\n" +
            "union\n" +
            "select a.id,b.tanggal as tanggalMulai,?2 as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' \n" +
            "and a.status_aktif = 'AKTIF' and b.status_aktif = 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee) as a\n" +
            "group by id;", nativeQuery = true)
    List<EmployeeAllDto> allEmployes(LocalDate tanggalMulai, LocalDate tanggalSelesai);


    @Query(value = "select id,max(tanggalMulai) as tanggalMulai,min(tanggalSelesai)as tanggalSelesai from\n" +
            "(select id,?1 as tanggalMulai,?2 as tanggalSelesai from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and id = ?3 and job_status not in ('HB','LB') \n" +
            "union\n" +
            "select a.id,?1 as tanggalMulai,b.tanggal as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' \n" +
            "and a.status_aktif <> 'AKTIF' and b.status_aktif <> 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee\n" +
            "union\n" +
            "select a.id,b.tanggal as tanggalMulai,?2 as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' \n" +
            "and a.status_aktif = 'AKTIF' and b.status_aktif = 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 and b.id = ?3 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee) as a\n" +
            "group by id;", nativeQuery = true)
    EmployeeAllDto employeeSatu(LocalDate tanggalMulai, LocalDate tanggalSelesai, String idEmployee);

    @Query(value = "select id from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and job_status not in ('LB','HB') and id_company = ?1 order by number", nativeQuery = true)
    List<String> companyEmployes2(String idCompanies);

    @Query(value = "select id,max(tanggalMulai) as tanggalMulai,min(tanggalSelesai)as tanggalSelesai from\n" +
            "(select id,?1 as tanggalMulai,?2 as tanggalSelesai from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and job_status not in ('HB','LB') and id_company = ?3 \n" +
            "union\n" +
            "select a.id,?1 as tanggalMulai,b.tanggal as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' and a.id_company = ?3 \n" +
            "and a.status_aktif <> 'AKTIF' and b.status_aktif <> 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee\n" +
            "union\n" +
            "select a.id,b.tanggal as tanggalMulai,?2 as tanggalSelesai from\n" +
            "(select a.id,max(b.date_update) as terakhir from employes as a\n" +
            "inner join employee_status_aktif as b on b.id_employee = a.id and a.status = 'AKTIF' and b.status = 'AKTIF' and a.id_company = ?3 \n" +
            "and a.status_aktif = 'AKTIF' and b.status_aktif = 'AKTIF' and b.tanggal >= ?1 and b.tanggal <= ?2 group by a.id) as a\n" +
            "inner join employee_status_aktif as b on a.id = b.id_employee and a.terakhir = b.date_update group by b.id_employee) as a\n" +
            "group by id;", nativeQuery = true)
    List<EmployeeAllDto> companyEmployes(LocalDate tanggalMulai, LocalDate tanggalSelesai, String idCompanies);



    @Query(value = "select a.id as id, number, full_name as fullName, status_aktif as statusAktif, status_employee as statusEmployee, b.ptkp_status as ptkpStatus from employes as a\n" +
            "left join employee_payroll_info as b on a.id = b.id_employee order by number", nativeQuery = true)
    List<ExportEmployeeStatusDto> listEmployeeExport2();

    @Query(value = "select a.id as id, number, full_name as fullName, a.status_aktif as statusAktif, coalesce(c.tanggal_string,'00/00/0000') as dateEmployeeStatusAktif, \n" +
            "a.status_employee as statusEmployee, coalesce(d.tanggal_string,'00/00/0000') as dateEmployeeStatus, b.ptkp_status as ptkpStatus \n" +
            "from employes as a\n" +
            "left join employee_payroll_info as b on a.id = b.id_employee \n" +
            "left join employee_status_aktif as c on a.id = c.id_employee\n" +
            "left join employee_status as d on a.id = d.id_employee\n" +
            "where a.status = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and coalesce(c.status,'AKTIF') = 'AKTIF' and \n" +
            "coalesce(d.status,'AKTIF') = 'AKTIF'\n" +
            "group by a.id order by fullName", nativeQuery = true)
    List<ExportEmployeeStatusDto> listEmployeeExport();

    @Query(value = "select a.id as id, number, full_name as fullName, status_aktif as statusAktif, status_employee as statusEmployee, b.ptkp_status as ptkpStatus from employes as a\n" +
            "left join employee_payroll_info as b on a.id = b.id_employee \n" +
            "where a.id_company = ?1 order by number", nativeQuery = true)
    List<ExportEmployeeStatusDto> listEmployeeExportWithCompany2(String idCompany);

    @Query(value = "select a.id as id, number, full_name as fullName, a.status_aktif as statusAktif, coalesce(c.tanggal_string,'00/00/0000') as dateEmployeeStatusAktif, \n" +
            "a.status_employee as statusEmployee, coalesce(d.tanggal_string,'00/00/0000') as dateEmployeeStatus, b.ptkp_status as ptkpStatus \n" +
            "from employes as a\n" +
            "left join employee_payroll_info as b on a.id = b.id_employee \n" +
            "left join employee_status_aktif as c on a.id = c.id_employee\n" +
            "left join employee_status as d on a.id = d.id_employee\n" +
            "where a.status = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and coalesce(c.status,'AKTIF') = 'AKTIF' and \n" +
            "coalesce(d.status,'AKTIF') = 'AKTIF' and a.id_company = ?1 group by a.id order by number", nativeQuery = true)
    List<ExportEmployeeStatusDto> listEmployeeExportWithCompany(String idCompany);


    @Query(value = "select number, full_name as nama from employes where status = 'AKTIF' and id_company = ?1 order by number", nativeQuery = true)
    List<EmployeeDto> listEmployee(String idCompany);

//    List<Employes> findByStatusOrderByFullName(StatusRecord statusRecord);


    @Query(value = "select id_absen as idAbsen from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and id_absen is not null and id_absen <> 0\n" +
            "and id_absen <> ''", nativeQuery = true)
    List<Integer> ambilIdAbsenEmployes();


    @Query(value = "SELECT a.id_company as companyId, b.company_name as companyName,sum(1) as total, sum(if(gender = 'PRIA',1,if(gender is null,1,0))) as male,\n" +
            "sum(if(gender = 'WANITA',1,0)) as female, sum(if(status_employee = 'TETAP',1,0)) as permanent, sum(if(status_employee <> 'TETAP', 1,0)) as temporary \n" +
            "FROM employes as a inner join companies as b on a.id_company = b.id where a.status = 'AKTIF' and status_aktif = 'AKTIF' group by a.id_company \n" +
            "order by b.company_name", nativeQuery = true)
    List<JumlahEmployeeDto2> tarikJumlahEmployee();


    @Query(value = "select id, full_name as fullName,sampai_tanggal as expiredDate,coalesce(statusEmployee,'BELUM DI SETTING') as statusEmployee, status_employee as jobStatus, if(sampai_tanggal < date(now()),'EXPIRED','ALMOST OVER') as description from\n" +
            "(select a.id, a.full_name,sampai_tanggal,c.status_employee,d.status_aktif as statusEmployee from employes as a\n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(tanggal) as max FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max = d.tanggal and d.status = 'AKTIF'\n" +
            "group by a.id) as a\n" +
            "where (status_employee <> 'TETAP' or status_employee is null) and (statusEmployee = 'AKTIF' or statusEmployee is null) AND sampai_tanggal <= DATE(NOW()) + INTERVAL 30 DAY",nativeQuery = true)
    List<EmployeeStatusDto> listExpiredKontrak();

    @Query(value = "select id, full_name as fullName,sampai_tanggal as expiredDate,coalesce(statusEmployee,'BELUM DI SETTING') as statusEmployee, status_employee as jobStatus, if(sampai_tanggal < date(now()),'EXPIRED','ALMOST OVER') as description from\n" +
            "(select a.id,id_company, a.full_name,sampai_tanggal,c.status_employee,d.status_aktif as statusEmployee from employes as a\n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(date_update) AS max_tanggal, MAX(dari_tanggal) AS max_tanggal_dari \n" +
            "\tFROM employee_status where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status ON a.id = latest_status.id_employee\n" +
            "LEFT JOIN employee_status AS c ON a.id = c.id_employee AND latest_status.max_tanggal_dari = c.dari_tanggal and max_tanggal = c.date_update \n" +
            "LEFT JOIN \n" +
            "    (SELECT id_employee,MAX(tanggal) as max FROM employee_status_aktif where status = 'AKTIF'\n" +
            "     GROUP BY id_employee) AS latest_status_aktif ON a.id = latest_status_aktif.id_employee \n" +
            "LEFT JOIN employee_status_aktif AS d ON a.id = d.id_employee AND latest_status_aktif.max = d.tanggal and d.status = 'AKTIF'\n" +
            "group by a.id) as a\n" +
            "where a.id_company in (?1) and (status_employee <> 'TETAP' or status_employee is null) and (statusEmployee = 'AKTIF' or statusEmployee is null) AND sampai_tanggal <= DATE(NOW()) + INTERVAL 30 DAY;\n",nativeQuery = true)
    List<EmployeeStatusDto> listExpiredKontrakCompanies(List<String> idCompanies);

}
