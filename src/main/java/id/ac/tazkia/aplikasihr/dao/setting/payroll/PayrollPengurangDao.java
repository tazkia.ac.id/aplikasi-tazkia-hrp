package id.ac.tazkia.aplikasihr.dao.setting.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPengurang;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollPengurangDao extends PagingAndSortingRepository<PayrollPengurang, String>, CrudRepository<PayrollPengurang, String> {

    Page<PayrollPengurang> findByStatusOrderByName(StatusRecord statusRecord, Pageable pageable);

    PayrollPengurang findByIdAndStatus(String id, StatusRecord statusRecord);

}
