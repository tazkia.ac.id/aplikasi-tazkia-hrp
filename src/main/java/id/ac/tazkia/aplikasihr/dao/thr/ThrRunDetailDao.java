package id.ac.tazkia.aplikasihr.dao.thr;

import id.ac.tazkia.aplikasihr.entity.thr.ThrRunDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ThrRunDetailDao extends PagingAndSortingRepository<ThrRunDetail, String>, CrudRepository<ThrRunDetail, String> {


}
