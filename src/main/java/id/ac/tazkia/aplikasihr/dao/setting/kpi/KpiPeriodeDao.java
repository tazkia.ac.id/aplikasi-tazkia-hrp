package id.ac.tazkia.aplikasihr.dao.setting.kpi;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.kpi.KpiPeriode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KpiPeriodeDao extends PagingAndSortingRepository<KpiPeriode, String>, CrudRepository<KpiPeriode, String> {

    Page<KpiPeriode> findByStatusOrderByKodePeriodeDesc(StatusRecord statusRecord, Pageable pageable);

    KpiPeriode findByStatusAndId(StatusRecord statusRecord, String id);

}
