package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JobPositionDao extends PagingAndSortingRepository<JobPosition, String>, CrudRepository<JobPosition, String> {



    Page<JobPosition> findByStatusOrderByPositionCode(StatusRecord statusRecord, Pageable pageable);

    Page<JobPosition> findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord statusRecord, List<String> idCompanies, Pageable pageable);

    List<JobPosition> findByStatusOrderByPositionCode(StatusRecord statusRecord);

    List<JobPosition> findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord statusRecord, List<String> idCompanies);

//    List<JobPosition> findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord statusRecord, List<String> idCompanies);


    List<JobPosition> findByStatusAndDepartmentIdInOrderByPositionCode(StatusRecord statusRecord, List<String> department);

    List<JobPosition> findByStatusAndIdNotContainingOrderByPositionCode(StatusRecord statusRecord, String idPosition);

    List<JobPosition> findByStatusAndIdNotOrderByPositionName(StatusRecord statusRecord, String id);

    List<JobPosition> findByStatusAndDepartmentCompaniesIdInAndIdNotOrderByPositionName(StatusRecord statusRecord,List<String> idCompanies, String id);


    Page<JobPosition> findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord statusRecord, String search1, StatusRecord statusRecord2, String search2, Pageable pageabe);

//    Page<JobPosition> findByStatusAndDepartmentCompaniesIdInAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord statusRecord,List<String> idCompanies,  String search1, StatusRecord statusRecord2, String search2, Pageable pageabe);

    Page<JobPosition> findByStatusAndDepartmentCompaniesIdInAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord statusRecord, List<String> idCompanies, String search1, StatusRecord statusRecord2, String search2, Pageable pageabe);

    JobPosition findByStatusAndId(StatusRecord statusRecord, String id);

    JobPosition findByStatusAndPositionCode(StatusRecord status, String positionCode);

}
