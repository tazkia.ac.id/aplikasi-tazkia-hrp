package id.ac.tazkia.aplikasihr.dao.setting;

import id.ac.tazkia.aplikasihr.entity.setting.EmployeeTasksSchedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface EmployeeTasksScheduleDao extends PagingAndSortingRepository<EmployeeTasksSchedule, String>, CrudRepository<EmployeeTasksSchedule, String> {

    @Query(value = "select sum(reward) as totalReward from employee_tasks_schedule as a\n" +
            "inner join employee_tasks as b on a.id_employee_tasks = b.id \n" +
            "where a.status = 'DONE' and b.id_employee = ?1 \n" +
            "and a.date_tasks >= ?2 and a.date_tasks <= ?3", nativeQuery = true)
    BigDecimal tarikTotalTunjanganKetazkiaan(String idEmployee, LocalDate mulai, LocalDate selesai);

}
