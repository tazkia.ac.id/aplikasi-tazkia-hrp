package id.ac.tazkia.aplikasihr.dao.payroll;

import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.TotalPayrollPerTahunDto;
import id.ac.tazkia.aplikasihr.dto.reports.listTotalPayrolEmployesDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface PayrollRunDao extends PagingAndSortingRepository<PayrollRun, String>, CrudRepository<PayrollRun, String> {

    PayrollRun findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord statusRecord, String tahun, String bulan, String idEmployee);

//    List<PayrollRun> findByStatusAndBulanAndTahunAndIdEmployeeCompanies(StatusRecord statusRecord, String bulan, String tahun, Companies companies);

    PayrollRun findByStatusAndId(StatusRecord statusRecord, String Id);

    @Query(value = "select b.id, b.number as nomor, b.full_name as  nama, a.bulan as  bulan, a.tahun as tahun, a.total_payroll as takeHomePay, a.id as idp from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "where a.bulan = ?1 and a.tahun = ?2 and b.id_company = ?3 \n" +
            "and a.status = 'AKTIF' and a.total_payroll > 0 order by b.number", nativeQuery = true)
    List<Object> listPayrollRun(String bulan, String tahun, String idCompanies);


    @Query(value = "select b.id, b.number as nomor, b.full_name as  nama, a.bulan as  bulan, a.tahun as tahun, a.total_payroll as takeHomePay, a.id as idp from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "where a.bulan = ?1 and a.tahun = ?2 and b.id_company = ?3 \n" +
            "and a.status = 'AKTIF' and b.id_company in (?4) order by b.number", nativeQuery = true)
    List<Object> listPayrollRunCompany(String bulan, String tahun, String idCompanies, List<String> idCompany);

    @Query(value = "select a.id as idp from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "where a.bulan = ?1 and a.tahun = ?2 and b.id_company = ?3 \n" +
            "and a.status = 'AKTIF' order by b.number", nativeQuery = true)
    List<String> listPayrollRunId(String bulan, String tahun, String idCompanies);


    @Query(value = "select b.number, b.full_name as fullName,a.gaji_pokok as gajiPokok, a.total_allowance as allowance,\n" +
            "(a.gaji_pokok + a.total_allowance) as totalGaji,total_kena_pajak as kenaPajak, \n" +
            "a.total_benefits as benefits,a.total_loan as loan, total_potongan_absen as potonganAbsen, total_potongan_terlambat as potonganTerlambat,\n" +
            "total_zakat as zakat, total_infaq as infaq, total_pajak as pajak,a.total_deductions as deduction, total_payroll as takeHomePay from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id \n" +
            "where a.bulan = ?1 and a.tahun = ?2 and total_payroll > 0 order by number", nativeQuery = true)
    List<DetailPayrollReportDto> listPayrollDetailAll(String bulan, String tahun);

    @Query(value = "select b.number, b.full_name as fullName,a.gaji_pokok as gajiPokok, a.total_allowance as allowance,\n" +
            "(a.gaji_pokok + a.total_allowance) as totalGaji,total_kena_pajak as kenaPajak, \n" +
            "a.total_benefits as benefits,a.total_loan as loan, total_potongan_absen as potonganAbsen, total_potongan_terlambat as potonganTerlambat,\n" +
            "total_zakat as zakat, total_infaq as infaq, total_pajak as pajak,a.total_deductions as deduction, total_payroll as takeHomePay from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id \n" +
            "where a.bulan = ?1 and a.tahun = ?2 and b.id_company = ?3 and total_payroll > 0 order by number", nativeQuery = true)
    List<DetailPayrollReportDto> listPayrollDetailCompanies(String bulan, String tahun, String idCompany);

    @Query(value = "select (sum(gaji_pokok)+sum(total_allowance)+sum(total_benefits))-(sum(total_potongan_absen)+sum(total_potongan_terlambat)) as total_payroll from payroll_run where tahun = year(now()) and status = 'AKTIF'", nativeQuery = true)
    BigDecimal totalSalaryPerTahun();


    @Query(value = "select (sum(gaji_pokok)+sum(total_allowance)+sum(total_benefits))-(sum(total_potongan_absen)+sum(total_potongan_terlambat)) as total_payroll from payroll_run as a inner join employes as b on a.id_employee = b.id where b.id_company in (?1) and tahun = year(now()) and a.status = 'AKTIF'", nativeQuery = true)
    BigDecimal totalSalaryPerTahunCompany(List<String> idCompanies);

    @Query(value = "select id_employee as idEmployee, b.number as number,b.full_name as fullName, c.company_name as companyName,sum(a.gaji_pokok)as totalGajiPokok,\n" +
            "sum(a.total_allowance) as totalAllowance,sum(total_deductions)as totalDeductions,sum(total_benefits) as totalBenefits,sum(total_zakat) as totalZakat,\n" +
            "sum(total_infaq) as totalInfaq,sum(total_pajak) as totalPajak,sum(total_payroll) as takeHomePay, sum(total_potongan_absen) as potonganAbsen, sum(total_potongan_terlambat) as potonganTerlambat, sum(total_loan) as totalLoan \n" +
            "from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join companies as c on b.id_company = c.id\n" +
            "where a.tahun = year(now()) and a.status = 'AKTIF'\n" +
            "group by a.id_employee \n" +
            "order by c.company_name, b.full_name", nativeQuery = true)
    List<listTotalPayrolEmployesDto> listTotalPayrollEmployes();


    @Query(value = "select id_employee as idEmployee, b.number as number,b.full_name as fullName, c.company_name as companyName,sum(a.gaji_pokok)as totalGajiPokok,\n" +
            "sum(a.total_allowance) as totalAllowance,sum(total_deductions)as totalDeductions,sum(total_benefits) as totalBenefits,sum(total_zakat) as totalZakat,\n" +
            "sum(total_infaq) as totalInfaq,sum(total_pajak) as totalPajak,sum(total_payroll) as takeHomePay, sum(total_potongan_absen) as potonganAbsen, sum(total_potongan_terlambat) as potonganTerlambat, sum(total_loan) as totalLoan \n" +
            "from payroll_run as a\n" +
            "inner join employes as b on a.id_employee = b.id\n" +
            "inner join companies as c on b.id_company = c.id\n" +
            "where a.tahun = year(now()) and b.id_company in (?1) and a.status = 'AKTIF'\n" +
            "group by a.id_employee \n" +
            "order by c.company_name, b.full_name", nativeQuery = true)
    List<listTotalPayrolEmployesDto> listTotalPayrollEmployesCompanies(List<String> idCompanies);


    @Query(value = "select concat(a.tahun,'-',a.bulan) as bulan,(a.penambah - coalesce(b.pengurang,0)) as totalPayroll from\n" +
            "(select bulan, tahun, sum(nominal) as penambah from payroll_run_detail where status = 'AKTIF' and DATE_FORMAT(STR_TO_DATE(concat(tahun,'-',bulan),'%Y-%m'),'%Y-%m') >= DATE_FORMAT(DATE_SUB(now(), INTERVAL 5 month), '%Y-%m') and DATE_FORMAT(STR_TO_DATE(concat(tahun,'-',bulan),'%Y-%m'),'%Y-%m') <= DATE_FORMAT(now(), '%Y-%m') and jenis_payroll in ('BASIC','ALLOWANCE') group by bulan, tahun) as a\n" +
            "left join\n" +
            "(select bulan, tahun, sum(nominal) as pengurang from payroll_run_detail where status = 'AKTIF' and DATE_FORMAT(STR_TO_DATE(concat(tahun,'-',bulan),'%Y-%m'),'%Y-%m')  >= DATE_FORMAT(DATE_SUB(now(), INTERVAL 5 month), '%Y-%m') and DATE_FORMAT(STR_TO_DATE(concat(tahun,'-',bulan),'%Y-%m'),'%Y-%m') <= DATE_FORMAT(now(), '%Y-%m') and deskripsi in ('Potongan Absen','Potongan Terlambat') group by bulan, tahun) as b\n" +
            "on a.bulan = b.bulan and a.tahun = b.tahun order by bulan desc", nativeQuery = true)
    List<TotalPayrollPerTahunDto> dashboardTotalPayroll();




}
