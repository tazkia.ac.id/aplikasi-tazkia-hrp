package id.ac.tazkia.aplikasihr.dao.thr;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.thr.ThrSetting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ThrSettingDao extends PagingAndSortingRepository<ThrSetting, String>, CrudRepository<ThrSetting, String> {

    List<ThrSetting> findByStatusOrderByDateActiveDesc(StatusRecord statusRecord);

}
