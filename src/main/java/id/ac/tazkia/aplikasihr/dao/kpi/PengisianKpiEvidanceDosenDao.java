package id.ac.tazkia.aplikasihr.dao.kpi;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidanceDosen;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengisianKpiEvidanceDosenDao extends PagingAndSortingRepository<PengisianKpiEvidanceDosen, String>, CrudRepository<PengisianKpiEvidanceDosen, String> {

    List<PengisianKpiEvidanceDosen> findByPengisianKpiDosenAndStatus(PengisianKpiDosen kpi, StatusRecord record);
}
