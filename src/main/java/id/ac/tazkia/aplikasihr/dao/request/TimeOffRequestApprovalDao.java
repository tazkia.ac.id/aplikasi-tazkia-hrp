package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequest;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequestApproval;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TimeOffRequestApprovalDao extends PagingAndSortingRepository<TimeOffRequestApproval, String>, CrudRepository<TimeOffRequestApproval, String> {

    List<TimeOffRequestApproval> findByStatusAndTimeOffRequest(StatusRecord statusRecord, TimeOffRequest timeOffRequest);

}
