package id.ac.tazkia.aplikasihr.dao.payroll;

import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollReportsFullDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HeaderPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.IsiDetailEmployeePayrollRunDto;
import id.ac.tazkia.aplikasihr.dto.payroll.IsiPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.thymeleaf.dialect.springdata.decorator.PaginationDecorator;

import java.math.BigDecimal;
import java.util.List;

public interface PayrollRunDetailDao extends PagingAndSortingRepository<PayrollRunDetail, String>, CrudRepository<PayrollRunDetail, String> {

    PayrollRunDetail findByStatusAndEmployesAndBulanAndTahunAndJenisPayrollAndDeskripsi(StatusRecord statusRecord, Employes employes, String bulan, String tahun, String jenisPayroll, String deskripsi);

    @Query(value = "delete from payroll_run_detail where status = 'AKTIF' and id_employee = ?1 and bulan = ?2 and tahun = ?3", nativeQuery = true)
    PayrollRunDetail deletePayrollRunDetail(String idEmployee, String bulan, String tahun);

    List<PayrollRunDetail> findByStatusAndEmployesAndBulanAndTahun(StatusRecord statusRecord, Employes employes, String bulan, String tahun);

    List<PayrollRunDetail> findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord statusRecord, Employes employes, String bulan, String tahun, BigDecimal nominal, String jenis);


    @Query(value = "select nomor, jenis, nama from\n" +
            "((select '01' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '02' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'AllOWANCE' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '03' as nomor,'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama) \n" +
            "union\n" +
            "(select '04' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '05' as nomor,'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama) \n" +
            "union\n" +
            "(select '06' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '07' as nomor,'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama) \n" +
            "union\n" +
            "(select '08' as nomor,'LOAN' as jenis,c.name as nama from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id where a.tahun = ?1 and bulan = ?2 group by c.name order by c.name asc)\n" +
            "union\n" +
            "(select '09' as nomor,'TOTAL_LOAN' as jenis, 'Total Loan' as nama)\n" +
            "union\n" +
            "(select '10' as nomor,'THP' as jenis, 'Take Home Pay' as nama)) as a\n" +
            "order by nomor,nama", nativeQuery = true)
    List<Object> headerLaporanPayroll(String tahun, String bulan, String companies);


    @Query(value = "select nomor, jenis, nama from\n" +
            "((select '01' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '02' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'AllOWANCE' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '03' as nomor,'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama) \n" +
            "union\n" +
            "(select '04' as nomor,'TOTAL_GROSS' as jenis, 'Total Gross' as nama) \n" +
            "union\n" +
            "(select '05' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '06' as nomor,'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama) \n" +
            "union\n" +
            "(select '07' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '08' as nomor,'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama) \n" +
            "union\n" +
            "(select '09' as nomor,'LOAN' as jenis,c.name as nama from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id where a.tahun = ?1 and bulan = ?2 group by c.name order by c.name asc)\n" +
            "union\n" +
            "(select '10' as nomor,'TOTAL_LOAN' as jenis, 'Total Loan' as nama)\n" +
            "union\n" +
            "(select '11' as nomor,'THP' as jenis, 'Take Home Pay' as nama)) as a\n" +
            "order by nomor,nama", nativeQuery = true)
    List<Object> headerLaporanPayrolls(String tahun, String bulan, String companies);


    @Query(value = "select nomor, jenis, nama from\n" +
            "((select '01' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company in ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '02' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company in ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'AllOWANCE' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '03' as nomor,'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama) \n" +
            "union\n" +
            "(select '04' as nomor,'TOTAL_GROSS' as jenis, 'Total Gross' as nama) \n" +
            "union\n" +
            "(select '05' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company in ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '06' as nomor,'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama) \n" +
            "union\n" +
            "(select '07' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company in ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '08' as nomor,'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama) \n" +
            "union\n" +
            "(select '09' as nomor,'LOAN' as jenis,c.name as nama from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id where a.tahun = ?1 and bulan = ?2 group by c.name order by c.name asc)\n" +
            "union\n" +
            "(select '10' as nomor,'TOTAL_LOAN' as jenis, 'Total Loan' as nama)\n" +
            "union\n" +
            "(select '11' as nomor,'THP' as jenis, 'Take Home Pay' as nama)) as a\n" +
            "order by nomor,nama", nativeQuery = true)
    List<Object> headerLaporanPayrolls2(String tahun, String bulan, List<String> companies);

    @Query(value = "select nomor, jenis, nama from\n" +
            "((select '01' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '02' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'AllOWANCE' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '03' as nomor,'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama) \n" +
            "union\n" +
            "(select '04' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '05' as nomor,'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama) \n" +
            "union\n" +
            "(select '06' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and  nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '07' as nomor,'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama) \n" +
            "union\n" +
            "(select '08' as nomor,'LOAN' as jenis,c.name as nama from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id where a.tahun = ?1 and bulan = ?2 group by c.name order by c.name asc)\n" +
            "union\n" +
            "(select '09' as nomor,'TOTAL_LOAN' as jenis, 'Total Loan' as nama)\n" +
            "union\n" +
            "(select '10' as nomor,'THP' as jenis, 'Take Home Pay' as nama)) as a\n" +
            "order by nomor,nama", nativeQuery = true)
    List<HeaderPayrollRunReportDto> headerLaporanPayroll1(String tahun, String bulan, String companies);


    @Query(value = "select * from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and  nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '03' as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select '04' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '05' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select '06' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '07' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '08' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, GROUP_CONCAT(payment SEPARATOR ' + ') AS nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by c.name,id_employee )\n" +
            "union\n" +
            "(select '09' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '10' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where a.status = 'AKTIF' and tahun = ?1 and bulan =?2)a\n" +
            "order by kode,nomor,nama", nativeQuery = true)
    List<Object> listLaporanPayroll(String tahun, String bulan, String companies);

    @Query(value = "select * from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and  nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '03' as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select  '04' as nomor, 'TOTAL_GROSS' as jenis, 'Total Gross' as nama,a.id_employee, a.kode, coalesce(a.nominal,0)+coalesce(b.nominal,0) as nominal from\n" +
            "(select  id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee) as a\n" +
            "left join\n" +
            "(select id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee) as b\n" +
            "on a.id_employee = b.id_employee\n" +
            "union\n" +
            "select '05' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '06' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select ?2 as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '08' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '09' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, GROUP_CONCAT(payment SEPARATOR ' + ') AS nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by c.name,id_employee )\n" +
            "union\n" +
            "(select '10' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '11' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where a.status = 'AKTIF' and tahun = ?1 and bulan =?2)a\n" +
            "order by kode,nomor,nama", nativeQuery = true)
    List<Object> listLaporanPayrolls(String tahun, String bulan, String companies);

    @Query(value = "select * from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and  nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '03' as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select  '04' as nomor, 'TOTAL_GROSS' as jenis, 'Total Gross' as nama,a.id_employee, a.kode, coalesce(a.nominal,0)+coalesce(b.nominal,0) as nominal from\n" +
            "(select  id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee) as a\n" +
            "left join\n" +
            "(select id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee) as b\n" +
            "on a.id_employee = b.id_employee\n" +
            "union\n" +
            "select '05' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '06' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select ?2 as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '08' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company in ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '09' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, GROUP_CONCAT(payment SEPARATOR ' + ') AS nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by c.name,id_employee )\n" +
            "union\n" +
            "(select '10' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '11' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where a.status = 'AKTIF' and tahun = ?1 and bulan =?2)a\n" +
            "order by kode,nomor,nama", nativeQuery = true)
    List<Object> listLaporanPayrolls22(String tahun, String bulan, List<String> companies);


    @Query(value = "select nomor,jenis,nama, id_employee as idEmployee, kode,nominal from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and  nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '03' as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select '04' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '05' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select '06' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '07' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_company = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '08' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, sum(payment) AS nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by c.name,id_employee )\n" +
            "union\n" +
            "(select '09' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '10' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where a.status = 'AKTIF' and tahun = ?1 and bulan =?2)a\n" +
            "order by kode,nomor,nama", nativeQuery = true)
    List<IsiPayrollRunReportDto> listLaporanPayroll1(String tahun, String bulan, String companies);


    @Query(value = "select nomor,jenis,nama, id_employee as idEmployee, kode,nominal from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and  nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select ?2 as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select '04' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '05' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select '06' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '07' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and nominal > 0 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '08' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, payment as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where id_employee = ?3 and a.tahun = ?1 and bulan = ?2  group by c.name,id_employee )\n" +
            "union\n" +
            "(select '09' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where id_employee = ?3 and a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '10' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where id_employee = ?3 and a.status = 'AKTIF' and tahun = ?1 and bulan =?2)a\n" +
            "order by idEmployee,nomor,nama", nativeQuery = true)
    List<IsiPayrollRunReportDto> listLaporanPayroll1Employee(String tahun, String bulan, String employes);

    @Query(value = "select b.id, b.number, b.full_name from payroll_run as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and a.total_payroll > 0 and a.tahun = ?1 and a.bulan = ?2 order by b.number", nativeQuery = true)
    List<Object> listEmployeePayroll(String tahun, String bulan, String companies);

    @Query(value = "select b.id, b.number, b.full_name from payroll_run as a inner join employes as b on a.id_employee = b.id where b.id_company in ?3 and a.status = 'AKTIF' and a.tahun = ?1 and a.bulan = ?2 order by b.number", nativeQuery = true)
    List<Object> listEmployeePayrolls2(String tahun, String bulan, List<String> companies);

    @Query(value = "select b.id, b.number, b.full_name from payroll_run as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and a.total_payroll > 0 and a.tahun = ?1 and a.bulan = ?2 order by b.number", nativeQuery = true)
    List<Object> listEmployeePayrolls(String tahun, String bulan, String companies);

    @Query(value = "select b.id, b.number, b.full_name as fullName from payroll_run as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and a.tahun = ?1 and a.bulan = ?2 order by b.id", nativeQuery = true)
    List<EmployeePayrollReportsFullDto> listEmployeePayroll1(String tahun, String bulan, String companies);


    @Query(value = "select a.nomor,a.jenis, a.nama, coalesce(nominal,0)as nominal from\n" +
            "(select jenis, nama from\n" +
            "((select '01' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and nominal > 0 and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '02' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'AllOWANCE' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '03' as nomor,'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama) \n" +
            "union\n" +
            "(select '04' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '05' as nomor,'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama) \n" +
            "union\n" +
            "(select '06' as nomor,jenis_payroll as jenis, deskripsi as nama from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where b.id_company = ?3 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' and deskripsi <> 'Loan' group by jenis_payroll,deskripsi order by deskripsi asc)\n" +
            "union\n" +
            "(select '07' as nomor,'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama) \n" +
            "union\n" +
            "(select '08' as nomor,'LOAN' as jenis,c.name as nama from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id inner join employes as d on b.id_employee = d.id where d.id_company = ?3 and a.tahun = ?1 and bulan = ?2 group by c.name order by c.name asc)\n" +
            "union\n" +
            "(select '09' as nomor,'TOTAL_LOAN' as jenis, 'Total Loan' as nama)\n" +
            "union\n" +
            "(select '10' as nomor,'THP' as jenis, 'Take Home Pay' as nama)) as a\n" +
            "order by nomor,nama) as a\n" +
            "left join\n" +
            "(select * from\n" +
            "(select '01' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BASIC' group by id_employee\n" +
            "union\n" +
            "select '02' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '03' as nomor, 'TOTAL_ALLOWANCE' as jenis, 'Total Allowance' as nama,id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'ALLOWANCE' group by id_employee\n" +
            "union\n" +
            "select '04' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '05' as nomor, 'TOTAL_DEDUCTIONS' as jenis, 'Total Deductions' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'DEDUCTION' group by id_employee\n" +
            "union\n" +
            "select '06' as nomor, jenis_payroll as jenis, deskripsi as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee,deskripsi\n" +
            "union\n" +
            "select '07' as nomor, 'TOTAL_BENEFITS' as jenis, 'Total Benefits' as nama, id_employee, b.number as kode, sum(nominal) as nominal from payroll_run_detail as a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2 and jenis_payroll = 'BENEFIT' group by id_employee\n" +
            "union\n" +
            "(select '08' as nomor, 'LOAN' as jenis,c.name as nama, id_employee, d.number as kode, payment as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where id_employee = ?4 and a.tahun = ?1 and bulan = ?2 group by c.name,id_employee )\n" +
            "union\n" +
            "(select '09' as nomor, 'TOTAL_LOAN' as jenis,'Total Loan' as nama, id_employee, d.number as kode, sum(payment) as nominal from employee_loan_bayar as a \n" +
            "inner join employee_loan as b on a.id_employee_loan=b.id\n" +
            "inner join loan as c on b.id_loan = c.id\n" +
            "inner join employes as d on b.id_employee = d.id where id_employee = ?4 and a.tahun = ?1 and bulan = ?2 group by id_employee )\n" +
            "union\n" +
            "select '10' as nomor, 'THP' as jenis, 'Take Home Pay' as nama,id_employee, b.number as kode, total_payroll as nominal from payroll_run a inner join employes as b on a.id_employee = b.id where id_employee = ?4 and a.status = 'AKTIF' and tahun = ?1 and bulan = ?2)a\n" +
            "order by kode,nomor,nama) b\n" +
            "on a.nama = b.nama order by nomor", nativeQuery = true)
    List<IsiDetailEmployeePayrollRunDto> listPayrollRunDetailEmployee(String tahun, String bulan, String company, String employee);

}
