package id.ac.tazkia.aplikasihr.dao.attendance;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasihr.dto.attendance.RunAttendanceDto;
import id.ac.tazkia.aplikasihr.dto.my.MyAttendanceDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.Attendance;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;

public interface AttendanceDao extends PagingAndSortingRepository <Attendance, String>, CrudRepository<Attendance,String> {

    Attendance findByStatusAndEmployesAndDateAndWaktuMasukAndWaktuKeluar(StatusRecord statusRecord, Employes employes, LocalDate date, LocalTime in, LocalTime out);

    Attendance findByStatusAndEmployesAndDateAndWaktuMasuk(StatusRecord statusRecord, Employes employes, LocalDate date, LocalTime in);

    Attendance findByStatusAndEmployesAndDateAndWaktuKeluar(StatusRecord statusRecord, Employes employes, LocalDate date, LocalTime out);

    Attendance findByStatusAndEmployesAndDate(StatusRecord statusRecord, Employes employes, LocalDate tanggal);

    List<Attendance> findByStatusAndEmployesAndDateOrderById(StatusRecord statusRecord, Employes employes, LocalDate tanggal);

    @Query(value = "select id, tanggal, hari, shiftName, shiftIn, shiftOut, workDayStatus, min(waktuMasuk) as waktuMasuk, max(waktuKeluar) as waktuKeluar, stat from\n" +
            "(select id, a.tanggal as tanggal, hari, coalesce(schedule_name,shift_name) as shiftName, shift_in as shiftIn, shift_out as shiftOut,work_day_status as workDayStatus, coalesce(d.time_off_name,e.jam_masuk,waktu_masuk)as waktuMasuk, coalesce(d.ket_timeoff,e.jam_keluar,waktu_keluar)as waktuKeluar, coalesce(d.time_off_name,c.keterangan,work_day_status) as stat from\n" +
            "(select aa.id,date(aa.tanggal) as tanggal, aa.hari, aa.schedule_name, aa.shift_name, aa.shift_in as shift_in, aa.shift_out as shift_out, aa.work_day_status from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, schedule_name, shift_name, shift_in, shift_out,efective_date,work_day_status FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, b.schedule_name, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal)a\n" +
            "left join\n" +
            "(select id as id_attendance, id_employee, date, waktu_masuk, waktu_keluar from attendance where id_employee = ?3 and status = 'AKTIF' and date BETWEEN ?1 AND  ?2)b\n" +
            "on a.tanggal = b.date \n" +
            "left join\n" +
            "(select date_day_off, keterangan from day_off where status = 'AKTIF' and date_day_off BETWEEN ?1 AND ?2)c\n" +
            "on a.tanggal = c.date_day_off\n" +
            "left join\n" +
            "(select a.id as id_time_off, a.tanggal_time_off, a.tanggal_time_off_sampai, b.time_off_name, a.keterangan as ket_timeoff from time_off_request as a\n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' and a.id_employee = ?3 and (\n" +
            "(tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or\n" +
            "            (tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai >= ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai >= ?2)))d \n" +
            "on a.tanggal between tanggal_time_off and tanggal_time_off_sampai \n" +
            "left join\n" +
            "(select tanggal,keterangan,'REQ' as status_attendance, if(hour(jam_masuk + INTERVAL 7 HOUR) >= 24,(jam_masuk + INTERVAL 7 HOUR)- INTERVAL 24 HOUR,jam_masuk + INTERVAL 7 HOUR) as jam_masuk, if(hour(jam_keluar + INTERVAL 7 HOUR) >= 24,(jam_keluar + INTERVAL 7 HOUR)- INTERVAL 24 HOUR,jam_keluar + INTERVAL 7 HOUR) as jam_keluar from attendance_request where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?3 and tanggal BETWEEN ?1 AND ?2)e\n" +
            "on a.tanggal = e.tanggal\n" +
            "order by a.tanggal)a group by tanggal order by tanggal", nativeQuery = true)
    List<MyAttendanceDto> listViewAttendance(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);

    @Query(value = "select id, a.tanggal as tanggal, hari, coalesce(schedule_name,shift_name) as shiftName, shift_in as shiftIn, shift_out as shiftOut,work_day_status as workDayStatus, coalesce(e.jam_masuk,waktu_masuk)as waktuMasuk, coalesce(e.jam_keluar,waktu_keluar)as waktuKeluar, coalesce(d.statusTimeOff,c.keterangan,work_day_status) as stat,flexible,attd from\n" +
            "(select aa.id,date(aa.tanggal) as tanggal, aa.hari, aa.schedule_name, aa.shift_name, aa.shift_in as shift_in, aa.shift_out as shift_out, aa.work_day_status,flexible,attd from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, schedule_name, shift_name, shift_in, shift_out,efective_date,work_day_status,flexible,attd FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day, b.schedule_name, d.*, (a.date_update)as dates, flexible, attendance as attd  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name, d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal)a\n" +
            "left join\n" +
            "(select id as id_attendance, id_employee, date, concat(date,' ',waktu_masuk) as waktu_masuk,concat(date,' ',waktu_keluar) as waktu_keluar from attendance where id_employee = ?3 and status = 'AKTIF' and date BETWEEN ?1 AND  ?2)b\n" +
            "on a.tanggal = b.date \n" +
            "left join\n" +
            "(select date_day_off, keterangan, 'CUTI' as statusTimeOff from day_off where status = 'AKTIF' and date_day_off BETWEEN ?1 AND ?2)c\n" +
            "on a.tanggal = c.date_day_off\n" +
            "left join\n" +
            "(select a.id as id_time_off, a.tanggal_time_off, a.tanggal_time_off_sampai, b.time_off_name, a.keterangan as ket_timeoff, 'CUTI' as statusTimeOff from time_off_request as a\n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' and a.id_employee = ?3 and (tanggal_time_off BETWEEN ?1 AND ?2 or tanggal_time_off_sampai BETWEEN ?1 AND ?2))d\n" +
            "on a.tanggal between tanggal_time_off and tanggal_time_off_sampai \n" +
            "left join\n" +
            "(select tanggal,keterangan,'REQ' as status_attendance, addtime(concat(tanggal,' ',jam_masuk),'07:00:00') as jam_masuk, addtime(concat(tanggal,' ',jam_keluar),'07:00:00')as jam_keluar from attendance_request where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?3 and tanggal BETWEEN ?1 AND ?2)e\n" +
            "on a.tanggal = e.tanggal\n" +
            "order by a.tanggal", nativeQuery = true)
    List<RunAttendanceDto> listRunAttendance1(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);


    @Query(value = "select id, tanggal, hari, shiftName, shiftIn, shiftOut, workDayStatus, min(waktuMasuk) as waktuMasuk, max(waktuKeluar) as waktuKeluar, stat, flexible, attd from\n" +
            "(select id, a.tanggal as tanggal, hari, coalesce(schedule_name,shift_name) as shiftName, shift_in as shiftIn, shift_out as shiftOut,work_day_status as workDayStatus, coalesce(e.jam_masuk,waktu_masuk)as waktuMasuk, coalesce(e.jam_keluar,waktu_keluar)as waktuKeluar, coalesce(d.statusTimeOff,c.keterangan,work_day_status) as stat,flexible,attd from\n" +
            "(select aa.id,date(aa.tanggal) as tanggal, aa.hari, aa.schedule_name, aa.shift_name, aa.shift_in as shift_in, aa.shift_out as shift_out, aa.work_day_status,flexible,attd from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, schedule_name, shift_name, shift_in, shift_out,efective_date,work_day_status,flexible,attd FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name, d.*, (a.date_update)as dates, flexible, attendance as attd  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name , d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal)a\n" +
            "left join\n" +
            "(select id as id_attendance, id_employee, date, concat(date,' ',waktu_masuk) + INTERVAL 7 HOUR as waktu_masuk,concat(date,' ',waktu_keluar) + INTERVAL 7 HOUR as waktu_keluar from attendance where id_employee = ?3 and status = 'AKTIF' and date BETWEEN ?1 AND  ?2)b\n" +
            "on a.tanggal = b.date \n" +
            "left join\n" +
            "(select date_day_off, keterangan, 'CUTI' as statusTimeOff from day_off where status = 'AKTIF' and date_day_off BETWEEN ?1 AND ?2)c\n" +
            "on a.tanggal = c.date_day_off\n" +
            "left join\n" +
            "(select a.id as id_time_off, a.tanggal_time_off, a.tanggal_time_off_sampai, b.time_off_name, a.keterangan as ket_timeoff, 'CUTI' as statusTimeOff from time_off_request as a\n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' and a.id_employee = ?3 and ((tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or\n" +
            "            (tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai >= ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai >= ?2)))d \n" +
            "on a.tanggal between tanggal_time_off and tanggal_time_off_sampai \n" +
            "left join\n" +
            "(select tanggal,keterangan,'REQ' as status_attendance, addtime(concat(tanggal,' ',jam_masuk),'07:00:00') as jam_masuk, addtime(concat(tanggal,' ',jam_keluar),'07:00:00')as jam_keluar from attendance_request where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?3 and tanggal BETWEEN ?1 AND ?2)e\n" +
            "on a.tanggal = e.tanggal\n" +
            "order by a.tanggal)a group by tanggal order by tanggal", nativeQuery = true)
    List<RunAttendanceDto> listRunAttendance11(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);
    
    @Query(value = "select id, tanggal, hari, shiftName, shiftIn, shiftOut, workDayStatus, min(waktuMasuk) as waktuMasuk, max(waktuKeluar) as waktuKeluar, stat, flexible, attd from\n" +
            "(select id, a.tanggal as tanggal, hari, coalesce(schedule_name,shift_name) as shiftName, shift_in as shiftIn, shift_out as shiftOut,work_day_status as workDayStatus, coalesce(waktu_masuk,e.jam_masuk)as waktuMasuk, coalesce(waktu_keluar,e.jam_keluar)as waktuKeluar, coalesce(d.hitung_masuk,c.hitung_masuk,work_day_status) as stat,flexible,attd from\n" +
            "(select aa.id,date(aa.tanggal) as tanggal, aa.hari, aa.schedule_name, aa.shift_name, concat(aa.tanggal,' ',aa.shift_in)+ interval 7 hour as shift_in, concat(aa.tanggal,' ',aa.shift_out)+ interval 7 hour as shift_out, aa.work_day_status,flexible,attd from\n" +
            "(SELECT b.id,a.gen_date AS tanggal, DAYNAME(a.gen_date) AS hari, schedule_name, shift_name, shift_in, shift_out,efective_date,work_day_status,flexible,attd FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name, d.*, (a.date_update)as dates, flexible, attendance as attd  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date  order by tanggal)aa \n" +
            "inner join\n" +
            "(SELECT a.gen_date AS tanggal,max(efective_date)as edate FROM\n" +
            "(SELECT gen_date, DAYOFWEEK(gen_date) AS hari FROM \n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            " (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            " (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            " (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            " (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            " (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2)AS a\n" +
            "LEFT JOIN\n" +
            "(select b.* from\n" +
            "(select efective_date, max(date_update) as dates from employee_schedule\n" +
            "where id_employee = ?3 and status = 'AKTIF'\n" +
            "group by id_employee, efective_date\n" +
            "order by efective_date)a\n" +
            "inner join\n" +
            "(SELECT a.id AS id_schedule, a.efective_date,c.id_day,b.schedule_name , d.*, (a.date_update)as dates  FROM employee_schedule AS a\n" +
            "INNER JOIN pattern_schedule AS b ON a.id_pattern_schedule = b.id \n" +
            "INNER JOIN shift_pattern AS c ON b.id_pattern = c.id_pattern\n" +
            "INNER JOIN shift_detail AS d ON c.id_shift_detail = d.id\n" +
            "WHERE a.id_employee = ?3 AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status = 'AKTIF'\n" +
            "order by efective_date desc limit 2000) b\n" +
            "on a.efective_date = b.efective_date and a.dates = b.dates order by efective_date)AS b\n" +
            "ON a.hari = b.id_day AND a.gen_date >= b.efective_date group by tanggal  order by tanggal)as bb \n" +
            "on aa.tanggal = bb.tanggal and aa.efective_date = bb.edate order by aa.tanggal)a\n" +
            "left join\n" +
            "(select id as id_attendance, id_employee, date, concat(date,' ',waktu_masuk) as waktu_masuk,concat(date,' ',waktu_keluar) as waktu_keluar from attendance where id_employee = ?3 and status = 'AKTIF' and date BETWEEN ?1 AND  ?2)b\n" +
            "on a.tanggal = b.date \n" +
            "left join\n" +
            "(select date_day_off, keterangan, 'CUTI' as statusTimeOff,coalesce(if(hitung_masuk = 'AKTIF',hitung_masuk, 'OFF'),'OFF') as hitung_masuk from day_off where status = 'AKTIF' and date_day_off BETWEEN ?1 AND ?2)c\n" +
            "on a.tanggal = c.date_day_off\n" +
            "left join\n" +
            "(select a.id as id_time_off, a.tanggal_time_off, a.tanggal_time_off_sampai, b.time_off_name, a.keterangan as ket_timeoff, 'CUTI' as statusTimeOff,b.hitung_masuk from time_off_request as a\n" +
            "inner join time_off_jenis as b on a.id_time_off_jenis = b.id \n" +
            "where a.status = 'AKTIF' and a.status_approve = 'APPROVED' and a.id_employee = ?3 and ((tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai BETWEEN ?1 AND ?2) or\n" +
            "            (tanggal_time_off BETWEEN ?1 and ?2 and tanggal_time_off_sampai >= ?2) or \n" +
            "            (tanggal_time_off <= ?1 and tanggal_time_off_sampai >= ?2)))d \n" +
            "on a.tanggal between tanggal_time_off and tanggal_time_off_sampai \n" +
            "left join\n" +
            "(select tanggal,keterangan,'REQ' as status_attendance, addtime(concat(tanggal,' ',jam_masuk),'07:00:00') as jam_masuk, addtime(concat(tanggal,' ',jam_keluar),'07:00:00')as jam_keluar from attendance_request where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?3 and tanggal BETWEEN ?1 AND ?2)e\n" +
            "on a.tanggal = e.tanggal\n" +
            "order by a.tanggal)a group by tanggal order by tanggal", nativeQuery = true)
    List<RunAttendanceDto> listRunAttendance(LocalDate tanggalDari, LocalDate tanggalSampai, String idEmployee);



}
