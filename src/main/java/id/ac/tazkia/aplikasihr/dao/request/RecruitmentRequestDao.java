package id.ac.tazkia.aplikasihr.dao.request;

import id.ac.tazkia.aplikasihr.dto.recruitment.ApprovalListRecruitmentDto;
import id.ac.tazkia.aplikasihr.dto.recruitment.ApprovalRecruitmentRequestDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeType;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface RecruitmentRequestDao extends PagingAndSortingRepository<RecruitmentRequest, String>, CrudRepository<RecruitmentRequest, String> {

    Page<RecruitmentRequest> findByStatusAndEmployesOrderByDateRequestDesc(StatusRecord status, Employes employes, Pageable page);

    Page<RecruitmentRequest> findByStatusAndStatusApproveOrderByDateRequest(StatusRecord status, StatusRecord statusApprove, Pageable page);

    List<RecruitmentRequest> findByStatusAndStatusApproveAndStatusLaunchingAndEmployeeTypeOrderByOpenDate(StatusRecord status, StatusRecord statusApprove, StatusRecord statusLaunching, EmployeeType employe);

    List<RecruitmentRequest> findByStatusAndStatusLaunchingAndOpenDate(StatusRecord status, StatusRecord statusLaunching, LocalDate open);

    @Query(value = "SELECT coalesce(number,0) as number FROM (SELECT b.id_employee,a.sequence AS number,'WAITING' AS status_approve,'' AS keterangan,'AKTIF' AS STATUS \n" +
            "FROM approval_request AS a INNER JOIN employee_job_position AS b ON a.id_position_approve = b.id_position INNER JOIN employes AS c \n" +
            "ON b.id_employee = c.id WHERE a.id_position IN ?1 AND a.status = 'AKTIF' AND b.start_date <= DATE(NOW()) \n" +
            "AND b.end_date >= DATE(NOW()) AND b.status='AKTIF' AND c.status = 'AKTIF')a ORDER BY number desc limit 1", nativeQuery = true)
    Integer jumlahApproval(List<String> idPosition);

    @Query(value = "select concat(job_description, '<br>', additional_information) as descHrd from recruitment_request where id=?1", nativeQuery = true)
    String descHRD(String idRecruitment);

    @Query(value = "SELECT * FROM\n" +
            "(SELECT c.sequence AS nomor, a.id AS approvalRequest, e.full_name AS employee, u.position_name as position , COALESCE(f.status_approve,'WAITING') \n" +
            "AS approve, f.tanggal_approve AS tanggalApprove, f.keterangan as keterangan FROM recruitment_request AS a \n" +
            "INNER JOIN (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) AS b \n" +
            "ON a.id_employee_request = b.id_employee \n" +
            "INNER JOIN approval_request AS c ON b.id_position = c.id_position\n" +
            "INNER JOIN (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) AS d \n" +
            "ON c.id_position_approve = d.id_position \n" +
            "INNER JOIN employes AS e ON d.id_employee = e.id \n" +
            "inner join job_position as u on d.id_position = u.id \n" +
            "LEFT JOIN recruitment_request_approval AS f ON a.id = f.id_recruitment_request AND c.sequence = f.number AND e.id = f.id_employee \n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND d.status = 'AKTIF' AND (e.status = 'AKTIF' OR e.status IS NULL)\n" +
            "AND b.id_employee = ?1 ORDER BY a.id)a ORDER BY approvalRequest, nomor", nativeQuery = true)
    List<ApprovalRecruitmentRequestDto> listApprovalRequestRecruitment(String idEmployee);

    @Query(value = "select a.* from\n" +
            "(select a.id, e.full_name as fullName, a.date_request as tanggalPengajuan, qualification, job_description as description, additional_information as information, a.gender, f.department_name as department, g.position_name as position, a.number from\n" +
            "(select * from recruitment_request where status = 'AKTIF' and status_approve = 'WAITING')a\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) as b on a.id_employee_request = b.id_employee \n" +
            "inner join approval_request as c on b.id_position = c.id_position and a.number = c.sequence \n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) as d on c.id_position_approve = d.id_position \n" +
            "inner join employes as e on a.id_employee_request = e.id\n" +
            "inner join department as f on a.for_departement = f.id\n" +
            "inner join job_position as g on a.for_job_position = g.id\n" +
            "where d.id_employee = ?1 and b.status='AKTIF' and b.status_aktif = 'AKTIF' and c.status = 'AKTIF' and d.status='AKTIF' and d.status_aktif = 'AKTIF'\n" +
            "group by a.id)a\n" +
            "left join \n" +
            "(select id, id_recruitment_request as id_request, status_approve, number from recruitment_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1)b \n" +
            "on a.id = b.id_request and a.number = b.number\n" +
            "where b.status_approve is null\n" +
            "order by tanggalPengajuan", countQuery = "select count(a.id) as page from\n" +
            "(select a.id, e.full_name as fullName, a.date_request as tanggalPengajuan, qualification, job_description as description, additional_information as information, a.gender, f.department_name as department, g.position_name as position, a.number from\n" +
            "(select * from recruitment_request where status = 'AKTIF' and status_approve = 'WAITING')a\n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) as b on a.id_employee_request = b.id_employee \n" +
            "inner join approval_request as c on b.id_position = c.id_position and a.number = c.sequence \n" +
            "inner join (select * from employee_job_position where status = 'AKTIF' and end_date >= date(now()) group by id_employee) as d on c.id_position_approve = d.id_position \n" +
            "inner join employes as e on a.id_employee_request = e.id\n" +
            "inner join department as f on a.for_departement = f.id\n" +
            "inner join job_position as g on a.for_job_position = g.id\n" +
            "where d.id_employee = ?1 and b.status='AKTIF' and b.status_aktif = 'AKTIF' and c.status = 'AKTIF' and d.status='AKTIF' and d.status_aktif = 'AKTIF'\n" +
            "group by a.id)a\n" +
            "left join \n" +
            "(select id, id_recruitment_request as id_request, status_approve, number from recruitment_request_approval where status = 'AKTIF' and status_approve = 'APPROVED' and id_employee = ?1)b \n" +
            "on a.id = b.id_request and a.number = b.number\n" +
            "where b.status_approve is null\n" +
            "order by tanggalPengajuan",
            nativeQuery = true)
    Page<ApprovalListRecruitmentDto> listApprovalRecruitment(String idEmployee, Pageable pageable);

    @Query(value = "select c.type, b.department_name, count(a.for_job_position) as lowongan from recruitment_request as a inner join department as b on a.for_departement=b.id inner join employee_type as c on a.new_employee_type=c.id where a.status='AKTIF' and a.status_launching='AKTIF' group by for_departement", nativeQuery = true)
    List<Object> listLowongan();

}
