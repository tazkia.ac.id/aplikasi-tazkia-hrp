package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeRiwayatPendidikan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeRiwayatPendidikanDao extends PagingAndSortingRepository<EmployeeRiwayatPendidikan, String>, CrudRepository<EmployeeRiwayatPendidikan, String> {

    List<EmployeeRiwayatPendidikan> findByStatusAndEmployesOrderByTahunMasuk(StatusRecord statusRecord, Employes employes);

}
