package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.PtkpStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PtkpStatusDao extends PagingAndSortingRepository<PtkpStatus, String>, CrudRepository<PtkpStatus, String> {

    List<PtkpStatus> findByStatusOrderByPtkp(StatusRecord statusRecord);

}
