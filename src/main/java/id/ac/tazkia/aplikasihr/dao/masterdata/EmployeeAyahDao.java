package id.ac.tazkia.aplikasihr.dao.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeAyah;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeAyahDao extends PagingAndSortingRepository<EmployeeAyah, String>, CrudRepository<EmployeeAyah, String> {

    List<EmployeeAyah> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes);

}
