package id.ac.tazkia.aplikasihr.dao.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PatternDao extends PagingAndSortingRepository<Pattern, String>, CrudRepository<Pattern, String> {

    List<Pattern> findByStatusOrderByPatternName(StatusRecord statusRecord);

    Page<Pattern> findByStatusOrderByPatternName(StatusRecord statusRecord, Pageable pageable);

    Page<Pattern> findByStatusAndPatternNameContainingIgnoreCaseOrderByDateUpdate(StatusRecord statusRecord, String name, Pageable pageable);

}
