package id.ac.tazkia.aplikasihr.dao.thr;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRunProcess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ThrRunProcessDao extends PagingAndSortingRepository<ThrRunProcess, String>, CrudRepository<ThrRunProcess, String> {

    ThrRunProcess findFirstByStatusOrderByTanggalInput(StatusRecord statusRecord);

    List<ThrRunProcess> findByStatusNotOrderByTanggalInputDesc(StatusRecord statusRecord);

}
