package id.ac.tazkia.aplikasihr.dao.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantRecruitmentRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ApplicantRecruitmentRequestDao extends PagingAndSortingRepository<ApplicantRecruitmentRequest, String>, CrudRepository<ApplicantRecruitmentRequest, String> {

    List<ApplicantRecruitmentRequest> findByApplicantAndStatus(Applicant applicant, StatusRecord status);

}
