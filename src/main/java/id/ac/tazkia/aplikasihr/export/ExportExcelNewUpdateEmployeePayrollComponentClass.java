package id.ac.tazkia.aplikasihr.export;

import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
//import jdk.nashorn.internal.codegen.types.NumericType;
import org.apache.poi.hpsf.Decimal;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import javax.swing.text.NumberFormatter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Currency;
import java.util.List;

public class ExportExcelNewUpdateEmployeePayrollComponentClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<ExportPayrollComponentToExcelDto> listExportPayrollComponentToExcelDto;

    public ExportExcelNewUpdateEmployeePayrollComponentClass(List<ExportPayrollComponentToExcelDto> listExportPayrollComponentToExcelDto) {
        this.listExportPayrollComponentToExcelDto = listExportPayrollComponentToExcelDto;
        workbook = new XSSFWorkbook();
    }



        private void writeHeaderLine() {
            sheet = workbook.createSheet("Users");

            Row row = sheet.createRow(0);

            CellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setFontHeight(16);
            style.setFont(font);

            createCell(row, 0, "id_employee", style);
            createCell(row, 1, "number", style);
            createCell(row, 2, "full_name", style);
            createCell(row, 3, "current_ammount", style);
            createCell(row, 4, "new_ammount", style);

        }

        private void createCell(Row row, int columnCount, Object value, CellStyle style) {
            sheet.autoSizeColumn(columnCount);
            Cell cell = row.createCell(columnCount);
            if (value instanceof Integer) {
                cell.setCellValue((Integer) value);
            } else if (value instanceof Boolean) {
                cell.setCellValue((Boolean) value);
            }else {
                cell.setCellValue((String) value);
            }
            cell.setCellStyle(style);
        }
    private void createStringCell(Row row, String val, int col) {
        Cell cell = row.createCell(col);
        cell.setCellType(CellType.STRING);
        cell.setCellValue(val);
    }

    public static String withLargeIntegers(double value) {
        DecimalFormat df = new DecimalFormat("###,###,###");
        return df.format(value);
    }

    public static double withTwoDecimalPlaces(double value) {
        DecimalFormat df = new DecimalFormat("#.00");
        return new Double(df.format(value));
    }


    private void createNumericCell(Row row, int columnCount, Number value) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        cell.setCellType(CellType.NUMERIC);
        cell.setCellValue((Double) value);
    }

        private void writeDataLines() {
            int rowCount = 1;

            CellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setFontHeight(14);
            style.setFont(font);

            for (ExportPayrollComponentToExcelDto exportPayrollComponentToExcelDto : listExportPayrollComponentToExcelDto) {
                Row row = sheet.createRow(rowCount++);
                int columnCount = 0;

                Double satu = new Double(exportPayrollComponentToExcelDto.getCurrrentAmmount().doubleValue());
                Double dua = new Double(exportPayrollComponentToExcelDto.getNewAmmount().doubleValue());

                createCell(row, columnCount++, exportPayrollComponentToExcelDto.getId(), style);
                createCell(row, columnCount++, exportPayrollComponentToExcelDto.getNumber(), style);
                createCell(row, columnCount++, exportPayrollComponentToExcelDto.getFullName(), style);
                createNumericCell(row, columnCount++, withTwoDecimalPlaces(satu));
                createNumericCell(row, columnCount++, withTwoDecimalPlaces(dua));

            }
        }

        public void export(HttpServletResponse response) throws IOException {
            writeHeaderLine();
            writeDataLines();

            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            workbook.close();

            outputStream.close();

        }


}
