package id.ac.tazkia.aplikasihr.export.shift;

import id.ac.tazkia.aplikasihr.dto.EmployeeDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.ListHeaderShiftDto;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.List;

public class ExportShiftClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<ListHeaderShiftDto> shiftList;

    private List<EmployeeDto> employeeDtoList;

    public ExportShiftClass(List<ListHeaderShiftDto> shiftList, List<EmployeeDto> employeeDtoList)  {
        this.shiftList = shiftList;
        this.employeeDtoList = employeeDtoList;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(12);
        style.setFont(font);

        createCell(row, 0, "Employee_id", style);
        createCell(row, 1, "name", style);
        int columnCount = 2;
        for (ListHeaderShiftDto sl : shiftList) {
//            int columnCount = 0;
            createCell(row, columnCount++, sl.getTanggal(), style);
        }
//        createCell(row, 0, "id_employee", style);
//        createCell(row, 1, "number", style);
//        createCell(row, 2, "full_name", style);
//        createCell(row, 3, "status_aktif", style);
//        createCell(row, 4, "status_employee", style);
//        createCell(row, 5, "ptkp_status", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (EmployeeDto employes : employeeDtoList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, employes.getNumber(), style);
            createCell(row, columnCount++, employes.getNama(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
