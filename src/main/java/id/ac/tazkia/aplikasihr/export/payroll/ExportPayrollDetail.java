package id.ac.tazkia.aplikasihr.export.payroll;

import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportPayrollDetail {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<DetailPayrollReportDto> payrollReportDtos;

    public ExportPayrollDetail(List<DetailPayrollReportDto> payrollReportDtos) {
        this.payrollReportDtos = payrollReportDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row1 = sheet.createRow(0);

        CellStyle style1 = workbook.createCellStyle();
        XSSFFont font1 = workbook.createFont();
        font1.setBold(true);
        font1.setFontHeight(16);
        style1.setFont(font1);

        createCell(row1, 0, "", style1);
        createCell(row1, 1, "", style1);
        createCell(row1, 2, "", style1);
        createCell(row1, 3, "", style1);
        createCell(row1, 4, "", style1);
        createCell(row1, 5, "", style1);
        createCell(row1, 6, "", style1);
        createCell(row1, 7, "Deductions", style1);
        createCell(row1, 8, "Deductions", style1);
        createCell(row1, 9, "Deductions", style1);
        createCell(row1, 10, "Deductions", style1);
        createCell(row1, 11, "Deductions", style1);
        createCell(row1, 12, "Deductions", style1);
        createCell(row1, 13, "Deductions", style1);
        createCell(row1, 14, "", style1);


        Row row = sheet.createRow(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Employee_No", style);
        createCell(row, 1, "Full_Name", style);
        createCell(row, 2, "Basic_Salary", style);
        createCell(row, 3, "Total_Allowance", style);
        createCell(row, 4, "Total_Payroll", style);
        createCell(row, 5, "Taxable", style);
        createCell(row, 6, "Benefits", style);
        createCell(row, 7, "Loan", style);
        createCell(row, 8, "Potongan_Absen", style);
        createCell(row, 9, "Potongan_Terlambat", style);
        createCell(row, 10, "Zakat", style);
        createCell(row, 11, "Infaq", style);
        createCell(row, 12, "Pajak", style);
        createCell(row, 13, "Else", style);
        createCell(row, 14, "Take_Home_Pay", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 2;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (DetailPayrollReportDto employes : payrollReportDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, employes.getNumber(), style);
            createCell(row, columnCount++, employes.getFullName(), style);
            createCell(row, columnCount++, employes.getGajiPokok().doubleValue(), style);
            createCell(row, columnCount++, employes.getAllowance().doubleValue(), style);
            createCell(row, columnCount++, employes.getTotalGaji().doubleValue(), style);
            createCell(row, columnCount++, employes.getKenaPajak().doubleValue(), style);
            createCell(row, columnCount++, employes.getBenefits().doubleValue(), style);
            createCell(row, columnCount++, employes.getLoan().doubleValue(), style);
            createCell(row, columnCount++, employes.getPotonganAbsen().doubleValue(), style);
            createCell(row, columnCount++, employes.getPotonganTerlambat().doubleValue(), style);
            createCell(row, columnCount++, employes.getZakat().doubleValue(), style);
            createCell(row, columnCount++, employes.getInfaq().doubleValue(), style);
            createCell(row, columnCount++, employes.getPajak().doubleValue(), style);
            createCell(row, columnCount++, employes.getDeduction().doubleValue(), style);
            createCell(row, columnCount++, employes.getTakeHomePay().doubleValue(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }


}
