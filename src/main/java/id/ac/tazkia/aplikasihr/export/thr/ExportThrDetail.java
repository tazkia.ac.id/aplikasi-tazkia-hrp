package id.ac.tazkia.aplikasihr.export.thr;

import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRun;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportThrDetail {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<ThrRun> thrRuns;
    private String bulan;
    private String tahun;
    private String companies;

    public ExportThrDetail(List<ThrRun> thrRuns, String bulan, String tahun, String companies) {
        this.thrRuns = thrRuns;
        this.bulan = bulan;
        this.tahun = tahun;
        this.companies = companies;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row1 = sheet.createRow(0);

        CellStyle style1 = workbook.createCellStyle();
        XSSFFont font1 = workbook.createFont();
        font1.setBold(true);
        font1.setFontHeight(16);
        style1.setFont(font1);

        createCell(row1, 0, "Report THR Bulan "+ bulan +", Tahun "+ tahun, style1);

        Row row = sheet.createRow(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Employee_No", style);
        createCell(row, 1, "Full_Name", style);
        createCell(row, 2, "Branch", style);
        createCell(row, 3, "THR_Gross", style);
        createCell(row, 4, "Tax", style);
        createCell(row, 5, "THR", style);
        createCell(row, 6, "Part 1", style);
        createCell(row, 7, "Part 2", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 2;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (ThrRun thr : thrRuns) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, thr.getEmployes().getNumber(), style);
            createCell(row, columnCount++, thr.getEmployes().getFullName(), style);
            createCell(row, columnCount++, thr.getEmployes().getCompanies().getCompanyName(), style);
            createCell(row, columnCount++, thr.getTotalThr().doubleValue(), style);
            createCell(row, columnCount++, thr.getTotalPajak().doubleValue(), style);
            createCell(row, columnCount++, thr.getThrBersih().doubleValue(), style);
            createCell(row, columnCount++, thr.getPartSatu().doubleValue(), style);
            createCell(row, columnCount++, thr.getPartDua().doubleValue(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
