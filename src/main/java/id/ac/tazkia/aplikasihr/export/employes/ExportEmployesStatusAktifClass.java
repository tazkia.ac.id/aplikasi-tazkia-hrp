package id.ac.tazkia.aplikasihr.export.employes;

import id.ac.tazkia.aplikasihr.dto.EmployeeStatusAktifDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.apache.poi.ss.util.CellUtil.createCell;

public class ExportEmployesStatusAktifClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<EmployeeStatusAktifDto> employeeStatusAktifDtoList;

    public ExportEmployesStatusAktifClass(List<EmployeeStatusAktifDto> employeeStatusAktifDtoList) {
        this.employeeStatusAktifDtoList = employeeStatusAktifDtoList;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "id_employee", style);
        createCell(row, 1, "full_name", style);
        createCell(row, 2, "status_aktif", style);
        createCell(row, 3, "date (YYYY-MM-DD)", style);


    }

//    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
//        sheet.autoSizeColumn(columnCount);
//        Cell cell = row.createCell(columnCount);
//        if (value instanceof Integer) {
//            cell.setCellValue((Integer) value);
//        } else if (value instanceof Boolean) {
//            cell.setCellValue((Boolean) value);
//        }else {
//            cell.setCellValue((String) value);
//        }
//        cell.setCellStyle(style);
//    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        cell.setCellStyle(style);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Date) { // Check if value is a Date
            cell.setCellValue((Date) value); // Set the Date value directly
            CellStyle dateStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd/MM/yyyy")); // Adjust the date format as needed
            cell.setCellStyle(dateStyle);
        } else {
            cell.setCellValue(value.toString()); // Convert other types to String
        }
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (EmployeeStatusAktifDto employes : employeeStatusAktifDtoList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            if(employes.getNikAda() != null) {
                createCell(row, columnCount++, employes.getNikAda(), style);
            }else{
                createCell(row, columnCount++, "", style);
            }
            createCell(row, columnCount++, employes.getNama(), style);
            if(employes.getStatusAda() != null) {
                createCell(row, columnCount++, employes.getStatusAda(), style);
            }else{
                createCell(row, columnCount++, "", style);
            }
            if(employes.getTanggalAda() != null) {
                createCell(row, columnCount++, employes.getTanggalAda(), style);
            }else{
                createCell(row, columnCount++, "", style);
            }

        }
    }


    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
