package id.ac.tazkia.aplikasihr.export.payroll;

import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.LecturerSalaryReportDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportPayrollLecturer {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<LecturerSalaryReportDto> lecturerSalaryReportDtos;

    public ExportPayrollLecturer(List<LecturerSalaryReportDto> lecturerSalaryReportDtos) {
        this.lecturerSalaryReportDtos = lecturerSalaryReportDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Lecturer_salary");

        Row row1 = sheet.createRow(0);

        CellStyle style1 = workbook.createCellStyle();
        XSSFFont font1 = workbook.createFont();
        font1.setBold(true);
        font1.setFontHeight(16);
        style1.setFont(font1);

        createCell(row1, 0, "No", style1);
        createCell(row1, 1, "Name", style1);
        createCell(row1, 2, "Type", style1);
        createCell(row1, 3, "Course", style1);
        createCell(row1, 4, "Class", style1);
        createCell(row1, 5, "Total Session", style1);
        createCell(row1, 6, "Date", style1);
        createCell(row1, 7, "SKS Total", style1);
        createCell(row1, 8, "Tarif", style1);
        createCell(row1, 9, "Total", style1);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (LecturerSalaryReportDto lecturerSalaryReportDto : lecturerSalaryReportDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, rowCount - 1, style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getNama(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getClasss(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getMatkul(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getKelas(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getTotalSesi().doubleValue(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getTanggal(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getTotalSks().doubleValue(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getTarif().doubleValue(), style);
            createCell(row, columnCount++, lecturerSalaryReportDto.getTotal().doubleValue(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
