package id.ac.tazkia.aplikasihr.export.payroll;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDetailDao;
import id.ac.tazkia.aplikasihr.dto.payroll.*;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;


public class ExportPayrollFullDetail {


    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    PayrollRunDetailDao payrollRunDetailDao;

    private List<IsiPayrollRunReportDto> payrollRunReportDtos;
    private List<EmployeePayrollReportsFullDto> employeePayrollReportsFullDtos;
    private List<HeaderPayrollRunReportDto> headerPayrollRunReportDtos;

    String tahun;

    String bulan;


    public ExportPayrollFullDetail(List<EmployeePayrollReportsFullDto> employeePayrollReportsFullDtos, List<HeaderPayrollRunReportDto> headerPayrollRunReportDtos, List<IsiPayrollRunReportDto> payrollRunReportDtos) {
        this.payrollRunReportDtos = payrollRunReportDtos;
        this.employeePayrollReportsFullDtos = employeePayrollReportsFullDtos;
        this.headerPayrollRunReportDtos = headerPayrollRunReportDtos;
        this.tahun = tahun;
        this.bulan = bulan;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        int rowCount = 0;
        sheet = workbook.createSheet("Salary");

        Row row1 = sheet.createRow(0);

        CellStyle style1 = workbook.createCellStyle();
        XSSFFont font1 = workbook.createFont();
        font1.setBold(true);
        font1.setFontHeight(11);
        style1.setFont(font1);


        Row row = sheet.createRow(rowCount);
        int columnCount = 2;

        createCell(row, 0, "NIP",style1);
        createCell(row, 1, "Nama",style1);
        for (HeaderPayrollRunReportDto headerPayrollRunReportDto : headerPayrollRunReportDtos) {
            createCell(row, columnCount++, headerPayrollRunReportDto.getNama(),style1);
        }

    }

//    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
//        sheet.autoSizeColumn(columnCount);
//        Cell cell = row.createCell(columnCount);
//        if (value instanceof Integer) {
//            cell.setCellValue((Integer) value);
//        } else if (value instanceof Boolean) {
//            cell.setCellValue((Boolean) value);
//        } else if (value instanceof Double) {
//            cell.setCellValue((Double) value);
//        }else {
//            cell.setCellValue((String) value);
//        }
//        cell.setCellStyle(style);
//    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else {
            cell.setCellValue(value.toString());
        }

        cell.setCellStyle(style);
    }


    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(11);
        style.setFont(font);
        int jumlahRow = 1;
        int jumlahColum = 1;
        BigDecimal kosong = BigDecimal.ZERO;


        for (EmployeePayrollReportsFullDto employes : employeePayrollReportsFullDtos) {
            System.out.println("export employee : "+employes.getFullName());
            Row arow = sheet.getRow(0);
            Row row = sheet.createRow(rowCount++);
            int columnCount = 2;
            if(employes.getNumber() != null) {
                createCell(row, 0, employes.getNumber(), style);
            }else{
                createCell(row, 0, '-', style);
            }
            if(employes.getFullName() != null) {
                createCell(row, 1, employes.getFullName(), style);
            }else{
                createCell(row, 1,'-', style);
            }

            for (HeaderPayrollRunReportDto headerPayrollRunReportDto : headerPayrollRunReportDtos) {
                int ada = 0;
                for (IsiPayrollRunReportDto isiPayrollRunReportDto : payrollRunReportDtos) {
                    if (employes.getId().equals(isiPayrollRunReportDto.getIdEmployee())) {
                        if(headerPayrollRunReportDto.getNama().equals(isiPayrollRunReportDto.getNama()) && headerPayrollRunReportDto.getJenis().equals(isiPayrollRunReportDto.getJenis()) && headerPayrollRunReportDto.getNomor().equals(isiPayrollRunReportDto.getNomor())) {
//                            if(isiPayrollRunReportDto.getNominal() != null) {
                                    createCell(row, columnCount++, isiPayrollRunReportDto.getNominal(), style);
//                            }else{
//                                createCell(row, columnCount++, 0, style);
//                            }
                            ada = 1;
                        }
                    }
                }
                if(ada == 0){
                    createCell(row, columnCount++, 0, style);
                }
            }

        }

//        for (EmployeePayrollReportsFullDto employes : employeePayrollReportsFullDtos) {
//            Row arow = sheet.getRow(0);
//            Row row = sheet.createRow(rowCount++);
//            int columnCount = 2;
//            createCell(row,0, employes.getNumber(), style);
//            createCell(row,1, employes.getFullName(), style);
//
//            for (IsiPayrollRunReportDto isiPayrollRunReportDto : payrollRunReportDtos) {
//                if (row.getCell(0).getCellType() == CellType.STRING) {
////                    if (row.getCell(columnCount).getCellType() == CellType.STRING) {
//                    if (isiPayrollRunReportDto.getKode().equals(arow.getCell(columnCount++).getStringCellValue())) {
//                        createCell(row, columnCount++, isiPayrollRunReportDto.getNominal(), style);
////                        column = column++;
//                    }
////                    }
//                }
//            }
////            for (HeaderPayrollRunReportDto headerPayrollRunReportDto : headerPayrollRunReportDtos){
////               createCell(row, columnCount++, headerPayrollRunReportDto.getNama(),style);
////
////               jumlahColum = columnCount++;
////            }
////            jumlahRow = rowCount++;
//        }

//        for(int i=1;i<jumlahRow ;i++) {
//            XSSFRow row = sheet.getRow(i);
//            for(int j=2;j<jumlahColum ;j++){
//                if (row.getCell(0) != null) {
//                    if (row.getCell(j) != null) {
//                        for (IsiPayrollRunReportDto isiPayrollRunReportDto : payrollRunReportDtos) {
//                            if (row.getCell(0).getCellType() == CellType.STRING) {
//                                if (row.getCell(j).getCellType() == CellType.STRING) {
//                                    if (isiPayrollRunReportDto.getKode().equals(row.getCell(0).getStringCellValue()) & isiPayrollRunReportDto.getNama().equals(row.getCell(j).getStringCellValue())) {
//                                        row.getCell(j).setCellValue(isiPayrollRunReportDto.getNominal().doubleValue());
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }

//    private void isiDatalines(){
//        for(int i=1;i<sheet.getPhysicalNumberOfRows() ;i++) {
//            XSSFRow row3 = sheet.getRow(i);
//            int columnCount2 = 2;
//            if(row3.getCell(columnCount2++) != null) {
//                for (IsiPayrollRunReportDto isiPayrollRunReportDto : payrollRunReportDtos) {
//                    if (isiPayrollRunReportDto.getNama().equals(row3.getCell(columnCount2++).getStringCellValue())) {
//                        row3.getCell(columnCount2++).setCellValue(isiPayrollRunReportDto.getNominal().doubleValue());
//                    } else {
//                        row3.getCell(columnCount2++).setCellValue(BigDecimal.ZERO.doubleValue());
//                    }
//                }
//            }
//        }
//    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
//        isiDatalines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
