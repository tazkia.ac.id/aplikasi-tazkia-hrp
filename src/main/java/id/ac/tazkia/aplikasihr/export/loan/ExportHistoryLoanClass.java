package id.ac.tazkia.aplikasihr.export.loan;

import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.loan.HistoryLoanDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportHistoryLoanClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<HistoryLoanDto> historyLoanDtos;

    public ExportHistoryLoanClass(List<HistoryLoanDto> historyLoanDtos) {
        this.historyLoanDtos = historyLoanDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "number", style);
        createCell(row, 1, "Full_name", style);
        createCell(row, 2, "loan_name", style);
        createCell(row, 3, "Desription", style);
        createCell(row, 4, "installment", style);
        createCell(row, 5, "installment_paid", style);
        createCell(row, 6, "payment", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (HistoryLoanDto hl : historyLoanDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, hl.getNumber(), style);
            createCell(row, columnCount++, hl.getEmployeeName(), style);
            createCell(row, columnCount++, hl.getLoan(), style);
            createCell(row, columnCount++, hl.getDescriptions(), style);
            createCell(row, columnCount++, hl.getInstallment(), style);
            createCell(row, columnCount++, hl.getCicilanKe(), style);
            createCell(row, columnCount++, hl.getPayment(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }


}
