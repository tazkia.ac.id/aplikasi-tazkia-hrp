package id.ac.tazkia.aplikasihr.export.shift;

import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.PatternScheduleListDto;
import jakarta.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import javax.servlet.ServletOutputStream;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportPatternSchedule {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<PatternScheduleListDto> patternScheduleListDtos;

    public ExportPatternSchedule(List<PatternScheduleListDto> patternScheduleListDtos) {
        this.patternScheduleListDtos = patternScheduleListDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Schedule_name", style);
        createCell(row, 1, "Sunday", style);
        createCell(row, 2, "Monday", style);
        createCell(row, 3, "Tuesday", style);
        createCell(row, 4, "Wednesday", style);
        createCell(row, 5, "Tuhrsday", style);
        createCell(row, 6, "Friday", style);
        createCell(row, 7, "Saturday", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (PatternScheduleListDto psld : patternScheduleListDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, psld.getScheduleName(), style);
            createCell(row, columnCount++, psld.getMinggu(), style);
            createCell(row, columnCount++, psld.getSenin(), style);
            createCell(row, columnCount++, psld.getSelasa(), style);
            createCell(row, columnCount++, psld.getRabu(), style);
            createCell(row, columnCount++, psld.getKamis(), style);
            createCell(row, columnCount++, psld.getJumat(), style);
            createCell(row, columnCount++, psld.getSabtu(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();


    }
}
