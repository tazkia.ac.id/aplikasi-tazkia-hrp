package id.ac.tazkia.aplikasihr.export.finance;

import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

public class ExportEmployeeLoanClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<EmployeeLoan> employeeLoanList;

    public ExportEmployeeLoanClass(List<EmployeeLoan> employeeLoanList) {
        this.employeeLoanList = employeeLoanList;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(12);
        style.setFont(font);

//        createCell(row, 0, "id", style);
        createCell(row, 0, "employee_id", style);
        createCell(row, 1, "full_name", style);
        createCell(row, 2, "loan_name", style);
        createCell(row, 3, "loan_ammount", style);
        createCell(row, 4, "installment", style);
        createCell(row, 5, "installment_paid", style);
        createCell(row, 6, "current_payment", style);
        createCell(row, 7, "total_payment", style);
        createCell(row, 8, "remaining_loan", style);
        createCell(row, 9, "interest(%)", style);
        createCell(row, 10, "effective_date", style);
        createCell(row, 11, "descriptions", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }
    private void createStringCell(Row row, String val, int col) {
        Cell cell = row.createCell(col);
        cell.setCellType(CellType.STRING);
        cell.setCellValue(val);
    }

    public static String withLargeIntegers(double value) {
        DecimalFormat df = new DecimalFormat("###,###,###");
        return df.format(value);
    }

    public static double withTwoDecimalPlaces(double value) {
        DecimalFormat df = new DecimalFormat("#.00");
        return new Double(df.format(value));
    }


    private void createNumericCell(Row row, int columnCount, Number value) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        cell.setCellType(CellType.NUMERIC);
        cell.setCellValue((Double) value);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (EmployeeLoan employeeLoan : employeeLoanList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            Double satu = new Double(employeeLoan.getAmount().doubleValue());
            Double dua = new Double(employeeLoan.getInstallment().doubleValue());
            Double tiga = new Double(employeeLoan.getInstallmentPaid().doubleValue());
            Double empat = new Double(employeeLoan.getCurrentPayment().doubleValue());
            Double lima = new Double(employeeLoan.getTotalPayment().doubleValue());
            Double enam = new Double(employeeLoan.getRemaining().doubleValue());
            Double tujuh = new Double(employeeLoan.getInterest().doubleValue());

            createCell(row, columnCount++, employeeLoan.getEmployes().getNumber(), style);
            createCell(row, columnCount++, employeeLoan.getEmployes().getFullName(), style);
            createCell(row, columnCount++, employeeLoan.getLoan().getName(), style);
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(satu));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(dua));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(tiga));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(empat));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(lima));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(enam));
            createNumericCell(row, columnCount++, withTwoDecimalPlaces(tujuh));
            createCell(row, columnCount++, employeeLoan.getEffectiveDate(), style);
            createCell(row, columnCount++, employeeLoan.getDescriptions(), style);


        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }


}
