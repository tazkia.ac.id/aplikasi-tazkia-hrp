package id.ac.tazkia.aplikasihr.export;

import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExportEmployesClass {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<ExportEmployeeStatusDto> employesList;

    public ExportEmployesClass(List<ExportEmployeeStatusDto> employesList) {
        this.employesList = employesList;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "id_employee", style);
        createCell(row, 1, "number", style);
        createCell(row, 2, "full_name", style);
        createCell(row, 3, "status_aktif", style);
//        createCell(row, 4, "date_MM/DD/YYYY", style);
        createCell(row, 4, "status_employee", style);
//        createCell(row, 6, "date_MM/DD/YYYY", style);
        createCell(row, 5, "ptkp_status", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (ExportEmployeeStatusDto employes : employesList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, employes.getId(), style);
            createCell(row, columnCount++, employes.getNumber(), style);
            createCell(row, columnCount++, employes.getFullName(), style);
            createCell(row, columnCount++, employes.getStatusAktif(), style);
//            createCell(row, columnCount++, employes.getDateEmployeeStatusAktif(), style);
            createCell(row, columnCount++, employes.getStatusEmployee(), style);
//            createCell(row, columnCount++, employes.getDateEmployeeStatus(), style);
            createCell(row, columnCount++, employes.getPtkpStatus(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
