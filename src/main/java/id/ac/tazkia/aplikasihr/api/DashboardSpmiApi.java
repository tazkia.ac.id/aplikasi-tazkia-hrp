package id.ac.tazkia.aplikasihr.api;


import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dto.api.JumlahEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.api.JumlahEmployeeDto2;
import id.ac.tazkia.aplikasihr.dto.employes.TarikIdAbsenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DashboardSpmiApi {

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/api/jumlah-employee")
    @ResponseBody
    public ResponseEntity<List<JumlahEmployeeDto>> jumlahEmployeeDtos(){

        List<JumlahEmployeeDto2> jumlahEmployeeDto2 = employesDao.tarikJumlahEmployee();

        List<JumlahEmployeeDto> jumlahEmployeeDtos = new ArrayList<>();
        for(JumlahEmployeeDto2 jumlahEmployeeDto2s : jumlahEmployeeDto2){
            JumlahEmployeeDto jumlahEmployeeDto = new JumlahEmployeeDto();

            jumlahEmployeeDto.setCompanyId(jumlahEmployeeDto2s.getCompanyId());
            jumlahEmployeeDto.setCompanyName(jumlahEmployeeDto2s.getCompanyName());
            jumlahEmployeeDto.setTotal(jumlahEmployeeDto2s.getTotal());
            jumlahEmployeeDto.setMale(jumlahEmployeeDto2s.getMale());
            jumlahEmployeeDto.setFemale(jumlahEmployeeDto2s.getFemale());
            jumlahEmployeeDto.setPermanent(jumlahEmployeeDto2s.getPermanent());
            jumlahEmployeeDto.setTemporary(jumlahEmployeeDto2s.getTemporary());
            jumlahEmployeeDtos.add(jumlahEmployeeDto);

        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(jumlahEmployeeDtos, httpHeaders, HttpStatus.OK);
    }


}
