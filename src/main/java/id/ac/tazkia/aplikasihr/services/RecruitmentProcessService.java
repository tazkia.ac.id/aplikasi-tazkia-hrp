package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantScheduleDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.StatusWawancara;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantSchedule;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@EnableAsync
public class RecruitmentProcessService {

    @Autowired
    ApplicantScheduleDao scheduleDao;

    @Autowired
    RecruitmentRequestDao requestDao;

    @Scheduled(cron = "0 00 22 * * *", zone = "Asia/Jakarta")
    public void settingScheduleApplicantExpired(){

        LocalDate today = LocalDate.now();

        List<ApplicantSchedule> expired = scheduleDao.findByStatusAndTanggal(StatusRecord.AKTIF, today);

        for (ApplicantSchedule list : expired){

            list.setStatusWawancara(StatusWawancara.EXPIRED);
            scheduleDao.save(list);

        }

    }

    @Scheduled(cron = "0 50 9 * * *")
    public void launchingRecruitment(){

        LocalDate today = LocalDate.now();

        List<RecruitmentRequest> request = requestDao.findByStatusAndStatusLaunchingAndOpenDate(StatusRecord.AKTIF, StatusRecord.NONAKTIF, today);

        for (RecruitmentRequest publish : request){
            publish.setStatusLaunching(StatusRecord.AKTIF);
            requestDao.save(publish);
        }

    }

}
