package id.ac.tazkia.aplikasihr.services;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.stereotype.Service;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Service
public class ResizeFIleService {
    public void resizeFileImage(File tujuan) throws IOException {
        BufferedImage image = ImageIO.read(tujuan);

        int width = image.getWidth();
        int height = image.getHeight();

        Image imageResu = image.getScaledInstance(width / 2, height / 2, Image.SCALE_DEFAULT);

        BufferedImage bimage = new BufferedImage(imageResu.getWidth(null), imageResu.getHeight(null),
                BufferedImage.TYPE_INT_RGB);
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(imageResu, 0, 0, null);
        bGr.dispose();

        OutputStream out = new FileOutputStream(tujuan);

        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);

        ImageWriteParam param = writer.getDefaultWriteParam();
        if (param.canWriteCompressed()) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(0.3f);
        }

        writer.write(null, new IIOImage(bimage, null, null), param);

        out.close();
        ios.close();
        writer.dispose();

    }

    public void resizeFilePdf(File pdfFile) throws IOException {
        // Buat file hasil output PDF terkompresi
        File compressedPdfFile = new File(pdfFile.getParent(), pdfFile.getName());

        try (PDDocument document = PDDocument.load(pdfFile)) {
            PDFRenderer renderer = new PDFRenderer(document);

            // Buat dokumen PDF baru
            PDDocument newDocument = new PDDocument();

            for (int i = 0; i < document.getNumberOfPages(); i++) {
                // 1. Render halaman PDF menjadi gambar (DPI lebih rendah = ukuran file lebih kecil)
                BufferedImage image = renderer.renderImageWithDPI(i, 75); // DPI diturunkan ke 100 (dari 150)

                // 2. Kompres gambar ke format JPG dengan kualitas 30%
                File compressedImageFile = compressImage(image, 0.3f); // Simpan gambar JPG sementara

                // 3. Buat halaman PDF baru dengan ukuran yang sesuai dengan gambar
                float width = image.getWidth();
                float height = image.getHeight();
                PDPage page = new PDPage(new PDRectangle(width, height));
                newDocument.addPage(page);

                // 4. Tambahkan gambar ke halaman PDF
                PDImageXObject pdImage = PDImageXObject.createFromFileByContent(compressedImageFile, newDocument);
                try (PDPageContentStream contentStream = new PDPageContentStream(newDocument, page)) {
                    contentStream.drawImage(pdImage, 0, 0, width, height);
                    contentStream.close(); // Pastikan content stream ditutup
                }

                // Hapus file sementara (gambar JPG) setelah digunakan
                compressedImageFile.delete();
            }

            // Simpan file PDF yang telah dikompresi
            newDocument.save(compressedPdfFile);
            newDocument.close();
        }

        System.out.println("File PDF berhasil diresize: " + compressedPdfFile.getAbsolutePath());
    }

    /**
     * Kompres gambar BufferedImage ke file JPG sementara
     * @param image Gambar yang akan dikompresi
     * @param quality Kualitas gambar (0.1 - 1.0)
     * @return File gambar sementara
     */
    private File compressImage(BufferedImage image, float quality) throws IOException {
        File compressedImageFile = File.createTempFile("compressed_image", ".jpg");

        try (FileOutputStream fos = new FileOutputStream(compressedImageFile);
             ImageOutputStream ios = ImageIO.createImageOutputStream(fos)) {

            ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
            writer.setOutput(ios);

            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality); // Kompresi gambar ke 30% kualitas

            writer.write(null, new IIOImage(image, null, null), param);
            writer.dispose();
        }

        return compressedImageFile;
    }
}
