package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDosenDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiEvidanceDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiEvidanceDosenDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerClassKpiDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.PositionKpiDao;
import id.ac.tazkia.aplikasihr.dto.kpi.EditDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiLecturerDto;
import id.ac.tazkia.aplikasihr.dto.kpi.SaveKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidanceDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class KpiLecturerService {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private LecturerClassKpiDao lecturerClassKpiDao;

    @Autowired
    private PengisianKpiDosenDao pengisianKpiDosenDao;

    @Autowired
    private PengisianKpiEvidanceDosenDao pengisianKpiEvidanceDosenDao;

    @Autowired
    private KpiService kpiService;

    @Autowired
    private ResizeFIleService resizeFIleService;


    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;

    public Page<ListKpiLecturerDto> getKpiListLecturer(Lecturer lecturer, String bulan, String tahun, Pageable pageable) {
        List<ListKpiLecturerDto> listKpiLecturerDtos = new ArrayList<>();
        Page<PengisianKpiDosen> pengisianKpiDosenList = pengisianKpiDosenDao
                .findByStatusAndStatusKpiAndLecturerAndBulanAndTahunOrderByTanggal(
                        StatusRecord.AKTIF, StatusRecord.OPEN, lecturer, bulan, tahun, pageable);
        for (PengisianKpiDosen data : pengisianKpiDosenList){
            ListKpiLecturerDto listKpiLecturerDto = new ListKpiLecturerDto();
            List<PengisianKpiEvidanceDosen> pengisianKpiEvidanceDosens = pengisianKpiEvidanceDosenDao.findByPengisianKpiDosenAndStatus(data, StatusRecord.AKTIF);

            listKpiLecturerDto.setPengisianKpiDosen(data);
            listKpiLecturerDto.setPengisianKpiEvidanceDosenList (pengisianKpiEvidanceDosens);
            listKpiLecturerDtos.add(listKpiLecturerDto);
        }

        return new PageImpl<>(listKpiLecturerDtos, pageable, pengisianKpiDosenList.getTotalElements());
    }

    @Transactional
    public void savePengisianKpiLecturer(Authentication authentication, SaveKpiEmployeeDto saveKpiEmployeeDto) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);

        Optional<LecturerClassKpi> lecturerClassKpiOptional = lecturerClassKpiDao.findById(saveKpiEmployeeDto.getIdKpi());

        Integer bulan = saveKpiEmployeeDto.getTanggal().getMonthValue();
        Integer tahun = saveKpiEmployeeDto.getTanggal().getYear();
        if(saveKpiEmployeeDto.getTanggal().getDayOfMonth() > 20){
            if(saveKpiEmployeeDto.getTanggal().getMonthValue() == 12){
                bulan = 1;
                tahun = saveKpiEmployeeDto.getTanggal().getYear() + 1;
            }else{
                bulan = saveKpiEmployeeDto.getTanggal().getMonthValue() + 1;
            }
        }

        String tahunString = tahun.toString();
        String monthString = bulan.toString();


        if (monthString.length() > 1) {
            monthString = bulan.toString();
        } else {
            monthString = "0" + bulan.toString();
        }


        PengisianKpiDosen pengisianKpiDosen = new PengisianKpiDosen();
        pengisianKpiDosen.setLecturerClassKpi(lecturerClassKpiOptional.get());
        pengisianKpiDosen.setTanggal(saveKpiEmployeeDto.getTanggal());
        pengisianKpiDosen.setTanggalInput(LocalDateTime.now());
        pengisianKpiDosen.setLecturer(lecturer);
        pengisianKpiDosen.setDeskripsi(saveKpiEmployeeDto.getDeskripsi());
        pengisianKpiDosen.setTanggalPenugasan(saveKpiEmployeeDto.getTanggalPenugasan());
        pengisianKpiDosen.setDeskripsiPenugasan(saveKpiEmployeeDto.getDeskripsiPenugasan());
        pengisianKpiDosen.setBulan(monthString);
        pengisianKpiDosen.setTahun(tahunString);
        pengisianKpiDosenDao.save(pengisianKpiDosen);

        for (int i = 0; i < saveKpiEmployeeDto.getFiles().length; i++) {
            PengisianKpiEvidanceDosen pengisianKpiEvidanceDosen = new PengisianKpiEvidanceDosen();
            pengisianKpiEvidanceDosen.setPengisianKpiDosen(pengisianKpiDosen);
            pengisianKpiEvidanceDosen.setNamaFile(saveKpiEmployeeDto.getFiles()[i].getOriginalFilename());

            String namaAsli = saveKpiEmployeeDto.getFiles()[i].getOriginalFilename();
            String extension = "";

            int j = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (j > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile1 = UUID.randomUUID().toString();
            new File(uploadKinerja).mkdirs();
            File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
            saveKpiEmployeeDto.getFiles()[i].transferTo(tujuan2);

            if (saveKpiEmployeeDto.getFiles()[i].getContentType().equalsIgnoreCase("application/pdf")) {
                resizeFIleService.resizeFilePdf(tujuan2);
            } else {
                // Resize file image jika bukan PDF
                resizeFIleService.resizeFileImage(tujuan2);
            }

            pengisianKpiEvidanceDosen.setFile(idFile1 + "." + extension);
            pengisianKpiEvidanceDosen.setUserUpload(user.getUsername());
            pengisianKpiEvidanceDosen.setDateUpload(LocalDateTime.now());
            pengisianKpiEvidanceDosenDao.save(pengisianKpiEvidanceDosen);
        }
    }


    @Transactional
    public void editKpiDosen(EditDto editDto) throws IOException {
        PengisianKpiDosen pengisianKpiDosen = pengisianKpiDosenDao.findById(editDto.getId()).get();
        pengisianKpiDosen.setTanggal(editDto.getTanggal());
        pengisianKpiDosen.setDeskripsi(editDto.getDeskripsi());
        pengisianKpiDosen.setTanggalPenugasan(editDto.getTanggalPenugasan());
        pengisianKpiDosen.setDeskripsiPenugasan(editDto.getDeskripsiPenugasan());
        pengisianKpiDosenDao.save(pengisianKpiDosen);
        for (int i = 0; i < editDto.getFiles().length && i < editDto.getIdEvidance().length; i++) {
            Optional<PengisianKpiEvidanceDosen> pengisianKpiEvidanceDosenOptional = pengisianKpiEvidanceDosenDao.findById(editDto.getIdEvidance()[i]);

            if (pengisianKpiEvidanceDosenOptional.isPresent()) {
                PengisianKpiEvidanceDosen updatePengisianKpiEvidanceDosen = pengisianKpiEvidanceDosenOptional.get();

                if (editDto.getFiles()[i] != null && !editDto.getFiles()[i].isEmpty()) {
                    String filename = editDto.getFiles()[i].getOriginalFilename();
                    String extension = "";

                    int j = filename.lastIndexOf('.');
                    int p = Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
                    if (j > p) {
                        extension = filename.substring(j + 1);
                    }

                    String idFile1 = UUID.randomUUID().toString();
                    new File(uploadKinerja).mkdirs();
                    File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);

                    editDto.getFiles()[i].transferTo(tujuan2);

                    updatePengisianKpiEvidanceDosen.setNamaFile(filename);
                    updatePengisianKpiEvidanceDosen.setFile(idFile1 + "." + extension);
                } else {
                    updatePengisianKpiEvidanceDosen.setNamaFile(pengisianKpiEvidanceDosenOptional.get().getNamaFile());
                    updatePengisianKpiEvidanceDosen.setFile(pengisianKpiEvidanceDosenOptional.get().getFile());
                }

                // Simpan objek update ke database
                pengisianKpiEvidanceDosenDao.save(updatePengisianKpiEvidanceDosen);
            } else {
                System.out.println("Evidance ID " + editDto.getIdEvidance()[i] + " tidak ditemukan.");
            }
        }

    }

    @Transactional
    public void deleteListLectirerKpi(String id) {
        pengisianKpiDosenDao.hapusKpi(id);
    }

}
