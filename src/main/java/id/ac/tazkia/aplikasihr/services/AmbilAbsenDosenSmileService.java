package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceLecturerDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceLecturerImporProcessDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceLecturerImporProcessDetailDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.ImporAbsenDosenDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturer;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcessDetail;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.WebClient;

import java.beans.Transient;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@EnableScheduling

public class AmbilAbsenDosenSmileService {

    @Autowired
    private AttendanceLecturerImporProcessDao attendanceLecturerImporProcessDao;

    @Autowired
    private AttendanceLecturerDao attendanceLecturerDao;

    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private AttendanceLecturerImporProcessDetailDao attendanceLecturerImporProcessDetailDao;

    @Autowired
    @Value("${alamat.akademik}")
    private String alamat;

    WebClient webClient1 = WebClient.builder()
            .baseUrl(alamat)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();
    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private CompaniesDao companiesDao;
    @Autowired
    private LecturerClassDao lecturerClassDao;
    @Autowired
    private LecturerSalaryDao lecturerSalaryDao;
    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private ProdiDao prodiDao;

    public List<ImporAbsenDosenDto> getAbsenDosen(@RequestParam String tahun, @RequestParam String bulan) {
        return webClient1.get()
                .uri(alamat.toString()+"/api/rekap-absen-dosen?tahun="+ tahun + "&bulan="+ bulan)
                .retrieve().bodyToFlux(ImporAbsenDosenDto.class)
                .collectList()
                .block();
    }

    @Scheduled(fixedDelay = 1000)
    public void amblilDataProsesAttencanceLecturer(){

        AttendanceLecturerImporProcess attendanceLecturerImporProcess = attendanceLecturerImporProcessDao.findFirstByStatusOrderByTanggalInputAsc(StatusRecord.WAITING);
        if (attendanceLecturerImporProcess != null){
            attendanceLecturerImporProcess.setStatus(StatusRecord.ON_PROCESS);
            attendanceLecturerImporProcess.setTanggalMulai(LocalDateTime.now());
            attendanceLecturerImporProcessDao.save(attendanceLecturerImporProcess);
            runAttencanceLecturer(attendanceLecturerImporProcess.getCompanies(), attendanceLecturerImporProcess.getTahun(), attendanceLecturerImporProcess.getBulan(), attendanceLecturerImporProcess.getUserInput(), attendanceLecturerImporProcess);
        }

    }

    @Transactional
    public void runAttencanceLecturer(Companies companies, String tahun, String bulan, String user, AttendanceLecturerImporProcess attendanceLecturerImporProcess){

        attendanceLecturerDao.hapusAbsenDosenDiBulanYangSama(bulan,tahun, companies.getId());

        List<ImporAbsenDosenDto> imporAbsenDosenDtos = getAbsenDosen(tahun, bulan);
        for(ImporAbsenDosenDto imporAbsenDosenDto : imporAbsenDosenDtos){

            System.out.println("dosen :" +imporAbsenDosenDto.getNama());

            Lecturer lecturer = lecturerDao.findByStatusAndEmail(StatusRecord.AKTIF, imporAbsenDosenDto.getEmail());

            if(lecturer == null) {
                Employes employes = employesDao.findByEmailActiveAndStatus(imporAbsenDosenDto.getEmail(), StatusRecord.AKTIF);

                if(employes == null) {
                    Employes employes1 = new Employes();
                    String idFile = UUID.randomUUID().toString();
                    employes1.setCompanies(companies);
                    employes1.setStatus(StatusRecord.AKTIF);
                    employes1.setStatusAktif("AKTIF");
                    employes1.setEmailActive(imporAbsenDosenDto.getEmail());
                    employes1.setFullName(imporAbsenDosenDto.getNama());
//                    employes.setId(idFile);
                    employesDao.saveAndFlush(employes1);
                    System.out.println("Id Employee : " + employes1.getId());
                    employes = employes1;
                }

                Lecturer lecturer1 = new Lecturer();
//                lecturer1.setId(UUID.randomUUID().toString());
                lecturer1.setCompanies(companies);
                lecturer1.setEmail(imporAbsenDosenDto.getEmail());
                lecturer1.setEmployes(employes);
                lecturer1.setStatus(StatusRecord.AKTIF);
                lecturer1.setLecturerClass(lecturerClassDao.findByStatusAndId(StatusRecord.AKTIF, "LB" ));
                lecturer1.setUserInsert("IMPOR");
                lecturer1.setDateInsert(LocalDate.now());
                lecturer1.setProdi(prodiDao.findById(imporAbsenDosenDto.getProdi()).get());
                lecturerDao.save(lecturer1);

                AttendanceLecturer attendanceLecturer = new AttendanceLecturer();
                attendanceLecturer.setHari(imporAbsenDosenDto.getHari());
                attendanceLecturer.setEmail(imporAbsenDosenDto.getEmail());
                attendanceLecturer.setAttendanceLecturerImporProcess(attendanceLecturerImporProcess);
                attendanceLecturer.setLecturer(lecturer1);
                attendanceLecturer.setBulanDate(bulan);
                attendanceLecturer.setIdJadwal(imporAbsenDosenDto.getJadwal());
                attendanceLecturer.setKelas(imporAbsenDosenDto.getKelas());
                attendanceLecturer.setMatkul(imporAbsenDosenDto.getMatkul());
                attendanceLecturer.setBulan(imporAbsenDosenDto.getBulan());
                attendanceLecturer.setSks(new BigDecimal(imporAbsenDosenDto.getSks()));
                attendanceLecturer.setNama(imporAbsenDosenDto.getNama());
                attendanceLecturer.setTahun(tahun);
                attendanceLecturer.setTanggal(imporAbsenDosenDto.getTanggal());
                attendanceLecturer.setDosen(imporAbsenDosenDto.getDosen());
                attendanceLecturer.setTanggalRun(LocalDateTime.now());
                attendanceLecturer.setUserRun(attendanceLecturerImporProcess.getUserInput());
                attendanceLecturer.setTotalSesi(new BigDecimal(imporAbsenDosenDto.getTotalsesi()));
                attendanceLecturer.setTotalSks(new BigDecimal(imporAbsenDosenDto.getTotalsks()));
                attendanceLecturer.setTotalSeluruhSks(new BigDecimal(imporAbsenDosenDto.getTotalseluruhsks()));
                attendanceLecturer.setTotalSeluruhSesi(new BigDecimal(imporAbsenDosenDto.getTotalseluruhsesi()));
                attendanceLecturer.setCompanies(companies);
                attendanceLecturer.setStatus(StatusRecord.AKTIF);
                attendanceLecturer.setJenjang(imporAbsenDosenDto.getJenjang());
                attendanceLecturer.setIdProdi(imporAbsenDosenDto.getIdprodi());
                attendanceLecturer.setProdi(imporAbsenDosenDto.getProdi());
                attendanceLecturer.setIdJenjang(imporAbsenDosenDto.getIdJenjang());
                attendanceLecturerDao.save(attendanceLecturer);

                AttendanceLecturerImporProcessDetail attendanceLecturerImporProcessDetail = new AttendanceLecturerImporProcessDetail();
                attendanceLecturerImporProcessDetail.setAttendanceLecturerImporProcess(attendanceLecturerImporProcess);
                attendanceLecturerImporProcessDetail.setTopik(imporAbsenDosenDto.getNama());
                attendanceLecturerImporProcessDetail.setDescription("Attendance Imported and added");
                attendanceLecturerImporProcessDetail.setStatus(StatusRecord.DONE);
                attendanceLecturerImporProcessDetailDao.save(attendanceLecturerImporProcessDetail);

            } else {
                AttendanceLecturer attendanceLecturer = new AttendanceLecturer();
                attendanceLecturer.setHari(imporAbsenDosenDto.getHari());
                attendanceLecturer.setEmail(imporAbsenDosenDto.getEmail());
                attendanceLecturer.setAttendanceLecturerImporProcess(attendanceLecturerImporProcess);
                attendanceLecturer.setLecturer(lecturer);
                attendanceLecturer.setBulanDate(bulan);
                attendanceLecturer.setIdJadwal(imporAbsenDosenDto.getJadwal());
                attendanceLecturer.setKelas(imporAbsenDosenDto.getKelas());
                attendanceLecturer.setMatkul(imporAbsenDosenDto.getMatkul());
                attendanceLecturer.setBulan(imporAbsenDosenDto.getBulan());
                attendanceLecturer.setSks(new BigDecimal(imporAbsenDosenDto.getSks()));
                attendanceLecturer.setNama(imporAbsenDosenDto.getNama());
                attendanceLecturer.setTahun(tahun);
                attendanceLecturer.setTanggal(imporAbsenDosenDto.getTanggal());
                attendanceLecturer.setDosen(imporAbsenDosenDto.getDosen());
                attendanceLecturer.setTanggalRun(LocalDateTime.now());
                attendanceLecturer.setUserRun(attendanceLecturerImporProcess.getUserInput());
                attendanceLecturer.setTotalSesi(new BigDecimal(imporAbsenDosenDto.getTotalsesi()));
                attendanceLecturer.setTotalSks(new BigDecimal(imporAbsenDosenDto.getTotalsks()));
                attendanceLecturer.setTotalSeluruhSks(new BigDecimal(imporAbsenDosenDto.getTotalseluruhsks()));
                attendanceLecturer.setTotalSeluruhSesi(new BigDecimal(imporAbsenDosenDto.getTotalseluruhsesi()));
                attendanceLecturer.setCompanies(companies);
                attendanceLecturer.setStatus(StatusRecord.AKTIF);
                attendanceLecturer.setJenjang(imporAbsenDosenDto.getJenjang());
                attendanceLecturer.setIdProdi(imporAbsenDosenDto.getIdprodi());
                attendanceLecturer.setProdi(imporAbsenDosenDto.getProdi());
                attendanceLecturer.setIdJenjang(imporAbsenDosenDto.getIdJenjang());
                attendanceLecturerDao.save(attendanceLecturer);

                AttendanceLecturerImporProcessDetail attendanceLecturerImporProcessDetail = new AttendanceLecturerImporProcessDetail();
                attendanceLecturerImporProcessDetail.setAttendanceLecturerImporProcess(attendanceLecturerImporProcess);
                attendanceLecturerImporProcessDetail.setTopik(imporAbsenDosenDto.getNama());
                attendanceLecturerImporProcessDetail.setDescription("Attendance Imported");
                attendanceLecturerImporProcessDetail.setStatus(StatusRecord.DONE);
                attendanceLecturerImporProcessDetailDao.save(attendanceLecturerImporProcessDetail);

            }

        }

        attendanceLecturerImporProcess.setStatus(StatusRecord.DONE);
        attendanceLecturerImporProcess.setTanggalSelesai(LocalDateTime.now());
        attendanceLecturerImporProcessDao.save(attendanceLecturerImporProcess);

    }

}
