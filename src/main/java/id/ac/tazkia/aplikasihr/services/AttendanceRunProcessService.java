package id.ac.tazkia.aplikasihr.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import id.ac.tazkia.aplikasihr.dto.employes.EmployeeAllDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunProcessDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunProcessDetailDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.OverTimeRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.attendance.RunAttendanceDto;
import id.ac.tazkia.aplikasihr.dto.request.TarikOvertimeDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcessDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.EmployeeSchedule;

@Service
@EnableScheduling
public class AttendanceRunProcessService {

    @Autowired
    private AttendanceRunProcessDao attendanceRunProcessDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private OverTimeRequestDao overTimeRequestDao;

    @Autowired
    private AttendanceRunProcessDetailDao attendanceRunProcessDetailDao;

    @Scheduled(fixedDelay = 1000)
    public void amblilDataProsesAttencance(){

        AttendanceRunProcess attendanceRunProcess = attendanceRunProcessDao.findFirstByStatusOrderByTanggalInputAsc(StatusRecord.WAITING);
        if (attendanceRunProcess != null){
            attendanceRunProcess.setStatus(StatusRecord.ON_PROCESS);
            attendanceRunProcess.setTanggalMulai(LocalDateTime.now());
            attendanceRunProcessDao.save(attendanceRunProcess);
            runAttencance(attendanceRunProcess.getCompanies().getId(), attendanceRunProcess.getTahun(), attendanceRunProcess.getBulan(), attendanceRunProcess.getUserInput(), attendanceRunProcess);
        }

    }

    public void runAttencance(String companies, String tahun, String bulan, String user, AttendanceRunProcess attendanceRunProcess){

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        if(companies != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            List<EmployeeAllDto> employesList = employesDao.allEmployes(localDate, localDate2);
            if (companies.equals("All")) {
                employesList = employesList;
            } else {
                employesList = employesDao.companyEmployes(localDate, localDate2, companies);
            }

            for (EmployeeAllDto e : employesList) {

                EmployeeSchedule employeeSchedule = employeeScheduleDao.cariScheduleEmployee(e.getId(), e.getTanggalSelesai());

                if (employeeSchedule == null){

                    AttendanceRunProcessDetail attendanceRunProcessDetail = new AttendanceRunProcessDetail();
                    attendanceRunProcessDetail.setStatus(StatusRecord.ERROR);
                    attendanceRunProcessDetail.setDescription("Employee Schedule Not found");
                    attendanceRunProcessDetail.setAttendanceRunProcess(attendanceRunProcess);
                    attendanceRunProcessDetail.setEmployes(employesDao.findById(e.getId()).get());
                    attendanceRunProcessDetailDao.save(attendanceRunProcessDetail);

                } else {

                    Integer attendanceOk = 0;
                    Integer absenOk = 0;
                    Integer jumlahHari = 0;
                    Integer cutiOk = 0;
                    Integer jumlahJamShiftFlexible = 0;
                    Integer jumlahJamAttendanceFlexible = 0;
                    Integer terlambat = 0;
                    Integer overtime = 0;
                    Integer overtimeDayOff = 0;
                    Integer overtimeWorkingDayFirstHour = 0;
                    Integer overtimeWorkingDayNextHour = 0;
                    Integer overtimeHolidayFirstHour = 0;
                    Integer overtimeHolidaySecondHour = 0;
                    Integer overtimeHolidayNextHour = 0;

                    System.out.println("Employee :" + e.getId());
                    List<RunAttendanceDto> runAttendanceDtos = attendanceDao.listRunAttendance(e.getTanggalMulai(), e.getTanggalSelesai(), e.getId());

                    AttendanceRun attendanceRun = attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, e.getId());
                    if (attendanceRun == null) {
                        attendanceRun = new AttendanceRun();
                    }

                    for (RunAttendanceDto runAttendanceDto : runAttendanceDtos) {
//                        String shiftIn = runAttendanceDto.getTanggal().toString() + ' ' + runAttendanceDto.getShiftIn().toString() + ":00";

                        String shiftIn = runAttendanceDto.getShiftIn();
//                    if(runAttendanceDto.getShiftIn().getHour() > runAttendanceDto.getShiftOut().getHour()){
//
//                    }
                        LocalDate tanggal1 = runAttendanceDto.getTanggal().plusDays(1);
//                        String shiftOut = runAttendanceDto.getTanggal().toString() + ' ' + runAttendanceDto.getShiftOut().toString() + ":00";

                        String shiftOut = runAttendanceDto.getShiftOut();
                        DateTimeFormatter formatterShift = DateTimeFormatter.ofPattern("yyyy-MM-dd [HH][:mm][:ss]");

                        LocalDateTime shitInDateTime = LocalDateTime.parse(shiftIn, formatterShift);
                        LocalDateTime shitOutDateTime = LocalDateTime.parse(shiftOut, formatterShift);

//                    if (runAttendanceDto.getShiftIn().isAfter(runAttendanceDto.getShiftOut())){
//                         localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
//
//                         localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
//                    }else {
//                         localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
//
//                         localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
//                    }

                        String nol = " 00:00:00";
                        String attendanceIn = "-";
                        String attendanceOut = "-";

                        LocalDateTime localAttendanceIn = LocalDateTime.parse("1945-08-17 00:00:00", formatterShift);
                        LocalDateTime localAttendanceOut = LocalDateTime.parse("1945-08-17 00:00:00", formatterShift);

                        if (runAttendanceDto.getWaktuMasuk() != null) {
                            if (runAttendanceDto.getWaktuKeluar() != null) {
                                attendanceIn = runAttendanceDto.getWaktuMasuk();
                                localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
                                attendanceOut = runAttendanceDto.getWaktuKeluar();
                                localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
                            } else {
                                attendanceIn = runAttendanceDto.getWaktuMasuk();
                                localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
                                localAttendanceOut = LocalDateTime.parse(attendanceIn, formatterShift);
                                localAttendanceOut = localAttendanceOut.plusHours(1);
                                //jika absen dihitung tidak late
//                                localAttendanceOut = LocalDateTime.parse(shiftOut, formatterShift);
                            }
                        } else {
                            if (runAttendanceDto.getWaktuKeluar() != null) {
                                //jika absen dihitung tidak late
//                                localAttendanceIn = LocalDateTime.parse(shiftIn, formatterShift);
                                attendanceOut = runAttendanceDto.getWaktuKeluar();
                                localAttendanceIn = LocalDateTime.parse(attendanceOut, formatterShift);
                                localAttendanceIn = localAttendanceIn.plusHours(1);
                                localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
                            }
                        }
//                    LocalDateTime localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
//
                        if (shitInDateTime.isAfter(shitOutDateTime)) {
                            attendanceOut = shiftOut;
                        }
//                    if (runAttendanceDto.getWaktuKeluar() != null){
//                        attendanceOut = runAttendanceDto.getWaktuKeluar();
//                        localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
//                    }
//                    LocalDateTime localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);

                        if (shitInDateTime.isAfter(shitOutDateTime)) {
                            shiftIn = shiftIn;
                            shiftOut = shiftOut;
                        }

                        LocalDateTime localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
                        LocalDateTime localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
                        if (localAttendanceIn.getHour() > localAttendanceOut.getHour()) {
                            localAttendanceIn = localAttendanceIn;
                            localAttendanceOut = localAttendanceOut.plusDays(1);
                        }

                        Long duration = ChronoUnit.MINUTES.between(localShiftIn, localShiftOut);
                        Long duration1 = ChronoUnit.MINUTES.between(localAttendanceIn, localAttendanceOut);
//                    if(attendanceIn.equals("-") && attendanceOut.equals("-")){
//                        duration1 = ChronoUnit.MINUTES.between(localAttendanceIn, localAttendanceOut);
//                    }

//                    Integer intDuration = new Integer(String.valueOf(duration.toMinutes()));
//                    Integer intDuration2 = new Integer(String.valueOf(duration1.toMinutes()));
////                    long minutes = ChronoUnit.MINUTES.between(from, to);
//                    System.out.println("Durasi :" + duration.toHours());
//                    System.out.println("Durasi :" + duration.toMinutes());

                        if (runAttendanceDto.getStat().equals("ON") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                            jumlahHari = jumlahHari + 1;
                        }

//                    String ada = runAttendanceDto.getWaktuMasuk();
//                    LocalDateTime localAttendanceOuta = LocalDateTime.parse(ada, formatterShift);

                        System.out.println("ShiftMasuk :" + localShiftIn);
                        System.out.println("ShiftKeluar :" + localShiftOut);

                        System.out.println("masuk :" + localAttendanceIn);
                        System.out.println("keluar :" + localAttendanceOut);

                        if (runAttendanceDto.getAttd().equals("AKTIF")) {
                            if (runAttendanceDto.getFlexible().equals("AKTIF")) {
//                            jumlahJamShiftFlexible = jumlahJamShiftFlexible + intDuration;
//                            jumlahJamAttendanceFlexible = jumlahJamAttendanceFlexible + intDuration2;
                                if (runAttendanceDto.getStat().equals("ON") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                    if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
                                        if (duration1 > 0) {
                                            if (duration1 >= duration) {
                                                attendanceOk = attendanceOk + 1;
                                            } else {
                                                terlambat = terlambat + 1;
                                                attendanceOk = attendanceOk + 1;
                                            }
                                        } else {
                                            // absenOk = absenOk + 1;
                                        
                                        }
                                    }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() == null && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                        absenOk = absenOk + 1;
                                    }else{
                                        terlambat = terlambat + 1;
                                        attendanceOk = attendanceOk + 1;
                                    }
                                } else if (runAttendanceDto.getStat().equals("CUTI") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                    cutiOk = cutiOk + 1;
                                } else if (runAttendanceDto.getStat().equals("AKTIF") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                    attendanceOk = attendanceOk + 1;
                                }
                            } else {
                                if (runAttendanceDto.getStat().equals("ON") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                    if (runAttendanceDto.getTanggal().isAfter(LocalDate.parse("2024-12-08"))) {
                                        if (duration1 > 0) {
                                            Long selisihMasuk = ChronoUnit.MINUTES.between(localShiftIn, localAttendanceIn);
                                            Long selisihKeluar = ChronoUnit.MINUTES.between(localShiftOut, localAttendanceOut);

                                            System.out.println("masuk :" + selisihMasuk);
                                            System.out.println("keluar :" + selisihKeluar);

//                                    String ada = runAttendanceDto.getWaktuMasuk().toString();
//                                    LocalDateTime localAttendanceOuta = LocalDateTime.parse(ada, formatterShift);
                                            if (selisihMasuk > 0) {
                                                if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else{
                                                    absenOk = absenOk + 1;
                                                }
                                                //lanjut sampai sini
                                            } else {
                                                if (selisihKeluar < 0) {
//                                                terlambat = terlambat + 1;
//                                                attendanceOk = attendanceOk + 1;
                                                    if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else{
                                                        absenOk = absenOk + 1;
                                                    }
                                                } else {
//                                                attendanceOk = attendanceOk + 1;
                                                    if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
//                                                    terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else{
                                                        absenOk = absenOk + 1;
                                                    }
                                                }
                                            }
                                        } else {
                                            // absenOk = absenOk + 1;
                                        }
                                    } else {
                                        if (duration1 > 15) {
                                            Long selisihMasuk = ChronoUnit.MINUTES.between(localShiftIn, localAttendanceIn);
                                            Long selisihKeluar = ChronoUnit.MINUTES.between(localShiftOut, localAttendanceOut);

                                            System.out.println("masuk :" + selisihMasuk);
                                            System.out.println("keluar :" + selisihKeluar);

//                                    String ada = runAttendanceDto.getWaktuMasuk().toString();
//                                    LocalDateTime localAttendanceOuta = LocalDateTime.parse(ada, formatterShift);
                                            if (selisihMasuk > 0) {
                                                if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                    terlambat = terlambat + 1;
                                                    attendanceOk = attendanceOk + 1;
                                                }else{
                                                    absenOk = absenOk + 1;
                                                }
                                                //lanjut sampai sini
                                            } else {
                                                if (selisihKeluar < 0) {
//                                                terlambat = terlambat + 1;
//                                                attendanceOk = attendanceOk + 1;
                                                    if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else{
                                                        absenOk = absenOk + 1;
                                                    }
                                                } else {
//                                                attendanceOk = attendanceOk + 1;
                                                    if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() != null) {
//                                                    terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() != null && runAttendanceDto.getWaktuKeluar() == null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else if(runAttendanceDto.getWaktuMasuk() == null && runAttendanceDto.getWaktuKeluar() != null) {
                                                        terlambat = terlambat + 1;
                                                        attendanceOk = attendanceOk + 1;
                                                    }else{
                                                        absenOk = absenOk + 1;
                                                    }
                                                }
                                            }
                                        } else {
                                            // absenOk = absenOk + 1;
                                        }
                                    }
                                } else if (runAttendanceDto.getStat().equals("CUTI") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                    cutiOk = cutiOk + 1;
                                } else if (runAttendanceDto.getStat().equals("AKTIF") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                attendanceOk = attendanceOk + 1;
                                }
                            }
                        } else {
                            if (runAttendanceDto.getStat().equals("ON") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                attendanceOk = jumlahHari;
                            } else if (runAttendanceDto.getStat().equals("CUTI") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                cutiOk = cutiOk + 1;
                            } else if (runAttendanceDto.getStat().equals("AKTIF") && runAttendanceDto.getWorkDayStatus().equals("ON")) {
                                attendanceOk = attendanceOk + 1;
                            }
                        }
                    }

                    //run overtime disini
                    //cari overtime working day
                    List<TarikOvertimeDto> tarikOvertimeDtoWorkingDay = overTimeRequestDao.tarikDataOvertimeEmployee(e.getId(), e.getTanggalMulai(), e.getTanggalSelesai(), "ON");
                    if(tarikOvertimeDtoWorkingDay != null){
                        for(TarikOvertimeDto tarikOvertimeDtoWd : tarikOvertimeDtoWorkingDay){
                            if(tarikOvertimeDtoWd.getJam() > 1){
                                overtimeWorkingDayFirstHour = overtimeWorkingDayFirstHour + 1;
                                overtimeWorkingDayNextHour = overtimeWorkingDayNextHour + (tarikOvertimeDtoWd.getJam() - 1);
                            }else{
                                overtimeWorkingDayFirstHour = overtimeWorkingDayFirstHour + tarikOvertimeDtoWd.getJam();
                            }
                        }
                    }
                    List<TarikOvertimeDto> tarikOvertimeDtoHoliday = overTimeRequestDao.tarikDataOvertimeEmployee(e.getId(), e.getTanggalMulai(), e.getTanggalSelesai(), "OFF");
                    if(tarikOvertimeDtoHoliday != null){
                        for(TarikOvertimeDto tarikOvertimeDtoHd : tarikOvertimeDtoHoliday){
                            if(tarikOvertimeDtoHd.getJam() >= 1){
                                if(tarikOvertimeDtoHd.getJam() >= 2){
                                    overtimeHolidayFirstHour = overtimeHolidayFirstHour + 1;
                                    overtimeHolidaySecondHour = overtimeHolidaySecondHour + 1;
                                    overtimeHolidayNextHour = overtimeHolidayNextHour + (tarikOvertimeDtoHd.getJam() - 2);
                                }else{
                                    overtimeHolidayFirstHour = overtimeHolidayFirstHour + 1;
                                    overtimeHolidaySecondHour = overtimeHolidaySecondHour + 1;
                                }
                            }else{
                                overtimeHolidayFirstHour = overtimeHolidayFirstHour + tarikOvertimeDtoHd.getJam();
                            }
                        }
                    }

                    attendanceRun.setBulan(bulan);
                    attendanceRun.setTahun(tahun);
                    attendanceRun.setIdEmployee(e.getId());
                    attendanceRun.setTotalAttendance(attendanceOk);
                    attendanceRun.setTotalAbsen(absenOk);
                    attendanceRun.setTotalLate(terlambat);
                    attendanceRun.setTotalTimeOff(cutiOk);
                    attendanceRun.setTotalShift(jumlahHari);
                    attendanceRun.setStatus(StatusRecord.AKTIF);
                    attendanceRun.setUserUpdate(user);
                    attendanceRun.setDateUpdate(LocalDateTime.now());
                    if(overtimeWorkingDayFirstHour < 0){
                        attendanceRun.setOvertimeWorkingDayFirstHour(overtimeWorkingDayFirstHour * -1);
                    }else{
                        attendanceRun.setOvertimeWorkingDayFirstHour(overtimeWorkingDayFirstHour);
                    }
                   if(overtimeWorkingDayNextHour < 0){
                       attendanceRun.setOvertimeWorkingDayNextHour(overtimeWorkingDayNextHour * -1);
                   }else{
                       attendanceRun.setOvertimeWorkingDayNextHour(overtimeWorkingDayNextHour);
                   }
                   if(overtimeHolidayFirstHour < 0){
                       attendanceRun.setOvertimeHolidayFirstHour(overtimeHolidayFirstHour * -1);
                   }else{
                       attendanceRun.setOvertimeHolidayFirstHour(overtimeHolidayFirstHour);
                   }
                    if(overtimeHolidaySecondHour < 0){
                        attendanceRun.setOvertimeHolidaySecondHour(overtimeHolidaySecondHour * -1);
                    }else{
                        attendanceRun.setOvertimeHolidaySecondHour(overtimeHolidaySecondHour);
                    }
                    if(overtimeHolidayNextHour < 0){
                        attendanceRun.setOvertimeHolidayNextHour(overtimeHolidayNextHour * -1);
                    }else{
                        attendanceRun.setOvertimeHolidayNextHour(overtimeHolidayNextHour);
                    }

                    attendanceRunDao.save(attendanceRun);

                    AttendanceRunProcessDetail attendanceRunProcessDetail = new AttendanceRunProcessDetail();
                    attendanceRunProcessDetail.setStatus(StatusRecord.DONE);
                    attendanceRunProcessDetail.setDescription("Attendance Successfull");
                    attendanceRunProcessDetail.setAttendanceRunProcess(attendanceRunProcess);
                    attendanceRunProcessDetail.setEmployes(employesDao.findById(e.getId()).get());
                    attendanceRunProcessDetailDao.save(attendanceRunProcessDetail);

                }

            }

        }

        attendanceRunProcess.setStatus(StatusRecord.DONE);
        attendanceRunProcess.setTanggalSelesai(LocalDateTime.now());
        attendanceRunProcessDao.save(attendanceRunProcess);

    }

}
