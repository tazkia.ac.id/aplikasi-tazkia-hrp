package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrRunDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrRunProcessDao;
import id.ac.tazkia.aplikasihr.dto.employes.EmployesAktifThrDto;
import id.ac.tazkia.aplikasihr.dto.payroll.BayarLoanDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HitungPajakDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollInfo;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunDetail;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcess;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRun;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRunProcess;
import id.ac.tazkia.aplikasihr.entity.transaction.EmployeeLoanBayar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
@Transactional
@EnableScheduling
public class ThrRunProcessService {

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private HitungPajakDao hitungPajakDao;

    @Autowired
    private ThrRunDao thrRunDao;

    @Autowired
    private ThrRunProcessDao thrRunProcessDao;

    @Scheduled(fixedDelay = 1500)
    public void amblilDataProsesThr(){

        ThrRunProcess thrRunProcess = thrRunProcessDao.findFirstByStatusOrderByTanggalInput(StatusRecord.WAITING);
        if(thrRunProcess != null){
            thrRunProcess.setTanggalMulai(LocalDateTime.now());
            thrRunProcess.setStatus(StatusRecord.ON_PROCESS);
            thrRunProcessDao.save(thrRunProcess);
            runThr(thrRunProcess.getCompanies().getId(), thrRunProcess.getTahun(), thrRunProcess.getBulan(), thrRunProcess);
        }

    }

    public void runThr(String companies, String tahun, String bulan, ThrRunProcess thrRunProcess){

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {

            //Mengubah tahun jadi string
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            //Mengubah bulan jadi string
            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            //jika bulan lebih dari 1 karakter
            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            }
            //jika bulan hanya 1 karakter harus diubah menjadi 2 karakter dengan ditambah 0 di depan nya
            else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        //Konversi bulan ke integer
        Integer bulanAngka = new Integer(bulan);
        //Mencari bulan sebelum nya
        Integer bulanAngka2 = bulanAngka - 1;
        //Mengubah bulan sebelum nya ke string
        String bulan2 = bulanAngka2.toString();

        //Konversi tahun ke string
        Integer tahunAngka = new Integer(tahun);
        //mencari tahun sebelum nya
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        //Mencari tanggal payroll periode aktif
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        //deklarasi tanggal mulai periode
        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }

        //deklarasi tanggal selesai periode
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        //tanggal mulai periode
        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        //tanggal selesai periode
        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        String tanggalGajian = tahun + '-' + bulan + '-' + tanggalMulaiPeriode;
        LocalDate localDate3 = LocalDate.parse(tanggalGajian, formatter1);

        if (companies != null && bulan != null && tahun != null) {
            //mencari lama bulan pegawai untuk menentukan prorate atau tidak

            List<String> employes1 = employesDao.cariEmployeeCompanyAktif(companies);

            if(employes1 != null) {
                thrRunDao.deleteThrRun(tahun, bulan, employes1);
            }

            List<EmployesAktifThrDto> employesAktifThrDtos = employeeStatusAktifDao.listKaryawanAktifThr(companies, localDate2);
            for (EmployesAktifThrDto e : employesAktifThrDtos) {

                Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId());

                BigDecimal totalGajiPokok = BigDecimal.ZERO;
                BigDecimal totalAllowance = BigDecimal.ZERO;
                BigDecimal totalKenaPajak = BigDecimal.ZERO;
                BigDecimal totalGrossGaji = BigDecimal.ZERO;
                BigDecimal totalPajak = BigDecimal.ZERO;

                BigDecimal totalGajiPokokThr = BigDecimal.ZERO;
                BigDecimal totalAllowanceThr = BigDecimal.ZERO;
                BigDecimal totalKenaPajakThr = BigDecimal.ZERO;
                BigDecimal totalThr = BigDecimal.ZERO;
                BigDecimal totalPajakThr = BigDecimal.ZERO;
                BigDecimal partSatu = BigDecimal.ZERO;
                BigDecimal partDua = BigDecimal.ZERO;

                System.out.println("Employee :" + e.getId());
                System.out.println("Nama :" + employes.getFullName());

                //Baru Sampai Sini
                List<EmployeePayrollComponentDto> employeePayrollComponents = employeePayrollComponentDao.listEmployeePayrollComponent(localDate, localDate2, e.getId());
                if(employeePayrollComponents != null){
                    totalAllowance = totalAllowance.add(employes.getTotalTunjanganHarian());
                    totalAllowanceThr = totalAllowanceThr.add(employes.getTotalTunjanganHarian());
                    totalKenaPajakThr = totalKenaPajakThr.add(employes.getTotalTunjanganHarian());
                    totalKenaPajak = totalKenaPajak.add(employes.getTotalTunjanganHarian());
                    for (EmployeePayrollComponentDto em : employeePayrollComponents) {

                        //Hitung Gaji
                        if (em.getJenisComponentMaster().equals("BASIC")) {
                                totalGajiPokok = totalGajiPokok.add(em.getNominal());
                                if (em.getTaxable().equals("AKTIF")) {
                                    totalKenaPajak = totalKenaPajak.add(em.getNominal());
                                }
                        }


                        //Allowance
                        if (em.getJenisComponentMaster().equals("ALLOWANCE")) {
                            if(em.getPerhitungan().equals("HARIAN")) {
                                totalAllowance = totalAllowance.add(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                if (em.getTaxable().equals("AKTIF")) {
                                    totalKenaPajak = totalKenaPajak.add(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                }
                            }else{
                                totalAllowance = totalAllowance.add(em.getNominal());
                                if (em.getTaxable().equals("AKTIF")) {
                                    totalKenaPajak = totalKenaPajak.add(em.getNominal());
                                }
                            }
                        }


                        //hitung THR
                        //Gaji Pokok
                        if (em.getJenisComponentMaster().equals("BASIC")) {
                            if(em.getThr() == StatusRecord.AKTIF) {
                                totalGajiPokokThr = totalGajiPokokThr.add(em.getNominal());
                                if (em.getTaxable().equals("AKTIF")) {
                                    totalKenaPajakThr = totalKenaPajakThr.add(em.getNominal());
                                }
                            }
                        }


                        //Allowance
                        if (em.getJenisComponentMaster().equals("ALLOWANCE")) {
                            if(em.getThr() == StatusRecord.AKTIF){
                                if(em.getPerhitungan().equals("HARIAN")) {
                                    totalAllowanceThr = totalAllowanceThr.add(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                    //Karena pajak thr hanya diambil dari gaji pokok, jadi pajak allowance thr dimatikan
                                    if (em.getThrPajak() == StatusRecord.AKTIF) {
                                        totalKenaPajakThr = totalKenaPajakThr.add(em.getNominal().multiply((em.getPersentase().multiply(new BigDecimal(em.getMaksimalHari()))).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP)));
                                    }
                                }else{
                                    totalAllowanceThr = totalAllowanceThr.add(em.getNominal());
                                    //Karena pajak thr hanya diambil dari gaji pokok, jadi pajak allowance thr dimatikan
                                    if (em.getThrPajak() == StatusRecord.AKTIF) {
                                        totalKenaPajakThr = totalKenaPajakThr.add(em.getNominal().multiply(em.getPersentase().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP)));
                                    }
                                }

                            }
                        }


                    }

                    totalGrossGaji = totalGajiPokok.add(totalAllowance);
                    if (e.getStatus().equals("NO")){
                        totalThr = ((totalGajiPokokThr.add(totalAllowanceThr)).divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP)).multiply(e.getBulan());
                    } else {
                        totalThr = totalGajiPokokThr.add(totalAllowanceThr);
                    }

                    System.out.println("func pajak");
                    EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                    if (employeePayrollInfo != null) {
                        System.out.println("total_thr : " + totalThr);
                        System.out.println("kena_pajak_thr : " + totalKenaPajakThr);
                        if (totalKenaPajakThr.compareTo(BigDecimal.ZERO) > 0) {

                            BigDecimal persentasePajak = employeePayrollInfoDao.cariRumusPajak(employeePayrollInfo.getPtkpStatus().getRumus(), totalKenaPajak);
                            BigDecimal persentasePajakThr = employeePayrollInfoDao.cariRumusPajak(employeePayrollInfo.getPtkpStatus().getRumus(), totalKenaPajakThr);
                            if (persentasePajakThr != null){
                                totalPajakThr = totalKenaPajakThr.multiply(persentasePajakThr).divide(BigDecimal.valueOf(100));
                            }
                            if (persentasePajak != null){
                                totalPajak = totalKenaPajak.multiply(persentasePajak).divide(BigDecimal.valueOf(100));
                            }

//                            totalPajak = totalKenaPajak.multiply(BigDecimal.valueOf(12));
//                            if (e.getStatus().equals("NO")){
//                                totalKenaPajakThr = e.getBulan().multiply(totalAllowanceThr.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP));
//                            }
//                            totalPajakThr = totalPajak.add(totalKenaPajakThr);
//
//                            BigDecimal pengurangJabtan = totalPajak.multiply(BigDecimal.valueOf(0.05));
//                            BigDecimal pengurangJabatanThr = totalPajakThr.multiply(BigDecimal.valueOf(0.05));
//
//                            if (pengurangJabtan.compareTo(BigDecimal.valueOf(6000000)) > 0) {
//                                pengurangJabtan = BigDecimal.valueOf(6000000);
//                            }
//                            if(pengurangJabatanThr.compareTo(BigDecimal.valueOf(6000000))>0) {
//                                pengurangJabatanThr = BigDecimal.valueOf(6000000);
//                            }
//
//                            totalPajak = totalPajak.subtract(pengurangJabtan);
//                            totalPajakThr = totalPajakThr.subtract(pengurangJabatanThr);
//                            System.out.println("kena_pajak : " + totalPajak);
//                            System.out.println("kena_pajak_thr : " + totalPajakThr);
////                    totalPajak = totalPajak.subtract(totalPajak.multiply(BigDecimal.valueOf(0.05)));
//
//                            totalPajak = totalPajak.subtract(employeePayrollInfo.getPtkpStatus().getNominal());
//                            totalPajakThr = totalPajakThr.subtract(employeePayrollInfo.getPtkpStatus().getNominal());
//
//                            System.out.println("total_pajak : " + totalPajak);
//                            System.out.println("total_pajak_thr : " + totalPajakThr);
//
//                            if (totalPajakThr.compareTo(BigDecimal.ZERO) > 0) {
//
//                                BigDecimal hitungPajak = BigDecimal.ZERO;
//                                BigDecimal pajakSetahun = BigDecimal.ZERO;
//                                BigDecimal pajak = BigDecimal.ZERO;
//                                BigDecimal pajakThr = BigDecimal.ZERO;
//                                BigDecimal hitungPajakThr = BigDecimal.ZERO;
//                                BigDecimal pajakSetahunThr = BigDecimal.ZERO;
//
//                                List<HitungPajakDto> hitungPajakDtoList = hitungPajakDao.listHitungPajak(totalPajak);
//                                for (HitungPajakDto hpd : hitungPajakDtoList) {
//                                    totalPajak = totalPajak.subtract(hpd.getSyaratDari());
//                                    hitungPajak = hpd.getPersentase().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
//                                    if (totalPajak.compareTo(hpd.getSyaratSampai()) > 0) {
//                                        pajakSetahun = hpd.getSyaratSampai();
//                                        pajakSetahun = pajakSetahun.multiply(hitungPajak);
//                                        pajak = pajak.add(pajakSetahun);
//                                    } else {
//                                        if (totalPajak.compareTo(BigDecimal.ZERO) < 0){
//                                            totalPajak = BigDecimal.ZERO;
//                                        }
//                                        pajakSetahun = totalPajak;
//                                        pajakSetahun = pajakSetahun.multiply(hitungPajak);
//                                        pajak = pajak.add(pajakSetahun);
//                                    }
//                                }
//
//                                List<HitungPajakDto> hitungPajakDtoThrList = hitungPajakDao.listHitungPajak(totalPajakThr);
//                                for (HitungPajakDto hpdthr : hitungPajakDtoThrList) {
//                                    totalPajakThr = totalPajakThr.subtract(hpdthr.getSyaratDari());
//                                    hitungPajakThr = hpdthr.getPersentase().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
//                                    if (totalPajakThr.compareTo(hpdthr.getSyaratSampai()) > 0) {
//                                        pajakSetahunThr = hpdthr.getSyaratSampai();
//                                        pajakSetahunThr = pajakSetahunThr.multiply(hitungPajakThr);
//                                        pajakThr = pajakThr.add(pajakSetahunThr);
//                                    } else {
//                                        if (totalPajakThr.compareTo(BigDecimal.ZERO) < 0){
//                                            totalPajakThr = BigDecimal.ZERO;
//                                        }
//                                        pajakSetahunThr = totalPajakThr;
//                                        pajakSetahunThr = pajakSetahunThr.multiply(hitungPajakThr);
//                                        pajakThr = pajakThr.add(pajakSetahunThr);
//                                    }
//                                }
//
//                                totalPajak = pajak;
//                                totalPajakThr = pajakThr;
//                                totalPajak = totalPajak.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP);
//                                totalPajakThr = totalPajakThr.subtract(pajak);
//
//                            } else {
//                                totalPajak = BigDecimal.ZERO;
//                                totalPajakThr = BigDecimal.ZERO;
//                            }
                        } else {
                            totalPajak = BigDecimal.ZERO;
                            totalPajakThr = BigDecimal.ZERO;
                        }

                    }

                    //Simpan data THR
                    ThrRun thrRun = new ThrRun();
                    thrRun.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                    thrRun.setBulan(bulan);
                    thrRun.setTahun(tahun);
                    thrRun.setTotalPajak(totalPajakThr);
                    thrRun.setTotalThr(totalThr);
                    thrRun.setThrBersih(totalThr.subtract(totalPajakThr));
                    thrRun.setDateUpdate(LocalDateTime.now());
                    thrRun.setUserUpdate(thrRunProcess.getUserInput());
                    if(totalPajakThr.compareTo(BigDecimal.ZERO) > 0) {
                        thrRun.setPartSatu(totalKenaPajakThr.subtract(totalPajakThr));
                        thrRun.setPartDua(totalThr.subtract(totalKenaPajakThr));
                    }else{
                        thrRun.setPartSatu(totalThr.subtract(totalPajakThr));
                        thrRun.setPartDua(BigDecimal.ZERO);
                    }

                    thrRun.setPaySlip(StatusRecord.NONAKTIF);
                    thrRunDao.save(thrRun);


                }

            }


        }

        thrRunProcess.setStatus(StatusRecord.DONE);
        thrRunProcess.setTanggalSelesai(LocalDateTime.now());
        thrRunProcessDao.save(thrRunProcess);


    }


}
