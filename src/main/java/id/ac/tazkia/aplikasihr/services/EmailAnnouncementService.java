package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.AnnouncementCompaniesDao;
import id.ac.tazkia.aplikasihr.dao.AnnouncementDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantScheduleDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.recruitment.ApplicantSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;

@Service
@EnableAsync
public class EmailAnnouncementService {

    @Autowired
    private AnnouncementDao announcementDao;

    @Autowired
    private AnnouncementCompaniesDao announcementCompaniesDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private ApplicantScheduleDao scheduleDao;

    @Autowired
    @Value("${gmail.account.username}")
    private String email;

    @Autowired
    @Value("${gmail.password}")
    private String password;


    @Async
    public void kirimEmailAnnouncement(String idAnnouncement, List<String> companies, String isi, String attachement) throws MessagingException, IOException {

        for(String lc : companies) {
            List<Employes> employesList = employesDao.findByStatusAndStatusAktifAndCompanies(StatusRecord.AKTIF, "AKTIF", companiesDao.findByStatusAndId(StatusRecord.AKTIF, lc));
            for (Employes employes1 : employesList) {

                Properties prop = new Properties();
                prop.put("mail.smtp.auth", true);
                prop.put("mail.smtp.starttls.enable", true);
                prop.put("mail.smtp.host", "smtp.gmail.com");
                prop.put("mail.smtp.port", "587");
//                prop.put("mail.smtp.starttls.required", true);
//                prop.put("mail.test-connection", true);
//                prop.put("properties.mail.smtp.ssl.enable", true);

//                spring.mail.host: smtp.gmail.com
//                spring.mail.port: 465
//                spring.mail.username: xxx@gmail.com
//                spring.mail.password: xxx
//                spring.mail.properties.mail.smtp.auth: true
//                spring.mail.properties.mail.smtp.starttls.enable: true
//                spring.mail.properties.mail.smtp.starttls.required: true
//                spring.mail.properties.mail.smtp.ssl.enable: true
//                spring.mail.test-connection: true

                Session session = Session.getInstance(prop, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(email, password);
                    }
                });

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(email));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(employes1.getEmailActive()));
                message.setSubject("HRIS TAZKIA Announcement");

                BodyPart messageBodyPart = new MimeBodyPart();
                String htmlText1 = isi;
                messageBodyPart.setContent(htmlText1, "text/html");

                if (StringUtils.hasText(attachement)) {
                        MimeBodyPart attachmentPart = new MimeBodyPart();
                        attachmentPart.attachFile(new File(attachement));
                        Multipart multipart = new MimeMultipart();
                        multipart.addBodyPart(messageBodyPart);
                        multipart.addBodyPart(attachmentPart);

                        message.setContent(multipart);
                        Transport.send(message);
                }else{
                    Multipart multipart = new MimeMultipart();
                    multipart.addBodyPart(messageBodyPart);

                    message.setContent(multipart);
                    Transport.send(message);
                }

//            gmailApiService.kirimEmail(
//                    "HR Announcement",
//                    employes1.getEmailActive(),
//                    "Announcement",
//                    announcement.getIsi());

            }
        }

    }

    @Scheduled(cron = "0 00 14 * * *", zone = "Asia/Jakarta")
    @Async
    public void kirimAnnouncementApplicant() throws MessagingException{

        List<Object[]> listEmail = scheduleDao.listSentEmail();

        for (Object[] list : listEmail){
            Properties prop = new Properties();
            prop.put("mail.smtp.auth", true);
            prop.put("mail.smtp.starttls.enable", true);
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");

            Session session = Session.getInstance(prop, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication(){
                    return new PasswordAuthentication(email, password);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(list[1].toString()));
            message.setSubject("HRIS TAZKIA Announcment");
            message.setText(list[2].toString());

            ApplicantSchedule findId = scheduleDao.findById(list[0].toString()).get();
            findId.setStatusKirimEmail(StatusRecord.SENT);
            scheduleDao.save(findId);

            Transport.send(message);
        }

    }

}
