package id.ac.tazkia.aplikasihr.services;


import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PenilaianKpiDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PenilaianKpiDosenDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDetailDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunProcessDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunProcessDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeTasksScheduleDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeAllDto;
import id.ac.tazkia.aplikasihr.dto.payroll.BayarLoanDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HitungPajakDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.TypeRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunDetail;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcess;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcessDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.transaction.EmployeeLoanBayar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@EnableScheduling
public class PayrollRunProcessService {

    @Autowired
    private PayrollRunProcessDao payrollRunProcessDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private EmployeeStatusDao employeeStatusDao;

    @Autowired
    private PayrollRunDetailDao payrollRunDetailDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private EmployeeTypeDao employeeTypeDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private HitungPajakDao hitungPajakDao;

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private EmployeeTasksScheduleDao employeeTasksScheduleDao;

    @Autowired
    private PayrollRunProcessDetailDao payrollRunProcessDetailDao;

    @Autowired
    private PenilaianKpiDosenDao penilaianKpiDosenDao;

    @Autowired
    private PenilaianKpiDao penilaianKpiDao;

    @Autowired
    private LecturerDao lecturerDao;


    @Scheduled(fixedDelay = 1000)
    public void amblilDataProsesPayroll(){

        PayrollRunProcess payrollRunProcess = payrollRunProcessDao.findFirstByStatusOrderByTanggalInput(StatusRecord.WAITING);
        if(payrollRunProcess != null){
            payrollRunProcess.setTanggalMulai(LocalDateTime.now());
            payrollRunProcess.setStatus(StatusRecord.ON_PROCESS);
            payrollRunProcessDao.save(payrollRunProcess);
            runPayrollProcess(payrollRunProcess.getCompanies().getId(), payrollRunProcess.getTahun(), payrollRunProcess.getBulan(), payrollRunProcess);
        }

    }

    public void runPayrollProcess(String companies, String tahun, String bulan, PayrollRunProcess payrollRunProcess){

        String adaBulan = bulan;
        String adaTahun = tahun;
        StatusRecord statusRecord = StatusRecord.DONE;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        String tanggalGajian = tahun + '-' + bulan + '-' + tanggalMulaiPeriode;
        LocalDate localDate3 = LocalDate.parse(tanggalGajian, formatter1);


        if (companies != null && bulan != null && tahun != null) {

            List<EmployeeAllDto> employesList = employesDao.allEmployes(localDate, localDate2);
            if (companies.equals("All")) {
                employesList = employesList;
            } else {
                employesList = employesDao.companyEmployes(localDate,localDate2, companies);
            }

            if(employesList.isEmpty()){
                statusRecord = StatusRecord.ERROR;
                payrollRunProcess.setDescription("Tidak ada data yang terdaftar sebagai staff");
            }


            for (EmployeeAllDto e : employesList) {

                Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId());

                BigDecimal totalGajiPokok = BigDecimal.ZERO;
                BigDecimal totalAllowance = BigDecimal.ZERO;
                BigDecimal totalDeductions = BigDecimal.ZERO;
                BigDecimal totalBenefits = BigDecimal.ZERO;
                BigDecimal totalZakat = BigDecimal.ZERO;
                BigDecimal totalInfaq = BigDecimal.ZERO;
                BigDecimal totalKenaPajak = BigDecimal.ZERO;
                BigDecimal totalPajak = BigDecimal.ZERO;
                BigDecimal totalPayroll = BigDecimal.ZERO;
                BigDecimal totalLoan = BigDecimal.ZERO;
                BigDecimal totalPotonganTerlambat = BigDecimal.ZERO;
                BigDecimal totalPotonganAbsen = BigDecimal.ZERO;
                BigDecimal pajakAsli = BigDecimal.ZERO;
                BigDecimal tidakKenaZakat = BigDecimal.ZERO;
                BigDecimal totalLembur = BigDecimal.ZERO;
                BigDecimal ketazkiaan = BigDecimal.ZERO;


                System.out.println("Employee :" + e.getId());
                System.out.println("Nama :" + employes.getFullName());

                String statusAktif = "A";
                List<Object> employeeStatus = employeeStatusAktifDao.listStatusEmployee(e.getId(), e.getTanggalMulai(), e.getTanggalSelesai());
                if (employeeStatus.isEmpty()){
                    statusAktif = "A";
                }else{
                    statusAktif = "B";
                }

                System.out.println("Tanggal Mulai :" + tanggalMulai);
                System.out.println("Tanggal Selesai :" + tanggalSelesai);
                System.out.println("Status :" + statusAktif);
                List<PayrollRunDetail> payrollRunDetails = payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahun(StatusRecord.AKTIF, employesDao.findByStatusAndId(StatusRecord.AKTIF,e.getId()), bulan, tahun);
                System.out.println("Payroll Run Details : "+ payrollRunDetails);
                if (payrollRunDetails != null){
                    for (PayrollRunDetail pd : payrollRunDetails){

                        pd.setStatus(StatusRecord.HAPUS);
                        payrollRunDetailDao.save(pd);

                    }
                }

//                List<EmployeePayrollComponent> employeePayrollComponents = employeePayrollComponentDao.findByStatusNotAndEmployesOrderByPayrollComponent(StatusRecord.HAPUS, employesDao.findByStatusAndId(StatusRecord.AKTIF, e));
                List<EmployeePayrollComponentDto> employeePayrollComponents = employeePayrollComponentDao.listEmployeePayrollComponent(e.getTanggalMulai(), e.getTanggalSelesai(), e.getId());
                System.out.println("Components : " + employeePayrollComponents);
                AttendanceRun attendanceRun = attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, e.getId());
                if(attendanceRun == null){
                    PayrollRunProcessDetail payrollRunProcessDetail = new PayrollRunProcessDetail();
                    payrollRunProcessDetail.setStatusRun(StatusRecord.ERROR);
                    payrollRunProcessDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,e.getId()));
                    payrollRunProcessDetail.setDescription("Tidak ada data absensi.");
                    payrollRunProcessDetail.setPayrollRunProcess(payrollRunProcess);
                    payrollRunProcessDetail.setStatus(StatusRecord.AKTIF);
                    payrollRunProcessDetailDao.save(payrollRunProcessDetail);
                    statusRecord = statusRecord.ERROR;
                    payrollRunProcess.setDescription(payrollRunProcess.getDescription() + ", Ada pegawai yang tidak ada data presensi nya");
                } else {
                    System.out.println("attendance : " + attendanceRun);
                    if (employeePayrollComponents != null) {
                        for (EmployeePayrollComponentDto em : employeePayrollComponents) {

                            if (em.getJenisComponentMaster().equals("BASIC")) {

                                if (statusAktif.equals("A")) {
                                    totalGajiPokok = totalGajiPokok.add(em.getNominal());
                                } else {
                                    //Prorate
                                    if (attendanceRun.getTotalShift() > 0) {
                                        if (em.getNominal().compareTo(BigDecimal.ZERO) > 0) {
                                            BigDecimal hitungan = em.getNominal().divide(BigDecimal.valueOf(attendanceRun.getTotalShift()), 2, RoundingMode.HALF_UP);
                                            hitungan = hitungan.multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance()));
                                            totalGajiPokok = totalGajiPokok.add(hitungan);
                                        }
                                    }
                                }


                                //Input Payroll Run detail
                                PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                payrollRunDetail.setBulan(bulan);
                                payrollRunDetail.setTahun(tahun);
                                payrollRunDetail.setJenisPayroll(em.getJenisComponentMaster());
                                payrollRunDetail.setDeskripsi(em.getComponentName());
                                payrollRunDetail.setNominal(em.getNominal());
                                payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                payrollRunDetailDao.save(payrollRunDetail);
                                //

                                if (em.getTaxable().equals("AKTIF")) {
                                    totalKenaPajak = totalKenaPajak.add(em.getNominal());
                                }
                            }
                            if (em.getJenisComponentMaster().equals("ALLOWANCE")) {
//                        totalAllowance = totalAllowance.add(em.getNominal());


                                if (statusAktif.equals("A")) {
                                    if (em.getPerhitungan().equals("HARIAN")) {
                                        if (attendanceRun.getTotalAttendance() <= em.getMaksimalHari()) {
                                            totalAllowance = totalAllowance.add(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                        } else {
                                            totalAllowance = totalAllowance.add(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                        }
                                    } else {
                                        totalAllowance = totalAllowance.add(em.getNominal());
                                    }
                                } else {
                                    if (em.getPerhitungan().equals("HARIAN")) {
                                        if (attendanceRun.getTotalAttendance() <= em.getMaksimalHari()) {
                                            totalAllowance = totalAllowance.add(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                        } else {
                                            totalAllowance = totalAllowance.add(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                        }
                                    } else {
                                        if (attendanceRun.getTotalShift() > 0) {
                                            if (em.getNominal().compareTo(BigDecimal.ZERO) > 0) {
                                                BigDecimal totalShift = new BigDecimal(attendanceRun.getTotalShift());
                                                System.out.println("value is:" + totalShift);
                                                System.out.println("value is:" + em.getNominal());
                                                BigDecimal hitungan = em.getNominal().divide(totalShift, 2, RoundingMode.HALF_UP);
                                                hitungan = hitungan.multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance()));
                                                totalAllowance = totalAllowance.add(hitungan);
                                            }
                                        }
                                    }
                                }


                                //Input Payroll Run detail
                                PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                payrollRunDetail.setBulan(bulan);
                                payrollRunDetail.setTahun(tahun);
                                payrollRunDetail.setJenisPayroll(em.getJenisComponentMaster());
                                payrollRunDetail.setDeskripsi(em.getComponentName());
                                if (em.getPerhitungan().equals("HARIAN")) {
                                    if (attendanceRun.getTotalAttendance() <= em.getMaksimalHari()) {
                                        payrollRunDetail.setNominal(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                    } else {
                                        payrollRunDetail.setNominal(em.getNominal().multiply(new BigDecimal(em.getMaksimalHari())));
                                    }
                                } else {
                                    payrollRunDetail.setNominal(em.getNominal());
                                }
                                payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                payrollRunDetailDao.save(payrollRunDetail);
                                //
                                if (em.getTaxable().equals("AKTIF")) {
                                    if (em.getPerhitungan().equals("HARIAN")) {
                                        totalKenaPajak = totalKenaPajak.add(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                    } else {
                                        totalKenaPajak = totalKenaPajak.add(em.getNominal());
                                    }
                                }
//                        if (em.getPayrollComponent().getTaxable().equals(StatusRecord.KURANG)){
//                            totalKenaPajak = totalKenaPajak.subtract(em.getNominal());
//                        }
                                if (em.getZakat().equals("NONAKTIF")) {
//                                tidakKenaZakat = tidakKenaZakat.add(em.getNominal());
                                    if (em.getPerhitungan().equals("HARIAN")) {
                                        tidakKenaZakat = tidakKenaZakat.add(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                    } else {
                                        tidakKenaZakat = tidakKenaZakat.add(em.getNominal());
                                    }
                                }
                            }
                            if (em.getJenisComponentMaster().equals("BENEFIT")) {
                                if (em.getJenisNominal().equals("DEFAULT")) {

                                } else if (em.getJenisNominal().equals("CUSTOM")) {

                                } else if (em.getJenisNominal().equals("NOMINAL")) {
                                    totalBenefits = totalBenefits.add(em.getNominal());
                                    //Input Payroll Run detail
                                    PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                    payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                    payrollRunDetail.setBulan(bulan);
                                    payrollRunDetail.setTahun(tahun);
                                    payrollRunDetail.setJenisPayroll(em.getJenisComponentMaster());
                                    payrollRunDetail.setDeskripsi(em.getComponentName());
                                    payrollRunDetail.setNominal(em.getNominal());
                                    payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                    payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                    payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                    payrollRunDetailDao.save(payrollRunDetail);
                                    //
                                    if (em.getTaxable().equals("AKTIF")) {
                                        if (em.getPerhitungan().equals("HARIAN")) {
                                            totalKenaPajak = totalKenaPajak.add(em.getNominal().multiply(BigDecimal.valueOf(attendanceRun.getTotalAttendance())));
                                        } else {
                                            totalKenaPajak = totalKenaPajak.add(em.getNominal());
                                        }
                                    }
                                }
                            }
                        }

                        //Impor nominal ketazkiaan
                        BigDecimal ambilk = employeeTasksScheduleDao.tarikTotalTunjanganKetazkiaan(e.getId(), localDate, localDate2);

                        if (ambilk == null) {
                            PayrollRunProcessDetail payrollRunProcessDetail = new PayrollRunProcessDetail();
                            payrollRunProcessDetail.setStatusRun(StatusRecord.CHECK);
                            payrollRunProcessDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,e.getId()));
                            payrollRunProcessDetail.setDescription("Tidak  ada data ketazkiaan.");
                            payrollRunProcessDetail.setPayrollRunProcess(payrollRunProcess);
                            payrollRunProcessDetail.setStatus(StatusRecord.AKTIF);
                            payrollRunProcessDetailDao.save(payrollRunProcessDetail);
                            payrollRunProcess.setDescription(payrollRunProcess.getDescription() + ", Ada pegawai yang tidak ada data ketazkiaan nya");
                        }else{
                            ketazkiaan = ketazkiaan.add(ambilk);

                            totalAllowance = totalAllowance.add(ketazkiaan);
                            totalKenaPajak = totalKenaPajak.add(ketazkiaan);

                            PayrollRunDetail payrollRunDetail11 = new PayrollRunDetail();
                            payrollRunDetail11.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                            payrollRunDetail11.setBulan(bulan);
                            payrollRunDetail11.setTahun(tahun);
                            payrollRunDetail11.setJenisPayroll("ALLOWANCE");
                            payrollRunDetail11.setDeskripsi("Tunjangan Ketazkiaan");
                            payrollRunDetail11.setNominal(ketazkiaan);
                            payrollRunDetail11.setStatus(StatusRecord.AKTIF);
                            payrollRunDetail11.setUserUpdate(payrollRunProcess.getUserInput());
                            payrollRunDetail11.setDateUpdate(LocalDateTime.now());
                            payrollRunDetailDao.save(payrollRunDetail11);
                        }


                        //Hitung lembur
                        BigDecimal lemburPerJam = totalGajiPokok.divide(BigDecimal.valueOf(173), 2, RoundingMode.HALF_UP);
                        System.out.println("Lembur Per jam : " + lemburPerJam);
                        BigDecimal lemburWorkingJamPertama = lemburPerJam.multiply(BigDecimal.valueOf(1.5));
                        BigDecimal lemburWorkingJamBerikutnya = lemburPerJam.multiply(BigDecimal.valueOf(2));
                        BigDecimal lemburHolidayJamPertama = lemburPerJam.multiply(BigDecimal.valueOf(2));
                        BigDecimal lemburHolidayJamKedua = lemburPerJam.multiply(BigDecimal.valueOf(3));
                        BigDecimal lemburHolidayJamBerikutnya = lemburPerJam.multiply(BigDecimal.valueOf(4));

                        EmployeeType employeeType = employeeTypeDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
                        if (employeeType != null) {
                            if (employeeType.getType().equals(TypeRecord.SOPIR)) {
                                totalLembur = totalLembur.add(new BigDecimal(13000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayFirstHour())));
                                totalLembur = totalLembur.add(new BigDecimal(13000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayNextHour())));
                                totalLembur = totalLembur.add(new BigDecimal(13000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayFirstHour())));
                                totalLembur = totalLembur.add(new BigDecimal(13000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidaySecondHour())));
                                totalLembur = totalLembur.add(new BigDecimal(13000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayNextHour())));
                            } else if (employeeType.getType().equals(TypeRecord.TENDIK)) {
                                totalLembur = totalLembur.add(new BigDecimal(20000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayFirstHour())));
                                totalLembur = totalLembur.add(new BigDecimal(20000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayNextHour())));
                                totalLembur = totalLembur.add(new BigDecimal(20000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayFirstHour())));
                                totalLembur = totalLembur.add(new BigDecimal(20000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidaySecondHour())));
                                totalLembur = totalLembur.add(new BigDecimal(20000).multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayNextHour())));
                            }
                        }

//                    totalLembur = totalLembur.add(lemburWorkingJamPertama.multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayFirstHour())));
//                    totalLembur = totalLembur.add(lemburWorkingJamBerikutnya.multiply(BigDecimal.valueOf(attendanceRun.getOvertimeWorkingDayNextHour())));
//                    totalLembur = totalLembur.add(lemburHolidayJamPertama.multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayFirstHour())));
//                    totalLembur = totalLembur.add(lemburHolidayJamKedua.multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidaySecondHour())));
//                    totalLembur = totalLembur.add(lemburHolidayJamBerikutnya.multiply(BigDecimal.valueOf(attendanceRun.getOvertimeHolidayNextHour())));
                        totalAllowance = totalAllowance.add(totalLembur);
                        totalKenaPajak = totalKenaPajak.add(totalLembur);

                        System.out.println("Lembur : done");

                        PayrollRunDetail payrollRunDetail11 = new PayrollRunDetail();
                        payrollRunDetail11.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                        payrollRunDetail11.setBulan(bulan);
                        payrollRunDetail11.setTahun(tahun);
                        payrollRunDetail11.setJenisPayroll("ALLOWANCE");
                        payrollRunDetail11.setDeskripsi("Overtime");
                        payrollRunDetail11.setNominal(totalLembur);
                        payrollRunDetail11.setStatus(StatusRecord.AKTIF);
                        payrollRunDetail11.setUserUpdate(payrollRunProcess.getUserInput());
                        payrollRunDetail11.setDateUpdate(LocalDateTime.now());
                        payrollRunDetailDao.save(payrollRunDetail11);

                        System.out.println("Tunjangan Kinerja : start");

                        System.out.println("Tendik : start");

                        BigDecimal totalKpiTendik = penilaianKpiDao.totalKpi(bulan, tahun, e.getId());
                        if(totalKpiTendik != null){
                            if(totalKpiTendik.compareTo(BigDecimal.valueOf(0)) > 0){
                                totalKenaPajak = totalKenaPajak.add(totalKpiTendik);
                                totalAllowance = totalAllowance.add(totalKpiTendik);

                                PayrollRunDetail payrollRunDetailKpiTendik = new PayrollRunDetail();
                                payrollRunDetailKpiTendik.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                payrollRunDetailKpiTendik.setBulan(bulan);
                                payrollRunDetailKpiTendik.setTahun(tahun);
                                payrollRunDetailKpiTendik.setJenisPayroll("ALLOWANCE");
                                payrollRunDetailKpiTendik.setDeskripsi("Tunjangan Kinerja Tendik");
                                payrollRunDetailKpiTendik.setNominal(totalKpiTendik);
                                payrollRunDetailKpiTendik.setStatus(StatusRecord.AKTIF);
                                payrollRunDetailKpiTendik.setUserUpdate(payrollRunProcess.getUserInput());
                                payrollRunDetailKpiTendik.setDateUpdate(LocalDateTime.now());
                                payrollRunDetailDao.save(payrollRunDetailKpiTendik);
                            }
                        }

                        System.out.println("Tendik : end");

                        System.out.println("Deosen : start");

                        String idDosen = lecturerDao.ambilIdDosen(e.getId());
                        if (idDosen != null) {
                            BigDecimal totalKpiDosen = penilaianKpiDao.totalKpi(bulan, tahun, idDosen);
                            if(totalKpiDosen != null){
                                if(totalKpiDosen.compareTo(BigDecimal.valueOf(0)) > 0){
                                    totalKenaPajak = totalKenaPajak.add(totalKpiDosen);
                                    totalAllowance = totalAllowance.add(totalKpiDosen);

                                    PayrollRunDetail payrollRunDetailKpiDosen = new PayrollRunDetail();
                                    payrollRunDetailKpiDosen.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                    payrollRunDetailKpiDosen.setBulan(bulan);
                                    payrollRunDetailKpiDosen.setTahun(tahun);
                                    payrollRunDetailKpiDosen.setJenisPayroll("ALLOWANCE");
                                    payrollRunDetailKpiDosen.setDeskripsi("Tunjangan Kinerja Dosen");
                                    payrollRunDetailKpiDosen.setNominal(totalKpiDosen);
                                    payrollRunDetailKpiDosen.setStatus(StatusRecord.AKTIF);
                                    payrollRunDetailKpiDosen.setUserUpdate(payrollRunProcess.getUserInput());
                                    payrollRunDetailKpiDosen.setDateUpdate(LocalDateTime.now());
                                    payrollRunDetailDao.save(payrollRunDetailKpiDosen);
                                }
                            }
                        }

                        System.out.println("Dosen : end");

                        System.out.println("Tunjangan Kinerja : done");


                        //Perhitungan Zakat dan Infaq
                        for (EmployeePayrollComponentDto emc : employeePayrollComponents) {
                            BigDecimal batasZakat = BigDecimal.valueOf(6644868.00);
                            BigDecimal pengaliZakat = BigDecimal.valueOf(2.50);
                            BigDecimal totalPenghasilan = BigDecimal.ZERO;

                            if (totalAllowance == null) {
                                totalPenghasilan = totalGajiPokok;
                            } else {
                                totalPenghasilan = totalGajiPokok.add(totalAllowance);
                            }

                            totalPenghasilan = totalPenghasilan.subtract(tidakKenaZakat);
                            if (totalPenghasilan.compareTo(BigDecimal.ZERO) > 0) {
//                        totalPenghasilan = BigDecimal.ZERO;
                                if (emc.getJenisComponentMaster().equals("DEDUCTION")) {

                                    System.out.println("Zakat :" + batasZakat);
                                    System.out.println("Pengali :" + pengaliZakat);

                                    if (emc.getRumus().equals("DEFAULT")) {
                                        System.out.println("default");
                                        if (emc.getKodeRumusDefault() != null) {
                                            System.out.println("null : " + emc.getKodeRumusDefault());
                                            if (emc.getKodeRumusDefault().trim().equals("ZAKAT")) {
                                                System.out.println("zakat");
                                                if (totalPenghasilan.compareTo(batasZakat) >= 0) {
                                                    System.out.println("nilai");
                                                    totalZakat = totalPenghasilan.multiply(pengaliZakat);
                                                    totalZakat = totalZakat.divide(BigDecimal.valueOf(100.00), 2, RoundingMode.HALF_UP);

                                                    //Input Payroll Run detail
                                                    PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                                    payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                                    payrollRunDetail.setBulan(bulan);
                                                    payrollRunDetail.setTahun(tahun);
                                                    payrollRunDetail.setJenisPayroll(emc.getJenisComponentMaster());
                                                    payrollRunDetail.setDeskripsi(emc.getComponentName());
                                                    payrollRunDetail.setNominal(totalZakat);
                                                    payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                                    payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                                    payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                                    payrollRunDetailDao.save(payrollRunDetail);
                                                    //

                                                }
                                                System.out.println("Total zakat : " + totalZakat);
                                                if (emc.getTaxable().equals("AKTIF")) {
                                                    totalKenaPajak = totalKenaPajak.subtract(totalZakat);
                                                }
                                            }
                                            if (emc.getKodeRumusDefault().equals("INFAQ")) {
                                                if (totalPenghasilan.compareTo(batasZakat) < 0) {
                                                    totalInfaq = totalGajiPokok.multiply(pengaliZakat);
                                                    totalInfaq = totalInfaq.divide(BigDecimal.valueOf(100.00), 2, RoundingMode.HALF_UP);

                                                    //Input Payroll Run detail
                                                    PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                                    payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                                    payrollRunDetail.setBulan(bulan);
                                                    payrollRunDetail.setTahun(tahun);
                                                    payrollRunDetail.setJenisPayroll(emc.getJenisComponentMaster());
                                                    payrollRunDetail.setDeskripsi(emc.getComponentName());
                                                    payrollRunDetail.setNominal(totalInfaq);
                                                    payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                                    payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                                    payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                                    payrollRunDetailDao.save(payrollRunDetail);
                                                    //
                                                }
                                                System.out.println("Total infaq : " + totalInfaq);
                                                if (emc.getTaxable().equals("AKTIF")) {
                                                    totalKenaPajak = totalKenaPajak.subtract(totalInfaq);
                                                }
                                            }
                                        }


                                    }


                                    //Cari Deductions Non Pajak
                                    if (emc.getJenisNominal().equals("DEFAULT")) {

                                    } else if (emc.getJenisNominal().equals("CUSTOM")) {

                                    } else {

                                        if (!emc.getKodeRumusDefault().trim().equals("ZAKAT") && !emc.getKodeRumusDefault().trim().equals("INFAQ")) {
                                            totalDeductions = totalDeductions.add(emc.getNominal());

                                            //Input Payroll Run detail
                                            PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                            payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                            payrollRunDetail.setBulan(bulan);
                                            payrollRunDetail.setTahun(tahun);
                                            payrollRunDetail.setJenisPayroll(emc.getJenisComponentMaster());
                                            payrollRunDetail.setDeskripsi(emc.getComponentName());
                                            payrollRunDetail.setNominal(emc.getNominal());
                                            payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                            payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                            payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                            payrollRunDetailDao.save(payrollRunDetail);

                                            if (emc.getTaxable().equals("AKTIF")) {
                                                totalKenaPajak = totalKenaPajak.subtract(emc.getNominal());
                                            }
                                        }
                                    }
                                    //
                                }

                                System.out.println("total_zakat :" + totalZakat);
                                if (totalZakat.equals(BigDecimal.ZERO)) {
                                    if (totalPenghasilan.compareTo(batasZakat) >= 0) {
                                        System.out.println("nilai");
                                        totalZakat = totalPenghasilan.multiply(pengaliZakat);
                                        totalZakat = totalZakat.divide(BigDecimal.valueOf(100.00), 2, RoundingMode.HALF_UP);

                                        //Input Payroll Run detail
                                        PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                        payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                        payrollRunDetail.setBulan(bulan);
                                        payrollRunDetail.setTahun(tahun);
                                        payrollRunDetail.setJenisPayroll("DEDUCTION");
                                        payrollRunDetail.setDeskripsi("Zakat");
                                        payrollRunDetail.setNominal(totalZakat);
                                        payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                        payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                        payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                        payrollRunDetailDao.save(payrollRunDetail);
                                        //

                                        totalKenaPajak = totalKenaPajak.subtract(totalZakat);

                                    }
                                }

                                System.out.println("total_infaq :" + totalInfaq);
                                if (totalInfaq.equals(BigDecimal.ZERO)) {
                                    if (totalPenghasilan.compareTo(batasZakat) < 0) {
                                        totalInfaq = totalGajiPokok.multiply(pengaliZakat);
                                        totalInfaq = totalInfaq.divide(BigDecimal.valueOf(100.00), 2, RoundingMode.HALF_UP);

                                        //Input Payroll Run detail
                                        PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                                        payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                                        payrollRunDetail.setBulan(bulan);
                                        payrollRunDetail.setTahun(tahun);
                                        payrollRunDetail.setJenisPayroll("DEDUCTION");
                                        payrollRunDetail.setDeskripsi("Infaq");
                                        payrollRunDetail.setNominal(totalInfaq);
                                        payrollRunDetail.setStatus(StatusRecord.AKTIF);
                                        payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                                        payrollRunDetail.setDateUpdate(LocalDateTime.now());
                                        payrollRunDetailDao.save(payrollRunDetail);
                                        //
                                        totalKenaPajak = totalKenaPajak.subtract(totalInfaq);
                                    }
                                }

                            }

                        }
                        //


                        //Cari Loan ini harus link ke pembayaran loan
                        //List<EmployeeLoan> employeeLoans = employeeLoanDao.findByStatusAndEmployesAndEffectiveDateBefore(StatusRecord.AKTIF, employesDao.findByStatusAndId(StatusRecord.AKTIF, e), LocalDate.now());

                        System.out.println("func loan");

                        String tanggalan = tahun + '-' + bulan + '-' + "20";
                        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate localDates = LocalDate.parse(tanggalan, formatter);

                        List<BayarLoanDto> bayarLoanDtoList = employeeLoanDao.listLoanEmployee(e.getId(), localDates, bulan, tahun);
                        if (bayarLoanDtoList != null) {

                            for (BayarLoanDto bd : bayarLoanDtoList) {

                                EmployeeLoan employeeLoan = employeeLoanDao.findByStatusNotAndId(StatusRecord.HAPUS, bd.getId());
                                EmployeeLoanBayar employeeLoanBayar = employeeLoanBayarDao.findByStatusAndEmployeeLoanAndBulanAndTahun(StatusRecord.AKTIF, employeeLoan, bulan, tahun);
                                if (employeeLoanBayar == null) {
                                    employeeLoanBayar = new EmployeeLoanBayar();
                                }

                                if (bd.getSisaBayar().compareTo(BigDecimal.ZERO) > 0) {
                                    if (bd.getSisaBayar().compareTo(bd.getCurrentPayment()) >= 0) {
                                        totalLoan = totalLoan.add(bd.getCurrentPayment());
                                        employeeLoanBayar.setPayment(bd.getCurrentPayment());
                                    } else {
                                        totalLoan = totalLoan.add(bd.getSisaBayar());
                                        employeeLoanBayar.setPayment(bd.getSisaBayar());
                                    }

                                    employeeLoanBayar.setEmployeeLoan(employeeLoan);
                                    employeeLoanBayar.setBulan(bulan);
                                    employeeLoanBayar.setTahun(tahun);
                                    employeeLoanBayar.setTanggalBayar(LocalDateTime.now());
                                    employeeLoanBayar.setDateUpdate(LocalDateTime.now());
                                    employeeLoanBayar.setUserUpdate(payrollRunProcess.getUserInput());
                                    employeeLoanBayar.setCicilanKe(bd.getCicilanKe());
                                    employeeLoanBayarDao.save(employeeLoanBayar);


                                }

                            }

                            //Input Payroll Run detail
                            PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                            payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                            payrollRunDetail.setBulan(bulan);
                            payrollRunDetail.setTahun(tahun);
                            payrollRunDetail.setJenisPayroll("DEDUCTION");
                            payrollRunDetail.setDeskripsi("Loan");
                            payrollRunDetail.setNominal(totalLoan);
                            payrollRunDetail.setStatus(StatusRecord.AKTIF);
                            payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                            payrollRunDetail.setDateUpdate(LocalDateTime.now());
                            payrollRunDetailDao.save(payrollRunDetail);
                            //

                        }

                        //

                        //Potongan Absensi

                        System.out.println("func pot Absensi");
                        List<EmployeePayrollComponent> employeePayrollComponents1 = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponentPotonganAbsen(StatusRecord.HAPUS, employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()), StatusRecord.AKTIF);

                        if (employeePayrollComponents1 != null) {
                            for (EmployeePayrollComponent epc : employeePayrollComponents1) {
                                System.out.println(epc.getJenisNominal() + " tukin :" + epc.getNominal());
                                if (epc.getJenisNominal().equals("NOMINAL")) {
                                    if (statusAktif.equals("A")) {
                                        totalPotonganAbsen = totalPotonganAbsen.add((epc.getNominal().multiply(BigDecimal.valueOf(0.03)).multiply(BigDecimal.valueOf(attendanceRun.getTotalAbsen()))));
                                    }
                                } else if (epc.getJenisNominal().equals("CUSTOM")) {

                                } else {

                                }

                            }

                            System.out.println("potongan absen : " + totalPotonganAbsen);
                            //Input Payroll Run detail
                            PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                            payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                            payrollRunDetail.setBulan(bulan);
                            payrollRunDetail.setTahun(tahun);
                            payrollRunDetail.setJenisPayroll("DEDUCTION");
                            payrollRunDetail.setDeskripsi("Potongan Absen");
                            payrollRunDetail.setNominal(totalPotonganAbsen);
                            payrollRunDetail.setStatus(StatusRecord.AKTIF);
                            payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                            payrollRunDetail.setDateUpdate(LocalDateTime.now());
                            payrollRunDetailDao.save(payrollRunDetail);
                            //
                        }

                        //

                        //Potongan Terlambat

                        System.out.println("func pot terlambat");
                        List<EmployeePayrollComponent> employeePayrollComponents2 = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponentPotonganTerlambat(StatusRecord.HAPUS, employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()), StatusRecord.AKTIF);
                        if (employeePayrollComponents2 != null) {
                            for (EmployeePayrollComponent epd : employeePayrollComponents2) {
                                if (epd.getJenisNominal().equals("NOMINAL")) {
//                            if(statusAktif.equals("A")) {
                                    if (epd.getPayrollComponent().getPerhitungan() == StatusRecord.HARIAN) {
                                        totalPotonganTerlambat = totalPotonganTerlambat.add((epd.getNominal().multiply(BigDecimal.valueOf(0.25)).multiply(BigDecimal.valueOf(attendanceRun.getTotalLate()))));
                                    }else{
//                                        totalPotonganTerlambat = totalPotonganTerlambat.add(((epd.getNominal().divide(BigDecimal.valueOf(22))).multiply(BigDecimal.valueOf(0.25))).multiply(BigDecimal.valueOf(attendanceRun.getTotalLate())));
                                        totalPotonganTerlambat = totalPotonganTerlambat.add(
                                                epd.getNominal()
                                                        .divide(BigDecimal.valueOf(22), 2, RoundingMode.HALF_UP)
                                                        .multiply(BigDecimal.valueOf(0.25))
                                                        .multiply(BigDecimal.valueOf(attendanceRun.getTotalLate()))
                                        );

                                    }
//                            }
                                } else if (epd.getJenisNominal().equals("CUSTOM")) {

                                } else {

                                }

                            }

                            System.out.println("potongan terlambat : " + totalPotonganTerlambat);

                            //Input Payroll Run detail
                            PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                            payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                            payrollRunDetail.setBulan(bulan);
                            payrollRunDetail.setTahun(tahun);
                            payrollRunDetail.setJenisPayroll("DEDUCTION");
                            payrollRunDetail.setDeskripsi("Potongan Terlambat");
                            payrollRunDetail.setNominal(totalPotonganTerlambat);
                            payrollRunDetail.setStatus(StatusRecord.AKTIF);
                            payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                            payrollRunDetail.setDateUpdate(LocalDateTime.now());
                            payrollRunDetailDao.save(payrollRunDetail);
                            //
                        }

                        //Pajak

                        System.out.println("func pajak");
                        EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                        if(employeePayrollInfo == null){
                            PayrollRunProcessDetail payrollRunProcessDetail = new PayrollRunProcessDetail();
                            payrollRunProcessDetail.setStatusRun(StatusRecord.ERROR);
                            payrollRunProcessDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,e.getId()));
                            payrollRunProcessDetail.setDescription("Payroll Info belum di setting.");
                            payrollRunProcessDetail.setPayrollRunProcess(payrollRunProcess);
                            payrollRunProcessDetail.setStatus(StatusRecord.AKTIF);
                            payrollRunProcessDetailDao.save(payrollRunProcessDetail);
                            payrollRunProcess.setDescription(payrollRunProcess.getDescription() + ", Ada pegawai yang payroll info blm disetting");
                            statusRecord = statusRecord.ERROR;
                        }else{

                            if(employeePayrollInfo.getPtkpStatus() == null){
                                PayrollRunProcessDetail payrollRunProcessDetail = new PayrollRunProcessDetail();
                                payrollRunProcessDetail.setStatusRun(StatusRecord.ERROR);
                                payrollRunProcessDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,e.getId()));
                                payrollRunProcessDetail.setDescription("PTKP status belum di setting.");
                                payrollRunProcessDetail.setPayrollRunProcess(payrollRunProcess);
                                payrollRunProcessDetail.setStatus(StatusRecord.AKTIF);
                                payrollRunProcessDetailDao.save(payrollRunProcessDetail);
                                statusRecord = statusRecord.ERROR;
                                payrollRunProcess.setDescription(payrollRunProcess.getDescription() + ", Ada pegawai yang PTKP nya blm di setting");
                            }else {

                                String employeeStatusKontrak = "KONTRAK";
                                String employeeStatusKontrak2 = employeeStatusDao.statusEmployee(employes.getId());
                                if (employeeStatusKontrak2.isEmpty()) {

                                } else {
                                    if (employeeStatusKontrak2 != null) {
                                        employeeStatusKontrak = employeeStatusKontrak2;
                                    }
                                }
                                System.out.println(employes.getFullName() + " : " + employeeStatusKontrak);
                                if (employeePayrollInfo != null) {

                                    System.out.println("kena_pajak : " + totalKenaPajak);
                                    if (totalKenaPajak.compareTo(BigDecimal.ZERO) > 0) {

                                        BigDecimal persentasePajak = employeePayrollInfoDao.cariRumusPajak(employeePayrollInfo.getPtkpStatus().getRumus(), totalKenaPajak);
                                        if (persentasePajak != null){
                                            totalPajak = totalKenaPajak.multiply(persentasePajak).divide(BigDecimal.valueOf(100));
                                        }

                                        //Katanya bisa dipakai di perhitungan bulan 12

//                                        totalPajak = totalKenaPajak.multiply(BigDecimal.valueOf(12));
//                                        BigDecimal pengurangJabtan = totalPajak.multiply(BigDecimal.valueOf(0.05));
//                                        if (pengurangJabtan.compareTo(BigDecimal.valueOf(6000000)) > 0) {
//                                            pengurangJabtan = BigDecimal.valueOf(6000000);
//                                        }
//                                        if (employeeStatusKontrak.equals("TETAP")) {
//                                            totalPajak = totalPajak.subtract(pengurangJabtan);
//                                        }
//                                        System.out.println("kena_pajak : " + totalPajak);
//                                        //                    totalPajak = totalPajak.subtract(totalPajak.multiply(BigDecimal.valueOf(0.05)));
//                                        totalPajak = totalPajak.subtract(employeePayrollInfo.getPtkpStatus().getNominal());
//
//                                        System.out.println("total_pajak : " + totalPajak);
//
//                                        if (totalPajak.compareTo(BigDecimal.ZERO) > 0) {
//                                            BigDecimal hitungPajak = BigDecimal.ZERO;
//                                            BigDecimal pajakSetahun = BigDecimal.ZERO;
//                                            BigDecimal pajak = BigDecimal.ZERO;
//                                            List<HitungPajakDto> hitungPajakDtoList = hitungPajakDao.listHitungPajak(totalPajak);
//                                            for (HitungPajakDto hpd : hitungPajakDtoList) {
//                                                totalPajak = totalPajak.subtract(hpd.getSyaratDari());
//                                                hitungPajak = hpd.getPersentase().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
//                                                if (totalPajak.compareTo(hpd.getSyaratSampai()) > 0) {
//                                                    pajakSetahun = hpd.getSyaratSampai();
//                                                    pajakSetahun = pajakSetahun.multiply(hitungPajak);
//                                                    pajak = pajak.add(pajakSetahun);
//                                                } else {
//                                                    pajakSetahun = totalPajak;
//                                                    pajakSetahun = pajakSetahun.multiply(hitungPajak);
//                                                    pajak = pajak.add(pajakSetahun);
//                                                }
//                                            }
//
//                                            totalPajak = pajak;
//                                            totalPajak = totalPajak.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP);
//                                        } else {
//                                            totalPajak = BigDecimal.ZERO;
//                                        }
                                    } else {
                                        totalPajak = BigDecimal.ZERO;
                                    }
                                }
                            }
                        }

                        //Input Payroll Run detail
                        PayrollRunDetail payrollRunDetail = new PayrollRunDetail();
                        payrollRunDetail.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, e.getId()));
                        payrollRunDetail.setBulan(bulan);
                        payrollRunDetail.setTahun(tahun);
                        payrollRunDetail.setJenisPayroll("DEDUCTION");
                        payrollRunDetail.setDeskripsi("PPh21");
                        payrollRunDetail.setNominal(totalPajak);
                        payrollRunDetail.setStatus(StatusRecord.AKTIF);
                        payrollRunDetail.setUserUpdate(payrollRunProcess.getUserInput());
                        payrollRunDetail.setDateUpdate(LocalDateTime.now());
                        payrollRunDetailDao.save(payrollRunDetail);
                        //

                        //
                    }
                }
                System.out.println("func input payroll run");
                PayrollRun payrollRun = payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, e.getId());
                if(payrollRun == null){
                    payrollRun = new PayrollRun();
                }
                payrollRun.setIdEmployee(e.getId());
                payrollRun.setBulan(bulan);
                payrollRun.setTahun(tahun);
                payrollRun.setGajiPokok(totalGajiPokok);
                payrollRun.setTotalAllowance(totalAllowance);
                payrollRun.setTotalBenefits(totalBenefits);
                payrollRun.setTotalInfaq(totalInfaq);
                payrollRun.setTotalZakat(totalZakat);
                payrollRun.setTotalKenaPajak(totalKenaPajak);
                payrollRun.setTotalPajak(totalPajak);
                payrollRun.setTotalLoan(totalLoan);
                payrollRun.setTotalDeductions(totalDeductions);
                payrollRun.setTotalPotonganAbsen(totalPotonganAbsen);
                payrollRun.setTotalPotonganTerlambat(totalPotonganTerlambat);
                payrollRun.setDateUpdate(LocalDateTime.now());
                payrollRun.setUserUpdate(payrollRunProcess.getUserInput());
                payrollRun.setPaySlip(StatusRecord.NONAKTIF);
                BigDecimal totalPenambah = totalGajiPokok.add(totalAllowance);
                BigDecimal totalPengurang = totalDeductions.add(totalPajak).add(totalInfaq).add(totalZakat).add(totalLoan).add(totalPotonganAbsen).add(totalPotonganTerlambat);
                payrollRun.setTotalPayroll(totalPenambah.subtract(totalPengurang));
                payrollRunDao.save(payrollRun);
            }


        }

        payrollRunProcess.setStatus(statusRecord);
        payrollRunProcess.setTanggalSelesai(LocalDateTime.now());
        payrollRunProcessDao.save(payrollRunProcess);

    }


}
