package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDosenDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiEvidanceDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiEvidanceDosenDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerClassKpiDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.PositionKpiDao;
import id.ac.tazkia.aplikasihr.dto.kpi.EditDto;
import id.ac.tazkia.aplikasihr.dto.kpi.GetKpiAssesmentDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.kpi.RekapPengisianKpiDto;
import id.ac.tazkia.aplikasihr.dto.kpi.SaveKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidanceDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;


import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service @Slf4j
public class KpiService {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PositionKpiDao positionKpiDao;

    @Autowired
    private PengisianKpiDao pengisianKpiDao;

    @Autowired
    private PengisianKpiEvidanceDao pengisianKpiEvidanceDao;

    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private LecturerClassKpiDao lecturerClassKpiDao;

    @Autowired
    private PengisianKpiDosenDao pengisianKpiDosenDao;

    @Autowired PengisianKpiEvidanceDosenDao pengisianKpiEvidanceDosenDao;

    @Autowired
    private ResizeFIleService resizeFIleService;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;


    public Page<ListKpiEmployeeDto> getKpiListEmployee(Employes employee, String bulan, String tahun, Pageable pageable) {
        List<ListKpiEmployeeDto> listKpiEmployeeDtos = new ArrayList<>();
        Page<PengisianKpi> pengisianKpiList = pengisianKpiDao
                .findByStatusAndStatusKpiAndEmployesAndBulanAndTahunOrderByTanggal(
                        StatusRecord.AKTIF, StatusRecord.OPEN, employee, bulan, tahun, pageable);
        for (PengisianKpi data : pengisianKpiList){
            ListKpiEmployeeDto listKpiEmployeeDto = new ListKpiEmployeeDto();
            List<PengisianKpiEvidance> pengisianKpiEvidance = pengisianKpiEvidanceDao.findByPengisianKpiAndStatus(data, StatusRecord.AKTIF);

            listKpiEmployeeDto.setPengisianKpi(data);
            listKpiEmployeeDto.setPengisianKpiEvidanceList(pengisianKpiEvidance);
            listKpiEmployeeDtos.add(listKpiEmployeeDto);
        }

        return new PageImpl<>(listKpiEmployeeDtos, pageable, pengisianKpiList.getTotalElements());
    }

    @Transactional
    public void savePengisianKpi(Authentication authentication, SaveKpiEmployeeDto saveKpiEmployeeDto) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        Optional<PositionKpi> positionKpiOptional = positionKpiDao.findById(saveKpiEmployeeDto.getIdKpi());

        Integer bulan = saveKpiEmployeeDto.getTanggal().getMonthValue();
        Integer tahun = saveKpiEmployeeDto.getTanggal().getYear();
        if(saveKpiEmployeeDto.getTanggal().getDayOfMonth() > 20){
            if(saveKpiEmployeeDto.getTanggal().getMonthValue() == 12){
                bulan = 1;
                tahun = saveKpiEmployeeDto.getTanggal().getYear() + 1;
            }else{
                bulan = saveKpiEmployeeDto.getTanggal().getMonthValue() + 1;
            }
        }

        String tahunString = tahun.toString();
        String monthString = bulan.toString();


        if (monthString.length() > 1) {
            monthString = bulan.toString();
        } else {
            monthString = "0" + bulan.toString();
        }

        PengisianKpi pengisianKpi = new PengisianKpi();
        pengisianKpi.setPositionKpi(positionKpiOptional.get());
        pengisianKpi.setTanggal(saveKpiEmployeeDto.getTanggal());
        pengisianKpi.setTanggalInput(LocalDateTime.now());
        pengisianKpi.setEmployes(employes);
        pengisianKpi.setDeskripsi(saveKpiEmployeeDto.getDeskripsi());
        pengisianKpi.setTanggalPenugasan(saveKpiEmployeeDto.getTanggalPenugasan());
        pengisianKpi.setDeskripsiPenugasan(saveKpiEmployeeDto.getDeskripsiPenugasan());
        pengisianKpi.setBulan(monthString);
        pengisianKpi.setTahun(tahunString);
        pengisianKpiDao.save(pengisianKpi);

        for (int i = 0; i < saveKpiEmployeeDto.getFiles().length; i++) {
            PengisianKpiEvidance pengisianKpiEvidance = new PengisianKpiEvidance();
            pengisianKpiEvidance.setPengisianKpi(pengisianKpi);
            pengisianKpiEvidance.setNamaFile(saveKpiEmployeeDto.getFiles()[i].getOriginalFilename());

            String namaAsli = saveKpiEmployeeDto.getFiles()[i].getOriginalFilename();
            String extension = "";

            int j = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (j > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile1 = UUID.randomUUID().toString();
            new File(uploadKinerja).mkdirs();
            File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
            saveKpiEmployeeDto.getFiles()[i].transferTo(tujuan2);
            // **Jika file adalah PDF, resize PDF**

            if (saveKpiEmployeeDto.getFiles()[i].getContentType().equalsIgnoreCase("application/pdf")) {
                resizeFIleService.resizeFilePdf(tujuan2);
            } else {
                // Resize file image jika bukan PDF
                resizeFIleService.resizeFileImage(tujuan2);
            }



            pengisianKpiEvidance.setFile(idFile1 + "." + extension);
            pengisianKpiEvidance.setUserUpload(user.getUsername());
            pengisianKpiEvidance.setDateUpload(LocalDateTime.now());
            pengisianKpiEvidanceDao.save(pengisianKpiEvidance);
        }
    }

    @Transactional
    public void editKpiEmployee(EditDto editDto) throws IOException {
        PengisianKpi pengisianKpi = pengisianKpiDao.findById(editDto.getId()).get();
        pengisianKpi.setTanggal(editDto.getTanggal());
        pengisianKpi.setDeskripsi(editDto.getDeskripsi());
        pengisianKpi.setTanggalPenugasan(editDto.getTanggalPenugasan());
        pengisianKpi.setDeskripsiPenugasan(editDto.getDeskripsiPenugasan());
        pengisianKpiDao.save(pengisianKpi);
        for (int i = 0; i < editDto.getFiles().length && i < editDto.getIdEvidance().length; i++) {
            Optional<PengisianKpiEvidance> pengisianKpiEvidanceOptional = pengisianKpiEvidanceDao.findById(editDto.getIdEvidance()[i]);

            if (pengisianKpiEvidanceOptional.isPresent()) {
                PengisianKpiEvidance updatePengisianKpiEvidance = pengisianKpiEvidanceOptional.get();

                if (editDto.getFiles()[i] != null && !editDto.getFiles()[i].isEmpty()) {
                    String filename = editDto.getFiles()[i].getOriginalFilename();
                    String extension = "";

                    int j = filename.lastIndexOf('.');
                    int p = Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
                    if (j > p) {
                        extension = filename.substring(j + 1);
                    }

                    String idFile1 = UUID.randomUUID().toString();
                    new File(uploadKinerja).mkdirs();
                    File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);

                    editDto.getFiles()[i].transferTo(tujuan2);

                    updatePengisianKpiEvidance.setNamaFile(filename);
                    updatePengisianKpiEvidance.setFile(idFile1 + "." + extension);
                } else {
                    updatePengisianKpiEvidance.setNamaFile(pengisianKpiEvidanceOptional.get().getNamaFile());
                    updatePengisianKpiEvidance.setFile(pengisianKpiEvidanceOptional.get().getFile());
                }

                // Simpan objek update ke database
                pengisianKpiEvidanceDao.save(updatePengisianKpiEvidance);
            } else {
                System.out.println("Evidance ID " + editDto.getIdEvidance()[i] + " tidak ditemukan.");
            }
        }

    }

    @Transactional
    public void deleteListEmployeeKpi(String id) {
        pengisianKpiDao.hapusKpi(id);
    }

    public ResponseEntity<byte[]> getFileEvidence(PengisianKpiEvidance id) {
        String lokasiFile = uploadKinerja + File.separator + id.getFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (id.getFile().toLowerCase().endsWith("jpeg") || id.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(id.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(id.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public List<GetKpiAssesmentDto> getListKpiAssesmet(
            List<RekapPengisianKpiDto> rekapPengisianKpiDtos,
            String jenis, String bulan, String tahun, String employee) {
    
        List<GetKpiAssesmentDto> getKpiAssesmentDtoList = new ArrayList<>();
        
        for (RekapPengisianKpiDto data : rekapPengisianKpiDtos) {
            PositionKpi positionKpi = positionKpiDao.findById(data.getPositionKpi()).orElse(null);
            if (positionKpi == null) continue;

            System.out.println("Test E : " + data.getKpi());

            LecturerClassKpi lecturerClassKpi = lecturerClassKpiDao.findById(data.getPositionKpi()).orElse(null);
            
            GetKpiAssesmentDto getKpiAssesment = new GetKpiAssesmentDto();
            getKpiAssesment.setRekapPengisianKpi(data);
            
            if ("TENDIK".equals(jenis)) {
                Employes employes = employesDao.findById(employee).orElse(null);
                if (employes == null) continue;

                List<PengisianKpi> pengisianKpi = pengisianKpiDao
                        .findByStatusAndStatusKpiAndEmployesAndPositionKpiAndBulanAndTahunOrderByTanggal(
                                StatusRecord.AKTIF, StatusRecord.OPEN, employes, positionKpi, bulan, tahun);

                getKpiAssesment.setPengisianKpi(pengisianKpi);

                for (PengisianKpi dataPengisianKpi : pengisianKpi) {
                    PengisianKpi getPengisianKpi = pengisianKpiDao.findById(dataPengisianKpi.getId()).orElse(null);
                    if (getPengisianKpi == null) continue;

                    List<PengisianKpiEvidance> pengisianKpiEvidances = pengisianKpiEvidanceDao
                            .findByPengisianKpiAndStatus(getPengisianKpi, StatusRecord.AKTIF);
                    getKpiAssesment.setPengisianKpiEvidances(pengisianKpiEvidances);
                }
            }

            if ("DOSEN".equals(jenis)) {
                Lecturer lecturer = lecturerDao.findById(employee).orElse(null);
                if (lecturer == null) continue;

                List<PengisianKpiDosen> pengisianKpiDosen = pengisianKpiDosenDao
                        .findByStatusAndStatusKpiAndLecturerAndBulanAndTahunAndLecturerClassKpiOrderByTanggal(
                                StatusRecord.AKTIF, StatusRecord.OPEN, lecturer, bulan, tahun, lecturerClassKpi);

                getKpiAssesment.setPengisianKpiDosen(pengisianKpiDosen);

                for (PengisianKpiDosen dataPengisianKpiDosen : pengisianKpiDosen) {
                    PengisianKpiDosen getPengisianKpiDosen = pengisianKpiDosenDao
                            .findById(dataPengisianKpiDosen.getId()).orElse(null);
                    if (getPengisianKpiDosen == null) continue;

                    List<PengisianKpiEvidanceDosen> pengisianKpiEvidancesDosen = pengisianKpiEvidanceDosenDao
                            .findByPengisianKpiDosenAndStatus(getPengisianKpiDosen, StatusRecord.AKTIF);
                    getKpiAssesment.setPengisianKpiEvidancesDosen(pengisianKpiEvidancesDosen);
                }
            }

            getKpiAssesmentDtoList.add(getKpiAssesment);
        }

        return getKpiAssesmentDtoList;
    }



}
