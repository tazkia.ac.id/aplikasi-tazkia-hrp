package id.ac.tazkia.aplikasihr.services;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import id.ac.tazkia.aplikasihr.dao.document.DocumentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.document.BaseResponseDto;
import id.ac.tazkia.aplikasihr.dto.document.DetailDosenDto;
import id.ac.tazkia.aplikasihr.dto.document.SKMengajarDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Service
public class DocumentService {

    @Autowired private DocumentDao documentDao;

    @Autowired private JobPositionDao jobPositionDao;

    @Autowired private EmployesDao employesDao;

    @Autowired private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired private LecturerDao lecturerDao;

    @Autowired private ProdiDao prodiDao;

    @Autowired private LecturerClassDao lecturerClassDao;

    @Value("${icon.folder}")
    private String logo;

    @Value("${alamat.akademik}")
    private String baseUrl;

    @Value("${kode.warek1}")
    private String warek1;

    WebClient webClient = WebClient.builder()
            .baseUrl(baseUrl).defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    public BaseResponseDto getTahunAkademik() {
        return webClient.get()
                .uri(baseUrl.toString() + "/api/list-tahunakademik")
                .retrieve()
                .bodyToMono(BaseResponseDto.class)
                .block();
    }

    public BaseResponseDto getDosenByTahun(String tahunId){
        return webClient.get()
                .uri(baseUrl.toString() + "/api/list-dosen-mengajar/" + tahunId)
                .retrieve()
                .bodyToMono(BaseResponseDto.class)
                .block();
    }

    public BaseResponseDto getMatkul(String tahunId, String dosenId, String nomor){
        return webClient.get()
                .uri(baseUrl.toString() + "/api/jadwal-dosen/"+tahunId+"?id="+dosenId+"&no="+nomor)
                .retrieve()
                .bodyToMono(BaseResponseDto.class)
                .block();
    }

    public void updateNomor(String tahun, String dosen, String no, User user){
        id.ac.tazkia.aplikasihr.entity.document.Document document = documentDao.findByIdTahunAndIdDosen(tahun, dosen);
        if (document == null) {
            id.ac.tazkia.aplikasihr.entity.document.Document d = new id.ac.tazkia.aplikasihr.entity.document.Document();
            d.setIdTahun(tahun);
            d.setIdDosen(dosen);
            d.setNomor(Integer.parseInt(no));
            d.setNomorString(no);
            d.setUserUpdate(user.getUsername());
            d.setDateUpdate(LocalDateTime.now());
            documentDao.save(d);
        }
    }

    public void cekDataDosen(String nama, String email, String prodi, User user){
        System.out.println("Cek data dosen : " + nama);
        Lecturer lecturer = lecturerDao.findByStatusAndEmail(StatusRecord.AKTIF, email);
        if (lecturer == null) {
            Employes employes = employesDao.findByEmailActiveAndStatus(email, StatusRecord.AKTIF);
            if (employes == null) {

                Employes e = employesDao.findByUser(user);

                employes = new Employes();
                employes.setStatus(StatusRecord.AKTIF);
                employes.setStatusAktif("AKTIF");
                employes.setCompanies(e.getCompanies());
                employes.setEmailActive(email);
                employes.setFullName(nama);
                employesDao.save(employes);
            }

            lecturer = new Lecturer();
            lecturer.setEmployes(employes);
            lecturer.setEmail(email);
            lecturer.setCompanies(employes.getCompanies());
            lecturer.setLecturerClass(lecturerClassDao.findByStatusAndId(StatusRecord.AKTIF, "LB" ));
            lecturer.setProdi(prodiDao.findById(prodi).get());
            lecturer.setStatus(StatusRecord.AKTIF);
            lecturer.setUserInsert(user.getUsername());
            lecturer.setDateInsert(LocalDate.now());
            lecturerDao.save(lecturer);
        }
    }

    public String fixNamaBulanHijri(String namaBulanHijri){
        if ("Jumada I".equals(namaBulanHijri)) {
            namaBulanHijri = "Jumadil Awal";
        } else if ("Jumada II".equals(namaBulanHijri)) {
            namaBulanHijri = "Jumadil Akhir";
        } else if ("Rabiʻ I".equals(namaBulanHijri)) {
            namaBulanHijri = "Rabi'ul Awal";
        } else if ("Rabiʻ II".equals(namaBulanHijri)) {
            namaBulanHijri = "Rabi'ul Akhir";
        } else {
            namaBulanHijri = namaBulanHijri;
        }

        return namaBulanHijri;
    }

    // CREATE SURAT MENGAJAR
    public byte[] suratTugasMengajar(SKMengajarDto mengajarDto, String mulai, String selesai) {

        // Mengambil data Warek 1
        Employes warek = null;
        Lecturer lecturer = null;
        JobPosition position = jobPositionDao.findByStatusAndPositionCode(StatusRecord.AKTIF, warek1);
        EmployeeJobPosition getEmploye = employeeJobPositionDao.findByStatusAndPosition(StatusRecord.AKTIF, position);
        if (getEmploye != null) {
            warek = getEmploye.getEmployes();
            lecturer = lecturerDao.findByStatusAndEmail(StatusRecord.AKTIF, warek.getEmailActive());
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, baos);

            // parsing tgl mulai dan selesai
            LocalDate tglMulai = LocalDate.parse(mulai);
            LocalDate tglSelesai = LocalDate.parse(selesai);

            BaseFont narrowFont = BaseFont.createFont("static/fonts/arialnarrow.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            document.setMargins(36,36,36, 72);

            Image background = Image.getInstance(logo + File.separator + "logo_tr.png");
            background.scaleAbsolute(400, 450);

            Image footer = Image.getInstance(logo + File.separator + "footer.png");
            footer.scaleToFit(document.getPageSize().getWidth() - 20, 200);
//            footer.setAbsolutePosition(10, 10);
            writer.setPageEvent(new PdfPageEventHelper(){
                @Override
                public void onEndPage(PdfWriter writer, Document document) {
                    try {
                        float footerY = document.bottomMargin() - 50;
                        float footerX = (document.getPageSize().getWidth() - footer.getScaledWidth()) / 2;

                        footer.setAbsolutePosition(footerX, footerY);
                        writer.getDirectContent().addImage(footer);

                        background.setAbsolutePosition(400, 190);
                        writer.getDirectContent().addImage(background);

                        // Nomor Halaman
//                        PdfContentByte cb = writer.getDirectContent();
//                        cb.beginText();
//                        cb.setFontAndSize(narrowFont, 10);
//                        cb.showTextAligned(Element.ALIGN_CENTER, String.format("Page %d", writer.getPageNumber()), document.getPageSize().getWidth() / 2, footerY - 15, 0);
//                        cb.endText();
                    }catch (DocumentException e){
                        e.printStackTrace();
                    }
                }
            });

            document.open();

            Paragraph paragraphKosong = new Paragraph("");

            Font headerFont = new Font(narrowFont);
            headerFont.setSize(11);
            headerFont.setStyle(Font.BOLD);

            Font textFont = new Font(narrowFont);
//            textFont.setStyle(11);

            Font headerFontBismillah = new Font(narrowFont);
            headerFontBismillah.setSize(11);
            headerFontBismillah.setStyle(Font.BOLD | Font.ITALIC);

            Font warekFont = new Font(narrowFont);
            warekFont.setSize(11);
            warekFont.setStyle(Font.BOLD | Font.UNDERLINE);

            Image lTazkia = Image.getInstance(logo + File.separator + "logo_tazkia.png");
            lTazkia.scaleToFit(100, 100);
            lTazkia.setAbsolutePosition(20, PageSize.A4.getHeight() - (lTazkia.getScaledHeight()) - 15);
            Paragraph newLine1 = new Paragraph("\n");
            Paragraph newLine2 = new Paragraph("\n\n");
            document.add(lTazkia);

            paragraphKosong = new Paragraph("KEPUTUSAN WAKIL REKTOR BIDANG AKADEMIK DAN KEMAHASISWAAN", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph("INSTITUT AGAMA ISLAM TAZKIA", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);

            Paragraph kodeSurat = new Paragraph("NOMOR : " + mengajarDto.getNomor(), headerFont);
            kodeSurat.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(kodeSurat);

            paragraphKosong = new Paragraph("TENTANG", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph("PELAKSANAAN TRIDHARMA PERGURUAN TINGGI", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph(mengajarDto.getTahun(), headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph("DI INSTITUT AGAMA ISLAM TAZKIA", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);
            document.add(newLine1);

            Paragraph bismillah = new Paragraph("BISMILLAHIRRAHMANIRRAHIM", headerFontBismillah);
            bismillah.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(bismillah);
            document.add(newLine1);

            paragraphKosong = new Paragraph("WAKIL REKTOR BIDANG AKADEMIK DAN KEMAHASISWAAN INSTITUT AGAMA ISLAM TAZKIA", headerFont);
            paragraphKosong.setAlignment(Paragraph.ALIGN_CENTER);
            document.add(paragraphKosong);
            document.add(newLine1);

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.setWidths(new int[] {3, 1, 15});

            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Menimbang :", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("a. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Bahwa untuk menjamin standar mutu yang baik sesuai untuk pencapaian visi dan misi Institut Agama Islam Tazkia, maka perlu menetapkan pedoman pelaksanaan Tridharma Perguruan Tinggi di lingkungan IAI Tazkia;", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("b. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Bahwa dalam rangka kelancaran pelaksanaan Tridharma Perguruan Tinggi  pada " + mengajarDto.getTahun() + ", maka perlu menetapkan pedoman pelaksanaan Tridharma Perguruan Tinggi di lingkungan IAI Tazkia;", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("c. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Bahwa berdasarkan pertimbangan sebagaimana dimaksud pada huruf a dan b perlu menetapkan Keputusan Wakil Rektor Bidang Akademik dan Kemahasiswaan Institut Agama Islam Tazkia tentang Pelaksanaan Tridharma Perguruan Tinggi " + mengajarDto.getTahun() + " di Institut Agama Islam Tazkia.", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Mengingat : ", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("1. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Undang-Undang Republik Indonesia Nomor 20 Tahun 2003 tentang Sistem Pendidikan Nasional (Lembaran Negara Republik Indonesia Tahun 2003 Nomor 78, Tambahan Lembaran Negara Republik Indonesia Nomor 4301);", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("2. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Undang-Undang Republik Indonesia Nomor 12 Tahun 2012 tentang Pendidikan Tinggi (Lembaran Negara Republik Indonesia Tahun 2012 Nomor  158, Tambahan Lembaran Negara Republik Indonesia Nomor 5336)", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("3. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Peraturan Pemerintah Republik Indonesia Nomor 32 Tahun 2013 tentang Perubahan Atas Peraturan Pemerintah Nomor 19 Tahun 2005 tentang Standar Nasional Pendidikan (Lembaran Negara Republik Indonesia Tahun 2013 Nomor 71, Tambahan Lembaran Negara Republik Indonesia Nomor 5410)", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("4. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Peraturan Pemerintah Nomor 4 Tahun 2014 tentang Penyelenggaraan Pendidikan Tinggi dan Pengelolaan Perguruan Tinggi (Lembaran Negara Republik Indonesia Tahun 2014 Nomor 16, Tambahan Lembaran Negara Republik Indonesia Nomor 5500);", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("5. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Peraturan Menteri Riset, Teknologi, dan Pendidikan Tinggi Republik Indonesia Nomor 44 Tahun 2015 tentang Standar Mutu Pendidikan Tinggi (Berita Negara Republik Indonesia Tahun 2015 Nomor 1952)", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("6. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Peraturan Menteri Riset, Teknologi, dan Pendidikan Tinggi Republik Indonesia Nomor 62 Tahun 2016 tentang Sistem Penjamin Mutu Pendidikan Tinggi (Berita Negara Republik Indonesia Tahun 2016 Nomor 1462);", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("7. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Keputusan Menteri Agama Nomor 449 Tahun 2019 tentang Izin Perubahan Bentuk dari Sekolah Tinggi Ekonomi Islam Tazkia menjadi Institut Agama Islam Tazkia;", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("8. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Surat Keputusan Ketua Umum Yayasan Tazkia Cendekia Nomor 16 Tahun 2024 tentang Pengangkatan Rektor Institut Agama Islam Tazkia; (SK Rektor)", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("9. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Surat Keputusan Pengurus Yayasan Tazkia Cendekia Nomor 20 Tahun 2024 tentang Pengesahan Struktur Organisasi Institut Agama Islam Tazkia ;(SK Struktur Organisasi);", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("10. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Surat Keputusan Ketua Umum Yayasan Tazkia Cendekia Nomor17 Tahun 2024 Tentang Pengangkatan Wakil rektor Bidang Akademik dan Kemahasiswaan Institut Agama Islam Tazkia.", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("11. ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Surat Keputusan Ketua Umum Yayasan Tazkia Cendekia Nomor 16 Tahun 2024 tentang Pengangkatan Rektor Institut Agama Islam Tazkia; (SK Rektor)", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            document.add(table);
            document.add(newLine1);

            PdfPTable table2 = new PdfPTable(3);
            table2.setWidthPercentage(100);
            table2.setWidths(new int[] {3, 1, 15});

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("", headerFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("MEMUTUSKAN : \n Dengan Bertawakal Kepada Allah SWT", headerFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("KESATU", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase(" : ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            Phrase satu = new Phrase();
            satu.add(new Chunk("Mengangkat " + mengajarDto.getDosen() + " Untuk mengampu mata kuliah terlampir di ", textFont));
            satu.add(new Chunk(mengajarDto.getTahun(), headerFont));
            satu.add(new Chunk(" di Institut Agama Islam Tazkia", textFont));
            cell = new PdfPCell(new Phrase(satu));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("KEDUA", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase(" : ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("Dosen sebagaimana dimaksud dalam Diktum KESATU mendapatkan hak-hak yang melekat sebagai dosen tetap sesuai dengan  kebijakan Institut Agama Islam Tazkia.", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("KETIGA", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase(" : ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("Keputusan ini dapat ditinjau kembali apabila terjadi kekeliruan.", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("KEEMPAT", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase(" : ", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            cell = new PdfPCell(new Phrase("Keputusan ini berlaku terhitung tanggal " + tglMulai.format(DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("id"))) +" sampai dengan " + tglSelesai.format(DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("id"))) +".", textFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table2.addCell(cell);

            document.add(table2);

            HijrahDate islamicDate = HijrahDate.from(tglMulai);
            String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
            String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("d", new Locale("en")));
            String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

            namaBulanHijri = fixNamaBulanHijri(namaBulanHijri);

            document.add(newLine2);
            paragraphKosong = new Paragraph();
            paragraphKosong.add(new Chunk("Ditetapkan Di Bogor \n ", textFont));
            paragraphKosong
                    .add(new Chunk("Pada tanggal " + tglMulai.format(DateTimeFormatter.ofPattern("dd MMM yyyy", new Locale("id"))) + " M  / ", textFont));
            paragraphKosong.add(new Chunk(tanggalHijri + " " + namaBulanHijri + " " + tahunHijri + " H", textFont));
            paragraphKosong.add(new Chunk(" \n Wakil Rektor Bidang Akademik dan Kemahasiswaan", headerFont));
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            paragraphKosong.setIndentationLeft(300);
            document.add(paragraphKosong);

            document.add(newLine2);
            document.add(newLine2);

            paragraphKosong = new Paragraph(warek.getFullName(), warekFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            paragraphKosong.setIndentationLeft(300);
            document.add(paragraphKosong);

            Image ttd = Image.getInstance(logo + File.separator + "ttd.png");
            ttd.scaleToFit(250, 250);
            ttd.setAbsolutePosition(300, PageSize.A4.getHeight() - 365);
            document.add(ttd);

            paragraphKosong = new Paragraph("NIDN " + lecturer.getNidn() + " / NIK " + warek.getNumber(), headerFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            paragraphKosong.setIndentationLeft(300);
            document.add(paragraphKosong);

            // HALAMAN LAMPIRAN
            document.newPage();

            paragraphKosong = new Paragraph("LAMPIRAN I \n KEPUTUSAN WAKIL REKTOR BIDANG AKADEMIK DAN KEMAHSISWAAN \n INSTITUT AGAMA ISLAM TAZKIA", headerFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph("NOMOR: " + mengajarDto.getNomor(), headerFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph("PELAKSANAAN TRIDHARMA PERGURUAN TINGGI", headerFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            document.add(paragraphKosong);

            paragraphKosong = new Paragraph(mengajarDto.getTahun() + "\n DI INSTITUT AGAMA ISLAM TAZKIA", headerFont);
            paragraphKosong.setAlignment(Element.ALIGN_BASELINE);
            document.add(paragraphKosong);
            document.add(newLine1);

            PdfPTable table3 = new PdfPTable(2);
            table3.setWidthPercentage(100);
            table3.setWidths(new int[] {2, 50});

            cell = new PdfPCell(new Phrase("Pengajaran:", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 1. ", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Mengampu Mata Kuliah ", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            List<DetailDosenDto> listMatkul = mengajarDto.getJadwal();

            for (int i = 0; i < listMatkul.size(); i++) {
                DetailDosenDto matkul = listMatkul.get(i);
                char alphabet = (char) ('a' + i);

                cell = new PdfPCell(new Phrase("", textFont));
                cell.setBorder(Rectangle.LEFT);
                table3.addCell(cell);

                cell = new PdfPCell(new Phrase(alphabet + ". " + matkul.getMatakuliah() + "(" + matkul.getKelas() + ")", textFont));
                cell.setPaddingLeft(10);
                cell.setBorder(Rectangle.RIGHT);
                table3.addCell(cell);
            }

            cell = new PdfPCell(new Phrase(" 2.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Menyusun dan mengembangkan Rencana Pembelajaran Semester (RPS) berbasis  Outcome Based Education (OBE) sesuai mata kuliah yang akan diampu sebelum perkuliahan dimulai;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 3.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Menyusun dan menyampaikan materi perkuliahan sesuai dengan lingkup Rencana Pembelajaran Semester (RPS) berbasis Outcome Based Education (OBE) dengan beragam sumber belajar yang dapat memperkaya proses perkuliahan;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 4.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Menghadiri rapat koordinasi dosen dan atau kegiatan lainnya dalam rangka efektivitas perkuliahan;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 5.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Mengoptimalkan penggunaan smile Tazkia sebagai bagian dari proses perkuliahan;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 6.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Disiplin mengisi berita acara dan kehadiran mahasiswa sesuai dengan jadwal perkuliahan yang terdapat di Smile. ", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 7.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Disiplin dalam perkuliahan termasuk tepat waktu menyerahkan/upload materi perkuliahan, penugasan dan pengumpulan nilai mahasiswa;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 8.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Bersama-sama dengan koordinator mata kuliah mengembangkan sumber belajar dan evaluasi pembelajaran;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 9.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Mengawali dan mengakhiri  perkuliahan dengan doa serta memfasilitasi kegiatan 5T (Tilawah, Tahsin, Tarjamah, Tahsin, Tathbiq);", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 10.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Membimbing mahasiswa agar mempunyai kecerdasan intelegensia, emosional serta spiritual sesuai dengan nilai-nilai ke-Islaman dan ke-Indonesiaan;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 11.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Tidak diperkenankan mengganti pertemuan kelas dengan penugasan dan atau penggabungan kelas diluar jadwal perkuliahan yang telah ditetapkan.", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Pembimbingan:", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 1.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Membimbing Mahasiswa dalam menghasilkan Skripsi/Tesis dan Laporan Magang;", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" 2.", textFont));
            cell.setBorder(Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase(" Membimbing mahasiswa sebagai dosen Wali melalui mekanisme Student Dynamic Session (SDS).", textFont));
            cell.setBorder(Rectangle.RIGHT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Pengujian: ", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Melaksanakan Tugas sebagai Penguji pada Ujian Akhir Mahasiswa (seminar proposal / sidang hasil)", textFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Penelitian: ", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Melaksanakan Penelitian dan menghasilkan karya ilmiah sesuai dengan bidang ilmunya berupa Buku/Jurnal/Proceeding.", textFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Pengabdian Kepada Masyarakat: ", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Melaksanakan Pengabdian Kepada Masyarakat berupa memberikan pelatihan/penyuluhan/ penataran/ceramah pada masyarakat atau aktivitas lainnya yang relevan.", textFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Penunjang: ", headerFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT);
            table3.addCell(cell);

            cell = new PdfPCell(new Phrase("Melaksanakan kegiatan penunjang dalam bentuk terlibat kepanitian dalam kampus dan aktivitas penunjang lainnya.", textFont));
            cell.setColspan(2);
            cell.setBorder(Rectangle.RIGHT | Rectangle.LEFT | Rectangle.BOTTOM);
            table3.addCell(cell);

            document.add(table3);

            document.close();
            return baos.toByteArray();
        }catch (Exception e){
            throw new RuntimeException("Failed Generate PDF", e);
        }
    }
}
