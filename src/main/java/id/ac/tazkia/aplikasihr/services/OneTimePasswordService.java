package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.OneTimePasswordDao;
import id.ac.tazkia.aplikasihr.entity.OneTimePassword;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class OneTimePasswordService {

    @Autowired
    private OneTimePasswordDao oneTimePasswordDao;

    @Autowired
    @Value("${gmail.account.username}")
    private String email;

    @Autowired
    @Value("${gmail.password}")
    private String password;

    @Async
    public void kirimEmailOtp(String mailSent, int Otp) throws MessagingException {

        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", true);
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(email, password);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailSent));
        message.setSubject("One Time Password from HRIS TAZKIA");
        message.setText("This is your OTP Code from HRIS TAZKIA, YOUR OTP CODE: " + Otp +", DO NOT share this code to anyone.");

        Transport.send(message);
    }

//    @Scheduled(fixedDelay = 5000)
    public void nonaktifOtpExpired(){

        LocalDateTime now = LocalDateTime.now();

        List<OneTimePassword> oneTimePasswords = oneTimePasswordDao.findByStatusAndExpires(StatusRecord.AKTIF, now);

        for (OneTimePassword otp : oneTimePasswords){
            otp.setStatus(StatusRecord.NONAKTIF);
            oneTimePasswordDao.save(otp);
        }

    }

}
