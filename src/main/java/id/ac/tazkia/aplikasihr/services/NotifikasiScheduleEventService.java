package id.ac.tazkia.aplikasihr.services;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import groovy.util.logging.Log;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.schedule.ScheduleEventDao;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.dto.setting.schedule.ScheduleEventDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import jakarta.mail.BodyPart;
import jakarta.mail.internet.MimeBodyPart;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@EnableScheduling
public class NotifikasiScheduleEventService {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired private GmailApiService gmailApiService;

    @Autowired private MustacheFactory mustacheFactory;

//    @JsonFormat(pattern = "dd-MMMM-yyyy")
//    private Date dueDate;


    @Scheduled(cron = "0 0 6 * * ?")
    @Async
    public void notifikasiScheduleEvent(){

        System.out.println("Notifikasi event : Masuk");

        List<Employes> employes = employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF");

//        List<Employes> employes = employesDao.findByStatusAndStatusAktifAndId(StatusRecord.AKTIF, "AKTIF", "001");

        for(Employes employ : employes){
            ScheduleEventDto scheduleEventDto = scheduleEventDao.dataScheduleEvent(employ.getId());
            if (scheduleEventDto != null){

                String hari = scheduleEventDto.getScheduleDate().getDayOfWeek().toString();
                Integer tanggal = scheduleEventDto.getScheduleDate().getDayOfMonth();
                String bulan = scheduleEventDto.getScheduleDate().getMonth().toString();
                Integer tahun = scheduleEventDto.getScheduleDate().getYear();
                Integer.toString(tanggal);

                String tanggalLengkap = "Yang Insha Allah akan dilaksanakan pada hari " + hari + " tanggal " + Integer.toString(tanggal) + " - " + bulan + " - " + tahun;
                String waktu = "Mulai pukul " + scheduleEventDto.getScheduleTimeStart() + " WIB sampai dengan " + scheduleEventDto.getScheduleTimeEnd() + " WIB";

                var dynamic_html = scheduleEventDto.getScheduleDescription();

//                document.getElementByID("container").innerHTML = dynamic_html;

                Mustache templateEmail = mustacheFactory.compile("templates/email/notifikasi/schedule_event.html");
                Map<String, String> data = new HashMap<>();
                data.put("nama", scheduleEventDto.getScheduleName());
                data.put("tanggal", tanggalLengkap);
                data.put("waktu", waktu);
                data.put("deskripsi", dynamic_html);


                StringWriter output = new StringWriter();
                templateEmail.execute(output, data);

//                BodyPart messageBodyPart = new MimeBodyPart();
//                messageBodyPart.setText(isi);

                gmailApiService.kirimEmail(
                        "Notifikasi Event Institut Tazkia",
                        employ.getEmailActive(),
                        "Jadwal acara terdekat",
                        output.toString());

            }
        }

    }

}
