package id.ac.tazkia.aplikasihr.services;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanDao;
import id.ac.tazkia.aplikasihr.dto.LunasDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@EnableScheduling
public class PelunasanLoanService {

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Scheduled(fixedDelay = 60000)
    public void pelunasanLoan(){

        List<LunasDto> idEmployeeLoan = employeeLoanDao.cekStatusLoanLunasAktif();
        if(idEmployeeLoan != null){
            for(LunasDto lunasDto : idEmployeeLoan){
                EmployeeLoan employeeLoan = employeeLoanDao.findByStatusAndId(StatusRecord.AKTIF, lunasDto.getId());
                if (employeeLoan != null){
                    employeeLoan.setStatus(lunasDto.getStatus());
                    employeeLoanDao.save(employeeLoan);
                }
            }
        }

    }

}
