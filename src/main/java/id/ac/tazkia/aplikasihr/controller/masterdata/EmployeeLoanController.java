package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanBayarDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LoanDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import id.ac.tazkia.aplikasihr.export.ExportJobLevelClass;
import id.ac.tazkia.aplikasihr.export.finance.ExportEmployeeLoanClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class EmployeeLoanController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @GetMapping("/finance/loan")
    public String employeeLoan(Model model,
                               @RequestParam(required = false) String search,
                               @PageableDefault(size = 10) Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listEmployesLoan", employeeLoanDao.findByStatusNotAndEmployesFullNameContainingIgnoreCaseOrStatusAndLoanNameContainingIgnoreCaseOrderByEffectiveDateDesc(StatusRecord.HAPUS, search, StatusRecord.AKTIF, search, page));
        }else {
            model.addAttribute("listEmployesLoan", employeeLoanDao.findByStatusNotOrderByEffectiveDateDesc(StatusRecord.HAPUS, page));
        }

        model.addAttribute("finance", "active");
        model.addAttribute("loan", "active");
        return "finance/loan/list";

    }

    @GetMapping("/finance/loan/history")
    public String employeeLoanHistory(Model model,
                                      @RequestParam(required = true)EmployeeLoan loan,
                                      @RequestParam(required = false)String search,
                                      RedirectAttributes attribute,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employeeLoan", employeeLoanDao.findByStatusNotAndId(StatusRecord.HAPUS, loan.getId()));
        model.addAttribute("listEmployeeLoanDetail", employeeLoanBayarDao.historyEmployeeLoanBayar(loan.getId()));
        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
        }else{
            model.addAttribute("search", "");
        }

        return "finance/loan/list_history";
    }

    @GetMapping("/finance/loan/new")
    public String newLoan(Model model,
                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employeeLoan", new EmployeeLoan());
        model.addAttribute("listLoan", loanDao.findByStatusOrderByName(StatusRecord.AKTIF));
        model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));

        return "finance/loan/form";
    }

    @GetMapping("/finance/loan/edit")
    public String editLoan(Model model,
                           @RequestParam(required = true)EmployeeLoan employeeLoan,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employeeLoan", employeeLoanDao.findById(employeeLoan.getId()));
        model.addAttribute("listLoan", loanDao.findByStatusOrderByName(StatusRecord.AKTIF));
        model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));

        return "finance/loan/form";
    }

    @PostMapping("/finance/loan/save")
    public String saveLoan(@ModelAttribute @Valid EmployeeLoan employeeLoan,
                           Authentication authentication,
                           RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        String date = employeeLoan.getEffectiveDateString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        employeeLoan.setEffectiveDate(localDate);

        employeeLoan.setUserUpdate(user.getUsername());
        employeeLoan.setDateUpdate(LocalDateTime.now());
        employeeLoan.setStatus(StatusRecord.AKTIF);
        employeeLoan.setInterestType("FLAT");

        employeeLoanDao.save(employeeLoan);

        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";
    }

    @PostMapping("/finance/loan/delete")
    public String deleteLoan(@RequestParam(required = true) EmployeeLoan employeeLoan,
                             Authentication authentication,
                             RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        employeeLoan.setUserUpdate(user.getUsername());
        employeeLoan.setDateUpdate(LocalDateTime.now());
        employeeLoan.setStatus(StatusRecord.HAPUS);
        employeeLoanDao.save(employeeLoan);

        redirectAttributes.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../loan";

    }

    @GetMapping("/finance/loan/export")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=employes_loan_"+ currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<EmployeeLoan> employeeLoanList = employeeLoanDao.findByStatusOrderByEffectiveDateDesc(StatusRecord.AKTIF);

        ExportEmployeeLoanClass excelExporter = new ExportEmployeeLoanClass(employeeLoanList);

        excelExporter.export(response);

    }


    @PostMapping("/finance/loan/import")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        List<EmployeeLoan> employeeLoanList = new ArrayList<EmployeeLoan>();
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());



        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

            XSSFRow row = worksheet.getRow(i);

            BigDecimal satu = new BigDecimal(row.getCell(3).getNumericCellValue());
            BigDecimal dua = new BigDecimal(row.getCell(4).getNumericCellValue());
            BigDecimal tiga = new BigDecimal(row.getCell(5).getNumericCellValue());
            BigDecimal empat = new BigDecimal(row.getCell(6).getNumericCellValue());
            BigDecimal lima = new BigDecimal(row.getCell(7).getNumericCellValue());
            BigDecimal enam = new BigDecimal(row.getCell(8).getNumericCellValue());
            BigDecimal tujuh = new BigDecimal(row.getCell(9).getNumericCellValue());


            Date tanggal = row.getCell(10).getDateCellValue();
            LocalDateTime tanggal2 = new java.sql.Timestamp(tanggal.getTime()).toLocalDateTime();
            String localDate1 = tanggal2.toString().substring(0,10);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(localDate1, formatter);
            String bulan = localDate.toString().substring(5,7);
            String hari = localDate.toString().substring(8,10);
            String tahun = localDate.toString().substring(0,4);

            String dateString = bulan +'/'+hari+'/'+tahun;

            Employes employes = employesDao.findByStatusAndNumber(StatusRecord.AKTIF, row.getCell(0).getStringCellValue());
            Loan loan = loanDao.findByStatusAndName(StatusRecord.AKTIF, row.getCell(2).getStringCellValue());
            if(loan == null){
                Loan loan1 = new Loan();
                loan1.setName(row.getCell(2).getStringCellValue());
                loan1.setStatus(StatusRecord.AKTIF);
                loan1.setDateUpdate(LocalDateTime.now());
                loan1.setUserUpdate(user.getUsername());
                loanDao.save(loan1);
            }
            Loan loan2 = loanDao.findByStatusAndName(StatusRecord.AKTIF, row.getCell(2).getStringCellValue());

            EmployeeLoan employeeLoan = employeeLoanDao.findByStatusAndEmployesNumberAndLoanNameAndAmountAndInstallmentAndInterest(StatusRecord.AKTIF, row.getCell(0).getStringCellValue(), row.getCell(2).getStringCellValue(), satu , dua , tiga);
            if(employeeLoan == null){
                EmployeeLoan employeeLoan1 = new EmployeeLoan();
                employeeLoan1.setEmployes(employes);
                employeeLoan1.setLoan(loan2);
                employeeLoan1.setEffectiveDate(localDate);
                employeeLoan1.setAmount(satu);
                employeeLoan1.setInstallment(dua);
                employeeLoan1.setInstallmentPaid(tiga);
                employeeLoan1.setCurrentPayment(empat);
                employeeLoan1.setTotalPayment(lima);
                employeeLoan1.setRemaining(enam);
                employeeLoan1.setInterest(tujuh);
                employeeLoan1.setInterestType("FLAT");
                employeeLoan1.setStatus(StatusRecord.AKTIF);
                employeeLoan1.setUserUpdate(user.getUsername());
                employeeLoan1.setDateUpdate(LocalDateTime.now());
                employeeLoan1.setEffectiveDateString(dateString);
                employeeLoan1.setDescriptions(row.getCell(11).getStringCellValue());
                employeeLoanDao.save(employeeLoan1);
            }else{
                employeeLoan.setEmployes(employes);
                employeeLoan.setLoan(loan2);
                employeeLoan.setEffectiveDate(localDate);
                employeeLoan.setAmount(satu);
                employeeLoan.setInstallment(dua);
                employeeLoan.setInstallmentPaid(tiga);
                employeeLoan.setCurrentPayment(empat);
                employeeLoan.setTotalPayment(lima);
                employeeLoan.setRemaining(enam);
                employeeLoan.setInterest(tujuh);
                employeeLoan.setInterestType("FLAT");
                employeeLoan.setStatus(StatusRecord.AKTIF);
                employeeLoan.setUserUpdate(user.getUsername());
                employeeLoan.setDateUpdate(LocalDateTime.now());
                employeeLoan.setEffectiveDateString(dateString);
                employeeLoan.setDescriptions(row.getCell(11).getStringCellValue());
                employeeLoanDao.save(employeeLoan);
            }
        }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../loan";
    }

}
