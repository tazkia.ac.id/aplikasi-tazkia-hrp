package id.ac.tazkia.aplikasihr.controller.masterdata;


import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerClassDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerClassKpiDao;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Controller
public class KpiLecturerController {

    @Autowired
    private LecturerClassDao lecturerClassDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private LecturerClassKpiDao lecturerClassKpiDao;

    @Autowired
    private UserDao userDao;

    @GetMapping("/masterdata/kpi_lecturer")
    public String kpiLecturer(Model model,
                              @RequestParam(required = false) String search,
                              @PageableDefault(size = 10) Pageable page,
                              Authentication authentication) {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("lecturerClass", lecturerClassDao.listLecturerClassKpiSearch(search,page));
        }else{
            model.addAttribute("lecturerClass", lecturerClassDao.listLecturerClassKpi(page));
        }



        model.addAttribute("page1",page);
        model.addAttribute("menusettingkpilecturer", "active");
        return "masterdata/kpi_lecturer/list";
    }


    @GetMapping("/masterdata/kpi_lecturer/setting")
    public String kpiLecturerSetting(Model model,
                              @RequestParam(required = true) LecturerClass lecturerClass,
                              @RequestParam(required = false) String search,
                              @RequestParam(required = false) Integer page,
                              @RequestParam(required = false) Integer size,
                              Authentication authentication) {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("search", search);
        model.addAttribute("lecturerClass", lecturerClass);
        model.addAttribute("page",page);
        model.addAttribute("size",size);
        model.addAttribute("lecturerClassKpiList", lecturerClassKpiDao.findByStatusAndLecturerClass(StatusRecord.AKTIF,lecturerClass));
        model.addAttribute("lecturerClassKpi", new LecturerClassKpi());
        model.addAttribute("periodeEvaluasiKpi", PeriodeEvaluasiKpi.values());
        model.addAttribute("menusettingkpilecturer", "active");
        return "masterdata/kpi_lecturer/setting_form";
    }


    @PostMapping("/masterdata/kpi_lecturer/setting/save")
    public String lecturerClassKpiSave(Model model,
                                  @ModelAttribute @Valid LecturerClassKpi lecturerClassKpi,
                                  BindingResult bindingResult,
                                  @RequestParam(required = true) LecturerClass lecturerClass,
                                  @RequestParam(required = false) Integer page,
                                  @RequestParam(required = false) Integer size,
                                  @RequestParam(required = false) String search,
                                  RedirectAttributes redirectAttributes,
                                  Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);


        // Check for validation errors
        if (bindingResult.hasErrors()) {
            model.addAttribute("eroredit", null);
            model.addAttribute("employess", employess);
            model.addAttribute("lecturerClass", lecturerClass);
            model.addAttribute("lecturerClassKpiList", lecturerClassKpiDao.findByStatusAndLecturerClass(StatusRecord.AKTIF,lecturerClass));
            model.addAttribute("periodeEvaluasiKpi", PeriodeEvaluasiKpi.values());
            model.addAttribute("lecturerClassKpi", lecturerClassKpi); // Retain the submitted data
            model.addAttribute("search", search);
            model.addAttribute("page", page);
            model.addAttribute("size", size);
            model.addAttribute("menusettingkpi", "active");
            // Here, you need to return the modal view directly if you're using Thymeleaf or similar
            return "masterdata/kpi_lecturer/setting_form"; // Assuming you have a fragment for the modal
        }

        System.out.println("lecturerClass : "+ lecturerClass.getId());
        lecturerClassKpi.setLecturerClass(lecturerClass);
        lecturerClassKpi.setStatus(StatusRecord.AKTIF);
        lecturerClassKpi.setDateUpdate(LocalDateTime.now());
        lecturerClassKpi.setUserUpdate(user.getUsername());
        String cleanedString = lecturerClassKpi.getNominalString().replace(".", "");
        lecturerClassKpi.setNominal(new BigDecimal(cleanedString));
        lecturerClassKpiDao.save(lecturerClassKpi);


        redirectAttributes.addFlashAttribute("berhasil", "Save Data success");
        return "redirect:../setting?lecturerClass="+ lecturerClass.getId()+"&search="+search+"&page="+page+"&size="+size;
    }


    @GetMapping("/masterdata/kpi_lecturer/setting/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getPositionKpiLecturerData(@PathVariable("id") String id) {
        try {
            LecturerClassKpi lecturerClassKpi = lecturerClassKpiDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid KPI Lecturer ID: " + id));
            return ResponseEntity.ok(lecturerClassKpi);  // Jika ditemukan, return 200 OK dengan data KPI
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }

    @PostMapping("/masterdata/kpi_lecturere/setting/delete")
    public String positionKpiDelete(@RequestParam LecturerClassKpi lecturerClassKpi,
                                    @RequestParam(required = true) LecturerClass lecturerClass,
                                    @RequestParam(required = false) Integer page,
                                    @RequestParam(required = false) Integer size,
                                    @RequestParam(required = false) String search,
                                    RedirectAttributes redirectAttributes,
                                    Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        System.out.println("ID : "+ lecturerClassKpi.getId());
        System.out.println("jobposition : "+ lecturerClass.getId());
        lecturerClassKpi.setStatus(StatusRecord.HAPUS);
        lecturerClassKpi.setDateUpdate(LocalDateTime.now());
        lecturerClassKpi.setUserUpdate(user.getUsername());
        lecturerClassKpiDao.save(lecturerClassKpi);

        redirectAttributes.addFlashAttribute("berhasil","Delete Data success");
        return "redirect:../setting?position="+ lecturerClass.getId()+"&search="+search+"&page="+page+"&size="+size;
    }


}
