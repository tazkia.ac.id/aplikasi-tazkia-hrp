package id.ac.tazkia.aplikasihr.controller.attendance;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.*;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturerImporProcess;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class AttendanceLecturerController {


    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private AttendanceLecturerImporProcessDao attendanceLecturerImporProcessDao;

    @Autowired
    private AttendanceLecturerImporProcessDetailDao attendanceLecturerImporProcessDetailDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/attendance/lecturer/impor")
    public String imporAttendanceLecturer(Model model,
                                          @RequestParam(required = false) String companies,
                                          @RequestParam(required = false) String bulan,
                                          @RequestParam(required = false) String tahun,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(companies != null && tahun != null && bulan != null){
            model.addAttribute("companiessSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }


        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("listAttendanceRunProcess", attendanceLecturerImporProcessDao.listAttendanceLecturerImporProcess());


        return "attendance/impor/attendance_lecturer";
    }

    @PostMapping("/attendance/lecturer/impor/save")
    public String prosesRunPayroll(Model model,
                                   @RequestParam(required = true) String companies,
                                   @RequestParam(required = true) String bulan,
                                   @RequestParam(required = true) String tahun,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        AttendanceLecturerImporProcess attendanceLecturerImporProcess = new AttendanceLecturerImporProcess();
        attendanceLecturerImporProcess.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
        attendanceLecturerImporProcess.setTahun(tahun);
        attendanceLecturerImporProcess.setBulan(bulan);
        attendanceLecturerImporProcess.setTanggalInput(LocalDateTime.now());
        attendanceLecturerImporProcess.setStatus(StatusRecord.WAITING);
        attendanceLecturerImporProcess.setUserInput(employes.getFullName());
        attendanceLecturerImporProcessDao.save(attendanceLecturerImporProcess);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../impor?companies="+ companies +"&tahun="+ tahun + "&bulan" + bulan;

    }


    @GetMapping("/attendance/lecturer/impor/detail_error")
    public String listAttendanceDetailError(Model model,
                                            @RequestParam(required = true) AttendanceLecturerImporProcess attendanceLecturerImporProcess,
                                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listDetailError", attendanceLecturerImporProcessDetailDao.findByStatusAndAttendanceLecturerImporProcessOrderByTopik(StatusRecord.ERROR, attendanceLecturerImporProcess));


        return "attendance/impor/detail";
    }

    @GetMapping("/attendance/lecturer/impor/detail_success")
    public String listAttendanceDetaiSuccess(Model model,
                                            @RequestParam(required = true) AttendanceLecturerImporProcess attendanceLecturerImporProcess,
                                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listDetailError", attendanceLecturerImporProcessDetailDao.findByStatusAndAttendanceLecturerImporProcessOrderByTopik(StatusRecord.DONE, attendanceLecturerImporProcess));


        return "attendance/impor/detail";
    }

}
