package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.BankDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Bank;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class BankController {

    @Autowired
    private BankDao bankDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;



    @GetMapping("masterdata/bank")
    public String listBank(Model model,
                           @PageableDefault(size = 10)Pageable page,
                           @RequestParam(required = false) String search,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listBank", bankDao.findByStatusAndKodeBankContainingOrStatusAndNamaBankContainingIgnoreCaseOrderByNamaBank(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
        }else {
            model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF, page));
        }

        model.addAttribute("submenubank", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("setting", "active");
        return "masterdata/bank/list";

    }

    @GetMapping("masterdata/bank/new")
    public String addBank(Model model,
                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("bank", new Bank());

        model.addAttribute("submenubank", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("setting", "active");
        return "masterdata/bank/form";
    }

    @GetMapping("masterdata/bank/edit")
    public String editBank(Model model,
                           @RequestParam(required = true) Bank bank,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("bank", bank);

        model.addAttribute("submenubank", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("setting", "active");
        return "masterdata/bank/form";

    }

    @PostMapping("masterdata/bank/save")
    public String saveBank(@ModelAttribute @Valid Bank bank,
                           Authentication authentication,
                           RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        bank.setUserUpdate(user.getUsername());
        bank.setDateUpdate(LocalDateTime.now());
        bank.setStatus(StatusRecord.AKTIF);

        bankDao.save(bank);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../bank";

    }

    @PostMapping("/masterdata/bank/delete")
    public String deleteBank(@ModelAttribute @Valid Bank bank,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        bank.setStatus(StatusRecord.HAPUS);
        bank.setUserUpdate(user.getUsername());
        bank.setDateUpdate(LocalDateTime.now());

        bankDao.save(bank);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../bank";

    }


}
