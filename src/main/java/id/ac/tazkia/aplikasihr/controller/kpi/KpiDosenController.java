package id.ac.tazkia.aplikasihr.controller.kpi;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.kpi.*;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dto.kpi.EditDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiLecturerDto;
import id.ac.tazkia.aplikasihr.dto.kpi.SaveKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.kpi.*;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerClass;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.KpiLecturerService;
import id.ac.tazkia.aplikasihr.services.KpiService;
import id.ac.tazkia.aplikasihr.services.ResizeFIleService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller@Slf4j
public class KpiDosenController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PengisianKpiDao pengisianKpiDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;

    @Autowired
    private PengisianKpiEvidanceDao pengisianKpiEvidanceDao;

    @Autowired
    private LecturerClassDao lecturerClassDao;

    @Autowired
    private LecturerClassKpiDao lecturerClassKpiDao;

    @Autowired
    private KpiService kpiService;

    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private KpiLecturerService kpiLecturerService;
    @Autowired
    private PengisianKpiEvidanceDosenDao pengisianKpiEvidanceDosenDao;
    @Autowired
    private PengisianKpiDosenDao pengisianKpiDosenDao;

    @Autowired EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private PenilaianKpiDosenDao penilaianKpiDosenDao;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private ResizeFIleService resizeFIleService;

    @GetMapping("/kpi_lecturer")
    public String kpiTendik(Model model,
                            @PageableDefault(size = 10) Pageable pageable,
                            @RequestParam(required = false) String bulan,
                            @RequestParam(required = false) String tahun,
                            Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        model.addAttribute("employess", employes);
        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            bulan = monthString;
            tahun = tahunString;

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);
        }


        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1) {
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1) {
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));


        if (employes != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }


            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
            Page<ListKpiLecturerDto> data = kpiLecturerService.getKpiListLecturer(lecturer, bulan, tahun, pageable);
            model.addAttribute("listPengisianKpiDosen", data);
        }

        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpidosen", "active");
        model.addAttribute("menukpilecturer", "active");
//        model.addAttribute("menukpistaff", "active");
        return "kpi/dosen/list";

    }

    @GetMapping("/kpi_lecturer/add")
    public String kpiTendikAdd(Model model,
                               Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        model.addAttribute("employess", employes);

        if(lecturer.getLecturerClass() != null) {
            System.out.println("Bulan kpi : " + LocalDate.now().getMonthValue());
            if(LocalDate.now().getMonthValue() == 8){
                model.addAttribute("listKpi", lecturerClassKpiDao.findByStatusAndLecturerClass(StatusRecord.AKTIF,lecturer.getLecturerClass()));
            }else if (LocalDate.now().getMonthValue() == 2){
                model.addAttribute("listKpi", lecturerClassKpiDao.findByStatusAndLecturerClassAndPeriodeEvaluasiIn(
                        StatusRecord.AKTIF,
                        lecturer.getLecturerClass(),
                        Arrays.asList(PeriodeEvaluasiKpi.HARIAN, PeriodeEvaluasiKpi.BULANAN, PeriodeEvaluasiKpi.SEMESTERAN)
                ));
            }else{
                model.addAttribute("listKpi", lecturerClassKpiDao.findByStatusAndLecturerClassAndPeriodeEvaluasiIn(
                        StatusRecord.AKTIF,
                        lecturer.getLecturerClass(),
                        Arrays.asList(PeriodeEvaluasiKpi.HARIAN, PeriodeEvaluasiKpi.BULANAN)
                ));
            }
            model.addAttribute("pengisianKpiDosen", new PengisianKpiDosen());
            model.addAttribute("menukpi", "active");
            model.addAttribute("menukpidosen", "active");
            model.addAttribute("menukpilecturer", "active");

            return "kpi/dosen/form";
        }else{
            model.addAttribute("error", "Mohon maaf, anda tidak memiliki KPI sebagai dosen karena jabatan dosen anda kosong, untuk informasi lebih lanjut silahkan hubungi HRD ");
            model.addAttribute("menukpi", "active");
            model.addAttribute("menukpitendik", "active");
            model.addAttribute("menukpistaff", "active");
            return "kpi/dosen/error";
        }
    }

    @GetMapping("/kpi_lecturer/edit")
    public String getEdit(@RequestParam PengisianKpiDosen id,
                          Model model,
                          Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("pengisianKpiDosen", id);
        model.addAttribute("listEvidance", pengisianKpiEvidanceDosenDao.findByPengisianKpiDosenAndStatus(id, StatusRecord.AKTIF));
        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpitendik", "active");
        model.addAttribute("menukpistaff", "active");
        return "kpi/dosen/edit";
    }

    @PostMapping("/kpi_lecturer/save")
    public String saveKpi(@Valid SaveKpiEmployeeDto saveKpiEmployeeDto, @RequestParam("file") MultipartFile[] file,
                          Authentication authentication, RedirectAttributes redirectAttributes){
        try{
            saveKpiEmployeeDto.setFiles(file);
            kpiLecturerService.savePengisianKpiLecturer(authentication, saveKpiEmployeeDto);
            redirectAttributes.addFlashAttribute("success", "Berhasil disimpan");
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(e.getLocalizedMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi_lecturer";
    }

    @PostMapping("/kpi_lecturer/editevidance")
    public String editEvidance(@Valid EditDto editDto, RedirectAttributes redirectAttributes,
                               String[] idEvidance, @RequestParam(required = false, name = "file") MultipartFile[] file){
        try {
            System.out.println("isi : "+ editDto);
            editDto.setIdEvidance(idEvidance);
            editDto.setFiles(file);
            kpiLecturerService.editKpiDosen(editDto);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(e.getLocalizedMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi_lecturer";
    }

    @GetMapping("/kpi_lecturer/delete")
    public String deleteKpiLecturer(@RequestParam String id, RedirectAttributes redirectAttributes){
        try {
            kpiLecturerService.deleteListLectirerKpi(id);
            redirectAttributes.addFlashAttribute("success", "Berhasil dihapus");
        }catch (Exception e){
            log.error(e.getMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi_lecturer";
    }

    @PostMapping("/kpi_lecturer/add_evidance")
    public String tambahEvidanceKpiLecturer(@RequestParam String dataPengisianKpi,
                                                    @RequestParam("evidance") MultipartFile file,
                                                    Authentication authentication,
                                                    RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        PengisianKpiDosen pengisianKpiDosen = pengisianKpiDosenDao.findById(dataPengisianKpi).get();
        PengisianKpiEvidanceDosen pengisianKpiEvidanceDosen = new PengisianKpiEvidanceDosen();
        pengisianKpiEvidanceDosen.setPengisianKpiDosen(pengisianKpiDosen);
        pengisianKpiEvidanceDosen.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile1 = UUID.randomUUID().toString();
        pengisianKpiEvidanceDosen.setFile(idFile1 + "." + extension);
        new File(uploadKinerja).mkdirs();
        File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
        file.transferTo(tujuan2);

        if (file.getContentType().equalsIgnoreCase("application/pdf")) {
            resizeFIleService.resizeFilePdf(tujuan2);
        } else {
            // Resize file image jika bukan PDF
            resizeFIleService.resizeFileImage(tujuan2);
        }

//        pengisianKpiEvidance.setExtensi(extension);
        pengisianKpiEvidanceDosen.setStatus(StatusRecord.AKTIF);
        pengisianKpiEvidanceDosenDao.save(pengisianKpiEvidanceDosen);

        redirectAttributes.addFlashAttribute("success", "Dokumen kinerja berhasil di tambahkan");
        return"redirect:../kpi_lecturer";
    }

    @GetMapping("/kpi_lecturer/result")
    public String showKpiLecturer(Authentication authentication, Model model,
                                  @RequestParam(required = false) String bulan, @RequestParam(required = false) String tahun){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);

        String modelBulan = "";
        String modelTahun = "";
        Set<PenilaianKpiDosen> penilaianKpiSet = new LinkedHashSet<>();

        List<LecturerClassKpi> lecturerClassKpis = lecturerClassKpiDao.findByStatusAndLecturerClass(StatusRecord.AKTIF, lecturer.getLecturerClass());
        for (LecturerClassKpi dataKpi : lecturerClassKpis) {
            if (bulan == null && tahun == null){
                String bln = String.valueOf(LocalDate.now().getMonthValue());
                String thn = String.valueOf(LocalDate.now().getYear());
                PenilaianKpiDosen penilaianKpi = penilaianKpiDosenDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(
                        StatusRecord.AKTIF, lecturer, bln, thn, dataKpi
                );

                if (penilaianKpi != null) {
                    penilaianKpiSet.add(penilaianKpi);
                }
                modelBulan = bln;
                modelTahun = thn;
            }else {
                PenilaianKpiDosen penilaianKpi = penilaianKpiDosenDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(
                        StatusRecord.AKTIF, lecturer, bulan, tahun, dataKpi
                );

                if (penilaianKpi != null) {
                    penilaianKpiSet.add(penilaianKpi);
                }
                modelBulan = bulan;
                modelTahun = tahun;
            }
        }

        List<PenilaianKpiDosen> penilaianKpiList = new ArrayList<>(penilaianKpiSet);
        model.addAttribute("listPenilaianKpi", penilaianKpiList);
        model.addAttribute("listBulan", bulanDao.findByStatusOrderById(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("modelBulan", modelBulan);
        model.addAttribute("modelTahun", modelTahun);
        model.addAttribute("menukpilecturerresult", "active");
        return "kpi/dosen/result";
    }
}
