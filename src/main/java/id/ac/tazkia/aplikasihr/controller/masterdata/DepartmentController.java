package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.DepartmentClassDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.DepartmentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class DepartmentController {

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private DepartmentClassDao departmentClassDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/masterdata/department")
    public String listDepartment(Model model,
                                 @RequestParam(required = false) String search,
                                 @PageableDefault(size = 10)Pageable page,
                                 @RequestParam(required = false) Integer size,
                                 @RequestParam(required = false) Department department,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
            } else {
                model.addAttribute("listDepartment", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF, page));
            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord.AKTIF, idDepartements, search, StatusRecord.AKTIF, search, page));
            } else {
                model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord.AKTIF,idDepartements, page));
            }
        }

        if (department == null){
            model.addAttribute("department", new Department());
        }else{
            model.addAttribute("department", department);
        }
        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusOrderByCompanyName(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusAndIdInOrderByCompanyName(StatusRecord.AKTIF, idDepartements));
        }

        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("departmentClass", departmentClassDao.findByStatusOrderByClassName(StatusRecord.AKTIF));
        model.addAttribute("menudepartment", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/department/list";

    }

    @GetMapping("/masterdata/department/new")
    public String newDepartment(Model model,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("department", new Department());
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusOrderByCompanyName(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusAndIdInOrderByCompanyName(StatusRecord.AKTIF, idDepartements));
        }
        model.addAttribute("menudepartment", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/department/form";

    }

    @GetMapping("/masterdata/department/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getDepartmentData(@PathVariable("id") String id) {
        try {
            Department department = departmentDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid Department ID: " + id));
            return ResponseEntity.ok(department);  // Jika ditemukan, return 200 OK dengan data Department
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }

    @GetMapping("/masterdata/department/edit")
    public String editDepartment(Model model,
                                 @RequestParam(required = false) Department department,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("department", department);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusOrderByCompanyName(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusAndIdInOrderByCompanyName(StatusRecord.AKTIF, idDepartements));
        }
        model.addAttribute("menudepartment", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/department/form";

    }

    @PostMapping("/masterdata/department/save")
    public String saveDepartment(@ModelAttribute @Valid Department department,
                                 BindingResult bindingResult,
                                 Model model,
                                 @RequestParam(required = false) Integer size,
                                 @RequestParam(required = false) String search, Pageable page,
                                 Authentication authentication,
                                 RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        if (bindingResult.hasErrors()) {
            model.addAttribute("eroredit", null);
            model.addAttribute("employess", employess);
            UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

            if(role != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
                } else {
                    model.addAttribute("listDepartment", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF, page));
                }
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInAndDepartmentNameContainingIgnoreCaseOrStatusAndDepartmentCodeContainingIgnoreCaseOrderByDepartmentCode(StatusRecord.AKTIF, idDepartements, search, StatusRecord.AKTIF, search, page));
                } else {
                    model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord.AKTIF,idDepartements, page));
                }
            }

            if(role != null) {
                model.addAttribute("listCompanies", companiesDao.findByStatusOrderByCompanyName(StatusRecord.AKTIF));
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
                model.addAttribute("listCompanies", companiesDao.findByStatusAndIdInOrderByCompanyName(StatusRecord.AKTIF, idDepartements));
            }
            model.addAttribute("department", department);
            model.addAttribute("search", search);
            model.addAttribute("page", page);
            model.addAttribute("size", size);
            model.addAttribute("departmentClass", departmentClassDao.findByStatusOrderByClassName(StatusRecord.AKTIF));
            model.addAttribute("menudepartment", "active");
            model.addAttribute("masterdata", "active");
            // Here, you need to return the modal view directly if you're using Thymeleaf or similar
            return "masterdata/department/list"; // Assuming you have a fragment for the modal
        }

        department.setStatus(StatusRecord.AKTIF);
        department.setUserUpdate(user.getUsername());
        department.setDateUpdate(LocalDateTime.now());

        departmentDao.save(department);
        attribute.addFlashAttribute("berhasil", "Save Data Success");
        return "redirect:../department?search=" + search + "&page=" + page.getPageNumber() + "&size=" + page.getPageSize();

    }

    @PostMapping("/masterdata/department/delete")
    public String deleteDepartment(@RequestParam(required = false) Integer size,
                                   @RequestParam(required = false) String search,
                                   @RequestParam(required = false) Department department,
                                   @RequestParam(required = false) Integer page,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        department.setStatus(StatusRecord.HAPUS);
        department.setUserUpdate(user.getUsername());
        department.setDateUpdate(LocalDateTime.now());

        departmentDao.save(department);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../department?search=" + search + "&page=" + page + "&size=" + size;

    }

}
