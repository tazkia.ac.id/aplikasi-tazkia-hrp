package id.ac.tazkia.aplikasihr.controller.payroll;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanBayarDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.loan.HistoryLoanDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.export.ExportEmployesClass;
import id.ac.tazkia.aplikasihr.export.loan.ExportHistoryLoanClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class PayrollReportsLoanController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;


    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/payroll/reports/loan")
    public String payrollReportsLoan(Model model,
                                     @RequestParam(required = false) Companies companies,
                                     @RequestParam(required = false) String bulan,
                                     @RequestParam(required = false) String tahun,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companies);
        }

        if(bulan != null && tahun != null && companies != null){
            model.addAttribute("listHistoryLoan", employeeLoanBayarDao.historyLoanBayar(bulan, tahun, companies.getId()));
        }

        return "payroll/report/loan";
    }


    @PostMapping("/payroll/reports/loan/excel")
    public void loanHistoryToExcel(@RequestParam(required = false) Companies companies,
                                     @RequestParam(required = false) String bulan,
                                     @RequestParam(required = false) String tahun,
                                     HttpServletResponse response,
                                     Authentication authentication) throws IOException{

        User user = currentUserService.currentUser(authentication);

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=History_Loan_"+ companies.getCompanyName() +"_" + bulan + "_" + tahun +".xlsx";
        response.setHeader(headerKey, headerValue);

        List<HistoryLoanDto> historyLoanDtos = employeeLoanBayarDao.historyLoanBayar(bulan, tahun, companies.getId());
        ExportHistoryLoanClass excelExporter = new ExportHistoryLoanClass(historyLoanDtos);
        excelExporter.export(response);


    }

}
