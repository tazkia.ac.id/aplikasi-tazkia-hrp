package id.ac.tazkia.aplikasihr.controller.request;

import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.request.LoanRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.LoanRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.LoanApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequest;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequestApproval;
import id.ac.tazkia.aplikasihr.entity.transaction.EmployeeLoanBayar;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class LoanRequestController {

    @Autowired
    private LoanRequestDao loanRequestDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployesLuarDao employesLuarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private LoanRequestApprovalDao loanRequestApprovalDao;

    @Autowired
    private LoanApprovalSettingDao loanApprovalSettingDao;

    @GetMapping("/vendor/request/loan")
    public String listRequestLoan(Model model,
                                  @RequestParam(required = false)String search,
                                  @PageableDefault(size = 10)Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        model.addAttribute("employess", employesLuarDao.findByStatusAndUser(StatusRecord.AKTIF, user));
        model.addAttribute("listRequestLoan", loanRequestDao.findByStatusOrderByDateUpdate(StatusRecord.AKTIF, page));
        model.addAttribute("listRequestLoanApproval", loanRequestApprovalDao.findByStatusOrderByLoanRequest(StatusRecord.AKTIF));
        model.addAttribute("listLoanApprovalSetting", loanApprovalSettingDao.findByStatusOrderBySequence(StatusRecord.AKTIF));

        return "request/loan/list";
    }

    @GetMapping("/vendor/request/loan/new")
    public String addLoanRequest(Model model,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        model.addAttribute("employess", employesLuarDao.findByStatusAndUser(StatusRecord.AKTIF, user));

        model.addAttribute("listLoan", loanDao.findByStatusAndVendorOrderByName(StatusRecord.AKTIF, employesLuarDao.findByStatusAndUser(StatusRecord.AKTIF, user).getVendor()));
        model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));
        model.addAttribute("loanRequest", new LoanRequest());

        return "request/loan/form";
    }

    @PostMapping("/vendor/request/loan/save")
    public String saveLoanRequest(@ModelAttribute @Valid LoanRequest loanRequest,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        BigDecimal cicilan = new BigDecimal(loanRequest.getInstallment());
        BigDecimal bayar = loanRequest.getNominalEstimate().divide(cicilan);
        BigDecimal persentase = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal sisa = BigDecimal.ZERO;
        BigDecimal jumlahCicilan = BigDecimal.ZERO;
        BigDecimal totalAllowance = BigDecimal.ZERO;
        BigDecimal nominalCicilan = loanRequest.getNominalEstimate().divide(new BigDecimal(loanRequest.getInstallment()));

        //Cari cicilan existing
        List<EmployeeLoan> employeeLoanList = employeeLoanDao.findByStatusAndEmployes(StatusRecord.AKTIF, loanRequest.getEmployes());
        if(employeeLoanList != null){
            for(EmployeeLoan el : employeeLoanList){
                BigDecimal jml = employeeLoanBayarDao.jumlahPembayaran(el.getId());
                total = el.getTotalPayment().add(jml);
                if(el.getAmount().compareTo(total) > 0){
                    jumlahCicilan = jumlahCicilan.add(el.getCurrentPayment());
                }
            }
        }

        Integer loanApprovalTotal = loanApprovalSettingDao.countByStatus(StatusRecord.AKTIF);

        jumlahCicilan = jumlahCicilan.add(nominalCicilan);
        totalAllowance = employeePayrollComponentDao.totalAllowance(loanRequest.getEmployes().getId());

        BigDecimal pengali = new BigDecimal(100);
        persentase = jumlahCicilan.multiply(pengali);
        persentase = persentase.divide(totalAllowance,2, RoundingMode.HALF_UP);

//        totalZakat = totalPenghasilan.multiply(pengaliZakat);
//        totalZakat = totalZakat.divide(BigDecimal.valueOf(100.00),2, RoundingMode.HALF_UP);

        loanRequest.setRequestDate(LocalDateTime.now());
        loanRequest.setDateUpdate(LocalDateTime.now());
        loanRequest.setUserUpdate(user.getUsername());
        loanRequest.setNominalCicilan(nominalCicilan);
        loanRequest.setEstimatePersentase(persentase);
        loanRequest.setSequence(1);
        loanRequest.setTotalSequence(loanApprovalTotal);
        loanRequest.setStatusApprove(StatusRecord.WAITING);

        loanRequestDao.save(loanRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";
    }


    @PostMapping("/vendor/request/loan/delete")
    public String deleteLoanRequest(@ModelAttribute @Valid LoanRequest loanRequest,
                                  Authentication authentication,
                                  RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        List<LoanRequestApproval> loanRequestApprovalList = loanRequestApprovalDao.findByStatusAndLoanRequest(StatusRecord.AKTIF, loanRequest);

        if (loanRequestApprovalList != null){
            for (LoanRequestApproval lra : loanRequestApprovalList){
                lra.setStatus(StatusRecord.HAPUS);
                lra.setStatusApprove(StatusRecord.HAPUS);
                loanRequestApprovalDao.save(lra);
            }
        }

        loanRequest.setStatus(StatusRecord.HAPUS);
        loanRequest.setStatusApprove(StatusRecord.HAPUS);
        loanRequest.setUserUpdate(user.getUsername());
        loanRequest.setDateUpdate(LocalDateTime.now());

        loanRequestDao.save(loanRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";

    }

}
