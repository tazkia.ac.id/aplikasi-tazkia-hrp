package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.DayOffDao;
import id.ac.tazkia.aplikasihr.dao.setting.DayOffEmployeeAllowDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffEmployeeAllow;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffPatternScheduleAllow;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class DayOffEmployeeAllowController {

    @Autowired
    private DayOffDao dayOffDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private DayOffEmployeeAllowDao dayOffEmployeeAllowDao;

    @GetMapping("/setting/day/off/employee_allowed")
    public String listDayOffShiftAllowed(Model model,
                                         @RequestParam(required = false) DayOff dayOff,
                                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listEmployeeAllow", dayOffEmployeeAllowDao.listDayOffEmployee(dayOff.getId()));
        model.addAttribute("dayOff", dayOff);

        return "setting/day_off/allow_employee";

    }

    @GetMapping("/setting/day/off/employee_allowed/save")
    public String saveDayOffShiftAllowed(@RequestParam(required = false) List<String> dayOffEmployeeAllowDto,
                                         @RequestParam(required = true) DayOff dayOff,
                                         Authentication authentication,
                                         RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);


        List<Employes> employess = employesDao.findByStatusOrderByFullName(StatusRecord.AKTIF);
        for(Employes b : employess){

            DayOffEmployeeAllow dayOffEmployeeAllow = dayOffEmployeeAllowDao.findByStatusAndDayOffAndEmployes(StatusRecord.AKTIF, dayOff, b);

            if (dayOffEmployeeAllow != null) {
                dayOffEmployeeAllow.setStatus(StatusRecord.NONAKTIF);
                dayOffEmployeeAllow.setUserUpdate(user.getUsername());
                dayOffEmployeeAllow.setDateUpdate(LocalDateTime.now());
                dayOffEmployeeAllowDao.save(dayOffEmployeeAllow);
            }

        }

        if (dayOffEmployeeAllowDto != null) {
            for (String a : dayOffEmployeeAllowDto) {

                DayOffEmployeeAllow dayOffEmployeeAllow = dayOffEmployeeAllowDao.findByDayOffAndEmployes(dayOff, employesDao.findById(a).get());
                if (dayOffEmployeeAllow == null) {
                    DayOffEmployeeAllow dayOffEmployeeAllow1 = new DayOffEmployeeAllow();
                    dayOffEmployeeAllow1.setDayOff(dayOff);
                    dayOffEmployeeAllow1.setEmployes(employesDao.findById(a).get());
                    dayOffEmployeeAllow1.setDateUpdate(LocalDateTime.now());
                    dayOffEmployeeAllow1.setStatus(StatusRecord.AKTIF);
                    dayOffEmployeeAllow1.setUserUpdate(user.getUsername());
                    dayOffEmployeeAllowDao.save(dayOffEmployeeAllow1);
                } else {
                    DayOffEmployeeAllow dayOffEmployeeAllow1 = dayOffEmployeeAllow;
                    dayOffEmployeeAllow1.setDayOff(dayOff);
                    dayOffEmployeeAllow1.setEmployes(employesDao.findById(a).get());
                    dayOffEmployeeAllow1.setDateUpdate(LocalDateTime.now());
                    dayOffEmployeeAllow1.setStatus(StatusRecord.AKTIF);
                    dayOffEmployeeAllow1.setUserUpdate(user.getUsername());
                    dayOffEmployeeAllowDao.save(dayOffEmployeeAllow1);
                }

            }
        }
        return "redirect:../../off";

    }

}
