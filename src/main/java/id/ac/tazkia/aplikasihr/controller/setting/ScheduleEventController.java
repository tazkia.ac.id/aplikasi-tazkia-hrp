package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.schedule.ScheduleEventDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Controller
public class ScheduleEventController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private CompaniesDao companiesDao;


    @GetMapping("/setting/schedule/event")
    public String scheduleDailyList(Model model,@PageableDefault(size = 10) Pageable page,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listScheduleEvent", scheduleEventDao.findByStatusOrderByScheduleDateDesc(StatusRecord.AKTIF, page));

        model.addAttribute("menuscheduleevent", "active");

        return "setting/schedule/event/list";
    }

    @GetMapping("/setting/schedule/event/new")
    public String scheduleEventNew(Model model,
                                   Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("scheduleEvent", new ScheduleEvent());

        return "setting/schedule/event/form";

    }

    @PostMapping("/setting/schedule/event/save")
    public String scheduleEventSave(@ModelAttribute ScheduleEvent scheduleEvent,
                                    Authentication authentication,
                                    Model model,
                                    RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        if(scheduleEvent.getScheduleDateString() != null && scheduleEvent.getScheduleTimeStartString().length() > 4 && scheduleEvent.getScheduleTimeEndString().length() > 4){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(scheduleEvent.getScheduleDateString(), formatter);

            scheduleEvent.setScheduleDate(localDate);

            String jamMulai = scheduleEvent.getScheduleTimeStartString();
            String jam = "00";
            String menit = "00";
            String detik = "00";
            Integer jamInt = 00;
            String jam2 = "00";
            String menit2 = "00";
            String detik2 = "00";
            Integer jamInt2 = 00;

            if(jamMulai.length() == 4){
                jam = jamMulai.substring(0,1);
                menit = jamMulai.substring(2,4);
            } else if (jamMulai.length() == 5){
                jam = jamMulai.substring(0,2);
                menit = jamMulai.substring(3,5);
            } else if (jamMulai.length() == 7){
                jam = jamMulai.substring(0,1);
                menit = jamMulai.substring(2,4);
                jamInt = new Integer(jam);
                if (jamMulai.substring(5,7).equals("PM")){
                    jamInt = jamInt + 12;
                }
                jam = jamInt.toString();
                if (jam.length() == 1){
                    jam = '0'+jam;
                }
            }else{
                jam = jamMulai.substring(0,2);
                menit = jamMulai.substring(3,5);
                jamInt = new Integer(jam);
                if (jamMulai.substring(6,8).equals("PM")){
                    jamInt = jamInt + 12;
                }
                jam = jamInt.toString();
                if (jam.length() == 1){
                    jam = '0'+jam;
                }
            }

            String jamSelesai = scheduleEvent.getScheduleTimeEndString();

            if(jamSelesai.length() == 4){
                jam2 = jamSelesai.substring(0,1);
                menit2 = jamSelesai.substring(2,4);
            } else if (jamSelesai.length() == 5){
                jam2 = jamSelesai.substring(0,2);
                menit2 = jamSelesai.substring(3,5);
            } else if (jamSelesai.length() == 7){
                jam2 = jamSelesai.substring(0,1);
                menit2 = jamSelesai.substring(2,4);
                jamInt2 = new Integer(jam2);
                if (jamSelesai.substring(5,7).equals("PM")){
                    jamInt2 = jamInt2 + 12;
                }
                jam2 = jamInt2.toString();
                if (jam2.length() == 1){
                    jam2 = '0'+jam2;
                }
            }else{
                jam2 = jamSelesai.substring(0,2);
                menit2 = jamSelesai.substring(3,5);
                jamInt2 = new Integer(jam2);
                if (jamSelesai.substring(6,8).equals("PM")){
                    jamInt2 = jamInt2 + 12;
                }
                jam2 = jamInt2.toString();
                if (jam2.length() == 1){
                    jam2 = '0'+jam2;
                }
            }

            jamMulai = jam + ':' + menit + ':' + detik;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
            LocalTime localTime = LocalTime.parse(jamMulai, formatter1);

            jamSelesai = jam2 + ':' + menit2 + ':' + detik2;
            DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
            LocalTime localTime2 = LocalTime.parse(jamSelesai, formatter2);


            System.out.println("tanggal : " + localDate);
            System.out.println("jam mulai : " + localTime);
            System.out.println("jam selesai : " + localTime2);

            scheduleEvent.setScheduleDate(localDate);
            scheduleEvent.setScheduleTimeStart(localTime);
            scheduleEvent.setScheduleTimeEnd(localTime2);
            scheduleEvent.setStatusAktif(StatusRecord.AKTIF);
            scheduleEvent.setStatus(StatusRecord.AKTIF);
            scheduleEvent.setUserInsert(user.getUsername());
            scheduleEvent.setDateInsert(LocalDateTime.now());
            scheduleEvent.setMustMatch(StatusRecord.AKTIF);
            scheduleEvent.setUploadPicture(StatusRecord.AKTIF);
            scheduleEvent.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF,"eb5c2e2a-b711-4037-8f60-1e1aa0415958"));
            scheduleEventDao.save(scheduleEvent);

            attribute.addFlashAttribute("berhasil", "Data event schedule berhasil disimapan....");
            return "redirect:../event";

        }else {

            System.out.println("tanggal : " + scheduleEvent.getScheduleDateString());
            System.out.println("jam mulai : " + scheduleEvent.getScheduleTimeStartString());
            System.out.println("jam selesai : " + scheduleEvent.getScheduleTimeEndString());

            Employes employess = employesDao.findByUser(user);
            model.addAttribute("employess", employess);

            model.addAttribute("scheduleEvent", scheduleEvent);
            model.addAttribute("gagal", "Data gagal disimpan, isian belum lengkap...");

            return "setting/schedule/event/form";

        }

    }

}
