package id.ac.tazkia.aplikasihr.controller.setting.payroll;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JenjangPendidikanDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobLevelDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollMasterGajiPokokDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollMasterGajiPokok;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class PayrollMasterGajiPokokController {

    @Autowired
    private JenjangPendidikanDao jenjangPendidikanDao;

    @Autowired
    private JobLevelDao jobLevelDao;

    @Autowired
    private PayrollMasterGajiPokokDao payrollMasterGajiPokokDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/payroll/gajipokok")
    public String listMasterGajiPokok(Model model,
                                      @PageableDefault(size = 10)Pageable page,
                                      @RequestParam(required = false)String search,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPayrollMasterGajiPokok", payrollMasterGajiPokokDao.findByStatusAndDescriptionContainingIgnoreCaseOrStatusAndJenjangPendidikanKodePendidikanContainingIgnoreCaseOrStatusAndJenjangPendidikanNamaPendidikanContainingIgnoreCaseOrStatusAndJobLevelJobLevelContainingIgnoreCaseOrderByDescription(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listPayrollMasterGajiPokok", payrollMasterGajiPokokDao.findByStatusOrderByDescription(StatusRecord.AKTIF, page));
        }


        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/gaji_pokok/list";

    }

    @GetMapping("/setting/payroll/gajipokok/new")
    public String newMasterGajiPokok(Model model,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listJobLevel", jobLevelDao.findByStatusOrderByJobLevel(StatusRecord.AKTIF));
        model.addAttribute("payrollMasterGajiPokok",new PayrollMasterGajiPokok());

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/gaji_pokok/form";

    }

    @GetMapping("/setting/payroll/gajipokok/edit")
    public String editMasterGajiPokok(Model model,
                                      @RequestParam(required = false) PayrollMasterGajiPokok payrollMasterGajiPokok,
                                      RedirectAttributes attribute, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        PayrollMasterGajiPokok payrollMasterGajiPokok1 = payrollMasterGajiPokokDao.findByStatusAndId(StatusRecord.AKTIF, payrollMasterGajiPokok.getId());
        if (payrollMasterGajiPokok1 == null){
            attribute.addFlashAttribute("adeleted", "Save Data Success");
            return "redirect:../gajipokok";
        }else{
            model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
            model.addAttribute("listJobLevel", jobLevelDao.findByStatusOrderByJobLevel(StatusRecord.AKTIF));
            model.addAttribute("payrollMasterGajiPokok", payrollMasterGajiPokok1);

            model.addAttribute("menupayrollconfiguration", "active");
            model.addAttribute("setting", "active");
            return "setting/payroll/gaji_pokok/form";
        }

    }

    @PostMapping("/setting/payroll/gajipokok/save")
    public String savePayrollMasterGajiPokok(@ModelAttribute @Valid PayrollMasterGajiPokok payrollMasterGajiPokok,
                                             Authentication authentication,
                                             RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        payrollMasterGajiPokok.setDateUpdate(LocalDateTime.now());
        payrollMasterGajiPokok.setUserUpdate(user.getUsername());
        payrollMasterGajiPokok.setStatus(StatusRecord.AKTIF);

        payrollMasterGajiPokokDao.save(payrollMasterGajiPokok);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../gajipokok";

    }

    @PostMapping("/setting/payroll/gajipokok/delete")
    public String deletePayrollMasterGajiPokok(@ModelAttribute @Valid PayrollMasterGajiPokok payrollMasterGajiPokok,
                                             Authentication authentication,
                                             RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        payrollMasterGajiPokok.setDateUpdate(LocalDateTime.now());
        payrollMasterGajiPokok.setUserUpdate(user.getUsername());
        payrollMasterGajiPokok.setStatus(StatusRecord.HAPUS);

        payrollMasterGajiPokokDao.save(payrollMasterGajiPokok);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../gajipokok";

    }

}
