package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.EmployeeStatusAktifDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.EmployeeSchedule;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import id.ac.tazkia.aplikasihr.export.ExportEmployesClass;
import id.ac.tazkia.aplikasihr.export.employes.ExportEmployesStatusAktifClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class EmployesStatusController {

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/masterdata/employes/status")
    public String masterDataEmployesStatus(Model model,
                                           @RequestParam(required = false) Companies companies,
                                           @PageableDefault(size = 10)Pageable pageable,
                                           @RequestParam(required = false) String search,
                                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("listStatusEmployes", employeeStatusAktifDao.listEmployeeStatusAktifWithCompanyAndNama(companies.getId(), search, pageable));
                }else {
                    model.addAttribute("listStatusEmployes", employeeStatusAktifDao.listEmployeeStatusAktifWithCompany(companies.getId(), pageable));
                }
                model.addAttribute("companiesSelected", companies);
            }else{
                if (StringUtils.hasText(search)) {

                }else {
                    model.addAttribute("listStatusEmployes", employeeStatusAktifDao.listEmployeeStatusAktif(pageable));
                }
            }
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idCompaniesKaryawan = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (companies != null) {
                if (StringUtils.hasText(search)) {

                }else {
                    model.addAttribute("listStatusEmployes", employeeStatusAktifDao.listEmployeeStatusAktifWithCompany(companies.getId(), pageable));
                }
                model.addAttribute("companiesSelected", companies);
            }else{
                if (StringUtils.hasText(search)) {

                }else {
                    model.addAttribute("listStatusEmployes", employeeStatusAktifDao.listEmployeeStatusAktifWithCompanies(idCompaniesKaryawan, pageable));
                }
            }
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idCompaniesKaryawan));
        }

        return "masterdata/employes/status/list";
    }


    @GetMapping("/employes/export/status")
    public void exportToExcelStatus(@RequestParam(required = false) Companies companies,
                                    @RequestParam(required = false) String search,
                                    HttpServletResponse response,
                                    Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        Companies companies1 = companies;
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Employes_Status_Aktif" + currentDateTime + ".xlsx";
        if (companies1 != null) {
            headerValue = "attachment; filename=Employes_status_aktif" + companies1.getCompanyName() + "_" + currentDateTime + ".xlsx";
        }
        response.setHeader(headerKey, headerValue);


        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    List<EmployeeStatusAktifDto> employeeStatusAktifDtoList = employeeStatusAktifDao.listEmployeeStatusAktifWithCompanyAndNamaList(companies.getId(), search);
                    ExportEmployesStatusAktifClass excelExporter = new ExportEmployesStatusAktifClass(employeeStatusAktifDtoList);
                    excelExporter.export(response);
                }else {
                    List<EmployeeStatusAktifDto> employeeStatusAktifDtoList = employeeStatusAktifDao.listEmployeeStatusAktifWithCompanyList(companies.getId());
                    ExportEmployesStatusAktifClass excelExporter = new ExportEmployesStatusAktifClass(employeeStatusAktifDtoList);
                    excelExporter.export(response);
                }
            }else{
                if (StringUtils.hasText(search)) {

                }else {
                    List<EmployeeStatusAktifDto> employeeStatusAktifDtoList = employeeStatusAktifDao.listEmployeeStatusAktifList();
                    ExportEmployesStatusAktifClass excelExporter = new ExportEmployesStatusAktifClass(employeeStatusAktifDtoList);
                    excelExporter.export(response);
                }
            }
        }else{
            List<String> idCompaniesKaryawan = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (companies != null) {
                if (StringUtils.hasText(search)) {

                }else {
                    List<EmployeeStatusAktifDto> employeeStatusAktifDtoList = employeeStatusAktifDao.listEmployeeStatusAktifWithCompanyList(companies.getId());
                    ExportEmployesStatusAktifClass excelExporter = new ExportEmployesStatusAktifClass(employeeStatusAktifDtoList);
                    excelExporter.export(response);
                }
            }else{
                if (StringUtils.hasText(search)) {

                }else {
                    List<EmployeeStatusAktifDto> employeeStatusAktifDtoList = employeeStatusAktifDao.listEmployeeStatusAktifWithCompaniesList(idCompaniesKaryawan);
                    ExportEmployesStatusAktifClass excelExporter = new ExportEmployesStatusAktifClass(employeeStatusAktifDtoList);
                    excelExporter.export(response);
                }
            }
        }
    }


    @PostMapping("/employes/import/status_aktif")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        DateTimeFormatter dateFormatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

            XSSFRow row = worksheet.getRow(i);
            XSSFRow row1 = worksheet.getRow(0);

            int columnCount = 2;

            if (row != null) {
                for (int a = 2; a < row.getPhysicalNumberOfCells(); a++) {
                    if (row.getCell(a).getStringCellValue() != null) {
//                    String ada = row.getCell(a).getStringCellValue();
                        if(row.getCell(3).getStringCellValue() != null) {
                            if (row.getCell(3).getStringCellValue().equals("")){

                            }else {
                                System.out.println("tanggal cel : " + row.getCell(3).getStringCellValue());
                                LocalDate tanggal = LocalDate.parse(row.getCell(3).getStringCellValue(), dateFormatter2);
                                String fullName = row.getCell(1).getStringCellValue();
                                String status = row.getCell(2).getStringCellValue();
                                String number = null;
                                System.out.println("tanggal simpan : " + tanggal);
                                if (row.getCell(0).getStringCellValue() != null) {
                                    number = row.getCell(0).getStringCellValue();
                                }
                                EmployeeStatusAktif employeeStatusAktif = employeeStatusAktifDao.findByStatusAndStatusAktifAndEmployesFullNameAndTanggal(StatusRecord.AKTIF, "AKTIF", fullName, tanggal);
                                if (employeeStatusAktif == null) {

                                    Employes employes = employesDao.findByStatusAndFullName(StatusRecord.AKTIF, fullName);
                                    System.out.println("Name : " + employes);
                                    EmployeeStatusAktif employeeStatusAktif1 = new EmployeeStatusAktif();
                                    employeeStatusAktif1.setStatus(StatusRecord.AKTIF);
                                    employeeStatusAktif1.setStatusAktif(status);
                                    employeeStatusAktif1.setEmployes(employes);
                                    employeeStatusAktif1.setTanggal(tanggal);
                                    employeeStatusAktif1.setDateUpdate(LocalDateTime.now());
                                    employeeStatusAktif1.setUserUpdate(user.getUsername());
                                    employeeStatusAktif1.setDeskripsi("impor excel");
                                    employeeStatusAktifDao.save(employeeStatusAktif1);
                                    employes.setStatusAktif(status);
                                    employesDao.save(employes);
                                } else {
                                    employeeStatusAktif.setStatus(StatusRecord.AKTIF);
                                    employeeStatusAktif.setStatusAktif(status);
                                    employeeStatusAktif.setTanggal(tanggal);
                                    employeeStatusAktif.setUserUpdate(user.getUsername());
                                    employeeStatusAktif.setDateUpdate(LocalDateTime.now());
                                    employeeStatusAktif.setDeskripsi("impor excel");
                                    employeeStatusAktifDao.save(employeeStatusAktif);
                                    Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, employeeStatusAktif.getEmployes().getId());
                                    employes.setStatusAktif(status);
                                    employesDao.save(employes);
                                }
                            }
                        }else{

                        }
                    }
                }
            }
        }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../../masterdata/employes/status";
    }

}
