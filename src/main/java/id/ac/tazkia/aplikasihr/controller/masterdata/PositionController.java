package id.ac.tazkia.aplikasihr.controller.masterdata;


import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.DepartmentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
public class PositionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobPosition.class);

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    @Value("${upload.jobposition}")
    private String uploadFile;

    @GetMapping("/masterdata/position")
    public String listPosition(Model model,
                               @RequestParam(required = false)String search,
                               @PageableDefault(size = 10)Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
            } else {
                model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF, page));
            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, idDepartements, search, StatusRecord.AKTIF, search, page));
            } else {
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord.AKTIF, idDepartements, page));
            }
        }

        model.addAttribute("menuposition", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/position/list";

    }

    @GetMapping("/masterdata/position/new")
    public String newPosition(Model model,
                              Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listDepartment", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord.AKTIF, idDepartements));
        }

        model.addAttribute("position", new JobPosition());

        model.addAttribute("menuposition", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/position/form";

    }

    @GetMapping("/masterdata/position/edit")
    public String editPosition(Model model,
                               @RequestParam(required = true) JobPosition position,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listDepartment", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listDepartment", departmentDao.findByStatusAndCompaniesIdInOrderByDepartmentCode(StatusRecord.AKTIF, idDepartements));
        }
        model.addAttribute("position", position);
        model.addAttribute("file", position.getFile());

        model.addAttribute("menuposition", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/position/form";

    }

    @PostMapping("/masterdata/position/save")
    public String savePosition(@ModelAttribute @Valid JobPosition position,
                               Authentication authentication, @RequestParam("filePosition")MultipartFile file,
                               RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        position.setStatus(StatusRecord.AKTIF);
        position.setUserUpdate(user.getUsername());
        position.setDateUpdate(LocalDateTime.now());

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long size = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i+1);
        }

        String idFile = UUID.randomUUID().toString();
        position.setFile(idFile + "." + extension);
        new File(uploadFile).mkdirs();
        File tujuan = new File(uploadFile + File.separator + position.getFile());
        file.transferTo(tujuan);

        jobPositionDao.save(position);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../position";

    }

    @GetMapping("/masterdata/position/{position}/file/")
    public ResponseEntity<byte[]> listApplicantFoto(@PathVariable JobPosition position){
        String lokasiFile = uploadFile + File.separator + position.getFile();
        LOGGER.debug("Lokasi foto: {}" + lokasiFile);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (position.getFile().toLowerCase().endsWith("jpeg") || position.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (position.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (position.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }


    @PostMapping("/masterdata/position/delete")
    public String deletePosition(@ModelAttribute @Valid JobPosition position,
                                 Authentication authentication,
                                 RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        position.setStatus(StatusRecord.HAPUS);
        position.setUserUpdate(user.getUsername());
        position.setDateUpdate(LocalDateTime.now());

        jobPositionDao.save(position);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../position";

    }


    @GetMapping("/api/position/aktifkan")
    @ResponseBody
    public String cariJobPosition(@RequestParam(required = true) String id,
                                  @RequestParam(required = true) StatusRecord status){

        System.out.println("id ="+ id);
        System.out.println("value ="+ status);
        if (id != null){
            JobPosition jobPosition = jobPositionDao.findByStatusAndId(StatusRecord.AKTIF, id);
            jobPosition.setOvertimeEligible(status);
            jobPositionDao.save(jobPosition);
        }

        return "sukses";

    }

}
