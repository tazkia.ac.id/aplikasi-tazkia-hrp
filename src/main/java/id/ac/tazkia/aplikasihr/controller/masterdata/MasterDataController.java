package id.ac.tazkia.aplikasihr.controller.masterdata;


import id.ac.tazkia.aplikasihr.dao.config.PermissionDao;
import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.entity.config.Permission;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MasterDataController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/masterdata/home")
    public String masterDatas(Authentication authentication){

        User user = currentUserService.currentUser(authentication);

//        List<UserRole> role = userRoleDao.findByUser(user);
//
//        String ada = "TIDAK";
//        for(Permission permission : role.getRole().getPermissions()){
//            if(permission.getValue().equals("MENU_COMPANIES")){
//                ada = "masterdata/companies";
//            }else if (permission.getValue().equals("MENU_DEPARTMENT")){
//                ada = "masterdata/department";
////                return "redirect:/department";
//            }else if (permission.getValue().equals("MENU_JOB_LEVEL")){
////                return "redirect:/job_level";
//                ada = "masterdata/job_level";
//            }else if (permission.getValue().equals("MENU_POSITION")){
////                return "redirect:/position";
//                ada = "masterdata/position";
//            }else if (permission.getValue().equals("MENU_LECTURER")){
////                return "redirect:/lecturer";
//                ada = "masterdata/lecturer";
//            }else if (permission.getValue().equals("MENU_USER")){
////                return "redirect:/user";
//                ada = "masterdata/user";
//            }else if (permission.getValue().equals("MENU_SETTING_APPROVAL")){
////                return "redirect:/user";
//                ada = "setting/attendance_approval";
//            }else if (permission.getValue().equals("MENU_SETTING_TIME_MANAGEMENT")){
////                return "redirect:/user";
//                ada = "setting/timemanagement/pattern";
//            }else if (permission.getValue().equals("MENU_SETTING_PAYROLL_CONFIGURATION")){
////                return "redirect:/user";
//                ada = "setting/payroll/periode";
//            }else if (permission.getValue().equals("MENU_SETTING_TIME_OFF_CONFIGURATION")){
////                return "redirect:/user";
//                ada = "setting/timeoff/jenis";
//            }else if (permission.getValue().equals("MENU_SETTING_DAY_OFF_CONFIGURATION")){
////                return "redirect:/user";
//                ada = "setting/day/off";
//            }else if (permission.getValue().equals("MENU_EMPLOYES")){
////                return "redirect:/employes";
//                ada = "masterdata/employes";
//            }else{
////                return "redirect:../dashboard";
//            }
//        }
//        if (ada == "TIDAK") {
//            return "redirect:../dashboard";
//        }else{
//            return "redirect:../"+ada;
//        }

        List<UserRole> roles = userRoleDao.findByUser(user);
        Map<String, String> permissionUrlMap = new HashMap<>();
        permissionUrlMap.put("MENU_COMPANIES", "masterdata/companies");
        permissionUrlMap.put("MENU_DEPARTMENT", "masterdata/department");
        permissionUrlMap.put("MENU_JOB_LEVEL", "masterdata/job_level");
        permissionUrlMap.put("MENU_POSITION", "masterdata/position");
        permissionUrlMap.put("MENU_LECTURER", "masterdata/lecturer");
        permissionUrlMap.put("MENU_USER", "masterdata/user");
        permissionUrlMap.put("MENU_SETTING_APPROVAL", "setting/attendance_approval");
        permissionUrlMap.put("MENU_SETTING_TIME_MANAGEMENT", "setting/timemanagement/pattern");
        permissionUrlMap.put("MENU_SETTING_PAYROLL_CONFIGURATION", "setting/payroll/periode");
        permissionUrlMap.put("MENU_SETTING_TIME_OFF_CONFIGURATION", "setting/timeoff/jenis");
        permissionUrlMap.put("MENU_SETTING_DAY_OFF_CONFIGURATION", "setting/day/off");
        permissionUrlMap.put("MENU_EMPLOYES", "masterdata/employes");

        for (UserRole role : roles) {
            for (Permission permission : role.getRole().getPermissions()) {
                String url = permissionUrlMap.get(permission.getValue());
                if (url != null) {
                    return "redirect:../" + url;
                }
            }
        }

        return "redirect:../dashboard";
    }

}
