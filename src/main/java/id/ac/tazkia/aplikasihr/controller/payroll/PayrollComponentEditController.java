package id.ac.tazkia.aplikasihr.controller.payroll;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeePayrollComponentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeePayrollInfoDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.EmployeePayrollComponentHistoryDao;
import id.ac.tazkia.aplikasihr.dao.payroll.EmployeePayrollComponentHistoryDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollComponentDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistory;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistoryDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;

@Controller
public class PayrollComponentEditController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private PayrollComponentDao payrollComponentDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private EmployeePayrollComponentHistoryDao employeePayrollComponentHistoryDao;

    @Autowired
    private EmployeePayrollComponentHistoryDetailDao employeePayrollComponentHistoryDetailDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;
    @Autowired
    private CompaniesDao companiesDao;

    @GetMapping("/payroll/edit")
    public String updatePayrollComponent(Model model,
                                         Authentication authentication,
                                         @PageableDefault(size = 10) Pageable page){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);


        model.addAttribute("payrollMenu", "active");
        return "payroll/edit/list";
    }

    @GetMapping("/payroll/edit/new")
    public String newUpdatePayrollComponent(Model model,
                                            @RequestParam(required = false) PayrollComponent payrollComponent,
                                            Authentication authentication,
                                            RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        model.addAttribute("employess", employes);
        model.addAttribute("payrollMenu", "active");
        model.addAttribute("listPayrollComponent", payrollComponentDao.findByStatusNotOrderByJenisComponent(StatusRecord.HAPUS));

        if(payrollComponent != null){
            List<String> idEmployess = employesDao.karyawanBelumTunjangan(payrollComponent.getId());
            model.addAttribute("payrollComponentSelected", payrollComponent);
            if(role != null) {
                if(idEmployess != null) {
                    model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktifAndIdInOrderByNumber(StatusRecord.AKTIF, "AKTIF", idEmployess));
                }else{
                    model.addAttribute("listEmployee", employesDao.findByStatusOrderByNumber(StatusRecord.APPROVED));
                }
                model.addAttribute("listPayrollComponentEdit", employeePayrollComponentDao.listEmployeePayrollComponentEndDate(payrollComponent.getId()));
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                if (idEmployess != null) {
                    model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktifAndCompaniesMainCompanyAndIdInOrderByNumber(StatusRecord.AKTIF, "AKTIF", employes.getCompanies(), idEmployess));
                }else{
                    model.addAttribute("listEmployee", employesDao.findByStatusAndCompaniesMainCompanyIdInOrderByNumber(StatusRecord.APPROVED, idDepartements));
                }
                model.addAttribute("listPayrollComponentEdit", employeePayrollComponentDao.listEmployeePayrollComponentEndDateWithCompaniesMainCompanies(payrollComponent.getId(), idDepartements));
            }

        }

        model.addAttribute("datefailed", "ada");
        return "payroll/edit/list";

    }

    @GetMapping("/payroll/edit/form")
    public String editFormPayrolComponent(Model model,
                                          @RequestParam(required = false) PayrollComponent idPayrollComponent,
                                          @RequestParam(required = false) Employes employee,
                                          Authentication authentication,
                                          RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        StatusRecord statusRecord = idPayrollComponent.getStatus();

        EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusAndEmployesAndPayrollComponent(statusRecord, employee, idPayrollComponent);
        model.addAttribute("employeePayrollComponent", employeePayrollComponent);

        return "payroll/edit/form";
    }

    @PostMapping("/payroll/edit/save")
    public String savePayrollEdit(@RequestParam(required = true) String idPayrollComponent,
                                  @RequestParam(required = true) String employee,
                                  @RequestParam(required = true) String amount,
                                  @RequestParam(required = true) String tanggalMulaiString,
                                  @RequestParam(required = true) String tanggalSelesaiString,
                                  RedirectAttributes attributes,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String jumlah = "";

        for (int i = 0; i < amount.length(); i++) {
            if(amount.charAt(i) != '.') {
                if (amount.charAt(i) == 'H' || amount.charAt(i) == 'i' || amount.charAt(i) == 'b' || amount.charAt(i) == 'e' || amount.charAt(i) == 'r' || amount.charAt(i) == 'n' || amount.charAt(i) == 'a' || amount.charAt(i) == 't'){
                    jumlah = jumlah;
                }else{
                    if(amount.charAt(i) != ',') {
                        jumlah = jumlah + amount.charAt(i);
                    }else{
                        jumlah = jumlah + '.';
                    }
                }
            }
        }

        System.out.print("nominal : " + jumlah);

        BigDecimal nominalBaru = new BigDecimal(jumlah);

        System.out.print("nominalBaru : " + nominalBaru);

        String date = tanggalMulaiString;
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String date2 = tanggalSelesaiString;
        String tahun2 = date2.substring(6,10);
        String bulan2 = date2.substring(0,2);
        String tanggal2 = date2.substring(3,5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);

        Employes employes1 = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);
        List<StatusRecord> statusRecords = new ArrayList<>();
        statusRecords.add(StatusRecord.AKTIF);
        statusRecords.add(StatusRecord.BASIC);
        PayrollComponent payrollComponent = payrollComponentDao.findByStatusInAndId(statusRecords, idPayrollComponent);
        System.out.print("component :" + payrollComponent);

        //Cari employee payroll component terlebih dahulu
        EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponent(StatusRecord.HAPUS, employes1, payrollComponent);
        if (employeePayrollComponent != null){

                EmployeePayrollComponentHistory employeePayrollComponentHistory = employeePayrollComponentHistoryDao.findFirstByStatusAndEffectiveDateAndEndDateOrderByDateUpdateDesc(StatusRecord.AKTIF, localDate, localDate2);

                if (employeePayrollComponentHistory != null){
                    EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = employeePayrollComponentHistoryDetailDao.findByStatusAndEmployeePayrollComponentHistoryAndEmployeePayrollComponent(StatusRecord.AKTIF, employeePayrollComponentHistory, employeePayrollComponent);
                    if (employeePayrollComponentHistoryDetail != null){
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent);
                        employeePayrollComponentHistoryDetail.setCurrentAmount(employeePayrollComponent.getNominal());
                        employeePayrollComponentHistoryDetail.setNewAmount(nominalBaru);
                        employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                        employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                        employeePayrollComponentHistoryDetail.setUserUpdate(user.getUsername());
                        employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);
                    }else {
                        EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail1 = new EmployeePayrollComponentHistoryDetail();
                        employeePayrollComponentHistoryDetail1.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                        employeePayrollComponentHistoryDetail1.setEmployeePayrollComponent(employeePayrollComponent);
                        employeePayrollComponentHistoryDetail1.setCurrentAmount(employeePayrollComponent.getNominal());
                        employeePayrollComponentHistoryDetail1.setNewAmount(nominalBaru);
                        employeePayrollComponentHistoryDetail1.setStatus(StatusRecord.AKTIF);
                        employeePayrollComponentHistoryDetail1.setDateUpdate(LocalDateTime.now());
                        employeePayrollComponentHistoryDetail1.setUserUpdate(user.getUsername());
                        employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail1);
                    }

                }else {

//                    Employes employes2 = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);
                    System.out.print("Proses Simpan");
                    //Save History nya
                    EmployeePayrollComponentHistory employeePayrollComponentHistory1 = new EmployeePayrollComponentHistory();
                    employeePayrollComponentHistory1.setPayrollComponent(payrollComponent);
                    employeePayrollComponentHistory1.setEffectiveDate(localDate);
                    employeePayrollComponentHistory1.setEndDate(localDate2);
                    employeePayrollComponentHistory1.setTipe("ADJUSMENT");
                    employeePayrollComponentHistory1.setStatus(StatusRecord.AKTIF);
                    employeePayrollComponentHistory1.setTransactionId(currentDateTime);
                    employeePayrollComponentHistory1.setUserUpdate(user.getUsername());
                    employeePayrollComponentHistory1.setDateUpdate(LocalDateTime.now());
                    employeePayrollComponentHistory1.setCompanies(employes1.getCompanies());
                    employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory1);

                    //Save Detail History nya
                    EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
                    employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory1);
                    employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent);
                    employeePayrollComponentHistoryDetail.setCurrentAmount(employeePayrollComponent.getNominal());
                    employeePayrollComponentHistoryDetail.setNewAmount(nominalBaru);
                    employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                    employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                    employeePayrollComponentHistoryDetail.setUserUpdate(user.getUsername());
//                    employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory1(employeePayrollComponentHistoryDao.findByStatusAndId(StatusRecord.AKTIF, idKwitansi));
                    employeePayrollComponentHistory1.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                    employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);

                }

            employeePayrollComponent.setNominal(nominalBaru);
            employeePayrollComponent.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, employee));
            employeePayrollComponent.setDateUpdate(LocalDateTime.now());
            employeePayrollComponent.setUserUpdate(user.getUsername());
            employeePayrollComponent.setStatus(payrollComponent.getStatus());
            employeePayrollComponentDao.save(employeePayrollComponent);

        }

        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../edit/new?payrollComponent="+ idPayrollComponent;
    }


    @PostMapping("/payroll/edit/savenew")
    public String savePayrollSaveNew(@RequestParam(required = true) String idPayrollComponent,
                                  @RequestParam(required = true) String employeeNew,
                                  @RequestParam(required = true) String amountNew,
                                  @RequestParam(required = true) String tanggalMulaiStringNew,
                                  @RequestParam(required = true) String tanggalSelesaiStringNew,
                                  RedirectAttributes attributes,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String jumlah = "";

        for (int i = 0; i < amountNew.length(); i++) {
            if(amountNew.charAt(i) != '.') {
                if (amountNew.charAt(i) == 'H' || amountNew.charAt(i) == 'i' || amountNew.charAt(i) == 'b' || amountNew.charAt(i) == 'e' || amountNew.charAt(i) == 'r' || amountNew.charAt(i) == 'n' || amountNew.charAt(i) == 'a' || amountNew.charAt(i) == 't'){
                    jumlah = jumlah;
                }else{
                    if(amountNew.charAt(i) != ',') {
                        jumlah = jumlah + amountNew.charAt(i);
                    }else{
                        jumlah = jumlah + '.';
                    }
                }
            }
        }

        System.out.print("nominal : " + jumlah);

        BigDecimal nominalBaru = new BigDecimal(jumlah);

        System.out.print("nominalBaru : " + nominalBaru);

        String date = tanggalMulaiStringNew;
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String date2 = tanggalSelesaiStringNew;
        String tahun2 = date2.substring(6,10);
        String bulan2 = date2.substring(0,2);
        String tanggal2 = date2.substring(3,5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);

        Employes employes1 = employesDao.findByStatusAndId(StatusRecord.AKTIF, employeeNew);

        List<StatusRecord> statusRecords = new ArrayList<>();
        statusRecords.add(StatusRecord.AKTIF);
        statusRecords.add(StatusRecord.BASIC);

        PayrollComponent payrollComponent = payrollComponentDao.findByStatusInAndId(statusRecords, idPayrollComponent);
        System.out.print("component :" + payrollComponent);

        //Cari employee payroll component terlebih dahulu
        EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponent(StatusRecord.HAPUS, employes1, payrollComponent);
        if (employeePayrollComponent != null){

            System.out.print("Proses Simpan");
            //Save History nya
            EmployeePayrollComponentHistory employeePayrollComponentHistory = new EmployeePayrollComponentHistory();
            employeePayrollComponentHistory.setPayrollComponent(payrollComponent);
            employeePayrollComponentHistory.setEffectiveDate(localDate);
            employeePayrollComponentHistory.setEndDate(localDate2);
            employeePayrollComponentHistory.setTipe("ADJUSMENT");
            employeePayrollComponentHistory.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistory.setTransactionId(currentDateTime);
            employeePayrollComponentHistory.setUserUpdate(user.getUsername());
            employeePayrollComponentHistory.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistory.setCompanies(employes1.getCompanies());
            employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory);

            //Save Detail History nya
            EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
            employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
            employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent);
            employeePayrollComponentHistoryDetail.setCurrentAmount(employeePayrollComponent.getNominal());
            employeePayrollComponentHistoryDetail.setNewAmount(nominalBaru);
            employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
            employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);

            employeePayrollComponent.setNominal(nominalBaru);
            employeePayrollComponent.setEmployes(employes);
            employeePayrollComponent.setDateUpdate(LocalDateTime.now());
            employeePayrollComponent.setUserUpdate(user.getUsername());
            employeePayrollComponent.setStatus(payrollComponent.getStatus());
            employeePayrollComponentDao.save(employeePayrollComponent);

        }else{
            System.out.print("Proses Simpan");

            EmployeePayrollComponent employeePayrollComponentBaru = new EmployeePayrollComponent();
            employeePayrollComponentBaru.setNominal(nominalBaru);
            employeePayrollComponentBaru.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, employeeNew));
            employeePayrollComponentBaru.setPayrollComponent(payrollComponent);
            employeePayrollComponentBaru.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentBaru.setUserUpdate(user.getUsername());
            employeePayrollComponentBaru.setJenisComponent("CUSTOM");
            employeePayrollComponentBaru.setJenisNominal("NOMINAL");
            employeePayrollComponentBaru.setStatus(payrollComponent.getStatus());
            employeePayrollComponentDao.save(employeePayrollComponentBaru);

            //Save History nya
//            EmployeePayrollComponentHistory employeePayrollComponentHistory = new EmployeePayrollComponentHistory();
//            employeePayrollComponentHistory.setPayrollComponent(payrollComponent);
//            employeePayrollComponentHistory.setEffectiveDate(localDate);
//            employeePayrollComponentHistory.setEndDate(localDate2);
//            employeePayrollComponentHistory.setTipe("ADJUSMENT");
//            employeePayrollComponentHistory.setStatus(StatusRecord.AKTIF);
//            employeePayrollComponentHistory.setTransactionId(currentDateTime);
//            employeePayrollComponentHistory.setUserUpdate(user.getUsername());
//            employeePayrollComponentHistory.setDateUpdate(LocalDateTime.now());
//            employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory);

            EmployeePayrollComponentHistory employeePayrollComponentHistory1 = new EmployeePayrollComponentHistory();
            employeePayrollComponentHistory1.setPayrollComponent(payrollComponent);
            employeePayrollComponentHistory1.setEffectiveDate(localDate);
            employeePayrollComponentHistory1.setEndDate(localDate2);
            employeePayrollComponentHistory1.setTipe("ADJUSMENT");
            employeePayrollComponentHistory1.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistory1.setTransactionId(currentDateTime);
            employeePayrollComponentHistory1.setUserUpdate(user.getUsername());
            employeePayrollComponentHistory1.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistory1.setCompanies(employes1.getCompanies());
            employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory1);

            //Save Detail History nya
//            EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
//            employeePayrollComponentHistoryDetail.setCurrentAmount(BigDecimal.ZERO);
//            employeePayrollComponentHistoryDetail.setNewAmount(nominalBaru);
//            employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
//            employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
//            employeePayrollComponentHistory1.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
//            employeePayrollComponentBaru.getEmployeePayrollComponentHistoryDetailss().add(employeePayrollComponentHistoryDetail);
//            employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);


            EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
            employeePayrollComponentHistoryDetail.setCurrentAmount(BigDecimal.ZERO);
            employeePayrollComponentHistoryDetail.setNewAmount(nominalBaru);
            employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistoryDetail.setUserUpdate(user.getUsername());
            employeePayrollComponentHistory1.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
            employeePayrollComponentBaru.getEmployeePayrollComponentHistoryDetailss().add(employeePayrollComponentHistoryDetail);
            employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);


        }

        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../edit/new?payrollComponent="+payrollComponent.getId();
    }

}
