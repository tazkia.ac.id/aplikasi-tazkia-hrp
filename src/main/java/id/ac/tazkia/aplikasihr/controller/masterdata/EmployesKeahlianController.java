package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeKeahlianDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKeahlian;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeRiwayatPendidikan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class EmployesKeahlianController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);


    @Autowired
    private EmployeeKeahlianDao employeeKeahlianDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.sertifikatemployee}")
    private String uploadFolder;

    @GetMapping("/masterdata/employes/keahlian")
    public String listEmployesKeahliahn(Model model,
                                        @RequestParam(required = true)Employes employes,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listEmployeeKeahlian", employeeKeahlianDao.findByStatusAndEmployes(StatusRecord.AKTIF,employes));

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeSkils", "active");
        return "masterdata/employes/keahlian/list";

    }

    @GetMapping("/masterdata/employes/keahlian/new")
    public String newEmployesKeahlian(Model model,
                                      @RequestParam(required = true) Employes employes,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("employeeKeahlian", new EmployeeKeahlian());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeSkils", "active");
        return "masterdata/employes/keahlian/form";

    }

    @GetMapping("/masterdata/employes/keahlian/edit")
    public String editEmployesKeahlian(Model model,
                                       @RequestParam(required = true) EmployeeKeahlian employeeKeahlian,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employeeKeahlian.getEmployes());
        model.addAttribute("employeeKeahlian", employeeKeahlian);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeSkils", "active");
        return "masterdata/employes/keahlian/form";

    }

    @PostMapping("/masterdata/employes/keahlian/save")
    public String saveEmployesKeahlian(@ModelAttribute @Valid EmployeeKeahlian employeeKeahlian,
                                       @RequestParam(required = true) Employes employes,
                                       @RequestParam("sertifikat") MultipartFile fileSertifikat,
                                       Authentication authentication,
                                       RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);


        String namaFile =  fileSertifikat.getName();
        String jenisFile = fileSertifikat.getContentType();
        String namaAsli = fileSertifikat.getOriginalFilename();
        Long ukuran = fileSertifikat.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employes.getFileFoto() == null){
                employes.setFileFoto("default.jpg");
            }
        }else{
            employeeKeahlian.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            fileSertifikat.transferTo(tujuan);
        }

        employeeKeahlian.setDateUpdate(LocalDateTime.now());
        employeeKeahlian.setUserUpdate(user.getUsername());
        employeeKeahlian.setStatus(StatusRecord.AKTIF);

        employeeKeahlianDao.save(employeeKeahlian);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keahlian?employes="+ employes.getId();

    }

    @PostMapping("/masterdata/employes/keahlian/delete")
    public String deleteEmployesKeahlian(@ModelAttribute @Valid EmployeeKeahlian employeeKeahlian,
                                       @RequestParam(required = true) Employes employes,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeKeahlian.setDateUpdate(LocalDateTime.now());
        employeeKeahlian.setUserUpdate(user.getUsername());
        employeeKeahlian.setStatus(StatusRecord.HAPUS);

        employeeKeahlianDao.save(employeeKeahlian);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keahlian?employes="+ employes.getId();

    }


    @GetMapping("/masterdata/employes/keahlian/{employeeKeahlian}/sertifikat/")
    public ResponseEntity<byte[]> tanpilaknSertifikat(@PathVariable EmployeeKeahlian employeeKeahlian) throws Exception {
        String lokasiFile = uploadFolder + File.separator + employeeKeahlian.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeKeahlian.getFile().toLowerCase().endsWith("jpeg") || employeeKeahlian.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeKeahlian.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeKeahlian.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
