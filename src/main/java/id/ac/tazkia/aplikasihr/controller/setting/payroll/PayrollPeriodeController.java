package id.ac.tazkia.aplikasihr.controller.setting.payroll;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class PayrollPeriodeController {

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/payroll/periode")
    public String payrollPeriode(Model model,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        if (payrollPeriode == null){
            model.addAttribute("payrollPeriode", new PayrollPeriode());
        }else{
            model.addAttribute("payrollPeriode", payrollPeriode);
        }

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/payroll_periode";

    }

    @PostMapping("/setting/payroll/periode/save")
    public String savePayrollPeriode(@ModelAttribute @Valid PayrollPeriode payrollPeriode,
                                     Authentication authentication,
                                     RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        payrollPeriode.setUserUpdate(user.getUsername());
        payrollPeriode.setDateUpdate(LocalDateTime.now());
        payrollPeriode.setStatus(StatusRecord.AKTIF);

        payrollPeriodeDao.save(payrollPeriode);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../periode";

    }

}
