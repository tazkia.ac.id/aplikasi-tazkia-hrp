package id.ac.tazkia.aplikasihr.controller.api;

import id.ac.tazkia.aplikasihr.dao.masterdata.PositionKpiDao;
import id.ac.tazkia.aplikasihr.dto.api.BaseResponseApiDto;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@Slf4j
@RequestMapping("/api/v1")
public class ApiKpiController {
    @Autowired
    private PositionKpiDao positionKpiDao;

    @ResponseBody
    @GetMapping("/kpiposition")
    public ResponseEntity<BaseResponseApiDto> getKpiPosition(String position){
        Optional<PositionKpi> positionKpiOptional = positionKpiDao.findById(position);
        if (!positionKpiOptional.isPresent()){
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .responseCode("404")
                            .responseMessage("Data Tidak Ditemukan").build());
        }else {
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("Data Ditemukan")
                            .data(positionKpiOptional).build()
            );
        }
    }
}
