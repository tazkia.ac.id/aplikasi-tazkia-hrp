package id.ac.tazkia.aplikasihr.controller.reports;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
public class ReportKpiDosenController {

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;


    @GetMapping("/reports/kpi_dosen")
    public String reportKpiStaff(Model model,
                                 @RequestParam(required = false)String bulan,
                                 @RequestParam(required = false)String tahun) {

        if(bulan == null){
            bulan = String.format("%02d", LocalDate.now().getMonthValue());
        }
        if(tahun == null){
            tahun = String.valueOf(LocalDate.now().getYear());
        }
        model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF,bulan) );
        model.addAttribute("tahunSelected",tahunDao.findByStatusAndTahun(StatusRecord.AKTIF,tahun));
        model.addAttribute("listBulan", bulanDao.findByStatusOrderById(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        model.addAttribute("menureportkpidosen", "active");
        model.addAttribute("menureports", "active");
        return "reports/kpi_dosen/list";
    }

}
