package id.ac.tazkia.aplikasihr.controller.my;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hibernate.sql.ast.SqlTreeCreationLogger.LOGGER;

@Controller
public class MyPaySlipController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private PayrollRunDetailDao payrollRunDetailDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    @Value("${icon.file}")
    private String iconFile;

    @GetMapping("/my/payslip")
    public String myPaySlip(Model model,
                           @RequestParam(required = false) String bulan,
                           @RequestParam(required = false) String tahun,
                           RedirectAttributes attribute,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        //Konversi bulan ke integer
        Integer bulanAngka = new Integer(bulan);
        //Mencari bulan sebelum nya
        Integer bulanAngka2 = bulanAngka - 1;
        //Mengubah bulan sebelum nya ke string
        String bulan2 = bulanAngka2.toString();

        //Konversi tahun ke string
        Integer tahunAngka = new Integer(tahun);
        //mencari tahun sebelum nya
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        //Mencari tanggal payroll periode aktif
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        //deklarasi tanggal mulai periode
        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }

        //deklarasi tanggal selesai periode
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        //tanggal mulai periode
        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        //tanggal selesai periode
        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        String tanggalGajian = tahun + '-' + bulan + '-' + tanggalMulaiPeriode;
        LocalDate localDate3 = LocalDate.parse(tanggalGajian, formatter1);

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        PayrollRun payrollRun = payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employess.getId());

        if (payrollRun != null){
            if(payrollRun.getPaySlip() == StatusRecord.AKTIF){
                model.addAttribute("paySlip", payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employess.getId()));
                model.addAttribute("employes", employess);
                model.addAttribute("companies", employess.getCompanies());
                model.addAttribute("date", LocalDate.now());
                model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
                model.addAttribute("bulan", bulan);
                model.addAttribute("tahun", tahun);
                model.addAttribute("iconfile", iconFile);
                model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employess.getId(), localDate, localDate2));
                model.addAttribute("attendance", attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employess.getId()));
                model.addAttribute("allowance", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employess, bulan, tahun, BigDecimal.ZERO, "ALLOWANCE"));
                model.addAttribute("deduction", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employess, bulan, tahun, BigDecimal.ZERO, "DEDUCTION"));
                model.addAttribute("benefit", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employess, bulan, tahun, BigDecimal.ZERO, "BENEFIT"));
                model.addAttribute("home","active");
                model.addAttribute("mypayslip","active");
                return "my/payroll/payslip";
            }else{
                model.addAttribute("tahun", tahun);
                model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
                model.addAttribute("nonaktif","nonaktif");
                model.addAttribute("home","active");
                model.addAttribute("mypayslip","active");
                return "my/payroll/paynot";
            }
        }else{
            model.addAttribute("tahun", tahun);
            model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
            model.addAttribute("tidakada","tidakada");
            model.addAttribute("home","active");
            model.addAttribute("mypayslip","active");
            return "my/payroll/paynot";
        }
    }

    @PostMapping("/my/payslip/print")
    public String payrollMyReportsPrint(Model model,
                                      @RequestParam(required = true) String bulan,
                                      @RequestParam(required = true) String tahun,
                                      @RequestParam(required = true) Employes employes,
                                      Authentication authentication){

        //Konversi bulan ke integer
        Integer bulanAngka = new Integer(bulan);
        //Mencari bulan sebelum nya
        Integer bulanAngka2 = bulanAngka - 1;
        //Mengubah bulan sebelum nya ke string
        String bulan2 = bulanAngka2.toString();

        //Konversi tahun ke string
        Integer tahunAngka = new Integer(tahun);
        //mencari tahun sebelum nya
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        //Mencari tanggal payroll periode aktif
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        //deklarasi tanggal mulai periode
        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }

        //deklarasi tanggal selesai periode
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        //tanggal mulai periode
        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        //tanggal selesai periode
        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        String tanggalGajian = tahun + '-' + bulan + '-' + tanggalMulaiPeriode;
        LocalDate localDate3 = LocalDate.parse(tanggalGajian, formatter1);

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("paySlip", payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employes);
        model.addAttribute("companies", employes.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employes.getId(), localDate , localDate2));
        model.addAttribute("attendance", attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
        model.addAttribute("iconfile", iconFile);
        model.addAttribute("allowance", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "ALLOWANCE"));

        model.addAttribute("deduction", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "DEDUCTION"));

        model.addAttribute("benefit", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "BENEFIT"));

        return "payroll/report/print";
    }



    @GetMapping("/icon/logo")
    public ResponseEntity<byte[]> tampilkanBuktiAttendance() throws Exception {
        String lokasiFile = iconFile;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
