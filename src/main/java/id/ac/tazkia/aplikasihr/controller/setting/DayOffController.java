package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.DayOffDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
public class DayOffController {

    @Autowired
    private DayOffDao dayOffDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/day/off")
    public String dayOffCalendar(Model model,
                                 @RequestParam(required = false) String search,
                                 @PageableDefault(size = 10)Pageable page,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("listDayOff", dayOffDao.findByStatusAndKeteranganContainingIgnoreCaseOrderByDateDayOffDesc(StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listDayOff", dayOffDao.findByStatusOrderByDateDayOffDesc(StatusRecord.AKTIF, page));
        }

        model.addAttribute("dayOff", new DayOff());

        model.addAttribute("menudayoffconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/day_off/list_table";

    }

    @GetMapping("/setting/day/off/edit")
    public String editDayOff(Model model,
                             @RequestParam(required = false) DayOff dayOff,
                             @RequestParam(required = false) String search,
                             @PageableDefault(size = 10)Pageable page,
                             Authentication authentication){


        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("listDayOff", dayOffDao.findByStatusAndKeteranganContainingIgnoreCaseOrderByDateDayOffDesc(StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listDayOff", dayOffDao.findByStatusOrderByDateDayOffDesc(StatusRecord.AKTIF, page));
        }

        model.addAttribute("dayOff", dayOff);

        model.addAttribute("menudayoffconfiguration", "active");
        model.addAttribute("setting", "active");

        return "setting/day_off/list_table";
    }

    @PostMapping("/setting/day/off/save")
    public String saveDayOff(@ModelAttribute @Valid DayOff dayOff,
                             Authentication authentication,
                             RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = dayOff.getDateDayOffString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        dayOff.setDateDayOff(localDate);

        dayOff.setDateUpdate(LocalDateTime.now());
        dayOff.setUserUpdate(user.getUsername());
        dayOff.setStatus(StatusRecord.AKTIF);

        dayOffDao.save(dayOff);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../off";

    }


    @PostMapping("/setting/day/off/delete")
    public String deleteDayOff(@ModelAttribute @Valid DayOff dayOff,
                             Authentication authentication,
                             RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        dayOff.setDateUpdate(LocalDateTime.now());
        dayOff.setUserUpdate(user.getUsername());
        dayOff.setStatus(StatusRecord.HAPUS);

        dayOffDao.save(dayOff);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../off";

    }

}
