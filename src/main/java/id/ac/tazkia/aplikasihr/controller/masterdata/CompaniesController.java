package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class CompaniesController {

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;


    @GetMapping("/masterdata/companies")
    public String listCompanies(Model model,
                                @RequestParam(required = false) String search,
                                @PageableDefault(size = 10)Pageable page,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndCompanyNameContainingIgnoreCaseOrStatusNotAndCompanyTypeContainingIgnoreCaseOrStatusNotAndRegencyContainingIgnoreCaseOrderByCompanyName(StatusRecord.HAPUS, search, StatusRecord.HAPUS, search, StatusRecord.HAPUS, search, page));
        }else {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS, page));
        }

        model.addAttribute("menucompanies", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("settings", "active");
        return "masterdata/companies/list";

    }

    @GetMapping("/masterdata/companies/new")
    public String newCompanies(Model model,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("companies", new Companies());
        model.addAttribute("listCompanies", companiesDao.findByStatusAndCompanyTypeOrderByCompanyName(StatusRecord.AKTIF, "HQ"));
        model.addAttribute("menucompanies", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("settings", "active");
        return "masterdata/companies/form";

    }

    @GetMapping("/masterdata/companies/edit")
    public String editCompanies(Model model,
                                @RequestParam(required = false) Companies companies,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("companies", companies);
        model.addAttribute("listCompanies", companiesDao.findByStatusAndCompanyTypeOrderByCompanyName(StatusRecord.AKTIF, "HQ"));
        model.addAttribute("menucompanies", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("settings", "active");
        return "masterdata/companies/form";

    }

    @PostMapping("/masterdata/companies/save")
    public String saveCompanies(@ModelAttribute @Valid Companies companies,
                                Authentication authentication,
                                RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
//        Employes employes = employesDao.findByUser(user);

        String idBaru = UUID.randomUUID().toString();
        companies.setUserUpdate(user.getUsername());
        companies.setDateUpdate(LocalDateTime.now());
        companies.setId(idBaru);
        if (companies.getStatus() == null){
            companies.setStatus(StatusRecord.NONAKTIF);
        }
        if(companies.getCompanyType().equals("HQ")){
            companies.setMainCompany(companies);
        }

        companiesDao.save(companies);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../companies";

    }

    @PostMapping("/masterdata/companies/delete")
    public String deleteCompanies(@RequestParam(required = false) Companies companies,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
//        Employes employes = employesDao.findByUser(user);

        companies.setUserUpdate(user.getUsername());
        companies.setDateUpdate(LocalDateTime.now());
        companies.setStatus(StatusRecord.HAPUS);

        companiesDao.save(companies);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../companies";

    }

}
