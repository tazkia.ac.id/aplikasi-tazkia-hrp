package id.ac.tazkia.aplikasihr.controller.finance;


import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.request.LoanRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.LoanRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.LoanApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequest;
import id.ac.tazkia.aplikasihr.entity.request.LoanRequestApproval;
import id.ac.tazkia.aplikasihr.entity.setting.LoanApprovalSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

@Controller
public class ListLoanRequestController {

    @Autowired
    private LoanRequestDao loanRequestDao;

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployesLuarDao employesLuarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private LoanApprovalSettingDao loanApprovalSettingDao;

    @Autowired
    private LoanRequestApprovalDao loanRequestApprovalDao;


    @GetMapping("/finance/request/loan")
    public String listLoanRequest(Model model,
                                  @RequestParam(required = false) String search,
                                  @PageableDefault(size = 10) Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

//        if (StringUtils.hasText(search)) {
//            model.addAttribute("search", search);
//            model.addAttribute("listRequestLoan", loanRequestDao.findByStatusAndEmployesFullNameContainingIgnoreCaseOrderByRequestDate(StatusRecord.AKTIF,search, page));
//        }else {
//            model.addAttribute("listRequestLoan", loanRequestDao.findByStatusOrderByRequestDate(StatusRecord.AKTIF, page));
//        }

        model.addAttribute("listRequestLoan", loanRequestDao.listLoanRequest(employes.getId()));
        model.addAttribute("approvalLoanSetting", loanApprovalSettingDao.findByStatusNotAndEmployes(StatusRecord.HAPUS, employes));

        model.addAttribute("finance", "active");
        model.addAttribute("requestLoan", "active");
        return "finance/request_loan/list";

    }


    @PostMapping("/finance/request/loan/approve")
    public String approveLoanRequest(@RequestParam(required = true)LoanRequest loanRequest,
                                     @RequestParam(required = true) Integer ada,
                                     @RequestParam(required = false) String descriptions,
                                     RedirectAttributes redirectAttributes,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        LoanApprovalSetting loanApprovalSetting = loanApprovalSettingDao.findByStatusAndEmployesAndSequence(StatusRecord.AKTIF, employes, ada);
        if(loanApprovalSetting != null){
            LoanRequestApproval loanRequestApproval = new LoanRequestApproval();

            loanRequestApproval.setLoanRequest(loanRequest);
            loanRequestApproval.setLoanApprovalSetting(loanApprovalSetting);
            loanRequestApproval.setStatusApprove(StatusRecord.APPROVED);
            loanRequestApproval.setDateApprove(LocalDateTime.now());
            loanRequestApproval.setDeskripsi("Approved by "+ employes.getFullName());
            loanRequestApproval.setStatus(StatusRecord.AKTIF);
            loanRequestApproval.setEmployes(employes);
            loanRequestApproval.setDeskripsi(descriptions);

            loanRequestApprovalDao.save(loanRequestApproval);
        }

        Integer jumlahApproval = loanRequestApprovalDao.countByStatusAndStatusApproveAndLoanRequest(StatusRecord.AKTIF, StatusRecord.APPROVED, loanRequest);
        Integer approvalSeharusnya = loanApprovalSettingDao.countByStatus(StatusRecord.AKTIF);

        if(jumlahApproval >= loanRequest.getTotalSequence()){
            loanRequest.setStatusApprove(StatusRecord.APPROVED);
            loanRequestDao.save(loanRequest);
        }else{
            loanRequest.setSequence(loanRequest.getSequence()+1);
            loanRequestDao.save(loanRequest);
        }

        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";
    }


    @PostMapping("/finance/request/loan/reject")
    public String rejectLoanRequest(@RequestParam(required = true)LoanRequest loanRequest,
                                     @RequestParam(required = true) Integer ada,
                                     @RequestParam(required = false) String descriptions,
                                     RedirectAttributes redirectAttributes,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        LoanApprovalSetting loanApprovalSetting = loanApprovalSettingDao.findByStatusAndEmployesAndSequence(StatusRecord.AKTIF, employes, ada);
        if(loanApprovalSetting != null){
            LoanRequestApproval loanRequestApproval = new LoanRequestApproval();

            loanRequestApproval.setLoanRequest(loanRequest);
            loanRequestApproval.setLoanApprovalSetting(loanApprovalSetting);
            loanRequestApproval.setStatusApprove(StatusRecord.REJECTED);
            loanRequestApproval.setDateApprove(LocalDateTime.now());
            loanRequestApproval.setDeskripsi("Approved by "+ employes.getFullName());
            loanRequestApproval.setStatus(StatusRecord.AKTIF);
            loanRequestApproval.setEmployes(employes);
            loanRequestApproval.setDeskripsi(descriptions);

            loanRequestApprovalDao.save(loanRequestApproval);
        }

//        Integer jumlahApproval = loanRequestApprovalDao.countByStatusAndStatusApproveAndLoanRequest(StatusRecord.AKTIF, StatusRecord.APPROVED, loanRequest);
//        Integer approvalSeharusnya = loanApprovalSettingDao.countByStatus(StatusRecord.AKTIF);

        loanRequest.setStatusApprove(StatusRecord.REJECTED);
        loanRequestDao.save(loanRequest);

        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";
    }


}
