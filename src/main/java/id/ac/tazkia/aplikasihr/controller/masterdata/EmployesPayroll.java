package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollComponentDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollInfo;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeStatusAktif;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class EmployesPayroll {

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private PtkpStatusDao ptkpStatusDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private TaxStatusDao taxStatusDao;

    @Autowired
    private PayrollComponentDao payrollComponentDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private BankDao bankDao;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/masterdata/employes/payroll")
    public String formEmployesPayroll(Model model,
                                      @RequestParam(required = true) Employes employes,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("ptkpStatus", ptkpStatusDao.findByStatusOrderByPtkp(StatusRecord.AKTIF));
        model.addAttribute("taxStatus", taxStatusDao.findByStatusOrderById(StatusRecord.AKTIF));
        model.addAttribute("bank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
//        model.addAttribute("listEmployeePayrollComponent", employeePayrollComponentDao.findByStatusAndEmployesOrderByJenisComponent(StatusRecord.AKTIF, employes));
//        model.addAttribute("employeePayrollComponent", new EmployeePayrollComponent());
//        model.addAttribute("listPayrollComponent", payrollComponentDao.findByStatusOrderByJenisComponent(StatusRecord.AKTIF));

        EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);

        if (employeePayrollInfo == null){
            model.addAttribute("employeePayrollInfo", new EmployeePayrollInfo());
        }else{
            model.addAttribute("employeePayrollInfo", employeePayrollInfo);
        }

        return "masterdata/employes/payroll/form";

    }

    @PostMapping("/masterdata/employes/payroll/save")
    public String saveEmployeePayroll(@ModelAttribute @Valid EmployeePayrollInfo employeePayrollInfo,
                                      @RequestParam(required = true) Employes employes,
                                      Authentication authentication,
                                      RedirectAttributes attribute){



        User user = currentUserService.currentUser(authentication);

        employeePayrollInfo.setEmployes(employes);
        employeePayrollInfo.setDateInsert (LocalDateTime.now());
        employeePayrollInfo.setUserInsert(user.getUsername());
        employeePayrollInfo.setStatus(StatusRecord.AKTIF);

        if( employeePayrollInfo.getBasicSalary() != null ) {
            if (employeePayrollInfo.getBasicSalary().doubleValue() > 0) {
                EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusAndEmployesAndPayrollComponentStatus(StatusRecord.BASIC, employes, StatusRecord.BASIC);
                if (employeePayrollComponent == null) {
                    EmployeePayrollComponent employeePayrollComponent1 = new EmployeePayrollComponent();
                    employeePayrollComponent1.setPayrollComponent(payrollComponentDao.findByStatus(StatusRecord.BASIC));
                    employeePayrollComponent1.setJenisNominal("CUSTOM");
                    employeePayrollComponent1.setJenisComponent("CUSTOM");
                    employeePayrollComponent1.setNominal(employeePayrollInfo.getBasicSalary());
                    employeePayrollComponent1.setEmployes(employes);
                    employeePayrollComponent1.setStatus(StatusRecord.BASIC);
                    employeePayrollComponent1.setUserInsert(user.getUsername());
                    employeePayrollComponent1.setDateInsert(LocalDateTime.now());
                    employeePayrollComponentDao.save(employeePayrollComponent1);
                } else {
                    EmployeePayrollComponent employeePayrollComponent1 = employeePayrollComponent;
                    employeePayrollComponent1.setNominal(employeePayrollInfo.getBasicSalary());
                    employeePayrollComponent1.setJenisNominal("CUSTOM");
                    employeePayrollComponent1.setJenisComponent("CUSTOM");
                    employeePayrollComponent1.setUserUpdate(user.getUsername());
                    employeePayrollComponent1.setDateUpdate(LocalDateTime.now());
                    employeePayrollComponentDao.save(employeePayrollComponent1);
                }
            } else {

                EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusAndEmployesAndPayrollComponentStatus(StatusRecord.BASIC, employes, StatusRecord.BASIC);
                if (employeePayrollComponent != null) {
                    employeePayrollComponent.setStatus(StatusRecord.HAPUS);
                    employeePayrollComponent.setUserDelete(user.getUsername());
                    employeePayrollComponent.setDateDelete(LocalDateTime.now());
                    employeePayrollComponentDao.save(employeePayrollComponent);
                }

            }

        }else{
            EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusAndEmployesAndPayrollComponentStatus(StatusRecord.BASIC, employes, StatusRecord.BASIC);
            if(employeePayrollComponent != null){
                employeePayrollComponent.setStatus(StatusRecord.HAPUS);
                employeePayrollComponent.setUserDelete(user.getUsername());
                employeePayrollComponent.setDateDelete(LocalDateTime.now());
                employeePayrollComponentDao.save(employeePayrollComponent);
            }

        }

        employeePayrollInfoDao.save(employeePayrollInfo);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../payroll?employes="+ employes.getId();

    }

    @PostMapping("/masterdata/employes/payroll/component/save")
    public String saveEmployeePayrollComponent(@ModelAttribute @Valid EmployeePayrollComponent employeePayrollComponent,
                                               @RequestParam(required = true) Employes employes,
                                                Authentication authentication,
                                                RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeePayrollComponent.setEmployes(employes);
        employeePayrollComponent.setDateUpdate (LocalDateTime.now());
        employeePayrollComponent.setUserUpdate(user.getUsername());
        employeePayrollComponent.setStatus(StatusRecord.AKTIF);

        employeePayrollComponentDao.save(employeePayrollComponent);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../../payroll?employes="+ employes.getId();

    }


    @GetMapping("masterdata/employes/payroll/component/delete")
    public String deleteEmployeePayrollComponent(@RequestParam (required = true) EmployeePayrollComponent employeePayrollComponent,
                                                 Authentication authentication,
                                                 RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeePayrollComponent.setStatus(StatusRecord.HAPUS);
        employeePayrollComponent.setDateDelete(LocalDateTime.now());
        employeePayrollComponent.setUserDelete(user.getUsername());

        employeePayrollComponentDao.save(employeePayrollComponent);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../../payroll?employes="+ employeePayrollComponent.getEmployes().getId();

    }


}
