package id.ac.tazkia.aplikasihr.controller.request;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.request.AttendanceRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.AttendanceRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeStatusKontrakDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequestApproval;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;

import id.ac.tazkia.aplikasihr.services.GmailApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class AttendanceRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private AttendanceRequestDao attendanceRequestDao;

    @Autowired
    private AttendanceRequestApprovalDao attendanceRequestApprovalDao;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired private GmailApiService gmailApiService;

    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    @Value("${upload.attendancerequest}")
    private String uploadFolder;

    @Autowired
    @Value("${approve.email}")
    private String approveEmaiil;

    @GetMapping("/request")
    public String request(Model model){

        model.addAttribute("request", "active");

        return "request/home";
    }



    @GetMapping("/request/attendance")
    public String listAttendanceRequest(Model model,
                                        @PageableDefault(size = 10) Pageable page,
                                        @RequestParam(required = false)String search,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
//        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());

//       List<ApprovalAttendanceRequestListDto> approvalAttendanceRequestListDtos = attendanceApprovalSettingDao.listAttendanceRequestApproval(employes.getId());

        model.addAttribute("listAttendanceRequest", attendanceRequestDao.findByStatusAndEmployesOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, employes, page));

 //       if (approvalAttendanceRequestListDtos != null){
            model.addAttribute("listApproval", attendanceApprovalSettingDao.listAttendanceRequestApproval(employes.getId()));
 //       }

        model.addAttribute("request", "active");
        model.addAttribute("menurequestattendance", "active");
        return "request/attendance/list";

    }

    @GetMapping("/request/attendance/new")
    public String newAttendanceRequest(Model model,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        EmployeeStatusKontrakDto employeeStatusKontrakDto = employeeStatusAktifDao.cariStatusKontrak(employes.getId());

        if(employeeStatusKontrakDto.getStatus().equals("NO")){
            model.addAttribute("kontrak", employeeStatusKontrakDto);
            model.addAttribute("jenis", " Attendance Request");
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");;
            return "request/form_eror";
        }

//        model.addAttribute("listJobPosition", employeeJobPositionDao.findByStatusAndStatusAktifAndEmployes(StatusRecord.AKTIF, StatusRecord.AKTIF, employes));

        model.addAttribute("employes", employes);
        model.addAttribute("attendanceRequest", new AttendanceRequest());

        model.addAttribute("request", "active");
        model.addAttribute("menurequestattendance", "active");
        return "request/attendance/form";

    }

    @GetMapping("/request/attendance/edit")
    public String editAttendanceRequest(Model model,
                                        @RequestParam(required = true) AttendanceRequest attendanceRequest,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employes", employes);
        model.addAttribute("attendanceRequest", attendanceRequest);

        model.addAttribute("request", "active");
        model.addAttribute("menurequestattendance", "active");
        return "request/attendance/formedit";
        
    }


    @PostMapping("/request/attendance/save")
    @Transactional
    public String saveAttendanceRequest(@ModelAttribute @Valid AttendanceRequest attendanceRequest,
                                        Model model,
                                        @RequestParam("fileUpload") MultipartFile file,
                                        Authentication authentication,
                                        BindingResult errors,
                                        RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        if(errors.hasErrors()){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceRequest", attendanceRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");
            return "request/attendance/form";
        }

//        String date = attendanceRequest.getTanggalString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        String tanggalan2 = tahun + '-' + bulan + '-' + tanggal + " 00:00:00";
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        LocalDate localDate1 = LocalDate.parse(attendanceRequest.getTanggalString(),formatter);
//        LocalDateTime localDateTime = LocalDateTime.parse(tanggalan2, formatter2);



        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());

        if(idEmployes.isEmpty()){
            if (file == null){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                model.addAttribute("attendanceRequest", attendanceRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request Attendance tidak dapat dilakukan, Hal ini dikarenakan jabatan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/attendance/form";
            }
        }
//        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove1 = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);
        Integer jumlahApprove = 0;
        if (jumlahApprove1 != null){
            jumlahApprove = jumlahApprove1;
        }
        Integer nomorAwal = 1;

        if(nomorAwal > jumlahApprove){
            if (file == null){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                model.addAttribute("attendanceRequest", attendanceRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request Attendance tidak dapat dilakukan, Hal ini dikarenakan kemungkinan jabatan atasan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/attendance/form";
            }
        }

        System.out.println("position :"+ idEmployes);
        System.out.println("nomorawal :"+ nomorAwal);
        System.out.println("jumlah :"+ jumlahApprove);

        if (file == null){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceRequest", attendanceRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/attendance/form";
        }

        if (file.isEmpty()) {
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceRequest", attendanceRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/attendance/form";
        }


        if(localDate1.now().minusDays(1).getDayOfYear() > localDate1.getDayOfYear()){

            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceRequest", new AttendanceRequest());
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");
            model.addAttribute("gagalfile","Pengajuan Attendance gagal, request attendance maksimal 1 hari setelah tanggal masuk kerja");
            return "request/attendance/form";

        }


        String jamMulai = attendanceRequest.getJamMasukString();
        String jam = "00";
        String menit = "00";
        String detik = "00";
        Integer jamInt = 00;
        String jam2 = "00";
        String menit2 = "00";
        String detik2 = "00";
        Integer jamInt2 = 00;

        if(jamMulai.length() == 4){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
        } else if (jamMulai.length() == 5){
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
        } else if (jamMulai.length() == 7){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
            jamInt = new Integer(jam);
            if (jamMulai.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
                if (jamInt == 24){
                    jamInt = 12;
                }
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
            jamInt = new Integer(jam);
            if (jamMulai.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
                if (jamInt == 24){
                    jamInt = 12;
                }
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        String jamSelesai = attendanceRequest.getJamKeluarString();

        if(jamSelesai.length() == 4){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
        } else if (jamSelesai.length() == 5){
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
        } else if (jamSelesai.length() == 7){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(5,7).equals("PM")){
                jamInt2 = jamInt2 + 12;
                if (jamInt2 == 24){
                    jamInt2 = 12;
                }
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }else{
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(6,8).equals("PM")){
                jamInt2 = jamInt2 + 12;
                if (jamInt2 == 24){
                    jamInt2 = 12;
                }
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }

        jamMulai = jam + ':' + menit + ':' + detik;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime = LocalTime.parse(jamMulai, formatter1);

        jamSelesai = jam2 + ':' + menit2 + ':' + detik2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime2 = LocalTime.parse(jamSelesai, formatter2);

        System.out.println(attendanceRequest.getTanggal());
        attendanceRequest.setTanggal(localDate1);
        attendanceRequest.setDateUpdate(LocalDateTime.now());
        attendanceRequest.setUserUpdate(user.getUsername());
        attendanceRequest.setNomor(nomorAwal);
        attendanceRequest.setTotalNomor(jumlahApprove);
        attendanceRequest.setJamMasuk(localTime);
        attendanceRequest.setJamKeluar(localTime2);
        if (nomorAwal > jumlahApprove){
            attendanceRequest.setStatusApprove(StatusRecord.REJECTED);
        }else {
            attendanceRequest.setStatusApprove(StatusRecord.WAITING);
        }


        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(attendanceRequest.getFile() == null){
                attendanceRequest.setFile("default.jpg");
            }
        }else{
            attendanceRequest.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        attendanceRequestDao.save(attendanceRequest);

        AttendanceRequest attendanceRequest1 = attendanceRequestDao.findByStatusAndEmployesAndTanggalAndKeteranganAndStatusApproveAndJamMasukAndJamKeluarAndNomor(StatusRecord.AKTIF, attendanceRequest.getEmployes(), attendanceRequest.getTanggal(), attendanceRequest.getKeterangan(), StatusRecord.WAITING, attendanceRequest.getJamMasuk(), attendanceRequest.getJamKeluar(), attendanceRequest.getNomor());

        if (attendanceRequest1 != null){
            List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(idEmployes, 1);
            for(ApprovalListDetailDto aparl : approvalListDetailDtos){

                Mustache templateEmail = mustacheFactory.compile("templates/email/approval/attendance_request.html");
                Map<String, String> data = new HashMap<>();
                data.put("nama", aparl.getEmployeeApprove());
                data.put("permohonan", "Berikut Attendance Request yang menunggu persetujuan anda");
                data.put("jabatan", aparl.getPositionApprove());
                data.put("tanggal", attendanceRequest.getTanggal().toString());
                data.put("dari", attendanceRequest.getEmployes().getFullName());
                data.put("departemendari", "-");
                data.put("date", attendanceRequest.getTanggal().toString());
                data.put("time", attendanceRequest.getJamMasuk()+ " - " +attendanceRequest.getJamKeluar());
                data.put("description", attendanceRequest.getKeterangan());
                data.put("idApproval", aparl.getId());
                data.put("id", attendanceRequest1.getId());
                data.put("attendance", attendanceRequest1.getId());
                data.put("approve", aparl.getId());
                data.put("employee", aparl.getIdEmployee());
                data.put("alamat",approveEmaiil+"/api/attendance/approve");
                data.put("alamatr",approveEmaiil+"/api/attendance/reject");

                StringWriter output = new StringWriter();
                templateEmail.execute(output, data);

                gmailApiService.kirimEmail(
                        "Notifikasi Approval Attendance Request",
                        aparl.getEmail(),
                        "Approval Attendance Request",
                        output.toString());

            }
        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../attendance";

    }


    @PostMapping("/request/attendance/update")
    @Transactional
    public String updateAttendanceRequest(@ModelAttribute @Valid AttendanceRequest attendanceRequest,
                                        @RequestParam("fileUpload") MultipartFile file,
                                        Authentication authentication,
                                        RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        //        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove1 = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);
        Integer jumlahApprove = 0;
        if (jumlahApprove1 != null){
            jumlahApprove = jumlahApprove1;
        }
        Integer nomorAwal = 1;


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate1 = LocalDate.parse(attendanceRequest.getTanggalString(),formatter);

        String jamMulai = attendanceRequest.getJamMasukString();
        String jam = "00";
        String menit = "00";
        String detik = "00";
        Integer jamInt = 00;
        String jam2 = "00";
        String menit2 = "00";
        String detik2 = "00";
        Integer jamInt2 = 00;

        if(jamMulai.length() == 4){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
        } else if (jamMulai.length() == 5){
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
        } else if (jamMulai.length() == 7){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
            jamInt = new Integer(jam);
            if (jamMulai.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
            jamInt = new Integer(jam);
            if (jamMulai.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        String jamSelesai = attendanceRequest.getJamKeluarString();

        if(jamSelesai.length() == 4){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
        } else if (jamSelesai.length() == 5){
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
        } else if (jamSelesai.length() == 7){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(5,7).equals("PM")){
                jamInt2 = jamInt2 + 12;
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }else{
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(6,8).equals("PM")){
                jamInt2 = jamInt2 + 12;
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }

        jamMulai = jam + ':' + menit + ':' + detik;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime = LocalTime.parse(jamMulai, formatter1);

        jamSelesai = jam2 + ':' + menit2 + ':' + detik2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime2 = LocalTime.parse(jamSelesai, formatter2);

        attendanceRequest.setTanggal(localDate1);
        attendanceRequest.setDateUpdate(LocalDateTime.now());
        attendanceRequest.setUserUpdate(user.getUsername());
        attendanceRequest.setNomor(nomorAwal);
        attendanceRequest.setTotalNomor(jumlahApprove);
        attendanceRequest.setJamMasuk(localTime);
        attendanceRequest.setJamKeluar(localTime2);
        if (nomorAwal > jumlahApprove){
            attendanceRequest.setStatusApprove(StatusRecord.APPROVED);
        }else {
            attendanceRequest.setStatusApprove(StatusRecord.WAITING);
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(attendanceRequest.getFile() == null){
                attendanceRequest.setFile("default.jpg");
            }
        }else{
            attendanceRequest.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        List<AttendanceRequestApproval> attendanceRequestApprovals = attendanceRequestApprovalDao.findByStatusAndAttendanceRequest(StatusRecord.AKTIF, attendanceRequest);
        for (AttendanceRequestApproval s : attendanceRequestApprovals){

            s.setStatusApprove(StatusRecord.HAPUS);
            s.setStatus(StatusRecord.HAPUS);
            attendanceRequestApprovalDao.save(s);
            
        }

        attendanceRequestDao.save(attendanceRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../attendance";

    }


    @PostMapping("/request/attendance/delete")
    @Transactional
    public String deleteAttendanceRequest(@RequestParam (required = true) AttendanceRequest attendanceRequest,
                                          Authentication authentication,
                                          RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);

        attendanceRequest.setStatus(StatusRecord.HAPUS);
        attendanceRequest.setStatusApprove(StatusRecord.CANCELED);
        attendanceRequest.setDateUpdate(LocalDateTime.now());
        attendanceRequest.setUserUpdate(user.getUsername());

        List<AttendanceRequestApproval> attendanceRequestApprovals = attendanceRequestApprovalDao.findByStatusAndAttendanceRequest(StatusRecord.AKTIF, attendanceRequest);
        for (AttendanceRequestApproval s : attendanceRequestApprovals){

            s.setStatusApprove(StatusRecord.HAPUS);
            s.setStatus(StatusRecord.HAPUS);

            attendanceRequestApprovalDao.save(s);

        }

        attendanceRequestDao.save(attendanceRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../attendance";

    }


    @GetMapping("/attendance/{attendanceRequest}/request/")
    public ResponseEntity<byte[]> tampilkanBuktiAttendance(@PathVariable AttendanceRequest attendanceRequest) throws Exception {
        String lokasiFile = uploadFolder + File.separator + attendanceRequest.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (attendanceRequest.getFile().toLowerCase().endsWith("jpeg") || attendanceRequest.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (attendanceRequest.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (attendanceRequest.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
