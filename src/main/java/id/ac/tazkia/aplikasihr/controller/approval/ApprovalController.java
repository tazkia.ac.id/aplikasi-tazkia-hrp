package id.ac.tazkia.aplikasihr.controller.approval;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.request.*;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.timeoff.TimeOffJenisDao;
import id.ac.tazkia.aplikasihr.dto.StatusEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.approval.ApprovalListDto;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.request.*;
import id.ac.tazkia.aplikasihr.entity.setting.AttendanceApprovalSetting;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.GmailApiService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ApprovalController {

    @Autowired
    private AttendanceRequestDao attendanceRequestDao;

    @Autowired
    private OverTimeRequestDao overTimeRequestDao;

    @Autowired
    private TimeOffRequestDao timeOffRequestDao;

    @Autowired
    private AttendanceRequestApprovalDao attendanceRequestApprovalDao;

    @Autowired
    private OverTImeRequestApprovalDao overTImeRequestApprovalDao;

    @Autowired
    private TimeOffRequestApprovalDao timeOffRequestApprovalDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired private GmailApiService gmailApiService;
    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private TimeOffJenisDao timeOffJenisDao;

    @Autowired
    @Value("${approve.email}")
    private String approveEmaiil;

    @GetMapping("/approval")
    public String approval(Model model,
                           @PageableDefault(size = 10) Pageable page,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("listRequest", attendanceRequestDao.listApprovalProcess(employes.getId(),page));

        model.addAttribute("approvalMenu", "active");
        return "approval/list";

    }

    @PostMapping("/approval/change")
    public String approvalChangeSave(@ModelAttribute TimeOffRequest timeOffRequest,
                                     Authentication authentication,
                                     RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        TimeOffRequest timeOffRequest1 = timeOffRequestDao.findById(timeOffRequest.getId()).get();
        String jenisSebelum = timeOffRequest1.getTimeOffJenis().getTimeOffName();
        timeOffRequest1.setTimeOffJenis(timeOffRequest.getTimeOffJenis());
        timeOffRequestDao.save(timeOffRequest1);

        List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(timeOffRequest1.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor() );
        List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest1.getNomor());

        Integer sisaApproval = 0;

        for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
            TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
            timeOffRequestApproval.setEmployes(employes);
            timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
            timeOffRequestApproval.setTimeOffRequest(timeOffRequest1);
            timeOffRequestApproval.setStatusApprove(StatusRecord.APPROVED);
            timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
            timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
            timeOffRequestApproval.setKeterangan("Approve dengan pergantian jenis time off dari " + jenisSebelum + " menjadi "+ timeOffRequest.getTimeOffJenis().getTimeOffName() + " karena bukti tidak memenuhi syarat.");
            timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
            timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
            timeOffRequestApproval.setUserUpdate(user.getUsername());

            timeOffRequestApprovalDao.save(timeOffRequestApproval);
        }

        Integer sisaApprovalTimeOff = attendanceApprovalSettingDao.sisaApprovalTimeOff(employeeJobPositionList2, timeOffRequest1.getId(), timeOffRequest1.getNomor());

        if (sisaApprovalTimeOff == 0) {
            timeOffRequest1.setNomor(timeOffRequest1.getNomor() + 1);
            timeOffRequestDao.save(timeOffRequest1);
        }

        Integer sisaApprovalTimeOffTotals = attendanceApprovalSettingDao.sisaApprovalTimeOffTotal(employeeJobPositionList2, timeOffRequest1.getId());

        if (sisaApprovalTimeOffTotals == 0){
            timeOffRequest1.setStatusApprove(StatusRecord.APPROVED);
            timeOffRequestDao.save(timeOffRequest1);
        }

        return "redirect:../approval";
    }

    @GetMapping("/approval/change")
    public String approvalChange(Model model,
                           @RequestParam(required = true)String id,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        TimeOffRequest timeOffRequest = timeOffRequestDao.findByStatusAndId(StatusRecord.AKTIF,id);

        StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(timeOffRequest.getEmployes().getId());

        model.addAttribute("statusTimeOff", statusEmployeeDto);
        if(LocalDate.now().getMonth().getValue() <= 6){
            if(statusEmployeeDto.getStatusdua().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(timeOffRequest.getEmployes().getId()));
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(timeOffRequest.getEmployes().getId()));
                }else{
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTImeOffKaryawanKontrak(timeOffRequest.getEmployes().getId()));

                }
            }
        }else{
            if(statusEmployeeDto.getStatus().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(timeOffRequest.getEmployes().getId()));
            }else{
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTImeOffKaryawanKontrak(timeOffRequest.getEmployes().getId()));
            }
        }

        model.addAttribute("timeOffRequest", timeOffRequest);
        model.addAttribute("employesss", timeOffRequest.getEmployes());

        model.addAttribute("approvalMenu", "active");
        return "approval/form";

    }

    @GetMapping("/approval/konfirmasi")
    public String konfirmasi(Model model,
                             @RequestParam(required = true) String request,
                             @RequestParam(required = true) String option,
                             Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);


        if(option.equals("overtime")){
            model.addAttribute("detailRequest", overTimeRequestDao.cariEmployee(request));
        }else if(option.equals("time_off")){
            model.addAttribute("detailRequest", timeOffRequestDao.cariEmployee(request));
        }else{
            model.addAttribute("detailRequest", attendanceRequestDao.cariEmployee(request));
        }
        model.addAttribute("employess", employes);
        model.addAttribute("request", request);
        model.addAttribute("option", option);
        model.addAttribute("approvalMenu", "active");

        return "approval/reject";
    }

    @GetMapping("/approval/approve")
    public String approve(@RequestParam(required = true) String request,
                          @RequestParam(required = true) String option,
                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        if (option.equals("attendance")){

            AttendanceRequest attendanceRequest = attendanceRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                AttendanceRequestApproval attendanceRequestApproval = new AttendanceRequestApproval();
                attendanceRequestApproval.setEmployes(employes);
                attendanceRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                attendanceRequestApproval.setAttendanceRequest(attendanceRequest);
                attendanceRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                attendanceRequestApproval.setStatus(StatusRecord.AKTIF);
                attendanceRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                attendanceRequestApproval.setKeterangan("Approve");
                attendanceRequestApproval.setTanggalApprove(LocalDateTime.now());
                attendanceRequestApproval.setDateUpdate(LocalDateTime.now());
                attendanceRequestApproval.setUserUpdate(user.getUsername());

                attendanceRequestApprovalDao.save(attendanceRequestApproval);

            }

            Integer sisaApprovalAttendances = attendanceApprovalSettingDao.sisaApprovalAttendance(employeeJobPositionList2, attendanceRequest.getId(), attendanceRequest.getNomor());

            if (sisaApprovalAttendances == 0){
                attendanceRequest.setNomor(attendanceRequest.getNomor()+1);
                attendanceRequestDao.save(attendanceRequest);

                List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(employeeJobPositionList2, attendanceRequest.getNomor()+1);
                for(ApprovalListDetailDto aparl : approvalListDetailDtos){

                    Mustache templateEmail = mustacheFactory.compile("templates/email/approval/attendance_request.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", aparl.getEmployeeApprove());
                    data.put("permohonan", "Berikut Attendance Request yang menunggu persetujuan anda");
                    data.put("jabatan", aparl.getPositionApprove());
                    data.put("tanggal", attendanceRequest.getTanggal().toString());
                    data.put("dari", attendanceRequest.getEmployes().getFullName());
                    data.put("departemendari", "-");
                    data.put("date", attendanceRequest.getTanggal().toString());
                    data.put("time", attendanceRequest.getJamMasuk()+ " - " +attendanceRequest.getJamKeluar());
                    data.put("description", attendanceRequest.getKeterangan());
                    data.put("idApproval", aparl.getId());
                    data.put("id", attendanceRequest.getId());
                    data.put("attendance", attendanceRequest.getId());
                    data.put("approve", aparl.getId());
                    data.put("employee", aparl.getIdEmployee());
                    data.put("alamat",approveEmaiil+"/api/attendance/approve");
                    data.put("alamatr",approveEmaiil+"/api/attendance/reject");
                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Approval Attendance Request",
                            aparl.getEmail(),
                            "Approval Attendance Request",
                            output.toString());

                }
            }

            Integer sisaApprovalAttencanceTotals = attendanceApprovalSettingDao.sisaApprovalAttendanceTotal(employeeJobPositionList2, attendanceRequest.getId());

            if (sisaApprovalAttencanceTotals == 0){
                attendanceRequest.setStatusApprove(StatusRecord.APPROVED);
                attendanceRequestDao.save(attendanceRequest);
            }

        }

        if (option.equals("overtime")){

            OvertimeRequest overtimeRequest = overTimeRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(overtimeRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor() );

            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor());

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){

                OvertimeRequestApproval overtimeRequestApproval = new OvertimeRequestApproval();
                overtimeRequestApproval.setEmployes(employes);
                overtimeRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                overtimeRequestApproval.setOvertimeRequest(overtimeRequest);
                overtimeRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                overtimeRequestApproval.setStatus(StatusRecord.AKTIF);
                overtimeRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                overtimeRequestApproval.setKeterangan("Approve");
                overtimeRequestApproval.setTanggalApprove(LocalDateTime.now());
                overtimeRequestApproval.setDateUpdate(LocalDateTime.now());
                overtimeRequestApproval.setUserUpdate(user.getUsername());

                overTImeRequestApprovalDao.save(overtimeRequestApproval);

            }

            Integer sisaApprovalOvertime = attendanceApprovalSettingDao.sisaApprovalOvertime(employeeJobPositionList2, overtimeRequest.getId(), overtimeRequest.getNomor());

            if (sisaApprovalOvertime == 0) {
                overtimeRequest.setNomor(overtimeRequest.getNomor() + 1);
                overTimeRequestDao.save(overtimeRequest);
            }

            Integer sisaApprovalOvertimeTotals = attendanceApprovalSettingDao.sisaApprovalOvertimeTotal(employeeJobPositionList2, overtimeRequest.getId());

            if (sisaApprovalOvertimeTotals == 0){
                overtimeRequest.setStatusApprove(StatusRecord.APPROVED);
                overTimeRequestDao.save(overtimeRequest);
            }

        }

        if (option.equals("time_off")){

            TimeOffRequest timeOffRequest = timeOffRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(timeOffRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor() );
            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor());

            Integer sisaApproval = 0;

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
                timeOffRequestApproval.setEmployes(employes);
                timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                timeOffRequestApproval.setTimeOffRequest(timeOffRequest);
                timeOffRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
                timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                timeOffRequestApproval.setKeterangan("Approve");
                timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
                timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
                timeOffRequestApproval.setUserUpdate(user.getUsername());

                timeOffRequestApprovalDao.save(timeOffRequestApproval);
            }

            Integer sisaApprovalTimeOff = attendanceApprovalSettingDao.sisaApprovalTimeOff(employeeJobPositionList2, timeOffRequest.getId(), timeOffRequest.getNomor());

            if (sisaApprovalTimeOff == 0) {
                timeOffRequest.setNomor(timeOffRequest.getNomor() + 1);
                timeOffRequestDao.save(timeOffRequest);
            }

            Integer sisaApprovalTimeOffTotals = attendanceApprovalSettingDao.sisaApprovalTimeOffTotal(employeeJobPositionList2, timeOffRequest.getId());

            if (sisaApprovalTimeOffTotals == 0){
                timeOffRequest.setStatusApprove(StatusRecord.APPROVED);
                timeOffRequestDao.save(timeOffRequest);
            }

        }

        
        return "redirect:../approval";
    }

    @PostMapping("/approval/reject")
    public String reject(@RequestParam(required = true) String request,
                          @RequestParam(required = true) String option,
                          @RequestParam(required = false) String reason,
                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        if (option.equals("attendance")){
            AttendanceRequest attendanceRequest = attendanceRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                AttendanceRequestApproval attendanceRequestApproval = new AttendanceRequestApproval();
                attendanceRequestApproval.setEmployes(employes);
                attendanceRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                attendanceRequestApproval.setAttendanceRequest(attendanceRequest);
                attendanceRequestApproval.setStatusApprove(StatusRecord.REJECTED);
                attendanceRequestApproval.setStatus(StatusRecord.AKTIF);
                attendanceRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                attendanceRequestApproval.setKeterangan(reason);
                attendanceRequestApproval.setTanggalApprove(LocalDateTime.now());
                attendanceRequestApproval.setDateUpdate(LocalDateTime.now());
                attendanceRequestApproval.setUserUpdate(user.getUsername());

                attendanceRequestApprovalDao.save(attendanceRequestApproval);
            }


            attendanceRequest.setStatusApprove(StatusRecord.REJECTED);
            attendanceRequestDao.save(attendanceRequest);

        }

        if (option.equals("overtime")){

            OvertimeRequest overtimeRequest = overTimeRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(overtimeRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor() );

            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor());

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                OvertimeRequestApproval overtimeRequestApproval = new OvertimeRequestApproval();
                overtimeRequestApproval.setEmployes(employes);
                overtimeRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                overtimeRequestApproval.setOvertimeRequest(overtimeRequest);
                overtimeRequestApproval.setStatusApprove(StatusRecord.REJECTED);
                overtimeRequestApproval.setStatus(StatusRecord.AKTIF);
                overtimeRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                overtimeRequestApproval.setKeterangan(reason);
                overtimeRequestApproval.setTanggalApprove(LocalDateTime.now());
                overtimeRequestApproval.setDateUpdate(LocalDateTime.now());
                overtimeRequestApproval.setUserUpdate(user.getUsername());

                overTImeRequestApprovalDao.save(overtimeRequestApproval);
            }



            overtimeRequest.setStatusApprove(StatusRecord.REJECTED);
            overTimeRequestDao.save(overtimeRequest);

        }

        if (option.equals("time_off")){

            TimeOffRequest timeOffRequest = timeOffRequestDao.findById(request).get();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(timeOffRequest.getEmployes().getId());

//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor() );

            List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor());

            for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
                timeOffRequestApproval.setEmployes(employes);
                timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                timeOffRequestApproval.setTimeOffRequest(timeOffRequest);
                timeOffRequestApproval.setStatusApprove(StatusRecord.REJECTED);
                timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
                timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                timeOffRequestApproval.setKeterangan(reason);
                timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
                timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
                timeOffRequestApproval.setUserUpdate(user.getUsername());

                timeOffRequestApprovalDao.save(timeOffRequestApproval);
            }

            timeOffRequest.setStatusApprove(StatusRecord.REJECTED);
            timeOffRequestDao.save(timeOffRequest);

        }


        return "redirect:../approval";

    }

    @PostMapping("/approval/approve_checked")
    public String approve(@RequestParam(required = true) List<String> request,
                          Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        for (String req : request){

            AttendanceRequest attendanceRequest = attendanceRequestDao.findByStatusAndId(StatusRecord.AKTIF, req);
            if (attendanceRequest != null){

//                AttendanceRequest attendanceRequest = a;
                List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

                List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

                for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                    AttendanceRequestApproval attendanceRequestApproval = new AttendanceRequestApproval();
                    attendanceRequestApproval.setEmployes(employes);
                    attendanceRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                    attendanceRequestApproval.setAttendanceRequest(attendanceRequest);
                    attendanceRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                    attendanceRequestApproval.setStatus(StatusRecord.AKTIF);
                    attendanceRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                    attendanceRequestApproval.setKeterangan("Approve");
                    attendanceRequestApproval.setTanggalApprove(LocalDateTime.now());
                    attendanceRequestApproval.setDateUpdate(LocalDateTime.now());
                    attendanceRequestApproval.setUserUpdate(user.getUsername());

                    attendanceRequestApprovalDao.save(attendanceRequestApproval);

                }

                Integer sisaApprovalAttendances = attendanceApprovalSettingDao.sisaApprovalAttendance(employeeJobPositionList2, attendanceRequest.getId(), attendanceRequest.getNomor());

                if (sisaApprovalAttendances == 0){
                    attendanceRequest.setNomor(attendanceRequest.getNomor()+1);
                    attendanceRequestDao.save(attendanceRequest);

                    List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(employeeJobPositionList2, attendanceRequest.getNomor()+1);
                    for(ApprovalListDetailDto aparl : approvalListDetailDtos){

                        Mustache templateEmail = mustacheFactory.compile("templates/email/approval/attendance_request.html");
                        Map<String, String> data = new HashMap<>();
                        data.put("nama", aparl.getEmployeeApprove());
                        data.put("permohonan", "Berikut Attendance Request yang menunggu persetujuan anda");
                        data.put("jabatan", aparl.getPositionApprove());
                        data.put("tanggal", attendanceRequest.getTanggal().toString());
                        data.put("dari", attendanceRequest.getEmployes().getFullName());
                        data.put("departemendari", "-");
                        data.put("date", attendanceRequest.getTanggal().toString());
                        data.put("time", attendanceRequest.getJamMasuk()+ " - " +attendanceRequest.getJamKeluar());
                        data.put("description", attendanceRequest.getKeterangan());
                        data.put("idApproval", aparl.getId());
                        data.put("id", attendanceRequest.getId());
                        data.put("attendance", attendanceRequest.getId());
                        data.put("approve", aparl.getId());
                        data.put("employee", aparl.getIdEmployee());
                        data.put("alamat",approveEmaiil+"/api/attendance/approve");
                        data.put("alamatr",approveEmaiil+"/api/attendance/reject");
                        StringWriter output = new StringWriter();
                        templateEmail.execute(output, data);

                        gmailApiService.kirimEmail(
                                "Notifikasi Approval Attendance Request",
                                aparl.getEmail(),
                                "Approval Attendance Request",
                                output.toString());

                    }
                }

                Integer sisaApprovalAttencanceTotals = attendanceApprovalSettingDao.sisaApprovalAttendanceTotal(employeeJobPositionList2, attendanceRequest.getId());

                if (sisaApprovalAttencanceTotals == 0){
                    attendanceRequest.setStatusApprove(StatusRecord.APPROVED);
                    attendanceRequestDao.save(attendanceRequest);
                }

            }

            OvertimeRequest overtimeRequest = overTimeRequestDao.findByStatusAndId(StatusRecord.AKTIF, req);
            if (overtimeRequest != null){

                List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(overtimeRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor() );

                List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, overtimeRequest.getNomor());

                for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){

                    OvertimeRequestApproval overtimeRequestApproval = new OvertimeRequestApproval();
                    overtimeRequestApproval.setEmployes(employes);
                    overtimeRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                    overtimeRequestApproval.setOvertimeRequest(overtimeRequest);
                    overtimeRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                    overtimeRequestApproval.setStatus(StatusRecord.AKTIF);
                    overtimeRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                    overtimeRequestApproval.setKeterangan("Approve");
                    overtimeRequestApproval.setTanggalApprove(LocalDateTime.now());
                    overtimeRequestApproval.setDateUpdate(LocalDateTime.now());
                    overtimeRequestApproval.setUserUpdate(user.getUsername());

                    overTImeRequestApprovalDao.save(overtimeRequestApproval);

                }

                Integer sisaApprovalOvertime = attendanceApprovalSettingDao.sisaApprovalOvertime(employeeJobPositionList2, overtimeRequest.getId(), overtimeRequest.getNomor());

                if (sisaApprovalOvertime == 0) {
                    overtimeRequest.setNomor(overtimeRequest.getNomor() + 1);
                    overTimeRequestDao.save(overtimeRequest);
                }

                Integer sisaApprovalOvertimeTotals = attendanceApprovalSettingDao.sisaApprovalOvertimeTotal(employeeJobPositionList2, overtimeRequest.getId());

                if (sisaApprovalOvertimeTotals == 0){
                    overtimeRequest.setStatusApprove(StatusRecord.APPROVED);
                    overTimeRequestDao.save(overtimeRequest);
                }

            }

            TimeOffRequest timeOffRequest = timeOffRequestDao.findByStatusAndId(StatusRecord.AKTIF, req);
            if (timeOffRequest != null){

                List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(timeOffRequest.getEmployes().getId());
//            AttendanceApprovalSetting attendanceApprovalSetting = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequence(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor() );
                List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, timeOffRequest.getNomor());

                Integer sisaApproval = 0;

                for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
                    TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
                    timeOffRequestApproval.setEmployes(employes);
                    timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
                    timeOffRequestApproval.setTimeOffRequest(timeOffRequest);
                    timeOffRequestApproval.setStatusApprove(StatusRecord.APPROVED);
                    timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
                    timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
                    timeOffRequestApproval.setKeterangan("Approve");
                    timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
                    timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
                    timeOffRequestApproval.setUserUpdate(user.getUsername());

                    timeOffRequestApprovalDao.save(timeOffRequestApproval);
                }

                Integer sisaApprovalTimeOff = attendanceApprovalSettingDao.sisaApprovalTimeOff(employeeJobPositionList2, timeOffRequest.getId(), timeOffRequest.getNomor());

                if (sisaApprovalTimeOff == 0) {
                    timeOffRequest.setNomor(timeOffRequest.getNomor() + 1);
                    timeOffRequestDao.save(timeOffRequest);
                }

                Integer sisaApprovalTimeOffTotals = attendanceApprovalSettingDao.sisaApprovalTimeOffTotal(employeeJobPositionList2, timeOffRequest.getId());

                if (sisaApprovalTimeOffTotals == 0){
                    timeOffRequest.setStatusApprove(StatusRecord.APPROVED);
                    timeOffRequestDao.save(timeOffRequest);
                }

            }
        }

        return "redirect:../approval";
    }

}
