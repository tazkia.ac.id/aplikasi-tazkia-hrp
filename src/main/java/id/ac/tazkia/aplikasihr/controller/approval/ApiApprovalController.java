package id.ac.tazkia.aplikasihr.controller.approval;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.AttendanceRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.AttendanceRequestDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequestApproval;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequest;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequestApproval;
import id.ac.tazkia.aplikasihr.entity.setting.AttendanceApprovalSetting;
import id.ac.tazkia.aplikasihr.services.GmailApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ApiApprovalController {

    @Autowired
    private AttendanceRequestDao attendanceRequestDao;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

    @Autowired
    private TimeOffRequestApprovalDao timeOffRequestApprovalDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private TimeOffRequestDao timeOffRequestDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private AttendanceRequestApprovalDao attendanceRequestApprovalDao;

    @Autowired
    private UserDao userDao;

    @Autowired private GmailApiService gmailApiService;
    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    @Value("${approve.email}")
    private String approveEmaiil;

    @PostMapping("/api/attendance/approve")
//    @ResponseBody
    public String approvalEmailAttendance (@RequestParam String attendance,
                                           @RequestParam String approve,
                                           @RequestParam String employee){

        AttendanceApprovalSetting attendanceApprovalSetting= attendanceApprovalSettingDao.findByStatusAndId(StatusRecord.AKTIF, approve);

        Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);

        String employeeJobPositions = attendanceApprovalSetting.getPositionApprove().getId();

        AttendanceRequest attendanceRequest = attendanceRequestDao.findByStatusAndId(StatusRecord.AKTIF, attendance);
        if (attendanceRequest == null){
            AttendanceRequest attendanceRequest1 = attendanceRequestDao.findByStatusAndId(StatusRecord.CANCELED, attendance);
            if (attendanceRequest1 != null) {
                return "approval/canceled";
            }else{
                return "approval/deleted";
            }
        }else{
            String employeeJobPositionList2s = attendanceApprovalSetting.getPosition().getId();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());

//        for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSetting1){
            AttendanceRequestApproval attendanceRequestApproval = new AttendanceRequestApproval();
            attendanceRequestApproval.setEmployes(employes);
            attendanceRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
            attendanceRequestApproval.setAttendanceRequest(attendanceRequest);
            attendanceRequestApproval.setStatusApprove(StatusRecord.APPROVED);
            attendanceRequestApproval.setStatus(StatusRecord.AKTIF);
            attendanceRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
            attendanceRequestApproval.setKeterangan("Approve");
            attendanceRequestApproval.setTanggalApprove(LocalDateTime.now());
            attendanceRequestApproval.setDateUpdate(LocalDateTime.now());
            attendanceRequestApproval.setUserUpdate(employes.getUser().getUsername());

            attendanceRequestApprovalDao.save(attendanceRequestApproval);

//        }

            Integer sisaApprovalAttendances = attendanceApprovalSettingDao.sisaApprovalAttendance2(employeeJobPositionList2s, attendanceRequest.getId(), attendanceRequest.getNomor());

            if (sisaApprovalAttendances == 0) {
                attendanceRequest.setNomor(attendanceRequest.getNomor() + 1);
                attendanceRequestDao.save(attendanceRequest);

                List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(employeeJobPositionList2, attendanceRequest.getNomor() + 1);
                for (ApprovalListDetailDto aparl : approvalListDetailDtos) {

                    Mustache templateEmail = mustacheFactory.compile("templates/email/approval/attendance_request.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", aparl.getEmployeeApprove());
                    data.put("permohonan", "Berikut Attendance Request yang menunggu persetujuan anda");
                    data.put("jabatan", aparl.getPositionApprove());
                    data.put("tanggal", attendanceRequest.getTanggal().toString());
                    data.put("dari", attendanceRequest.getEmployes().getFullName());
                    data.put("departemendari", "-");
                    data.put("date", attendanceRequest.getTanggal().toString());
                    data.put("time", attendanceRequest.getJamMasuk() + " - " + attendanceRequest.getJamKeluar());
                    data.put("description", attendanceRequest.getKeterangan());
                    data.put("idApproval", aparl.getId());
                    data.put("id", attendanceRequest.getId());
                    data.put("attendance", attendanceRequest.getId());
                    data.put("approve", aparl.getId());
                    data.put("employee", aparl.getIdEmployee());
                    data.put("alamat", approveEmaiil + "/api/attendance/approve");
                    data.put("alamatr", approveEmaiil + "/api/attendance/reject");
                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Approval Attendance Request",
                            aparl.getEmail(),
                            "Approval Attendance Request",
                            output.toString());

                }
            }

            Integer sisaApprovalAttencanceTotals = attendanceApprovalSettingDao.sisaApprovalAttendanceTotal(employeeJobPositionList2, attendanceRequest.getId());

            if (sisaApprovalAttencanceTotals == 0) {
                attendanceRequest.setStatusApprove(StatusRecord.APPROVED);
                attendanceRequestDao.save(attendanceRequest);
            }


//        return "sukses";
            return "approval/sukses";
        }
    }

    @GetMapping("/suksesapprove")
    public String suksesApprove(){

        return "sukses";

    }


    @PostMapping("/api/attendance/reject")
//    @ResponseBody
    public String rejectedEmailAttendance (@RequestParam String attendance,
                                           @RequestParam String approve,
                                           @RequestParam String employee){


        AttendanceApprovalSetting attendanceApprovalSetting= attendanceApprovalSettingDao.findByStatusAndId(StatusRecord.AKTIF, approve);

        Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);

        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        AttendanceRequest attendanceRequest = attendanceRequestDao.findByStatusAndId(StatusRecord.AKTIF, attendance);
        if (attendanceRequest == null){
            AttendanceRequest attendanceRequest1 = attendanceRequestDao.findByStatusAndId(StatusRecord.CANCELED, attendance);
            if (attendanceRequest1 != null) {
                return "approval/canceled";
            }else{
                return "approval/deleted";
            }
        }else {
//        List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());
//
//        List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

//        for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
            AttendanceRequestApproval attendanceRequestApproval = new AttendanceRequestApproval();
            attendanceRequestApproval.setEmployes(employes);
            attendanceRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
            attendanceRequestApproval.setAttendanceRequest(attendanceRequest);
            attendanceRequestApproval.setStatusApprove(StatusRecord.REJECTED);
            attendanceRequestApproval.setStatus(StatusRecord.AKTIF);
            attendanceRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
            attendanceRequestApproval.setKeterangan("Rejected By Email");
            attendanceRequestApproval.setTanggalApprove(LocalDateTime.now());
            attendanceRequestApproval.setDateUpdate(LocalDateTime.now());
            attendanceRequestApproval.setUserUpdate(employes.getUser().getUsername());

            attendanceRequestApprovalDao.save(attendanceRequestApproval);
//        }


            attendanceRequest.setStatusApprove(StatusRecord.REJECTED);
            attendanceRequestDao.save(attendanceRequest);

//        return "rejected";

            return "approval/failed";
        }
    }



    @PostMapping("/api/time_off/approve")
//    @ResponseBody
    public String approveEmailTimeOff (@RequestParam String timeOff,
                                           @RequestParam String approve,
                                           @RequestParam String employee){

        AttendanceApprovalSetting attendanceApprovalSetting= attendanceApprovalSettingDao.findByStatusAndId(StatusRecord.AKTIF, approve);

        Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);

        String employeeJobPositions = attendanceApprovalSetting.getPositionApprove().getId();

        TimeOffRequest timeOffRequest = timeOffRequestDao.findByStatusAndId(StatusRecord.AKTIF, timeOff);
        if (timeOffRequest == null){
            TimeOffRequest timeOffRequest1 = timeOffRequestDao.findByStatusAndId(StatusRecord.CANCELED, timeOff);
            if (timeOffRequest1 != null) {
                return "approval/canceled";
            }else{
                return "approval/deleted";
            }
        }else{
            String employeeJobPositionList2s = attendanceApprovalSetting.getPosition().getId();
            List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(timeOffRequest.getEmployes().getId());

//        for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSetting1){
            TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
            timeOffRequestApproval.setEmployes(employes);
            timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
            timeOffRequestApproval.setTimeOffRequest(timeOffRequest);
            timeOffRequestApproval.setStatusApprove(StatusRecord.APPROVED);
            timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
            timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
            timeOffRequestApproval.setKeterangan("Approve");
            timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
            timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
            timeOffRequestApproval.setUserUpdate(employes.getUser().getUsername());

            timeOffRequestApprovalDao.save(timeOffRequestApproval);

//        }

            Integer sisaApprovalAttendances = attendanceApprovalSettingDao.sisaApprovalAttendance2(employeeJobPositionList2s, timeOffRequest.getId(), timeOffRequest.getNomor());

            if (sisaApprovalAttendances == 0) {
                timeOffRequest.setNomor(timeOffRequest.getNomor() + 1);
                timeOffRequestDao.save(timeOffRequest);

                List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(employeeJobPositionList2, timeOffRequest.getNomor() + 1);
                for (ApprovalListDetailDto aparl : approvalListDetailDtos) {

                    Mustache templateEmail = mustacheFactory.compile("templates/email/approval/time_off_request.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", aparl.getEmployeeApprove());
                    data.put("permohonan", "Berikut Time Off Request yang menunggu persetujuan anda");
                    data.put("jabatan", aparl.getPositionApprove());
                    data.put("tanggal", timeOffRequest.getTanggalPengajuan().toString());
                    data.put("dari", timeOffRequest.getEmployes().getFullName());
                    data.put("departemendari", "-");
                    data.put("date", timeOffRequest.getTanggalTimeOff().toString());
//                    data.put("time", timeOffRequest.getJamMasuk() + " - " + attendanceRequest.getJamKeluar());
                    data.put("description", timeOffRequest.getKeterangan());
                    data.put("idApproval", aparl.getId());
                    data.put("id", timeOffRequest.getId());
                    data.put("timeOff", timeOffRequest.getId());
                    data.put("approve", aparl.getId());
                    data.put("employee", aparl.getIdEmployee());
                    data.put("alamat", approveEmaiil + "/api/time_off/approve");
                    data.put("alamatr", approveEmaiil + "/api/time_off/reject");
                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Approval Attendance Request",
                            aparl.getEmail(),
                            "Approval Attendance Request",
                            output.toString());

                }
            }

            Integer sisaApprovalTimeOff = attendanceApprovalSettingDao.sisaApprovalTimeOffTotal(employeeJobPositionList2, timeOffRequest.getId());

            if (sisaApprovalTimeOff == 0) {
                timeOffRequest.setStatusApprove(StatusRecord.APPROVED);
                timeOffRequestDao.save(timeOffRequest);
            }


//        return "sukses";
            return "approval/sukses";
        }
    }


    @PostMapping("/api/time_off/reject")
//    @ResponseBody
    public String rejectTimeOff (@RequestParam String timeOff,
                                           @RequestParam String approve,
                                           @RequestParam String employee){


        AttendanceApprovalSetting attendanceApprovalSetting= attendanceApprovalSettingDao.findByStatusAndId(StatusRecord.AKTIF, approve);

        Employes employes = employesDao.findByStatusAndId(StatusRecord.AKTIF, employee);

        List<String> employeeJobPositions = employeeJobPositionDao.listJobPosition(employes.getId());

        TimeOffRequest timeOffRequest = timeOffRequestDao.findByStatusAndId(StatusRecord.AKTIF, timeOff);
        if (timeOffRequest == null){
            TimeOffRequest timeOffRequest1 = timeOffRequestDao.findByStatusAndId(StatusRecord.CANCELED, timeOff);
            if (timeOffRequest1 != null) {
                return "approval/canceled";
            }else{
                return "approval/deleted";
            }
        }else {
//        List<String> employeeJobPositionList2 = employeeJobPositionDao.listJobPosition(attendanceRequest.getEmployes().getId());
//
//        List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeJobPositions , employeeJobPositionList2, attendanceRequest.getNomor());

//        for(AttendanceApprovalSetting attendanceApprovalSetting : attendanceApprovalSettings){
            TimeOffRequestApproval timeOffRequestApproval = new TimeOffRequestApproval();
            timeOffRequestApproval.setEmployes(employes);
            timeOffRequestApproval.setAttendanceApprovalSetting(attendanceApprovalSetting);
            timeOffRequestApproval.setTimeOffRequest(timeOffRequest);
            timeOffRequestApproval.setStatusApprove(StatusRecord.REJECTED);
            timeOffRequestApproval.setStatus(StatusRecord.AKTIF);
            timeOffRequestApproval.setNumber(attendanceApprovalSetting.getSequence());
            timeOffRequestApproval.setKeterangan("Rejected By Email");
            timeOffRequestApproval.setTanggalApprove(LocalDateTime.now());
            timeOffRequestApproval.setDateUpdate(LocalDateTime.now());
            timeOffRequestApproval.setUserUpdate(employes.getUser().getUsername());

            timeOffRequestApprovalDao.save(timeOffRequestApproval);
//        }


            timeOffRequest.setStatusApprove(StatusRecord.REJECTED);
            timeOffRequestDao.save(timeOffRequest);

//        return "rejected";

            return "approval/failed";
        }
    }


}
