package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.TimeOffApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.TimeOffApprovalSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
//import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class TimeOffApprovalSettingController {

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private TimeOffApprovalSettingDao timeOffApprovalSettingDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/time_off_approval")
    public String listTimeOffApproval(Model model,
                                      @RequestParam(required = false) String search,
                                      @PageableDefault(size = 10)Pageable page,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
            model.addAttribute("listTimeOffApprovalSetting", timeOffApprovalSettingDao.findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search));
        }else{
            model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF, page));
            model.addAttribute("listTimeOffApprovalSetting", timeOffApprovalSettingDao.findByStatusOrderBySequence(StatusRecord.AKTIF));
        }

        return "setting/time_off_approval/list";

    }

    @GetMapping("/setting/time_off_approval/new")
    public String newTimeOffApproval(Model model,
                                     @RequestParam(required = true) JobPosition position,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Integer number = timeOffApprovalSettingDao.cariNumberTimeOffApproval(position.getId());
        Integer number1 = 1;

        if(number == null){
            number1 = 1 ;
        }else{
            number1 = number + 1;
        }

        model.addAttribute("position", position);
        model.addAttribute("number", number1);
        model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotContainingOrderByPositionCode(StatusRecord.AKTIF, position.getId()));
        model.addAttribute("timeOffApprovalSetting", new TimeOffApprovalSetting());

        return "setting/time_off_approval/form";

    }

    @PostMapping("/setting/time_off_approval/save")
    public String saveTimeOffApproval(@ModelAttribute @Valid TimeOffApprovalSetting timeOffApprovalSetting,
                                      @RequestParam(required = true) JobPosition position,
                                      Authentication authentication,
                                      RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        TimeOffApprovalSetting timeOffApprovalSetting1 = timeOffApprovalSettingDao.findByStatusAndPositionAndPositionApprove(StatusRecord.AKTIF, position, timeOffApprovalSetting.getPositionApprove());

        if(timeOffApprovalSetting1 == null) {
            Integer number = timeOffApprovalSettingDao.cariNumberTimeOffApproval(position.getId());
            Integer number1 = 1;

            if (number == null) {
                number1 = 1;
            } else {
                number1 = number + 1;
            }

            timeOffApprovalSetting.setSequence(number1);
            timeOffApprovalSetting.setPosition(position);
            timeOffApprovalSetting.setStatus(StatusRecord.AKTIF);
            timeOffApprovalSetting.setDateUpdate(LocalDateTime.now());
            timeOffApprovalSetting.setUserUpdate(user.getUsername());

            timeOffApprovalSettingDao.save(timeOffApprovalSetting);
            attribute.addFlashAttribute("success", "Save Data Success");
            return "redirect:../time_off_approval";
        }else{

            attribute.addFlashAttribute("already", "Save Data Failed");
            return "redirect:../time_off_approval/new?position="+ position.getId();

        }

    }

}
