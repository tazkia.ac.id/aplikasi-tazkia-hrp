package id.ac.tazkia.aplikasihr.controller.reports;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ReportEmployeeSalaryController {

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/report/employee_salary")
    public String reportEmployeeSalary(Model model,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listTotalSalaryEmployes", payrollRunDao.listTotalPayrollEmployes());
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listTotalSalaryEmployes", payrollRunDao.listTotalPayrollEmployesCompanies(idDepartements));
        }

        return "reports/employee_salary/list";
    }

}
