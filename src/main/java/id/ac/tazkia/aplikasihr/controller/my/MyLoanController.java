package id.ac.tazkia.aplikasihr.controller.my;

import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanBayarDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeLoanDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeLoan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MyLoanController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @GetMapping("/my/loan")
    public String myLoanList(Model model,
                             RedirectAttributes attribute,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listEmployeeLoan", employeeLoanDao.findByStatusNotAndEmployes(StatusRecord.HAPUS, employess));
        model.addAttribute("home","active");
        model.addAttribute("myloan","active");
        return "my/loan/list";
    }

    @GetMapping("/my/loan/detail")
    public String myLoanDetailList(Model model,
                                   @RequestParam(required = true)EmployeeLoan loan,
                                    RedirectAttributes attribute,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employeeLoan", loan);
        model.addAttribute("listEmployeeLoanDetail", employeeLoanBayarDao.historyEmployeeLoanBayar(loan.getId()));
        model.addAttribute("home","active");
        model.addAttribute("myloan","active");
        return "my/loan/detail_list";
    }

}
