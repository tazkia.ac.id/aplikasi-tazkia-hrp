package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistory;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistoryDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.export.ExportEmployesClass;
import id.ac.tazkia.aplikasihr.export.ExportExcelNewUpdateEmployeePayrollComponentClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class EmployesController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private BankDao bankDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PtkpStatusDao ptkpStatusDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private EmployeeStatusDao employeeStatusDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/masterdata/employes")
    public String listEmployes(Model model,
                               @PageableDefault(size = 10) Pageable page,
                               @RequestParam(required = false) String search,
                               @RequestParam(required = false) Companies companies,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listEmployes", employesDao.listEmployeeSearchCompany(search, companies.getId(), page));
                } else {
                    model.addAttribute("listEmployes", employesDao.listEmployeeAllCompany(companies.getId(), page));
                }
                model.addAttribute("companiesSelected", companies);
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listEmployes", employesDao.listEmployeeSearch(search, page));
                } else {
                    model.addAttribute("listEmployes", employesDao.listEmployeeAll(page));
                }
            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listEmployes", employesDao.listEmployeeSearchCompany(search, companies.getId(), page));
                } else {
                    model.addAttribute("listEmployes", employesDao.listEmployeeAllCompany(companies.getId(), page));
                }
                model.addAttribute("companiesSelected", companies);
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listEmployes", employesDao.listEmployeeSearchCompanies(search,idDepartements, page));
                } else {
                    model.addAttribute("listEmployes", employesDao.listEmployeeAllCompanies(idDepartements, page));
                }
            }

        }

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("pageable", page);
        return "masterdata/employes/list";

    }

    @GetMapping("/masterdata/employes/new")
    public String newEmployes(Model model,
                              Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

//        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
//        model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("employes", new Employes());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/employes/form";

    }

    @GetMapping("/masterdata/employes/edit")
    public String editEmployes(Model model,
                               @RequestParam(required = true) Employes employes,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("employes", employes);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/employes/form";

    }

    @PostMapping("/masterdata/employes/save")
    public String saveEmployes(@ModelAttribute @Valid Employes employes,
                               Model model,
                               Authentication authentication,
                               RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        String idFile = UUID.randomUUID().toString();

        String srole = "staff";

        User user2 = null;

        if(employes.getId().isEmpty()){
            user2 = userDao.findByUsernameAndActive(employes.getEmailActive(), true);
            if (user2 != null) {
                if(role != null) {
                    model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
                }else{
                    List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                    model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
                }
                model.addAttribute("employes", employes);
                model.addAttribute("emaileror", "Save Data Success");

                model.addAttribute("menuemployes", "active");
                model.addAttribute("masterdata", "active");
                return "masterdata/employes/form";
            }
        }else {
            user2 = userDao.findByUsernameAndActiveAndIdNot(employes.getEmailActive(), true, employes.getUser().getId());
            if (user2 != null) {

                if(role != null) {
                    model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
                }else{
                    List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                    model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
                }
                model.addAttribute("employes", employes);
                model.addAttribute("emaileror", "Save Data Success");

                model.addAttribute("menuemployes", "active");
                model.addAttribute("masterdata", "active");
                return "masterdata/employes/form";
            }
        }


        if(employes.getUser()==null) {

            User user1 = new User();
            user1.setId(UUID.randomUUID().toString());
            user1.setUsername(employes.getEmailActive());
            user1.setActive(true);
//            user1.setRole(roleDao.findById(srole).get());

            if (employes.getId() == null) {
                employes.setId(idFile);
            }


//            if(employes.getId().isEmpty()){
//                employes.setId(UUID.randomUUID().toString());
//            }
            employes.setStatus(StatusRecord.AKTIF);
            employes.setUserUpdate(user.getUsername());
            employes.setDateUpdate(LocalDateTime.now());


            employes.setUser(user1);
            if (employes.getJobStatus() == null) {
                employes.setJobStatus(StatusRecord.STAFF);
            }
            userDao.save(user1);
            employesDao.save(employes);
//            user1.setEmployes(employes);

        }else{

            User user1 = employes.getUser();
            user1.setUsername(employes.getEmailActive());

            if (employes.getId() == null) {
                employes.setId(idFile);
            }


//            if(employes.getId().isEmpty()){
//                employes.setId(UUID.randomUUID().toString());
//            }
            employes.setStatus(StatusRecord.AKTIF);
            employes.setUserUpdate(user.getUsername());
            employes.setDateUpdate(LocalDateTime.now());
            if (employes.getJobStatus() == null) {
                employes.setJobStatus(StatusRecord.STAFF);
            }

//            user.getEmployes().getId();
            employesDao.save(employes);
            userDao.save(user1);

        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../employes";

    }

    @GetMapping("/employes/export/excel")
    public void exportToExcel(@RequestParam(required = false) Companies companies,
                              @RequestParam(required = false) String search,
                              HttpServletResponse response,
                              Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        Companies companies1 = companies;
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Employes_All_Company_" + currentDateTime + ".xlsx";
        if (companies1 != null) {
            headerValue = "attachment; filename=Employes" + companies1.getCompanyName() + "_" + currentDateTime + ".xlsx";
        }
        response.setHeader(headerKey, headerValue);

        if (companies1 != null) {
            if (StringUtils.hasText(search)) {
                List<ExportEmployeeStatusDto> employesList = employesDao.listEmployeeExportWithCompany(companies1.getId());
                ExportEmployesClass excelExporter = new ExportEmployesClass(employesList);
                excelExporter.export(response);
            }else {
                List<ExportEmployeeStatusDto> employesList = employesDao.listEmployeeExportWithCompany(companies1.getId());
                ExportEmployesClass excelExporter = new ExportEmployesClass(employesList);
                excelExporter.export(response);
            }
        }else{
            if (StringUtils.hasText(search)) {
                List<ExportEmployeeStatusDto> employesList = employesDao.listEmployeeExport();
                ExportEmployesClass excelExporter = new ExportEmployesClass(employesList);
                excelExporter.export(response);
            }else {
                List<ExportEmployeeStatusDto> employesList = employesDao.listEmployeeExport();
                ExportEmployesClass excelExporter = new ExportEmployesClass(employesList);
                excelExporter.export(response);
            }
        }
    }

    @PostMapping("/employes/import/status")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        List<Employes> employesList = new ArrayList<Employes>();
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

            for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

                XSSFRow row = worksheet.getRow(i);

                Employes employes = employesDao.findById(row.getCell(0).getStringCellValue()).get();
                employes.setStatusAktif(row.getCell(3).getStringCellValue());
                employes.setStatusEmployee(row.getCell(4).getStringCellValue());
                employes.setUserUpdate(user.getUsername());
                employes.setDateUpdate(LocalDateTime.now());

                if(row.getCell(5).getStringCellValue() != null){
                    EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
                    EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusAndEmployes(StatusRecord.BASIC, employes);
                    if (employeePayrollInfo == null){
                        employeePayrollInfo = new EmployeePayrollInfo();
                        employeePayrollInfo.setEmployes(employes);
                        if (employeePayrollComponent == null) {
                            employeePayrollInfo.setBasicSalary(BigDecimal.ZERO);
                        }else{
                            employeePayrollInfo.setBasicSalary(employeePayrollComponent.getNominal());
                        }
                        employeePayrollInfo.setPtkpStatus(ptkpStatusDao.findById(row.getCell(5).getStringCellValue()).get());
                        employeePayrollInfoDao.save(employeePayrollInfo);
                    }else{
                        if (employeePayrollComponent == null) {
                            employeePayrollInfo.setBasicSalary(BigDecimal.ZERO);
                        }else{
                            employeePayrollInfo.setBasicSalary(employeePayrollComponent.getNominal());
                        }
                        employeePayrollInfo.setPtkpStatus(ptkpStatusDao.findById(row.getCell(5).getStringCellValue()).get());
                        employeePayrollInfoDao.save(employeePayrollInfo);
                    }
                }
                employesDao.save(employes);
            }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../../masterdata/employes";
    }


    @PostMapping("/masterdata/employes/status_aktif/save")
    public String updateStatusAktifEmploye(@RequestParam(required = true) Employes employes,
                                           @RequestParam(required = false) String statusAktif,
                                           @RequestParam(required = false) String tanggalActive,
                                           @RequestParam(required = false) String deskripsi,
                                           @RequestParam(required = false) Integer page,
                                           @RequestParam(required = false) Integer size,
                                           @RequestParam(required = false) String search,
                                           Authentication authentication,
                                           RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        System.out.println("Date :" + tanggalActive);
        String date = tanggalActive;
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        List<EmployeeStatusAktif> employeeStatusAktif = employeeStatusAktifDao.findByStatusAndEmployesOrderByDateUpdate(StatusRecord.AKTIF, employes);
        if (employeeStatusAktif != null){
            for(EmployeeStatusAktif employeeStatusAktife : employeeStatusAktif) {
                employeeStatusAktife.setStatus(StatusRecord.NONAKTIF);
                employeeStatusAktife.setDateUpdate(LocalDateTime.now());
                employeeStatusAktife.setUserUpdate(user.getUsername());
                employeeStatusAktifDao.save(employeeStatusAktife);
            }
        }

        System.out.println("employee : " + employes);

        employes.setStatusAktif(statusAktif);
        employes.setDateUpdate(LocalDateTime.now());
        employes.setUserUpdate(user.getUsername());
        if (employes.getJobStatus() == null) {
            employes.setJobStatus(StatusRecord.STAFF);
        }

        EmployeeStatusAktif employeeStatusAktif1 = new EmployeeStatusAktif();
        employeeStatusAktif1.setEmployes(employes);
        employeeStatusAktif1.setStatusAktif(statusAktif);
        employeeStatusAktif1.setTanggalString(tanggalActive);
        employeeStatusAktif1.setTanggal(localDate);
        employeeStatusAktif1.setStatus(StatusRecord.AKTIF);
        employeeStatusAktif1.setDeskripsi(deskripsi);
        employeeStatusAktif1.setUserUpdate(user.getUsername());
        employeeStatusAktif1.setDateUpdate(LocalDateTime.now());

        employeeStatusAktifDao.save(employeeStatusAktif1);
        employesDao.save(employes);

        attribute.addFlashAttribute("success", "Save Data Success");
//        return "redirect:../../employes?page=" + page + "&size="+ size;

        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../../employes?page=" + page + "&size="+ size;
            }else{
                return "redirect:../../employes?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../../employes";
            }else{
                return "redirect:../../employes?search=" + search;
            }
        }
    }


    @PostMapping("/masterdata/employes/status_employee/save")
    public String updateStatusAktifEmploye(@RequestParam(required = true) Employes employes,
                                           @RequestParam(required = false) String statusEmployee,
                                           @RequestParam(required = false) String dariTanggal,
                                           @RequestParam(required = false) String sampaiTanggal,
                                           @RequestParam(required = false) String deskripsi,
                                           @RequestParam(required = false) Integer page,
                                           @RequestParam(required = false) Integer size,
                                           @RequestParam(required = false) String search,
                                           Authentication authentication,
                                           RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);


        String tanggalan = dariTanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String tanggalan2 = sampaiTanggal;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);

        List<EmployeeStatus> employeeStatus = employeeStatusDao.findByStatusAndEmployesOrderByDariTanggalString(StatusRecord.AKTIF, employes);
        if (employeeStatus != null){
            for(EmployeeStatus employeeStatus2 : employeeStatus) {
                employeeStatus2.setStatus(StatusRecord.NONAKTIF);
                employeeStatus2.setDateUpdate(LocalDateTime.now());
                employeeStatus2.setUserUpdate(user.getUsername());
                employeeStatusDao.save(employeeStatus2);
            }
        }

        employes.setStatusEmployee(statusEmployee);
        employes.setDateUpdate(LocalDateTime.now());
        employes.setUserUpdate(user.getUsername());
        if (employes.getJobStatus() == null) {
            employes.setJobStatus(StatusRecord.STAFF);
        }

        EmployeeStatus employeeStatus1 = new EmployeeStatus();
        employeeStatus1.setEmployes(employes);
        employeeStatus1.setStatusEmployee(statusEmployee);
        employeeStatus1.setDariTanggalString(dariTanggal);
        employeeStatus1.setDariTanggal(localDate);
        employeeStatus1.setSampaiTanggalString(sampaiTanggal);
        employeeStatus1.setSampaiTanggal(localDate2);
        employeeStatus1.setStatus(StatusRecord.AKTIF);
        employeeStatus1.setDeskripsi(deskripsi);
        employeeStatus1.setUserUpdate(user.getUsername());
        employeeStatus1.setDateUpdate(LocalDateTime.now());

        employeeStatusDao.save(employeeStatus1);
        employesDao.save(employes);

        attribute.addFlashAttribute("success", "Save Data Success");
//        return "redirect:../../employes?page=" + page + "&size="+ size;

        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../../employes?page=" + page + "&size="+ size;
            }else{
                return "redirect:../../employes?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../../employes";
            }else{
                return "redirect:../../employes?search=" + search;
            }
        }
    }

}
