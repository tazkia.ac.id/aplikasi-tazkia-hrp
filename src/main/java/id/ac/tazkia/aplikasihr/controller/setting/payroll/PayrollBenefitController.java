package id.ac.tazkia.aplikasihr.controller.setting.payroll;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollBenefitDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollComponentDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollBenefit;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPengurang;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class PayrollBenefitController {

    @Autowired
    private PayrollComponentDao payrollComponentDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/payroll/benefit")
    public String listPayrollBenefit(Model model,
                                       @PageableDefault(size = 10) Pageable page,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("pageBenefit", payrollComponentDao.findByStatusAndJenisComponentOrderByName(StatusRecord.AKTIF, "BENEFIT", page));

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/benefit/list";

    }

    @GetMapping("/setting/payroll/benefit/new")
    public String newBenefit(Model model,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("payrollComponent", new PayrollComponent());

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/benefit/form";

    }

    @PostMapping("/setting/payroll/benefit/save")
    public String savePayrollBenefit(Model model,
                                       @ModelAttribute @Valid PayrollComponent payrollComponent,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

//        if(payrollPengurang.getId() != null) {
//            PayrollPengurang payrollPengurang1 = payrollPengurangDao.findByIdAndStatus(payrollPengurang.getId(), StatusRecord.AKTIF);
//            if (payrollPengurang1 == null) {
//
//                attribute.addFlashAttribute("invalid", "Save Data Success");
//                return "redirect:../pengurang";
//
//            }
//        }

        payrollComponent.setDateUpdate(LocalDateTime.now());
        payrollComponent.setUserUpdate(user.getUsername());
        payrollComponent.setJenisComponent("BENEFIT");
        payrollComponent.setRumus("DEFAULT");
        payrollComponent.setStatus(StatusRecord.AKTIF);

        if(payrollComponent.getTaxable() == null){
            payrollComponent.setTaxable(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setTaxable(StatusRecord.AKTIF);
        }

        if(payrollComponent.getPotonganAbsen() == null){
            payrollComponent.setPotonganAbsen(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setPotonganAbsen(StatusRecord.AKTIF);
        }

        if(payrollComponent.getPotonganTerlambat() == null){
            payrollComponent.setPotonganTerlambat(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setPotonganTerlambat(StatusRecord.AKTIF);
        }

        if(payrollComponent.getZakat() == null){
            payrollComponent.setZakat(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setZakat(StatusRecord.AKTIF);
        }

        payrollComponentDao.save(payrollComponent);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../benefit";

    }

    @GetMapping("/setting/payroll/benefit/edit")
    public String editPayrollBenefit(Model model,
                                       RedirectAttributes attribute,
                                       @RequestParam(required = true) PayrollComponent payrollComponent,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        PayrollComponent payrollComponent1 = payrollComponentDao.findByIdAndJenisComponentAndStatus(payrollComponent.getId(), "BENEFIT", StatusRecord.AKTIF);

        if (payrollComponent1 == null){

            attribute.addFlashAttribute("invalid", "Save Data Success");
            return "redirect:../benefit";

        }else{

            model.addAttribute("payrollComponent", payrollComponent1);

        }

        return "setting/payroll/benefit/form";

    }

}
