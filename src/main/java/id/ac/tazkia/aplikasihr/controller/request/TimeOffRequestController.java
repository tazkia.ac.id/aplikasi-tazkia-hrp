package id.ac.tazkia.aplikasihr.controller.request;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.ac.tazkia.aplikasihr.dto.CekShiftEmployeeAdaDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeStatusKontrakDto;
import id.ac.tazkia.aplikasihr.entity.setting.timeoff.TimeOffJenis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.timeoff.TimeOffJenisDao;
import id.ac.tazkia.aplikasihr.dto.StatusEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.TimeOffJenisDto2;
import id.ac.tazkia.aplikasihr.dto.setting.ApprovalListDetailDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequest;
import id.ac.tazkia.aplikasihr.entity.request.TimeOffRequestApproval;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.GmailApiService;
import jakarta.validation.Valid;

@Controller
public class TimeOffRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private TimeOffRequestDao timeOffRequestDao;

    @Autowired
    private TimeOffRequestApprovalDao timeOffRequestApprovalDao;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private TimeOffJenisDao timeOffJenisDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.timeoffrequest}")
    private String uploadFolder;

    @Autowired private GmailApiService gmailApiService;

    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    @Value("${approve.email}")
    private String approveEmaiil;

    @GetMapping("/request/time_off")
    public String listTimeOffRequest(Model model,
                                        @PageableDefault(size = 10) Pageable page,
                                        @RequestParam(required = false)String search,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listTimeOffRequest", timeOffRequestDao.findByStatusAndEmployesOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, employes, page));
        model.addAttribute("listApproval", attendanceApprovalSettingDao.listTimeOffRequestAttendance(employes.getId()));

        model.addAttribute("request", "active");
        model.addAttribute("menurequesttimeoff", "active");
        return "request/time_off/list";

    }

    @GetMapping("/request/time_off/new")
    public String newTimeOffRequest(Model model,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employes", employes);

        StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

        EmployeeStatusKontrakDto employeeStatusKontrakDto = employeeStatusAktifDao.cariStatusKontrak(employes.getId());
        if(employeeStatusKontrakDto != null) {
            if (employeeStatusKontrakDto.getStatus().equals("NO")) {
                model.addAttribute("kontrak", employeeStatusKontrakDto);
                model.addAttribute("jenis", " Time Off Request");
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                return "request/form_eror";
            }
        }

        model.addAttribute("statusTimeOff", statusEmployeeDto);

        System.out.println("status OK = " + statusEmployeeDto);

        if(LocalDate.now().getMonth().getValue() <= 6){
            if(statusEmployeeDto.getStatusdua().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                }else{
//                    // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
//                    TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
//                    timeOffJensiDto2.setId("");
//                    timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
//                    // timeOffJensiDtos.add(timeOffJensiDto2);
//                    model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTImeOffKaryawanKontrak(employes.getId()));
                    
                }
            }
        }else{
            if(statusEmployeeDto.getStatus().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
            }else{
//                 // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
//                    TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
//                    timeOffJensiDto2.setId("");
//                    timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
//                    // timeOffJensiDtos.add(timeOffJensiDto2);
//                    model.addAttribute("listTimeOffJenis", timeOffJensiDto2);
//
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTImeOffKaryawanKontrak(employes.getId()));
            }
        }

        model.addAttribute("timeOffRequest", new TimeOffRequest());

        model.addAttribute("request", "active");
        model.addAttribute("menurequesttimeoff", "active");
        return "request/time_off/form";

    }

    @GetMapping("/request/time_off/edit")
    public String editTimeOffRequest(Model model,
                                        @RequestParam(required = true) TimeOffRequest timeOffRequest,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employes", employes);
//        model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));
 StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

        model.addAttribute("statusTimeOff", statusEmployeeDto);

        System.out.println("status OK = " + statusEmployeeDto);


        // if(LocalDate.now().getMonth().getValue() <= 6) {
        //     model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
        // }else{
        //     model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
        // }

        if(LocalDate.now().getMonth().getValue() <= 6){
            if(statusEmployeeDto.getStatusdua().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                }else{
                    // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                    TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                    timeOffJensiDto2.setId("");
                    timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                    // timeOffJensiDtos.add(timeOffJensiDto2);
                    model.addAttribute("listTimeOffJenis", timeOffJensiDto2);
                    
                }
            }
        }else{
            if(statusEmployeeDto.getStatus().equals("OK")){
                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
            }else{
                 // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                    TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                    timeOffJensiDto2.setId("");
                    timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                    // timeOffJensiDtos.add(timeOffJensiDto2);
                    model.addAttribute("listTimeOffJenis", timeOffJensiDto2);
                
            }
        }
        
        model.addAttribute("timeOffRequest", timeOffRequest);

        model.addAttribute("request", "active");
        model.addAttribute("menurequesttimeoff", "active");
        return "request/time_off/form_edit";

    }

    @PostMapping("request/time_off/save")
    public String saveTimeOffRequest(@ModelAttribute @Valid TimeOffRequest timeOffRequest,
                                     @RequestParam("fileUpload") MultipartFile file,
                                     Model model,
                                     Authentication authentication,
                                     RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());




        if(idEmployes.isEmpty()){
            if (file == null){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request tidak dapat dilakukan, Hal ini dikarenakan jabatan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/time_off/form";
            }
        }
        //        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove1 = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);
        Integer jumlahApprove = 0;
        if (jumlahApprove1 != null){
            jumlahApprove = jumlahApprove1;
        }
        Integer nomorAwal = 1;

        if(nomorAwal > jumlahApprove){
            if (file == null){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request Attendance tidak dapat dilakukan, Hal ini dikarenakan kemungkinan jabatan atasan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/time_off/form";
            }
        }

        if(file.isEmpty()){
            if(timeOffRequest.getTimeOffJenis().getStatusQuota() == StatusRecord.NONAKTIF) {
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
//                model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));

                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                model.addAttribute("gagalfile","Pengajuan Cuti gagal, karena anda belum melampirkan file pendukung");
                return "request/time_off/form";
            }
        }

        if(nomorAwal > jumlahApprove){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("timeOffRequest", timeOffRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestattendance", "active");
            model.addAttribute("fatal","Maaf Request Time off tidak dapat dilakukan, Hal ini dikarenakan kemungkinan jabatan atasan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
            return "request/time_off/eror";
        }


//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        LocalDate localDate = LocalDate.parse(timeOffRequest.getTanggalTimeOffString(),formatter);
        LocalDate localDate1 = LocalDate.parse(timeOffRequest.getTanggalTimeOffSampaiString(),formatter);

//        String date = timeOffRequest.getTanggalTimeOffString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        String tanggalan2 = timeOffRequest.getTanggalTimeOffString() + " 00:00:00";
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        LocalDateTime localDateTime = LocalDateTime.parse(tanggalan2, formatter2);
//
//        String date1 = timeOffRequest.getTanggalTimeOffSampaiString();
//        String tahun1 = date1.substring(6,10);
//        String bulan1 = date1.substring(0,2);
//        String tanggal1 = date1.substring(3,5);
//        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        String tanggalan3 = timeOffRequest.getTanggalTimeOffSampaiString() + " 00:00:00";
//        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        LocalDateTime localDateTime1 = LocalDateTime.parse(tanggalan3, formatter2);

//        long sisaTimeoff = 0;
        Long sisaTimeOff = timeOffJenisDao.sisaTimeOff(timeOffRequest.getTimeOffJenis().getTahun(), timeOffRequest.getTimeOffJenis().getId(), employes.getId());
        if(sisaTimeOff == null){
            sisaTimeOff = 0L;
        }

        Duration duration = Duration.between(localDateTime, localDateTime1);
        Long selisihHari = duration.toDays() + 1;
        Integer selisihHari2 = selisihHari.intValue();

        System.out.println("Duration : " + selisihHari);
        System.out.println("SisaTimeOff : " + sisaTimeOff);



        System.out.println("Hari Cuti - 3 :" + localDate.minusDays(4).getDayOfYear());
        System.out.println("Hari request :" + LocalDate.now().getDayOfYear());
        System.out.println("Mulai Cuti :" + localDate.toEpochDay());
        System.out.println("Selesai Cuti :" + localDate.toEpochDay());
        System.out.println("bulan : " + localDate.getMonthValue());

        if(timeOffRequest.getTimeOffJenis().getBlockingType() != null){
            String blockingType = timeOffRequest.getTimeOffJenis().getBlockingType().toString();
            String lowercaseBlockingType = blockingType.toLowerCase();
            if(timeOffRequest.getTimeOffJenis().getBlockingType().toString().equals("YEAR")){
                Integer totalTimeOffJenis = timeOffRequestDao.sumTimeOffJenisBlocking(employes.getId(), timeOffRequest.getTimeOffJenis().getId(), timeOffRequest.getTimeOffJenis().getTahun());
                if (totalTimeOffJenis == null){
                    totalTimeOffJenis = 0;
                }
                System.out.println("Banyak Cuti : " + totalTimeOffJenis);
                if(totalTimeOffJenis + selisihHari2 > timeOffRequest.getTimeOffJenis().getBlockingCount()){
                    model.addAttribute("employess", employes);
                    model.addAttribute("employes", employes);
                    StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                    model.addAttribute("statusTimeOff", statusEmployeeDto);
                    if(LocalDate.now().getMonth().getValue() <= 6){
                        if(statusEmployeeDto.getStatusdua().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                        }else{
                            if(statusEmployeeDto.getStatus().equals("OK")){
                                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                            }else{
                                // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                                TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                                timeOffJensiDto2.setId("");
                                timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                                // timeOffJensiDtos.add(timeOffJensiDto2);
                                model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                            }
                        }
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                    model.addAttribute("timeOffRequest", timeOffRequest);
                    model.addAttribute("request", "active");
                    model.addAttribute("menurequestattendance", "active");
                    if(timeOffRequest.getTimeOffJenis().getBlockingCount() > 1) {
                        model.addAttribute("fatal", "Your time off is exceeding the quota " + timeOffRequest.getTimeOffJenis().getBlockingCount() + " days of the " + lowercaseBlockingType);
                    }else{
                        model.addAttribute("fatal", "Your time off is exceeding the quota " + timeOffRequest.getTimeOffJenis().getBlockingCount() + " day of the " + lowercaseBlockingType);
                    }
                    return "request/time_off/form";
                }
            }
            if (timeOffRequest.getTimeOffJenis().getBlockingType().toString().equals("MONTH")){
                Integer totalTimeOffJenis = timeOffRequestDao.sumTimeOffJenisBlockingMonth(employes.getId(), timeOffRequest.getTimeOffJenis().getId(), localDate.getMonthValue(), localDate.getYear());
                if (totalTimeOffJenis == null){
                    totalTimeOffJenis = 0;
                }
                if(totalTimeOffJenis + selisihHari2 > timeOffRequest.getTimeOffJenis().getBlockingCount()){
                    model.addAttribute("employess", employes);
                    model.addAttribute("employes", employes);
                    StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                    model.addAttribute("statusTimeOff", statusEmployeeDto);
                    if(LocalDate.now().getMonth().getValue() <= 6){
                        if(statusEmployeeDto.getStatusdua().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                        }else{
                            if(statusEmployeeDto.getStatus().equals("OK")){
                                model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                            }else{
                                // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                                TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                                timeOffJensiDto2.setId("");
                                timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                                // timeOffJensiDtos.add(timeOffJensiDto2);
                                model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                            }
                        }
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                    model.addAttribute("timeOffRequest", timeOffRequest);
                    model.addAttribute("request", "active");
                    model.addAttribute("menurequestattendance", "active");
                    if(timeOffRequest.getTimeOffJenis().getBlockingCount() > 1) {
                        model.addAttribute("fatal", "Your time off is exceeding the quota " + timeOffRequest.getTimeOffJenis().getBlockingCount() + " days of the " + lowercaseBlockingType);
                    }else{
                        model.addAttribute("fatal", "Your time off is exceeding the quota " + timeOffRequest.getTimeOffJenis().getBlockingCount() + " day of the " + lowercaseBlockingType);
                    }
                    return "request/time_off/form";
                }
            }
        }


        if (localDate.minusDays(3).isBefore(LocalDate.now()) || localDate.minusDays(2).isEqual(LocalDate.now())) {
            if(timeOffRequest.getTimeOffJenis().getStatusQuota() == StatusRecord.AKTIF) {
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
//                model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));

                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                model.addAttribute("gagalfile","Pengajuan Cuti gagal, request cuti minimal 3 hari sebelum tanggal cuti");
                return "request/time_off/form";
            }
        }

        if(timeOffRequest.getTimeOffJenis().getStatusQuota() == StatusRecord.AKTIF){
            if(selisihHari > sisaTimeOff){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
//                model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));

                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                model.addAttribute("lebih", "Save Data Lebih");
                return "request/time_off/form";
            }
        }else{
            if(selisihHari > timeOffRequest.getTimeOffJenis().getCount()){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

                model.addAttribute("statusTimeOff", statusEmployeeDto);
//                model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));

                if(LocalDate.now().getMonth().getValue() <= 6){
                    if(statusEmployeeDto.getStatusdua().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                    }else{
                        if(statusEmployeeDto.getStatus().equals("OK")){
                            model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                        }else{
                            // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                            TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                            timeOffJensiDto2.setId("");
                            timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                            // timeOffJensiDtos.add(timeOffJensiDto2);
                            model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                        }
                    }
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                model.addAttribute("lebihan", timeOffRequest.getTimeOffJenis().getCount());
                return "request/time_off/form";
            }
        }

        if(localDate.toEpochDay() > localDate1.toEpochDay()){

                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
            StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employes.getId());

            model.addAttribute("statusTimeOff", statusEmployeeDto);
//                model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusOrderByTimeOffName(StatusRecord.AKTIF));

            if(LocalDate.now().getMonth().getValue() <= 6){
                if(statusEmployeeDto.getStatusdua().equals("OK")){
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSebelumBulanTujuh(employes.getId()));
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                    }else{
                        // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                        TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                        timeOffJensiDto2.setId("");
                        timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                        // timeOffJensiDtos.add(timeOffJensiDto2);
                        model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                    }
                }
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("listTimeOffJenis", timeOffJenisDao.listTimeOffJenisSetelahBulanTujuh(employes.getId()));
                }else{
                    // List<TimeOffJenisDto> timeOffJensiDtos = new ArrayList<>();
                    TimeOffJenisDto2 timeOffJensiDto2 = new TimeOffJenisDto2();
                    timeOffJensiDto2.setId("");
                    timeOffJensiDto2.setNama("Tidak ada cuti yang bisa diambil karena masa kerja anda belum genap satu tahun");
                    // timeOffJensiDtos.add(timeOffJensiDto2);
                    model.addAttribute("listTimeOffJenis", timeOffJensiDto2);

                }
            }
                model.addAttribute("timeOffRequest", timeOffRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequesttimeoff", "active");
                model.addAttribute("gagalfile","Pengajuan Cuti gagal, Tanggal mulai cuti tidak boleh lebih besar dari tanggal selesai cuti");
                return "request/time_off/form";
        }

        timeOffRequest.setTanggalTimeOff(localDate);
        timeOffRequest.setTanggalTimeOffSampai(localDate1);
        timeOffRequest.setDateUpdate(LocalDateTime.now());
        timeOffRequest.setUserUpdate(user.getUsername());
        timeOffRequest.setNomor(nomorAwal);
        timeOffRequest.setTahun(timeOffRequest.getTimeOffJenis().getTahun());
        timeOffRequest.setTotalNomor(jumlahApprove);
        if (nomorAwal > jumlahApprove){
            timeOffRequest.setStatusApprove(StatusRecord.APPROVED);
        }else {
            timeOffRequest.setStatusApprove(StatusRecord.WAITING);
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(timeOffRequest.getFile() == null){
                timeOffRequest.setFile("default.jpg");
            }
        }else{
            timeOffRequest.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        timeOffRequestDao.save(timeOffRequest);

//        TimeOffRequest timeOffRequest1 = timeOffRequestDao.findByStatusAndEmployesAndTanggalAndKeteranganAndStatusApproveAndJamMasukAndJamKeluarAndNomor(StatusRecord.AKTIF, attendanceRequest.getEmployes(), attendanceRequest.getTanggal(), attendanceRequest.getKeterangan(), StatusRecord.WAITING, attendanceRequest.getJamMasuk(), attendanceRequest.getJamKeluar(), attendanceRequest.getNomor());

        if (timeOffRequest != null){
            List<ApprovalListDetailDto> approvalListDetailDtos = attendanceApprovalSettingDao.cariApprovalEmail(idEmployes, 1);
            for(ApprovalListDetailDto aparl : approvalListDetailDtos){

                Mustache templateEmail = mustacheFactory.compile("templates/email/approval/time_off_request.html");
                Map<String, String> data = new HashMap<>();
                data.put("nama", aparl.getEmployeeApprove());
                data.put("permohonan", "Berikut Time Off Request yang menunggu persetujuan anda");
                data.put("jabatan", aparl.getPositionApprove());
                data.put("tanggal", timeOffRequest.getTanggalPengajuan().toString());
                data.put("dari", timeOffRequest.getEmployes().getFullName());
                data.put("departemendari", "-");
                data.put("date", timeOffRequest.getTanggalTimeOff().toString());
//                data.put("time", timeOffRequest.getJ()+ " - " +attendanceRequest.getJamKeluar());
                data.put("description", timeOffRequest.getKeterangan());
                data.put("idApproval", aparl.getId());
                data.put("id", timeOffRequest.getId());
                data.put("timeOff", timeOffRequest.getId());
                data.put("approve", aparl.getId());
                data.put("employee", aparl.getIdEmployee());
                data.put("alamat",approveEmaiil+"/api/time_off/approve");
                data.put("alamatr",approveEmaiil+"/api/time_off/reject");

                StringWriter output = new StringWriter();
                templateEmail.execute(output, data);

                gmailApiService.kirimEmail(
                        "Notifikasi Approval Time Off Request",
                        aparl.getEmail(),
                        "Approval Time Off Request",
                        output.toString());

            }
        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../time_off";

    }


    @PostMapping("request/time_off/delete")
    @Transactional
    public String deleteTimeOffRequest(@RequestParam (required = true) TimeOffRequest timeOffRequest,
                                          Authentication authentication,
                                          RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);

        List<TimeOffRequestApproval> timeOffRequestApprovals = timeOffRequestApprovalDao.findByStatusAndTimeOffRequest(StatusRecord.AKTIF, timeOffRequest);
        for (TimeOffRequestApproval s : timeOffRequestApprovals){

            s.setStatusApprove(StatusRecord.HAPUS);
            s.setStatus(StatusRecord.HAPUS);

            timeOffRequestApprovalDao.save(s);

        }

        timeOffRequest.setStatus(StatusRecord.HAPUS);
        timeOffRequest.setStatusApprove(StatusRecord.CANCELED);
        timeOffRequest.setDateUpdate(LocalDateTime.now());
        timeOffRequest.setUserUpdate(user.getUsername());

        timeOffRequestDao.save(timeOffRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../time_off";

    }

    @GetMapping("/api/ceklimittimeoff")
    @ResponseBody
    public String cariLimit(@RequestParam(required = true) String tanggalMulai,
                                               @RequestParam(required = true) String tanggalSelesai,
                                               @RequestParam(required = true) TimeOffJenis timeOffJenis,
                                               @RequestParam(required = true) String employee){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate localDate = LocalDate.parse(tanggalMulai,formatter);
        LocalDate localDate1 = LocalDate.parse(tanggalSelesai,formatter);
        String tanggalan2 = tanggalMulai + " 00:00:00";
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(tanggalan2, formatter2);
        String tanggalan3 = tanggalSelesai + " 00:00:00";
        LocalDateTime localDateTime1 = LocalDateTime.parse(tanggalan3, formatter2);

//        if(timeOffJenis.t)

        Long sisaTimeOff = timeOffJenisDao.sisaTimeOff(timeOffJenis.getTahun(), timeOffJenis.getId(), employee);

        Duration duration = Duration.between(localDateTime, localDateTime1);
        Long selisihHari = duration.toDays() + 1;

        String cekLimit = "Cuti yang anda pilih saat ini telah melebihi batas limit";
        return cekLimit;

    }


    @GetMapping("/time_off/{timeOffRequest}/request/")
    public ResponseEntity<byte[]> tampilkanBuktiAttendance(@PathVariable TimeOffRequest timeOffRequest) throws Exception {
        String lokasiFile = uploadFolder + File.separator + timeOffRequest.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (timeOffRequest.getFile().toLowerCase().endsWith("jpeg") || timeOffRequest.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (timeOffRequest.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (timeOffRequest.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
