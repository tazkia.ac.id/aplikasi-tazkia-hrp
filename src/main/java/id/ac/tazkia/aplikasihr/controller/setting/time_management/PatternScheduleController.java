package id.ac.tazkia.aplikasihr.controller.setting.time_management;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternScheduleDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
public class PatternScheduleController {

    @Autowired
    private PatternDao patternDao;

    @Autowired
    private PatternScheduleDao patternScheduleDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/timemanagement/pattern/schedule")
    public String patternSchedule(Model model,
                                  @RequestParam(required = false) String search,
                                  @PageableDefault(size = 10)Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPatternSchedule", patternScheduleDao.findByStatusAndScheduleNameContainingIgnoreCase(StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listPatternSchedule", patternScheduleDao.findByStatusOrderByScheduleName(StatusRecord.AKTIF, page));
        }

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/pattern_schedule/list";

    }

    @GetMapping("/setting/timemanagement/pattern/schedule/new")
    public String newPatternSchedule(Model model,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listPattern", patternDao.findByStatusOrderByPatternName(StatusRecord.AKTIF));
        model.addAttribute("patternSchedule", new PatternSchedule());

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/pattern_schedule/form";

    }

    @GetMapping("/setting/timemanagement/pattern/schedule/edit")
    public String editPatternSchedule(Model model,
                                      @RequestParam(required = false) PatternSchedule patternSchedule,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listPattern", patternDao.findByStatusOrderByPatternName(StatusRecord.AKTIF));
        model.addAttribute("patternSchedule", patternSchedule);

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/pattern_schedule/form";

    }

    @PostMapping("/setting/timemanagement/pattern/schedule/save")
    public String saveShiftPattern(@ModelAttribute @Valid PatternSchedule patternSchedule,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = patternSchedule.getEfectiveDateString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        patternSchedule.setEfectiveDate(localDate);

        if (patternSchedule.getFlexible() == null){
            patternSchedule.setFlexible("NONAKTIF");
        }
        if (patternSchedule.getOverideCompanyHoliday() == null){
            patternSchedule.setOverideCompanyHoliday("NONAKTIF");
        }
        if (patternSchedule.getOverideNationalHoliday() == null){
            patternSchedule.setOverideNationalHoliday("NONAKTIF");
        }
        if (patternSchedule.getAttendance() == null){
            patternSchedule.setAttendance("NONAKTIF");
        }
        if(patternSchedule.getOvertimeHoliday() == null){
            patternSchedule.setOvertimeHoliday(StatusRecord.NONAKTIF);
        }
        if(patternSchedule.getOvertimeWorkingDay() == null){
            patternSchedule.setOvertimeWorkingDay(StatusRecord.NONAKTIF);
        }


        patternSchedule.setDateUpdate(LocalDateTime.now());
        patternSchedule.setUserUpdate(user.getUsername());
        patternSchedule.setStatus(StatusRecord.AKTIF);

        patternScheduleDao.save(patternSchedule);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../schedule";

    }

}
