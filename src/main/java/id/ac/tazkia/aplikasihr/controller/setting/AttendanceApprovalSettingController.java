package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;

import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.request.AttendanceRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.AttendanceApprovalSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class AttendanceApprovalSettingController {

        @Autowired
        private JobPositionDao jobPositionDao;

        @Autowired
        private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

        @Autowired
        private AttendanceRequestApprovalDao attendanceRequestApprovalDao;

        @Autowired
        private CurrentUserService currentUserService;

        @Autowired
        private EmployesDao employesDao;

        @GetMapping("/setting/attendance_approval")
        public String listTimeOffApprovalss(Model model,
                                          @RequestParam(required = false) String search,
                                          @PageableDefault(size = 10) Pageable page,
                                          Authentication authentication){

            User user = currentUserService.currentUser(authentication);
            Employes employess = employesDao.findByUser(user);
            model.addAttribute("employess", employess);

            if (StringUtils.hasText(search)) {
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
                model.addAttribute("listAttendanceApprovalSetting", attendanceApprovalSettingDao.listAttendanceApprovalSetting(StatusRecord.AKTIF));
            }else{
                model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF, page));
                model.addAttribute("listAttendanceApprovalSetting", attendanceApprovalSettingDao.findByStatusOrderBySequence(StatusRecord.AKTIF));
            }

            model.addAttribute("menuapproval", "active");
            model.addAttribute("setting", "active");
            return "setting/attendance_approval/list";

        }

        @GetMapping("/setting/attendance_approval/new")
        public String newTimeOffApprovalss(Model model,
                                         @RequestParam(required = true) JobPosition position,
                                         Authentication authentication){

            User user = currentUserService.currentUser(authentication);
            Employes employess = employesDao.findByUser(user);
            model.addAttribute("employess", employess);

            Integer number = attendanceApprovalSettingDao.cariNumberAttendanceApproval(position.getId());
            Integer number1 = 1;

            if(number == null){
                number1 = 1 ;
            }else{
                number1 = number + 1;
            }

            model.addAttribute("position", position);
            model.addAttribute("number", number1);
//            model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotContainingOrderByPositionCode(StatusRecord.AKTIF, position.getId()));
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotOrderByPositionName(StatusRecord.AKTIF, position.getId()));
            model.addAttribute("attendanceApprovalSetting", new AttendanceApprovalSetting());

            model.addAttribute("menuapproval", "active");
            model.addAttribute("setting", "active");
            return "setting/attendance_approval/form";

        }

        @GetMapping("/setting/attendance_approval/edit")
        public String editAttendanceApproval(Model model,
                                             @RequestParam(required = true) AttendanceApprovalSetting attendanceApprovalSetting){

            model.addAttribute("position", attendanceApprovalSetting.getPosition());
            model.addAttribute("number", attendanceApprovalSetting.getSequence());
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotContainingOrderByPositionCode(StatusRecord.AKTIF, attendanceApprovalSetting.getPosition().getId()));
            model.addAttribute("attendanceApprovalSetting", attendanceApprovalSetting);

            model.addAttribute("menuapproval", "active");
            model.addAttribute("setting", "active");
            return "setting/attendance_approval/form";

        }

        @PostMapping("/setting/attendance_approval/save")
        public String saveTimeOffApproval(@ModelAttribute @Valid AttendanceApprovalSetting attendanceApprovalSetting,
                                          @RequestParam(required = true) JobPosition position,
                                          Authentication authentication,
                                          RedirectAttributes attribute){

            User user = currentUserService.currentUser(authentication);

            AttendanceApprovalSetting attendanceApprovalSetting1 = attendanceApprovalSettingDao.findByStatusAndPositionAndPositionApprove(StatusRecord.AKTIF, position, attendanceApprovalSetting.getPositionApprove());

            if(attendanceApprovalSetting1 == null) {
                Integer number = attendanceApprovalSettingDao.cariNumberAttendanceApproval(position.getId());
                Integer number1 = 1;

                if (number == null) {
                    number1 = 1;
                } else {
                    number1 = number + 1;
                }

                attendanceApprovalSetting.setSequence(number1);
                attendanceApprovalSetting.setPosition(position);
                attendanceApprovalSetting.setStatus(StatusRecord.AKTIF);
                attendanceApprovalSetting.setDateUpdate(LocalDateTime.now());
                attendanceApprovalSetting.setUserUpdate(user.getUsername());

                attendanceApprovalSettingDao.save(attendanceApprovalSetting);
                attribute.addFlashAttribute("success", "Save Data Success");
                return "redirect:../attendance_approval";
            }else{

                attribute.addFlashAttribute("already", "Save Data Failed");
                return "redirect:../attendance_approval/new?position="+ position.getId();

            }

        }

        @PostMapping("/setting/attendance_approval/delete")
        public String deleteAttendanceApproval(@ModelAttribute @Valid AttendanceApprovalSetting attendanceApprovalSetting,
                                               Authentication authentication,
                                               RedirectAttributes attribute){

            User user = currentUserService.currentUser(authentication);
            JobPosition position = attendanceApprovalSetting.getPosition();

            Integer approvalAktif = attendanceRequestApprovalDao.countByStatusAndAttendanceApprovalSettingAndStatusApprove(StatusRecord.AKTIF, attendanceApprovalSetting, StatusRecord.WAITING);

            if(approvalAktif > 0){
                attribute.addFlashAttribute("aktif", "Save Data Gagal");
                return "redirect:../attendance_approval";
            }else {
                List<AttendanceApprovalSetting> attendanceApprovalSettings = attendanceApprovalSettingDao.findByStatusAndPositionAndSequenceGreaterThanOrderBySequence(StatusRecord.AKTIF, position, attendanceApprovalSetting.getSequence());

                attendanceApprovalSetting.setDateUpdate(LocalDateTime.now());
                attendanceApprovalSetting.setUserUpdate(user.getUsername());
                attendanceApprovalSetting.setStatus(StatusRecord.HAPUS);

                for (AttendanceApprovalSetting s : attendanceApprovalSettings) {

                    s.setSequence(s.getSequence() - 1);
                    attendanceApprovalSettingDao.save(s);

                }

                attendanceApprovalSettingDao.save(attendanceApprovalSetting);
                attribute.addFlashAttribute("success", "Save Data Success");
                return "redirect:../attendance_approval";
            }

        }

}



