package id.ac.tazkia.aplikasihr.controller.api;


import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceHistoryDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.api.EmployeeScheduleDailyDto;
import id.ac.tazkia.aplikasihr.dto.download.DownloadAbsensiDto;
import id.ac.tazkia.aplikasihr.dto.response.BaseResponse;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.Attendance;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@RestController
@Slf4j
public class ImporAbsensiDariMesinHarianApi {

    @Autowired
    private AttendanceDao attendanceDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;
    @Autowired
    private AttendanceHistoryDao attendanceHistoryDao;


    @PostMapping(value = "/impor/absen/harian", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public BaseResponse importAbsenRevDuaPuluh(@RequestBody DownloadAbsensiDto request) {

        System.out.println("impor absen harian jalan :");
        System.out.println("Request : " + request);
        LocalDate localDate = LocalDate.now();

        if(request.getTanggal().isAfter(localDate.minusDays(30))){
            System.out.println("Date : " +  request.getTanggal());
            Integer jumlah = employesDao.countByStatusAndIdAbsen(StatusRecord.AKTIF, request.getId());
            //Cari Shift pegawai pada tanggal yang dimaksud
            if (jumlah == 1) {
                //ambil data pegawai nya
                Employes employes = employesDao.findByStatusAndIdAbsen(StatusRecord.AKTIF, request.getId());
                //Cari Shift pegawai pada tanggal yang dimaksud
                EmployeeScheduleDailyDto employeeScheduleDailyDto = employeeScheduleDao.cariShiftHarianPegawai(employes.getId(), request.getTanggal());
                if(employeeScheduleDailyDto != null){
                    Attendance attendance = attendanceDao.findByStatusAndEmployesAndDate(StatusRecord.AKTIF, employes, request.getTanggal());
                    String shiftMasuk = employeeScheduleDailyDto.getTanggalMasuk()+" "+employeeScheduleDailyDto.getShiftMasuk()+":00";
                    String shiftKeluar = employeeScheduleDailyDto.getTanggalKeluar()+" "+employeeScheduleDailyDto.getShiftKeluar()+":00";
                    String absen = request.getTanggal()+" "+request.getWaktu();
                    if(request.getWaktu().toString().length() == 5){
                        absen = request.getTanggal()+" "+request.getWaktu() + ":00";
                    }
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime absenHitung = LocalDateTime.parse(absen, formatter);


                    LocalDateTime shiftMasukHitung = LocalDateTime.parse(shiftMasuk, formatter);
                    LocalDateTime shiftKeluarHitung = LocalDateTime.parse(shiftKeluar, formatter);
                    absenHitung = absenHitung.plusHours(7);

                    long selisihWaktuMasuk = ChronoUnit.HOURS.between(shiftMasukHitung, absenHitung);
                    long selisihWaktuKeluar = ChronoUnit.HOURS.between(absenHitung, shiftKeluarHitung);

                    System.out.println("selisih masuk : " + selisihWaktuMasuk);
                    System.out.println("selisih keluar : " + selisihWaktuKeluar);
                    if(attendance != null){
//                        System.out.println("attendance : " + attendance);
                        if(attendance.getWaktuMasuk() == null) {
                            if(selisihWaktuMasuk <= selisihWaktuKeluar) {
                                attendance.setWaktuMasuk(absenHitung.toLocalTime());
                            }
                        }else{
                            String waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().minusHours(7).toString();
                            if (attendance.getWaktuMasuk().toString().length() == 5) {
                                waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().minusHours(7).toString() + ":00";
                            }
                            System.out.println("waktu masuk sebelumnya : " + attendance.getWaktuMasuk());
                            LocalDateTime waktuMasukHitung = LocalDateTime.parse(waktuMasuk, formatter);
                            if (absenHitung.minusHours(7).isBefore(waktuMasukHitung)) {
                                System.out.println("waktu masuk : " + absenHitung.toLocalTime());
                                attendance.setWaktuMasuk(absenHitung.toLocalTime());
                            }
                        }

                        if(attendance.getWaktuKeluar() == null) {
                            if(selisihWaktuMasuk > selisihWaktuKeluar) {
                                attendance.setWaktuKeluar(absenHitung.toLocalTime());
                            }
                        }else {
                            String waktuKeluar = attendance.getDate().toString() + " " + attendance.getWaktuKeluar().toString();
                            if(attendance.getWaktuKeluar().toString().length() == 5){
                                waktuKeluar = attendance.getDate().toString() + " " + attendance.getWaktuKeluar().toString() + ":00";
                            }
                            System.out.println("waktu keluar sebelumnya : " + attendance.getWaktuKeluar());
                            LocalDateTime waktuKeluarHitung = LocalDateTime.parse(waktuKeluar, formatter);
                            if (absenHitung.minusHours(7).isAfter(waktuKeluarHitung.minusHours(7))){
                                System.out.println("waktu keluar : " + absenHitung.toLocalTime());
                                attendance.setWaktuKeluar(absenHitung.toLocalTime());
                            }
                        }

                        attendance.setDateUpdate(LocalDateTime.now());
                        attendance.setUserUpdate("mesin");
                        attendanceDao.save(attendance);
                        return new BaseResponse(HttpStatus.OK.getReasonPhrase(),
                                String.valueOf(HttpStatus.OK.value()));
                    }else{
                        Attendance attendance1 = new Attendance();
                        attendance1.setEmployes(employes);
                        attendance1.setAttendanceHistory(attendanceHistoryDao.findByStatusAndId(StatusRecord.AKTIF, "-"));
                        attendance1.setDate(request.getTanggal());
                        attendance1.setStatus(StatusRecord.AKTIF);
                        if(selisihWaktuMasuk <= selisihWaktuKeluar) {
                            attendance1.setWaktuMasuk(absenHitung.toLocalTime());
                        }else {
                            attendance1.setWaktuKeluar(absenHitung.toLocalTime());
                        }
                        attendance1.setDateUpdate(LocalDateTime.now());
                        attendance1.setUserUpdate("mesin");
                        attendanceDao.save(attendance1);
                    }
                }
            }
        }

        return new BaseResponse(HttpStatus.FORBIDDEN.getReasonPhrase(),
                String.valueOf(HttpStatus.FORBIDDEN.value()));

    }



    @PostMapping(value = "/impor/absen/harianmatrik", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public BaseResponse importAbsenMatrikHarian(@RequestBody DownloadAbsensiDto request) {

        System.out.println("impor absen harian jalan :");
        System.out.println("Request : " + request);
        LocalDate localDate = LocalDate.now();


        if(request.getTanggal().isAfter(localDate.minusDays(30))){
            Integer jumlah = employesDao.countByStatusAndIdAbsen(StatusRecord.AKTIF, request.getId()); //cek id absen pegawai tidak boleh ada yang sama (jika sama maka dilewat)
            //Cari Shift pegawai pada tanggal yang dimaksud
            if (jumlah == 1) {
                //ambil data pegawai nya
                Employes employes = employesDao.findByStatusAndIdAbsen(StatusRecord.AKTIF, request.getId());
                //Cari Shift pegawai pada tanggal yang dimaksud
                EmployeeScheduleDailyDto employeeScheduleDailyDto = employeeScheduleDao.cariShiftHarianPegawai(employes.getId(), request.getTanggal());
                EmployeeScheduleDailyDto employeeScheduleDailyDtoSebelum = employeeScheduleDao.cariShiftHarianPegawai(employes.getId(), request.getTanggal().minusDays(1));
                if(employeeScheduleDailyDto != null){
                    //jika shift hari ini adalah shift 3
                    if (employeeScheduleDailyDto.getShiftMasuk().isAfter(employeeScheduleDailyDto.getShiftKeluar())){ //cari yang shift n 3
                        //jika hari sebelum nya tidak ada shift
                        if(employeeScheduleDailyDtoSebelum == null){
                            Attendance attendance = attendanceDao.findByStatusAndEmployesAndDate(StatusRecord.AKTIF, employes, request.getTanggal());
                            String shiftMasuk = employeeScheduleDailyDto.getTanggalMasuk()+" "+employeeScheduleDailyDto.getShiftMasuk()+":00";
                            String shiftKeluar = employeeScheduleDailyDto.getTanggalKeluar()+" "+employeeScheduleDailyDto.getShiftKeluar()+":00";
                            String absen = request.getTanggal()+" "+request.getWaktu();
                            if(request.getWaktu().toString().length() == 5){
                                absen = request.getTanggal()+" "+request.getWaktu() + ":00";
                            }
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime absenHitung = LocalDateTime.parse(absen, formatter);
                            LocalDateTime shiftMasukHitung = LocalDateTime.parse(shiftMasuk, formatter);
                            LocalDateTime shiftKeluarHitung = LocalDateTime.parse(shiftKeluar, formatter);
                            absenHitung = absenHitung.plusHours(7);

                            long selisihWaktuMasuk = ChronoUnit.HOURS.between(shiftMasukHitung, absenHitung);
                            long selisihWaktuKeluar = ChronoUnit.HOURS.between(absenHitung, shiftKeluarHitung);

                            System.out.println("selisih masuk : " + selisihWaktuMasuk);
                            System.out.println("selisih keluar : " + selisihWaktuKeluar);

                            //jika sudah ada absen tercatat di hari ini
                            if(attendance != null){
                                //masukan absem masuk saja
                                if(attendance.getWaktuMasuk() == null) {
                                    if(selisihWaktuMasuk <= selisihWaktuKeluar) {
                                        attendance.setWaktuMasuk(absenHitung.toLocalTime());
                                    }
                                }else{
                                    String waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().toString();
                                    if(attendance.getWaktuMasuk().toString().length() == 5){
                                        waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().toString() + ":00";
                                    }
                                    LocalDateTime waktuMasukHitung = LocalDateTime.parse(waktuMasuk, formatter);
                                    if (absenHitung.isBefore(waktuMasukHitung)){
                                        attendance.setWaktuMasuk(absenHitung.toLocalTime());
                                    }
                                }
                                attendance.setDateUpdate(LocalDateTime.now());
                                attendance.setUserUpdate("mesin");
                                attendanceDao.save(attendance);
                                return new BaseResponse(HttpStatus.OK.getReasonPhrase(),
                                        String.valueOf(HttpStatus.OK.value()));
                                //jika belum ada absen masuk tercatat
                            }else{
                                //masukan absen masuk saja di hari ini
                                Attendance attendance1 = new Attendance();
                                attendance1.setEmployes(employes);
                                attendance1.setAttendanceHistory(attendanceHistoryDao.findByStatusAndId(StatusRecord.AKTIF, "-"));
                                attendance1.setDate(request.getTanggal());
                                attendance1.setStatus(StatusRecord.AKTIF);
                                if(selisihWaktuMasuk <= selisihWaktuKeluar) {
                                    attendance1.setWaktuMasuk(absenHitung.toLocalTime());
                                }
                                attendance1.setDateUpdate(LocalDateTime.now());
                                attendance1.setUserUpdate("mesin");
                                attendanceDao.save(attendance1);
                            }

                            //Jika di hari sebelum nya ada shift juga
                        }else{
                            //jika di hari sebelum nya ybs shift 3 juga
                            if (employeeScheduleDailyDtoSebelum.getShiftMasuk().isAfter(employeeScheduleDailyDtoSebelum.getShiftKeluar())) {
                                Attendance attendanceSebelum = attendanceDao.findByStatusAndEmployesAndDate(StatusRecord.AKTIF, employes, request.getTanggal().minusDays(1));
                                String shiftMasukSebelum = employeeScheduleDailyDtoSebelum.getTanggalMasuk() + " " + employeeScheduleDailyDtoSebelum.getShiftMasuk() + ":00";
                                String shiftKeluarSebelum = employeeScheduleDailyDtoSebelum.getTanggalKeluar() + " " + employeeScheduleDailyDtoSebelum.getShiftKeluar() + ":00";
                                String absen = request.getTanggal()+" "+request.getWaktu();
                                if(request.getWaktu().toString().length() == 5){
                                    absen = request.getTanggal()+" "+request.getWaktu() + ":00";
                                }
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                                LocalDateTime absenHitung = LocalDateTime.parse(absen, formatter);
                                LocalDateTime shiftMasukHitungSebelum = LocalDateTime.parse(shiftMasukSebelum, formatter);
                                LocalDateTime shiftKeluarHitungSebelum = LocalDateTime.parse(shiftKeluarSebelum, formatter);
                                absenHitung = absenHitung.plusHours(7);

                                long selisihWaktuMasuk = ChronoUnit.HOURS.between(shiftMasukHitungSebelum, absenHitung);
                                long selisihWaktuKeluar = ChronoUnit.HOURS.between(absenHitung, shiftKeluarHitungSebelum);

                                System.out.println("selisih masuk : " + selisihWaktuMasuk);
                                System.out.println("selisih keluar : " + selisihWaktuKeluar);
                                //jika sudah ada absen di hari sebelum nya
                                if(attendanceSebelum != null){
                                    //masukan absen keluar nya saja
                                    if(attendanceSebelum.getWaktuKeluar() == null) {
                                        if(selisihWaktuMasuk > selisihWaktuKeluar) {
                                            attendanceSebelum.setWaktuKeluar(absenHitung.toLocalTime());
                                        }
                                    }else{
                                        String waktuKeluar = attendanceSebelum.getDate().toString() + " " + attendanceSebelum.getWaktuKeluar().toString();
                                        if(attendanceSebelum.getWaktuKeluar().toString().length() == 5){
                                            waktuKeluar = attendanceSebelum.getDate().toString() + " " + attendanceSebelum.getWaktuKeluar().toString() + ":00";
                                        }
                                        LocalDateTime waktuKeluarHitung = LocalDateTime.parse(waktuKeluar, formatter);
                                        if (absenHitung.isAfter(waktuKeluarHitung)){
                                            attendanceSebelum.setWaktuKeluar(absenHitung.toLocalTime());
                                        }
                                    }
                                    attendanceSebelum.setDateUpdate(LocalDateTime.now());
                                    attendanceSebelum.setUserUpdate("mesin");
                                    attendanceDao.save(attendanceSebelum);
                                    return new BaseResponse(HttpStatus.OK.getReasonPhrase(),
                                            String.valueOf(HttpStatus.OK.value()));
                                    //jika belum ada absen di hari sebelum nya
                                }else{
                                    //masukan data baru dan input absen keluar nya saja
                                    Attendance attendance1 = new Attendance();
                                    attendance1.setEmployes(employes);
                                    attendance1.setAttendanceHistory(attendanceHistoryDao.findByStatusAndId(StatusRecord.AKTIF, "-"));
                                    attendance1.setDate(request.getTanggal());
                                    attendance1.setStatus(StatusRecord.AKTIF);
                                    if(selisihWaktuMasuk >= selisihWaktuKeluar) {
                                        attendance1.setWaktuKeluar(absenHitung.toLocalTime());
                                    }
                                    attendance1.setDateUpdate(LocalDateTime.now());
                                    attendance1.setUserUpdate("mesin");
                                    attendanceDao.save(attendance1);
                                }
                            }
                        }
                        //jika ybs bukan shift tiga
                    }else{
                        if(employeeScheduleDailyDtoSebelum == null) {
                            Attendance attendance = attendanceDao.findByStatusAndEmployesAndDate(StatusRecord.AKTIF, employes, request.getTanggal());
                            String shiftMasuk = employeeScheduleDailyDto.getTanggalMasuk() + " " + employeeScheduleDailyDto.getShiftMasuk() + ":00";
                            String shiftKeluar = employeeScheduleDailyDto.getTanggalKeluar() + " " + employeeScheduleDailyDto.getShiftKeluar() + ":00";
                            String absen = request.getTanggal() + " " + request.getWaktu();
                            if (request.getWaktu().toString().length() == 5) {
                                absen = request.getTanggal() + " " + request.getWaktu() + ":00";
                            }
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime absenHitung = LocalDateTime.parse(absen, formatter);

                            LocalDateTime shiftMasukHitung = LocalDateTime.parse(shiftMasuk, formatter);
                            LocalDateTime shiftKeluarHitung = LocalDateTime.parse(shiftKeluar, formatter);
                            absenHitung = absenHitung.plusHours(7);

                            long selisihWaktuMasuk = ChronoUnit.HOURS.between(shiftMasukHitung, absenHitung);
                            long selisihWaktuKeluar = ChronoUnit.HOURS.between(absenHitung, shiftKeluarHitung);

                            System.out.println("selisih masuk : " + selisihWaktuMasuk);
                            System.out.println("selisih keluar : " + selisihWaktuKeluar);
                            if (attendance != null) {
//                        System.out.println("attendance : " + attendance);
                                if (attendance.getWaktuMasuk() == null) {
                                    if (selisihWaktuMasuk <= selisihWaktuKeluar) {
                                        attendance.setWaktuMasuk(absenHitung.toLocalTime());
                                    }
                                } else {
                                    String waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().toString();
                                    if (attendance.getWaktuMasuk().toString().length() == 5) {
                                        waktuMasuk = attendance.getDate().toString() + " " + attendance.getWaktuMasuk().toString() + ":00";
                                    }
                                    LocalDateTime waktuMasukHitung = LocalDateTime.parse(waktuMasuk, formatter);
                                    if (absenHitung.isBefore(waktuMasukHitung)) {
                                        attendance.setWaktuMasuk(absenHitung.toLocalTime());
                                    }
                                }

                                if (attendance.getWaktuKeluar() == null) {
                                    if (selisihWaktuMasuk > selisihWaktuKeluar) {
                                        attendance.setWaktuKeluar(absenHitung.toLocalTime());
                                    }
                                } else {
                                    String waktuKeluar = attendance.getDate().toString() + " " + attendance.getWaktuKeluar().toString();
                                    if (attendance.getWaktuKeluar().toString().length() == 5) {
                                        waktuKeluar = attendance.getDate().toString() + " " + attendance.getWaktuKeluar().toString() + ":00";
                                    }
                                    LocalDateTime waktuKeluarHitung = LocalDateTime.parse(waktuKeluar, formatter);
                                    if (absenHitung.isAfter(waktuKeluarHitung)) {
                                        attendance.setWaktuKeluar(absenHitung.toLocalTime());
                                    }
                                }

                                attendance.setDateUpdate(LocalDateTime.now());
                                attendance.setUserUpdate("mesin");
                                attendanceDao.save(attendance);
                                return new BaseResponse(HttpStatus.OK.getReasonPhrase(),
                                        String.valueOf(HttpStatus.OK.value()));
                            } else {
                                Attendance attendance1 = new Attendance();
                                attendance1.setEmployes(employes);
                                attendance1.setAttendanceHistory(attendanceHistoryDao.findByStatusAndId(StatusRecord.AKTIF, "-"));
                                attendance1.setDate(request.getTanggal());
                                attendance1.setStatus(StatusRecord.AKTIF);
                                if (selisihWaktuMasuk <= selisihWaktuKeluar) {
                                    attendance1.setWaktuMasuk(absenHitung.toLocalTime());
                                } else {
                                    attendance1.setWaktuKeluar(absenHitung.toLocalTime());
                                }
                                attendance1.setDateUpdate(LocalDateTime.now());
                                attendance1.setUserUpdate("mesin");
                                attendanceDao.save(attendance1);
                            }
                        }else{
                            //jika di hari sebelum nya ybs shift 3
                            if (employeeScheduleDailyDtoSebelum.getShiftMasuk().isAfter(employeeScheduleDailyDtoSebelum.getShiftKeluar())) {
                                Attendance attendanceSebelum = attendanceDao.findByStatusAndEmployesAndDate(StatusRecord.AKTIF, employes, request.getTanggal().minusDays(1));
                                String shiftMasukSebelum = employeeScheduleDailyDtoSebelum.getTanggalMasuk() + " " + employeeScheduleDailyDtoSebelum.getShiftMasuk() + ":00";
                                String shiftKeluarSebelum = employeeScheduleDailyDtoSebelum.getTanggalKeluar() + " " + employeeScheduleDailyDtoSebelum.getShiftKeluar() + ":00";
                                String absen = request.getTanggal()+" "+request.getWaktu();
                                if(request.getWaktu().toString().length() == 5){
                                    absen = request.getTanggal()+" "+request.getWaktu() + ":00";
                                }
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                                LocalDateTime absenHitung = LocalDateTime.parse(absen, formatter);
                                LocalDateTime shiftMasukHitungSebelum = LocalDateTime.parse(shiftMasukSebelum, formatter);
                                LocalDateTime shiftKeluarHitungSebelum = LocalDateTime.parse(shiftKeluarSebelum, formatter);
                                absenHitung = absenHitung.plusHours(7);

                                long selisihWaktuMasuk = ChronoUnit.HOURS.between(shiftMasukHitungSebelum, absenHitung);
                                long selisihWaktuKeluar = ChronoUnit.HOURS.between(absenHitung, shiftKeluarHitungSebelum);

                                System.out.println("selisih masuk : " + selisihWaktuMasuk);
                                System.out.println("selisih keluar : " + selisihWaktuKeluar);
                                //jika sudah ada absen di hari sebelum nya
                                if(attendanceSebelum != null){
                                    //masukan absen keluar nya saja
                                    if(attendanceSebelum.getWaktuKeluar() == null) {
                                        if(selisihWaktuMasuk > selisihWaktuKeluar) {
                                            attendanceSebelum.setWaktuKeluar(absenHitung.toLocalTime());
                                        }
                                    }else{
                                        String waktuKeluar = attendanceSebelum.getDate().toString() + " " + attendanceSebelum.getWaktuKeluar().toString();
                                        if(attendanceSebelum.getWaktuKeluar().toString().length() == 5){
                                            waktuKeluar = attendanceSebelum.getDate().toString() + " " + attendanceSebelum.getWaktuKeluar().toString() + ":00";
                                        }
                                        LocalDateTime waktuKeluarHitung = LocalDateTime.parse(waktuKeluar, formatter);
                                        if (absenHitung.isAfter(waktuKeluarHitung)){
                                            attendanceSebelum.setWaktuKeluar(absenHitung.toLocalTime());
                                        }
                                    }
                                    attendanceSebelum.setDateUpdate(LocalDateTime.now());
                                    attendanceSebelum.setUserUpdate("mesin");
                                    attendanceDao.save(attendanceSebelum);
                                    return new BaseResponse(HttpStatus.OK.getReasonPhrase(),
                                            String.valueOf(HttpStatus.OK.value()));
                                    //jika belum ada absen di hari sebelum nya
                                }else{
                                    //masukan data baru dan input absen keluar nya saja
                                    Attendance attendance1 = new Attendance();
                                    attendance1.setEmployes(employes);
                                    attendance1.setAttendanceHistory(attendanceHistoryDao.findByStatusAndId(StatusRecord.AKTIF, "-"));
                                    attendance1.setDate(request.getTanggal());
                                    attendance1.setStatus(StatusRecord.AKTIF);
                                    if(selisihWaktuMasuk >= selisihWaktuKeluar) {
                                        attendance1.setWaktuKeluar(absenHitung.toLocalTime());
                                    }
                                    attendance1.setDateUpdate(LocalDateTime.now());
                                    attendance1.setUserUpdate("mesin");
                                    attendanceDao.save(attendance1);
                                }
                            }
                        }
                    }
                }
            }
        }

        return new BaseResponse(HttpStatus.FORBIDDEN.getReasonPhrase(),
                String.valueOf(HttpStatus.FORBIDDEN.value()));

    }
}
