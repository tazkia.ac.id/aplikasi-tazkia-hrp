package id.ac.tazkia.aplikasihr.controller.reports;

import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceScheduleEventDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.schedule.ScheduleEventDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceScheduleEvent;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequest;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

@Controller
public class ReportAttendanceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private AttendanceScheduleEventDao attendanceScheduleEventDao;

    @Autowired
    @Value("${upload.attendancerequest}")
    private String uploadFolder;
    @Autowired
    private TimeOffRequestDao timeOffRequestDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @GetMapping("/reports/attendance")
    public String reportAttendance(Model model,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("reports", "active");
        model.addAttribute("reportattendance", "active");

        return "reports/attendance/list";
    }

    @GetMapping("/attendance/event/report")
    public String attendanceEventReport(Model model,
                                        @RequestParam(required = false) String search,
                                        @PageableDefault(size = 10) Pageable page,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("scheduleEvent", scheduleEventDao.listScheduleEventReportWithSearch(search, page));
        }else {
            model.addAttribute("scheduleEvent", scheduleEventDao.listScheduleEventReport(page));
        }

        model.addAttribute("reports", "active");
        model.addAttribute("reportattendanceevent", "active");

        return "reports/attendance/event";
    }

    @GetMapping("/attendance/event/detail")
    public String attendanceEventDetail(Model model,
                                        @RequestParam(required = false) String search,
                                        @RequestParam(required = true) ScheduleEvent scheduleEvent,
                                        Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            System.out.println("search : " + search);
            System.out.println("Schedule Id : " + scheduleEvent.getId());
            model.addAttribute("detailAttendanceEvent", attendanceScheduleEventDao.findByStatusAndScheduleEventAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullName(StatusRecord.AKTIF, scheduleEvent, search));
        }else {
            model.addAttribute("detailAttendanceEvent", attendanceScheduleEventDao.findByStatusAndScheduleEventOrderByEmployesFullName(StatusRecord.AKTIF, scheduleEvent));
        }
        model.addAttribute("scheduleEvent", scheduleEvent);
        model.addAttribute("reports", "active");
        model.addAttribute("reportattendanceevent", "active");

        return "reports/attendance/event_detail";
    }

    @GetMapping("/attendance/timeoff/monitoring")
    public String timeOffReports(Model model,
                                 @RequestParam(required = false) String tanggal){

        if (tanggal == null){
            tanggal = LocalDate.now().toString();
            model.addAttribute("tanggal", LocalDate.now());
        }else{
            model.addAttribute("tanggal", tanggal);
        }

        model.addAttribute("listTimeOff", timeOffRequestDao.timeOffMonitoring(tanggal));

        return "reports/timeoff/list";
    }


    @GetMapping("/attendance/{attendanceScheduleEvent}/event/")
    public ResponseEntity<byte[]> tampilkanBuktiAttendance(@PathVariable AttendanceScheduleEvent attendanceScheduleEvent) throws Exception {
        String lokasiFile = uploadFolder + File.separator + attendanceScheduleEvent.getFileUpload();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (attendanceScheduleEvent.getFileUpload().toLowerCase().endsWith("jpeg") || attendanceScheduleEvent.getFileUpload().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (attendanceScheduleEvent.getFileUpload().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (attendanceScheduleEvent.getFileUpload().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
