package id.ac.tazkia.aplikasihr.controller.setting;


import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.LoanApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import id.ac.tazkia.aplikasihr.entity.setting.LoanApprovalSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class LoanApprovalSettingController {

    @Autowired
    private LoanApprovalSettingDao loanApprovalSettingDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/setting/approval/loan")
    private String listApprovalLoan(Model model,
                                    @RequestParam(required = false) String search,
                                    @PageableDefault(size = 10)Pageable page,
                                    Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listApprovalLoan", loanApprovalSettingDao.findByStatusOrderBySequence(StatusRecord.AKTIF, page));
        }else{

            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listApprovalLoan", loanApprovalSettingDao.findByStatusAndCompaniesIdInOrderBySequence(StatusRecord.AKTIF,idDepartements, page));
        }
        model.addAttribute("approval", "active");
        model.addAttribute("settingApprovalLoan", "active");
        return "setting/loan_approval/list";
    }

    @GetMapping("/setting/approval/loan/new")
    private String addApprovalLoan(Model model,
                                    Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifAndCompaniesIdIn(StatusRecord.AKTIF, "AKTIF", idDepartements));
        }

        model.addAttribute("loanApprovalSetting", new LoanApprovalSetting());

        model.addAttribute("approval", "active");
        model.addAttribute("settingApprovalLoan", "active");
        return "setting/loan_approval/form";

    }

    @GetMapping("/setting/approval/loan/edit")
    private String editApprovalLoan(Model model,
                                   @RequestParam(required = true) LoanApprovalSetting loanApprovalSetting,
                                   Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifAndCompaniesIdIn(StatusRecord.AKTIF, "AKTIF", idDepartements));
        }
        model.addAttribute("loanApprovalSetting", loanApprovalSetting);

        model.addAttribute("approval", "active");
        model.addAttribute("settingApprovalLoan", "active");
        return "setting/loan_approval/form";

    }

    @PostMapping("/setting/approval/loan/save")
    private String saveApprovalLoan(@ModelAttribute @Valid LoanApprovalSetting loanApprovalSetting,
                                    Authentication authentication,
                                    RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        loanApprovalSetting.setDateUpdate(LocalDateTime.now());
        loanApprovalSetting.setUserUpdate(user.getUsername());
        loanApprovalSetting.setStatus(StatusRecord.AKTIF);

        loanApprovalSettingDao.save(loanApprovalSetting);

        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";

    }

    @PostMapping("/setting/approval/loan/delete")
    private String deleteApprovalLoan(@RequestParam(required = true) LoanApprovalSetting loanApprovalSetting,
                                    Authentication authentication,
                                    RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        loanApprovalSetting.setStatus(StatusRecord.HAPUS);

        loanApprovalSettingDao.save(loanApprovalSetting);

        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../loan";

    }

}
