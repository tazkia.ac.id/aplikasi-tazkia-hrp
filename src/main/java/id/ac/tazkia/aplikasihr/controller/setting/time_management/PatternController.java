package id.ac.tazkia.aplikasihr.controller.setting.time_management;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternScheduleDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftPatternDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class PatternController {

    @Autowired
    private ShiftPatternDao shiftPatternDao;

    @Autowired
    private ShiftDetailDao shiftDetailDao;

    @Autowired
    private PatternDao patternDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PatternScheduleDao patternScheduleDao;

    @GetMapping("/setting/timemanagement/pattern")
    public String listShiftPattern(Model model,
                                   @RequestParam(required = false) String search,
                                   @PageableDefault(size = 10) Pageable page,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("pattern", new Pattern());
            model.addAttribute("listPattern", patternDao.findByStatusAndPatternNameContainingIgnoreCaseOrderByDateUpdate(StatusRecord.AKTIF, search, page));
        }else {
            model.addAttribute("pattern", new Pattern());
            model.addAttribute("listPattern", patternDao.findByStatusOrderByPatternName(StatusRecord.AKTIF, page));
        }

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/pattern/list";

    }

    @PostMapping("/setting/timemanagement/pattern/save")
    public String savePattern(@ModelAttribute @Valid Pattern pattern,
                              Authentication authentication,
                              RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        if (pattern.getPatternName() == null){
            attribute.addFlashAttribute("kosong", "Pattern Name tidak boleh kosong");
            return "redirect:../pattern";
        }

        if (pattern.getPatternName().equals(null)){
            attribute.addFlashAttribute("kosong", "Pattern Name tidak boleh kosong");
            return "redirect:../pattern";
        }

        if (pattern.getPatternName().equals("")){
            attribute.addFlashAttribute("kosong", "Pattern Name tidak boleh kosong");
            return "redirect:../pattern";
        }

        pattern.setId(UUID.randomUUID().toString());
        pattern.setDateUpdate(LocalDateTime.now());
        pattern.setUserUpdate(user.getUsername());
        pattern.setStatus(StatusRecord.AKTIF);

        patternDao.save(pattern);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../shift/pattern?pattern="+pattern.getId();

    }

    @PostMapping("/setting/timemanagement/pattern/delete")
    public String deletePattern(@RequestParam(required = true) Pattern pattern,
                                Authentication authentication,
                                RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        Integer countPatternSchedule = patternScheduleDao.countByStatusAndPattern(StatusRecord.AKTIF, pattern);

        if (countPatternSchedule >= 1){
            attribute.addFlashAttribute("terpakai", "Delete Data (" + pattern.getPatternName() + ") Gagal, Karena data sudah terpakai di pattern schedule");
            return "redirect:../pattern";
        }

        pattern.setStatus(StatusRecord.HAPUS);
        pattern.setUserUpdate(user.getUsername());
        pattern.setDateUpdate(LocalDateTime.now());
        patternDao.save(pattern);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pattern";

    }

}
