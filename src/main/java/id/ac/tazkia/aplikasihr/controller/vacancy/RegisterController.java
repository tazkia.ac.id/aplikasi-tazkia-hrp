package id.ac.tazkia.aplikasihr.controller.vacancy;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasihr.dao.OneTimePasswordDao;
import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantDao;
import id.ac.tazkia.aplikasihr.entity.OneTimePassword;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.recruitment.Applicant;
import id.ac.tazkia.aplikasihr.services.GmailApiService;
import id.ac.tazkia.aplikasihr.services.OneTimePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import reactor.core.publisher.Sinks;

import javax.mail.MessagingException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.*;

@Controller
public class RegisterController {

    private final Long expiryInterval = 5L * 60 * 1000;
    @Autowired
    private ApplicantDao applicantDao;

    @Autowired
    private OneTimePasswordDao oneTimePasswordDao;

    @Autowired
    private OneTimePasswordService oneTimePasswordService;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private GmailApiService gmailApiService;

    private final static Integer LENGTH = 6;
    @GetMapping("/vacancy/register")
    public String formRegister(Model model){

        Random random = new Random();
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < LENGTH; i++){
            int randomNumber = random.nextInt(10);
            otp.append(randomNumber);
        }

        model.addAttribute("testOTP", Integer.parseInt(otp.toString().trim()));

        return "register";
    }

    @PostMapping("/vacancy/register/save")
    public String saveRegister(Model model, @RequestParam String name, @RequestParam String email, RedirectAttributes attributes) throws MessagingException {

        OneTimePassword otp = new OneTimePassword();
        Applicant applicant = new Applicant();

        Applicant validasiEmail = applicantDao.findByStatusAndEmailActive(StatusRecord.AKTIF, email);

        if (validasiEmail == null){
            applicant.setEmailActive(email);
            applicant.setFullName(name);
            applicant.setDateUpdate(LocalDateTime.now());
            applicantDao.save(applicant);

            Random random = new Random();
            StringBuilder oneTimePassword = new StringBuilder();
            for (int i = 0; i < LENGTH; i++){
                int randomNumber = random.nextInt(10);
                oneTimePassword.append(randomNumber);
            }

            otp.setOneTimePasswordCode(Integer.parseInt(oneTimePassword.toString().trim()));
            otp.setEmail(email);
            otp.setExpires(new Date(System.currentTimeMillis() + expiryInterval));
            otp.setStatus(StatusRecord.AKTIF);
            oneTimePasswordDao.save(otp);

            Mustache templateEmail = mustacheFactory.compile("templates/otp_confirmation.html");
            Map<String, String> data = new HashMap<>();
            data.put("otp",otp.getOneTimePasswordCode() + "");
            data.put("expires", otp.getExpires().toString());

            StringWriter output = new StringWriter();
            templateEmail.execute(output, data);

            gmailApiService.kirimEmail(
                    "One Time Password from HRIS Tazkia",
                    otp.getEmail(),
                    "One Time Password verification",
                    output.toString()
            );

            return "redirect:../verify?otp="+otp.getId();
        }else {
            model.addAttribute("gagal", "email sudah terdaftar");
            return "register";
        }

    }

    @GetMapping("/vacancy/verify")
    public String verifyOtp(Model model, @RequestParam(required = false) OneTimePassword otp){

        model.addAttribute("otp", otp);
        model.addAttribute("email", otp.getEmail());

        return "verifyOTP";
    }

    @PostMapping("/vacancy/verify/otp")
    public String verify(Model model, @RequestParam String otp, @RequestParam String first, @RequestParam String second, @RequestParam String third,
                         @RequestParam String fourth, @RequestParam String fifth, @RequestParam String sixth){

        String code = first+second+third+fourth+fifth+sixth;

        OneTimePassword oneTimePassword = oneTimePasswordDao.findById(otp).get();

        System.out.println("Code int is : " + Integer.parseInt(code));

        if (oneTimePassword.getOneTimePasswordCode() == Integer.parseInt(code) && oneTimePassword.getStatus() == StatusRecord.AKTIF) {
            oneTimePassword.setOneTimePasswordCodeVerify(Integer.parseInt(code));
            oneTimePasswordDao.save(oneTimePassword);
            model.addAttribute("first", first);
            model.addAttribute("second", second);
            model.addAttribute("third", third);
            model.addAttribute("fourth", fourth);
            model.addAttribute("fifth", fifth);
            model.addAttribute("sixth", sixth);

            Applicant applicant = applicantDao.findByStatusAndEmailActive(StatusRecord.AKTIF, oneTimePassword.getEmail());
            Role pelamar = roleDao.findById("pelamar").get();

            User user = new User();
            String id = UUID.randomUUID().toString();
            user.setId(id);
            user.setUsername(oneTimePassword.getEmail());
            user.setActive(true);
//            user.setRole(pelamar);
            userDao.save(user);

            applicant.setStatusAktif("AKTIF");
            applicant.setUser(user);
            applicant.setUserUpdate(user.getUsername());
            applicant.setDateUpdate(LocalDateTime.now());
            applicantDao.save(applicant);

        }else if (oneTimePassword.getStatus() == StatusRecord.NONAKTIF){
            model.addAttribute("habis", "otp sudah tidak aktif");
            return verifyOtp(model, oneTimePassword);
        }else{
            model.addAttribute("gagal", "kode Otp Salah");
            return verifyOtp(model, oneTimePassword);
        }

        model.addAttribute("success", "Kode OTP berhasil.");
        return verifyOtp(model, oneTimePassword);
    }

}
