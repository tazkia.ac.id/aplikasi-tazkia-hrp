package id.ac.tazkia.aplikasihr.controller.kpi;


import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiEvidanceDao;
import id.ac.tazkia.aplikasihr.dao.kpi.PenilaianKpiDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.PositionKpiDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dto.kpi.EditDto;
import id.ac.tazkia.aplikasihr.dto.kpi.ListKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.kpi.SaveKpiEmployeeDto;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpi;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.KpiService;
import id.ac.tazkia.aplikasihr.services.ResizeFIleService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller@Slf4j
public class KpiTendikController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PengisianKpiDao pengisianKpiDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    @Value("${upload.kinerja}")
    private String uploadKinerja;

    @Autowired
    private PengisianKpiEvidanceDao pengisianKpiEvidanceDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;
    @Autowired
    private PositionKpiDao positionKpiDao;

    @Autowired
    private KpiService kpiService;

    @Autowired
    private PenilaianKpiDao penilaianKpiDao;
    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private ResizeFIleService resizeFIleService;

    @GetMapping("/kpi")
    public String kpiTendik(Model model,
                            @PageableDefault(size = 10) Pageable pageable,
                            @RequestParam(required = false) String bulan,
                            @RequestParam(required = false) String tahun,
                            Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        String adaBulan = bulan;
        String adaTahun = tahun;
        String bulands = bulan;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();


            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            bulan = monthString;
            tahun = tahunString;

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);
        }


        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1) {
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1) {
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));


        if (employes != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }


            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
            System.out.println("bulankpi : " + bulands);
            System.out.println("tahunKpi : " + tahun);
            Page<ListKpiEmployeeDto> data = kpiService.getKpiListEmployee(employes, bulan, tahun, pageable);
            model.addAttribute("listPengisianKpi", data);
        }

        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpitendik", "active");
        model.addAttribute("menukpistaff", "active");
        return "kpi/tendik/list";

    }

    @GetMapping("/kpi/edit")
    public String getEdit(@RequestParam PengisianKpi id,
                          Model model,
                          Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("pengisianKpi", id);
        model.addAttribute("listEvidance", pengisianKpiEvidanceDao.findByPengisianKpiAndStatus(id, StatusRecord.AKTIF));
        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpitendik", "active");
        model.addAttribute("menukpistaff", "active");
        return "kpi/tendik/edit";
    }


    @GetMapping("/kpi/add")
    public String kpiTendikAdd(Model model,
                            Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        List<String> idJobPosition = employeeJobPositionDao.cariIdEmployeeJobPosition(employes.getId());
        if(idJobPosition != null) {

            if(LocalDate.now().getMonthValue() == 8){
                model.addAttribute("listKpi", positionKpiDao.findByStatusAndJobPositionIdInOrderByKpi(StatusRecord.AKTIF,idJobPosition));
            }else if (LocalDate.now().getMonthValue() == 2){
                model.addAttribute("listKpi", positionKpiDao.findByStatusAndJobPositionIdInAndPeriodeEvaluasiInOrderByKpi(
                        StatusRecord.AKTIF,
                        idJobPosition,
                        Arrays.asList(PeriodeEvaluasiKpi.HARIAN, PeriodeEvaluasiKpi.BULANAN, PeriodeEvaluasiKpi.SEMESTERAN)
                ));
            }else{
                model.addAttribute("listKpi", positionKpiDao.findByStatusAndJobPositionIdInAndPeriodeEvaluasiInOrderByKpi(
                        StatusRecord.AKTIF,
                        idJobPosition,
                        Arrays.asList(PeriodeEvaluasiKpi.HARIAN, PeriodeEvaluasiKpi.BULANAN)
                ));
            }

            model.addAttribute("pengisianKpi", new PengisianKpi());
            model.addAttribute("menukpi", "active");
            model.addAttribute("menukpitendik", "active");
            model.addAttribute("menukpistaff", "active");

            return "kpi/tendik/form";
        }else{
            model.addAttribute("error", "Mohon maaf, anda tidak memiliki KPI karena Job Position anda telah berakhir, untuk informasi lebih lanjut silahkan hubungi HRD ");
            model.addAttribute("menukpi", "active");
            model.addAttribute("menukpitendik", "active");
            model.addAttribute("menukpistaff", "active");
            return "kpi/tendik/error";
        }
    }

    @PostMapping("/kpi/editevidance")
    public String editEvidance(@Valid EditDto editDto, RedirectAttributes redirectAttributes,
                               String[] idEvidance, @RequestParam(required = false, name = "file") MultipartFile[] file){
        try {
            editDto.setIdEvidance(idEvidance);
            editDto.setFiles(file);
            kpiService.editKpiEmployee(editDto);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(e.getLocalizedMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi";
    }

    @PostMapping("/kpi/add_evidance")
    public String bidangPendidikanTambahFileKinerja(@RequestParam String dataPengisianKpi,
                                                    @RequestParam("evidance") MultipartFile file,
                                                    Authentication authentication,
                                                    RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        PengisianKpi pengisianKpi = pengisianKpiDao.findById(dataPengisianKpi).get();
        PengisianKpiEvidance pengisianKpiEvidance = new PengisianKpiEvidance();
        pengisianKpiEvidance.setPengisianKpi(pengisianKpi);
        pengisianKpiEvidance.setNamaFile(file.getOriginalFilename());
        String namaAsli = file.getOriginalFilename();
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile1 = UUID.randomUUID().toString();
        pengisianKpiEvidance.setFile(idFile1 + "." + extension);
        new File(uploadKinerja).mkdirs();
        File tujuan2 = new File(uploadKinerja + File.separator + idFile1 + "." + extension);
        file.transferTo(tujuan2);

        if (file.getContentType().equalsIgnoreCase("application/pdf")) {
            resizeFIleService.resizeFilePdf(tujuan2);
        } else {
            // Resize file image jika bukan PDF
            resizeFIleService.resizeFileImage(tujuan2);
        }

//        pengisianKpiEvidance.setExtensi(extension);
        pengisianKpiEvidance.setStatus(StatusRecord.AKTIF);
        pengisianKpiEvidanceDao.save(pengisianKpiEvidance);

        redirectAttributes.addFlashAttribute("success", "Dokumen kinerja berhasil di tambahkan");
        return"redirect:../kpi";
    }

    @PostMapping("/kpi/save")
    public String saveKpi(@Valid SaveKpiEmployeeDto saveKpiEmployeeDto, @RequestParam("file") MultipartFile[] file,
                          Authentication authentication, RedirectAttributes redirectAttributes){
        try{
            saveKpiEmployeeDto.setFiles(file);
            kpiService.savePengisianKpi(authentication, saveKpiEmployeeDto);
            redirectAttributes.addFlashAttribute("success", "Berhasil disimpan");
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(e.getLocalizedMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi";
    }

    @GetMapping("/kpi/delete")
    public String deleteKpi(@RequestParam String id, RedirectAttributes redirectAttributes){
        try {
            kpiService.deleteListEmployeeKpi(id);
            redirectAttributes.addFlashAttribute("success", "Berhasil dihapus");
        }catch (Exception e){
            log.error(e.getMessage());
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/kpi";
    }

    @GetMapping("/kpi/evidance")
    public ResponseEntity<byte[]> getFileEvidance(@RequestParam PengisianKpiEvidance id) throws Exception{
        return kpiService.getFileEvidence(id);
    }

    @GetMapping("/kpi/result")
    public String kpiResult(Authentication authentication, Model model,
                            @RequestParam(required = false) String bulan, @RequestParam(required = false) String tahun) {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        String modelBulan = "";
        String modelTahun = "";
        List<String> idJobPosition = employeeJobPositionDao.cariIdEmployeeJobPosition(employes.getId());

        Set<PenilaianKpi> penilaianKpiSet = new LinkedHashSet<>();

        for (String id : idJobPosition) {
            JobPosition jobPosition = jobPositionDao.findByStatusAndId(StatusRecord.AKTIF, id);

            if (jobPosition != null) {
                List<PositionKpi> positionKpiList = positionKpiDao.findByStatusAndJobPosition(StatusRecord.AKTIF, jobPosition);
                for (PositionKpi dataKpi : positionKpiList) {
                    if (bulan == null && tahun == null){
                        String bln = String.valueOf(LocalDate.now().getMonthValue());
                        String thn = String.valueOf(LocalDate.now().getYear());
                        PenilaianKpi penilaianKpi = penilaianKpiDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(
                                StatusRecord.AKTIF, employes, bln, thn, dataKpi
                        );

                        if (penilaianKpi != null) {
                            penilaianKpiSet.add(penilaianKpi);
                        }

                        modelBulan = bln;
                        modelTahun = thn;
                    }else {
                        PenilaianKpi penilaianKpi = penilaianKpiDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(
                                StatusRecord.AKTIF, employes, bulan, tahun, dataKpi
                        );

                        if (penilaianKpi != null) {
                            penilaianKpiSet.add(penilaianKpi);
                        }

                        modelBulan = bulan;
                        modelTahun = tahun;
                    }
                }
            }
        }

        List<PenilaianKpi> penilaianKpiList = new ArrayList<>(penilaianKpiSet);
        model.addAttribute("listPenilaianKpi", penilaianKpiList);
        model.addAttribute("listBulan", bulanDao.findByStatusOrderById(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("modelBulan", modelBulan);
        model.addAttribute("modelTahun", modelTahun);
        model.addAttribute("menukpiresult", "active");

        return "kpi/tendik/result";
    }

}
