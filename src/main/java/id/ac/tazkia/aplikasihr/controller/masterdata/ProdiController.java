package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.DepartmentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.FakultasDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.ProdiDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusClass;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Fakultas;
import id.ac.tazkia.aplikasihr.entity.masterdata.Prodi;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class ProdiController {

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private FakultasDao faultasDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @GetMapping("/masterdata/prodi")
    public String prodi(Model model,
                        @PageableDefault(size = 10) Pageable page,
                        @RequestParam(required = false)String search,
                        @RequestParam(required = false)Integer size,
                        Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);


        List<String> idCompanies = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listProdi",prodiDao.findByStatusAndFakultasCompaniesIdInAndNamaProdiContainingIgnoreCaseOrStatusAndFakultasCompaniesIdInAndNamaProdiEnglishContainingIgnoreCaseOrderByNamaProdiAsc(StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,page));
        }else{
            model.addAttribute("listProdi",prodiDao.findByStatusAndFakultasCompaniesIdInOrderByNamaProdiAsc(StatusRecord.AKTIF,idCompanies,page));
        }

        model.addAttribute("listFakultas", faultasDao.findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies));
        model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentClassJenis(StatusRecord.AKTIF, StatusClass.PRODI));
        model.addAttribute("prodi", new Prodi());
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("menuProdi", "active");

        return "masterdata/prodi/list";
    }


    @PostMapping("/masterdata/prodi/delete")
    public String deleteProdi(@RequestParam(required = false) Integer size,
                                   @RequestParam(required = false) String search,
                                   @RequestParam(required = false) Prodi prodi,
                                   @RequestParam(required = false) Integer page,
                                   Authentication authentication,
                                   RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        prodi.setStatus(StatusRecord.HAPUS);
        prodi.setUserUpdate(user.getUsername());
        prodi.setDateUpdate(LocalDateTime.now());

        prodiDao.save(prodi);
        attribute.addFlashAttribute("berhasil", "Delete Data Success");
        return "redirect:../prodi?search=" + search + "&page=" + page + "&size=" + size;
    }


    @GetMapping("/masterdata/prodi/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getProdiData(@PathVariable("id") String id) {
        try {
            Prodi prodi = prodiDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid Fakultas ID: " + id));
            return ResponseEntity.ok(prodi);  // Jika ditemukan, return 200 OK dengan data Department
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }

    @PostMapping("/masterdata/prodi/save")
    public String saveFakultas(@ModelAttribute @Valid Prodi prodi,
                               BindingResult bindingResult,
                               Model model,
                               @RequestParam(required = false) String search,
                               @RequestParam(required = false) Integer size,
                               Pageable page,
                               RedirectAttributes redirectAttributes,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        if (size == null){
            size=10;
        }
        if (bindingResult.hasErrors()) {

            model.addAttribute("employess", employess);
            List<String> idCompanies = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listProdi",prodiDao.findByStatusAndFakultasCompaniesIdInAndNamaProdiContainingIgnoreCaseOrStatusAndFakultasCompaniesIdInAndNamaProdiEnglishContainingIgnoreCaseOrderByNamaProdiAsc(StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,page));
            }else{
                model.addAttribute("listProdi",prodiDao.findByStatusAndFakultasCompaniesIdInOrderByNamaProdiAsc(StatusRecord.AKTIF,idCompanies,page));
            }
            model.addAttribute("listFakultas", faultasDao.findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies));
            model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentClassJenis(StatusRecord.AKTIF, StatusClass.PRODI));
            model.addAttribute("prodi", prodi);
            model.addAttribute("page", page);
            model.addAttribute("size", size);
            model.addAttribute("menuProdi", "active");
            return "masterdata/prodi/list";
        }
        prodi.setUserUpdate(user.getUsername());
        prodi.setDateUpdate(LocalDateTime.now());
        prodiDao.save(prodi);
        redirectAttributes.addFlashAttribute("berhasil", "Save Data Success");
        return "redirect:../prodi?search=" + search + "&page=" + page.getPageNumber() + "&size=" + size;
    }

}
