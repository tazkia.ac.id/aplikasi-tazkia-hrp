package id.ac.tazkia.aplikasihr.controller.setting.time_management;

import id.ac.tazkia.aplikasihr.dao.DaysDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftPatternDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftDetail;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftPattern;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class ShitPatternController {

    @Autowired
    private PatternDao patternDao;

    @Autowired
    private ShiftPatternDao shiftPatternDao;

    @Autowired
    private ShiftDetailDao shiftDetailDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private DaysDao daysDao;

    @Autowired
    private EmployesDao employesDao;


    @GetMapping("setting/timemanagement/shift/pattern")
    public String listShiftDetail(Model model,
                                  @RequestParam(required = true) Pattern pattern,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listDays", daysDao.findByStatusOrderByNumberDay(StatusRecord.AKTIF));
        model.addAttribute("listShiftDetail", shiftDetailDao.findByStatusOrderByShiftName(StatusRecord.AKTIF));
        model.addAttribute("listShiftPattern", shiftPatternDao.findByStatusAndPatternOrderByDaysNumberDay(StatusRecord.AKTIF, pattern));
        model.addAttribute("shiftDetail", new ShiftDetail());
        model.addAttribute("pattern", pattern);
        model.addAttribute("shiftPattern", new ShiftPattern());

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/shift_pattern/list";

    }

    @PostMapping("/setting/timemanagement/shift/pattern/submit")
    public String saveShiftPattern(@RequestParam(required = true) Pattern pattern,
                                   @ModelAttribute @Valid ShiftPattern shiftPattern,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);


        shiftPattern.setStatus(StatusRecord.AKTIF);
        shiftPattern.setUserUpdate(user.getUsername());
        shiftPattern.setDateUpdate(LocalDateTime.now());

        shiftPatternDao.save(shiftPattern);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pattern?pattern="+pattern.getId();

    }


    @PostMapping("/setting/timemanagement/shift/pattern/delete")
    public String deleteShiftPattern(@RequestParam(required = true)ShiftPattern shiftPattern,
                                     Authentication authentication,
                                     RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        shiftPattern.setStatus(StatusRecord.HAPUS);
        shiftPattern.setUserUpdate(user.getUsername());
        shiftPattern.setDateUpdate(LocalDateTime.now());
        shiftPatternDao.save(shiftPattern);

//        List<ShiftPattern> shiftPatterns = shiftPatternDao.findByStatusAndNumberGreaterThanOrderByNumber(StatusRecord.AKTIF, shiftPattern.getNumber());
//        for (ShiftPattern sp : shiftPatterns){
//
////            sp.setNumber(sp.getNumber()-1);
//            sp.setUserUpdate(user.getUsername());
//            sp.setDateUpdate(LocalDateTime.now());
//            shiftPatternDao.save(sp);
//
//        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pattern?pattern="+shiftPattern.getPattern().getId();
    }
}
