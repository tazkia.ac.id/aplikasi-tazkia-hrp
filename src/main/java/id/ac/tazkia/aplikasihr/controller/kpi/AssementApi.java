package id.ac.tazkia.aplikasihr.controller.kpi;

import id.ac.tazkia.aplikasihr.dao.kpi.PengisianKpiDosenRepository;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class AssementApi {

    @Autowired
    private PengisianKpiDosenRepository pengisianKpiDosenRepository;

    @GetMapping("/kpi_assesment/process/detail")
    public List<PengisianKpiDosen> kpiAssesmentProcessDetail(
            @RequestParam String idPositionKpi,
            @RequestParam String idEmployee) {

        String bulan = LocalDate.now().getMonth().toString();
        String tahun = String.valueOf(LocalDate.now().getYear());

        return pengisianKpiDosenRepository.findByPositionKpiAndEmployeeAndBulanAndTahun(
                idPositionKpi, idEmployee, bulan, tahun);
    }

}
