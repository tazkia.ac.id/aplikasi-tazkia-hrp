package id.ac.tazkia.aplikasihr.controller.attendance;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class AttendanceReportsController {

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/attendance/reports")
    public String runPayroll(Model model,
                             @RequestParam(required = false) String companies,
                             @RequestParam(required = false) String bulan,
                             @RequestParam(required = false) String tahun,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            bulan = monthString;
            tahun = tahunString;

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);
        }

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }


        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));


        if (companies != null && bulan != null && tahun != null) {
            model.addAttribute("companiesSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));

            if (companies.equals("All")){
                model.addAttribute("listAttendanceRun", attendanceRunDao.listAttendanceRunAll(tahun, bulan));
            }else{
                model.addAttribute("listAttendanceRun", attendanceRunDao.listAttendanceRun(companies,tahun, bulan));
            }
        }

        model.addAttribute("attendanceRun", new AttendanceRun());

        return "attendance/report";

    }

    @PostMapping("/attendance/report/update")
    public String updateAttendance(@RequestParam(required = false) String companies,
                                   @RequestParam(required = false) String bulan,
                                   @RequestParam(required = false) String tahun,
                                   @RequestParam(required = false) AttendanceRun attendanceRun,
                                   @RequestParam(required = false) Integer totalAttendance,
                                   @RequestParam(required = false) Integer totalAbsen,
                                   @RequestParam(required = false) Integer totalLate,
                                   RedirectAttributes redirectAttributes,
                                   Authentication authentication){

        System.out.println("isi :" + attendanceRun);
        System.out.println("Attendance : "+ totalAttendance);
        System.out.println("Abstain : "+ totalAbsen);
        System.out.println("Late : "+ totalLate);

        attendanceRun.setTotalAttendance(totalAttendance);
        attendanceRun.setTotalAbsen(totalAbsen);
        attendanceRun.setTotalLate(totalLate);
        attendanceRunDao.save(attendanceRun);

        return "redirect:../reports?bulan="+bulan+"&tahun="+tahun+"&companies="+companies;
    }

//    @GetMapping("/attendance/report/edit")
//    public String editAttendance(Model model,
//                                 @RequestParam(required = false) String bulan,
//                                 @RequestParam(required = false) String tahun,
//                                 @RequestParam(required = false) String employes,
//                                 @RequestParam(required = false) AttendanceRun attendanceRun,
//                                 Authentication authentication) {
//
//        User user = currentUserService.currentUser(authentication);
//        Employes employess = employesDao.findByUser(user);
//        model.addAttribute("employess", employess);
//        model.addAttribute("bulan", bulan);
//        model.addAttribute("tahun", tahun);
////        model.addAttribute("employes", employes);
//
//
//        return "attendance/edit";
//
//    }


    @GetMapping("/attendance/detail")
    public String myAttendance(Model model,
                               @RequestParam(required = false) String bulan,
                               @RequestParam(required = false) String tahun,
                               @RequestParam(required = false) String employes,
                               Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            bulan = monthString;
            tahun = tahunString;

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);
        }



        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1) {
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1) {
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }
//        model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, monthString));
//        model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahunString));

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listEmployes", employesDao.findByStatusOrderByNumber(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listEmployes", employesDao.findByStatusAndCompaniesIdInOrderByNumber(StatusRecord.AKTIF, idDepartements));
        }


        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));


        if (employes != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            model.addAttribute("employesSelected", employesDao.findByStatusAndId(StatusRecord.AKTIF, employes));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));


            model.addAttribute("listAttendanceEmployee", attendanceDao.listViewAttendance(localDate, localDate2, employes));



        }
        return "attendance/detail";

    }

}
