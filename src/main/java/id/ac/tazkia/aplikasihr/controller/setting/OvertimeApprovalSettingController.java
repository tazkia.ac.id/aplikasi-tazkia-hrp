package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.OvertimeApprovalSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.OvertimeApprovalSetting;
import id.ac.tazkia.aplikasihr.entity.setting.TimeOffApprovalSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class OvertimeApprovalSettingController {

    @Autowired
    private JobPositionDao JobPositionDao;

    @Autowired
    private OvertimeApprovalSettingDao overtimeApprovalSettingDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/overtime_approval")
    public String listTimeOffApproval(Model model,
                                      @RequestParam(required = false) String search,
                                      @PageableDefault(size = 10) Pageable page,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("listPosition", JobPositionDao.findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
            model.addAttribute("listOvertimeApprovalSetting", overtimeApprovalSettingDao.findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search));
        }else{
            model.addAttribute("listPosition", JobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF, page));
            model.addAttribute("listOvertimeApprovalSetting", overtimeApprovalSettingDao.findByStatusOrderBySequence(StatusRecord.AKTIF));
        }

        return "setting/overtime_approval/list";

    }

    @GetMapping("/setting/overtime_approval/new")
    public String newTimeOffApproval(Model model,
                                     @RequestParam(required = true) JobPosition position,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Integer number = overtimeApprovalSettingDao.cariNumberOvertimeApproval(position.getId());
        Integer number1 = 1;

        if(number == null){
            number1 = 1 ;
        }else{
            number1 = number + 1;
        }

        model.addAttribute("position", position);
        model.addAttribute("number", number1);
        model.addAttribute("listPosition", JobPositionDao.findByStatusAndIdNotContainingOrderByPositionCode(StatusRecord.AKTIF, position.getId()));
        model.addAttribute("overtimeApprovalSetting", new TimeOffApprovalSetting());

        return "setting/overtime_approval/form";

    }

    @PostMapping("/setting/overtime_approval/save")
    public String saveTimeOffApproval(@ModelAttribute @Valid OvertimeApprovalSetting overtimeApprovalSetting,
                                      @RequestParam(required = true) JobPosition position,
                                      Authentication authentication,
                                      RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        OvertimeApprovalSetting overtimeApprovalSetting1 = overtimeApprovalSettingDao.findByStatusAndPositionAndPositionApprove(StatusRecord.AKTIF, position, overtimeApprovalSetting.getPositionApprove());

        if(overtimeApprovalSetting1 == null) {
            Integer number = overtimeApprovalSettingDao.cariNumberOvertimeApproval(position.getId());
            Integer number1 = 1;

            if (number == null) {
                number1 = 1;
            } else {
                number1 = number + 1;
            }

            overtimeApprovalSetting.setSequence(number1);
            overtimeApprovalSetting.setPosition(position);
            overtimeApprovalSetting.setStatus(StatusRecord.AKTIF);
            overtimeApprovalSetting.setDateUpdate(LocalDateTime.now());
            overtimeApprovalSetting.setUserUpdate(user.getUsername());

            overtimeApprovalSettingDao.save(overtimeApprovalSetting);
            attribute.addFlashAttribute("success", "Save Data Success");
            return "redirect:../overtime_approval";
        }else{

            attribute.addFlashAttribute("already", "Save Data Failed");
            return "redirect:../overtime_approval/new?position="+ position.getId();

        }

    }

}
