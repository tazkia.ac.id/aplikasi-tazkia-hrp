package id.ac.tazkia.aplikasihr.controller;


import java.time.LocalDate;
import java.util.List;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import id.ac.tazkia.aplikasihr.dao.AnnouncementDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesLuarDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantRecruitmentRequestDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantScheduleDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.schedule.ScheduleEventDao;
import id.ac.tazkia.aplikasihr.dao.setting.timeoff.TimeOffJenisDao;
import id.ac.tazkia.aplikasihr.dto.StatusEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.payroll.TotalPayrollPerTahunDto;
import id.ac.tazkia.aplikasihr.entity.Announcement;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;

@Controller
public class DashboardController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private TimeOffJenisDao timeOffJenisDao;

    @Autowired
    private AnnouncementDao announcementDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private EmployesLuarDao employesLuarDao;

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private TimeOffRequestDao timeOffRequestDao;

    @Autowired
    private ApplicantDao applicantDao;

    @Autowired
    private ApplicantScheduleDao scheduleDao;

    @Autowired
    private ApplicantRecruitmentRequestDao applicantRequestDao;

    @Autowired
    private RecruitmentRequestDao requestDao;

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/dashboard")
    public String dashboard(Model model,
                            @PageableDefault(size = 1) Pageable page,
                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        model.addAttribute("scheduleEvent", scheduleEventDao.listScheduleEvent(employesDao.findByUser(user).getId()));

        Integer jmlSchedule = scheduleEventDao.scheduleEvent(employesDao.findByUser(user).getId());
        System.out.println("schedule : " + jmlSchedule);
        model.addAttribute("jmlSchedule", jmlSchedule);

 
        StatusEmployeeDto statusEmployeeDto = employeeStatusAktifDao.cariStatusTimeOff(employesDao.findByUser(user).getId());

        model.addAttribute("statusTimeOff", statusEmployeeDto);

        System.out.println("status OK = " + statusEmployeeDto);

        String admin = null;
        String hrd = null;
        String vendor = null;
        String pelamar = null;

        List<UserRole> roles = userRoleDao.findByUser(user);
        for (UserRole role : roles) {
            if(role.getRole().getName().equals("SUPERADMIN")){
                admin = role.getRole().getName();
            }
            if(role.getRole().getName().equals("HRD")){
                hrd = role.getRole().getName();
            }
            if(role.getRole().getName().equals("VENDOR")){
                vendor = role.getRole().getName();
            }
            if(role.getRole().getName().equals("PELAMAR")){
                pelamar = role.getRole().getName();
            }
        }

        if (admin != null){
            Employes employes = employesDao.findByUser(user);
//            model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dasboardTimeOff(employes.getId()));
//
            if(LocalDate.now().getMonth().getValue() <= 6){
                if(statusEmployeeDto.getStatusdua().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSebelumBulanTujuh(employes.getId()));
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                    }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
                }
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
            }

            model.addAttribute("employess", employes);
            model.addAttribute("announcement", new Announcement());
            model.addAttribute("listAnnouncement", announcementDao.pagePengumumanAdmin(page));
            model.addAttribute("jumlahEmployes", employesDao.countByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));
            model.addAttribute("jumlahEmployesPria", employesDao.countByStatusAndStatusAktifAndGender(StatusRecord.AKTIF, "AKTIF", "PRIA"));
            model.addAttribute("jumlahEmployesWanita", employesDao.countByStatusAndStatusAktifAndGender(StatusRecord.AKTIF, "AKTIF", "WANITA"));
            model.addAttribute("toalSalaryPerTahun", payrollRunDao.totalSalaryPerTahun());
            model.addAttribute("timeOffHariIni", timeOffRequestDao.timeOffHariIni());
            model.addAttribute("employeeJobPositionExpired", employeeJobPositionDao.employeeJobPositionExpired());
            model.addAttribute("home","active");
            model.addAttribute("listExpiredKontrak", employesDao.listExpiredKontrak());
            return "dashboard/admin1";

        }else if (hrd != null){
            Employes employes = employesDao.findByUser(user);
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
//            model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dasboardTimeOff(employes.getId()));
//
           if(LocalDate.now().getMonth().getValue() <= 6){
                if(statusEmployeeDto.getStatusdua().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSebelumBulanTujuh(employes.getId()));
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                    }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
                }
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
            }

            model.addAttribute("employess", employes);
            model.addAttribute("announcement", new Announcement());
            model.addAttribute("listAnnouncement", announcementDao.pagePengumumanAdminHrd(idDepartements, page));
            model.addAttribute("jumlahEmployes", employesDao.countByStatusAndStatusAktifAndCompaniesIdIn(StatusRecord.AKTIF, "AKTIF", idDepartements));
            model.addAttribute("jumlahEmployesPria", employesDao.countByStatusAndStatusAktifAndGenderAndCompaniesIdIn(StatusRecord.AKTIF, "AKTIF", "PRIA", idDepartements));
            model.addAttribute("jumlahEmployesWanita", employesDao.countByStatusAndStatusAktifAndGenderAndCompaniesIdIn(StatusRecord.AKTIF, "AKTIF", "WANITA",idDepartements));
            model.addAttribute("toalSalaryPerTahun", payrollRunDao.totalSalaryPerTahunCompany(idDepartements));
            model.addAttribute("timeOffHariIni", timeOffRequestDao.timeOffHariIniCompanies(idDepartements));
            model.addAttribute("employeeJobPositionExpired", employeeJobPositionDao.employeeJobPositionExpired());
            model.addAttribute("home","active");
            model.addAttribute("listExpiredKontrak", employesDao.listExpiredKontrakCompanies(idDepartements));
            return "dashboard/admin1";

        }else if (vendor != null){
            model.addAttribute("employess", employesLuarDao.findByStatusAndUser(StatusRecord.AKTIF, user));
            return "dashboard/vendor";
        }else if (pelamar != null){
//            Applicant applicant = applicantDao.findByUser(user);
//            List<ApplicantRecruitmentRequest> listRequest = applicantRequestDao.findByApplicantAndStatus(applicant, StatusRecord.AKTIF);
//            model.addAttribute("list", listRequest);
//            model.addAttribute("employess", applicant);
//            model.addAttribute("announcmentApplicant", scheduleDao.listAnnouncmentApplicant(applicant.getId(), page));
            return "redirect:/vacancy/jobs_list";
        }else{
            Employes employes = employesDao.findByUser(user);
//            model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dasboardTimeOff(employes.getId()));

           if(LocalDate.now().getMonth().getValue() <= 6){
                if(statusEmployeeDto.getStatusdua().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSebelumBulanTujuh(employes.getId()));
                }else{
                    if(statusEmployeeDto.getStatus().equals("OK")){
                        model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                    }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
                }
            }else{
                if(statusEmployeeDto.getStatus().equals("OK")){
                    model.addAttribute("dashboardTimeOffJenis", timeOffJenisDao.dashboardTimeOffSetelahBulanTujuh(employes.getId()));
                }else{
                        model.addAttribute("dashboardTimeOffJenis", "Tanggal masuk anda belum diupdate oleh HRD");
                    }
            }


            model.addAttribute("employess", employes);
            model.addAttribute("listAnnouncement", announcementDao.pagePengumuman(employes.getCompanies().getId(), page));
            model.addAttribute("timeOffHariIni", timeOffRequestDao.timeOffHariIni());
            model.addAttribute("announcement", new Announcement());
            model.addAttribute("home","active");
            return "dashboard/staff2";
        }

    }


    @GetMapping("/api/total/payroll")
    @ResponseBody
    public List<TotalPayrollPerTahunDto> cariTotalPayrollBulanan(@RequestParam(required = false) String program){

        List<TotalPayrollPerTahunDto> totalPayrollPerTahunDtos = payrollRunDao.dashboardTotalPayroll();

        return totalPayrollPerTahunDtos;
    }

    @RequestMapping(value = "/starter", method = RequestMethod.GET)
    public String starter(Model model) {
        model.addAttribute("loginError", true);

//        model.addAttribute("listLowongan", requestDao.listLowongan());

        return "login2";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        return "login2";
    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/dashboard";
    }

    @GetMapping("/404")
    public void form404(){
    }

    @GetMapping("/masterdata/dashboard")
    public String masterDataDashboard(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("setting", "active");
        return "masterdata/dashboard";
    }

}
