package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.setting.ApprovalRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.setting.ApprovalRequest;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.JoinColumn;
import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class ApprovalRequestController {

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private ApprovalRequestDao approvalRequestDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private RecruitmentRequestApprovalDao recruitmentRequestApprovalDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/setting/approval_request")
    public String settingRecruitmentApproval(Model model, @RequestParam(required = false) String search, @PageableDefault(size = 10)Pageable page,
                                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
                model.addAttribute("listApprovalRequest", approvalRequestDao.findByStatusAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search));
            } else {
                model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF, page));
                model.addAttribute("listApprovalRequest", approvalRequestDao.findByStatusOrderBySequence(StatusRecord.AKTIF));
            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInAndPositionCodeContainingIgnoreCaseOrStatusAndPositionNameContainingIgnoreCaseOrderByPositionCode(StatusRecord.AKTIF,idDepartements, search, StatusRecord.AKTIF, search, page));
                model.addAttribute("listApprovalRequest", approvalRequestDao.findByStatusAndPositionApproveDepartmentCompaniesIdInAndPositionPositionCodeContainingIgnoreCaseOrStatusAndPositionPositionNameContainingIgnoreCaseOrderBySequence(StatusRecord.AKTIF, idDepartements, search, StatusRecord.AKTIF, search));
            } else {
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord.AKTIF,idDepartements, page));
                model.addAttribute("listApprovalRequest", approvalRequestDao.findByStatusAndPositionApproveDepartmentCompaniesIdInOrderBySequence(StatusRecord.AKTIF, idDepartements));
            }
        }

        model.addAttribute("menurecruitmentapproval", "active");
        model.addAttribute("setting", "active");
        return "setting/approval_request/list";

    }

    @GetMapping("/setting/approval_request/new")
    public String newRecruitmentApproval(Model model, @RequestParam JobPosition position, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        Integer number1 = approvalRequestDao.cariNumberApprovalRequest(position.getId());
        Integer number2 = 1;
        if (number1 == 0) {
            number2 = 1;
        }else{
            number2 = number1 + 1;
        }

        model.addAttribute("position", position);
        model.addAttribute("number", number2);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotOrderByPositionName(StatusRecord.AKTIF, position.getId()));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInAndIdNotOrderByPositionName(StatusRecord.AKTIF, idDepartements, position.getId()));
        }


        model.addAttribute("approvalRequest", new ApprovalRequest());

        model.addAttribute("menurecruitmentapproval", "active");
        model.addAttribute("setting", "active");

        return "/setting/approval_request/form";

    }

    @GetMapping("/setting/approval_request/edit")
    public String editApprovalRequest(Model model, Authentication authentication, @RequestParam ApprovalRequest approvalRequest){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("position", approvalRequest.getPosition());
        model.addAttribute("number", approvalRequest.getSequence());
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndIdNotOrderByPositionName(StatusRecord.AKTIF, approvalRequest.getPosition().getId()));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInAndIdNotOrderByPositionName(StatusRecord.AKTIF, idDepartements, approvalRequest.getPosition().getId()));
        }
        model.addAttribute("approvalRequest", approvalRequest);

        model.addAttribute("menurecruitmentapproval", "active");
        model.addAttribute("setting", "active");

        return "/setting/approval_request/form";

    }

    @PostMapping("/setting/approval_request/save")
    public String saveRecruitmentApproval(@ModelAttribute @Valid ApprovalRequest approvalRequest,
                                          @RequestParam Integer number,
                                          @RequestParam JobPosition position, Authentication authentication, RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);

        ApprovalRequest cekData = approvalRequestDao.findByStatusAndPositionAndPositionApprove(StatusRecord.AKTIF, position, approvalRequest.getPositionApprove());

        if (cekData == null) {
            approvalRequest.setSequence(number);
            approvalRequest.setPosition(position);
            approvalRequest.setStatus(StatusRecord.AKTIF);
            approvalRequest.setUserUpdate(user.getUsername());
            approvalRequest.setDateUpdate(LocalDateTime.now());

            approvalRequestDao.save(approvalRequest);
            attributes.addFlashAttribute("success", "Save Data Success");
            return "redirect:../approval_request";
        }else{
            attributes.addFlashAttribute("already", "Save Data Failed");
            return "redirect:../approval_request/new?position="+position.getId();
        }

    }

    @PostMapping("/setting/approval_request/delete")
    public String deleteApproval(@ModelAttribute @Valid ApprovalRequest approvalRequest,
                          Authentication authentication, RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        JobPosition position = approvalRequest.getPosition();

        Integer approvalAktif = recruitmentRequestApprovalDao.countByStatusAndApprovalRequestAndStatusApprove(StatusRecord.AKTIF, approvalRequest, StatusRecord.WAITING);

        if (approvalAktif > 0) {
            attributes.addFlashAttribute("aktif","Save Data Gagal");
            return "redirect:../approval_request";
        }else{

            List<ApprovalRequest> approvalRequests = approvalRequestDao.findByStatusAndPositionAndSequenceGreaterThanOrderBySequence(StatusRecord.AKTIF, position, approvalRequest.getSequence());

            approvalRequest.setDateUpdate(LocalDateTime.now());
            approvalRequest.setUserUpdate(user.getUsername());
            approvalRequest.setStatus(StatusRecord.HAPUS);

            for (ApprovalRequest ar : approvalRequests){
                ar.setSequence(ar.getSequence() - 1);
                approvalRequestDao.save(ar);
            }

            approvalRequestDao.save(approvalRequest);
            attributes.addFlashAttribute("success", "Save data success");
            return "redirect:../approval_request";

        }

    }


}
