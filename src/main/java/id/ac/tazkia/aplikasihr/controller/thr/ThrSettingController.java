package id.ac.tazkia.aplikasihr.controller.thr;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrSettingDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.thr.ThrSetting;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class ThrSettingController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private ThrSettingDao thrSettingDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/thr/setting")
    public String thrSetting(Model model,
                             @RequestParam(required = false) String companies,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }

        model.addAttribute("thrSetting", new ThrSetting());
        model.addAttribute("listThrSetting", thrSettingDao.findByStatusOrderByDateActiveDesc(StatusRecord.AKTIF));

        return "thr/form_setting";

    }

    @PostMapping("/thr/setting/save")
    public String thrSettingSave(@ModelAttribute @Valid ThrSetting thrSetting,
                                 Authentication authentication,
                                 RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        String date = thrSetting.getDateActiveString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        thrSetting.setDateActive(localDate);
        thrSetting.setStatus(StatusRecord.AKTIF);
        thrSetting.setUserUpdate(user.getUsername());
        thrSetting.setDateUpdate(LocalDateTime.now());
        thrSettingDao.save(thrSetting);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../setting";
    }

    @PostMapping("/thr/setting/delete")
    public String thrSettingDelete(@RequestParam(required = true) ThrSetting thrSetting,
                                   Authentication authentication,
                                   RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        thrSetting.setStatus(StatusRecord.HAPUS);
        thrSettingDao.save(thrSetting);

        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../setting";
    }



}
