package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.DepartmentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.FakultasDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusClass;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Fakultas;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class FakultasController {

    @Autowired
    private FakultasDao fakultasDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;


    @GetMapping("/masterdata/fakultas")
    public String fakultas(Model model,
                           @RequestParam(required = false) String search,
                           @PageableDefault(size = 10) Pageable page,
                           @RequestParam(required = false) Integer size,
                           Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);


        List<String> idCompanies = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listFakultas",fakultasDao.findByStatusAndCompaniesIdInAndKodeFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasEnglishContainingIgnoreCaseOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,page));
        }else{
            model.addAttribute("listFakultas",fakultasDao.findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies,page));
        }

        model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentClassJenis(StatusRecord.AKTIF, StatusClass.FAKULTAS));
        model.addAttribute("fakultas", new Fakultas());
        model.addAttribute("page", page);
        model.addAttribute("size", size);

        model.addAttribute("menuFakultas", "active");
        return "masterdata/fakultas/list";
    }


    @GetMapping("/masterdata/fakultas/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getFakultasData(@PathVariable("id") String id) {
        try {
            Fakultas fakultas = fakultasDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid Fakultas ID: " + id));
            return ResponseEntity.ok(fakultas);  // Jika ditemukan, return 200 OK dengan data Department
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }

    @PostMapping("/masterdata/fakultas/save")
    public String saveFakultas(@ModelAttribute @Valid Fakultas fakultas,
                               BindingResult bindingResult,
                               Model model,
                               @RequestParam(required = false) String search,
                               @RequestParam(required = false) Integer size,
                               Pageable page,
                               RedirectAttributes redirectAttributes,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        if (size == null){
            size=10;
        }
        if (bindingResult.hasErrors()) {

            model.addAttribute("employess", employess);
            List<String> idCompanies = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listFakultas",fakultasDao.findByStatusAndCompaniesIdInAndKodeFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasContainingIgnoreCaseOrStatusAndCompaniesIdInAndNamaFakultasEnglishContainingIgnoreCaseOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,StatusRecord.AKTIF,idCompanies,search,page));
            }else{
                model.addAttribute("listFakultas",fakultasDao.findByStatusAndCompaniesIdInOrderByNamaFakultas(StatusRecord.AKTIF,idCompanies,page));
            }
            model.addAttribute("listDepartment", departmentDao.findByStatusAndDepartmentClassJenis(StatusRecord.AKTIF, StatusClass.FAKULTAS));
            model.addAttribute("fakultas", fakultas);
            model.addAttribute("page", page);
            model.addAttribute("size", size);
            model.addAttribute("menuFakultas", "active");
            return "masterdata/fakultas/list";
        }
        if(fakultas.getCompanies() == null){
            fakultas.setCompanies(fakultas.getDepartment().getCompanies());
        }
        fakultas.setUserUpdate(user.getUsername());
        fakultas.setDateUpdate(LocalDateTime.now());
        fakultasDao.save(fakultas);
        redirectAttributes.addFlashAttribute("berhasil", "Save Data Success");
        return "redirect:../fakultas?search=" + search + "&page=" + page.getPageNumber() + "&size=" + page.getPageSize();
    }


    @PostMapping("/masterdata/fakultas/delete")
    public String deleteFakultas(@RequestParam(required = false) Integer size,
                                   @RequestParam(required = false) String search,
                                   @RequestParam(required = false) Fakultas fakultas,
                                   @RequestParam(required = false) Integer page,
                                   Authentication authentication,
                                   RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);

        fakultas.setStatus(StatusRecord.HAPUS);
        fakultas.setUserUpdate(user.getUsername());
        fakultas.setDateUpdate(LocalDateTime.now());

        fakultasDao.save(fakultas);
        attribute.addFlashAttribute("berhasil", "Delete Data Success");
        return "redirect:../fakultas?search=" + search + "&page=" + page + "&size=" + size;
    }

}
