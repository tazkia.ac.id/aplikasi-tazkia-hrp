package id.ac.tazkia.aplikasihr.controller.document;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.aplikasihr.dao.document.DocumentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dto.document.BaseResponseDto;
import id.ac.tazkia.aplikasihr.dto.document.SKMengajarDto;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.document.Document;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter;

import java.time.LocalDateTime;

@Controller
@Slf4j
public class DocumentController {

    @Autowired private CurrentUserService currentUserService;

    @Autowired private EmployesDao employesDao;

    @Autowired private DocumentDao documentDao;

    @Autowired private DocumentService documentService;
    @Autowired
    private SimpleControllerHandlerAdapter simpleControllerHandlerAdapter;

    @GetMapping("/document")
    public String generateSuratMengajar(Model model, @RequestParam(required = false) String tahun, @RequestParam(required = false) String mulai, @RequestParam(required = false) String selesai, @RequestParam(required = false) String dosen, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("document", "active");

        BaseResponseDto getTahun = documentService.getTahunAkademik();
        model.addAttribute("tahun", getTahun.getData());

        if (tahun != null && dosen != null) {
            Integer code;
            String codeString;
            Document document = documentDao.findTopByIdTahunOrderByNomorDesc(tahun);
            if (document == null) {
                code = 1;
                codeString = "001";
            }else{
                Document cekDocument = documentDao.findByIdTahunAndIdDosen(tahun, dosen);
                if (cekDocument != null) {
                    code = cekDocument.getNomor();
                    codeString = cekDocument.getNomorString();
                }else{
                    code = document.getNomor() + 1;
                    codeString = String.format("%03d", code);
                }
            }
            BaseResponseDto getDosen = documentService.getMatkul(tahun, dosen, codeString);
            model.addAttribute("idTahun", tahun);
            model.addAttribute("idDosen", dosen);
            model.addAttribute("dosen", getDosen.getData());
            model.addAttribute("code", codeString);
            model.addAttribute("mulai", mulai);
            model.addAttribute("selesai", selesai);
        }

        return "document/generate";
    }

    @GetMapping("/api/get-dosen/{tahunId}")
    @ResponseBody
    public Object getDosen(@PathVariable String tahunId){

        return documentService.getDosenByTahun(tahunId).getData();
    }

    @GetMapping("/document/download")
    public ResponseEntity<byte[]> generatePdf(@RequestParam String tahun, @RequestParam String dosen, @RequestParam String no, @RequestParam String mulai, @RequestParam String selesai, Authentication authentication){
        try {
            User user = currentUserService.currentUser(authentication);
            ObjectMapper mapper = new ObjectMapper();

            BaseResponseDto getDosen = documentService.getMatkul(tahun, dosen, no);
            SKMengajarDto mengajarDto = mapper.convertValue(getDosen.getData(), SKMengajarDto.class);

            documentService.updateNomor(tahun, dosen, no, user);

            // Cek data dosen sudah ada atau belum
            documentService.cekDataDosen(mengajarDto.getDosen(), mengajarDto.getEmail(), mengajarDto.getProdi(), user);

            byte[] pdfBytes = documentService.suratTugasMengajar(mengajarDto, mulai, selesai);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDisposition(
                    ContentDisposition.attachment().filename("ST Mengajar " + mengajarDto.getDosen() + " - " + mengajarDto.getTahun() + ".pdf")
                            .build()
            );

            return new ResponseEntity<>(pdfBytes, headers, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    public void downloadDocument(@RequestParam String tahun, @RequestParam String dosen, @RequestParam String no, Authentication authentication, HttpServletResponse response) throws IOException {
//        User user = currentUserService.currentUser(authentication);
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//
//            BaseResponseDto getDosen = documentService.getMatkul(tahun, dosen, no);
//            SKMengajarDto mengajarDto = mapper.convertValue(getDosen.getData(), SKMengajarDto.class);
//
//            System.out.println(mengajarDto);
//
//            response.setContentType("application/pdf");
//            String headerValue = "attachment; filename=ST Mengajar " + mengajarDto.getDosen() +".pdf";
//
//            response.setHeader("Content-Disposition", headerValue);
//
//            documentService.suratTugasMengajar(mengajarDto, response);
//
//        }catch (Exception e){
//            e.printStackTrace();
//            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//        }
////        return "redirect:/document";
//    }

}
