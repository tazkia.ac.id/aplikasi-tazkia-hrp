package id.ac.tazkia.aplikasihr.controller.payroll;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.EmployeeDto;
import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollReportsFullDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HeaderPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.IsiPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.ListHeaderShiftDto;
import id.ac.tazkia.aplikasihr.entity.Bulan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.Tahun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.export.payroll.ExportPayrollDetail;
import id.ac.tazkia.aplikasihr.export.payroll.ExportPayrollFullDetail;
import id.ac.tazkia.aplikasihr.export.shift.ExportShiftClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.Header;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
public class PayrollReportsController {

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private PayrollRunDetailDao payrollRunDetailDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/payroll/reports/payslip")
    public String payrollReports(Model model,
                                 @RequestParam(required = false) String bulan,
                                 @RequestParam(required = false) String tahun,
                                 @RequestParam(required = false) Companies companies,
                                 Authentication authentication){


        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

//        model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companies);
        }

        if(bulan != null && tahun != null && companies != null){
            model.addAttribute("listPayslip", payrollRunDao.listPayrollRun(bulan, tahun, companies.getId()));
        }

        return "payroll/report/payslip";
    }


    @GetMapping("/payroll/reports/payslip/view")
    public String payrollReportsView(Model model,
                                 @RequestParam(required = true) String bulan,
                                 @RequestParam(required = true) String tahun,
                                 @RequestParam(required = true) Employes employes,
                                 Authentication authentication){


        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        String tanggalGajian = tahun + '-' + bulan + '-' + tanggalMulaiPeriode;
        LocalDate localDate3 = LocalDate.parse(tanggalGajian, formatter1);

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("paySlip", payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employes);
        model.addAttribute("companies", employes.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employes.getId(), localDate, localDate2));
        model.addAttribute("attendance", attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));

        model.addAttribute("allowance", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "ALLOWANCE"));

        model.addAttribute("deduction", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "DEDUCTION"));

        model.addAttribute("benefit", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "BENEFIT"));

        return "payroll/report/view";
    }

    @PostMapping("/payroll/reports/payslip/allowed")
    public String payrollPaySlipAllowed(@RequestParam(required = false) String bulan,
                                        @RequestParam(required = false) String tahun,
                                        @RequestParam(required = false) Companies companies,
                                        Authentication authentication,
                                        RedirectAttributes attribute) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        List<String> payrollRuns = payrollRunDao.listPayrollRunId(bulan, tahun, companies.getId());
        if (payrollRuns != null){
            for(String asd : payrollRuns){
                PayrollRun payrollRun = payrollRunDao.findByStatusAndId(StatusRecord.AKTIF, asd);
                if (payrollRun != null){
                    payrollRun.setPaySlip(StatusRecord.AKTIF);
                    payrollRun.setDateUpdate(LocalDateTime.now());
                    payrollRun.setUserUpdate(user.getUsername());
                    payrollRunDao.save(payrollRun);
                }
            }
        }

        attribute.addFlashAttribute("success", "Allowed PaySlip Successfull");
        return "redirect:../payslip?companies="+companies.getId()+"&bulan="+bulan+"&tahun="+tahun;

    }

    @PostMapping("/payroll/reports/payslip/print")
    public String payrollReportsPrint(Model model,
                                     @RequestParam(required = true) String bulan,
                                     @RequestParam(required = true) String tahun,
                                     @RequestParam(required = true) Employes employes,
                                     Authentication authentication){
        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("paySlip", payrollRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employes);
        model.addAttribute("companies", employes.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employes.getId(), localDate, localDate2));
        model.addAttribute("attendance", attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, employes.getId()));

        model.addAttribute("allowance", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "ALLOWANCE"));

        model.addAttribute("deduction", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "DEDUCTION"));

        model.addAttribute("benefit", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfterAndJenisPayroll(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO, "BENEFIT"));

        return "payroll/report/print";
    }


    @GetMapping("/payroll/reports/detail")
    public String payrollReportsList(Model model,
                                 @RequestParam(required = false) String bulan,
                                 @RequestParam(required = false) String tahun,
                                 @RequestParam(required = false) Companies companies,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
//        model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companies);
        }

//        if(bulan != null && tahun != null && companies != null){
//            model.addAttribute("listPayslip", payrollRunDao.listPayrollRun(bulan, tahun, companies.getId()));
//        }

        return "payroll/report/detail";
    }

    @PostMapping("/payroll/reports/detail/download")
    public String exportShiftHeader(@RequestParam(required = true) Companies companies,
                                    @RequestParam(required = true) String bulan,
                                    @RequestParam(required = true) String tahun,
                                    HttpServletResponse response,
                                    Authentication authentication,
                                    RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        Employes employes = employesDao.findByUser(user);

        Bulan bulanSelected = bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan);

        Tahun tahunSelecterd =  tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun);

        String companiesSelected = "All";
        if(companies != null) {
            companiesSelected = companies.getCompanyName() ;
        }

            response.setContentType("application/octet-stream");
            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String currentDateTime = dateFormatter.format(new Date());

//            Companies companies1 = companiesDao.findById(companies.getId()).get();
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=Payroll_detail_"+ companiesSelected +"_bulan_"+ bulanSelected.getNama() +"_tahun_"+ tahunSelecterd.getTahun() + ".xlsx";
            response.setHeader(headerKey, headerValue);


//            List<EmployeeDto> employeeDtoList = employesDao.listEmployee(companies);
        if (companies != null){
            List<DetailPayrollReportDto> payrollRuns = payrollRunDao.listPayrollDetailCompanies(bulan, tahun, companies.getId()) ;
            ExportPayrollDetail excelExporter = new ExportPayrollDetail(payrollRuns);
            excelExporter.export(response);
        }else{
            List<DetailPayrollReportDto> payrollRuns = payrollRunDao.listPayrollDetailAll(bulan, tahun) ;
            ExportPayrollDetail excelExporter = new ExportPayrollDetail(payrollRuns);
            excelExporter.export(response);
        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../schedule";

    }


    @GetMapping("/payroll/report/full/detail")
    public String payrollReportFullDetail(Model model,
                                          @RequestParam(required = false) String tahun,
                                          @RequestParam(required = false) String bulan,
                                          @RequestParam(required = false) String companies,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
//            model.addAttribute("companiesSelected", companiesDao.findByStatusAndIdIn(StatusRecord.AKTIF, companies));
        }

//        model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if(bulan != null && tahun != null && companies != null) {
//        model.addAttribute("listEmployes", payrollRunDetailDao.listEmployeePayroll(tahun, bulan, companies));
            model.addAttribute("listEmployes", payrollRunDetailDao.listEmployeePayrolls(tahun, bulan, companies));
//        model.addAttribute("headerPayrollReport", payrollRunDetailDao.headerLaporanPayroll(tahun, bulan, companies));
            model.addAttribute("headerPayrollReport", payrollRunDetailDao.headerLaporanPayrolls(tahun, bulan, companies));
//        model.addAttribute("isiPayrollReport", payrollRunDetailDao.listLaporanPayroll(tahun, bulan, companies));
            model.addAttribute("isiPayrollReport", payrollRunDetailDao.listLaporanPayrolls(tahun, bulan, companies));
        }
        return "payroll/report/full";
    }


    @PostMapping("/payroll/reports/detail/export")
    public String payrollReportFullExport(Model model,
                                          @RequestParam(required = false) String tahun,
                                          @RequestParam(required = false) String bulan,
                                          HttpServletResponse response,
                                          @RequestParam(required = false) String companies,
                                          Authentication authentication,
                                          RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        Companies companies1 = companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies);

        Bulan bulanSelected = bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan);

        Tahun tahunSelecterd =  tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun);

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
        }

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        model.addAttribute("listEmployes", payrollRunDetailDao.listEmployeePayroll(tahun, bulan, companies));
        model.addAttribute("headerPayrollReport", payrollRunDetailDao.headerLaporanPayroll(tahun, bulan, companies));
        model.addAttribute("isiPayrollReport", payrollRunDetailDao.listLaporanPayroll(tahun, bulan, companies));


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Payroll_full_detail_"+ companies +"_bulan_"+ bulanSelected.getNama() +"_tahun_"+ tahunSelecterd.getTahun() + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<EmployeePayrollReportsFullDto> employeePayrollReportsFullDtos = payrollRunDetailDao.listEmployeePayroll1(tahun, bulan, companies);
        List<HeaderPayrollRunReportDto> headerPayrollRunReportDtos = payrollRunDetailDao.headerLaporanPayroll1(tahun, bulan, companies);
        List<IsiPayrollRunReportDto> isiPayrollRunReportDtos = payrollRunDetailDao.listLaporanPayroll1(tahun, bulan, companies);
        ExportPayrollFullDetail excelExporter = new ExportPayrollFullDetail(employeePayrollReportsFullDtos, headerPayrollRunReportDtos, isiPayrollRunReportDtos);
        excelExporter.export(response);



        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../full";

    }



}
