package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.DayOffPatternScheduleAllowDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.DayOffDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternScheduleDao;
import id.ac.tazkia.aplikasihr.dto.DayOffPatternScheduleAllowDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.DayOff;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffPatternScheduleAllow;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class DayOffPatternScheduleAllowController {

    @Autowired
    private DayOffDao dayOffDao;

    @Autowired
    private PatternScheduleDao patternScheduleDao;

    @Autowired
    private DayOffPatternScheduleAllowDao dayOffPatternScheduleAllowDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;


    @GetMapping("/setting/day/off/shift_allowed")
    public String listDayOffShiftAllowed(Model model,
                                         @RequestParam(required = false)DayOff dayOff,
                                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listPatternScheduleAllow", dayOffPatternScheduleAllowDao.listDarOffPatternScheduleAllow(dayOff.getId()));
        model.addAttribute("dayOff", dayOff);

        return "setting/day_off/allow_shift";

    }

    @GetMapping("/setting/day/off/shift_allowed/save")
    public String saveDayOffShiftAllowed(@RequestParam(required = false) List<String> dayOffPatternScheduleAllowDto,
                                         @RequestParam(required = true) DayOff dayOff,
                                         Authentication authentication,
                                         RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        List<PatternSchedule> patternSchedules = patternScheduleDao.findByStatusOrderByScheduleName(StatusRecord.AKTIF);
        for(PatternSchedule b : patternSchedules){

            DayOffPatternScheduleAllow dayOffPatternScheduleAllow = dayOffPatternScheduleAllowDao.findByStatusAndDayOffAndPatternSchedule(StatusRecord.AKTIF, dayOff, b);

            if (dayOffPatternScheduleAllow != null) {
                dayOffPatternScheduleAllow.setStatus(StatusRecord.NONAKTIF);
                dayOffPatternScheduleAllow.setUserUpdate(user.getUsername());
                dayOffPatternScheduleAllow.setDateUpdate(LocalDateTime.now());
                dayOffPatternScheduleAllowDao.save(dayOffPatternScheduleAllow);
            }

        }

        if (dayOffPatternScheduleAllowDto != null) {
            for (String a : dayOffPatternScheduleAllowDto) {

                DayOffPatternScheduleAllow dayOffPatternScheduleAllow = dayOffPatternScheduleAllowDao.findByDayOffAndPatternSchedule(dayOff, patternScheduleDao.findById(a).get());
                if (dayOffPatternScheduleAllow == null) {
                    DayOffPatternScheduleAllow dayOffPatternScheduleAllow1 = new DayOffPatternScheduleAllow();
                    dayOffPatternScheduleAllow1.setDayOff(dayOff);
                    dayOffPatternScheduleAllow1.setPatternSchedule(patternScheduleDao.findById(a).get());
                    dayOffPatternScheduleAllow1.setDateUpdate(LocalDateTime.now());
                    dayOffPatternScheduleAllow1.setStatus(StatusRecord.AKTIF);
                    dayOffPatternScheduleAllow1.setUserUpdate(user.getUsername());
                    dayOffPatternScheduleAllowDao.save(dayOffPatternScheduleAllow1);
                } else {
                    DayOffPatternScheduleAllow dayOffPatternScheduleAllow1 = dayOffPatternScheduleAllow;
                    dayOffPatternScheduleAllow1.setDayOff(dayOff);
                    dayOffPatternScheduleAllow1.setPatternSchedule(patternScheduleDao.findById(a).get());
                    dayOffPatternScheduleAllow1.setDateUpdate(LocalDateTime.now());
                    dayOffPatternScheduleAllow1.setStatus(StatusRecord.AKTIF);
                    dayOffPatternScheduleAllow1.setUserUpdate(user.getUsername());
                    dayOffPatternScheduleAllowDao.save(dayOffPatternScheduleAllow1);
                }

            }
        }
        return "redirect:../../off";

    }

}
