package id.ac.tazkia.aplikasihr.controller.masterdata;


import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class EmployesDetailKeluargaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeAyahDao employeeAyahDao;

    @Autowired
    private EmployeeIbuDao employeeIbuDao;

    @Autowired
    private EmployeeAnakDao employeeAnakDao;

    @Autowired
    private EmployeeSaudaraDao employeeSaudaraDao;

    @Autowired
    private EmployeePasanganMenikahDao employeePasanganMenikahDao;

    @Autowired
    private JenjangPendidikanDao jenjangPendidikanDao;

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private EmployeeKartuIdentitasDao employeeKartuIdentitasDao;

    @Autowired
    private EmployeeKartuKeluargaDao employeeKartuKeluargaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.kartuidentitas}")
    private String uploadFolderKartuIdentitas;


    @Autowired
    @Value("${upload.kartukeluarga}")
    private String uploadFolderKartuKeluarga;

    @GetMapping("masterdata/employes/keluarga")
    public String listEmployeeKeluarga(Model model,
                                       @RequestParam(required = true) Employes employes,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        EmployeeKartuIdentitas employeeKartuIdentitas = employeeKartuIdentitasDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        EmployeeKartuKeluarga employeeKartuKeluarga = employeeKartuKeluargaDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        String kartuIdentitas = "default.jpg";
        String kartuKeluarga = "default.jpg";

        if (employeeKartuIdentitas == null){
            kartuIdentitas = "default.jpg";
        }else{
            kartuIdentitas = employeeKartuIdentitas.getFile();
        }

        if (employeeKartuKeluarga == null){
            kartuKeluarga = "default.jpg";
        }else{
            kartuKeluarga = employeeKartuKeluarga.getFile();
        }

        model.addAttribute("employes", employes);
        model.addAttribute("listEmployeeAyah", employeeAyahDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes));
        model.addAttribute("listEmployeeIbu", employeeIbuDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes));
        model.addAttribute("listEmployeePasanganMenikah", employeePasanganMenikahDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes));
        model.addAttribute("listEmployeeAnak", employeeAnakDao.findByStatusAndEmployes(StatusRecord.AKTIF,employes));
        model.addAttribute("listEmployeeSaudara", employeeSaudaraDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes));
        model.addAttribute("fileKartuIdentitas", kartuIdentitas);
        model.addAttribute("fileKartuKeluarga", kartuKeluarga);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/keluarga/list";

    }

    @GetMapping("masterdata/employes/ayah/new")
    public String newEmployesAyah(Model model,
                                  @RequestParam(required = true) Employes employes,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeAyah", new EmployeeAyah());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/ayah/form";
    }

    @GetMapping("masterdata/employes/ayah/edit")
    public String editEmployeAyah(Model model,
                                  @RequestParam(required = true) EmployeeAyah employeeAyah){


        model.addAttribute("employess", employesDao.findById(employeeAyah.getEmployes().getId()).get());
        model.addAttribute("employes", employesDao.findById(employeeAyah.getEmployes().getId()).get());
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeAyah", employeeAyah);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/ayah/form";

    }


    @PostMapping("masterdata/employes/ayah/save")
    public String saveEmployesAyah(@RequestParam(required = true) Employes employes,
                                   @ModelAttribute @Valid EmployeeAyah employeeAyah,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = employeeAyah.getTanggalLahir();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employeeAyah.getTanggalLahir(), formatter);
        employeeAyah.setTanggalLahirAyah(localDate);

        employeeAyah.setEmployes(employes);
        employeeAyah.setDateUpdate(LocalDateTime.now());
        employeeAyah.setUserUpdate(user.getUsername());
        employeeAyah.setStatus(StatusRecord.AKTIF);

        employeeAyahDao.save(employeeAyah);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }

    @PostMapping("masterdata/employes/ayah/delete")
    public String deleteEmployeeAyah(@ModelAttribute @Valid EmployeeAyah employeeAyah,
                                     Authentication authentication,
                                     RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeAyah.setDateUpdate(LocalDateTime.now());
        employeeAyah.setUserUpdate(user.getUsername());
        employeeAyah.setStatus(StatusRecord.HAPUS);

        employeeAyahDao.save(employeeAyah);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employeeAyah.getEmployes().getId();

    }

    @GetMapping("masterdata/employes/ibu/new")
    public String newEmployesIbu(Model model,
                                  @RequestParam(required = true) Employes employes,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeIbu", new EmployeeIbu());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/ibu/form";

    }

    @GetMapping("masterdata/employes/ibu/edit")
    public String editEmployeIbu(Model model,
                                  @RequestParam(required = true) EmployeeIbu employeeIbu,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employesDao.findById(employeeIbu.getEmployes().getId()).get());

        model.addAttribute("employes", employesDao.findById(employeeIbu.getEmployes().getId()).get());
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeIbu", employeeIbu);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/ibu/form";

    }


    @PostMapping("masterdata/employes/ibu/save")
    public String saveEmployesIbu(@RequestParam(required = true) Employes employes,
                                   @ModelAttribute @Valid EmployeeIbu employeeIbu,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = employeeIbu.getTanggalLahir();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employeeIbu.getTanggalLahir(), formatter);
        employeeIbu.setTanggalLahirIbu(localDate);

        employeeIbu.setEmployes(employes);
        employeeIbu.setDateUpdate(LocalDateTime.now());
        employeeIbu.setUserUpdate(user.getUsername());
        employeeIbu.setStatus(StatusRecord.AKTIF);

        employeeIbuDao.save(employeeIbu);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }

    @PostMapping("masterdata/employes/ibu/delete")
    public String deleteEmployeeIbu(@ModelAttribute @Valid EmployeeIbu employeeIbu,
                                     Authentication authentication,
                                     RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeIbu.setDateUpdate(LocalDateTime.now());
        employeeIbu.setUserUpdate(user.getUsername());
        employeeIbu.setStatus(StatusRecord.HAPUS);

        employeeIbuDao.save(employeeIbu);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employeeIbu.getEmployes().getId();

    }


    @GetMapping("masterdata/employes/saudara/new")
    public String newEmployesSaudara(Model model,
                                 @RequestParam(required = true) Employes employes,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeSaudara", new EmployeeSaudara());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/saudara/form";

    }

    @GetMapping("masterdata/employes/saudara/edit")
    public String editEmployeSaudara(Model model,
                                 @RequestParam(required = true) EmployeeSaudara employeeSaudara,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employesDao.findById(employeeSaudara.getEmployes().getId()).get());

        model.addAttribute("employes", employesDao.findById(employeeSaudara.getEmployes().getId()).get());
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeSaudara", employeeSaudara);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/saudara/form";

    }


    @PostMapping("masterdata/employes/saudara/save")
    public String saveEmployesSaudara(@RequestParam(required = true) Employes employes,
                                  @ModelAttribute @Valid EmployeeSaudara employeeSaudara,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = employeeSaudara.getTanggalLahir();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employeeSaudara.getTanggalLahir(), formatter);
        employeeSaudara.setTanggalLahirSaudara(localDate);

        employeeSaudara.setEmployes(employes);
        employeeSaudara.setDateUpdate(LocalDateTime.now());
        employeeSaudara.setUserUpdate(user.getUsername());
        employeeSaudara.setStatus(StatusRecord.AKTIF);

        employeeSaudaraDao.save(employeeSaudara);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }

    @PostMapping("masterdata/employes/saudara/delete")
    public String deleteEmployeeSaudara(@ModelAttribute @Valid EmployeeSaudara employeeSaudara,
                                    Authentication authentication,
                                    RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeSaudara.setDateUpdate(LocalDateTime.now());
        employeeSaudara.setUserUpdate(user.getUsername());
        employeeSaudara.setStatus(StatusRecord.HAPUS);

        employeeSaudaraDao.save(employeeSaudara);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employeeSaudara.getEmployes().getId();

    }


    @GetMapping("masterdata/employes/pasangan_menikah/new")
    public String newEmployesPasanganMenikah(Model model,
                                 @RequestParam(required = true) Employes employes,
                                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeePasanganMenikah", new EmployeePasanganMenikah());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/pasangan_menikah/form";

    }

    @GetMapping("masterdata/employes/pasangan_menikah/edit")
    public String editEmployePasanganMenikah(Model model,
                                 @RequestParam(required = true) EmployeePasanganMenikah employeePasanganMenikah,
                                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employesDao.findById(employeePasanganMenikah.getEmployes().getId()).get());

        model.addAttribute("employes", employesDao.findById(employeePasanganMenikah.getEmployes().getId()).get());
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeePasanganMenikah", employeePasanganMenikah);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/pasangan_menikah/form";

    }


    @PostMapping("masterdata/employes/pasangan_menikah/save")
    public String saveEmployesIbu(@RequestParam(required = true) Employes employes,
                                  @ModelAttribute @Valid EmployeePasanganMenikah employeePasanganMenikah,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = employeePasanganMenikah.getTanggalLahir();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employeePasanganMenikah.getTanggalLahir(), formatter);
        employeePasanganMenikah.setTanggalLahirPasangan(localDate);

        employeePasanganMenikah.setEmployes(employes);
        employeePasanganMenikah.setDateUpdate(LocalDateTime.now());
        employeePasanganMenikah.setUserUpdate(user.getUsername());
        employeePasanganMenikah.setStatus(StatusRecord.AKTIF);

        employeePasanganMenikahDao.save(employeePasanganMenikah);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }

    @PostMapping("masterdata/employes/pasangan_menikah/delete")
    public String deleteEmployeeIbu(@ModelAttribute @Valid EmployeePasanganMenikah employeePasanganMenikah,
                                    Authentication authentication,
                                    RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeePasanganMenikah.setDateUpdate(LocalDateTime.now());
        employeePasanganMenikah.setUserUpdate(user.getUsername());
        employeePasanganMenikah.setStatus(StatusRecord.HAPUS);

        employeePasanganMenikahDao.save(employeePasanganMenikah);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employeePasanganMenikah.getEmployes().getId();

    }


    @GetMapping("masterdata/employes/anak/new")
    public String newEmployesAnak(Model model,
                                 @RequestParam(required = true) Employes employes,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeAnak", new EmployeeAnak());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/anak/form";

    }

    @GetMapping("masterdata/employes/anak/edit")
    public String editEmployeAnak(Model model,
                                 @RequestParam(required = true) EmployeeAnak employeeAnak,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employesDao.findById(employeeAnak.getEmployes().getId()).get());

        model.addAttribute("employes", employesDao.findById(employeeAnak.getEmployes().getId()).get());
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("listAgama", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("employeeAnak", employeeAnak);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeFamily","active");
        return "masterdata/employes/anak/form";

    }


    @PostMapping("masterdata/employes/anak/save")
    public String saveEmployesAnak(@RequestParam(required = true) Employes employes,
                                  @ModelAttribute @Valid EmployeeAnak employeeAnak,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

//        String date = employeeAnak.getTanggalLahir();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employeeAnak.getTanggalLahir(), formatter);
        employeeAnak.setTanggalLahirAnak(localDate);

        employeeAnak.setEmployes(employes);
        employeeAnak.setDateUpdate(LocalDateTime.now());
        employeeAnak.setUserUpdate(user.getUsername());
        employeeAnak.setStatus(StatusRecord.AKTIF);

        employeeAnakDao.save(employeeAnak);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }

    @PostMapping("masterdata/employes/anak/delete")
    public String deleteEmployeeAnak(@ModelAttribute @Valid EmployeeAnak employeeAnak,
                                    Authentication authentication,
                                    RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeAnak.setDateUpdate(LocalDateTime.now());
        employeeAnak.setUserUpdate(user.getUsername());
        employeeAnak.setStatus(StatusRecord.HAPUS);

        employeeAnakDao.save(employeeAnak);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employeeAnak.getEmployes().getId();

    }


    @PostMapping("/masterdata/employes/detail/upload")
    @Transactional
    public String saveDetailEmployee(@ModelAttribute @Valid Employes employes,
                                     @RequestParam("ktp") MultipartFile ktp,
                                     @RequestParam("kk") MultipartFile kk,
                                     Authentication authentication,
                                     RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        if (ktp.isEmpty()){

        }else {
            List<EmployeeKartuIdentitas> employeeKartuIdentitass = employeeKartuIdentitasDao.findByStatusAndEmployesOrderById(StatusRecord.AKTIF, employes);
            if(employeeKartuIdentitass != null){
                for(EmployeeKartuIdentitas eki : employeeKartuIdentitass) {
                    eki.setStatus(StatusRecord.HAPUS);
                    eki.setDateUpdate(LocalDateTime.now());
                    eki.setUserUpdate(user.getUsername());

                    employeeKartuIdentitasDao.save(eki);
                }
            }
            EmployeeKartuIdentitas employeeKartuIdentitas = new EmployeeKartuIdentitas();
            String namaFile = ktp.getName();
            String jenisFile = ktp.getContentType();
            String namaAsli = ktp.getOriginalFilename();
            Long ukuran = ktp.getSize();
            String extension = "";
            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            if (ukuran <= 0) {

                System.out.println("file :" + namaAsli);
                System.out.println("Ukuran :" + ukuran);
//                if (employes.getFileFoto() == null) {
//                    employes.setFileFoto("default.jpg");
//                }
            } else {
                employeeKartuIdentitas.setFile(idFile + "." + extension);
                System.out.println("file :" + namaAsli);
                System.out.println("Ukuran :" + ukuran);
                LOGGER.debug("Lokasi upload : {}", uploadFolderKartuIdentitas);
                new File(uploadFolderKartuIdentitas).mkdirs();
                File tujuan = new File(uploadFolderKartuIdentitas + File.separator + idFile + "." + extension);
                ktp.transferTo(tujuan);
            }
            employeeKartuIdentitas.setEmployes(employes);
            employeeKartuIdentitas.setUserUpdate(user.getUsername());
            employeeKartuIdentitas.setDateUpdate(LocalDateTime.now());
            employeeKartuIdentitas.setStatus(StatusRecord.AKTIF);

            employeeKartuIdentitasDao.save(employeeKartuIdentitas);
        }


        if (kk.isEmpty()){

        }else {
            List<EmployeeKartuKeluarga> employeeKartuKeluargas = employeeKartuKeluargaDao.findByStatusAndEmployesOrderById(StatusRecord.AKTIF, employes);
            if(employeeKartuKeluargas != null){
                for(EmployeeKartuKeluarga ekk : employeeKartuKeluargas) {
                    ekk.setStatus(StatusRecord.HAPUS);
                    ekk.setDateUpdate(LocalDateTime.now());
                    ekk.setUserUpdate(user.getUsername());

                    employeeKartuKeluargaDao.save(ekk);
                }
            }
            EmployeeKartuKeluarga employeeKartuKeluarga = new EmployeeKartuKeluarga();
            String namaFile = kk.getName();
            String jenisFile = kk.getContentType();
            String namaAsli = kk.getOriginalFilename();
            Long ukuran = kk.getSize();
            String extension = "";
            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            if (ukuran <= 0) {

                System.out.println("file :" + namaAsli);
                System.out.println("Ukuran :" + ukuran);
//                if (employes.getFileFoto() == null) {
//                    employes.setFileFoto("default.jpg");
//                }
            } else {
                employeeKartuKeluarga.setFile(idFile + "." + extension);
                System.out.println("file :" + namaAsli);
                System.out.println("Ukuran :" + ukuran);
                LOGGER.debug("Lokasi upload : {}", uploadFolderKartuKeluarga);
                new File(uploadFolderKartuKeluarga).mkdirs();
                File tujuan = new File(uploadFolderKartuKeluarga + File.separator + idFile + "." + extension);
                kk.transferTo(tujuan);
            }
            employeeKartuKeluarga.setEmployes(employes);
            employeeKartuKeluarga.setUserUpdate(user.getUsername());
            employeeKartuKeluarga.setDateUpdate(LocalDateTime.now());
            employeeKartuKeluarga.setStatus(StatusRecord.AKTIF);

            employeeKartuKeluargaDao.save(employeeKartuKeluarga);
        }


        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../keluarga?employes="+ employes.getId();

    }


}
