package id.ac.tazkia.aplikasihr.controller.thr;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrRunDao;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollReportsFullDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HeaderPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.IsiPayrollRunReportDto;
import id.ac.tazkia.aplikasihr.entity.Bulan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.Tahun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRun;
import id.ac.tazkia.aplikasihr.export.payroll.ExportPayrollFullDetail;
import id.ac.tazkia.aplikasihr.export.thr.ExportThrDetail;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@Transactional
public class ThrController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private ThrRunDao thrRunDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/thr")
    public String homeThr(Model model,
                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("thrMenu", "active");
        return "thr/home";
    }

    @GetMapping("/thr/report")
    public String thrReportList(Model model,
                                @RequestParam(required = false) String bulan,
                                @RequestParam(required = false) String tahun,
                                @RequestParam(required = false) Companies companies,
//                                @RequestParam(required = false) Companies companies,
                                Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }


        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if (bulan != null) {
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null) {
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }

        if (companies != null) {

//            List<String> companiesString = new ArrayList<>();
//            for(String companies1 : companies){
//                companiesString.add(companies1);
//            }
//            model.addAttribute("companiesSelected", companiesString);
            model.addAttribute("companiesSelectedd", companies);
        }

        if (bulan != null && tahun != null && companies != null) {
            model.addAttribute("listThr", thrRunDao.findByStatusAndBulanAndTahunAndEmployesCompaniesIdOrderByEmployesNumber(StatusRecord.AKTIF, bulan, tahun, companies.getId()));
        }

        return "thr/form_report";
    }

    @PostMapping("/thr/reports/allowed")
    public String thrReportsAllowed(Model model,
                                    @RequestParam(required = true) String bulan,
                                    @RequestParam(required = true) String tahun,
                                    @RequestParam(required = false) Companies companies,
//                                    @RequestParam(required = true) Companies companies,
                                    Authentication authentication,
                                    RedirectAttributes redirectAttributes) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);



        List<String> employes1 = employesDao.cariEmployeeCompanyAktif2(companies.getId());

        if(employes1 != null) {
            thrRunDao.updatePaySlipThr(tahun, bulan, employes1);
        }

        return "redirect:../report?companies="+ companies.getId() +"&tahun="+ tahun + "&bulan" + bulan;

    }

    @PostMapping("/thr/reports/export")
    public String thrReportsExport(Model model,
                                    @RequestParam(required = true) String bulan,
                                    @RequestParam(required = true) String tahun,
                                    @RequestParam(required = false) Companies companies,
//                                    @RequestParam(required = true) Companies companies,
                                    HttpServletResponse response,
                                    Authentication authentication,
                                    RedirectAttributes attribute) throws IOException {


        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Bulan bulanSelected = bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan);

        Tahun tahunSelecterd =  tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun);

        if (bulan != null){
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }
        if (tahun != null){
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        }
        if (companies != null){
            model.addAttribute("companiesSelected", companies);
        }

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=THR_detail_bulan_"+ bulanSelected.getNama() +"_tahun_"+ tahunSelecterd.getTahun() + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<ThrRun> thrRuns = thrRunDao.findByStatusAndBulanAndTahunAndEmployesCompaniesIdOrderByEmployesNumber(StatusRecord.AKTIF, bulan, tahun, companies.getId());
        ExportThrDetail excelExporter = new ExportThrDetail(thrRuns, bulan, tahun, companies.getId());
        excelExporter.export(response);

        return "redirect:../report?companies="+ companies.getId() +"&tahun="+ tahun + "&bulan" + bulan;

    }

    @GetMapping("/thr/reports/slip")
    public String payrollReportsView(Model model,
                                     @RequestParam(required = true) String bulan,
                                     @RequestParam(required = true) String tahun,
                                     @RequestParam(required = true) Employes employes,
                                     Authentication authentication){

        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);


        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("paySlip", thrRunDao.findByStatusAndTahunAndBulanAndEmployesId(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employes);
        model.addAttribute("companies", employes.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employes.getId(), localDate, localDate2));

        return "thr/slip";
    }

    @PostMapping("/thr/reports/slip/print")
    public String payrollReportsPrint(Model model,
                                      @RequestParam(required = true) String bulan,
                                      @RequestParam(required = true) String tahun,
                                      @RequestParam(required = true) Employes employes,
                                      Authentication authentication){

        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);


        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("paySlip", thrRunDao.findByStatusAndTahunAndBulanAndEmployesId(StatusRecord.AKTIF, tahun, bulan, employes.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employes);
        model.addAttribute("companies", employes.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employes.getId(), localDate, localDate2));

        return "thr/print";
    }

}
