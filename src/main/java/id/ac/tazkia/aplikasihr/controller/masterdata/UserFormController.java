package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserFormController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/masterdata/user")
    public String listUser(Model model,
                           @RequestParam(required = false) String search,
                           @PageableDefault(size = 10)Pageable page,
                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listEmployee", employesDao.semuaUserSearch(search, page));
            } else {
                model.addAttribute("listEmployee", employesDao.semuaUser(page));
            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listEmployee", employesDao.semuaUserCompanySearch(idDepartements, search, page));
            } else {
                model.addAttribute("listEmployee", employesDao.semuaUserCompany(idDepartements, page));
            }
        }

        model.addAttribute("menuuser", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/user/list";

    }

    @GetMapping("/masterdata/user/roleedit")
    public String editRoleUser(Model model,
                               @RequestParam(required = false)User user,
                               Authentication authentication){

        User user1 = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user1);
        model.addAttribute("employess", employess);

        model.addAttribute("user", user);
        model.addAttribute("userRole", userRoleDao.findByUser(user));
        model.addAttribute("employes", employesDao.findByUser(user));

        List<String> selectedRoleIds = userRoleDao.findByUser(user)
                .stream()
                .map(ur -> ur.getRole().getId())
                .collect(Collectors.toList());

        model.addAttribute("selectedRoleIds", selectedRoleIds);



        model.addAttribute("roles", roleDao.findAll());

        model.addAttribute("menuuser", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/user/role";

    }

    @PostMapping("/masterdata/user/save")
    public String saveUser(@ModelAttribute @Valid User user,
                           @RequestParam(required = false) List<Role> role,
                           Authentication authentication,
                           RedirectAttributes attribute){

        List<UserRole> userRole = userRoleDao.findByUser(user);
        if(userRole != null) {
            for (UserRole ur : userRole) {
                userRoleDao.delete(ur);
            }
        }

        for(Role ur : role) {
            UserRole userRole1 = new UserRole();
            userRole1.setRole(ur);
            userRole1.setUser(user);
            userRoleDao.save(userRole1);
        }
        userDao.save(user);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../user";

    }

    @PostMapping("/masterdata/user/active")
    public String activeUser(@RequestParam(required = false) User user,
                             Authentication authentication,
                             RedirectAttributes attribute){

        user.setActive(true);

        userDao.save(user);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../user";

    }

    @PostMapping("/masterdata/user/nonactive")
    public String nonActiveUser(@RequestParam(required = false) User user,
                             Authentication authentication,
                             RedirectAttributes attribute){

        user.setActive(false);

        userDao.save(user);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../user";

    }

}
