package id.ac.tazkia.aplikasihr.controller.setting.kpi;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.kpi.KpiPeriodeDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.kpi.KpiPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
//import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
public class KpiPeriodeController {

    @Autowired
    private KpiPeriodeDao kpiPeriodeDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/kpi/periode")
    public String settingKpiPeriode(Model model,
                                    @PageableDefault(size = 10) Pageable page,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listKpiPeriode", kpiPeriodeDao.findByStatusOrderByKodePeriodeDesc(StatusRecord.AKTIF, page));

        model.addAttribute("menusettingkpiperiode", "active");
        model.addAttribute("setting", "active");
        return "setting/kpi/periode/list";
    }

    @GetMapping("/setting/kpi/periode/new")
    public String settingKpiPeriodeNew(Model model,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("kpiPeriode", new KpiPeriode());

        model.addAttribute("menusettingkpiperiode", "active");
        model.addAttribute("setting", "active");

        return "setting/kpi/periode/form";
    }

    @GetMapping("/setting/kpi/periode/edit")
    public String settingKpiPeriodeEdit(Model model,
                                        @RequestParam(required = true) String kpiPeriode,
                                        RedirectAttributes attribute,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        KpiPeriode kpiPeriode1 = kpiPeriodeDao.findByStatusAndId(StatusRecord.AKTIF, kpiPeriode);

        if(kpiPeriode1 != null) {
            model.addAttribute("employess", employess);
            model.addAttribute("kpiPeriode", kpiPeriode1);
            model.addAttribute("menusettingkpiperiode", "active");
            model.addAttribute("setting", "active");
            return "setting/kpi/periode/form";
        }else{
            attribute.addFlashAttribute("empty", "Data not available");
            return "redirect:../periode";
        }
    }

    @PostMapping("/setting/kpi/periode/save")
    public String settingKpiPeriodeSave(@ModelAttribute @Valid KpiPeriode kpiPeriode,
                                          BindingResult errors,
                                          Model model,
                                          RedirectAttributes attribute,
                                          Authentication authentication) throws IOException  {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        if(errors.hasErrors()){
            model.addAttribute("employess", employes);
//            model.addAttribute("kpiPeriode", kpiPeriode);
            model.addAttribute("menusettingkpiperiode", "active");
            model.addAttribute("setting", "active");
            return "setting/kpi/periode/form";
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate mulaiSetting = LocalDate.parse(kpiPeriode.getMulaiSettingString(), formatter);
        LocalDate selesaiSetting = LocalDate.parse(kpiPeriode.getSelesaiSettingString(), formatter);
        LocalDate mulaiPeriode = LocalDate.parse(kpiPeriode.getMulaiPeriodeString(), formatter);
        LocalDate selesaiPeriode = LocalDate.parse(kpiPeriode.getSelesaiPeriodeString(), formatter);
        LocalDate mulaiPenilaian = LocalDate.parse(kpiPeriode.getMulaiPenilaianString(), formatter);
        LocalDate selesaiPenilaian = LocalDate.parse(kpiPeriode.getSelesaiPenilaianString(), formatter);
        LocalDate tanggalKalkulasi = LocalDate.parse(kpiPeriode.getTanggalKalkulasiString(), formatter);

        kpiPeriode.setMulaiSetting(mulaiSetting);
        kpiPeriode.setSelesaiSetting(selesaiSetting);
        kpiPeriode.setMulaiPeriode(mulaiPeriode);
        kpiPeriode.setSelesaiPeriode(selesaiPeriode);
        kpiPeriode.setMulaiPenilaian(mulaiPenilaian);
        kpiPeriode.setSelesaiPenilaian(selesaiPenilaian);
        kpiPeriode.setTanggalKalkulasi(tanggalKalkulasi);
        kpiPeriode.setStatus(StatusRecord.AKTIF);
        if (kpiPeriode.getStatusAktif() == null){
            kpiPeriode.setStatusAktif(StatusRecord.NONAKTIF);
        }else{
            kpiPeriode.setStatusAktif(StatusRecord.AKTIF);
        }
        kpiPeriode.setUserUpdate(user.getUsername());
        kpiPeriode.setDateUpdate(LocalDateTime.now());
        kpiPeriodeDao.save(kpiPeriode);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../periode";
    }


    @PostMapping("/setting/kpi/periode/delete")
    public String settingKpiPeriodeDelete(@ModelAttribute @Valid KpiPeriode kpiPeriode,
                                          BindingResult errors,
                                          Model model,
                                          RedirectAttributes attribute,
                                          Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        kpiPeriode.setStatus(StatusRecord.HAPUS);
        kpiPeriode.setUserDelete(user.getUsername());
        kpiPeriode.setDateDelete(LocalDateTime.now());
        kpiPeriodeDao.save(kpiPeriode);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../periode";
    }
}
