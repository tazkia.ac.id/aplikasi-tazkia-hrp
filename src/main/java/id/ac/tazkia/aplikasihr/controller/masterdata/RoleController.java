package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.PermissionDao;
import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class RoleController {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;


    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/masterdata/role")
    public String listRole(Model model,
                           @RequestParam(required = false) User user,
                           Authentication authentication){

        User users = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(users);
        model.addAttribute("employess", employess);

        model.addAttribute("listRolePermission", roleDao.listRolePermission());
        model.addAttribute("user", user);
        model.addAttribute("employes", employesDao.findByUser(user));

        return "masterdata/role/list";
    }

    @GetMapping("/masterdata/role/add")
    public String addRole(Model model,
                          @RequestParam(required = false) User user,
                           Authentication authentication){

        User users = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(users);
        model.addAttribute("employess", employess);

        model.addAttribute("role", new Role());
        model.addAttribute("listPermission", permissionDao.findAll());
        model.addAttribute("user", user);
        return "masterdata/role/form";
    }

    @GetMapping("/masterdata/role/edit")
    public String listRole(Model model,
                           @RequestParam(required = false) User user,
                           @RequestParam(required = true) String role,
                           Authentication authentication){

        User users = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(users);
        model.addAttribute("employess", employess);

        model.addAttribute("role", roleDao.findById(role));
        model.addAttribute("listPermission", permissionDao.findAll());
        model.addAttribute("user", user);

        return "masterdata/role/form";
    }

    @PostMapping("/masterdata/role/save")
    public String saveRole(@ModelAttribute @Valid Role role,
                           @RequestParam(required = false) User user,
                           Authentication authentication,
                           RedirectAttributes redirectAttributes){

        User users = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(users);

        roleDao.save(role);
        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../role?user="+user.getId();

    }

}
