package id.ac.tazkia.aplikasihr.controller.setting.time_management;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.PatternScheduleDao;
import id.ac.tazkia.aplikasihr.dto.EmployeeDto;
import id.ac.tazkia.aplikasihr.dto.attendance.RunAttendanceDto;
import id.ac.tazkia.aplikasihr.dto.export.ExportEmployeeStatusDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.ListHeaderShiftDto;
import id.ac.tazkia.aplikasihr.dto.setting.time_management.PatternScheduleListDto;
import id.ac.tazkia.aplikasihr.entity.Bulan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.Tahun;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollInfo;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.EmployeeSchedule;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.PatternSchedule;
import id.ac.tazkia.aplikasihr.export.ExportEmployesClass;
import id.ac.tazkia.aplikasihr.export.shift.ExportPatternSchedule;
import id.ac.tazkia.aplikasihr.export.shift.ExportShiftClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class EmployeeScheduleController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private PatternScheduleDao patternScheduleDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/setting/timemanagement/employee/schedule")
    public String listEmployee(Model model,
                               @RequestParam(required = false)String statusRecord,
                               @RequestParam(required = false)String search,
                               @RequestParam(required = false)String patternSchedule,
                               @PageableDefault(size = 10)Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");


        if(statusRecord == null){
            statusRecord = "AKTIF";
        }

        String status = statusRecord;

        Object statuses = employeeJobPositionDao.lisStatusSelected(status);

        if (StringUtils.hasText(patternSchedule)) {
            PatternSchedule patternSchedule1 = patternScheduleDao.findByStatusAndId(StatusRecord.AKTIF, patternSchedule);
            model.addAttribute("patternScheduleSelected", patternScheduleDao.findByStatusAndId(StatusRecord.AKTIF, patternSchedule));

            if(role != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionDenganSearchSchedule(status, search, patternSchedule1.getScheduleName(), page));
                } else {
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionTanpaSearchSchedule(status,patternSchedule1.getScheduleName(), page));
                }
            } else {
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionCompaniesDenganSearchSchedule(status, search, idDepartements, patternSchedule1.getScheduleName(), page));
                } else {
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionCompaniesTanpaSearchSchedule(status, idDepartements,patternSchedule1.getScheduleName(), page));
                }
            }
        }else{

            if(role != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionDenganSearch(status, search, page));
                } else {
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionTanpaSearch(status, page));
                }
            } else {
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionCompaniesDenganSearch(status, search, idDepartements, page));
                } else {
                    model.addAttribute("statusRecordSelected", statuses);
                    model.addAttribute("statusRecord", employeeJobPositionDao.listStatus());
                    model.addAttribute("listEmployee", employeeJobPositionDao.listEmployeeJobPositionCompaniesTanpaSearch(status, idDepartements, page));
                }
            }
        }



        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        model.addAttribute("listPatternSchedule", patternScheduleDao.findByStatusOrderByScheduleName(StatusRecord.AKTIF));
        return "setting/time_management/employee_schedule/list";

    }


    @GetMapping("/setting/timemanagement/employee/schedule/proses")
    public String proseEmployeeSchedule(Model model,
                                        @RequestParam(required = true) List<Employes> employes,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listPatternSchedule", patternScheduleDao.findByStatusOrderByScheduleName(StatusRecord.AKTIF));
        model.addAttribute("listEmployes", employes);

        model.addAttribute("menutimemanagement", "active");
        model.addAttribute("setting", "active");
        return "setting/time_management/employee_schedule/form";

    }

    @Transactional
    @GetMapping("/setting/timemanagement/employee/schedule/save")
    public String saveEmployeeSchedule(Model model,
                                       @RequestParam(required = true) PatternSchedule patternSchedule,
                                       @RequestParam(required = true) String efectiveDateString,
                                       @RequestParam(required = true) List<Employes> employes,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String date = efectiveDateString;
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//        patternSchedule.setEfectiveDate(localDate);

        for (Employes eml : employes){
            employeeScheduleDao.deleteEmployeeSchedule(eml.getId(),localDate);
            EmployeeSchedule employeeSchedule = new EmployeeSchedule();
            employeeSchedule.setEmployes(eml);
            employeeSchedule.setPatternSchedule(patternSchedule);
            employeeSchedule.setEfectiveDate(localDate);
            employeeSchedule.setEfectiveDateString(efectiveDateString);
            employeeSchedule.setUserUpdate(user.getUsername());
            employeeSchedule.setDateUpdate(LocalDateTime.now());
            employeeScheduleDao.save(employeeSchedule);
        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../schedule";

    }

    @GetMapping("/setting/timemanagement/employee/schedule/view")
    public String viewEmployeeSchedule(Model model,
                                       @RequestParam(required = false) String bulan,
                                       @RequestParam(required = false) String tahun,
                                       @RequestParam(required = false) String employes,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");


        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        model.addAttribute("employess", employess);
        if(role != null) {
            model.addAttribute("listEmployee", employesDao.findByStatusOrderByNumber(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listEmployee", employesDao.findByStatusAndCompaniesIdInOrderByNumber(StatusRecord.AKTIF, idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        if(employes != null && bulan != null && tahun != null){
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1){
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")){
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            String tanggalMulai = tahun2+'-'+bulan2+'-'+tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun+'-'+bulan+'-'+tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            model.addAttribute("employeeSelected", employesDao.findByStatusAndId(StatusRecord.AKTIF, employes));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("listShiftEmployee", employeeScheduleDao.listEmployeeSchedule(localDate, localDate2, employes));

        }

        return "setting/time_management/employee_schedule/list_employee";

    }

    @GetMapping("/setting/timemanagement/employee/schedule/impor")
    public String runPayroll(Model model,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        return "setting/time_management/employee_schedule/import";

    }

    @Transactional
    @PostMapping("/setting/timemanagement/employee/schedule/impor/process")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Model model,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        System.out.println("Impor Schedule Mulai");
        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

            XSSFRow row = worksheet.getRow(i);
            XSSFRow row1 = worksheet.getRow(0);

            int columnCount = 2;

            if (row != null) {
                for (int a = 2; a < row.getPhysicalNumberOfCells(); a++) {
                    System.out.println("row = " + a);
                    if (row.getCell(a).getStringCellValue() != null) {
                        System.out.println("Cell = " + row.getCell(a));
//                    String ada = row.getCell(a).getStringCellValue();
                        PatternSchedule patternSchedule = patternScheduleDao.findOneByStatusAndScheduleName(StatusRecord.AKTIF, row.getCell(a).getStringCellValue());
                        if (patternSchedule != null) {
                            Employes employes = employesDao.findByStatusAndNumber(StatusRecord.AKTIF, row.getCell(0).getStringCellValue());
                            System.out.println("Employes = " + employes.getFullName());

                            String tanggal = row1.getCell(a).getStringCellValue();
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            LocalDate localDate = LocalDate.parse(tanggal, formatter);

                            employeeScheduleDao.deleteEmployeeSchedule(employes.getId(),localDate);

                            EmployeeSchedule employeeSchedule = new EmployeeSchedule();
                            employeeSchedule.setEmployes(employes);
                            employeeSchedule.setPatternSchedule(patternSchedule);

                            employeeSchedule.setEfectiveDate(localDate);
                            employeeSchedule.setEfectiveDateString(tanggal);
                            employeeSchedule.setUserUpdate(user.getUsername());
                            employeeSchedule.setDateUpdate(LocalDateTime.now());
                            employeeScheduleDao.save(employeeSchedule);
                        }else{
//                            model.addAttribute("companiesSelectet", c)
                            attributes.addFlashAttribute("gagalimpor", "nama shift " + row.getCell(a).getStringCellValue() + " tidak tersedia cek ulang kembali shift yang tersedia di tombol shift list");
                            return "redirect:../impor";
                        }
                    }
                }
            }
        }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../impor";
    }

    @PostMapping("/setting/timemanagement/employee/schedule/list")
    public String exportShiftList(HttpServletResponse response,
                                  Authentication authentication,
                                  RedirectAttributes attribute) throws IOException{

        User user = currentUserService.currentUser(authentication);

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Shedule_list.xlsx";
        response.setHeader(headerKey, headerValue);


        List<PatternScheduleListDto> patternScheduleListDtos = patternScheduleDao.listPatternSchedule();
        ExportPatternSchedule excelExporter = new ExportPatternSchedule(patternScheduleListDtos);

        excelExporter.export(response);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../schedule";
    }

    @PostMapping("/setting/timemanagement/employee/schedule/export")
    public String exportShiftHeader(HttpServletResponse response,
                                    @RequestParam(required = true) String companies,
                                   @RequestParam(required = true) String bulan,
                                   @RequestParam(required = true) String tahun,
                                   Authentication authentication,
                                   RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        if(companies != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);


            response.setContentType("application/octet-stream");
            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String currentDateTime = dateFormatter.format(new Date());

            Companies companies1 = companiesDao.findById(companies).get();
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=Shift_dari_"+ tanggalMulai +"_sampai_"+ tanggalSelesai + ".xlsx";
            response.setHeader(headerKey, headerValue);


            List<EmployeeDto> employeeDtoList = employesDao.listEmployee(companies);
            List<ListHeaderShiftDto> shiftList = employeeScheduleDao.listHeaderShift(localDate, localDate2);

            ExportShiftClass excelExporter = new ExportShiftClass(shiftList , employeeDtoList);

            excelExporter.export(response);

        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../schedule";
    }

}
