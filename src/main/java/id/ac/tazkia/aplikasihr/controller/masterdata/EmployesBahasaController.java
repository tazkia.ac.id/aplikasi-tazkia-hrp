package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeBahasaDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeBahasa;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKeahlian;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class EmployesBahasaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployeeBahasaDao employeeBahasaDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.sertifiatkbahasaatemployee}")
    private String uploadFolder;

    @GetMapping("/masterdata/employes/bahasa")
    public String listEmployeBahasa(Model model,
                                    @RequestParam(required = true) Employes employes,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listEmployeeBahasa", employeeBahasaDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes));

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeLanguages", "active");
        return "masterdata/employes/bahasa/list";

    }

    @GetMapping("/masterdata/employes/bahasa/new")
    public String newEmployeBahasa(Model model,
                                   @RequestParam(required = true) Employes employes,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("employeeBahasa", new EmployeeBahasa());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeLanguages", "active");
        return "masterdata/employes/bahasa/form";

    }

    @GetMapping("/masterdata/employes/bahasa/edit")
    public String editEmployeeBahasa(Model model,
                                     @RequestParam(required = true) EmployeeBahasa employeeBahasa,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employeeBahasa.getEmployes());
        model.addAttribute("employeeBahasa", employeeBahasa);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeLanguages", "active");
        return "masterdata/employes/bahasa/form";

    }

    @PostMapping("/masterdata/employes/bahasa/save")
    public String saveEmployeeBahasa(Model model,
                                     @RequestParam(required = true) Employes employes,
                                     @ModelAttribute @Valid EmployeeBahasa employeeBahasa,
                                     @RequestParam("fileSertifikat") MultipartFile fileSertifikat,
                                     Authentication authentication,
                                     RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        String namaFile =  fileSertifikat.getName();
        String jenisFile = fileSertifikat.getContentType();
        String namaAsli = fileSertifikat.getOriginalFilename();
        Long ukuran = fileSertifikat.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employeeBahasa.getSertifikat() == null){
                employes.setFileFoto("default.jpg");
            }
        }else{
            employeeBahasa.setSertifikat(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            fileSertifikat.transferTo(tujuan);
        }


        employeeBahasa.setEmployes(employes);
        employeeBahasa.setStatus(StatusRecord.AKTIF);
        employeeBahasa.setDateUpdate(LocalDateTime.now());
        employeeBahasa.setUserUpdate(user.getUsername());

        employeeBahasaDao.save(employeeBahasa);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../bahasa?employes="+ employes.getId();

    }

    @PostMapping("/masterdata/employes/bahasa/delete")
    public String deleteEmployesBahasa(Model model,
                                       @ModelAttribute @Valid EmployeeBahasa employeeBahasa,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeBahasa.setStatus(StatusRecord.HAPUS);
        employeeBahasa.setDateUpdate(LocalDateTime.now());
        employeeBahasa.setUserUpdate(user.getUsername());

        employeeBahasaDao.save(employeeBahasa);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../bahasa?employes="+ employeeBahasa.getEmployes().getId();

    }


    @GetMapping("/masterdata/employes/bahasa/{employeeBahasa}/sertifikat/")
    public ResponseEntity<byte[]> tanpilaknSertifikatBahasa(@PathVariable EmployeeBahasa employeeBahasa) throws Exception {
        String lokasiFile = uploadFolder + File.separator + employeeBahasa.getSertifikat();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeBahasa.getSertifikat().toLowerCase().endsWith("jpeg") || employeeBahasa.getSertifikat().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeBahasa.getSertifikat().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeBahasa.getSertifikat().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
