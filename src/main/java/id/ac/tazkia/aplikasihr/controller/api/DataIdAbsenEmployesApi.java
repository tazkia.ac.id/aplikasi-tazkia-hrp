package id.ac.tazkia.aplikasihr.controller.api;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dto.employes.TarikIdAbsenDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DataIdAbsenEmployesApi {

    @Autowired
    private EmployesDao employesDao;


    @GetMapping( "/api/tarikidabsen/pegawai")
    @ResponseBody
    public ResponseEntity<List<TarikIdAbsenDto>> idAbsenKaryawan() {

        List<Integer> idAbsenEmployes = employesDao.ambilIdAbsenEmployes();

        List<TarikIdAbsenDto> tarikAbsensiDtos = new ArrayList<>();

        for (Integer id : idAbsenEmployes) {
            TarikIdAbsenDto tarikAbsensiDto = new TarikIdAbsenDto();
            tarikAbsensiDto.setIdAbsen(id);

            tarikAbsensiDtos.add(tarikAbsensiDto);
        }
//
//        List<TarikAbsensiDto> tarikAbsensiDtos1 = tarikAbsenDtoDao.findByTanggalOrderByTanggal(LocalDate.now());

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(tarikAbsensiDtos, httpHeaders, HttpStatus.OK);

    }


}
