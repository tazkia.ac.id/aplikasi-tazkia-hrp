package id.ac.tazkia.aplikasihr.controller.payroll;

import com.lowagie.text.DocumentException;
import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceLecturerImporProcessDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.LecturerSalaryDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dto.payroll.DetailPayrollReportDto;
import id.ac.tazkia.aplikasihr.dto.payroll.LecturerSalaryReportDto;
import id.ac.tazkia.aplikasihr.entity.Bulan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.Tahun;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceLecturer;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import id.ac.tazkia.aplikasihr.entity.masterdata.LecturerSalary;
import id.ac.tazkia.aplikasihr.export.payroll.ExportPayrollDetail;
import id.ac.tazkia.aplikasihr.export.payroll.ExportPayrollLecturer;
import id.ac.tazkia.aplikasihr.export.payroll.PayslipDosenPdf;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
public class PayrollLecturerController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private LecturerSalaryDao lecturerSalaryDao;

    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;



    @Autowired
    private AttendanceLecturerImporProcessDao attendanceLecturerImporProcessDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/payroll/lecturer")
    public String payrollLecturer(Model model, Authentication authentication, @PageableDefault Pageable pageable, @RequestParam(required = false) String search){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");


        model.addAttribute("lecturerSalary", new LecturerSalary());
//        model.addAttribute("lecturerSalaryList", lecturerSalaryDao.listData(pageable));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            if(role != null) {
                model.addAttribute("lecturerSalaryList", lecturerSalaryDao.findByStatusOrderByTanggalBerlakuDesc(StatusRecord.AKTIF, pageable));
                model.addAttribute("lecturer", lecturerDao.selectDosen());
            } else {
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                model.addAttribute("lecturerSalaryList", lecturerSalaryDao.findByStatusAndLecturerCompaniesIdInOrderByTanggalBerlakuDesc(StatusRecord.AKTIF, idDepartements, pageable));
                model.addAttribute("lecturer", lecturerDao.selectDosenCompany(idDepartements));
            }
        }else{
            if(role != null) {
                model.addAttribute("lecturerSalaryList", lecturerSalaryDao.findByStatusOrderByTanggalBerlakuDesc(StatusRecord.AKTIF, pageable));
                model.addAttribute("lecturer", lecturerDao.selectDosen());
            } else {
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                model.addAttribute("lecturerSalaryList", lecturerSalaryDao.findByStatusAndLecturerCompaniesIdInOrderByTanggalBerlakuDesc(StatusRecord.AKTIF, idDepartements, pageable));
                model.addAttribute("lecturer", lecturerDao.selectDosenCompany(idDepartements));
            }
        }

        return "payroll/lecturer/list";
    }

    @PostMapping("/payroll/lecturer/save")
    public String lecturerSalarySave(@ModelAttribute LecturerSalary lecturerSalary,
                                     Authentication authentication,
                                     RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        lecturerSalary.setStatus(StatusRecord.AKTIF);
        lecturerSalaryDao.save(lecturerSalary);
        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:/payroll/lecturer";
    }

    @PostMapping("/payroll/lecturer/update")
    public String updateLecturer(@RequestParam(required = false) String idLecturer,
                                 @RequestParam(required = false) String tanggalBerlaku,
                                 @RequestParam(required = false) BigDecimal salaryPerSks,
                                 @RequestParam String id, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        String date = tanggalBerlaku;
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        Lecturer lecturer = lecturerDao.findByStatusAndId(StatusRecord.AKTIF, idLecturer);

        LecturerSalary lecturerSalary = lecturerSalaryDao.findByStatusAndId(StatusRecord.AKTIF, id);
        lecturerSalary.setLecturer(lecturer);
        lecturerSalary.setTanggalBerlaku(localDate);
        lecturerSalary.setSalaryPerSks(salaryPerSks);
        lecturerSalaryDao.save(lecturerSalary);

        return "redirect:/payroll/lecturer";
    }

    @PostMapping("/payroll/lecturer/delete")
    public String lecturerSalaryDelete(@RequestParam String id, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        lecturerSalaryDao.deleteSalaryLecturer(id);
        return "redirect:/payroll/lecturer";
    }

    @GetMapping("/payroll/lecturer/reports")
    public String payrollLecturerReport(Model model, Authentication authentication,
                                        @RequestParam(required = false) String bulan,
                                        @RequestParam(required = false) String jenjang,
                                        @RequestParam(required = false) String tahun){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(bulan != null && tahun != null){
            Bulan bulanSelected = bulanDao.findByStatusAndId(StatusRecord.AKTIF, bulan);
            Tahun tahunSelected = tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun);


            String tanggal = tahunSelected.getTahun() + '-' + bulanSelected.getNomor() + '-' + "01";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggal, formatter);

            model.addAttribute("tahunSelected", tahunSelected);
            model.addAttribute("bulanSelected", bulanSelected);
            model.addAttribute("jenjangSelected", lecturerSalaryDao.listJenjangSelected(jenjang));

            UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

            if(role != null) {
                model.addAttribute("listSalaryLecturer", lecturerSalaryDao.lecturerSalaryReport(tahunSelected.getTahun(), bulanSelected.getNomor(), localDate, jenjang));
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                model.addAttribute("listSalaryLecturer", lecturerSalaryDao.lecturerSalaryReportCompany(tahunSelected.getTahun(), bulanSelected.getNomor(), localDate, jenjang, idDepartements));
            }

        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("listJenjang", lecturerSalaryDao.listJenjang());

        return "payroll/lecturer/reports";
    }

    @PostMapping("/payroll/lecturer/expor")
    public String exporPayrollLecturer(@RequestParam(required = false) String bulan,
                                       @RequestParam(required = false) String jenjang,
                                       @RequestParam(required = false) String tahun,
                                       HttpServletResponse response,
                                       Authentication authentication,
                                       RedirectAttributes attribute) throws IOException {

        if(bulan != null && tahun != null && jenjang != null) {

            Bulan bulanSelected = bulanDao.findByStatusAndId(StatusRecord.AKTIF, bulan);
            Tahun tahunSelected = tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun);
            String jenjangSelected = lecturerSalaryDao.listJenjangSelectedS(jenjang);

            String tanggal = tahunSelected.getTahun() + '-' + bulanSelected.getNomor() + '-' + "01";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggal, formatter);


            response.setContentType("application/octet-stream");
            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String currentDateTime = dateFormatter.format(new Date());

//            Companies companies1 = companiesDao.findById(companies.getId()).get();
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=Payroll_lecturer_"+ jenjangSelected +"_bulan_"+ bulanSelected.getNama() +"_tahun_"+ tahunSelected.getTahun() + ".xlsx";
            response.setHeader(headerKey, headerValue);

            List<LecturerSalaryReportDto> lecturerSalaryReportDtos = lecturerSalaryDao.lecturerSalaryReport(tahunSelected.getTahun(), bulanSelected.getNomor(), localDate, jenjang);
            ExportPayrollLecturer excelExporter = new ExportPayrollLecturer(lecturerSalaryReportDtos);
            excelExporter.export(response);

        }

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../expor";

    }

    @GetMapping("/payroll/lecturer/payslip")
    public void generatePdfFile(HttpServletResponse response,
                                @RequestParam(required = true) Lecturer lecturer,
                                @RequestParam(required = false) String bulan,
                                @RequestParam(required = false) String jenjang,
                                @RequestParam(required = false) String tahun) throws DocumentException, IOException {

        response.setContentType("application/pdf");

        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");

        String currentDateTime = dateFormat.format(new Date());

        String headerkey = "Content-Disposition";

        String headervalue = "attachment; filename=Payslip_" + lecturer.getEmployes().getFullName() +"-Bulan-" + bulan + "-Tahun-" + tahun + ".pdf";

        response.setHeader(headerkey, headervalue);

        LecturerSalaryReportDto lecturerSalaryReportDto = lecturerSalaryDao.lecturerSalaryReportDosen(tahun, bulan, LocalDate.now(), jenjang, lecturer.getId());

        PayslipDosenPdf generator = new PayslipDosenPdf();

        generator.generate(lecturerSalaryReportDto);

    }

}
