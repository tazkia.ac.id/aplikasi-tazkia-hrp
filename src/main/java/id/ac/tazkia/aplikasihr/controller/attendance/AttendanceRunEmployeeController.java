package id.ac.tazkia.aplikasihr.controller.attendance;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.OverTimeRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.attendance.RunAttendanceDto;
import id.ac.tazkia.aplikasihr.dto.request.TarikOvertimeDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Controller
public class AttendanceRunEmployeeController {

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private OverTimeRequestDao overTimeRequestDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/attendance/run_employee")
    public String attendanceRunEmployee(Model model,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktifOrderByNumber(StatusRecord.AKTIF, "AKTIF"));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktifAndCompaniesIdInOrderByNumber(StatusRecord.AKTIF, "AKTIF", idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        return "attendance/run_employee";

    }

    @PostMapping("/attendance/run_employee/process")
    public String prosesRunEmployeePayroll(Model model,
                                           @RequestParam(required = true) List<String> employess,
                                           @RequestParam(required = true) String bulan,
                                           @RequestParam(required = true) String tahun,
                                           Authentication authentication,
                                           RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String adaBulan = bulan;
        String adaTahun = tahun;

        if (adaTahun == null && adaBulan == null) {
            LocalDateTime tahunAsli = LocalDateTime.now();
            Integer tahunAs = tahunAsli.getYear();
            String tahunString = tahunAs.toString();

            LocalDateTime bulanAsli = LocalDateTime.now();
            Integer bulanAs = bulanAsli.getMonthValue();
            String monthString = bulanAs.toString();

            if (monthString.length() > 1) {
                monthString = bulanAs.toString();
            } else {
                monthString = "0" + bulanAs.toString();
            }

            System.out.println("Bulan :" + monthString);
            System.out.println("Tahun :" + tahunString);

            bulan = monthString;
            tahun = tahunString;
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        if(employess != null && bulan != null && tahun != null) {
            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

//            List<String> employesList = employesDao.allEmployes();
//            if (companies.equals("All")) {
//                employesList = employesList;
//            } else {
//                employesList = employesDao.companyEmployes(companies);
//            }



            for (String e : employess) {

//                Integer attendanceOk = 0;
//                Integer absenOk = 0;
//                Integer jumlahHari = 0;
//                Integer cutiOk = 0;
//                Integer jumlahJamShiftFlexible = 0;
//                Integer jumlahJamAttendanceFlexible = 0;
//                Integer terlambat = 0;


                Integer attendanceOk = 0;
                Integer absenOk = 0;
                Integer jumlahHari = 0;
                Integer cutiOk = 0;
                Integer jumlahJamShiftFlexible = 0;
                Integer jumlahJamAttendanceFlexible = 0;
                Integer terlambat = 0;
                Integer overtime = 0;
                Integer overtimeDayOff = 0;
                Integer overtimeWorkingDayFirstHour = 0;
                Integer overtimeWorkingDayNextHour = 0;
                Integer overtimeHolidayFirstHour = 0;
                Integer overtimeHolidaySecondHour = 0;
                Integer overtimeHolidayNextHour = 0;

                System.out.println("Employee :" + e);
                List<RunAttendanceDto> runAttendanceDtos = attendanceDao.listRunAttendance(localDate, localDate2, e);

                AttendanceRun attendanceRun = attendanceRunDao.findByStatusAndTahunAndBulanAndIdEmployee(StatusRecord.AKTIF, tahun, bulan, e);
                if (attendanceRun == null){
                    attendanceRun = new AttendanceRun();
                }

                for (RunAttendanceDto runAttendanceDto : runAttendanceDtos){
                    String shiftIn = runAttendanceDto.getTanggal().toString()+ ' ' +runAttendanceDto.getShiftIn().toString()+":00";
//                    if(runAttendanceDto.getShiftIn().getHour() > runAttendanceDto.getShiftOut().getHour()){
//
//                    }
                    LocalDate tanggal1 = runAttendanceDto.getTanggal().plusDays(1);
                    String shiftOut = runAttendanceDto.getTanggal().toString()+ ' ' +runAttendanceDto.getShiftOut().toString()+":00";
                    DateTimeFormatter formatterShift = DateTimeFormatter.ofPattern("yyyy-MM-dd [HH][:mm][:ss]");

                    LocalDateTime shitInDateTime = LocalDateTime.parse(shiftIn, formatterShift);
                    LocalDateTime shitOutDateTime = LocalDateTime.parse(shiftOut, formatterShift);
//                    if (runAttendanceDto.getShiftIn().isAfter(runAttendanceDto.getShiftOut())){
//                         localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
//
//                         localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
//                    }else {
//                         localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
//
//                         localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
//                    }

                    String nol = " 00:00:00";
                    String attendanceIn = "-";
                    String attendanceOut = "-";

                    LocalDateTime localAttendanceIn = LocalDateTime.parse("1945-08-17 00:00:00", formatterShift);
                    LocalDateTime localAttendanceOut = LocalDateTime.parse("1945-08-17 00:00:00", formatterShift);

                    if (runAttendanceDto.getWaktuMasuk() != null){
                        if (runAttendanceDto.getWaktuKeluar() != null){
                            attendanceIn = runAttendanceDto.getWaktuMasuk();
                            localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
                            attendanceOut = runAttendanceDto.getWaktuKeluar();
                            localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
                        }else{
                            attendanceIn = runAttendanceDto.getWaktuMasuk();
                            localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
                            localAttendanceOut = LocalDateTime.parse(shiftOut, formatterShift);
                        }
                    }else{
                        if (runAttendanceDto.getWaktuKeluar() != null){
                            localAttendanceIn = LocalDateTime.parse(shiftIn, formatterShift);
                            attendanceOut = runAttendanceDto.getWaktuKeluar();
                            localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
                        }
                    }
//                    LocalDateTime localAttendanceIn = LocalDateTime.parse(attendanceIn, formatterShift);
//

                    if(shitInDateTime.isAfter(shitOutDateTime)){
                        attendanceOut = shiftOut;
                    }
//                    if (runAttendanceDto.getWaktuKeluar() != null){
//                        attendanceOut = runAttendanceDto.getWaktuKeluar();
//                        localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);
//                    }
//                    LocalDateTime localAttendanceOut = LocalDateTime.parse(attendanceOut, formatterShift);

                    if(shitInDateTime.isAfter(shitOutDateTime)){
                        shiftIn = shiftIn;
                        shiftOut = shiftOut;
                    }

                    LocalDateTime localShiftIn = LocalDateTime.parse(shiftIn, formatterShift);
                    LocalDateTime localShiftOut = LocalDateTime.parse(shiftOut, formatterShift);
                    if(localAttendanceIn.getHour() > localAttendanceOut.getHour()){
                        localAttendanceIn = localAttendanceIn;
                        localAttendanceOut = localAttendanceOut.plusDays(1);
                    }


                    Long duration = ChronoUnit.MINUTES.between(localShiftIn, localShiftOut);
                    Long duration1 = ChronoUnit.MINUTES.between(localAttendanceIn, localAttendanceOut);
//                    if(attendanceIn.equals("-") && attendanceOut.equals("-")){
//                        duration1 = ChronoUnit.MINUTES.between(localAttendanceIn, localAttendanceOut);
//                    }

//                    Integer intDuration = new Integer(String.valueOf(duration.toMinutes()));
//                    Integer intDuration2 = new Integer(String.valueOf(duration1.toMinutes()));
////                    long minutes = ChronoUnit.MINUTES.between(from, to);
//                    System.out.println("Durasi :" + duration.toHours());
//                    System.out.println("Durasi :" + duration.toMinutes());

                    if(runAttendanceDto.getStat().equals("ON")){
                        jumlahHari = jumlahHari + 1;
                    }

//                    String ada = runAttendanceDto.getWaktuMasuk();
//                    LocalDateTime localAttendanceOuta = LocalDateTime.parse(ada, formatterShift);

                    System.out.println("ShiftMasuk :" + localShiftIn);
                    System.out.println("ShiftKeluar :" + localShiftOut);

                    System.out.println("masuk :" + localAttendanceIn);
                    System.out.println("keluar :" + localAttendanceOut);


                    if (runAttendanceDto.getAttd().equals("AKTIF")){
                        if(runAttendanceDto.getFlexible().equals("AKTIF")){
//                            jumlahJamShiftFlexible = jumlahJamShiftFlexible + intDuration;
//                            jumlahJamAttendanceFlexible = jumlahJamAttendanceFlexible + intDuration2;
                            if(runAttendanceDto.getStat().equals("ON")){
                                if(duration1 > 0) {
                                    if (duration1 >= duration) {
                                        attendanceOk = attendanceOk + 1;
                                    } else {
                                        terlambat = terlambat + 1;
                                    }
                                }else{
                                    absenOk = absenOk + 1;
                                }
                            }else if(runAttendanceDto.getStat().equals("CUTI")){
                                cutiOk = cutiOk + 1;
                            }
                        }else{
                            if(runAttendanceDto.getStat().equals("ON")){
                                if(duration1 > 0) {
                                    Long selisihMasuk = ChronoUnit.MINUTES.between(localShiftIn, localAttendanceIn);
                                    Long selisihKeluar = ChronoUnit.MINUTES.between(localShiftOut, localAttendanceOut);

                                    System.out.println("masuk :" + selisihMasuk);
                                    System.out.println("keluar :" + selisihKeluar);




//                                    String ada = runAttendanceDto.getWaktuMasuk().toString();
//                                    LocalDateTime localAttendanceOuta = LocalDateTime.parse(ada, formatterShift);
                                    if(selisihMasuk > 15){
                                        terlambat = terlambat + 1;
                                        attendanceOk = attendanceOk + 1;
                                    }else{
                                        if(selisihKeluar < 0) {
                                            terlambat = terlambat + 1;
                                            attendanceOk = attendanceOk + 1;
                                        }else{
                                            attendanceOk = attendanceOk + 1;
                                        }
                                    }
                                }else{
                                    absenOk = absenOk + 1;
                                }

                            }else if(runAttendanceDto.getStat().equals("CUTI")) {
                                cutiOk = cutiOk + 1;
                            }
                        }
                    }else{
                        if(runAttendanceDto.getStat().equals("ON")){
                            attendanceOk = jumlahHari;
                        }
                        else if(runAttendanceDto.getStat().equals("CUTI")) {
                            cutiOk = cutiOk + 1;
                        }
                    }

                }

                //run overtime disini
                //cari overtime working day
                List<TarikOvertimeDto> tarikOvertimeDtoWorkingDay = overTimeRequestDao.tarikDataOvertimeEmployee(e, localDate, localDate2, "ON");
                if(tarikOvertimeDtoWorkingDay != null){
                    for(TarikOvertimeDto tarikOvertimeDtoWd : tarikOvertimeDtoWorkingDay){
                        if(tarikOvertimeDtoWd.getJam() > 1){
                            overtimeWorkingDayFirstHour = overtimeWorkingDayFirstHour + 1;
                            overtimeWorkingDayNextHour = overtimeWorkingDayNextHour + (tarikOvertimeDtoWd.getJam() - 1);
                        }else{
                            overtimeWorkingDayFirstHour = overtimeWorkingDayFirstHour + tarikOvertimeDtoWd.getJam();
                        }
                    }
                }
                List<TarikOvertimeDto> tarikOvertimeDtoHoliday = overTimeRequestDao.tarikDataOvertimeEmployee(e, localDate, localDate2, "OFF");
                if(tarikOvertimeDtoHoliday != null){
                    for(TarikOvertimeDto tarikOvertimeDtoHd : tarikOvertimeDtoHoliday){
                        if(tarikOvertimeDtoHd.getJam() >= 1){
                            if(tarikOvertimeDtoHd.getJam() >= 2){
                                overtimeHolidayFirstHour = overtimeHolidayFirstHour + 1;
                                overtimeHolidaySecondHour = overtimeHolidaySecondHour + 1;
                                overtimeHolidayNextHour = overtimeHolidayNextHour + (tarikOvertimeDtoHd.getJam() - 2);
                            }else{
                                overtimeHolidayFirstHour = overtimeHolidayFirstHour + 1;
                                overtimeHolidaySecondHour = overtimeHolidaySecondHour + 1;
                            }
                        }else{
                            overtimeHolidayFirstHour = overtimeHolidayFirstHour + tarikOvertimeDtoHd.getJam();
                        }
                    }
                }

                attendanceRun.setBulan(bulan);
                attendanceRun.setTahun(tahun);
                attendanceRun.setIdEmployee(e);
                attendanceRun.setTotalAttendance(attendanceOk);
                attendanceRun.setTotalAbsen(absenOk);
                attendanceRun.setTotalLate(terlambat);
                attendanceRun.setTotalTimeOff(cutiOk);
                attendanceRun.setTotalShift(jumlahHari);
                attendanceRun.setStatus(StatusRecord.AKTIF);
                attendanceRun.setUserUpdate(user.getUsername());
                attendanceRun.setDateUpdate(LocalDateTime.now());
                attendanceRun.setOvertimeWorkingDayFirstHour(overtimeWorkingDayFirstHour);
                attendanceRun.setOvertimeWorkingDayNextHour(overtimeWorkingDayNextHour);
                attendanceRun.setOvertimeHolidayFirstHour(overtimeHolidayFirstHour);
                attendanceRun.setOvertimeHolidaySecondHour(overtimeHolidaySecondHour);
                attendanceRun.setOvertimeHolidayNextHour(overtimeHolidayNextHour);
                attendanceRunDao.save(attendanceRun);

            }
        }

        model.addAttribute("employeeSelected", employesDao.findByStatusAndStatusAktifAndIdIn(StatusRecord.AKTIF, "AKTIF", employess));
        model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../run_employee";
    }

}
