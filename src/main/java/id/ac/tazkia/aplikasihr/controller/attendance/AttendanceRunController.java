package id.ac.tazkia.aplikasihr.controller.attendance;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunProcessDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunProcessDetailDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.attendance.RunAttendanceDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRunProcess;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Controller
public class AttendanceRunController {

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;


    @Autowired
    private AttendanceRunProcessDao attendanceRunProcessDao;

    @Autowired
    private AttendanceRunProcessDetailDao attendanceRunProcessDetailDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/attendance/run")
    public String runPayroll(Model model,
                             @RequestParam(required = false) String companies,
                             @RequestParam(required = false) String bulan,
                             @RequestParam(required = false) String tahun,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(companies != null && tahun != null && bulan != null){
            model.addAttribute("companiessSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }


        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }


        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("listAttendanceRunProcess", attendanceRunProcessDao.listAttendanceRunProcess());
//        model.addAttribute("listDone", attendanceRunProcessDetailDao.statusAttendaceRun(StatusRecord.DONE));
//        model.addAttribute("listError", attendanceRunProcessDetailDao.statusAttendaceRun(StatusRecord.ERROR));

        return "attendance/run";

    }

    @GetMapping("/attendance/run/detail_success")
    public String listAttendanceDetailSuccess(Model model,
                                              @RequestParam(required = true) AttendanceRunProcess attendanceRunProcess,
                                              Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listDetailSuccess", attendanceRunProcessDetailDao.findByStatusAndAndAttendanceRunProcessOrderByEmployesNumber(StatusRecord.DONE, attendanceRunProcess));

        return "attendance/run/success";
    }

    @GetMapping("/attendance/run/detail_error")
    public String listAttendanceDetailError(Model model,
                                            @RequestParam(required = true) AttendanceRunProcess attendanceRunProcess,
                                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listDetailSuccess", attendanceRunProcessDetailDao.findByStatusAndAndAttendanceRunProcessOrderByEmployesNumber(StatusRecord.ERROR, attendanceRunProcess));


        return "attendance/run/error";
    }

    @PostMapping("/attendance/run/process")
    public String prosesRunPayroll(Model model,
                                   @RequestParam(required = true) String companies,
                                   @RequestParam(required = true) String bulan,
                                   @RequestParam(required = true) String tahun,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        AttendanceRunProcess attendanceRunProcess = new AttendanceRunProcess();
        attendanceRunProcess.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
        attendanceRunProcess.setTahun(tahun);
        attendanceRunProcess.setBulan(bulan);
        attendanceRunProcess.setTanggalInput(LocalDateTime.now());
        attendanceRunProcess.setStatus(StatusRecord.WAITING);
        attendanceRunProcess.setUserInput(employes.getFullName());
        attendanceRunProcessDao.save(attendanceRunProcess);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../run?companies="+ companies +"&tahun="+ tahun + "&bulan" + bulan;

    }

}
