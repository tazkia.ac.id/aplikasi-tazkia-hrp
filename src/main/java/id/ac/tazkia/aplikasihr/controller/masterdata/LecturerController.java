package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Controller
public class LecturerController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private LecturerClassDao lecturerClassDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private JabatanFungsionalDosenDao jabatanFungsionalDosenDao;

    @Value("${upload.jabatanDosen}")
    private String uploadJabatanDosen;

    @GetMapping("/masterdata/lecturer")
    public String lecturerDataList(Model model,
                                   @RequestParam(required = false) String search,
                                   @RequestParam(required = false) Companies companies,
                                   Authentication authentication,
                                   @PageableDefault(size = 10) Pageable page){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("lecturer", new Lecturer());
        model.addAttribute("jenisDosen", lecturerClassDao.findByStatusOrderByClasss(StatusRecord.AKTIF));
        model.addAttribute("jabatanDosen", jabatanFungsionalDosenDao.findByStatus(StatusRecord.AKTIF));
//        model.addAttribute("departemen", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF));

        List<String> idCompanies = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullNameAsc(StatusRecord.AKTIF, companies, search, page));
                } else {
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesOrderByEmployesFullNameAsc(StatusRecord.AKTIF, companies, page));
                }
                model.addAttribute("companiesSelected", companies);
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndEmployesFullNameContainingIgnoreCaseOrderByNameAsc(StatusRecord.AKTIF, search, page));
                } else {
                    model.addAttribute("listDosen", lecturerDao.findByStatusOrderByEmployesFullNameAsc(StatusRecord.AKTIF, page));
                }

            }
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            if (companies != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullNameAsc(StatusRecord.AKTIF, companies, search, page));
                } else {
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesOrderByEmployesFullNameAsc(StatusRecord.AKTIF, companies, page));
                }
                model.addAttribute("companiesSelected", companies);
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesIdInAndEmployesFullNameContainingIgnoreCaseOrderByEmployesFullNameAsc(StatusRecord.AKTIF, idDepartements, search, page));
                } else {
                    model.addAttribute("listDosen", lecturerDao.findByStatusAndProdiDepartmentCompaniesIdInOrderByEmployesFullNameAsc(StatusRecord.AKTIF, idDepartements, page));
                }

            }
        }

        if(role != null) {
            model.addAttribute("listProdi", prodiDao.findByStatusOrderByNamaProdiAsc(StatusRecord.AKTIF));
            model.addAttribute("listEmployee", employesDao.findByStatusOrderByNumber(StatusRecord.AKTIF));
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listProdi", prodiDao.findByStatusAndFakultasCompaniesIdInOrderByNamaProdiAsc(StatusRecord.AKTIF, idCompanies));
            model.addAttribute("listEmployee", employesDao.findByStatusAndCompaniesIdInOrderByNumber(StatusRecord.AKTIF, idCompanies));
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }


        model.addAttribute("lecturerClass", lecturerClassDao.findByStatusOrderByClasss(StatusRecord.AKTIF));
        model.addAttribute("page1",page);
        model.addAttribute("menulecturer", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/lecturer/list";
    }

    @GetMapping("/masterdata/lecturer/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getLecturerData(@PathVariable("id") String id) {
        try {
            Lecturer lecturer = lecturerDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid Lecturer ID: " + id));
            return ResponseEntity.ok(lecturer);  // Jika ditemukan, return 200 OK dengan data Department
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }

    @PostMapping("/masterdata/lecturer/save")
    public String saveLecturer(@ModelAttribute @Valid Lecturer lecturer,
                               BindingResult bindingResult,
                               @RequestParam(required = false)String search,
                               @RequestParam(required = false)Integer size,
                               Pageable page,
                               Authentication authentication,
                               RedirectAttributes attribute,
                               @RequestParam(required = false, name = "fileJabatanFungsionalDosen") MultipartFile fileJabatanDosen) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        if (bindingResult.hasErrors()) {

        }

        lecturer.setCompanies(lecturer.getProdi().getDepartment().getCompanies());
        lecturer.setDateUpdate(LocalDateTime.now());
        lecturer.setUserUpdate(user.getUsername());
        lecturer.setJabatanFungsionalDosen(lecturer.getJabatanFungsionalDosen());

        if (fileJabatanDosen != null && !fileJabatanDosen.isEmpty()) {
            String namaAsli = fileJabatanDosen.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadJabatanDosen + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            fileJabatanDosen.transferTo(tujuan);
            lecturer.setFileJabatanFungsionalDosen(idFile + "." + extension);
        }else{
            Lecturer existingLecturer = lecturerDao.findById(lecturer.getId()).orElse(null);
            if (existingLecturer != null) {
                lecturer.setFileJabatanFungsionalDosen(existingLecturer.getFileJabatanFungsionalDosen());
            }
        }

        lecturerDao.save(lecturer);

        attribute.addFlashAttribute("berhasil", "Update Data Success");
        return "redirect:/masterdata/lecturer?search=" + search + "&page=" + page.getPageNumber() + "&size=" + size;
    }

    @PostMapping("/masterdata/lecturer/del")
    public String deleteLecturer(@RequestParam String lecturer, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);



        lecturerDao.deleteLecturer(lecturer);
        return "redirect:/masterdata/lecturer";
    }

    @GetMapping("/masterdata/lecturer/detail")
    public String getDetailLecturer(Model model, @RequestParam Lecturer param) {
        model.addAttribute("menulecturer", "active");
        model.addAttribute("masterdata", "active");

        model.addAttribute("lecturer", param);
        return "masterdata/lecturer/detail";
    }

    @GetMapping("/masterdata/lecturer/jabatan")
    public ResponseEntity<byte[]> fileGambar(@RequestParam Lecturer file) throws Exception{
        String lokasiFile = uploadJabatanDosen + File.separator + file.getFileJabatanFungsionalDosen();
        System.out.println("Test : " + lokasiFile);
        try {
            HttpHeaders headers = new HttpHeaders();
            if (file.getFileJabatanFungsionalDosen().toLowerCase().endsWith("jpeg") || file.getFileJabatanFungsionalDosen().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(file.getFileJabatanFungsionalDosen().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(file.getFileJabatanFungsionalDosen().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    
}
