package id.ac.tazkia.aplikasihr.controller;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.AnnouncementCompaniesDao;
import id.ac.tazkia.aplikasihr.dao.AnnouncementDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.Announcement;
import id.ac.tazkia.aplikasihr.entity.AnnouncementCompanies;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKartuKeluarga;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.EmailAnnouncementService;
import id.ac.tazkia.aplikasihr.services.GmailApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;

@Controller
public class AnnouncementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);


    @Autowired
    private AnnouncementDao announcementDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private AnnouncementCompaniesDao announcementCompaniesDao;

    @Autowired private GmailApiService gmailApiService;

    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    @Value("${upload.announcement}")
    private String uploadFolderAnnouncment;

    @Autowired
    @Value("${gmail.account.username}")
    private String email;

    @Autowired
    @Value("${gmail.credential}")
    private String password;

    @Autowired
    private EmailAnnouncementService emailAnnouncementService;

    @PostMapping("/announcement/save")
    public String saveAnnouncement(@ModelAttribute @Valid Announcement announcement,
                                   @RequestParam("file") MultipartFile file,
                                   @RequestParam(required = false) List<String> companies,
                                   Authentication authentication,
                                   RedirectAttributes attribute) throws IOException, MessagingException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        File tujuan = new File("");
        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employes.getFileFoto() == null){
                announcement.setFileUpload("default.jpg");
            }
        }else{
            announcement.setFileUpload(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolderAnnouncment);
            new File(uploadFolderAnnouncment).mkdirs();
            tujuan = new File(uploadFolderAnnouncment + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        announcement.setStatus(StatusRecord.AKTIF);
        announcement.setDateUpdate(LocalDateTime.now());
        announcement.setUserUpdate(user.getUsername());
        announcement.setEmployes(employes);

//        List<Employes> employesList = employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, StatusRecord.AKTIF);
//        if(announcement.getCompanies() != null){
//            employesList = employesDao.findByStatusAndStatusAktifAndCompanies(StatusRecord.AKTIF, StatusRecord.AKTIF, announcement.getCompanies());
//        }

        announcementDao.save(announcement);

        for(String lc : companies){

            AnnouncementCompanies announcementCompanies = new AnnouncementCompanies();
            announcementCompanies.setAnnouncement(announcement);
            announcementCompanies.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF, lc));
            announcement.getAnnouncementCompanies().add(announcementCompanies);

            announcementCompaniesDao.save(announcementCompanies);

        }

        emailAnnouncementService.kirimEmailAnnouncement(announcement.getId(), companies, announcement.getIsi(), tujuan.toString());
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../dashboard";

    }


    @GetMapping("/announcement/{announcement}/file/")
    public ResponseEntity<byte[]> tampilAttachmentAnnouncement(@PathVariable Announcement announcement) throws Exception {
        String lokasiFile = uploadFolderAnnouncment + File.separator + announcement.getFileUpload();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (announcement.getFileUpload().toLowerCase().endsWith("jpeg") || announcement.getFileUpload().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (announcement.getFileUpload().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (announcement.getFileUpload().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
