package id.ac.tazkia.aplikasihr.controller.setting.payroll;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollComponentDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollTunjanganDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollTunjangan;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Controller
public class PayrollTunjanganController {

    @Autowired
    private PayrollComponentDao payrollComponentDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/setting/payroll/tunjangan")
    public String listPayrollTunjangan(Model model,
                                       @PageableDefault(size = 10)Pageable page,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("pageTunjangan", payrollComponentDao.findByStatusAndJenisComponentOrderByName(StatusRecord.AKTIF,"ALLOWANCE", page));

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/tunjangan/list";

    }

    @GetMapping("/setting/payroll/tunjangan/new")
    public String newTunjangan(Model model,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("payrollComponent", new PayrollComponent());

        model.addAttribute("menupayrollconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/payroll/tunjangan/form";

    }

    @PostMapping("/setting/payroll/tunjangan/save")
    public String savePayrollTunjangan(Model model,
                                       @ModelAttribute @Valid PayrollComponent payrollComponent,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);


        payrollComponent.setDateUpdate(LocalDateTime.now());
        payrollComponent.setUserUpdate(user.getUsername());
        payrollComponent.setJenisComponent("ALLOWANCE");
        payrollComponent.setRumus("DEFAULT");
        payrollComponent.setStatus(StatusRecord.AKTIF);
        if(payrollComponent.getTaxable() == null){
            payrollComponent.setTaxable(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setTaxable(StatusRecord.AKTIF);
        }

        if(payrollComponent.getThr() == null){
            payrollComponent.setThr(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setThr(StatusRecord.AKTIF);
        }

        if(payrollComponent.getThrPajak() == null){
            payrollComponent.setThrPajak(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setThrPajak(StatusRecord.AKTIF);
        }

        if(payrollComponent.getPersentase() == null){
            payrollComponent.setPersentase(BigDecimal.ZERO);
        }

        if(payrollComponent.getPotonganAbsen() == null){
            payrollComponent.setPotonganAbsen(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setPotonganAbsen(StatusRecord.AKTIF);
        }


        if(payrollComponent.getPotonganTerlambat() == null){
            payrollComponent.setPotonganTerlambat(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setPotonganTerlambat(StatusRecord.AKTIF);
        }

        if(payrollComponent.getZakat() == null){
            payrollComponent.setZakat(StatusRecord.NONAKTIF);
        }else{
            payrollComponent.setZakat(StatusRecord.AKTIF);
        }


        payrollComponentDao.save(payrollComponent);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../tunjangan";

    }

    @GetMapping("/setting/payroll/tunjangan/edit")
    public String editPayrollTunjangan(Model model,
                                       RedirectAttributes attribute,
                                       @RequestParam(required = true) PayrollComponent payrollComponent,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        PayrollComponent payrollComponent1 = payrollComponentDao.findByIdAndJenisComponentAndStatus(payrollComponent.getId(),"ALLOWANCE", StatusRecord.AKTIF);

        if (payrollComponent1 == null){

            attribute.addFlashAttribute("invalid", "Save Data Success");
            return "redirect:../tunjangan";

        }else{

            model.addAttribute("payrollComponent", payrollComponent1);

        }

        return "setting/payroll/tunjangan/form";

    }

}
