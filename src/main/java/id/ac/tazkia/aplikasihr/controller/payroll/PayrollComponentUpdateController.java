package id.ac.tazkia.aplikasihr.controller.payroll;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeePayrollComponentDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeePayrollInfoDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.payroll.EmployeePayrollComponentHistoryDao;
import id.ac.tazkia.aplikasihr.dao.payroll.EmployeePayrollComponentHistoryDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollComponentDao;
import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollInfo;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistory;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistoryDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.export.ExportExcelNewUpdateEmployeePayrollComponentClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class PayrollComponentUpdateController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private PayrollComponentDao payrollComponentDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private EmployeePayrollComponentHistoryDao employeePayrollComponentHistoryDao;

    @Autowired
    private EmployeePayrollComponentHistoryDetailDao employeePayrollComponentHistoryDetailDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;


    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/payroll/update")
    public String updatePayrollComponent(Model model,
                                         Authentication authentication,
                                         @PageableDefault(size = 10) Pageable page){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("bulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("tahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listEmployeePayrollComponentHistory", employeePayrollComponentHistoryDao.findByStatusOrderByEffectiveDateDesc(StatusRecord.AKTIF, page));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listEmployeePayrollComponentHistory", employeePayrollComponentHistoryDao.findByStatusAndCompaniesIdInOrderByEffectiveDateDesc(StatusRecord.AKTIF,idDepartements, page));
        }
//        model.addAttribute("listPayrollComponent", payrollComponentDao.findByStatusOrderByJenisComponent(StatusRecord.AKTIF));

        model.addAttribute("payrollMenu", "active");
        return "payroll/update/list";

    }

    @GetMapping("/payroll/update/new")
    public String newUpdatePayrollComponent(Model model,
                                            @RequestParam(required = false) PayrollComponent payrollComponent,
                                            @RequestParam(required = false) String effectiveDate,
                                            @RequestParam(required = false) String endDate,
                                            Authentication authentication,
                                            RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("payrollMenu", "active");

        model.addAttribute("payrollComponentSelected", payrollComponent);
        model.addAttribute("listPayrollComponent", payrollComponentDao.findByStatusNotOrderByJenisComponent(StatusRecord.HAPUS));
        if (StringUtils.hasText(effectiveDate)) {
            String date = effectiveDate;
            String tahun = date.substring(6,10);
            String bulan = date.substring(0,2);
            String tanggal = date.substring(3,5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);
            model.addAttribute("effectiveDateString", effectiveDate);
            LocalDate localDate1 = LocalDate.parse(tanggalan, formatter);

            if (StringUtils.hasText(endDate)) {
                String date1 = endDate;
                String tahun1 = date1.substring(6, 10);
                String bulan1 = date1.substring(0, 2);
                String tanggal1 = date1.substring(3, 5);
                String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                localDate1 = LocalDate.parse(tanggalan1, formatter1);
                model.addAttribute("endDateString", endDate);

            }
            model.addAttribute("datefailed", null);
//            model.addAttribute("listEmployeePayrollComponentHistoryDetail", employeePayrollComponentHistoryDetailDao.findByStatusAndEmployeePayrollComponentHistoryPayrollComponentAndEmployeePayrollComponentHistoryEffectiveDateOrderByEmployeePayrollComponentHistory(StatusRecord.AKTIF, payrollComponent, localDate));

            if (payrollComponent != null){
//                model.addAttribute("listEmployeePayrollComponentHistoryDetail", employeePayrollComponentDao.listEmployeePayrollComponenHistoryDetailAll());
//            }else {
                UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

                if(role != null) {
                    model.addAttribute("listEmployeePayrollComponentHistoryDetail", employeePayrollComponentDao.listEmployeePayrollComponenHistoryDetail(payrollComponent.getId()));
                }else{
                    List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                    model.addAttribute("listEmployeePayrollComponentHistoryDetail", employeePayrollComponentDao.listEmployeePayrollComponenHistoryDetailCompanies(payrollComponent.getId(), idDepartements));
                }
            }
            return "payroll/update/form";
        }else{
            model.addAttribute("datefailed", "ada");
            return "payroll/update/form";
        }

    }


    @GetMapping("/payroll/update/excel")
    public void exportToExcel(@RequestParam(required = true) String payrollComponent,
                              Authentication authentication,
                              HttpServletResponse response) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        PayrollComponent payrollComponent1 = payrollComponentDao.findById(payrollComponent).get();
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename="+ payrollComponent1.getName()+"_"+ currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            List<ExportPayrollComponentToExcelDto> listxportPayrollComponentToExcelDto = employeePayrollComponentDao.listPayrollComponentExportExcel(payrollComponent);
            ExportExcelNewUpdateEmployeePayrollComponentClass excelExporter = new ExportExcelNewUpdateEmployeePayrollComponentClass(listxportPayrollComponentToExcelDto);
            excelExporter.export(response);
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            List<ExportPayrollComponentToExcelDto> listxportPayrollComponentToExcelDto = employeePayrollComponentDao.listPayrollComponentExportExcelCompanies(payrollComponent, idDepartements);
            ExportExcelNewUpdateEmployeePayrollComponentClass excelExporter = new ExportExcelNewUpdateEmployeePayrollComponentClass(listxportPayrollComponentToExcelDto);
            excelExporter.export(response);
        }



    }

    @PostMapping("/payroll/update/import")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       @RequestParam(required = true)PayrollComponent payrollComponent,
                                       @RequestParam(required = true)String effectiveDate,
                                       @RequestParam(required = true)String endDate,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        List<ExportPayrollComponentToExcelDto> exportPayrollComponentToExcelDtos = new ArrayList<ExportPayrollComponentToExcelDto>();
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String date = effectiveDate;
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String date1 = endDate;
        String tahun1 = date1.substring(6,10);
        String bulan1 = date1.substring(0,2);
        String tanggal1 = date1.substring(3,5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);

        System.out.println("History : RUN");

        EmployeePayrollComponentHistory employeePayrollComponentHistory1 = employeePayrollComponentHistoryDao.findByStatusAndPayrollComponentAndEffectiveDateAndEndDate(StatusRecord.AKTIF, payrollComponent, localDate, localDate1);



        if(employeePayrollComponentHistory1 == null) {

            System.out.println("History : Null");
            EmployeePayrollComponentHistory employeePayrollComponentHistory = new EmployeePayrollComponentHistory();
            employeePayrollComponentHistory.setTransactionId(currentDateTime);
            employeePayrollComponentHistory.setEffectiveDate(localDate);
            employeePayrollComponentHistory.setEndDate(localDate1);
            employeePayrollComponentHistory.setPayrollComponent(payrollComponent);
            employeePayrollComponentHistory.setTipe("ADJUSMENT");
            employeePayrollComponentHistory.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistory.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistory.setUserUpdate(user.getUsername());
            employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory);

            for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

                XSSFRow row = worksheet.getRow(i);

                BigDecimal nominal1 = new BigDecimal(row.getCell(3).getNumericCellValue());
                BigDecimal nominal2 = new BigDecimal(row.getCell(4).getNumericCellValue());

                System.out.println("EPC : RUN");

                EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponent(StatusRecord.HAPUS, employesDao.findByStatusAndId(StatusRecord.AKTIF,row.getCell(0).getStringCellValue()), payrollComponent);



                if (payrollComponent.getStatus()== StatusRecord.BASIC){

                    EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employesDao.findByStatusAndId(StatusRecord.AKTIF,row.getCell(0).getStringCellValue()));
                    if (employeePayrollInfo != null){
                        employeePayrollInfo.setBasicSalary(nominal2);
                        employeePayrollInfo.setDateUpdate(LocalDateTime.now());
                        employeePayrollInfo.setUserUpdate(user.getUsername());
                        employeePayrollInfoDao.save(employeePayrollInfo);
                    }else{
                        employeePayrollInfo = new EmployeePayrollInfo();
                        employeePayrollInfo.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,row.getCell(0).getStringCellValue()));
                        employeePayrollInfo.setBasicSalary(nominal2);
                        employeePayrollInfo.setDateUpdate(LocalDateTime.now());
                        employeePayrollInfo.setUserUpdate(user.getUsername());
                        employeePayrollInfoDao.save(employeePayrollInfo);
                    }

                }

                if (employeePayrollComponent == null){
                    System.out.println("EPC : Null");
                    EmployeePayrollComponent employeePayrollComponent1 = new EmployeePayrollComponent();
                    employeePayrollComponent1.setPayrollComponent(payrollComponent);
                    employeePayrollComponent1.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, row.getCell(0).getStringCellValue()));
                    BigDecimal nominal = new BigDecimal(row.getCell(3).getNumericCellValue());
                    employeePayrollComponent1.setNominal(nominal2);
                    employeePayrollComponent1.setJenisComponent("CUSTOM");
                    employeePayrollComponent1.setJenisNominal("NOMINAL");
                    employeePayrollComponent1.setStatus(payrollComponent.getStatus());
                    employeePayrollComponentDao.save(employeePayrollComponent1);

//                    if(nominal1.compareTo(nominal2) != 0){
                        EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent1);
                        employeePayrollComponentHistoryDetail.setCurrentAmount(nominal1);
                        employeePayrollComponentHistoryDetail.setNewAmount(nominal2);
                        employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                        employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                        employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                        employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);

//                    }

                }else{

//                    System.out.println("EPC :" + employeePayrollComponent.getEmployes().getId());
                    EmployeePayrollComponent employeePayrollComponent1 = employeePayrollComponent;
                    employeePayrollComponent1.setPayrollComponent(payrollComponent);
                    employeePayrollComponent1.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, row.getCell(0).getStringCellValue()));
                    BigDecimal nominal = new BigDecimal(row.getCell(4).getNumericCellValue());
                    employeePayrollComponent1.setNominal(nominal2);
                    employeePayrollComponent1.setJenisComponent("CUSTOM");
                    employeePayrollComponent1.setJenisNominal("NOMINAL");
                    employeePayrollComponent1.setStatus(payrollComponent.getStatus());
                    employeePayrollComponentDao.save(employeePayrollComponent1);

//                    if(nominal1.compareTo(nominal2) != 0){
                        EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                        employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent1);
                        employeePayrollComponentHistoryDetail.setCurrentAmount(nominal1);
                        employeePayrollComponentHistoryDetail.setNewAmount(nominal2);
                        employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                        employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                        employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                        employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);
//
//                    }
                }

            }

        }else{

            System.out.println("History :" + employeePayrollComponentHistory1.getId());


            EmployeePayrollComponentHistory employeePayrollComponentHistory = employeePayrollComponentHistory1;
            employeePayrollComponentHistory.setTransactionId(currentDateTime);
            employeePayrollComponentHistory.setEffectiveDate(localDate);
            employeePayrollComponentHistory.setEndDate(localDate1);
            employeePayrollComponentHistory.setPayrollComponent(payrollComponent);
            employeePayrollComponentHistory.setTipe("ADJUSMENT");
            employeePayrollComponentHistory.setStatus(StatusRecord.AKTIF);
            employeePayrollComponentHistory.setDateUpdate(LocalDateTime.now());
            employeePayrollComponentHistory.setUserUpdate(user.getUsername());
            employeePayrollComponentHistoryDao.save(employeePayrollComponentHistory);

            for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

                XSSFRow row = worksheet.getRow(i);

                BigDecimal nominal1 = new BigDecimal(row.getCell(3).getNumericCellValue());
                BigDecimal nominal2 = new BigDecimal(row.getCell(4).getNumericCellValue());

                System.out.println("EPC : RUN");
                    EmployeePayrollComponent employeePayrollComponent = employeePayrollComponentDao.findByStatusNotAndEmployesAndPayrollComponent(StatusRecord.HAPUS, employesDao.findByStatusAndId(StatusRecord.AKTIF, row.getCell(0).getStringCellValue()), payrollComponent);

//                System.out.println("EPC :" + employeePayrollComponent.getEmployes().getId());
                    if (payrollComponent.getStatus()== StatusRecord.BASIC){

                        EmployeePayrollInfo employeePayrollInfo = employeePayrollInfoDao.findByStatusAndEmployes(StatusRecord.AKTIF, employesDao.findByStatusAndId( StatusRecord.AKTIF,row.getCell(0).getStringCellValue()));
                        if (employeePayrollInfo != null){
                            employeePayrollInfo.setBasicSalary(nominal2);
                            employeePayrollInfo.setDateUpdate(LocalDateTime.now());
                            employeePayrollInfo.setUserUpdate(user.getUsername());
                            employeePayrollInfoDao.save(employeePayrollInfo);
                        }else{
                            employeePayrollInfo = new EmployeePayrollInfo();
                            employeePayrollInfo.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF,row.getCell(0).getStringCellValue()));
                            employeePayrollInfo.setBasicSalary(nominal2);
                            employeePayrollInfo.setDateUpdate(LocalDateTime.now());
                            employeePayrollInfo.setUserUpdate(user.getUsername());
                            employeePayrollInfoDao.save(employeePayrollInfo);
                        }

                    }

                    if (employeePayrollComponent == null){
                        EmployeePayrollComponent employeePayrollComponent1 = new EmployeePayrollComponent();
                        employeePayrollComponent1.setPayrollComponent(payrollComponent);
                        employeePayrollComponent1.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, row.getCell(0).getStringCellValue()));
                        BigDecimal nominal = new BigDecimal(row.getCell(3).getNumericCellValue());
                        employeePayrollComponent1.setNominal(nominal2);
                        employeePayrollComponent1.setJenisComponent("CUSTOM");
                        employeePayrollComponent1.setJenisNominal("NOMINAL");
                        employeePayrollComponent1.setStatus(payrollComponent.getStatus());
                        employeePayrollComponentDao.save(employeePayrollComponent1);

//                        if(nominal1.compareTo(nominal2) != 0){
                            EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
                            employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                            employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent1);
                            employeePayrollComponentHistoryDetail.setCurrentAmount(nominal1);
                            employeePayrollComponentHistoryDetail.setNewAmount(nominal2);
                            employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                            employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                            employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                            employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);

//                        }

                    }else{
                        EmployeePayrollComponent employeePayrollComponent1 = employeePayrollComponent;
                        employeePayrollComponent1.setPayrollComponent(payrollComponent);
                        employeePayrollComponent1.setEmployes(employesDao.findByStatusAndId(StatusRecord.AKTIF, row.getCell(0).getStringCellValue()));
                        BigDecimal nominal = new BigDecimal(row.getCell(4).getNumericCellValue());
                        employeePayrollComponent1.setNominal(nominal2);
                        employeePayrollComponent1.setJenisComponent("CUSTOM");
                        employeePayrollComponent1.setJenisNominal("NOMINAL");
                        employeePayrollComponent1.setStatus(payrollComponent.getStatus());
                        employeePayrollComponentDao.save(employeePayrollComponent1);

//                        if(nominal1.compareTo(nominal2) != 0){

                            System.out.println("EPCH : RUN");
                            EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail1 = employeePayrollComponentHistoryDetailDao.findByStatusAndEmployeePayrollComponentHistoryAndEmployeePayrollComponent(StatusRecord.AKTIF, employeePayrollComponentHistory, employeePayrollComponent);
//                            System.out.println("EPCH :" + employeePayrollComponentHistoryDetail1.getEmployeePayrollComponent().getEmployes().getId());
                            if(employeePayrollComponentHistoryDetail1 == null) {
                                EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = new EmployeePayrollComponentHistoryDetail();
                                employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                                employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent1);
                                employeePayrollComponentHistoryDetail.setCurrentAmount(nominal1);
                                employeePayrollComponentHistoryDetail.setNewAmount(nominal2);
                                employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                                employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                                employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                                employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);
                            }else{
                                EmployeePayrollComponentHistoryDetail employeePayrollComponentHistoryDetail = employeePayrollComponentHistoryDetail1;
                                employeePayrollComponentHistoryDetail.setEmployeePayrollComponentHistory(employeePayrollComponentHistory);
                                employeePayrollComponentHistoryDetail.setEmployeePayrollComponent(employeePayrollComponent1);
                                employeePayrollComponentHistoryDetail.setCurrentAmount(nominal1);
                                employeePayrollComponentHistoryDetail.setNewAmount(nominal2);
                                employeePayrollComponentHistoryDetail.setStatus(StatusRecord.AKTIF);
                                employeePayrollComponentHistoryDetail.setDateUpdate(LocalDateTime.now());
                                employeePayrollComponentHistory.getEmployeePayrollComponentHistoryDetails().add(employeePayrollComponentHistoryDetail);
                                employeePayrollComponentHistoryDetailDao.save(employeePayrollComponentHistoryDetail);
                            }
//
//                        }
                    }
            }

        }




        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../update/new?payrollComponent="+payrollComponent.getId()+"&effectiveDate="+effectiveDate+"&endDate="+endDate;
    }

}
