package id.ac.tazkia.aplikasihr.controller.reports;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.OverTimeRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class OvertimeReportController {

    @Autowired
    private OverTimeRequestDao overTimeRequestDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @GetMapping("/report/overtime/sumary")
    public String reportOvertimeSummary(Model model,
                                        @RequestParam(required = false) String tahun,
                                        @RequestParam(required = false) String bulan,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(tahun != null && bulan != null){

            Integer bulanAngka = new Integer(bulan);
            Integer bulanAngka2 = bulanAngka - 1;
            String bulan2 = bulanAngka2.toString();
            Integer tahunAngka = new Integer(tahun);
            Integer tahunAngka2 = tahunAngka - 1;
            String tahun2 = tahun;
            if (bulan2.length() == 1) {
                bulan2 = '0' + bulan2;
            }
            if (bulan.equals("01")) {
                bulan = "01";
                bulan2 = "12";
                tahun2 = tahunAngka2.toString();
            }

            PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

            String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
            if (tanggalMulaiPeriode.length() == 1){
                tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
            }
            String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
            if (tanggalSelesaiPeriode.length() == 1){
                tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
            }

            String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

            String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
            UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

            if(role != null) {
                model.addAttribute("listOvertimeSumary", overTimeRequestDao.summaryDataOvertimeFlat(localDate, localDate2));
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
                model.addAttribute("listOvertimeSumary", overTimeRequestDao.summaryDataOvertimeCompanyFlat(localDate, localDate2, idDepartements));
            }
        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));


        return "reports/overtime/sumary";
    }

}
