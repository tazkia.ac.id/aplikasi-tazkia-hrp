package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeRiwayatPekerjaanDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeRiwayatPekerjaan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class EmployesRiwayatPekerjaanController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeRiwayatPekerjaanDao employeeRiwayatPekerjaanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/masterdata/employes/riwayat_kerja")
    public String listEmployeeRiwayatPekerjaan(Model model,
                                               @RequestParam(required = true)Employes employes,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listEmployeeRiwayatPekerjaan", employeeRiwayatPekerjaanDao.findByStatusAndEmployesOrderByTahunMasuk(StatusRecord.AKTIF, employes));

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeWorkHistory", "active");
        return "masterdata/employes/pekerjaan/list";

    }

    @GetMapping("/masterdata/employes/riwayat_kerja/new")
    public String newEmployeeRiwayatPekerjaan(Model model,
                                              @RequestParam(required = true) Employes employes,
                                              Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("employeeRiwayatPekerjaan", new EmployeeRiwayatPekerjaan());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeWorkHistory", "active");
        return "masterdata/employes/pekerjaan/form";

    }

    @GetMapping("/masterdata/employes/riwayat_kerja/edit")
    public String editEmployeeRiwayatPekerjaan(Model model,
                                               @RequestParam(required = true) EmployeeRiwayatPekerjaan employeeRiwayatPekerjaan,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employeeRiwayatPekerjaan.getEmployes());
        model.addAttribute("employeeRiwayatPekerjaan", employeeRiwayatPekerjaan);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeWorkHistory", "active");
        return "masterdata/employes/pekerjaan/form";

    }

    @PostMapping("/masterdata/employes/riwayat_kerja/save")
    public String saveEmployeeRiwayatPekerjaan(@ModelAttribute @Valid EmployeeRiwayatPekerjaan employeeRiwayatPekerjaan,
                                               @RequestParam(required = true) Employes employes,
                                               Authentication authentication,
                                               RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeRiwayatPekerjaan.setUserUpdate(user.getUsername());
        employeeRiwayatPekerjaan.setStatus(StatusRecord.AKTIF);
        employeeRiwayatPekerjaan.setDateUpdate(LocalDateTime.now());

        employeeRiwayatPekerjaanDao.save(employeeRiwayatPekerjaan);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../riwayat_kerja?employes="+ employes.getId();

    }


    @PostMapping("/masterdata/employes/riwayat_kerja/delete")
    public String deleteEmployeeRiwayatPekerjaan(@ModelAttribute @Valid EmployeeRiwayatPekerjaan employeeRiwayatPekerjaan,
                                               Authentication authentication,
                                               RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeRiwayatPekerjaan.setUserUpdate(user.getUsername());
        employeeRiwayatPekerjaan.setStatus(StatusRecord.HAPUS);
        employeeRiwayatPekerjaan.setDateUpdate(LocalDateTime.now());

        employeeRiwayatPekerjaanDao.save(employeeRiwayatPekerjaan);
        attribute.addFlashAttribute("deleted", "Save Data Success");
        return "redirect:../riwayat_kerja?employes="+ employeeRiwayatPekerjaan.getEmployes().getId();

    }


}
