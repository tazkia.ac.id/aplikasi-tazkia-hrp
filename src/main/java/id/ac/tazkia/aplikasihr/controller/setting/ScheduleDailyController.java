package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ScheduleDailyController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;


    @GetMapping("/setting/schedule/daily")
    public String scheduleDailyList(Model model,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);



        model.addAttribute("menuscheduledaily", "active");

        return "setting/schedule/daily/list";
    }

}
