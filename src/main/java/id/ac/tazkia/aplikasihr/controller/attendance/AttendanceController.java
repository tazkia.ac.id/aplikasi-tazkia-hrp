package id.ac.tazkia.aplikasihr.controller.attendance;

import com.google.api.client.util.DateTime;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceHistoryDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dto.export.ExportPayrollComponentToExcelDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.Attendance;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceHistory;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import id.ac.tazkia.aplikasihr.export.ExportExcelNewUpdateEmployeePayrollComponentClass;
import id.ac.tazkia.aplikasihr.export.attendance.ExportAttendanceFormatClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
public class AttendanceController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private AttendanceHistoryDao attendanceHistoryDao;

    @GetMapping("/attendance")
    public String dashboardAttendance(Model model,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("attendanceMenu", "active");
        return "attendance/home";
    }

    @GetMapping("/attendance/impor")
    public String imporAttendance(Model model,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);



        model.addAttribute("attendanceMenu", "active");
        return "attendance/form_impor";
    }

    @GetMapping("/attendance/export/format")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=format_attendance.xlsx";
        response.setHeader(headerKey, headerValue);

        ExportAttendanceFormatClass excelExporter = new ExportAttendanceFormatClass();

        excelExporter.export(response);
    }

    @PostMapping("/attendance/import/process")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

       User user = currentUserService.currentUser(authentication);
        
        Employes employess = employesDao.findByUser(user);
        
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        
        AttendanceHistory attendanceHistory = new AttendanceHistory();
        attendanceHistory.setJenis(StatusRecord.IMPOR);
        attendanceHistory.setStatus(StatusRecord.AKTIF);
        attendanceHistory.setTanggal(LocalDateTime.now());
        attendanceHistory.setUserUpdate(user.getUsername());
        attendanceHistoryDao.save(attendanceHistory);
        
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            System.out.println("row :" + i);
            XSSFRow row = worksheet.getRow(i);
            
            if (row != null && row.getCell(0) != null && row.getCell(1) != null && row.getCell(3) != null && row.getCell(4) != null) {
                String id = row.getCell(0).getStringCellValue();
                String ada = row.getCell(0).getStringCellValue().isEmpty() ? "0" : row.getCell(0).getStringCellValue();

                    Integer satu = new Double(ada).intValue();

                    String nama = new String(row.getCell(1).getStringCellValue());

                    Employes employes = employesDao.findByIdAbsen(satu);
                    String detik = "00";

                    if (employes != null) {

                        if (row.getCell(3).getStringCellValue().length() > 2) {

                            String tanggal = row.getCell(2).getStringCellValue();
                            String hari = tanggal.substring(0, 2);
                            String bulan = tanggal.substring(3, 5);
                            String tahun = tanggal.substring(6, 10);
                            String tanggalan = tahun + '-' + bulan + '-' + hari;
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            LocalDate localDate = LocalDate.parse(tanggalan, formatter);

                            String dateString = bulan + '/' + hari + '/' + tahun;

                            String am = " AM";
                            String pm = " PM";

                            String in = row.getCell(3).getStringCellValue();
                            String jam = in.substring(0, 2);
                            String menit = in.substring(3, 5);


//                    Integer jamb = new Integer(jam);
//                    String waktuMasuk = "00:00 AM";
//                    if (jamb > 12){
//                        Integer jamc = jamb - 12;
//                        jam = jamc.toString();
//                        if (jam.length() == 1){
//                            jam = '0' + jam;
//                        }
//                        waktuMasuk = jam +':'+ menit + ':' + detik;
//                    }else{
//                        waktuMasuk = jam +':'+ menit + ':' + detik;
//                    }
                            String waktuMasuk = in + ':' + detik;

                            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
                            LocalTime timeIn = LocalTime.parse(waktuMasuk, formatter1).plusHours(7);

                            if (row.getCell(4).getStringCellValue().length() > 2) {
                                String out = row.getCell(4).getStringCellValue();
                                String jamo = out.substring(0, 2);
                                String menito = out.substring(3, 5);
                                Integer jambo = new Integer(jamo);
                                String waktuKeluar = "00:00 AM";
//                        if (jambo > 12){
//                            Integer jamco = jambo - 12;
//                            jamo = jamco.toString();
//                            if (jamo.length() == 1){
//                                jamo = '0' + jamo;
//                            }
//                            waktuKeluar = jamo +':'+ menito + ':' + detik;
//                        }else{
//                            waktuKeluar = jamo +':'+ menito + ':' + detik;
//                        }

                                waktuKeluar = out + ':' + detik;
                                DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
                                LocalTime timeOut = LocalTime.parse(waktuKeluar, formatter2).plusHours(7);

                                Attendance attendance = attendanceDao.findByStatusAndEmployesAndDateAndWaktuMasukAndWaktuKeluar(StatusRecord.AKTIF, employes, localDate, timeIn, timeOut);

                                if (attendance == null) {
//                                    List<Attendance> attendance2 = attendanceDao.findByStatusAndEmployesAndDateOrderById(StatusRecord.AKTIF, employes, localDate);
//                                    if(attendance2 != null){
//                                        for(Attendance attendance1 : attendance2){
//                                            attendanceDao.delete(attendance1);
//                                        }
//                                    }else {
                                        Attendance attendance1 = new Attendance();
                                        attendance1.setAttendanceHistory(attendanceHistory);
                                        attendance1.setDate(localDate);
                                        attendance1.setEmployes(employes);
                                        attendance1.setWaktuMasuk(timeIn);
                                        attendance1.setWaktuKeluar(timeOut);
                                        attendanceHistory.getAttendances().add(attendance1);
                                        attendanceDao.save(attendance1);
//                                    }
                                }else{
                                    attendance.setWaktuMasuk(timeIn);
                                    attendance.setWaktuKeluar(timeOut);
                                    attendanceDao.save(attendance);
                                }

                            } else {

                                Attendance attendance = attendanceDao.findByStatusAndEmployesAndDateAndWaktuMasuk(StatusRecord.AKTIF, employes, localDate, timeIn);

                                if (attendance == null) {

                                    Attendance attendance1 = new Attendance();
                                    attendance1.setAttendanceHistory(attendanceHistory);
                                    attendance1.setDate(localDate);
                                    attendance1.setEmployes(employes);
                                    attendance1.setWaktuMasuk(timeIn);
                                    attendanceHistory.getAttendances().add(attendance1);
                                    attendanceDao.save(attendance1);

                                }else{
                                    attendance.setWaktuMasuk(timeIn);
                                    attendanceDao.save(attendance);
                                }
                            }

                        } else {
                            if (row.getCell(4).getStringCellValue().length() > 2) {

                                String tanggal = row.getCell(2).getStringCellValue();
                                String hari = tanggal.substring(0, 2);
                                String bulan = tanggal.substring(3, 5);
                                String tahun = tanggal.substring(6, 10);
                                String tanggalan = tahun + '-' + bulan + '-' + hari;
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                                LocalDate localDate = LocalDate.parse(tanggalan, formatter);

                                String dateString = bulan + '/' + hari + '/' + tahun;

                                String am = " AM";
                                String pm = " PM";

                                String out = row.getCell(4).getStringCellValue();
                                String jamo = out.substring(0, 2);
                                String menito = out.substring(3, 5);
                                Integer jambo = new Integer(jamo);
                                String waktuKeluar = "00:00 AM";
//                        if (jambo > 12){
//                            Integer jamco = jambo - 12;
//                            jamo = jamco.toString();
//                            if (jamo.length() == 1){
//                                jamo = '0' + jamo;
//                            }
//                            waktuKeluar = jamo +':'+ menito + ':' + detik;
//                        }else{
//                            waktuKeluar = jamo +':'+ menito + ':' + detik;
//                        }

                                waktuKeluar = out + ':' + detik;
                                DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
                                LocalTime timeOut = LocalTime.parse(waktuKeluar, formatter2).plusHours(7);

                                Attendance attendance = attendanceDao.findByStatusAndEmployesAndDateAndWaktuKeluar(StatusRecord.AKTIF, employes, localDate, timeOut);

                                if (attendance == null) {
                                    Attendance attendance1 = new Attendance();
                                    attendance1.setAttendanceHistory(attendanceHistory);
                                    attendance1.setDate(localDate);
                                    attendance1.setEmployes(employes);
                                    attendance1.setWaktuKeluar(timeOut);
                                    attendanceHistory.getAttendances().add(attendance1);
                                    attendanceDao.save(attendance1);
                                }else{
                                    attendance.setWaktuKeluar(timeOut);
                                    attendanceDao.save(attendance);
                                }

                            }
                        }

                    }
                }
            // }


        }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../impor";

    }

}
