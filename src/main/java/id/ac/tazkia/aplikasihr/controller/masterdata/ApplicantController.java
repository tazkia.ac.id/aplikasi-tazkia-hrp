package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.AgamaDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantCertificateDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantFileSupportDao;
import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantIdentityFileDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestSupportFileDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.recruitment.*;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
public class ApplicantController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicantController.class);

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ApplicantDao applicantDao;

    @Autowired
    private ApplicantCertificateDao certificateDao;

    @Autowired
    private ApplicantIdentityFileDao identityFileDao;

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private RecruitmentRequestSupportFileDao supportFileDao;

    @Autowired
    private ApplicantFileSupportDao applicantFileSupportDao;

    @Autowired
    @Value("${upload.fotoapplicant}")
    private String uploadFoto;

    @Autowired
    @Value("${upload.certificateapplicant}")
    private String uploadCertificate;

    @Autowired
    @Value("${upload.identityapplicant}")
    private String uploadIdentity;

    @Autowired
    @Value("${upload.supportfileapplicant}")
    private String uploadSupportFile;

    // Edit Profile Applicant

    @GetMapping("/applicant/detail")
    public String detailApplicant(Model model, @RequestParam Applicant applicant){

        model.addAttribute("employess", applicant);
        model.addAttribute("listReligion", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));

        model.addAttribute("menuPelamar", "menuPelamar");

        return "applicant/detail";

    }

    @PostMapping("/applicant/detail/save")
    public String saveApplicant(@RequestParam Applicant applicant, @RequestParam("foto") MultipartFile file, RedirectAttributes attributes) throws IOException {

        applicant.setDateUpdate(LocalDateTime.now());
        applicant.setUserUpdate(applicant.getEmailActive());

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i+1);
        }

        String idFile = UUID.randomUUID().toString();
        applicant.setFileFoto(idFile + '.' + extension);
        new File(uploadFoto).mkdirs();
        File tujuan = new File(uploadFoto + File.separator + applicant.getFileFoto());
        file.transferTo(tujuan);

        attributes.addFlashAttribute("success", "save data berhasil!");
        applicantDao.save(applicant);

        return "redirect:../detail?applicant="+applicant.getId();
    }

    @GetMapping("/applicant/{applicant}/foto/")
    public ResponseEntity<byte[]> applicantFoto(@PathVariable Applicant applicant){
        String lokasiFile = uploadFoto + File.separator + applicant.getFileFoto();
        LOGGER.debug("Lokasi foto : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (applicant.getFileFoto().toLowerCase().endsWith("jpeg") || applicant.getFileFoto().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (applicant.getFileFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (applicant.getFileFoto().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers,HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Certificate Applicant

    @GetMapping("/applicant/certificate")
    public String certificateApplicant(Model model, @RequestParam Applicant applicant){
        model.addAttribute("employess", applicant);


        ApplicantCertificate certificate = certificateDao.findByApplicantAndStatus(applicant, StatusRecord.AKTIF);
        model.addAttribute("certificate", certificate);

        model.addAttribute("menuPelamar", "menuPelamar");

        return "applicant/certificate/list";
    }

    @GetMapping("/applicant/certificate/new")
    public String certificateNew(Model model, @RequestParam Applicant applicant){

        model.addAttribute("employess", applicant);

        model.addAttribute("certificate", new ApplicantCertificate());

        model.addAttribute("menuPelamar", "menuPelamar");

        return "applicant/certificate/form";
    }

    @PostMapping("/applicant/certificate/save")
    public String saveCertificate(@ModelAttribute @Valid ApplicantCertificate certificate, @RequestParam("certificate") MultipartFile file,
                                  @RequestParam Applicant applicant, RedirectAttributes attributes, Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);

        certificate.setUserUpdate(user.getUsername());
        certificate.setDateUpdate(LocalDateTime.now());

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long size = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p){
            extension = namaAsli.substring(i+1);
        }

        String idFile = UUID.randomUUID().toString();
        String nama = certificate.getApplicant().getIdentityNumber();
        String lokasiUpload = uploadCertificate + File.separator + nama;
        certificate.setCertificateFile(idFile + '.' + extension);
        new File(lokasiUpload).mkdirs();
        File tujuan = new File(lokasiUpload + File.separator + certificate.getCertificateFile());
        file.transferTo(tujuan);

        certificate.setApplicant(applicant);
        certificateDao.save(certificate);

        attributes.addFlashAttribute("success", "Save Data Berhasil!");
        return "redirect:../certificate?applicant="+certificate.getApplicant().getId();
    }


    @GetMapping("/applicant/{certificate}/certificate/")
    public ResponseEntity<byte[]> applicantCertficate(@PathVariable ApplicantCertificate certificate){
        String lokasi = uploadCertificate + File.separator + certificate.getApplicant().getIdentityNumber();
        String lokasiFile = lokasi + File.separator + certificate.getCertificateFile();
        LOGGER.debug("Lokasi foto : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (certificate.getCertificateFile().toLowerCase().endsWith("jpeg") || certificate.getCertificateFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (certificate.getCertificateFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (certificate.getCertificateFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers,HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // file identitas applicant

    @GetMapping("/applicant/identity")
    public String identityApplicant(Model model, @RequestParam Applicant applicant){

        model.addAttribute("employess", applicant);

        ApplicantIdentityFile identityFile = identityFileDao.findByApplicantAndStatus(applicant, StatusRecord.AKTIF);
        String fileKtp = "default.jpg";
        String fileKk = "default.jpg";
        String fileNpwp = "default.jpg";

        if (identityFile == null) {
            model.addAttribute("identityFile", new ApplicantIdentityFile());
        }else{
            model.addAttribute("identityFile", identityFile);
            if (identityFile.getFileKtp().isEmpty()) {
                fileKtp = "default.jpg";
            }else {
                fileKtp = identityFile.getFileKtp();
            }

            if (identityFile.getFileKk().isEmpty()) {
                fileKk = "default.jpg";
            }else{
                fileKk = identityFile.getFileKk();
            }

            if (identityFile.getFileNpwp().isEmpty()) {
                fileNpwp = "dafault.jpg";
            }else{
                fileNpwp = identityFile.getFileNpwp();
            }
        }

        model.addAttribute("fileKtp", fileKtp);
        model.addAttribute("fileKk", fileKk);
        model.addAttribute("fileNpwp", fileNpwp);

        model.addAttribute("menuPelamar", "menuPelamar");

        return "applicant/identity/list";
    }

    @PostMapping("/applicant/identity/upload")
    public String identityUpload(@ModelAttribute @Valid ApplicantIdentityFile identityFile, @RequestParam Applicant applicant,
                                 @RequestParam("ktp") MultipartFile ktp, @RequestParam("kk") MultipartFile kk,
                                 @RequestParam("npwp") MultipartFile npwp, RedirectAttributes attributes) throws IOException {

        String lokasi = uploadIdentity + File.separator + applicant.getIdentityNumber();

        String namaAsliKtp = ktp.getOriginalFilename();
        Long sizeKtp = ktp.getSize();

        String namaAsliKk = kk.getOriginalFilename();
        Long sizeKk = kk.getSize();

        String namaAsliNpwp = npwp.getOriginalFilename();
        Long sizeNpwp = npwp.getSize();

        LOGGER.debug("Size file ktp : {}", sizeKtp);
        LOGGER.debug("Size file kk : {}", sizeKk);
        LOGGER.debug("Size file npwp : {}", sizeNpwp);

        if (sizeKtp > 2000000) {
            attributes.addFlashAttribute("ktp", "File Ktp Terlalu Besar");
            return "redirect:../identity?applicant="+applicant.getId();
        }

        if (sizeKk > 2000000) {
            attributes.addFlashAttribute("kk", "File KK Terlalu Besar");
            return "redirect:../identity?applicant="+applicant.getId();
        }

        if (sizeNpwp > 2000000) {
            attributes.addFlashAttribute("npwp", "File NPWP Terlalu Besar");
            return "redirect:../identity?applicant="+applicant.getId();
        }

        String extensionKtp = "";
        String extensionKk = "";
        String extensionNpwp = "";

        int iktp = namaAsliKtp.lastIndexOf('.');
        int pktp = Math.max(namaAsliKtp.lastIndexOf('/'), namaAsliKtp.lastIndexOf('\\'));
        if (iktp > pktp) {
            extensionKtp = namaAsliKtp.substring(iktp+1);
        }

        int ikk = namaAsliKk.lastIndexOf('.');
        int pkk = Math.max(namaAsliKk.lastIndexOf('/'), namaAsliKk.lastIndexOf('\\'));
        if (ikk > pkk) {
            extensionKk = namaAsliKk.substring(ikk+1);
        }

        int inpwp = namaAsliNpwp.lastIndexOf('.');
        int pnpwp = Math.max(namaAsliNpwp.lastIndexOf('/'), namaAsliNpwp.lastIndexOf('\\'));
        if (inpwp > pnpwp) {
            extensionNpwp = namaAsliNpwp.substring(inpwp+1);
        }

        String idFileKtp = UUID.randomUUID().toString();
        String idFileKk = UUID.randomUUID().toString();
        String idFileNpwp = UUID.randomUUID().toString();

        identityFile.setFileKtp(idFileKtp + '.' + extensionKtp);
        identityFile.setFileKk(idFileKk + '.' + extensionKk);
        identityFile.setFileNpwp(idFileNpwp + '.' + extensionNpwp);

        new File(lokasi).mkdirs();
        File tujuanKtp = new File(lokasi + File.separator + identityFile.getFileKtp());
        File tujuanKk = new File(lokasi + File.separator + identityFile.getFileKk());
        File tujuanNpwp = new File(lokasi + File.separator + identityFile.getFileNpwp());
        ktp.transferTo(tujuanKtp);
        kk.transferTo(tujuanKk);
        npwp.transferTo(tujuanNpwp);

        attributes.addFlashAttribute("success", "Save Data Success!");
        identityFile.setApplicant(applicant);
        identityFileDao.save(identityFile);

        return "redirect:../identity?applicant="+applicant.getId();
    }

    @GetMapping("/applicant/{identityFile}/identity/ktp/")
    public ResponseEntity<byte[]> applicantIdentityKtp(@PathVariable ApplicantIdentityFile identityFile){
        String lokasi = uploadIdentity + File.separator + identityFile.getApplicant().getIdentityNumber();
        String lokasiUpload = lokasi + File.separator + identityFile.getFileKtp();
        LOGGER.debug("Lokasi File : {}", lokasiUpload);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (identityFile.getFileKtp().toLowerCase().endsWith("jpeg") || identityFile.getFileKtp().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (identityFile.getFileKtp().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (identityFile.getFileKtp().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }

            byte[] dataKtp = Files.readAllBytes(Paths.get(lokasiUpload));
            return new ResponseEntity<byte[]>(dataKtp, headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/applicant/{identityFile}/identity/kk/")
    public ResponseEntity<byte[]> applicantIdentityKk(@PathVariable ApplicantIdentityFile identityFile){
        String lokasi = uploadIdentity + File.separator + identityFile.getApplicant().getIdentityNumber();
        String lokasiUpload = lokasi + File.separator + identityFile.getFileKk();
        LOGGER.debug("Lokasi File : {}", lokasiUpload);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (identityFile.getFileKk().toLowerCase().endsWith("jpeg") || identityFile.getFileKk().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (identityFile.getFileKk().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (identityFile.getFileKk().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }

            byte[] dataKtp = Files.readAllBytes(Paths.get(lokasiUpload));
            return new ResponseEntity<byte[]>(dataKtp, headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/applicant/{identityFile}/identity/npwp/")
    public ResponseEntity<byte[]> applicantIdentityNpwp(@PathVariable ApplicantIdentityFile identityFile){
        String lokasi = uploadIdentity + File.separator + identityFile.getApplicant().getIdentityNumber();
        String lokasiUpload = lokasi + File.separator + identityFile.getFileNpwp();
        LOGGER.debug("Lokasi File : {}", lokasiUpload);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (identityFile.getFileNpwp().toLowerCase().endsWith("jpeg") || identityFile.getFileNpwp().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (identityFile.getFileNpwp().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (identityFile.getFileNpwp().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }

            byte[] dataKtp = Files.readAllBytes(Paths.get(lokasiUpload));
            return new ResponseEntity<byte[]>(dataKtp, headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/applicant/identity/edit")
    public String editIdentity(Model model, @RequestParam String id, @RequestParam String kategori, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Applicant applicant = applicantDao.findByUser(user);
        model.addAttribute("employess", applicant);

        ApplicantIdentityFile identityFile = identityFileDao.findById(id).get();
        model.addAttribute("identityFile", identityFile);
        model.addAttribute("kategori", kategori);


        return "applicant/identity/form";
    }

    @PostMapping("/applicant/identity/save")
    public String editIdentity(@RequestParam ApplicantIdentityFile identityFile, @RequestParam("identity") MultipartFile file,
                               @RequestParam String kategori, RedirectAttributes attributes) throws IOException {

        String lokasi = uploadIdentity + File.separator + identityFile.getApplicant().getIdentityNumber();

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long size = file.getSize();

        if (size > 2000000){
            if (kategori.equals("Ktp")) {
                attributes.addFlashAttribute("ktp", "file too large");
                return "redirect:../identity?applicant="+identityFile.getApplicant().getId();
            } else if (kategori.equals("Kk")) {
                attributes.addFlashAttribute("kk", "file too large");
                return "redirect:../identity?applicant="+identityFile.getApplicant().getId();
            }else{
                attributes.addFlashAttribute("npwp", "file too large");
                return "redirect:../identity?applicant="+identityFile.getApplicant().getId();
            }
        }

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i+1);
        }

        String idFile = UUID.randomUUID().toString();
        if (kategori.equals("Ktp")) {
            identityFile.setFileKtp(idFile + '.' + extension);
        } else if (kategori.equals("Kk")) {
            identityFile.setFileKk(idFile + '.' + extension);
        }else{
            identityFile.setFileNpwp(idFile + '.' + extension);
        }

        new File(lokasi).mkdirs();
        if (kategori.equals("Ktp")) {
            File tujuan = new File(lokasi + File.separator + identityFile.getFileKtp());
            file.transferTo(tujuan);
        } else if (kategori.equals("Kk")) {
            File tujuan = new File(lokasi + File.separator + identityFile.getFileKk());
            file.transferTo(tujuan);
        }else{
            File tujuan = new File(lokasi + File.separator + identityFile.getFileNpwp());
            file.transferTo(tujuan);
        }

        identityFileDao.save(identityFile);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../identity?applicant="+identityFile.getApplicant().getId();
    }

    // file pendukung profile

    @GetMapping("/applicant/supportFile")
    public String supportFile(Model model, @RequestParam Applicant applicant, @RequestParam RecruitmentRequest request){

        model.addAttribute("employess", applicant);

        List<RecruitmentFileSupport> supportFile = supportFileDao.findByRequestAndStatus(request, StatusRecord.AKTIF);

        List<ApplicantFileSupport> fileSupport = applicantFileSupportDao.findBySupportRequestAndApplicantAndStatus(request, applicant, StatusRecord.AKTIF);

        model.addAttribute("fileSupportApplicant", fileSupport);

        model.addAttribute("supportFileNeeded", supportFile);

        return "applicant/supportFile/list";
    }

    @GetMapping("/applicant/supportFile/new")
    public String newSupportFile(Model model, @RequestParam RecruitmentFileSupport support, @RequestParam(required = false) String id,
                                 Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Applicant applicant = applicantDao.findByUser(user);

        model.addAttribute("supportFile", new ApplicantFileSupport());
        model.addAttribute("employess", applicant);
        model.addAttribute("support", support);

        if (id != null && !id.isEmpty()){
            ApplicantFileSupport applicantFileSupport = applicantFileSupportDao.findById(id).get();
            if (applicantFileSupport != null){
                model.addAttribute("supportFile", applicantFileSupport);
            }
        }

        return "edit";
    }

    @PostMapping("/applicant/supportFile/save")
    public String saveSupportFile(@ModelAttribute @Valid ApplicantFileSupport applicantFileSupport, @RequestParam("supportFile") MultipartFile file, Authentication authentication) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Applicant applicant = applicantDao.findByUser(user);

        String lokasi = uploadSupportFile + File.separator + applicantFileSupport.getApplicant().getIdentityNumber();

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long size = file.getSize();

        LOGGER.debug("size gambar : {}", size);

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p){
            extension = namaAsli.substring(i+1);
        }

        String idFile = UUID.randomUUID().toString();
        applicantFileSupport.setFile(idFile + '.' + extension);
        new File(lokasi).mkdirs();
        File tujuan = new File(lokasi + File.separator + applicantFileSupport.getFile());
        file.transferTo(tujuan);

        applicantFileSupport.setApplicant(applicant);
        applicantFileSupportDao.save(applicantFileSupport);

        return "redirect:../supportFile?applicant="+applicantFileSupport.getApplicant().getId()+"&request="+applicantFileSupport.getSupport().getRequest().getId();
    }

    @GetMapping("/applicant/{supportFile}/supportFile/")
    public ResponseEntity<byte[]> applicantSupportFile(@PathVariable ApplicantFileSupport supportFile){
        String lokasi = uploadSupportFile + File.separator + supportFile.getApplicant().getIdentityNumber();
        String lokasiFile = lokasi + File.separator + supportFile.getFile();
        LOGGER.debug("Lokasi file : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (supportFile.getFile().toLowerCase().endsWith("jpeg") || supportFile.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (supportFile.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (supportFile.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}