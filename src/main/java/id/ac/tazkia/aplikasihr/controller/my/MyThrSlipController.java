package id.ac.tazkia.aplikasihr.controller.my;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrRunDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class MyThrSlipController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private ThrRunDao thrRunDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @GetMapping("/my/thrSlip")
    public String myThrSlip(Model model,
                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
        model.addAttribute("listThrRun", thrRunDao.findByStatusAndEmployesOrderByTahun(StatusRecord.AKTIF, employess));
        model.addAttribute("home","active");
        model.addAttribute("mythrslip","active");
        return "my/thr/list";

    }

    @GetMapping("/my/thr/view")
    public String myThrView(Model model,
                            @RequestParam(required = true) String bulan,
                            @RequestParam(required = true) String tahun,
                            Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Integer bulanAngka = new Integer(bulan);
        Integer bulanAngka2 = bulanAngka - 1;
        String bulan2 = bulanAngka2.toString();
        Integer tahunAngka = new Integer(tahun);
        Integer tahunAngka2 = tahunAngka - 1;
        String tahun2 = tahun;
        if (bulan2.length() == 1) {
            bulan2 = '0' + bulan2;
        }
        if (bulan.equals("01")) {
            bulan = "01";
            bulan2 = "12";
            tahun2 = tahunAngka2.toString();
        }

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        String tanggalMulaiPeriode = payrollPeriode.getDariTanggal().toString();
        if (tanggalMulaiPeriode.length() == 1){
            tanggalMulaiPeriode = '0' + tanggalMulaiPeriode;
        }
        String tanggalSelesaiPeriode = payrollPeriode.getSampaiTanggal().toString();
        if (tanggalSelesaiPeriode.length() == 1){
            tanggalSelesaiPeriode = '0' + tanggalSelesaiPeriode;
        }

        String tanggalMulai = tahun2 + '-' + bulan2 + '-' + tanggalMulaiPeriode;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);

        String tanggalSelesai = tahun + '-' + bulan + '-' + tanggalSelesaiPeriode;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalSelesai, formatter1);

        model.addAttribute("paySlip", thrRunDao.findByStatusAndTahunAndBulanAndEmployesId(StatusRecord.AKTIF, tahun, bulan, employess.getId()));
//        model.addAttribute("paySipDetail", payrollRunDetailDao.findByStatusAndEmployesAndBulanAndTahunAndNominalAfter(StatusRecord.AKTIF, employes, bulan, tahun, BigDecimal.ZERO));
        model.addAttribute("employes", employess);
        model.addAttribute("companies", employess.getCompanies());
        model.addAttribute("date", LocalDate.now());
        model.addAttribute("namaBulan", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("position", employeeJobPositionDao.employeeJobPositonAda(employess.getId(), localDate, localDate2));
        model.addAttribute("home","active");
        model.addAttribute("mythrslip","active");
        return "my/thr/view";
    }
}
