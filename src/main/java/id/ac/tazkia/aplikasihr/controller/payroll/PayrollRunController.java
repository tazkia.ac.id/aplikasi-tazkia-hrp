package id.ac.tazkia.aplikasihr.controller.payroll;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceDao;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceRunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunDetailDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunProcessDao;
import id.ac.tazkia.aplikasihr.dao.payroll.PayrollRunProcessDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.payroll.BayarLoanDto;
import id.ac.tazkia.aplikasihr.dto.payroll.EmployeePayrollComponentDto;
import id.ac.tazkia.aplikasihr.dto.payroll.HitungPajakDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceRun;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRun;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunDetail;
import id.ac.tazkia.aplikasihr.entity.payroll.PayrollRunProcess;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import id.ac.tazkia.aplikasihr.entity.transaction.EmployeeLoanBayar;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.catalina.authenticator.SpnegoAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class PayrollRunController {

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private AttendanceDao attendanceDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeePayrollComponentDao employeePayrollComponentDao;

    @Autowired
    private PayrollRunDao payrollRunDao;

    @Autowired
    private EmployeeLoanDao employeeLoanDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployeeLoanBayarDao employeeLoanBayarDao;

    @Autowired
    private AttendanceRunDao attendanceRunDao;

    @Autowired
    private EmployeePayrollInfoDao employeePayrollInfoDao;

    @Autowired
    private HitungPajakDao hitungPajakDao;

    @Autowired
    private PayrollRunDetailDao payrollRunDetailDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    private PayrollRunProcessDao payrollRunProcessDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private PayrollRunProcessDetailDao payrollRunProcessDetailDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/payroll/run")
    public String runPayroll(Model model,
                             @RequestParam(required = false) String companies,
                             @RequestParam(required = false) String tahun,
                             @RequestParam(required = false) String bulan,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(companies != null && tahun != null && bulan != null){
            model.addAttribute("companiesSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }

        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("listPayrollRunProcess", payrollRunProcessDao.findByStatusNotOrderByTanggalInputDesc(StatusRecord.HAPUS));

        return "payroll/run/form";

    }

    @PostMapping("/payroll/run/process")
    public String prosesRunPayroll(Model model,
                                   @RequestParam(required = false) String companies,
                                   @RequestParam(required = false) String bulan,
                                   @RequestParam(required = false) String tahun,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        PayrollRunProcess payrollRunProcess = new PayrollRunProcess();
        payrollRunProcess.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
        payrollRunProcess.setTahun(tahun);
        payrollRunProcess.setBulan(bulan);
        payrollRunProcess.setTanggalInput(LocalDateTime.now());
        payrollRunProcess.setStatus(StatusRecord.WAITING);
        payrollRunProcess.setUserInput(employes.getFullName());
        payrollRunProcessDao.save(payrollRunProcess);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../run?companies="+ companies +"&tahun="+ tahun + "&bulan" + bulan;

    }


    @GetMapping("/payroll/run/detail")
    public String processRunPayrollDetail(Model model,
                                          @RequestParam(required = true) PayrollRunProcess payrollRunProcess,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("listPayrollRunProcessDetail", payrollRunProcessDetailDao.findByStatusAndPayrollRunProcess(StatusRecord.AKTIF, payrollRunProcess));
        model.addAttribute("payrollRunProcess", payrollRunProcess);

        return "payroll/run/form_detail";
    }


}
