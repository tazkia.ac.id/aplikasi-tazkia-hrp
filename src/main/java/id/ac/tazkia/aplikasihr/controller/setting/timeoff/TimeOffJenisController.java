package id.ac.tazkia.aplikasihr.controller.setting.timeoff;

import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.timeoff.TimeOffJenisDao;
import id.ac.tazkia.aplikasihr.entity.BlockingType;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.timeoff.TimeOffJenis;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.hibernate.validator.constraints.Mod10Check;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Controller
public class TimeOffJenisController {

    @Autowired
    private TimeOffJenisDao timeOffJenisDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private TahunDao tahunDao;

    @GetMapping("/setting/timeoff/jenis")
    public String listJenisTimeOff(Model model,
                                   @RequestParam(required = false) String search,
                                   @RequestParam(required = false) String tahun,
                                   @PageableDefault(size = 10) Pageable page,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Integer tahunAsli = LocalDate.now().getYear();
        if (tahun == null){
            tahun = tahunAsli.toString();
        }

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusAndTahunAndTimeOffNameContainingIgnoreCaseOrderByTimeOffName(StatusRecord.AKTIF, tahun, search, page));
        }else{
            model.addAttribute("listTimeOffJenis", timeOffJenisDao.findByStatusAndTahunOrderByTimeOffName(StatusRecord.AKTIF, tahun, page));
        }
        model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("menutimeoffconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/timeoff/jenis/list";

    }

    @GetMapping("/setting/timeoff/jenis/new")
    public String newTimeOffJenis(Model model,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        Integer tahunAsli = LocalDate.now().getYear();
        String tahun = tahunAsli.toString();

        model.addAttribute("timeOffJenis", new TimeOffJenis());
        model.addAttribute("blockingType", BlockingType.values());


        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
        model.addAttribute("menutimeoffconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/timeoff/jenis/form";

    }

    @GetMapping("/setting/timeoff/jenis/edit")
    public String editTimeOffJenis(Model model,
                                   @RequestParam(required = true)TimeOffJenis timeOffJenis,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("timeOffJenis", timeOffJenis);
        model.addAttribute("blockingType", BlockingType.values());
        model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, timeOffJenis.getTahun()));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        model.addAttribute("menutimeoffconfiguration", "active");
        model.addAttribute("setting", "active");
        return "setting/timeoff/jenis/form";

    }

    @PostMapping("/setting/timeoff/jenis/save")
    public String saveTimeOffJenis(@ModelAttribute @Valid TimeOffJenis timeOffJenis,
                                   @RequestParam(required = true) String tahun,
                                   Authentication authentication,
                                   RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        timeOffJenis.setDateUpdate(LocalDateTime.now());
        timeOffJenis.setUserUpdate(user.getUsername());
        timeOffJenis.setStatus(StatusRecord.AKTIF);

        if (timeOffJenis.getStatusQuota() == null){
            timeOffJenis.setStatusQuota(StatusRecord.NONAKTIF);
        }else{
            timeOffJenis.setStatusQuota(StatusRecord.AKTIF);
        }

        timeOffJenis.setTahun(tahun);
        timeOffJenisDao.save(timeOffJenis);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../jenis?tahun="+ timeOffJenis.getTahun();

    }

    @PostMapping("/setting/timeoff/jenis/delete")
    public String deleteTimeOffJenis(@ModelAttribute @Valid TimeOffJenis timeOffJenis,
                                     Authentication authentication,
                                     RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);

        timeOffJenis.setDateUpdate(LocalDateTime.now());
        timeOffJenis.setUserUpdate(user.getUsername());
        timeOffJenis.setStatus(StatusRecord.HAPUS);

        timeOffJenisDao.save(timeOffJenis);
        attributes.addFlashAttribute("deleted","Delete data berhasil");
        return "redirect:../jenis?tahun="+ timeOffJenis.getTahun();

    }

}
