package id.ac.tazkia.aplikasihr.controller.attendance;

import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.attendance.AttendanceScheduleEventDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.schedule.ScheduleEventDao;
import id.ac.tazkia.aplikasihr.entity.attendance.AttendanceScheduleEvent;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

@Controller
public class AttendanceScheduleEventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private AttendanceScheduleEventDao attendanceScheduleEventDao;

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    @Value("${upload.attendancerequest}")
    private String uploadFolder;

    @GetMapping("/request/attendance/event")
    public String inputAttendanceScheduleEvent(Model model,
                                               @RequestParam(required = true) ScheduleEvent scheduleEvent,
                                               Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("scheduleEvent", scheduleEvent);
        model.addAttribute("attendanceScheduleEvent", new AttendanceScheduleEvent());

        return "request/attendance/schedule/form";
    }

    @PostMapping("/request/attendance/event/save")
    public String inputAttendanceScheduleEventSave(@ModelAttribute @Valid AttendanceScheduleEvent attendanceScheduleEvent,
                                                   @RequestParam("lampiran") MultipartFile file,
                                                   @RequestParam(required = true) ScheduleEvent scheduleEvent,
                                                   Authentication authentication,
                                                   BindingResult errors,
                                                   Model model,
                                                   RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        if(errors.hasErrors()){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceScheduleEvent", attendanceScheduleEvent);
//            model.addAttribute("request", "active");
//            model.addAttribute("menurequestattendance", "active");
            return "request/attendance/schedule/form";
        }

        if (file == null){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceScheduleEvent", attendanceScheduleEvent);
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/attendance/schedule/form";
        }

        if (file.isEmpty()) {
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("attendanceScheduleEvent", attendanceScheduleEvent);
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/attendance/schedule/form";
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(attendanceScheduleEvent.getFileUpload() == null){
                attendanceScheduleEvent.setFileUpload("default.jpg");
            }
        }else{
            attendanceScheduleEvent.setFileUpload(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        attendanceScheduleEvent.setDateAttendance(LocalDate.now());
        attendanceScheduleEvent.setTimeIn(LocalTime.now());
        attendanceScheduleEvent.setDateTimeIn(LocalDateTime.now());
        attendanceScheduleEvent.setEmployes(employes);
        attendanceScheduleEvent.setUserInsert(user.getUsername());
        attendanceScheduleEvent.setDateInsert(LocalDateTime.now());
        attendanceScheduleEvent.setScheduleEvent(scheduleEvent);
        attendanceScheduleEventDao.save(attendanceScheduleEvent);

        model.addAttribute("success", "Save Data Success");
        return "redirect:../../../dashboard";

    }
}
