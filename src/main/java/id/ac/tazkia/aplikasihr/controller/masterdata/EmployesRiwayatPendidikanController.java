package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeRiwayatPendidikanDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JenjangPendidikanDao;
import id.ac.tazkia.aplikasihr.entity.StatusLulus;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeRiwayatPendidikan;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.hibernate.validator.constraints.pl.REGON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class EmployesRiwayatPendidikanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeRiwayatPendidikanDao employeeRiwayatPendidikanDao;

    @Autowired
    private JenjangPendidikanDao jenjangPendidikanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.ijasahemployee}")
    private String uploadFolderIjasah;

    @Autowired
    @Value("${upload.transkriptemployee}")
    private String uploadFolderTranskript;

    @GetMapping("/masterdata/employes/pendidikan")
    public String listEmployesRiwayatPendidikan(Model model,
                                                @RequestParam(required = true) Employes employes,
                                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        model.addAttribute("listRiwayatPendidikan", employeeRiwayatPendidikanDao.findByStatusAndEmployesOrderByTahunMasuk(StatusRecord.AKTIF, employes));

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeEducation", "active");
        return "masterdata/employes/pendidikan/list";

    }

    @GetMapping("/masterdata/employes/pendidikan/new")
    public String newEmployesRiwayatPendidikan(Model model,
                                               @RequestParam(required = true) Employes employes,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        StatusLulus[] statusLuluses = id.ac.tazkia.aplikasihr.entity.StatusLulus.values();

        model.addAttribute("employes", employes);
        model.addAttribute("listStatusLulus", statusLuluses);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("employeeRiwayatPendidikan", new EmployeeRiwayatPendidikan());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeEducation", "active");
        return "masterdata/employes/pendidikan/form";

    }

    @GetMapping("/masterdata/employes/pendidikan/edit")
    public String editEmployesRiwayatPendidikan(Model model,
                                                @RequestParam(required = true) EmployeeRiwayatPendidikan employeeRiwayatPendidikan,
                                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        StatusLulus[] statusLuluses = id.ac.tazkia.aplikasihr.entity.StatusLulus.values();

        model.addAttribute("employes", employeeRiwayatPendidikan.getEmployes());
        model.addAttribute("listStatusLulus", statusLuluses);
        model.addAttribute("listJenjangPendidikan", jenjangPendidikanDao.findByStatusOrderByNamaPendidikan(StatusRecord.AKTIF));
        model.addAttribute("employeeRiwayatPendidikan", employeeRiwayatPendidikan);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeEducation", "active");
        return "masterdata/employes/pendidikan/form";

    }

    @PostMapping("/masterdata/employes/pendidikan/save")
    public String saveEmployesRiwayatPendidikan(@ModelAttribute @Valid EmployeeRiwayatPendidikan employeeRiwayatPendidikan,
                                                @RequestParam(required = true)Employes employes,
                                                @RequestParam("ijasah") MultipartFile fileIjasah,
                                                @RequestParam("transkript") MultipartFile fileTranskript,
                                                Authentication authentication,
                                                RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);


        String namaFile =  fileIjasah.getName();
        String jenisFile = fileIjasah.getContentType();
        String namaAsli = fileIjasah.getOriginalFilename();
        Long ukuran = fileIjasah.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employes.getFileFoto() == null){
                employes.setFileFoto("default.jpg");
            }
        }else{
            employeeRiwayatPendidikan.setFileIjasah(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolderIjasah);
            new File(uploadFolderIjasah).mkdirs();
            File tujuan = new File(uploadFolderIjasah + File.separator + idFile + "." + extension);
            fileIjasah.transferTo(tujuan);
        }

        String namaFileTranskript =  fileTranskript.getName();
        String jenisFileTranskript = fileTranskript.getContentType();
        String namaAsliTranskript = fileTranskript.getOriginalFilename();
        Long ukuranTranskript = fileTranskript.getSize();
        String extensionTranskript = "";
        int iTranskript = namaAsliTranskript.lastIndexOf('.');
        int pTranskript = Math.max(namaAsliTranskript.lastIndexOf('/'), namaAsliTranskript.lastIndexOf('\\'));
        if (iTranskript > pTranskript) {
            extensionTranskript= namaAsliTranskript.substring(iTranskript + 1);
        }
        String idFileTranskript= UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsliTranskript);
            System.out.println("Ukuran :"+ ukuranTranskript);
            if(employeeRiwayatPendidikan.getFileTranskrip() == null){
                employeeRiwayatPendidikan.setFileTranskrip("default.jpg");
            }
        }else{
            employeeRiwayatPendidikan.setFileTranskrip(idFileTranskript + "." + extension);
            System.out.println("file :"+ namaAsliTranskript);
            System.out.println("Ukuran :"+ ukuranTranskript);
            LOGGER.debug("Lokasi upload : {}", uploadFolderTranskript);
            new File(uploadFolderTranskript).mkdirs();
            File tujuan = new File(uploadFolderTranskript + File.separator + idFileTranskript + "." + extensionTranskript);
            fileTranskript.transferTo(tujuan);
        }

        employeeRiwayatPendidikan.setDateUpdate(LocalDateTime.now());
        employeeRiwayatPendidikan.setUserUpdate(user.getUsername());
        employeeRiwayatPendidikan.setStatus(StatusRecord.AKTIF);
        employeeRiwayatPendidikan.setEmployes(employes);

        employeeRiwayatPendidikanDao.save(employeeRiwayatPendidikan);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pendidikan?employes="+ employes.getId();

    }

    @PostMapping("/masterdata/employes/pendidikan/delete")
    public String deleteEmployesRiwayatPendidikan(@ModelAttribute @Valid EmployeeRiwayatPendidikan employeeRiwayatPendidikan,
                                                  Authentication authentication,
                                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        employeeRiwayatPendidikan.setDateUpdate(LocalDateTime.now());
        employeeRiwayatPendidikan.setUserUpdate(user.getUsername());
        employeeRiwayatPendidikan.setStatus(StatusRecord.HAPUS);
//        employeeRiwayatPendidikan.setEmployes(employes);

        employeeRiwayatPendidikanDao.save(employeeRiwayatPendidikan);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pendidikan?employes="+ employeeRiwayatPendidikan.getEmployes().getId();

    }



    @GetMapping("/masterdata/employes/pendidikan/{employeeRiwayatPendidikan}/ijasah/")
    public ResponseEntity<byte[]> tampolkanIjasahEmployee(@PathVariable EmployeeRiwayatPendidikan employeeRiwayatPendidikan) throws Exception {
        String lokasiFile = uploadFolderIjasah + File.separator + employeeRiwayatPendidikan.getFileIjasah();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeRiwayatPendidikan.getFileIjasah().toLowerCase().endsWith("jpeg") || employeeRiwayatPendidikan.getFileIjasah().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeRiwayatPendidikan.getFileIjasah().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeRiwayatPendidikan.getFileIjasah().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/masterdata/employes/pendidikan/{employeeRiwayatPendidikan}/transkript/")
    public ResponseEntity<byte[]> tampolkanTranskriptEmployee(@PathVariable EmployeeRiwayatPendidikan employeeRiwayatPendidikan) throws Exception {
        String lokasiFile1 = uploadFolderTranskript + File.separator + employeeRiwayatPendidikan.getFileTranskrip();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile1);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeRiwayatPendidikan.getFileTranskrip().toLowerCase().endsWith("jpeg") || employeeRiwayatPendidikan.getFileTranskrip().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeRiwayatPendidikan.getFileTranskrip().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeRiwayatPendidikan.getFileTranskrip().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile1));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
