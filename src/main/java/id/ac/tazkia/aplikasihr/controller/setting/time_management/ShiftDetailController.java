package id.ac.tazkia.aplikasihr.controller.setting.time_management;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftDetailDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.ShiftPatternDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.Pattern;
import id.ac.tazkia.aplikasihr.entity.setting.time_management.ShiftDetail;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Controller
public class ShiftDetailController {

    @Autowired
    private ShiftDetailDao shiftDetailDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ShiftPatternDao shiftPatternDao;

    @Autowired
    private EmployesDao employesDao;

    @PostMapping("/setting/timemanagement/shift/detail/save")
    public String saveShifDeteil (@ModelAttribute @Valid ShiftDetail shiftDetail,
                                  @RequestParam(required = false)Pattern pattern,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        String breakS = "NO";

        if (shiftDetail.getBreakStatus() == null){
            breakS = "NO";
        }else{
            breakS = shiftDetail.getBreakStatus();
        }

        if (shiftDetail.getWorkDayStatus() == null){
            shiftDetail.setWorkDayStatus("OFF");
        }

        String shiftIn = shiftDetail.getShiftInString();
        String shiftOut = shiftDetail.getShiftOutString();
        String breakIn = shiftDetail.getBreakInString();
        String breakOut = shiftDetail.getBreakOutString();
        String overtimeBefore = shiftDetail.getOvertimeBeforeString();
        String overtimeAfter = shiftDetail.getOvertimeAfterString();


        String jam = "00";
        String menit = "00";
        Integer jamInt = 00;
        String detik = "00";

        if(shiftIn.length() == 4){
            jam = shiftIn.substring(0,1);
            menit = shiftIn.substring(2,4);
        } else if (shiftIn.length() == 5){
            jam = shiftIn.substring(0,2);
            menit = shiftIn.substring(3,5);
        } else if (shiftIn.length() == 7){
            jam = shiftIn.substring(0,1);
            menit = shiftIn.substring(2,4);
            jamInt = new Integer(jam);
            if (shiftIn.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = shiftIn.substring(0,2);
            menit = shiftIn.substring(3,5);
            jamInt = new Integer(jam);
            if (shiftIn.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        shiftIn = jam + ':' + menit + ':' + detik;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTimeShiftIn = LocalTime.parse(shiftIn, formatter1);

        if(shiftOut.length() == 4){
            jam = shiftOut.substring(0,1);
            menit = shiftOut.substring(2,4);
        } else if (shiftOut.length() == 5){
            jam = shiftOut.substring(0,2);
            menit = shiftOut.substring(3,5);
        } else if (shiftOut.length() == 7){
            jam = shiftOut.substring(0,1);
            menit = shiftOut.substring(2,4);
            jamInt = new Integer(jam);
            if (shiftOut.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = shiftOut.substring(0,2);
            menit = shiftOut.substring(3,5);
            jamInt = new Integer(jam);
            if (shiftOut.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        shiftOut = jam + ':' + menit + ':' + detik;
        LocalTime localTimeShiftOut = LocalTime.parse(shiftOut, formatter1);

        if(breakIn.length() == 4){
            jam = breakIn.substring(0,1);
            menit = breakIn.substring(2,4);
        } else if (breakIn.length() == 5){
            jam = breakIn.substring(0,2);
            menit = breakIn.substring(3,5);
        } else if (breakIn.length() == 7){
            jam = breakIn.substring(0,1);
            menit = breakIn.substring(2,4);
            jamInt = new Integer(jam);
            if (breakIn.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = breakIn.substring(0,2);
            menit = breakIn.substring(3,5);
            jamInt = new Integer(jam);
            if (breakIn.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        breakIn = jam + ':' + menit + ':' + detik;
        LocalTime localTimeBreakIn = LocalTime.parse(shiftOut, formatter1);

        if(breakOut.length() == 4){
            jam = breakOut.substring(0,1);
            menit = breakOut.substring(2,4);
        } else if (breakOut.length() == 5){
            jam = breakOut.substring(0,2);
            menit = breakOut.substring(3,5);
        } else if (breakOut.length() == 7){
            jam = breakOut.substring(0,1);
            menit = breakOut.substring(2,4);
            jamInt = new Integer(jam);
            if (breakOut.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = breakOut.substring(0,2);
            menit = breakOut.substring(3,5);
            jamInt = new Integer(jam);
            if (breakOut.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        breakOut = jam + ':' + menit + ':' + detik;
        LocalTime localTimeBreakOut = LocalTime.parse(shiftOut, formatter1);

        if(overtimeBefore.length() == 4){
            jam = overtimeBefore.substring(0,1);
            menit = overtimeBefore.substring(2,4);
        } else if (overtimeBefore.length() == 5){
            jam = overtimeBefore.substring(0,2);
            menit = overtimeBefore.substring(3,5);
        } else if (overtimeBefore.length() == 7){
            jam = overtimeBefore.substring(0,1);
            menit = overtimeBefore.substring(2,4);
            jamInt = new Integer(jam);
            if (overtimeBefore.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = overtimeBefore.substring(0,2);
            menit = overtimeBefore.substring(3,5);
            jamInt = new Integer(jam);
            if (overtimeBefore.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        overtimeBefore = jam + ':' + menit + ':' + detik;
        LocalTime localTimeOvertimeBefore = LocalTime.parse(shiftOut, formatter1);

        if(overtimeAfter.length() == 4){
            jam = overtimeAfter.substring(0,1);
            menit = overtimeAfter.substring(2,4);
        } else if (overtimeAfter.length() == 5){
            jam = overtimeAfter.substring(0,2);
            menit = overtimeAfter.substring(3,5);
        } else if (overtimeAfter.length() == 7){
            jam = overtimeAfter.substring(0,1);
            menit = overtimeAfter.substring(2,4);
            jamInt = new Integer(jam);
            if (overtimeAfter.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = overtimeAfter.substring(0,2);
            menit = overtimeAfter.substring(3,5);
            jamInt = new Integer(jam);
            if (overtimeAfter.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        overtimeAfter = jam + ':' + menit + ':' + detik;
        LocalTime localTimeOvertimeAftert = LocalTime.parse(shiftOut, formatter1);

        if (shiftDetail.getWorkDayStatus().equals("OFF")){
            String jamMulai = "00:00:00";
            LocalTime localTime = LocalTime.parse(jamMulai, formatter1);
            shiftDetail.setShiftIn(localTime);
            shiftDetail.setShiftOut(localTime);
            shiftDetail.setBreakIn(localTime);
            shiftDetail.setBreakOut(localTime);
            shiftDetail.setOvertimeBefore(localTime);
            shiftDetail.setOvertimeAfter(localTime);
        }else{
            shiftDetail.setShiftIn(localTimeShiftIn);
            shiftDetail.setShiftOut(localTimeShiftOut);
            shiftDetail.setBreakIn(localTimeBreakIn);
            shiftDetail.setBreakOut(localTimeBreakOut);
            shiftDetail.setOvertimeBefore(localTimeOvertimeBefore);
            shiftDetail.setOvertimeAfter(localTimeOvertimeAftert);
        }

        shiftDetail.setBreakStatus(breakS);
        shiftDetail.setUserUpdate(user.getUsername());
        shiftDetail.setDateUpdate(LocalDateTime.now());

        shiftDetailDao.save(shiftDetail);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pattern?pattern="+pattern.getId();

    }

    @PostMapping("/setting/timemanagement/shift/detail/delete")
    public String deleteShifDeteil (@RequestParam(required = true)ShiftDetail shiftDetail,
                                  @RequestParam(required = false)Pattern pattern,
                                  Authentication authentication,
                                  RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        Integer jmlShiftPattern = shiftPatternDao.countByStatusAndShiftDetail(StatusRecord.AKTIF, shiftDetail);
        if (jmlShiftPattern == null){
            shiftDetail.setStatus(StatusRecord.HAPUS);
            shiftDetail.setDateUpdate(LocalDateTime.now());
            shiftDetail.setUserUpdate(user.getUsername());
        }else{
            attribute.addFlashAttribute("shiftDetailNotDeleteAllowed", "Save Data Success");
            return "redirect:../pattern?pattern="+pattern.getId();
        }

        shiftDetailDao.save(shiftDetail);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../pattern?pattern="+pattern.getId();

    }

}
