package id.ac.tazkia.aplikasihr.controller.kpi;

import id.ac.tazkia.aplikasihr.dao.kpi.*;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dto.kpi.SavePenilaianKpiDto;
import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PenilaianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.KpiService;

import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


@Controller
public class AssesmentController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;
    @Autowired
    private PengisianKpiDao pengisianKpiDao;
    @Autowired
    private PengisianKpiDosenDao pengisianKpiDosenDao;
    @Autowired
    private LecturerClassDao lecturerClassDao;
    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private KpiService kpiService;

    @Autowired
    private PenilaianKpiDosenDao penilaianKpiDosenDao;

    @Autowired
    private PenilaianKpiDao penilaianKpiDao;

    @Autowired
    private PositionKpiDao positionKpiDao;

    @Autowired
    private LecturerClassKpiDao lecturerClassKpiDao;

    @GetMapping("/kpi_assesment")
    public String kpiAssesment(Model model, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        model.addAttribute("employess", employes);
        model.addAttribute("listEmployee", attendanceApprovalSettingDao.listPenilaianKpi(employes.getId()));
        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpiassesment", "active");
        model.addAttribute("menukpiassesmentwaiting", "active");
        return "kpi/assesment/list";
    }


    @GetMapping("/kpi_assesment/process")
    public String kpiAssesmentProcess(Model model,
                                      @RequestParam(required = true) String idpos,
                                      @RequestParam(required = true) String employes,
                                      @RequestParam(required = true) String jenis,
                                      Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
        String bulan = String.format("%02d", LocalDate.now().getMonthValue());
        String tahun = String.valueOf(LocalDate.now().getYear());

        System.out.println("bulan : " + bulan);
        System.out.println("Tahun : " + tahun);

        Employes employesData = employesDao.findById(employes).get();
        model.addAttribute("employes", employesData);

        if(jenis.equals("TENDIK")){
            model.addAttribute("position", jobPositionDao.findByStatusAndId(StatusRecord.AKTIF,idpos));
            model.addAttribute("listRekapPengisianKpi", kpiService.getListKpiAssesmet(pengisianKpiDao.rekapPengisianKpi(idpos, employesData.getId(), bulan, tahun), jenis, bulan, tahun, employesData.getId()));
            model.addAttribute("jenis","TENDIK");
        } else {
            Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, employesData);
            model.addAttribute("position", lecturerClassDao.findByStatusAndId(StatusRecord.AKTIF,idpos));
            model.addAttribute("listRekapPengisianKpi", kpiService.getListKpiAssesmet(pengisianKpiDao.rekapPengisianKpi(idpos, lecturer.getId(), bulan, tahun), jenis, bulan, tahun, lecturer.getId()));
            model.addAttribute("jenis","DOSEN");
        }

        model.addAttribute("menukpi", "active");
        model.addAttribute("menukpiassesment", "active");
        model.addAttribute("menukpiassesmentwaiting", "active");

        return "kpi/assesment/process";
    }

    @PostMapping("/kpiAssement/penilaian")
    public String kpiAssementPenilaian(@Valid SavePenilaianKpiDto savePenilaianKpiDto) {
        String idpos = "";
        String jenis = "";
        String employesid = "";
        if (savePenilaianKpiDto.getJenis().equals("TENDIK")) {
            PositionKpi positionKpi = positionKpiDao.findById(savePenilaianKpiDto.getPositionKpi()).get();
            PenilaianKpi penilaianKpiAda = penilaianKpiDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(StatusRecord.AKTIF,savePenilaianKpiDto.getEmployes(),savePenilaianKpiDto.getBulan(),savePenilaianKpiDto.getTahun(),positionKpi);
            PenilaianKpi penilaianKpi = new PenilaianKpi();
            if(penilaianKpiAda != null){
                penilaianKpi=penilaianKpiAda;
            }
            penilaianKpi.setPositionKpi(positionKpi);
            penilaianKpi.setEmployes(savePenilaianKpiDto.getEmployes());
            penilaianKpi.setGrade(savePenilaianKpiDto.getGrade());

            idpos = positionKpi.getJobPosition().getId();
            jenis = "TENDIK";
            employesid = savePenilaianKpiDto.getEmployes().getId();
            if (savePenilaianKpiDto.getGrade().equals("A")){
                penilaianKpi.setNilai(positionKpi.getBobotA());
                penilaianKpi.setNominal(positionKpi.getNominal().multiply(positionKpi.getBobotA().divide(new BigDecimal(100))));
            } else if (savePenilaianKpiDto.getGrade().equals("B")) {
                penilaianKpi.setNilai(positionKpi.getBobotB());
                penilaianKpi.setNominal(positionKpi.getNominal().multiply(positionKpi.getBobotB().divide(new BigDecimal(100))));
            } else if (savePenilaianKpiDto.getGrade().equals("C")) {
                penilaianKpi.setNilai(positionKpi.getBobotC());
                penilaianKpi.setNominal(positionKpi.getNominal().multiply(positionKpi.getBobotC().divide(new BigDecimal(100))));
            }
            penilaianKpi.setBulan(savePenilaianKpiDto.getBulan());
            penilaianKpi.setTahun(savePenilaianKpiDto.getTahun());
            penilaianKpiDao.save(penilaianKpi);
//            return "redirect:/kpi_assesment/process?idpos="+positionKpi.getJobPosition().getId()+"&employes="+savePenilaianKpiDto.getEmployes().getId()+"&jenis=TENDIK";
        }
        if (savePenilaianKpiDto.getJenis().equals("DOSEN")) {
            LecturerClassKpi lecturerClassKpi = lecturerClassKpiDao.findById(savePenilaianKpiDto.getPositionKpi()).get();
            Lecturer lecturer = lecturerDao.findByStatusAndEmployes(StatusRecord.AKTIF, savePenilaianKpiDto.getEmployes());

            PenilaianKpiDosen penilaianKpiDosenAda = penilaianKpiDosenDao.findByStatusAndEmployesAndBulanAndTahunAndPositionKpi(StatusRecord.AKTIF,lecturer,savePenilaianKpiDto.getBulan(), savePenilaianKpiDto.getTahun(),lecturerClassKpi);
            PenilaianKpiDosen penilaianKpiDosen = new PenilaianKpiDosen();
            if(penilaianKpiDosenAda != null){
                penilaianKpiDosen=penilaianKpiDosenAda;
            }
            penilaianKpiDosen.setPositionKpi(lecturerClassKpi);
            penilaianKpiDosen.setEmployes(lecturer);
            penilaianKpiDosen.setGrade(savePenilaianKpiDto.getGrade());

            idpos = lecturerClassKpi.getLecturerClass().getId();
            jenis = "DOSEN";
            employesid = lecturer.getEmployes().getId();
            if (savePenilaianKpiDto.getGrade().equals("A")){
                penilaianKpiDosen.setNilai(lecturerClassKpi.getBobotA());
                penilaianKpiDosen.setNominal(lecturerClassKpi.getNominal().multiply(lecturerClassKpi.getBobotA().divide(new BigDecimal(100))));
            } else if (savePenilaianKpiDto.getGrade().equals("B")) {
                penilaianKpiDosen.setNilai(lecturerClassKpi.getBobotB());
                penilaianKpiDosen.setNominal(lecturerClassKpi.getNominal().multiply(lecturerClassKpi.getBobotB().divide(new BigDecimal(100))));
            } else if (savePenilaianKpiDto.getGrade().equals("C")) {
                penilaianKpiDosen.setNilai(lecturerClassKpi.getBobotC());
                penilaianKpiDosen.setNominal(lecturerClassKpi.getNominal().multiply(lecturerClassKpi.getBobotC().divide(new BigDecimal(100))));
            }
            penilaianKpiDosen.setBulan(savePenilaianKpiDto.getBulan());
            penilaianKpiDosen.setTahun(savePenilaianKpiDto.getTahun());
            penilaianKpiDosenDao.save(penilaianKpiDosen);


        }
        return "redirect:/kpi_assesment/process?idpos="+idpos+"&employes="+employesid+"&jenis="+jenis;
    }


}
