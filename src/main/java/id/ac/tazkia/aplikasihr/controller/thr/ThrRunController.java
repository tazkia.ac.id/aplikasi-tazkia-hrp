package id.ac.tazkia.aplikasihr.controller.thr;

import id.ac.tazkia.aplikasihr.dao.BulanDao;
import id.ac.tazkia.aplikasihr.dao.TahunDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.CompaniesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.thr.ThrRunProcessDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.thr.ThrRunProcess;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class ThrRunController {

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private ThrRunProcessDao thrRunProcessDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;


    @GetMapping("/thr/run")
    public String thrRun(Model model,
                         @RequestParam(required = false) String companies,
                         @RequestParam(required = false) String tahun,
                         @RequestParam(required = false) String bulan,
                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        if(companies != null && tahun != null && bulan != null){
            model.addAttribute("companiesSelected", companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndNomor(StatusRecord.AKTIF, bulan));
        }

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listCompanies", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employes.getId());
            model.addAttribute("listCompanies", companiesDao.findByStatusNotAndIdInOrderByCompanyName(StatusRecord.HAPUS, idDepartements));
        }
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByNomor(StatusRecord.AKTIF));
        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));

        model.addAttribute("listThrRunProcess", thrRunProcessDao.findByStatusNotOrderByTanggalInputDesc(StatusRecord.HAPUS));

        return "thr/form_run";
    }

    @PostMapping("/thr/run/process")
    public String thrRunProcess(Model model,
                                @RequestParam(required = false) String companies,
                                @RequestParam(required = false) String bulan,
                                @RequestParam(required = false) String tahun,
                                Authentication authentication,
                                RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        ThrRunProcess thrRunProcess = new ThrRunProcess();
        thrRunProcess.setCompanies(companiesDao.findByStatusAndId(StatusRecord.AKTIF, companies));
        thrRunProcess.setTahun(tahun);
        thrRunProcess.setBulan(bulan);
        thrRunProcess.setTanggalInput(LocalDateTime.now());
        thrRunProcess.setStatus(StatusRecord.WAITING);
        thrRunProcess.setUserInput(employes.getFullName());
        thrRunProcessDao.save(thrRunProcess);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../run?companies="+ companies +"&tahun="+ tahun + "&bulan" + bulan;

    }

}
