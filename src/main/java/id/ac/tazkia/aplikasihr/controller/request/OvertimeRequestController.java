package id.ac.tazkia.aplikasihr.controller.request;

import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployeeStatusAktifDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.OverTImeRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.OverTimeRequestDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.TimeOffRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.AttendanceApprovalSettingDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.time_management.EmployeeScheduleDao;
import id.ac.tazkia.aplikasihr.dto.CekShiftEmployeeAdaDto;
import id.ac.tazkia.aplikasihr.dto.CekShiftEmployeeDto;
import id.ac.tazkia.aplikasihr.dto.CekShiftKeluarDto;
import id.ac.tazkia.aplikasihr.dto.CekShiftMasukDto;
import id.ac.tazkia.aplikasihr.dto.employes.EmployeeStatusKontrakDto;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.*;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Array;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class OvertimeRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private OverTimeRequestDao overTimeRequestDao;

    @Autowired
    private OverTImeRequestApprovalDao overTImeRequestApprovalDao;

    @Autowired
    private AttendanceApprovalSettingDao attendanceApprovalSettingDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private EmployeeStatusAktifDao employeeStatusAktifDao;

    @Autowired
    @Value("${upload.overtimerequest}")
    private String uploadFolder;

    @GetMapping("/request/overtime")
    public String listOverTimeRequest(Model model,
                                     @PageableDefault(size = 10) Pageable page,
                                     @RequestParam(required = false)String search,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);



        model.addAttribute("listOvertimeRequest", overTimeRequestDao.findByStatusAndEmployesOrderByTanggalDesc(StatusRecord.AKTIF, employes, page));
        model.addAttribute("listApproval", attendanceApprovalSettingDao.listOvertimeRequestAttendance(employes.getId()));

        model.addAttribute("request", "active");
        model.addAttribute("menurequestovertime", "active");
        return "request/overtime/list";

    }

    @GetMapping("/request/overtime/new")
    public String newOvertimeRequest(Model model,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("employes", employes);

        EmployeeStatusKontrakDto employeeStatusKontrakDto = employeeStatusAktifDao.cariStatusKontrak(employes.getId());

        if(employeeStatusKontrakDto.getStatus().equals("NO")){
            model.addAttribute("kontrak", employeeStatusKontrakDto);
            model.addAttribute("jenis", " Overtime Request");
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            return "request/form_eror";
        }

        LocalDate today = LocalDate.now();
        String formattedDate = today.format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy"));

        model.addAttribute("tanggalSelected", formattedDate);
        model.addAttribute("overTimeRequest", new OvertimeRequest());

        model.addAttribute("request", "active");
        model.addAttribute("menurequestovertime", "active");
        return "request/overtime/form";

    }

    @GetMapping("/request/overtime/edit")
    public String editOvertimeRequest(Model model,
                                     @RequestParam(required = true) OvertimeRequest overtimeRequest,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);
        model.addAttribute("employes", employes);
        model.addAttribute("overtimeRequest", overtimeRequest);
        model.addAttribute("request", "active");
        model.addAttribute("menurequestovertime", "active");
        return "request/overtime/formedit";

    }

    @PostMapping("request/overtime/save")
    public String saveOvertimeRequest(@ModelAttribute @Valid OvertimeRequest overtimeRequest,
                                     @RequestParam(required = false) String statusHari,
                                     Model model,
                                     @RequestParam("fileUpload") MultipartFile file,
                                     Authentication authentication,
                                     RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        System.out.println("Status hari :" + statusHari);

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        LocalDate localDate = LocalDate.parse(overtimeRequest.getTanggalOvertimeString(),formatter);

        String jamMulai = overtimeRequest.getOvertimeFromString();
        String jam = "00";
        String menit = "00";
        String detik = "00";
        Integer jamInt = 00;
        String jam2 = "00";
        String menit2 = "00";
        String detik2 = "00";
        Integer jamInt2 = 00;

        if(jamMulai.length() == 4){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
        } else if (jamMulai.length() == 5){
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
        } else if (jamMulai.length() == 7){
            jam = jamMulai.substring(0,1);
            menit = jamMulai.substring(2,4);
            jamInt = new Integer(jam);
            if (jamMulai.substring(5,7).equals("PM")){
                jamInt = jamInt + 12;
                if (jamInt == 24){
                    jamInt = 12;
                }
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }else{
            jam = jamMulai.substring(0,2);
            menit = jamMulai.substring(3,5);
            jamInt = new Integer(jam);
            if (jamMulai.substring(6,8).equals("PM")){
                jamInt = jamInt + 12;
                if (jamInt == 24){
                    jamInt = 12;
                }
            }
            jam = jamInt.toString();
            if (jam.length() == 1){
                jam = '0'+jam;
            }
        }

        String jamSelesai = overtimeRequest.getOvertimeToString();

        if(jamSelesai.length() == 4){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
        } else if (jamSelesai.length() == 5){
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
        } else if (jamSelesai.length() == 7){
            jam2 = jamSelesai.substring(0,1);
            menit2 = jamSelesai.substring(2,4);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(5,7).equals("PM")){
                jamInt2 = jamInt2 + 12;
                if (jamInt2 == 24){
                    jamInt2 = 12;
                }
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }else{
            jam2 = jamSelesai.substring(0,2);
            menit2 = jamSelesai.substring(3,5);
            jamInt2 = new Integer(jam2);
            if (jamSelesai.substring(6,8).equals("PM")){
                jamInt2 = jamInt2 + 12;
                if (jamInt2 == 24){
                    jamInt2 = 12;
                }
            }
            jam2 = jamInt2.toString();
            if (jam2.length() == 1){
                jam2 = '0'+jam2;
            }
        }

        jamMulai = jam + ':' + menit + ':' + detik;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime = LocalTime.parse(jamMulai, formatter1);

        jamSelesai = jam2 + ':' + menit2 + ':' + detik2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
        LocalTime localTime2 = LocalTime.parse(jamSelesai, formatter2);

//        String date = overtimeRequest.getTanggalOvertimeString();
//        String tahun = date.substring(6,10);
//        String tanggal = date.substring(0,2);
//        String bulan = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        String tanggalan2 = tahun + '-' + bulan + '-' + tanggal + " 00:00:00";
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//        LocalDateTime localDateTime = LocalDateTime.parse(tanggalan2, formatter2);

        System.out.println("tanggal" + localDate);

        if (file == null){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("overtimeRequest", overtimeRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/overtime/eror";
        }

        if (file.isEmpty()) {
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("overtimeRequest", overtimeRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            model.addAttribute("fatal","Please select a file!, attachement is required");
            return "request/overtime/eror";
        }

//        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime overtimeMasuk = LocalDateTime.parse(overtimeRequest.getTanggalOvertimeString() + ' '+ localTime, formatter3);
        LocalDateTime overtimeKeluar = LocalDateTime.parse(overtimeRequest.getTanggalOvertimeString() + ' '+ localTime2, formatter3);

        Integer diff = overtimeKeluar.getHour() - overtimeMasuk.getHour();
        if (diff < 0){
            if (diff < -12) {
                diff = diff + 24;
            }else{
                diff = diff + 12;
            }
        }

        Integer sisaSeminggu = overTimeRequestDao.cekOvertimeTerpakaiSeminggu(employes.getId(), localDate);

        if( (sisaSeminggu + diff) > 14){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("overtimeRequest", overtimeRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            model.addAttribute("fatal","Quota lembur anda dalam seminggu tidak boleh lebih dari 14 jam");
            return "request/overtime/eror";
        }else{
            Integer sisaSehari = overTimeRequestDao.cekOvertimeTerpakaiSehari(employes.getId(), localDate);
            if(overtimeRequest.getStatusOvertime().toString().equals("OFF")){
                if((sisaSehari + diff) > 5){
                    model.addAttribute("employess", employes);
                    model.addAttribute("employes", employes);
                    model.addAttribute("overtimeRequest", overtimeRequest);
                    model.addAttribute("request", "active");
                    model.addAttribute("menurequestovertime", "active");
                    model.addAttribute("fatal","Quota lembur anda di hari libur tidak boleh lebih dari 5 jam");
                    return "request/overtime/eror";
                }
            }else{
                if((sisaSehari + diff) > 3){
                    model.addAttribute("employess", employes);
                    model.addAttribute("employes", employes);
                    model.addAttribute("overtimeRequest", overtimeRequest);
                    model.addAttribute("request", "active");
                    model.addAttribute("menurequestovertime", "active");
                    model.addAttribute("fatal","Quota lembur anda di hari kerja tidak boleh lebih dari 3 jam");
                    return "request/overtime/eror";
                }
            }
        }

        Integer adaData = overTimeRequestDao.cariDataOvertimeSerupa(localDate, overtimeMasuk.toLocalTime(), overtimeKeluar.toLocalTime(), employes.getId());
        System.out.println("Data : "+ adaData);
        if (adaData == null){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("overtimeRequest", overtimeRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            model.addAttribute("fatal","Anda sudah input data serupa sebelum nya");
            return "request/overtime/eror";
        }else if(adaData > 0){
            model.addAttribute("employess", employes);
            model.addAttribute("employes", employes);
            model.addAttribute("overtimeRequest", overtimeRequest);
            model.addAttribute("request", "active");
            model.addAttribute("menurequestovertime", "active");
            model.addAttribute("fatal","Anda sudah input data serupa sebelum nya");
            return "request/overtime/eror";
        }

        System.out.println("Compare datetime = " + diff);

//        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate1 = LocalDate.parse(overtimeRequest.getTanggalOvertimeString(), formatter1);

        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());

        if(idEmployes.isEmpty()){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                model.addAttribute("overtimeRequest", overtimeRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request Overtime tidak dapat dilakukan, Hal ini dikarenakan jabatan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/overtime/eror";
        }
        //        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove1 = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);
        Integer jumlahApprove = 0;
        if (jumlahApprove1 != null){
            jumlahApprove = jumlahApprove1;
        }
        Integer nomorAwal = 1;

        if(nomorAwal > jumlahApprove){
                model.addAttribute("employess", employes);
                model.addAttribute("employes", employes);
                model.addAttribute("overtimeRequest", overtimeRequest);
                model.addAttribute("request", "active");
                model.addAttribute("menurequestattendance", "active");
                model.addAttribute("fatal","Maaf Request Overtime tidak dapat dilakukan, Hal ini dikarenakan kemungkinan jabatan atasan anda telah berakhir, hubungi pihak SDM untuk mengkonfirmasi hal tersebut");
                return "request/overtime/eror";
        }

        overtimeRequest.setDateUpdate(LocalDateTime.now());
        overtimeRequest.setUserUpdate(user.getUsername());
        overtimeRequest.setNomor(nomorAwal);
        overtimeRequest.setTotalNomor(jumlahApprove);
        if (nomorAwal > jumlahApprove){
            overtimeRequest.setStatusApprove(StatusRecord.APPROVED);
        }else {
            overtimeRequest.setStatusApprove(StatusRecord.WAITING);
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(overtimeRequest.getFile() == null){
                overtimeRequest.setFile("default.jpg");
            }
        }else{
            overtimeRequest.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        overtimeRequest.setTanggalOvertime(localDate);
        overtimeRequest.setOvertimeFrom(localTime);
        overtimeRequest.setOvertimeTo(localTime2);
        overtimeRequest.setJam(diff);
        overTimeRequestDao.save(overtimeRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../overtime";

    }

    @PostMapping("request/overtime/update")
    @Transactional
    public String updateOvertimeRequest(@ModelAttribute @Valid OvertimeRequest overtimeRequest,
                                       @RequestParam("fileUpload") MultipartFile file,
                                       Authentication authentication,
                                       RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        //        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove1 = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);
        Integer jumlahApprove = 0;
        if (jumlahApprove1 != null){
            jumlahApprove = jumlahApprove1;
        }
        Integer nomorAwal = 1;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        LocalDate localDate = LocalDate.parse(overtimeRequest.getTanggalOvertimeString(),formatter);

        overtimeRequest.setTanggalOvertime(localDate);
        overtimeRequest.setDateUpdate(LocalDateTime.now());
        overtimeRequest.setUserUpdate(user.getUsername());
        overtimeRequest.setNomor(nomorAwal);
        overtimeRequest.setTotalNomor(jumlahApprove);
        if (nomorAwal > jumlahApprove){
            overtimeRequest.setStatusApprove(StatusRecord.APPROVED);
        }else {
            overtimeRequest.setStatusApprove(StatusRecord.WAITING);
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(overtimeRequest.getFile() == null){
                overtimeRequest.setFile("default.jpg");
            }
        }else{
            overtimeRequest.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        List<OvertimeRequestApproval> overtimeRequestApprovals = overTImeRequestApprovalDao.findByStatusAndOvertimeRequest(StatusRecord.AKTIF, overtimeRequest);
        for (OvertimeRequestApproval s : overtimeRequestApprovals){

            s.setStatusApprove(StatusRecord.HAPUS);
            s.setStatus(StatusRecord.HAPUS);

            overTImeRequestApprovalDao.save(s);

        }

        overTimeRequestDao.save(overtimeRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../overtime";

    }

    @PostMapping("request/overtime/delete")
    @Transactional
    public String deleteOvertimeRequest(@RequestParam (required = true) OvertimeRequest overtimeRequest,
                                       Authentication authentication,
                                       RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        Integer nomorAwal = attendanceApprovalSettingDao.getNomorAwal(idEmployes);
        Integer jumlahApprove = attendanceApprovalSettingDao.getJumlahApproval(idEmployes);

        List<OvertimeRequestApproval> overtimeRequestApprovals = overTImeRequestApprovalDao.findByStatusAndOvertimeRequest(StatusRecord.AKTIF, overtimeRequest);
        for (OvertimeRequestApproval s : overtimeRequestApprovals){

            s.setStatusApprove(StatusRecord.HAPUS);
            s.setStatus(StatusRecord.HAPUS);

            overTImeRequestApprovalDao.save(s);

        }

        overtimeRequest.setStatus(StatusRecord.HAPUS);
        overtimeRequest.setStatusApprove(StatusRecord.CANCELED);
        overtimeRequest.setDateUpdate(LocalDateTime.now());
        overtimeRequest.setUserUpdate(user.getUsername());

        overTimeRequestDao.save(overtimeRequest);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../overtime";

    }


    @GetMapping("/overtime/{overtimeRequest}/request/")
    public ResponseEntity<byte[]> tampilkanBuktiAttendance(@PathVariable OvertimeRequest overtimeRequest) throws Exception {
        String lokasiFile = uploadFolder + File.separator + overtimeRequest.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (overtimeRequest.getFile().toLowerCase().endsWith("jpeg") || overtimeRequest.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (overtimeRequest.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (overtimeRequest.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


//    @GetMapping("/api/overtime/cek_shift")
//    public String

    @GetMapping("/api/overtime/cek_shift")
    @ResponseBody
    public CekShiftEmployeeAdaDto cariShiftDto(@RequestParam(required = false) String tanggal,
                                                  @RequestParam(required = false) String employee){

        System.out.println("tanggal : " + tanggal);
        String date = tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggal, formatter);

        CekShiftEmployeeDto cekShiftEmployeeDtos = employeeScheduleDao.cariShiftEmployee(localDate, employee);
        CekShiftEmployeeAdaDto cekShiftEmployeeAdaDto = new CekShiftEmployeeAdaDto();
        cekShiftEmployeeAdaDto.setId(cekShiftEmployeeDtos.getId());
        cekShiftEmployeeAdaDto.setShiftIn(cekShiftEmployeeDtos.getShiftIn());
        cekShiftEmployeeAdaDto.setShiftOut(cekShiftEmployeeDtos.getShiftOut());
        cekShiftEmployeeAdaDto.setOvertimeAfter(cekShiftEmployeeDtos.getOvertimeAfter());
        cekShiftEmployeeAdaDto.setOvertimeBefore(cekShiftEmployeeDtos.getOvertimeBefore());
        cekShiftEmployeeAdaDto.setHari(cekShiftEmployeeDtos.getHari());
        cekShiftEmployeeAdaDto.setKeterangan(cekShiftEmployeeDtos.getKeterangan());
        cekShiftEmployeeAdaDto.setStatus(cekShiftEmployeeDtos.getStatus());
        cekShiftEmployeeAdaDto.setTanggals(cekShiftEmployeeDtos.getTanggals());
        cekShiftEmployeeAdaDto.setWorkDayStatys(cekShiftEmployeeDtos.getWorkDayStatys());
        cekShiftEmployeeAdaDto.setOvertimeWorkingDay(cekShiftEmployeeDtos.getOvertimeWorkingDay());
        cekShiftEmployeeAdaDto.setOvertimeHoliday(cekShiftEmployeeDtos.getOvertimeHoliday());

        return cekShiftEmployeeAdaDto;

    }

    @GetMapping("/api/overtime/cek_shift2")
    @ResponseBody
    public CekShiftEmployeeDto cariShiftDto2(@RequestParam(required = false) String tanggal,
                                               @RequestParam(required = false) String employee){

        String date = tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(tanggal, formatter);

        CekShiftEmployeeDto cekShiftEmployeeDtos = employeeScheduleDao.cariShiftEmployee(localDate, employee);
        return cekShiftEmployeeDtos;

    }

    @GetMapping("/api/overtime/cek_masuk")
    @ResponseBody
    public CekShiftMasukDto cariShifMasuktDto(@RequestParam(required = false) String tanggal,
                                         @RequestParam(required = false) String employee,
                                         @RequestParam(required = false) String masuk){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(tanggal, formatter);

        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("h:mm a");
        LocalTime localTime = LocalTime.parse(masuk, formatter1);

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a");
        LocalDateTime localDateTime = LocalDateTime.parse(tanggal + ' '+ masuk, formatter2);


        CekShiftMasukDto cekShiftMasukDto = new CekShiftMasukDto();
        CekShiftEmployeeDto cekShiftEmployeeDtos = employeeScheduleDao.cariShiftEmployee(localDate, employee);
        cekShiftMasukDto.setStatus("OFF");

        if(cekShiftEmployeeDtos.getStatus().equals("ON")) {
            if (localTime.isAfter(cekShiftEmployeeDtos.getOvertimeBefore())) {
                if(localTime.isAfter(cekShiftEmployeeDtos.getShiftOut())){
                    cekShiftMasukDto.setStatus("ON");
                }else {
                    cekShiftMasukDto.setStatus("OFF");
                }
            }else{
                cekShiftMasukDto.setStatus("ON");
            }
        }else{
           cekShiftMasukDto.setStatus("ON");
        }
        cekShiftMasukDto.setJamMasuk(localTime);
        cekShiftMasukDto.setOvertimeBefore(cekShiftEmployeeDtos.getOvertimeBefore());
        cekShiftMasukDto.setShiftMasuk(cekShiftEmployeeDtos.getShiftIn());
        cekShiftMasukDto.setShiftKeluar(cekShiftEmployeeDtos.getShiftOut());
        cekShiftMasukDto.setOverTimeAfter(cekShiftEmployeeDtos.getOvertimeAfter());
        cekShiftMasukDto.setTanggal(localDate);

        return cekShiftMasukDto;
    }

    @GetMapping("/api/overtime/cek_keluar")
    @ResponseBody
    public CekShiftKeluarDto cariShifKeluar(@RequestParam(required = false) String tanggal,
                                            @RequestParam(required = false) String employee,
                                            @RequestParam(required = false) String masuk,
                                            @RequestParam(required = false) String keluar){

        System.out.println("tanggal : " + tanggal);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.parse(tanggal, formatter);

        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("h:mm a");
        LocalTime localTime = LocalTime.parse(keluar, formatter1);

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm a");
        LocalDateTime localDateTimeMasuk = LocalDateTime.parse(tanggal + ' '+ masuk, formatter2);
        LocalDateTime localDateTimeKeluar = LocalDateTime.parse(tanggal + ' '+ keluar, formatter2);

        CekShiftKeluarDto cekShiftKeluarDto = new CekShiftKeluarDto();
        CekShiftEmployeeDto cekShiftEmployeeDtos = employeeScheduleDao.cariShiftEmployee(localDate, employee);
        cekShiftKeluarDto.setStatusMasuk("ON");
        cekShiftKeluarDto.setStatusKeluar("ON");
        cekShiftKeluarDto.setKeteranganMasuk("");
        cekShiftKeluarDto.setKeteranganKeluar("");

        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime shiftIn = LocalDateTime.parse(cekShiftEmployeeDtos.getTanggals().toString() + ' '+ cekShiftEmployeeDtos.getShiftIn().toString(), formatter3);

        LocalDateTime shiftOut = LocalDateTime.parse(cekShiftEmployeeDtos.getTanggals().toString() + ' '+ cekShiftEmployeeDtos.getShiftOut().toString(), formatter3);

        LocalDateTime overtimeBefore = LocalDateTime.parse(cekShiftEmployeeDtos.getTanggals().toString() + ' '+ cekShiftEmployeeDtos.getOvertimeBefore().toString(), formatter3);

        LocalDateTime overtimeAfter = LocalDateTime.parse(cekShiftEmployeeDtos.getTanggals().toString() + ' '+ cekShiftEmployeeDtos.getOvertimeAfter().toString(), formatter3);

        if(localDateTimeMasuk.isAfter(localDateTimeKeluar)){
            cekShiftKeluarDto.setStatusMasuk("OFF");
            cekShiftKeluarDto.setKeteranganMasuk("Waktu awal overtime tidak boleh lebih besar dari waktu akhir overtime. lihat petunjuk diatas..");
        }else {
            if (cekShiftEmployeeDtos.getStatus().equals("ON")) {
                if (localDateTimeMasuk.isAfter(overtimeBefore)) {
                    if (localDateTimeMasuk.isBefore(shiftOut)) {
                        cekShiftKeluarDto.setStatusMasuk("OFF");
                        cekShiftKeluarDto.setKeteranganMasuk("Pengisian waktu awal overtime salah. lihat petunjuk diatas..");
                    } else {
                        cekShiftKeluarDto.setKeteranganMasuk("OK");
                        if (localDateTimeKeluar.isBefore(overtimeAfter)) {
                            cekShiftKeluarDto.setStatusMasuk("OFF");
                            cekShiftKeluarDto.setKeteranganKeluar("Waktu akhir overtime belum melebihi ketentuan. lihat petunjuk diatas..");
                        }else{
                            if (localDateTimeMasuk.plusHours(3).isBefore(localDateTimeKeluar)) {
                                cekShiftKeluarDto.setStatusMasuk("OFF");
                                cekShiftKeluarDto.setStatusKeluar("OFF");
                                cekShiftKeluarDto.setKeteranganMasuk("Lembur tidak boleh lebih dari 3 jam");
                                cekShiftKeluarDto.setKeteranganKeluar("Lembur tidak boleh lebih dari 3 jam");
                            } else {
                                cekShiftKeluarDto.setStatusMasuk("ON");
                                cekShiftKeluarDto.setStatusKeluar("ON");
                                cekShiftKeluarDto.setKeteranganMasuk("OK");
                                cekShiftKeluarDto.setKeteranganKeluar("OK");
                            }
                        }
                    }
                } else {
                    cekShiftKeluarDto.setKeteranganMasuk("OK");
                    if (localDateTimeKeluar.isAfter(shiftIn)) {
                        cekShiftKeluarDto.setStatusMasuk("OFF");
                        cekShiftKeluarDto.setKeteranganKeluar("Pengisian waktu selesai overtime salah. lihat petunjuk diatas..");
                    }else{
                        if (localDateTimeMasuk.plusHours(3).isBefore(localDateTimeKeluar)) {
                            cekShiftKeluarDto.setStatusMasuk("OFF");
                            cekShiftKeluarDto.setStatusKeluar("OFF");
                            cekShiftKeluarDto.setKeteranganMasuk("Lembur tidak boleh lebih dari 3 jam");
                            cekShiftKeluarDto.setKeteranganKeluar("Lembur tidak boleh lebih dari 3 jam");
                        } else {
                            cekShiftKeluarDto.setStatusMasuk("ON");
                            cekShiftKeluarDto.setStatusKeluar("ON");
                            cekShiftKeluarDto.setKeteranganMasuk("OK");
                            cekShiftKeluarDto.setKeteranganKeluar("OK");
                        }
                    }
                }
            } else {
                if (localDateTimeMasuk.plusHours(5).isBefore(localDateTimeKeluar)) {
                    cekShiftKeluarDto.setStatusMasuk("OFF");
                    cekShiftKeluarDto.setStatusKeluar("OFF");
                    cekShiftKeluarDto.setKeteranganMasuk("Lembur tidak boleh lebih dari 5 jam");
                    cekShiftKeluarDto.setKeteranganKeluar("Lembur tidak boleh lebih dari 5 jam");
                } else {
                    cekShiftKeluarDto.setStatusMasuk("ON");
                    cekShiftKeluarDto.setStatusKeluar("ON");
                    cekShiftKeluarDto.setKeteranganMasuk("OK");
                    cekShiftKeluarDto.setKeteranganKeluar("OK");
                }
            }
        }

        cekShiftKeluarDto.setJamMasuk(localTime);
        cekShiftKeluarDto.setOvertimeBefore(cekShiftEmployeeDtos.getOvertimeBefore());
        cekShiftKeluarDto.setShiftMasuk(cekShiftEmployeeDtos.getShiftIn());
        cekShiftKeluarDto.setShiftKeluar(cekShiftEmployeeDtos.getShiftOut());
        cekShiftKeluarDto.setOverTimeAfter(cekShiftEmployeeDtos.getOvertimeAfter());

        return cekShiftKeluarDto;
    }

}
