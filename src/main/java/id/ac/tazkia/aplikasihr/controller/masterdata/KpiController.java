package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.PositionKpiDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.PeriodeEvaluasiKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class KpiController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private PositionKpiDao positionKpiDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @GetMapping("/masterdata/kpi")
    public String kpi(Model model,
                      @RequestParam(required = false) String search,
                      @PageableDefault(size = 10) Pageable page,
                      Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPositionKpi", positionKpiDao.listPositionKpiSearch(idDepartements,search,page));
        }else {
            model.addAttribute("listPositionKpi", positionKpiDao.listPositionKpi(idDepartements,page));
        }

        model.addAttribute("page", page);
        model.addAttribute("search", search);
        model.addAttribute("menusettingkpi", "active");

        return "masterdata/kpi/list";
    }


    @GetMapping("/masterdata/kpi/setting")
    public String kpiSetting(Model model,
                             @RequestParam(required = true) JobPosition position,
                             @RequestParam(required = false) Integer page,
                             @RequestParam(required = false) Integer size,
                             @RequestParam(required = false) String search,
                             @RequestParam(required = false) PositionKpi positionKpi,
                             Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("search", search);
        model.addAttribute("menusettingkpi", "active");
        model.addAttribute("jobPosition", position);
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("postitionKpi", positionKpiDao.findByStatusAndJobPosition(StatusRecord.AKTIF, position));
        model.addAttribute("periodeEvaluasiKpi", PeriodeEvaluasiKpi.values());
        if (positionKpi == null){
            model.addAttribute("positionKpi", new PositionKpi());
        }else{
            model.addAttribute("positionKpi", positionKpi);
        }


        return "masterdata/kpi/setting_form";
    }

    @PostMapping("/masterdata/kpi/setting/save")
    public String positionKpiSave(Model model,
                                  @ModelAttribute @Valid PositionKpi positionKpi,
                                  BindingResult bindingResult,
                                  @RequestParam(required = true) JobPosition position,
                                  @RequestParam(required = false) Integer page,
                                  @RequestParam(required = false) Integer size,
                                  @RequestParam(required = false) String search,
                                  RedirectAttributes redirectAttributes,
                                  Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);


        // Check for validation errors
        if (bindingResult.hasErrors()) {
            model.addAttribute("eroredit", null);
            model.addAttribute("employess", employess);
            model.addAttribute("jobPosition", position);
            model.addAttribute("postitionKpi", positionKpiDao.findByStatusAndJobPosition(StatusRecord.AKTIF, position));
            model.addAttribute("periodeEvaluasiKpi", PeriodeEvaluasiKpi.values());
            model.addAttribute("positionKpi", positionKpi); // Retain the submitted data
            model.addAttribute("search", search);
            model.addAttribute("page", page);
            model.addAttribute("size", size);
            model.addAttribute("menusettingkpi", "active");
            // Here, you need to return the modal view directly if you're using Thymeleaf or similar
            return "masterdata/kpi/setting_form"; // Assuming you have a fragment for the modal
        }

        System.out.println("jobposition : "+ position.getId());
        positionKpi.setJobPosition(position);
        positionKpi.setStatus(StatusRecord.AKTIF);
        positionKpi.setDateUpdate(LocalDateTime.now());
        positionKpi.setUserUpdate(user.getUsername());
        String cleanedString = positionKpi.getNominalString().replace(".", "");
        positionKpi.setNominal(new BigDecimal(cleanedString));
        positionKpiDao.save(positionKpi);


        redirectAttributes.addFlashAttribute("berhasil", "Save Data success");
        return "redirect:../setting?position="+ position.getId()+"&search="+search+"&page="+page+"&size="+size;
    }


    @GetMapping("/masterdata/kpi/setting/data/{id}")
    @ResponseBody
    public ResponseEntity<?> getPositionKpiData(@PathVariable("id") String id) {
        try {
            PositionKpi positionKpi = positionKpiDao.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid KPI ID: " + id));
            return ResponseEntity.ok(positionKpi);  // Jika ditemukan, return 200 OK dengan data KPI
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error: " + e.getMessage());  // Jika tidak ditemukan, return 404
        }
    }


//    @PostMapping("/masterdata/kpi/setting/update")
//    @ResponseBody
//    public ResponseEntity<?> updateKpi(@RequestBody PositionKpi updatedKpi) {
//        PositionKpi existingKpi = positionKpiDao.findById(updatedKpi.getId())
//                .orElseThrow(() -> new IllegalArgumentException("Invalid KPI ID: " + updatedKpi.getId()));
//
//        // Update data KPI dengan nilai yang baru
//        existingKpi.setKpi(updatedKpi.getKpi());
//
//        // Simpan perubahan ke database
//        positionKpiDao.save(existingKpi);
//
//        return ResponseEntity.ok("KPI successfully updated!");
//    }




    @PostMapping("/masterdata/kpi/setting/delete")
    public String positionKpiDelete(@RequestParam PositionKpi positionKpi,
                                  @RequestParam(required = true) JobPosition position,
                                  @RequestParam(required = false) Integer page,
                                  @RequestParam(required = false) Integer size,
                                  @RequestParam(required = false) String search,
                                  RedirectAttributes redirectAttributes,
                                  Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        System.out.println("ID : "+ positionKpi.getId());
        System.out.println("jobposition : "+ position.getId());
        positionKpi.setStatus(StatusRecord.HAPUS);
        positionKpi.setDateUpdate(LocalDateTime.now());
        positionKpi.setUserUpdate(user.getUsername());
        positionKpiDao.save(positionKpi);

        redirectAttributes.addFlashAttribute("berhasil","Delete Data success");
        return "redirect:../setting?position="+ position.getId()+"&search="+search+"&page="+page+"&size="+size;
    }

}
