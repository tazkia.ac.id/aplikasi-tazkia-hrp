package id.ac.tazkia.aplikasihr.controller.setting;

import id.ac.tazkia.aplikasihr.controller.masterdata.EmployesDetailController;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobPositionDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class EmployeeJobPositionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    @Value("${upload.jobpositionemployee}")
    private String uploadFolder;

    @GetMapping("/setting/employes/job")
    public String listEmployesJob(Model model,
                                  @RequestParam(required = true)Employes employes,
                                  @RequestParam(required = false) String search,
                                  @PageableDefault(size = 10) Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("employes", employes);
                model.addAttribute("search", search);
                model.addAttribute("listEmployeeJobPosition", employeeJobPositionDao.findByStatusAndEmployesAndPositionPositionNameContainingIgnoreCaseOrderByStatusAktif(StatusRecord.AKTIF, employes, search, page));
            } else {
                model.addAttribute("employes", employes);
                model.addAttribute("listEmployeeJobPosition", employeeJobPositionDao.findByStatusAndEmployesOrderByStatusAktif(StatusRecord.AKTIF, employes, page));
            }
        }else{
            if (StringUtils.hasText(search)) {
                model.addAttribute("employes", employes);
                model.addAttribute("search", search);
                model.addAttribute("listEmployeeJobPosition", employeeJobPositionDao.findByStatusAndEmployesAndPositionPositionNameContainingIgnoreCaseOrderByStatusAktif(StatusRecord.AKTIF, employes, search, page));
            } else {
                model.addAttribute("employes", employes);
                model.addAttribute("listEmployeeJobPosition", employeeJobPositionDao.findByStatusAndEmployesOrderByStatusAktif(StatusRecord.AKTIF, employes, page));
            }
        }

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        return "setting/job_position/list";

    }

    @GetMapping("/setting/employes/job/new")
    public String newEmployesJob(Model model,
                                 @RequestParam(required = true) Employes employes,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("employes", employes);
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");

        if(role != null) {
            model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF));
        }else{
            List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
            model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord.AKTIF, idDepartements));
        }
        model.addAttribute("employeeJobPosition", new EmployeeJobPosition());

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        return "setting/job_position/form";

    }

    @GetMapping("/setting/employes/job/edit")
    public String editEmployesJob(Model model,
                                  @RequestParam(required = true) EmployeeJobPosition employeeJobPosition,
                                  RedirectAttributes attribute,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        EmployeeJobPosition employeeJobPosition1 = employeeJobPositionDao.findByStatusAndId(StatusRecord.AKTIF, employeeJobPosition.getId());
        UserRole role = userRoleDao.findByUserAndRoleId(user,"superadmin");



        if(employeeJobPosition1 == null){
            attribute.addFlashAttribute("sudahhapus", "Save Data Success");
            return "redirect:../job?employes="+employeeJobPosition.getEmployes().getId();

        }else {
            model.addAttribute("employes", employeeJobPosition.getEmployes());
            if(role != null) {
                model.addAttribute("listPosition", jobPositionDao.findByStatusOrderByPositionCode(StatusRecord.AKTIF));
            }else{
                List<String> idDepartements = employeeJobPositionDao.cariIdCompaniesKaryawan(employess.getId());
                model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentCompaniesIdInOrderByPositionCode(StatusRecord.AKTIF, idDepartements));
            }
            model.addAttribute("employeeJobPosition", employeeJobPosition1);

            model.addAttribute("menuemployes", "active");
            model.addAttribute("masterdata", "active");
            return "setting/job_position/form";
        }



    }

    @PostMapping("/setting/employes/job/save")
    public String saveEmployesJob(@ModelAttribute @Valid EmployeeJobPosition employeeJobPosition,
                                  @RequestParam(required = true) Employes employes,
                                  @RequestParam("fileUpload") MultipartFile file,
                                  Authentication authentication,
                                  RedirectAttributes attribute) throws IOException {

        User user = currentUserService.currentUser(authentication);

        String date = employeeJobPosition.getStartDateString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        employeeJobPosition.setStartDate(localDate);

        String date1 = employeeJobPosition.getEndDateString();
        String tahun1 = date1.substring(6,10);
        String bulan1 = date1.substring(0,2);
        String tanggal1 = date1.substring(3,5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        employeeJobPosition.setEndDate(localDate1);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employeeJobPosition.getFile() == null){
                employeeJobPosition.setFile("default.jpg");
            }
        }else{
            employeeJobPosition.setFile(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

//        if (employeeJobPosition.getStatus()==null){
//            employeeJobPosition.setStatus(StatusRecord.HAPUS);
//        }
        if (employeeJobPosition.getStatusAktif() == null){
            employeeJobPosition.setStatusAktif(StatusRecord.NONAKTIF);
        }

        employeeJobPosition.setUserUpdate(user.getUsername());
        employeeJobPosition.setDateUpdate(LocalDateTime.now());

        employeeJobPositionDao.save(employeeJobPosition);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../job?employes="+employes.getId();

    }

    @PostMapping("/setting/employes/job/delete")
    public String deleteJobPosition(@RequestParam(required = true)EmployeeJobPosition employeeJobPosition,
                                    Authentication authentication,
                                    RedirectAttributes attribute){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findById(employeeJobPosition.getEmployes().getId()).get();

        employeeJobPosition.setStatus(StatusRecord.HAPUS);
        employeeJobPosition.setDateUpdate(LocalDateTime.now());
        employeeJobPosition.setUserUpdate(user.getUsername());
        employeeJobPositionDao.save(employeeJobPosition);

        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../job?employes="+employes.getId();

    }


    @GetMapping("/setting/employee/job/{employeeJobPosition}/file/")
    public ResponseEntity<byte[]> tampilkanFileJobPosition(@PathVariable EmployeeJobPosition employeeJobPosition) throws Exception {
        String lokasiFile = uploadFolder + File.separator + employeeJobPosition.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeJobPosition.getFile().toLowerCase().endsWith("jpeg") || employeeJobPosition.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeJobPosition.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeJobPosition.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
