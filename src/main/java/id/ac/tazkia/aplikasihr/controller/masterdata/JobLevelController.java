package id.ac.tazkia.aplikasihr.controller.masterdata;

import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.JobLevelDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import id.ac.tazkia.aplikasihr.export.ExportEmployesClass;
import id.ac.tazkia.aplikasihr.export.ExportJobLevelClass;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class JobLevelController {

    @Autowired
    private JobLevelDao jobLevelDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @GetMapping("/masterdata/job_level")
    public String listJobLevel(Model model,
                               @RequestParam(required = false) String search,
                               @PageableDefault(size = 10)Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        if (StringUtils.hasText(search)) {
            model.addAttribute("listJobLevel", jobLevelDao.findByStatusAndJobLevelOrderByJobLevel(StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listJobLevel", jobLevelDao.findByStatusOrderByJobLevel(StatusRecord.AKTIF, page));
        }

        model.addAttribute("menujoblevel", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/job_level/list";

    }

    @GetMapping("/masterdata/job_level/new")
    public String newJobLevel(Model model,
                              Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("jobLevel", new JobLevel());

        model.addAttribute("menujoblevel", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/job_level/form";

    }

    @GetMapping("/masterdata/job_level/edit")
    public String editJoblevel(Model model,
                               @RequestParam(required = true) JobLevel jobLevel,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("jobLevel", jobLevel);

        model.addAttribute("menujoblevel", "active");
        model.addAttribute("masterdata", "active");
        return "masterdata/job_level/form";

    }

    @PostMapping("/masterdata/job_level/save")
    public String saveJoblevel(@ModelAttribute @Valid JobLevel jobLevel,
                               Authentication authentication,
                               RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        jobLevel.setDateUpdate(LocalDateTime.now());
        jobLevel.setUserUpdate(user.getUsername());
        jobLevel.setStatus(StatusRecord.AKTIF);

        jobLevelDao.save(jobLevel);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../job_level";

    }

    @PostMapping("/masterdata/job_level/delete")
    public String deleteJobLevel(@ModelAttribute @Valid JobLevel jobLevel,
                                 Authentication authentication,
                                 RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        jobLevel.setDateUpdate(LocalDateTime.now());
        jobLevel.setUserUpdate(user.getUsername());
        jobLevel.setStatus(StatusRecord.HAPUS);

        jobLevelDao.save(jobLevel);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../job_level";

    }

    @GetMapping("/masterdata/job_level/export/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Job_level_"+ currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<JobLevel> jobLevelList = jobLevelDao.findByStatusOrderByJobLevel(StatusRecord.AKTIF);

        ExportJobLevelClass excelExporter = new ExportJobLevelClass(jobLevelList);

        excelExporter.export(response);

    }

    @PostMapping("/masterdata/job_level/import")
    public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile,
                                       RedirectAttributes attributes,
                                       Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);

        List<JobLevel> jobLevelList = new ArrayList<JobLevel>();
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());



        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {

            XSSFRow row = worksheet.getRow(i);

//            if (row.getCell(0).getStringCellValue() == null || row.getCell(0).getStringCellValue() == ""){
                JobLevel jobLevel2 = jobLevelDao.findByJobLevelAndStatus(row.getCell(0).getStringCellValue(), StatusRecord.AKTIF);
                if (jobLevel2 == null){
                    JobLevel jobLevel1 = new JobLevel();
                    jobLevel1.setJobLevel(row.getCell(0).getStringCellValue());
                    jobLevel1.setDateUpdate(LocalDateTime.now());
                    jobLevel1.setUserUpdate(user.getUsername());
                    jobLevelDao.save(jobLevel1);
                }else{
                    jobLevel2.setJobLevel(row.getCell(0).getStringCellValue());
                    jobLevel2.setDateUpdate(LocalDateTime.now());
                    jobLevel2.setUserUpdate(user.getUsername());
                    jobLevelDao.save(jobLevel2);
                }
//            }else{
//                JobLevel jobLevel = jobLevelDao.findById(row.getCell(0).getStringCellValue()).get();
//                if (jobLevel == null) {
//                    JobLevel jobLevel1 = new JobLevel();
//                    jobLevel1.setJobLevel(row.getCell(1).getStringCellValue());
//                    jobLevel1.setDateUpdate(LocalDateTime.now());
//                    jobLevel1.setUserUpdate(user.getUsername());
//                    jobLevelDao.save(jobLevel1);
//                }else{
//                    jobLevel.setJobLevel(row.getCell(1).getStringCellValue());
//                    jobLevel.setDateUpdate(LocalDateTime.now());
//                    jobLevel.setUserUpdate(user.getUsername());
//                    jobLevelDao.save(jobLevel);
//                }
//            }

        }

        attributes.addFlashAttribute("successimpor", "Save Data Success");
        return "redirect:../job_level";
    }

}
