package id.ac.tazkia.aplikasihr.controller.masterdata;


import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKartuIdentitas;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeKartuKeluarga;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Controller
public class EmployesDetailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployesDetailController.class);

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeAyahDao employeeAyahDao;

    @Autowired
    private EmployeeIbuDao employeeIbuDao;

    @Autowired
    private EmployeePasanganMenikahDao employeePasanganMenikahDao;

    @Autowired
    private EmployeeAnakDao employeeAnakDao;

    @Autowired
    private EmployeeRiwayatPendidikanDao employeeRiwayatPendidikianDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private BankDao bankDao;

    @Autowired
    private EmployeeKartuIdentitasDao employeeKartuIdentitasDao;

    @Autowired
    private EmployeeKartuKeluargaDao employeeKartuKeluargaDao;

    @Autowired
    @Value("${upload.fotoemployee}")
    private String uploadFolder;

    @Autowired
    @Value("${upload.kartuidentitas}")
    private String uploadFolderKartuIdentitas;


    @Autowired
    @Value("${upload.kartukeluarga}")
    private String uploadFolderKartuKeluarga;



    @GetMapping("/masterdata/employes/detail")
    public String detailEmployee(Model model,
                                 @RequestParam(required = false) Employes employes,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);

        model.addAttribute("listCompany", companiesDao.findByStatusNotOrderByCompanyName(StatusRecord.HAPUS));
        model.addAttribute("listReligion", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));
        model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
        model.addAttribute("employes", employes);

        model.addAttribute("menuemployes", "active");
        model.addAttribute("masterdata", "active");
        model.addAttribute("employeeDetail", "active");
        return "masterdata/employes/detail";

    }

    @PostMapping("/masterdata/employes/foto/save")
    public String simpanFotoProfile(@RequestParam(required = true)Employes employes,
                                    @RequestParam("foto") MultipartFile file)throws IOException{

        Employes employes1 = employesDao.findById(employes.getId()).get();

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(employes1.getFileFoto() == null){
                employes1.setFileFoto("default.jpg");
            }
        }else{
            employes1.setFileFoto(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }
        employesDao.save(employes1);
        return "redirect:../foto?employes="+ employes.getId();
    }

    @PostMapping("/masterdata/employes/detail/save")
    @Transactional
    public String saveDetailEmployee(@ModelAttribute @Valid Employes employes,
                                     Model model,
                                     Authentication authentication,
                                     RedirectAttributes attribute) throws IOException {

//        @RequestParam("foto") MultipartFile file,

        User user = currentUserService.currentUser(authentication);
        Employes employess = employesDao.findByUser(user);
        model.addAttribute("employess", employess);
        System.out.println("Tanggal lahir : "+ employes.getTanggalLahir());
        String date = employes.getTanggalLahir();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(employes.getTanggalLahir(), formatter);
        employes.setDateOfBirth(localDate);

        employes.setDateUpdate(LocalDateTime.now());
        employes.setUserUpdate(user.getUsername());

//        if(file.getResource().exists()){

//        }

        employesDao.save(employes);
        attribute.addFlashAttribute("success", "Save Data Success");
        return "redirect:../detail?employes="+ employes.getId();

    }







    @GetMapping("/masterdata/employee/{employes}/foto/")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable Employes employes) throws Exception {
        String lokasiFile = uploadFolder + File.separator + employes.getFileFoto();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employes.getFileFoto().toLowerCase().endsWith("jpeg") || employes.getFileFoto().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employes.getFileFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employes.getFileFoto().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/masterdata/employee/{employes}/kartu_identitas/")
    public ResponseEntity<byte[]> tampolkanKartuIdentitasEmployee(@PathVariable Employes employes) throws Exception {
        EmployeeKartuIdentitas employeeKartuIdentitas = employeeKartuIdentitasDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        String lokasiFile = uploadFolderKartuIdentitas + File.separator + employeeKartuIdentitas.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeKartuIdentitas.getFile().toLowerCase().endsWith("jpeg") || employeeKartuIdentitas.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeKartuIdentitas.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeKartuIdentitas.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/masterdata/employee/{employes}/kartu_keluarga/")
    public ResponseEntity<byte[]> tampolkanKartuKeluargaEmployee(@PathVariable Employes employes) throws Exception {
        EmployeeKartuKeluarga employeeKartuKeluarga = employeeKartuKeluargaDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes);
        String lokasiFile = uploadFolderKartuKeluarga + File.separator + employeeKartuKeluarga.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (employeeKartuKeluarga.getFile().toLowerCase().endsWith("jpeg") || employeeKartuKeluarga.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (employeeKartuKeluarga.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (employeeKartuKeluarga.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
