package id.ac.tazkia.aplikasihr.controller.approval;

import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.EmployesDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.ApprovalRequestDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequest;
import id.ac.tazkia.aplikasihr.entity.recruitment.RecruitmentRequestApproval;
import id.ac.tazkia.aplikasihr.entity.setting.ApprovalRequest;
import id.ac.tazkia.aplikasihr.entity.setting.EmployeeJobPosition;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ApprovalRecruitmentController {

    @Autowired
    private RecruitmentRequestDao recruitmentRequestDao;

    @Autowired
    private RecruitmentRequestApprovalDao recruitmentRequestApprovalDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private ApprovalRequestDao approvalRequestDao;

    @GetMapping("/approval/recruitment")
    private String listRequestRecruitment(Model model, @PageableDefault(size = 10) Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listRequest", recruitmentRequestDao.listApprovalRecruitment(employes.getId(), page));

        model.addAttribute("approvalRecruitment", "active");
        return "recruitment/approval/list";
    }

    @GetMapping("/approval/recruitment/approve")
    private String approveRequest(@RequestParam String request, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        RecruitmentRequest recruitmentRequest = recruitmentRequestDao.findById(request).get();

        List<String> employeeRequest = employeeJobPositionDao.listJobPosition(recruitmentRequest.getEmployes().getId());
        List<String> employeeApprove = employeeJobPositionDao.listJobPosition(employes.getId());

        List<ApprovalRequest> approvalRequests = approvalRequestDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeApprove, employeeRequest, recruitmentRequest.getNumber());

        for (ApprovalRequest ar : approvalRequests){
            RecruitmentRequestApproval recruitmentRequestApproval = new RecruitmentRequestApproval();
            recruitmentRequestApproval.setRecruitmentRequest(recruitmentRequest);
            recruitmentRequestApproval.setEmployes(employes);
            recruitmentRequestApproval.setApprovalRequest(ar);
            recruitmentRequestApproval.setStatus(StatusRecord.AKTIF);
            recruitmentRequestApproval.setStatusApprove(StatusRecord.APPROVED);
            recruitmentRequestApproval.setNumber(recruitmentRequest.getNumber());
            recruitmentRequestApproval.setKeterangan("Approve");
            recruitmentRequestApproval.setTanggalApprove(LocalDate.now());
            recruitmentRequestApproval.setDateUpdate(LocalDate.now());
            recruitmentRequestApproval.setUserUpdate(user.getUsername());

            recruitmentRequestApprovalDao.save(recruitmentRequestApproval);
        }

        Integer sisaApprovalRecruitment = approvalRequestDao.sisaApprovalRecruitment(employeeRequest, recruitmentRequest.getNumber(), recruitmentRequest.getId());

        if (sisaApprovalRecruitment == 0) {
            recruitmentRequest.setNumber(recruitmentRequest.getNumber() + 1);
            recruitmentRequestDao.save(recruitmentRequest);
        }

        Integer sisaApprovalRecruitmentTotal = approvalRequestDao.sisaAppprovalRecruitmentTotal(employeeRequest, recruitmentRequest.getId());
        if (sisaApprovalRecruitmentTotal == 0) {

            recruitmentRequest.setStatusApprove(StatusRecord.APPROVED);
            recruitmentRequestDao.save(recruitmentRequest);

        }

        return "redirect:../recruitment";
    }

    @GetMapping("/approval/recruitment/konfirmasi")
    public String konfirmasiReject(Model model, @RequestParam String request, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("request", request);

        return "/recruitment/approval/reject";
    }

    @PostMapping("/approval/recruitment/reject")
    public String rejectRecruitment(@RequestParam String request, Authentication authentication,
                                    @RequestParam(required = false) String reason){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        RecruitmentRequest recruitmentRequest = recruitmentRequestDao.findById(request).get();

        List<String> employeeRequest = employeeJobPositionDao.listJobPosition(recruitmentRequest.getEmployes().getId());
        List<String> employeeApprove = employeeJobPositionDao.listJobPosition(employes.getId());

        List<ApprovalRequest> approvalRequests = approvalRequestDao.findByStatusAndPositionApproveIdInAndPositionIdInAndSequenceOrderByPosition(StatusRecord.AKTIF, employeeApprove, employeeRequest, recruitmentRequest.getNumber());

        for (ApprovalRequest ar : approvalRequests){
            RecruitmentRequestApproval recruitmentRequestApproval = new RecruitmentRequestApproval();
            recruitmentRequestApproval.setRecruitmentRequest(recruitmentRequest);
            recruitmentRequestApproval.setEmployes(employes);
            recruitmentRequestApproval.setApprovalRequest(ar);
            recruitmentRequestApproval.setStatus(StatusRecord.AKTIF);
            recruitmentRequestApproval.setStatusApprove(StatusRecord.REJECTED);
            recruitmentRequestApproval.setNumber(recruitmentRequest.getNumber());
            recruitmentRequestApproval.setKeterangan(reason);
            recruitmentRequestApproval.setTanggalApprove(LocalDate.now());
            recruitmentRequestApproval.setDateUpdate(LocalDate.now());
            recruitmentRequestApproval.setUserUpdate(user.getUsername());

            recruitmentRequestApprovalDao.save(recruitmentRequestApproval);
        }

        recruitmentRequest.setStatusApprove(StatusRecord.REJECTED);
        recruitmentRequestDao.save(recruitmentRequest);

        return "redirect:../recruitment";
    }

}
