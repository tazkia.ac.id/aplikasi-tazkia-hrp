package id.ac.tazkia.aplikasihr.controller.request;

import id.ac.tazkia.aplikasihr.dao.config.RoleDao;
import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.masterdata.*;
import id.ac.tazkia.aplikasihr.dao.recruitment.*;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestApprovalDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestDao;
import id.ac.tazkia.aplikasihr.dao.request.RecruitmentRequestSupportFileDao;
import id.ac.tazkia.aplikasihr.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.aplikasihr.entity.JenisUndangan;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.StatusWawancara;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeType;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import id.ac.tazkia.aplikasihr.entity.recruitment.*;
import id.ac.tazkia.aplikasihr.services.CurrentUserService;
import id.ac.tazkia.aplikasihr.services.EmailAnnouncementService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class RecruitmentRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecruitmentRequest.class);

    @Autowired
    private AgamaDao agamaDao;
    @Autowired
    private RecruitmentRequestDao recruitmentRequestDao;

    @Autowired
    private RecruitmentRequestApprovalDao recruitmentRequestApprovalDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployeeTypeDao employeeTypeDao;

    @Autowired
    private JobPositionDao jobPositionDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private RecruitmentRequestSupportFileDao supportFileDao;

    @Autowired
    private ApplicantDao applicantDao;

    @Autowired
    private ApplicantRecruitmentRequestDao applicantRequestDao;

    @Autowired
    private ApplicantCertificateDao certificateDao;

    @Autowired
    private ApplicantIdentityFileDao identityFileDao;

    @Autowired
    private ApplicantFileSupportDao applicantFileSupportDao;

    @Autowired
    private ApplicantScheduleDao scheduleDao;

    @Autowired
    private EmailAnnouncementService announcementService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    @Value("${upload.flyerrecruitment}")
    private String uploadFile;

    @Autowired
    @Value("${upload.fotoapplicant}")
    private String fotoApplicant;

    @GetMapping("/request/recruitment")
    public String requestRecruitment(Model model, @PageableDefault(size = 10)Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listRequest", recruitmentRequestDao.findByStatusAndEmployesOrderByDateRequestDesc(StatusRecord.AKTIF, employes, page));
        model.addAttribute("listApproval", recruitmentRequestDao.listApprovalRequestRecruitment(employes.getId()));

        model.addAttribute("request", "active");
        model.addAttribute("menurequestrecruitment", "active");

        return "request/recruitment/list";

    }

    @GetMapping("/request/recruitment/new")
    private String formRequestRecruitment(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        List<String> listPosition = employeeJobPositionDao.listJobPosition(employes.getId());
        List<String> idDepartment = new ArrayList<>();
        for (String p : listPosition){
            JobPosition position = jobPositionDao.findById(p).get();
            idDepartment.add(position.getDepartment().getId());
        }

        model.addAttribute("recruitment", new RecruitmentRequest());
        model.addAttribute("listEmployeeType", employeeTypeDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listPosition", jobPositionDao.findByStatusAndDepartmentIdInOrderByPositionCode(StatusRecord.AKTIF, idDepartment));
        model.addAttribute("listDepartment", departmentDao.findByStatusOrderByDepartmentCode(StatusRecord.AKTIF));

        model.addAttribute("request", "active");
        model.addAttribute("menurequestrecruitment", "active");

        return "request/recruitment/form";
    }

    @PostMapping("/request/recruitment/save")
    private String saveRequestRecruitment(@ModelAttribute @Valid RecruitmentRequest recruitmentRequest, Authentication authentication,
                                          RedirectAttributes attributes, @RequestParam String[] support){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        List<String> idEmployes = employeeJobPositionDao.listEmployeeJobPosition(employes.getId());
        Integer jumlahApproval = recruitmentRequestDao.jumlahApproval(idEmployes);
        Integer jumlahApproval1 = 0;
        if (jumlahApproval != null) {
            jumlahApproval1 = jumlahApproval;
        }
        Integer nomorAwal = 1;

        System.out.println("position :"+ idEmployes);
        System.out.println("nomorawal :"+ nomorAwal);
        System.out.println("jumlah :"+ jumlahApproval1);

        recruitmentRequest.setDepartment(recruitmentRequest.getPosition().getDepartment());
        recruitmentRequest.setUserUpdate(user.getUsername());
        recruitmentRequest.setDateRequest(LocalDate.now());
        recruitmentRequest.setEmployes(employes);
        recruitmentRequest.setDateRequest(LocalDate.now());
        recruitmentRequest.setNumber(nomorAwal);
        recruitmentRequest.setTotalNumber(jumlahApproval1);
        recruitmentRequest.setUnit("ORANG");
        recruitmentRequest.setOpenDate(null);
        recruitmentRequest.setExpiredDate(null);
        if (nomorAwal > jumlahApproval1){
            recruitmentRequest.setStatusApprove(StatusRecord.APPROVED);
        }else {
            recruitmentRequest.setStatusApprove(StatusRecord.WAITING);
        }

        recruitmentRequestDao.save(recruitmentRequest);

        for (String name : support){
            RecruitmentFileSupport fileSupport = new RecruitmentFileSupport();
            fileSupport.setDescription("-");
            fileSupport.setRequest(recruitmentRequest);
            fileSupport.setName(name);
            supportFileDao.save(fileSupport);
        }

        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../recruitment";
    }

    @PostMapping("/request/recruitment/delete")
    public String deleteRecruitmentRequest(@RequestParam RecruitmentRequest recruitmentRequest, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        recruitmentRequest.setStatus(StatusRecord.HAPUS);
        recruitmentRequest.setStatusApprove(StatusRecord.CANCELED);
        recruitmentRequest.setDateUpdate(LocalDate.now());
        recruitmentRequest.setUserUpdate(user.getUsername());

        List<RecruitmentRequestApproval> recruitmentRequestApprovals = recruitmentRequestApprovalDao.findByStatusAndRecruitmentRequest(StatusRecord.AKTIF, recruitmentRequest);
        for (RecruitmentRequestApproval r : recruitmentRequestApprovals){

            r.setStatus(StatusRecord.HAPUS);
            r.setStatusApprove(StatusRecord.HAPUS);
            recruitmentRequestApprovalDao.save(r);

        }
        recruitmentRequestDao.save(recruitmentRequest);
        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../recruitment";
    }

    // publish recruitment

    @GetMapping("/recruitment/publish")
    private String publishRecruitment(Model model, @PageableDefault(size = 10)Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("listRecruitment", recruitmentRequestDao.findByStatusAndStatusApproveOrderByDateRequest(StatusRecord.AKTIF, StatusRecord.APPROVED, page));

        model.addAttribute("publishRecruitment", "active");

        return "recruitment/publish/list";
    }

    @GetMapping("/recruitment/launching")
    private String publishHrd(Model model, @RequestParam RecruitmentRequest request, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        String desc = recruitmentRequestDao.descHRD(request.getId());
        String tes = desc.trim();

        model.addAttribute("recruitment", request);
        model.addAttribute("descHrd", tes);

        model.addAttribute("publishRecruitment", "active");

        return "recruitment/publish/launching";
    }

    @PostMapping("/recruitment/publish/save")
    private String savePublish(@RequestParam String id, @RequestParam("file")MultipartFile file, @RequestParam String open, @RequestParam String expired,
                               @RequestParam(required = false) String desc, Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        RecruitmentRequest request = recruitmentRequestDao.findById(id).get();

        String tahun = open.substring(6,10);
        String bulan = open.substring(0,2);
        String tanggal = open.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate openDate = LocalDate.parse(tanggalan, formatter);

        String tahun1 = expired.substring(6,10);
        String bulan1 = expired.substring(0,2);
        String tanggal1 = expired.substring(3,5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate expiredDate = LocalDate.parse(tanggalan1, formatter1);

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i+1);
        }
        String idFile = UUID.randomUUID().toString();
        request.setFile(idFile + "." + extension);
        new File(uploadFile).mkdirs();
        File tujuan = new File(uploadFile + File.separator + request.getFile());
        file.transferTo(tujuan);

        request.setDescriptionHrd(desc);
        request.setOpenDate(openDate);
        request.setExpiredDate(expiredDate);
        if (LocalDate.now().compareTo(openDate) == 0){
            request.setStatusLaunching(StatusRecord.AKTIF);
        }
        recruitmentRequestDao.save(request);

        return "redirect:../support?request="+id;

    }

    @GetMapping("/recruitment/support")
    public String inputSupport(Model model, @RequestParam RecruitmentRequest request, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("recruitment", request);

        model.addAttribute("publishRecruitment", "active");


        return "recruitment/publish/support";
    }

    @PostMapping("/recruitment/support/save")
    public String saveSupport(@RequestParam RecruitmentRequest request, @RequestParam String[] support){

        for (String name : support){
            RecruitmentFileSupport fileSupport = new RecruitmentFileSupport();
            fileSupport.setDescription("-");
            fileSupport.setRequest(request);
            fileSupport.setName(name);
            supportFileDao.save(fileSupport);
        }

        return "redirect:../publish";
    }

    // LIST APPLICANT WHO APPLY

    @GetMapping("/recruitment/applicant")
    public String listApplicant(Model model, @PageableDefault(size = 10) Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        model.addAttribute("employess", employes);

        model.addAttribute("schedule", new ApplicantSchedule());
        model.addAttribute("employes", employesDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF"));

        model.addAttribute("jenisUndangan", JenisUndangan.values());
        model.addAttribute("statusWawancara", StatusWawancara.values());
        model.addAttribute("tanggalNow", LocalDate.now());
        model.addAttribute("listApplicant", applicantDao.listApplicant(page));
        model.addAttribute("listIdentity", identityFileDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listCertificate", certificateDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listFileSupport", applicantFileSupportDao.findByStatus(StatusRecord.AKTIF));

        return "recruitment/applicant/list";
    }

    @GetMapping("/recruitment/applicant/{applicant}/applicant/")
    public ResponseEntity<byte[]> listApplicantFoto(@PathVariable Applicant applicant){
        String lokasiFile = fotoApplicant + File.separator + applicant.getFileFoto();
        LOGGER.debug("Lokasi foto: {}" + lokasiFile);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (applicant.getFileFoto().toLowerCase().endsWith("jpeg") || applicant.getFileFoto().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (applicant.getFileFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (applicant.getFileFoto().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers, HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping("/recruitment/applicant/send")
    public String applicantEmail(@ModelAttribute @Valid ApplicantSchedule schedule, @RequestParam String applicant, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        Applicant app = applicantDao.findById(applicant).get();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate date = LocalDate.parse(schedule.getTanggalString(), formatter);

        schedule.setTanggal(date);
        schedule.setApplicant(app);
        schedule.setStatus(StatusRecord.AKTIF);
        schedule.setStatusKirimEmail(StatusRecord.WAITING);
        schedule.setUserUpdate(employes.getEmailActive());
        schedule.setDateUpdate(LocalDateTime.now());
        schedule.setEmployes(employes);
        scheduleDao.save(schedule);

        return "redirect:";
    }

    @PostMapping("/recruitment/applicant/wawancara")
    public String applicantWawancara(@RequestParam String schedule, @RequestParam StatusWawancara wawancara, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        ApplicantSchedule sch = scheduleDao.findById(schedule).get();
        sch.setStatusWawancara(wawancara);
        scheduleDao.save(sch);

        return "redirect:";
    }

    // VACANCY

    @GetMapping("/vacancy/jobs_list")
    public String listJobs(Model model){

        EmployeeType tendik = employeeTypeDao.findById("1").get(); // tenaga pendidik
        EmployeeType staff = employeeTypeDao.findById("2").get(); // staff
        EmployeeType dosen = employeeTypeDao.findById("3").get(); // dosen

        List<RecruitmentRequest> requestDosen = recruitmentRequestDao.findByStatusAndStatusApproveAndStatusLaunchingAndEmployeeTypeOrderByOpenDate(StatusRecord.AKTIF, StatusRecord.APPROVED, StatusRecord.AKTIF, dosen);
        if (requestDosen.isEmpty()){
            model.addAttribute("messageDosen", "message");
        }else{
            model.addAttribute("listDosen", requestDosen);
        }

        List<RecruitmentRequest> requestStaff = recruitmentRequestDao.findByStatusAndStatusApproveAndStatusLaunchingAndEmployeeTypeOrderByOpenDate(StatusRecord.AKTIF, StatusRecord.APPROVED, StatusRecord.AKTIF, staff);
        if (requestStaff.isEmpty()) {
            model.addAttribute("messageStaff", "message");
        }else{
            model.addAttribute("listStaff", requestStaff);
        }

        List<RecruitmentRequest> requestTendik = recruitmentRequestDao.findByStatusAndStatusApproveAndStatusLaunchingAndEmployeeTypeOrderByOpenDate(StatusRecord.AKTIF, StatusRecord.APPROVED, StatusRecord.AKTIF, tendik);
        if (requestTendik.isEmpty()) {
            model.addAttribute("messageTendik", "message");
        }else{
            model.addAttribute("listTendik", requestTendik);
        }


        return "jobs";
    }

    @GetMapping("/vacancy/jobsDetail")
    public String jobsDetail(Model model, @RequestParam String id){

        RecruitmentRequest request = recruitmentRequestDao.findById(id).get();
        List<RecruitmentFileSupport> support = supportFileDao.findByRequestAndStatus(request, StatusRecord.AKTIF);

        if (support.isEmpty()) {
            model.addAttribute("support", "Tidak ada file tambahan!");
        }

        model.addAttribute("supportFile", support);

        model.addAttribute("request", request);


        return "jobsDetail";
    }

    @GetMapping("/vacancy/{request}/flyer/")
    public ResponseEntity<byte[]> flyerRecruitment(@PathVariable RecruitmentRequest request){
        String lokasiFile = uploadFile + File.separator + request.getFile();
        LOGGER.debug("Lokasi File : {}", lokasiFile);

        try{
            HttpHeaders headers = new HttpHeaders();
            if (request.getFile().toLowerCase().endsWith("jpeg") || request.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (request.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (request.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data,headers,HttpStatus.OK);
        }catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/recruitment/newEmployee")
    public String newEmployee(Model model, @RequestParam String id){

        RecruitmentRequest request = recruitmentRequestDao.findById(id).get();

        model.addAttribute("request", request);
        model.addAttribute("applicant", new Applicant());
        model.addAttribute("listReligion", agamaDao.findByStatusOrderByAgama(StatusRecord.AKTIF));

        return "formNewEmployee";

    }

    @PostMapping("/recruitment/applicant/save")
    public String saveEmployes(@ModelAttribute @Valid Applicant applicant, @RequestParam RecruitmentRequest request, RedirectAttributes attributes){

        Role pelamar = roleDao.findById("pelamar").get();

        User cekEmail = userDao.findByUsernameAndActive(applicant.getEmailActive(), true);
        if (cekEmail != null){
            attributes.addFlashAttribute("gagal", "email sudah terdaftar");
            return "redirect:../jobsDetail?id="+request.getId();
        }

        User user = new User();
        String id = UUID.randomUUID().toString();
        user.setId(id);
        user.setUsername(applicant.getEmailActive());
        user.setActive(true);
//        user.setRole(pelamar);
        userDao.save(user);

        applicant.setStatusAktif("AKTIF");
        applicant.setUser(user);
        applicant.setUserUpdate(user.getUsername());
        applicant.setDateUpdate(LocalDateTime.now());
        applicantDao.save(applicant);

        ApplicantRecruitmentRequest applicantRequest = new ApplicantRecruitmentRequest();
        applicantRequest.setApplicant(applicant);
        applicantRequest.setRequest(request);
        applicantRequest.setUserUpdate(user.getUsername());
        applicantRequest.setDateUpdate(LocalDateTime.now());
        applicantRequest.setStatus(StatusRecord.AKTIF);
        applicantRequest.setStatusAktif("AKTIF");
        applicantRequestDao.save(applicantRequest);

        attributes.addFlashAttribute("success", "daftar berhasil");
        return "redirect:../jobsDetail?id="+request.getId();
    }

}
