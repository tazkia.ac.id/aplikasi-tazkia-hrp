package id.ac.tazkia.aplikasihr;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;



@SpringBootApplication
public class AplikasiHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiHrApplication.class, args);
	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	@Bean
	public MustacheFactory mustacheFactory(){
		return new DefaultMustacheFactory();
	}


//		@Bean(name = "asyncExecutor")
//		public Executor asyncExecutor()
//		{
//			ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//			executor.setCorePoolSize(3);
//			executor.setMaxPoolSize(3);
//			executor.setQueueCapacity(100);
//			executor.setThreadNamePrefix("AsynchThread-");
//			executor.initialize();
//			return executor;
//		}


}
