package id.ac.tazkia.aplikasihr.konfigurasi;

import id.ac.tazkia.aplikasihr.dao.config.UserDao;
import id.ac.tazkia.aplikasihr.dao.config.UserRoleDao;
import id.ac.tazkia.aplikasihr.entity.config.Permission;
import id.ac.tazkia.aplikasihr.entity.config.Role;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.config.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.SecurityFilterChain;
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class KonfigurasiSecurity {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Bean @Order(1)
    public SecurityFilterChain apiSecurity(HttpSecurity http) throws Exception {
        http.securityMatcher("/impor/absen/mesin","/impor/absen/harian")
                .authorizeHttpRequests(auth -> auth
                        .anyRequest().permitAll()
                ).oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                .csrf().disable();
        return http.build();
    }

    @Bean @Order(2)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/dist/**",
                        "/depan/**",
                        "/depan/*",
                        "/login",
                        "/login2",
                        "/vacancy/**",
                        "/templates/otp_confirmation.html",
                        "/register/**",
                        "/bower_components/**",
                        "/build/**",
                        "/dist/**",
                        "/css/**",
                        "/icon/**",
                        "/yogi/*",
                        "/icons/*",
                        "/yogi/js/*",
                        "/documentation/**",
                        "/plugins/**",
                        "/",
                        "/404",
                        "/logo/**",
                        "/js/**",
                        "/js2/**",
                        "/css2/**",
                        "/bootstrap52/**",
                        "/api/attendance/approve",
                        "/api/attendance/reject",
                        "/api/time_off/approve",
                        "/api/time_off/reject",
                        "/api/tarikidabsen/pegawai",
                        "/api/jumlah-employee",
                        "/suksesapprove",
                        "/pdf/**",
                        "/images/**").permitAll()
                                .requestMatchers("/masterdata/companies").hasAnyAuthority("MENU_COMPANIES")
                                .requestMatchers("/masterdata/department").hasAnyAuthority("MENU_DEPARTMENT")
                                .requestMatchers("/masterdata/job_level").hasAnyAuthority("MENU_JOB_LEVEL")
                                .requestMatchers("/masterdata/position").hasAnyAuthority("MENU_POSITION")
                                .requestMatchers("/masterdata/employes").hasAnyAuthority("MENU_EMPLOYES")
                                .requestMatchers("/masterdata/user").hasAnyAuthority("MENU_USER")
                                .requestMatchers("/masterdata/role").hasAnyAuthority("MENU_USER")
                                .requestMatchers("/masterdata/role/add").hasAnyAuthority("MENU_USER")
                                .requestMatchers("/masterdata/role/edit").hasAnyAuthority("MENU_USER")
                                .requestMatchers("/masterdata/kpi").hasAnyAuthority("MENU_SETTING_KPI")
                                .requestMatchers("/masterdata/kpi/setting").hasAnyAuthority("MENU_SETTING_KPI")
                                .requestMatchers("/masterdata/kpi/setting/save").hasAnyAuthority("MENU_SETTING_KPI")
                                .requestMatchers("/masterdata/kpi/setting/delete").hasAnyAuthority("MENU_SETTING_KPI")
                                .requestMatchers("/masterdata/kpi/setting/data/").hasAnyAuthority("MENU_SETTING_KPI")
                                .requestMatchers("/masterdata/bank").hasAnyAuthority("MENU_BANK")
                                .requestMatchers("/setting/attendance_approval").hasAnyAuthority("MENU_SETTING_APPROVAL")
                                .requestMatchers("/setting/timemanagement/pattern").hasAnyAuthority("MENU_SETTING_TIME_MANAGEMENT")
                                .requestMatchers("/setting/timemanagement/pattern/schedule").hasAnyAuthority("MENU_SETTING_TIME_MANAGEMENT","SETTING_TIME")
                                .requestMatchers("/setting/timemanagement/employee/schedule").hasAnyAuthority("MENU_SETTING_TIME_MANAGEMENT","SETTING_TIME")
                                .requestMatchers("/setting/timemanagement/employee/schedule/view").hasAnyAuthority("MENU_SETTING_TIME_MANAGEMENT","SETTING_TIME")
                                .requestMatchers("/setting/timemanagement/employee/schedule/impor").hasAnyAuthority("MENU_SETTING_TIME_MANAGEMENT","SETTING_TIME")
                                .requestMatchers("/setting/payroll/periode").hasAnyAuthority("MENU_SETTING_PAYROLL_CONFIGURATION")
                                .requestMatchers("/setting/timeoff/jenis").hasAnyAuthority("MENU_SETTING_TIME_OFF_CONFIGURATION")
                                .requestMatchers("/setting/day/off").hasAnyAuthority("MENU_SETTING_DAY_OFF_CONFIGURATION")
                                .requestMatchers("/attendance").hasAnyAuthority("MENU_ATTENDANCE","MENU_ATTENDANCE_RUN","MENU_ATTENDANCE_REPORT")
                                .requestMatchers("/payroll").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT","MENU_PAYROLL_UPDATE","MENU_PAYROLL_RUN")
                                .requestMatchers("/payroll/update/new").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_UPDATE")
                                .requestMatchers("/payroll/update").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_UPDATE")
                                .requestMatchers("/payroll/run").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_RUN")
                                .requestMatchers("/payroll/reports/payslip").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT")
                                .requestMatchers("/payroll/reports/view").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT")
                                .requestMatchers("/payroll/update/excel").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT")
                                .requestMatchers("/payroll/update/impor").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT")
                                .requestMatchers("/payroll/report/full/detail").hasAnyAuthority("MENU_PAYROLL","MENU_PAYROLL_REPORT_DETAIL")
                        .anyRequest().authenticated()
                )
                .headers().frameOptions().sameOrigin()
                .and().logout().permitAll()
                .and().oauth2Login().loginPage("/starter").permitAll()
                .userInfoEndpoint()
                .userAuthoritiesMapper(authoritiesMapper())
                .and().defaultSuccessUrl("/dashboard", true);
        return http.build();
    }

    private GrantedAuthoritiesMapper authoritiesMapper(){
        return (authorities) -> {
            String emailAttrName = "email";
            String email = authorities.stream()
                    .filter(OAuth2UserAuthority.class::isInstance)
                    .map(OAuth2UserAuthority.class::cast)
                    .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                    .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                    .findFirst()
                    .orElse(null);

            if (email == null) {
                return authorities;     // data email tidak ada di userInfo dari Google
            }

            User user = userDao.findByUsername(email);
            if(user == null) {
                throw new IllegalStateException("Email "+email+" tidak ada dalam database");

            }

//            Set<Permission> userAuthorities = user.getRole().getPermissions();
//            System.out.println("role : "+ userAuthorities);
//            if (userAuthorities.isEmpty()) {
//                return authorities;     // authorities defaultnya ROLE_USER
//            }

            Set<Permission> userAuthorities = new HashSet<>();
            List<UserRole> userRoleList = userRoleDao.findByUser(user);
            for(UserRole userRole : userRoleList) {
                Role role = userRole.getRole();
                userAuthorities.addAll(role.getPermissions());
            }

            return Stream.concat(
                    authorities.stream(),
                    userAuthorities.stream()
                            .map(Permission::getValue)
                            .map(SimpleGrantedAuthority::new)
            ).collect(Collectors.toCollection(ArrayList::new));
        };
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }

}
