package id.ac.tazkia.aplikasihr.dto.kpi;

import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;

@Data
public class SavePenilaianKpiDto {
    private String positionKpi;
    private Employes employes;
    private String bulan;
    private String tahun;
    private String grade;
    private String nilai;
    private String jenis;
}
