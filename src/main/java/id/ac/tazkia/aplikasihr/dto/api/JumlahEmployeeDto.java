package id.ac.tazkia.aplikasihr.dto.api;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JumlahEmployeeDto {

    @Id
    private String companyId;

    private String companyName;

    private Integer total;

    private Integer male;

    private Integer female;

    private Integer permanent;

    private Integer temporary;

}
