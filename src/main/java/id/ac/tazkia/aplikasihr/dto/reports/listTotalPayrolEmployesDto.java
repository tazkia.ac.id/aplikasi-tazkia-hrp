package id.ac.tazkia.aplikasihr.dto.reports;

import java.math.BigDecimal;

public interface listTotalPayrolEmployesDto {

    String getIdEmployee();
    String getNumber();
    String getFullName();
    String getCompanyName();
    BigDecimal getTotalGajiPokok();
    BigDecimal getTotalAllowance();
    BigDecimal getTotalDeductions();
    BigDecimal getTotalBenefits();
    BigDecimal getTotalZakat();
    BigDecimal getTotalInfaq();
    BigDecimal getTotalPajak();
    BigDecimal getTakeHomePay();
    BigDecimal getPotonganAbsen();
    BigDecimal getPotonganTerlambat();
    BigDecimal getTotalLoan();

}
