package id.ac.tazkia.aplikasihr.dto;

public interface EmployeeDto {

    String getNumber();
    String getNama();

}
