package id.ac.tazkia.aplikasihr.dto;

public interface DayOffEmployeeAllowDto {

    String getId();
    String getIdEmployee();
    String getNamaEmployee();
    String getStatusAktif();

}
