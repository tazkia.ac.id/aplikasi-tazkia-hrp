package id.ac.tazkia.aplikasihr.dto.api;

import org.springframework.cglib.core.Local;

import java.time.LocalDate;
import java.time.LocalTime;

public interface EmployeeScheduleDailyDto {

    LocalDate getTanggalMasuk();

    LocalTime getShiftMasuk();

    LocalDate getTanggalKeluar();

    LocalTime getShiftKeluar();

}
