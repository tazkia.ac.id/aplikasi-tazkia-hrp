package id.ac.tazkia.aplikasihr.dto.employes;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface EmployesAktifThrDto {

    String getId();
    String getNumber();
    String getNickName();
    String getFullName();
    LocalDate getTanggalMasuk();
    LocalDate getTanggalKeluar();
    BigDecimal getBulan();
    String getStatus();

}
