package id.ac.tazkia.aplikasihr.dto.setting;

import java.time.LocalDateTime;

public interface ApprovalTimeOffRequestListDto {

    Integer getNomor();
    String getTimeOffRequest();
    String getEmployee();
    String getPosition();
    String getApprove();
    LocalDateTime getTanggalApprove();
    String getKeterangan();

}
