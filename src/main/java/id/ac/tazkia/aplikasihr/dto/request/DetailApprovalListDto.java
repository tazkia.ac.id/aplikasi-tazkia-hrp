package id.ac.tazkia.aplikasihr.dto.request;

import com.google.api.client.util.DateTime;

public interface DetailApprovalListDto {

    String getId();
    String getIdAttendanceRequest();
    String getNumber();
    String getFullName();
    String getStatusApprove();
    DateTime getTanggalApprove();
    String getKeterangan();

}
