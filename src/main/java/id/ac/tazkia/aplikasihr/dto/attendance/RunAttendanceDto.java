package id.ac.tazkia.aplikasihr.dto.attendance;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public interface RunAttendanceDto {

    String getId();
    LocalDate getTanggal();
    String getHari();
    String getShiftName();
    String getShiftIn();
    String getShiftOut();
    String getWorkDayStatus();
    String getWaktuMasuk();
    String getWaktuKeluar();
    String getStat();
    String getFlexible();
    String getAttd();

}
