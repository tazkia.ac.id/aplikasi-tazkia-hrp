package id.ac.tazkia.aplikasihr.dto.payroll;

public interface HeaderPayrollRunReportDto {

    String getNomor();
    String getJenis();
    String getNama();
}
