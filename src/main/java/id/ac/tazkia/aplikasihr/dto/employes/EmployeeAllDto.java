package id.ac.tazkia.aplikasihr.dto.employes;

import java.time.LocalDate;

public interface EmployeeAllDto {

    String getId();
    LocalDate getTanggalMulai();
    LocalDate getTanggalSelesai();

}
