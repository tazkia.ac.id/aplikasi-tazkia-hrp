package id.ac.tazkia.aplikasihr.dto.setting;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

public interface ApprovalAttendanceRequestListDto {

    Integer getNomor();
    String getAttendanceRequest();
    String getEmployee();
    String getPosition();
    String getApprove();
    LocalDateTime getTanggalApprove();
    String getKeterangan();

}
