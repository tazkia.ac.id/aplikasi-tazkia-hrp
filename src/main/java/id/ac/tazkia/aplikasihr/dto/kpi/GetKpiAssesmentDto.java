package id.ac.tazkia.aplikasihr.dto.kpi;

import java.util.List;

import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidanceDosen;
import lombok.Data;

@Data
public class GetKpiAssesmentDto {
    RekapPengisianKpiDto rekapPengisianKpi;
    List<PengisianKpi> pengisianKpi;
    List<PengisianKpiEvidance> pengisianKpiEvidances;

    List<PengisianKpiDosen> pengisianKpiDosen;
    List<PengisianKpiEvidanceDosen> pengisianKpiEvidancesDosen;
}
