package id.ac.tazkia.aplikasihr.dto.setting;

public interface ApprovalListDetailDto {

    String getId();
    String getPositionApprove();
    String getEmployeeApprove();
    String getEmail();
    String getIdEmployee();

}
