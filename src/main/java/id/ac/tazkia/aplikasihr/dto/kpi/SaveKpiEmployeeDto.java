package id.ac.tazkia.aplikasihr.dto.kpi;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class SaveKpiEmployeeDto {
    private String idKpi;
    private LocalDate tanggalPenugasan;
    private String deskripsiPenugasan;
    private LocalDate tanggal;
    private String deskripsi;
    private MultipartFile[] files;
}
