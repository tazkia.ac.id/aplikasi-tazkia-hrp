package id.ac.tazkia.aplikasihr.dto;

public interface TimeOffJensiDto {

    String getId();

    String getNama();

}
