package id.ac.tazkia.aplikasihr.dto;

import lombok.Data;

import java.time.LocalTime;

@Data
public class CekShiftKeluarDto {

    private String statusMasuk;
    private String statusKeluar;
    private LocalTime jamMasuk;
    private LocalTime overtimeBefore;
    private LocalTime overTimeAfter;
    private LocalTime shiftMasuk;
    private LocalTime shiftKeluar;
    private String keteranganMasuk;
    private String keteranganKeluar;

}
