package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface IsiDetailEmployeePayrollRunDto {

    String getNomor();
    String getJenis();
    String getNama();
    BigDecimal getNominal();

}
