package id.ac.tazkia.aplikasihr.dto.kpi;

public interface RekapPengisianKpiDto {

    String getPositionKpi();
    String getIdPosition();
    String getKpi();
    String getIndikator();
    String getIdKpi();
    Integer getJml();
    String getEmployee();

    String getSyaratAa();

    String getSyaratBb();

    String getSyaratCc();

    String getBulan();
    String getTahun();

    String getGrade();
    String getNilai();
}
