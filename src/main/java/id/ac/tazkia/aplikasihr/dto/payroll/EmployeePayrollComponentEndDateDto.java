package id.ac.tazkia.aplikasihr.dto.payroll;

import id.ac.tazkia.aplikasihr.dao.setting.payroll.PayrollPeriodeDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollPeriode;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public interface EmployeePayrollComponentEndDateDto {


    String getId();
    String getIdComponent();
    String getCodeComponent();
    String getNameComponent();
    String getIdEmployee();
    String getNip();
    String getEmployeeName();
    String getJenisComponent();
    BigDecimal getNominal();
    String getJenisNominal();
    LocalDate getEffectiveDate();
    LocalDate getEndDate();
    LocalDate getSampaiTanggal();

    default boolean isExpired(){
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate1 = LocalDate.parse(getSampaiTanggal(), formatter);
        return getEndDate().isBefore(getSampaiTanggal());
    }

}
