package id.ac.tazkia.aplikasihr.dto.setting.time_management;

import java.time.LocalDate;

public interface ListHeaderShiftDto {

    String getNumber();
    String getName();
    String getTanggal();

}
