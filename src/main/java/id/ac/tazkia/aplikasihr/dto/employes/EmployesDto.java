package id.ac.tazkia.aplikasihr.dto.employes;

import java.time.LocalDate;

public interface EmployesDto {

    String getId();
    String getNumber();
    Integer getIdAbsen();
    String getNickName();
    String getFullName();
    String getCompany();
    String getGender();
    String getStatus();
    LocalDate getTanggal();
    String getStatusEmployee();
    LocalDate getDariTanggal();
    LocalDate getSampaiTanggal();
    String getType();
    String getClasss();

}
