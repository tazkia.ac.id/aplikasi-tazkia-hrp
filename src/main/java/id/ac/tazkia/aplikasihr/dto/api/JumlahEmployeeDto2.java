package id.ac.tazkia.aplikasihr.dto.api;

import jakarta.persistence.Id;

public interface JumlahEmployeeDto2 {

    String getCompanyId();

    String getCompanyName();

    Integer getTotal();

    Integer getMale();

    Integer getFemale();

    Integer getPermanent();

    Integer getTemporary();
}
