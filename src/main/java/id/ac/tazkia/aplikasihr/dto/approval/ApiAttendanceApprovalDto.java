package id.ac.tazkia.aplikasihr.dto.approval;

import lombok.Data;

@Data
public class ApiAttendanceApprovalDto {

    private String attendance;
    private String approval;
    private String employee;

}
