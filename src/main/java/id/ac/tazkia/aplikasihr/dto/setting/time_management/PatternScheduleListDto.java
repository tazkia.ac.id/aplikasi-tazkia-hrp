package id.ac.tazkia.aplikasihr.dto.setting.time_management;

public interface PatternScheduleListDto {

    String getId();
    String getScheduleName();
    String getMinggu();
    String getSenin();
    String getSelasa();
    String getRabu();
    String getKamis();
    String getJumat();
    String getSabtu();

}
