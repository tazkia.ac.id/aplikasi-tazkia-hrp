package id.ac.tazkia.aplikasihr.dto;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;

public interface LunasDto {

    String getId();
    StatusRecord getStatus();
}
