package id.ac.tazkia.aplikasihr.dto.employes;

import java.time.LocalDate;

public interface EmployeeStatusDto {

    String getId();
    String getFullName();
    LocalDate getExpiredDate();
    String getStatusEmployee();
    String getJobStatus();
    String getDescription();

}
