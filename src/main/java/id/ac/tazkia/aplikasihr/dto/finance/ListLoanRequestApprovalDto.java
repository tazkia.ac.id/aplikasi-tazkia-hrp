package id.ac.tazkia.aplikasihr.dto.finance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ListLoanRequestApprovalDto {
    String getId();
    BigDecimal getNominalEstimasi();
    Integer getInstallment();
    String getLoanFor();
    String getDeskripsi();
    BigDecimal getEstimatePersentase();
    StatusRecord getStatus();
    StatusRecord getStatusApprove();
    LocalDateTime getRequestDate();
    BigDecimal getNominalCicilan();
    Integer getAda();
    String getNumber();
    String getFullName();
    String getName();
}
