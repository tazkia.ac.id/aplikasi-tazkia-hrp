package id.ac.tazkia.aplikasihr.dto;

public interface ListPositionKpiDto {

    String getId();
    String getDepartemen();
    String getPosition();
    String getKpi();

}
