package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface TotalPayrollPerTahunDto {

    String getBulan();
    BigDecimal getTotalPayroll();

}
