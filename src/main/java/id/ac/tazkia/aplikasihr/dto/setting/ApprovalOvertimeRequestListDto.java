package id.ac.tazkia.aplikasihr.dto.setting;

import java.time.LocalDateTime;

public interface ApprovalOvertimeRequestListDto {

    Integer getNomor();
    String getOverTimeRequest();
    String getEmployee();
    String getPosition();
    String getApprove();
    LocalDateTime getTanggalApprove();
    String getKeterangan();

}
