package id.ac.tazkia.aplikasihr.dto.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.xmlbeans.impl.soap.Detail;

import java.util.List;

@Data
public class SKMengajarDto {
    private String dosen;
    private String email;
    private String nomor;
    private String tahun;
    private String prodi;
    private List<DetailDosenDto> jadwal;
}
