package id.ac.tazkia.aplikasihr.dto.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;

import java.math.BigDecimal;

public interface EmployeePayrollComponentDto {

    String getId();
    String getIdEmployee();
    String getIdComponent();
    String getJenisComponent();
    BigDecimal getNominal();
    BigDecimal getNominalAsli();
    String getJenisNominal();
    String getStatus();
    String getJenisComponentMaster();
    String getCode();
    String getComponentName();
    String getRumus();
    String getStatusComponent();
    String getTaxable();
    String getKodeRumusDefault();
    String getPotonganAbsen();
    String getPotonganTerlambat();
    String getZakat();
    StatusRecord getThr();
    StatusRecord getThrPajak();
    BigDecimal getPersentase();

    String getPerhitungan();

    Integer getMaksimalHari();


}
