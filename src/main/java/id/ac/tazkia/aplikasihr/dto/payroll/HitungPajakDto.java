package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface HitungPajakDto {

    BigDecimal getPersentase();
    BigDecimal getSyaratDari();
    BigDecimal getSyaratSampai();

}
