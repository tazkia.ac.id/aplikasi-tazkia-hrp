package id.ac.tazkia.aplikasihr.dto.setting.time_management;

import java.time.LocalDate;
import java.time.LocalTime;

public interface EmployeeScheduleDto {

    String getId();
    LocalDate getTanggal();
    String getHari();
    String getShiftName();
    LocalTime getShiftIn();
    LocalTime getShiftOut();

}
