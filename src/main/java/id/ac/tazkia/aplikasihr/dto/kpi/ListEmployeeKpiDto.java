package id.ac.tazkia.aplikasihr.dto.kpi;

public interface ListEmployeeKpiDto {

    String getIdPosition();
    String getIdEmployee();
    String getNumber();
    String getFullName();
    String getPositionName();
    String getJenis();
}
