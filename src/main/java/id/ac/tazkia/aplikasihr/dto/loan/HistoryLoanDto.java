package id.ac.tazkia.aplikasihr.dto.loan;

public interface HistoryLoanDto {

    String getIdEmployeeLoanBayar();
    String getIdEmployeeLoan();
    String getNumber();
    String getEmployeeName();
    String getLoan();
    String getDescriptions();
    Integer getInstallment();
    Integer getCicilanKe();
    Integer getPayment();

}
