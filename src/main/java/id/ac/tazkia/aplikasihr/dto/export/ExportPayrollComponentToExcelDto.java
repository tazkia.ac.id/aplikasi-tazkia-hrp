package id.ac.tazkia.aplikasihr.dto.export;

import java.math.BigDecimal;

public interface ExportPayrollComponentToExcelDto {

    String getId();
    String getNumber();
    String getFullName();
    BigDecimal getCurrrentAmmount();
    BigDecimal getNewAmmount();

}
