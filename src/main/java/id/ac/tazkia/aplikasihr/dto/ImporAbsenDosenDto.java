package id.ac.tazkia.aplikasihr.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ImporAbsenDosenDto {

    private String bulan;
    private String hari;
    private Integer sks;
    private String matkul;
    private String prodi;
    private String jadwal;
    private String dosen;
    private String tanggal;
    private String nama;
    private String kelas;
    private Integer totalsesi;
    private Integer totalsks;
    private Integer totalseluruhsks;
    private String email;
    private String jenjang;
    private Integer totalseluruhsesi;
    private String idprodi;
    private String idJenjang;

}
