package id.ac.tazkia.aplikasihr.dto;

import java.time.LocalDateTime;

public interface AnnouncementDto {

    String getId();
    String getIdEmployee();
    String getIsi();
    String getFileUpload();
    LocalDateTime getDateUpdate();
    String getFullName();

}
