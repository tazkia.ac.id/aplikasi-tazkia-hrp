package id.ac.tazkia.aplikasihr.dto.payroll;

public interface EmployeePayrollReportsFullDto {

    String getId();
    String getNumber();
    String getFullName();

}
