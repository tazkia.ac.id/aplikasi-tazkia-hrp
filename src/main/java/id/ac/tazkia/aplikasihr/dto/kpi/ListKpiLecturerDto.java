package id.ac.tazkia.aplikasihr.dto.kpi;

import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiDosen;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidanceDosen;
import lombok.Data;

import java.util.List;

@Data
public class ListKpiLecturerDto {

    private PengisianKpiDosen pengisianKpiDosen;
    private List<PengisianKpiEvidanceDosen> pengisianKpiEvidanceDosenList;
}
