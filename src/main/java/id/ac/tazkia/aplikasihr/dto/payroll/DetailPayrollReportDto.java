package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface DetailPayrollReportDto {

    String getNumber();
    String getFullName();
    BigDecimal getGajiPokok();
    BigDecimal getAllowance();
    BigDecimal getTotalGaji();
    BigDecimal getKenaPajak();
    BigDecimal getBenefits();
    BigDecimal getLoan();
    BigDecimal getPotonganAbsen();
    BigDecimal getPotonganTerlambat();
    BigDecimal getZakat();
    BigDecimal getInfaq();
    BigDecimal getPajak();
    BigDecimal getDeduction();
    BigDecimal getTakeHomePay();


}
