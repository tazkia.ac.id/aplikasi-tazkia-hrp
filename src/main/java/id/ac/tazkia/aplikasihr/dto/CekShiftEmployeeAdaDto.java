package id.ac.tazkia.aplikasihr.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class CekShiftEmployeeAdaDto {

    private String id;

    private String workDayStatys;

    @JsonFormat(pattern="h:mm a")
    private LocalTime shiftIn;

    @JsonFormat(pattern="h:mm a")
    private LocalTime shiftOut;

    @JsonFormat(pattern="h:mm a")
    private LocalTime overtimeBefore;

    @JsonFormat(pattern="h:mm a")
    private LocalTime overtimeAfter;

    private LocalDate tanggals;

    private String hari;

    private String keterangan;

    private String status;

    private String overtimeWorkingDay;

    private String overtimeHoliday;

}
