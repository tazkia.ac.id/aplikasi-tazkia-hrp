package id.ac.tazkia.aplikasihr.dto;

public interface DayOffPatternScheduleAllowDto {

    String getId();
    String getPatternSchedule();
    String getStatusAktif();

}
