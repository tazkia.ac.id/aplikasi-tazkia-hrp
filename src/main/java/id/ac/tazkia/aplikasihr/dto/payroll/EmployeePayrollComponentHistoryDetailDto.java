package id.ac.tazkia.aplikasihr.dto.payroll;

//import org.apache.tomcat.jni.Local;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface EmployeePayrollComponentHistoryDetailDto {

    String getId();
    String getEmployeeId();
    String getFullName();
    BigDecimal getCurrentAmmount();
    BigDecimal getNewAmmount();
    String getIdHistory();
    LocalDate getEffectiveDate();
    LocalDate getEndDate();

}
