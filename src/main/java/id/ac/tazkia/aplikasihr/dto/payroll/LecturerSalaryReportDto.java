package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface LecturerSalaryReportDto {

    String getId();
    String getNama();
    String getClasss();
    String getMatkul();
    String getKelas();
    Integer getTotalSesi();
    String getTanggal();
    Integer getTotalSks();
    BigDecimal getTarif();
    BigDecimal getTotal();

}
