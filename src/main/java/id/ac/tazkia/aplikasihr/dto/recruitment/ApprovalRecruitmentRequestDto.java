package id.ac.tazkia.aplikasihr.dto.recruitment;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ApprovalRecruitmentRequestDto {

    Integer getNomor();
    String getApprovalRequest();
    String getEmployee();
    String getPosition();
    String getApprove();
    LocalDateTime getTanggalApprove();
    String getKeterangan();

}
