package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface BayarLoanDto {

    String getId();
    String getEffectiveDate();
    BigDecimal getAmmount();
    BigDecimal getCurrentPayment();
    BigDecimal getSisaBayar();
    Integer getCicilanKe();

}
