package id.ac.tazkia.aplikasihr.dto.payroll;

import java.math.BigDecimal;

public interface IsiPayrollRunReportDto {

    String getNomor();
    String getJenis();
    String getNama();
    String getIdEmployee();
    String getKode();
    BigDecimal getNominal();

}
