package id.ac.tazkia.aplikasihr.dto;

public interface StatusEmployeeDto {
     
     String getStatus();

     String getStatusdua();

}
