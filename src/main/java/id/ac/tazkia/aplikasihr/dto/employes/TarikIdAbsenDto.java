package id.ac.tazkia.aplikasihr.dto.employes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TarikIdAbsenDto {

    private Integer idAbsen;

}
