package id.ac.tazkia.aplikasihr.dto.kpi;

import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpi;
import id.ac.tazkia.aplikasihr.entity.kpi.PengisianKpiEvidance;
import lombok.Data;

import java.util.List;

@Data
public class ListKpiEmployeeDto {
    private PengisianKpi pengisianKpi;
    private List<PengisianKpiEvidance> pengisianKpiEvidanceList;

}
