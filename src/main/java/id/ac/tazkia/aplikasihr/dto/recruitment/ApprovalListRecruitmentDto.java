package id.ac.tazkia.aplikasihr.dto.recruitment;

import java.time.LocalDate;

public interface ApprovalListRecruitmentDto {

    String getId();
    String getFullName();
    LocalDate getTanggalPengajuan();
    String getQualification();
    String getDescription();
    String getInformation();
    String getGender();
    String getDepartment();
    String getPosition();

}
