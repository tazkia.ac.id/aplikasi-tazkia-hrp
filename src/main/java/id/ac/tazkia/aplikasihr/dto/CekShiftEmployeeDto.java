package id.ac.tazkia.aplikasihr.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalTime;

public interface CekShiftEmployeeDto {

    String getId();
    String getWorkDayStatys();
    LocalTime getShiftIn();
    LocalTime getShiftOut();
    LocalTime getOvertimeBefore();
    LocalTime getOvertimeAfter();
    LocalDate getTanggals();
    String getHari();
    String getKeterangan();
    String getStatus();

    String getOvertimeWorkingDay();

    String getOvertimeHoliday();

}
