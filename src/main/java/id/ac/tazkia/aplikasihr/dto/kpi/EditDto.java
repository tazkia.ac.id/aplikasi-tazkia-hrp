package id.ac.tazkia.aplikasihr.dto.kpi;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@Data
public class EditDto {
    @NotNull
    private String id;

    @NotNull
    private LocalDate tanggal;

    private LocalDate tanggalPenugasan;
    private String deskripsiPenugasan;

    private String deskripsi;

    private String[] idEvidance;

    private MultipartFile[] files;
}
