package id.ac.tazkia.aplikasihr.dto.request;

public interface TarikOvertimeDto {

    String getIdEmployee();
    String getTanggalOvertime();
    Integer getJam();
    String getStatusOvertime();
}
