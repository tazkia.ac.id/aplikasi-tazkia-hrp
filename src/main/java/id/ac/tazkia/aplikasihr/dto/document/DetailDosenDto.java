package id.ac.tazkia.aplikasihr.dto.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.xmlbeans.impl.soap.Detail;

@Data
public class DetailDosenDto {
    private String email;
    private String mulai;
    private String selesai;
    private String matakuliah;
    private String kelas;
    private String jenis;

    public DetailDosenDto(){}

    public DetailDosenDto(
            @JsonProperty("email") String email,
            @JsonProperty("mulai") String mulai,
            @JsonProperty("selesai") String selesai,
            @JsonProperty("matakuliah") String matakuliah,
            @JsonProperty("kelas") String kelas,
            @JsonProperty("jenis") String jenis
    ){
        this.email = email;
        this.mulai = mulai;
        this.selesai = selesai;
        this.matakuliah = matakuliah;
        this.kelas = kelas;
        this.jenis = jenis;
    }
}
