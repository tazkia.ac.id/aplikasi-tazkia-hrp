package id.ac.tazkia.aplikasihr.dto.download;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DownloadAbsensiDto implements Serializable {

    @NotNull
    private Integer id;

    @NotNull
    private LocalDate tanggal;

    @NotNull
    private LocalTime waktu;

    @NotNull
    private String workCode;

    @NotNull
    private String verivied;


}
