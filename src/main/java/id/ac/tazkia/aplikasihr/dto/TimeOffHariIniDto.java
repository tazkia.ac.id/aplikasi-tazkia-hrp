package id.ac.tazkia.aplikasihr.dto;

import java.time.LocalDate;

public interface TimeOffHariIniDto {

    LocalDate getTanggal();
    String getNama();
    String getStatus();
    String getTimeOff();

}
