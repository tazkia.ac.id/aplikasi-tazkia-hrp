package id.ac.tazkia.aplikasihr.dto.setting.schedule;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public interface ScheduleEventDto {

    String getId();
    String getIdCompany();
    LocalDate getScheduleDate();
    LocalTime getScheduleTimeStart();
    LocalTime getScheduleTimeEnd();
    String getScheduleName();
    String getScheduleDescription();
    String getStatusAktif();
    LocalDateTime getSelesai();
    LocalDateTime getSekarang();
    String getStatusMulai();
    String getAbsen();
    String getIdAbsen();

}
