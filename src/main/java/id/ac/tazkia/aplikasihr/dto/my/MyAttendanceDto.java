package id.ac.tazkia.aplikasihr.dto.my;

import java.time.LocalDate;
import java.time.LocalTime;

public interface MyAttendanceDto {


    String getId();
    LocalDate getTanggal();
    String getHari();
    String getShiftName();
    LocalTime getShiftIn();
    LocalTime getShiftOut();
    String getWorkDayStatus();
    String getWaktuMasuk();
    String getWaktuKeluar();
    String getStat();

}
