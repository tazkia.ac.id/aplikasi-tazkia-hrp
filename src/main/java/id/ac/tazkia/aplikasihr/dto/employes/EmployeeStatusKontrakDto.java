package id.ac.tazkia.aplikasihr.dto.employes;

import java.time.LocalDate;

public interface EmployeeStatusKontrakDto {

    String getIdEmployee();
    String getStatusEmployee();
    LocalDate getDariTanggal();
    LocalDate getSampaiTanggal();
    String getStatus();

}
