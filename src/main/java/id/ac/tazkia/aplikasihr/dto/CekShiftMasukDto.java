package id.ac.tazkia.aplikasihr.dto;


import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class CekShiftMasukDto{

    private String status;
    private LocalTime jamMasuk;
    private LocalTime overtimeBefore;
    private LocalTime overTimeAfter;
    private LocalTime shiftMasuk;
    private LocalTime shiftKeluar;
    private LocalDate tanggal;

}
