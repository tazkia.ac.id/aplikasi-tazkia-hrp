package id.ac.tazkia.aplikasihr.dto.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;

import java.time.LocalDateTime;

public interface AttendanceRunProcessDto {

    String getId();
    String getCompanyName();
    String getTahun();
    String getBulan();
    LocalDateTime getTanggalInput();
    LocalDateTime getTanggalMulai();
    LocalDateTime getTanggalSelesai();
    String getUser();
    StatusRecord getStatus();
    Integer getDone();
    Integer getErrorr();

}
