package id.ac.tazkia.aplikasihr.dto.reports;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface OvertimeSumaryDto {

    String getId();
    String getNumber();
    String getFullName();
    String getTanggal();
    String getKeterangan();
    String getJam();
    String getStatus();
    Integer getTotalJam();
    BigDecimal getTotalNominal();

}
