package id.ac.tazkia.aplikasihr.dto.payroll;

import java.time.LocalDate;

public interface LectureSalaryDto {

    String getId();
    String getNama();
    String getSalaryPerSks();
    String getStatus();
    LocalDate getTanggalBerlaku();

}
