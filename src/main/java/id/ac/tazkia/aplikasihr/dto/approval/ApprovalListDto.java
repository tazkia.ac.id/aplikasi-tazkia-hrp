package id.ac.tazkia.aplikasihr.dto.approval;

import com.google.api.client.util.DateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public interface ApprovalListDto {

    String getId();
    String getFullName();
    LocalDateTime getTanggalPengajuan();
    LocalDate getTanggal();
    LocalDate getSampai();
    String getRequest();
    String getKeterangan();
    String getFile();
    String getJenis();

    String getApprove();

}
