package id.ac.tazkia.aplikasihr.dto.export;

public interface ExportEmployeeStatusDto {

    String getId();
    String getNumber();
    String getFullName();
    String getStatusAktif();
    String getDateEmployeeStatusAktif();
    String getStatusEmployee();
    String getDateEmployeeStatus();
    String getPtkpStatus();

}
