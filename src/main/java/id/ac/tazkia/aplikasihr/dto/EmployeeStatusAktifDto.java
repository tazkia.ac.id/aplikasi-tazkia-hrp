package id.ac.tazkia.aplikasihr.dto;

import java.time.LocalDate;

public interface EmployeeStatusAktifDto {

    String getId();
    String getNikAda();
    String getNama();
    LocalDate getTanggalAda();
    String getStatusAda();

}
