package id.ac.tazkia.aplikasihr.entity.masterdata;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeSaudara {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    private String namaSaudara;

    private String tempatLahirSaudara;

    private LocalDate tanggalLahirSaudara;

    @ManyToOne
    @JoinColumn(name = "id_agama")
    private Agama agama;

    @ManyToOne
    @JoinColumn(name = "id_pendidikan_terakhir_saudara")
    private JenjangPendidikan pendidikanTerakhirSaudara;

    private String jabatanTerakhirSaudara;

//    @ManyToOne
//    @JoinColumn(name = "id_status_hidup_saudara")
    private String statusHidup;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String tanggalLahir;

}
