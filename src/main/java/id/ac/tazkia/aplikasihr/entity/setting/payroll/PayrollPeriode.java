package id.ac.tazkia.aplikasihr.entity.setting.payroll;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollPeriode {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private Integer dariTanggal;

    @NotNull
    private Integer sampaiTanggal;

    @NotNull
    private String bulan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String defaultPeriod;

    private String taxSetting;

    private String salaryTaxSetting;

    private String jhtSetting;

    private String bpjsKesehatanSetting;

    private String jaminanPensiunSetting;

    private Integer payrollDariTanggal;

    private Integer payrollSampaiTanggal;

    private Integer payrollDate;

    private String attendancePayLastMonth;

}
