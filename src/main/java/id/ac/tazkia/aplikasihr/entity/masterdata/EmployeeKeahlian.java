package id.ac.tazkia.aplikasihr.entity.masterdata;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeKeahlian {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    private String deskripsi;

    private String file;

    private String mulaiBerlaku;

    private String selesaiBerlaku;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
