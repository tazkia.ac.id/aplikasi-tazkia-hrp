package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class ApplicantCertificate {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_applicant")
    private Applicant applicant;

    private String certificateName;

    private String certificateFile;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate = LocalDateTime.now();

    private String mulaiBerlaku;

    private String selesaiBerlaku;

}
