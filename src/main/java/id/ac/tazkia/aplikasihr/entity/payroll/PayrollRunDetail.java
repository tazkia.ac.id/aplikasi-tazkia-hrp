package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollRunDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private String bulan;

    private String tahun;

    private String jenisPayroll;

    private String deskripsi;

    private BigDecimal nominal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @JoinColumn(name = "id_employee_payroll_component")
    private EmployeePayrollComponent employeePayrollComponent;

}
