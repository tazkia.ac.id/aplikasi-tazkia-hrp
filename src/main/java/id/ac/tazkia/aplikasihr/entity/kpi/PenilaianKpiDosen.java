package id.ac.tazkia.aplikasihr.entity.kpi;

import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;

@Entity
@Data
@Table(name = "penilaian_kpi")
public class PenilaianKpiDosen {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_position_kpi")
    private LecturerClassKpi positionKpi;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_employee")
    private Lecturer employes;

    private String bulan;
    private String tahun;

    private String grade;

    private BigDecimal nilai;

    private BigDecimal nominal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
