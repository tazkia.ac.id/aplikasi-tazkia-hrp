package id.ac.tazkia.aplikasihr.entity.config;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "s_user_role")
@IdClass(UserRoleId.class) // Composite Key
public class UserRole {
    @Id
    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id")
    private Role role;
}


