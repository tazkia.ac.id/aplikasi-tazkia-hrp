package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class EmployeePayrollComponentHistory {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String transactionId;

    private String tipe;

    @ManyToOne
    @JoinColumn(name = "id_payroll_component")
    private PayrollComponent payrollComponent;

    private LocalDate effectiveDate;

    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;


    @OneToMany(mappedBy = "employeePayrollComponentHistory", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<EmployeePayrollComponentHistoryDetail> employeePayrollComponentHistoryDetails = new HashSet<>();


}
