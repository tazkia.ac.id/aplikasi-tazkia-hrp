package id.ac.tazkia.aplikasihr.entity.kpi;

import id.ac.tazkia.aplikasihr.entity.LecturerClassKpi;
import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "pengisian_kpi")
public class PengisianKpiDosen {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_kpi")
    private LecturerClassKpi lecturerClassKpi;

    private LocalDate tanggal;

    private LocalDate tanggalPenugasan;

    private LocalDateTime tanggalInput;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_employee")
    private Lecturer lecturer;

    @Lob
    private String deskripsi;

    @Lob
    private String deskripsiPenugasan;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusKpi = StatusRecord.OPEN;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private String dateUpdate;

    private String bulan;

    private String tahun;

}
