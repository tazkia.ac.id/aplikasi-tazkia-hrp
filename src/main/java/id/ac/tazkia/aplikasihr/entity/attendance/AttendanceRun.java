package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class AttendanceRun {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String bulan;

    private String tahun;

    private String idEmployee;

    private Integer totalShift;

    private Integer totalAttendance;

    private Integer totalTimeOff;

    private Integer totalAbsen;

    private Integer totalLate;

    private Integer totalOvertime;

    private Integer totalOvertimeDayOff;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private Integer overtimeWorkingDayFirstHour;

    private Integer overtimeWorkingDayNextHour;

    private Integer overtimeHolidayFirstHour;

    private Integer overtimeHolidaySecondHour;

    private Integer overtimeHolidayNextHour;

}
