package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeRiwayatPendidikan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_jenjang_pendidikan")
    private JenjangPendidikan jenjangPendidikan;

    @NotNull
    private String namaInstitusi;

    private Integer tahunMasuk;

    private Integer tahunLulus;

    private BigDecimal nilaiAkhir;

//    @Enumerated(EnumType.STRING)
    private String statusLulus;

    private String fileIjasah;

    private String fileTranskrip;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
