package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import id.ac.tazkia.aplikasihr.entity.masterdata.Agama;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class Applicant {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String nickName;

    private String fullName;

    private String placeOfBirth;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    private String gender;

    private String blod;

    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Agama religion;

    private String noNpwp;

    private String bpjsKesehatanNumber;

    private String bpjsKetenagakerjaanNumber;

    private String identityNumber;

    private String identityAddress;

    private String domisiliAddress;

    private String homeTelephoneNumber;

    private String celularTelephoneNumber;

    private String emailActive;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String fileFoto;

    private String statusAktif;

}
