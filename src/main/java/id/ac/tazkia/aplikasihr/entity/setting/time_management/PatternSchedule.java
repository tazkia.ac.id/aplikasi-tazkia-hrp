package id.ac.tazkia.aplikasihr.entity.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class PatternSchedule {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String scheduleName;

    @ManyToOne
    @JoinColumn(name = "id_pattern")
    private Pattern pattern;

    private LocalDate efectiveDate;

    private String initialPattern;

    private String overideNationalHoliday;

    private String overideCompanyHoliday;

    private String flexible;

    private String attendance;

    @Enumerated(EnumType.STRING)
    private StatusRecord overtimeWorkingDay;

    @Enumerated(EnumType.STRING)
    private StatusRecord overtimeHoliday;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String efectiveDateString;

}
