package id.ac.tazkia.aplikasihr.entity.masterdata;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeePayrollInfo {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private BigDecimal basicSalary;

    @ManyToOne
    @JoinColumn(name = "ptkpStatus")
    private PtkpStatus ptkpStatus;

    private String taxConfiguration;

    private String prorateType;

    private String taxable;


    private String bpjsKesehatanConfiguration;

    private String jaminanPensiunConfiguration;

    private String nppBpjsKetenagakerjaan;

    private String bpjsKetenagakerjaan;

    private String bpjsKesehatan;

    private Integer bpjsKesehatanFamily;

    private String npwp;

    private String bankName;

    private String bankAccount;

    private String bankAccountHolder;

    private String currency;

    private String typeSalary;

    private String salaryConfiguration;

    private LocalDate salaryTaxableDate;

    private String overtimeStatus;

    private String employeeTaxStatus;

    private String jhtConfiguration;

    private LocalDate bpjsKetenagakerjaanDate;

    private LocalDate bpjsKesehatanDate;

    private BigDecimal biginningNetto;

    private BigDecimal pph21Paid;

    private String bpjsKetenagakerjaanDateString;

    private String bpjsKesehatanDateString;

    private String salaryTaxableDateString;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

}
