package id.ac.tazkia.aplikasihr.entity;

public enum JenisUndangan {
    TES, WAWANCARA, TES_DAN_WAWANCARA
}
