package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollRunProcess {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private String tahun;

    private String bulan;

    private LocalDateTime tanggalInput;

    private LocalDateTime tanggalMulai;

    private LocalDateTime tanggalSelesai;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    private String userInput;

}
