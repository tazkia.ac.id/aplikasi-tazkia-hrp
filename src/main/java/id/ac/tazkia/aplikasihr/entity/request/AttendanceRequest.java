package id.ac.tazkia.aplikasihr.entity.request;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class AttendanceRequest {


    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

//    @NotNull
    private LocalDateTime tanggalPengajuan = LocalDateTime.now();

//    @NotNull
//    @DateTimeFormat(pattern = "dd-MMMM-yyyy")
    private LocalDate tanggal;

//    @NotNull
//    @DateTimeFormat(pattern = "h:mm a")
    private LocalTime jamMasuk;

//    @NotNull
//    @DateTimeFormat(pattern = "h:mm a")
    private LocalTime jamKeluar;

    @NotNull
    private String keterangan;

    private String file;

//    @NotNull
    private Integer nomor;

//    @NotNull
    private Integer totalNomor;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

//    @NotNull
    private String userUpdate;

//    @NotNull
    private LocalDateTime dateUpdate;

    @NotNull
//    @NotEmpty
    private String tanggalString;

    @NotNull
//    @NotEmpty
    private String jamMasukString;

    @NotNull
//    @NotEmpty
    private String jamKeluarString;

//    @OneToOne(mappedBy = "attendanceRequest", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<String> ada = new HashSet<>();

}
