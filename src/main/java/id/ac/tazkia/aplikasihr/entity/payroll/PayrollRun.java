package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollRun {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String bulan;

    private String tahun;

    private String idEmployee;

    private BigDecimal gajiPokok;

    private BigDecimal totalAllowance;

    private BigDecimal totalDeductions;

    private BigDecimal totalBenefits;

    private BigDecimal totalZakat;

    private BigDecimal totalInfaq;

    private BigDecimal totalPajak;

    private BigDecimal totalLoan;

    private BigDecimal totalPayroll;

    private BigDecimal totalKenaPajak;

    private BigDecimal totalPotonganAbsen;

    private BigDecimal totalPotonganTerlambat;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord paySlip;

}
