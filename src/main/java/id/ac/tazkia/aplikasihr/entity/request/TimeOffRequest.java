package id.ac.tazkia.aplikasihr.entity.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.timeoff.TimeOffJenis;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class TimeOffRequest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_time_off_jenis")
    private TimeOffJenis timeOffJenis;

    @NotNull
    private LocalDateTime tanggalPengajuan = LocalDateTime.now();

    //    @NotNull
    private LocalDate tanggalTimeOff;

    private LocalDate tanggalTimeOffSampai;

    @NotNull
    private String keterangan;

    private String file;

    //    @NotNull
    private Integer nomor;

    //    @NotNull
    private Integer totalNomor;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    //    @NotNull
    private String userUpdate;

    //    @NotNull
    private LocalDateTime dateUpdate;

    private String tanggalTimeOffString;

    private String tanggalTimeOffSampaiString;

    private String tahun;

//    @OneToMany(mappedBy = "attendanceRequest", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<AttendanceRequestApproval> attendanceRequestApproval = new HashSet<>();

}
