package id.ac.tazkia.aplikasihr.entity.thr;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class ThrRun {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String bulan;

    private String tahun;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private BigDecimal totalPajak;

    private BigDecimal totalThr;

    private BigDecimal thrBersih;

    private BigDecimal partSatu;

    private BigDecimal partDua;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord paySlip;

}
