package id.ac.tazkia.aplikasihr.entity.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class ShiftDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String shiftName;

//    @NotNull
    private LocalTime shiftIn;

    private String shiftInString;

//    @NotNull
    private LocalTime shiftOut;

    private String shiftOutString;

    private LocalTime breakIn;

    private String breakInString;

    private LocalTime breakOut;

    private String breakOutString;

    private LocalTime overtimeBefore;

    private String overtimeBeforeString;

    private LocalTime overtimeAfter;

    private String overtimeAfterString;

    private String breakStatus;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String workDayStatus;

}
