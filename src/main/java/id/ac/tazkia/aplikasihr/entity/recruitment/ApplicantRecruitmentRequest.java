package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.dao.recruitment.ApplicantDao;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class ApplicantRecruitmentRequest {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_applicant")
    private Applicant applicant;

    @ManyToOne
    @JoinColumn(name = "id_recruitment_request")
    private RecruitmentRequest request;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String statusAktif;

}
