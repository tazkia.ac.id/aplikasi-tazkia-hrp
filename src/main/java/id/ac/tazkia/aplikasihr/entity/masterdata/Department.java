package id.ac.tazkia.aplikasihr.entity.masterdata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class Department {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty
    private String departmentCode;

    @NotEmpty
    private String departmentName;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_company")
    private Companies companies;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_department_class")
    private DepartmentClass departmentClass;

}
