package id.ac.tazkia.aplikasihr.entity.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class OvertimeRequest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    private LocalDateTime tanggal = LocalDateTime.now();

    @DateTimeFormat(pattern = "dd-MMMM-yyyy")
    private LocalDate tanggalOvertime;

    @DateTimeFormat(pattern = "h:mm a")
    private LocalTime overtimeFrom;

    @DateTimeFormat(pattern = "h:mm a")
    private LocalTime overtimeTo;

    private String jenis;

    private String kelas;

    private String keterangan;

    private String file;

    private Integer nomor;

    private Integer totalNomor;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String tanggalOvertimeString;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusOvertime;

    private Integer jam;

    private String overtimeFromString;

    private String overtimeToString;

}
