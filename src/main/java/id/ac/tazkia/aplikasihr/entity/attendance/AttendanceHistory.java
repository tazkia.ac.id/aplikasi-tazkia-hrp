package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistoryDetail;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class AttendanceHistory {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private LocalDateTime tanggal;

    @Enumerated(EnumType.STRING)
    private StatusRecord jenis;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    @OneToMany(mappedBy = "attendanceHistory", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Attendance> attendances = new HashSet<>();

}
