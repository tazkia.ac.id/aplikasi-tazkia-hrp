package id.ac.tazkia.aplikasihr.entity;

public enum  StatusLulus {

    LULUS, PINDAHAN, TIDAK_LULUS

}
