package id.ac.tazkia.aplikasihr.entity.kpi;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "pengisian_kpi_evidance")
public class PengisianKpiEvidanceDosen {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_pengisian_kpi")
    private PengisianKpiDosen pengisianKpiDosen;

    @Lob
    private String namaFile;

    @Lob
    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpload;

    private LocalDateTime dateUpload;

    private String userDelete;

    private LocalDateTime dateDelete;

}
