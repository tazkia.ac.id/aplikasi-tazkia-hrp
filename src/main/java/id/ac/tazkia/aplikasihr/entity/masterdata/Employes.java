package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
//@EqualsAndHashCode(of = {"id", "user"})
@Data
public class Employes {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;


    private String number;


    private String nickName;

    @NotNull
    private String fullName;

//    @NotNull
    private String placeOfBirth;

//    @NotNull
    private LocalDate dateOfBirth;

//    @NotNull
    private String gender;

    private String blod;

    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Agama religion;

    private String noNpwp;

    private String bpjsKesehatanNumber;

    private String bpjsKetenagakerjaanNumber;

//    @NotNull
    private String identityNumber;

//    @NotNull
    private String identityAddress;

//    @NotNull
    private String domisiliAddress;

    private String homeTelephoneNumber;

    private String celularTelephoneNumber;

    @NotNull
    private String emailActive;

    private String rekeningNumber;

    @ManyToOne
    @JoinColumn(name = "bank")
    private Bank bank;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "id_user", nullable = false)
//    private User user;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String tanggalLahir;

    private String fileFoto;

    private Integer idAbsen;

    private String statusAktif;

    private String statusEmployee;

    private LocalDate tanggalMasuk;

    private LocalDate tanggalKeluar;

    private String tanggalMasukString;

    private String tanggalKeluarString;

    @Enumerated(EnumType.STRING)
    private StatusRecord jobStatus;

//    private String tanggalLahirString;

//    @OneToMany(mappedBy = "employes", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<EmployeeAyah> employeeAyahs = new HashSet<>();
//
//    @OneToMany(mappedBy = "employes", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<EmployeeIbu> employeeIbus = new HashSet<>();

    private BigDecimal totalTunjanganHarian;

}
