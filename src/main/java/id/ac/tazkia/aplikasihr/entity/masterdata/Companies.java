package id.ac.tazkia.aplikasihr.entity.masterdata;


import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Companies {

    @Id
//    @GeneratedValue(generator = "uuid" )
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String companyName;

    @NotNull
    private String companyType;

    @NotNull
    private String regNumber;

    @NotNull
    private String address;

    @NotNull
    private String regency;

    @NotNull
    private String country;

    @NotNull
    private String postalCode;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_main_company")
    private Companies mainCompany;

}
