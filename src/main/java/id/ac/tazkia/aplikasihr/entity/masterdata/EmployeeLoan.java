package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data

public class EmployeeLoan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_loan")
    private Loan loan;

    private LocalDate effectiveDate;

    private BigDecimal amount;

    private BigDecimal installment;

    private BigDecimal installmentPaid;

    private BigDecimal currentPayment;

    private BigDecimal totalPayment;

    private BigDecimal remaining;

    private String interestType;

    private BigDecimal interest;

    private String descriptions;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String effectiveDateString;

}
