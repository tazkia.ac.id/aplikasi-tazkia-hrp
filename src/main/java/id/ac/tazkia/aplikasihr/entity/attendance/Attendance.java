package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@EqualsAndHashCode(of = {"id","attendanceHistory"})
@Data
public class Attendance {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_attendance_history")
    private AttendanceHistory attendanceHistory;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private LocalDate date;

    private LocalTime waktuMasuk;

    private LocalTime waktuKeluar;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime dateUpdate;

    private String userUpdate;

}
