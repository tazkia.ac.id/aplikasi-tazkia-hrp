package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class AttendanceLecturerImporProcessDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_attendance_lecturer_impor_process")
    private AttendanceLecturerImporProcess attendanceLecturerImporProcess;

    private String topik;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.DONE;

}
