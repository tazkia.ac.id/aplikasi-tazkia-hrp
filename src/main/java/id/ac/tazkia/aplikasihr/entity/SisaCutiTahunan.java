package id.ac.tazkia.aplikasihr.entity;

import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class SisaCutiTahunan {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String tahun;
    private Integer sisaCuti;
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

}
