package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusClass;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.Data;


@Entity
@Data
public class DepartmentClass {

    @Id
    private String id;

    private String className;

    @Enumerated(EnumType.STRING)
    private StatusClass jenis;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
