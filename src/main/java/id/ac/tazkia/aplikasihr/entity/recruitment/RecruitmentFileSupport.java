package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
@Table(name = "recruitment_request_file_support")
public class RecruitmentFileSupport {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_recruitment_request")
    private RecruitmentRequest request;

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
