package id.ac.tazkia.aplikasihr.entity.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeTasks {

    @Id
//    @GeneratedValue(generator = "uuid" )
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_schedule_event")
    private ScheduleEvent scheduleEvent;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userEdit;

    private String userDelete;

    private LocalDateTime tanggalInsert;

    private LocalDateTime tanggalEdit;

    private LocalDateTime tanggalDelete;

}
