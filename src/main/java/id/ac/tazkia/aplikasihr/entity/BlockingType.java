package id.ac.tazkia.aplikasihr.entity;

public enum BlockingType {

    NONE, MONTH, YEAR

}
