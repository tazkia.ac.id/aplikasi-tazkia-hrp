package id.ac.tazkia.aplikasihr.entity.attendance;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class AttendanceLecturerImporProcess {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private String tahun;

    private String bulan;

    private LocalDateTime tanggalInput;

    private LocalDateTime tanggalMulai;

    private LocalDateTime tanggalSelesai;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.WAITING;

    private String userInput;


}
