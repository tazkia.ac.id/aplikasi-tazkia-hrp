package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class AttendanceRunProcessDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_attendance_run_process")
    private AttendanceRunProcess attendanceRunProcess;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.DONE;

}
