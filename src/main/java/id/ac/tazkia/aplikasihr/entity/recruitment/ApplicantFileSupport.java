package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Data
@Entity
public class ApplicantFileSupport {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_applicant")
    private Applicant applicant;

    @ManyToOne
    @JoinColumn(name = "id_recruitment_request_file_support")
    private RecruitmentFileSupport support;

    private String name;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
