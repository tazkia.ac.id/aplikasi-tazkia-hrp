package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.Gender;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Department;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeeType;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.domain.Auditable;

import jakarta.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class RecruitmentRequest {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "new_employee_type")
    private EmployeeType employeeType;

    private Integer quantity;

    private String unit;

    @ManyToOne
    @JoinColumn(name = "for_departement")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "for_job_position")
    private JobPosition position;

    private String qualification;

    private String jobDescription;

    private String additionalInformation;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove;

    private Integer number;

    private Integer totalNumber;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusLaunching = StatusRecord.NONAKTIF;

    private LocalDate openDate;

    private LocalDate expiredDate;

    private String gender;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDate DateUpdate = LocalDate.now();

    @ManyToOne
    @JoinColumn(name = "id_employee_request")
    private Employes employes;

    private LocalDate dateRequest;

    private String descriptionHrd;

    private String file;

}
