package id.ac.tazkia.aplikasihr.entity.setting.schedule;

import id.ac.tazkia.aplikasihr.entity.Days;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;

@Entity
@Data
public class ScheduleDaily {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;


    @ManyToOne
    @JoinColumn(name = "id_day")
    private Days days;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private Time timeStart;

    private Time timeEnd;

    private String scheduleName;

    private String scheduleDescription;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

}
