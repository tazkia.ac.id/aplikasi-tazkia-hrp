package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class PayrollRunProcessDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_payroll_run_process")
    private PayrollRunProcess payrollRunProcess;

    private String description;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusRun;

}
