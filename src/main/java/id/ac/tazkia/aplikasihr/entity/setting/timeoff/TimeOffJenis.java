package id.ac.tazkia.aplikasihr.entity.setting.timeoff;

import id.ac.tazkia.aplikasihr.entity.BlockingType;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity
public class TimeOffJenis {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String timeOffName;

    @NotNull
    private Integer count;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusQuota;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String tahun;

    @Enumerated(EnumType.STRING)
    private StatusRecord hitungMasuk;

    private Integer blockingCount;

    @Enumerated(EnumType.STRING)
    private BlockingType blockingType;


}
