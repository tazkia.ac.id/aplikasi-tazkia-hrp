package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.config.User;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployesLuar {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_vendor")
    private Vendor vendor;

    private String nama;

    private String fullName;

    private String email;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String statusAktif;

}
