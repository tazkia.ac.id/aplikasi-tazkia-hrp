package id.ac.tazkia.aplikasihr.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class PositionKpi {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_position")
    private JobPosition jobPosition;

    @NotEmpty
    private String kpi;

    @NotEmpty
    private String indikator;

    @NotEmpty
    private String syaratAa;

    @NotEmpty
    private String syaratBb;

    @NotEmpty
    private String syaratCc;

    @Enumerated(EnumType.STRING)
    private PeriodeEvaluasiKpi periodeEvaluasi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private BigDecimal nominal;

    @NotEmpty
    private String nominalString;

    @NotNull
    private BigDecimal bobotA;

    @NotNull
    private BigDecimal bobotB;

    @NotNull
    private BigDecimal bobotC;

}
