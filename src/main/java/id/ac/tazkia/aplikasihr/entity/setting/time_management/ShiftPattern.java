package id.ac.tazkia.aplikasihr.entity.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.Days;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class ShiftPattern {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_day")
    private Days days;

    @ManyToOne
    @JoinColumn(name = "id_pattern")
    private Pattern pattern;

    @ManyToOne
    @JoinColumn(name = "id_shift_detail")
    private ShiftDetail shiftDetail;

//    @NotNull
//    private String name;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
