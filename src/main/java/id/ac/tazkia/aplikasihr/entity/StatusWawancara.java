package id.ac.tazkia.aplikasihr.entity;

public enum StatusWawancara {

    DATANG, TIDAK_DATANG, REKOMENDASI, TIDAK_REKOMENDASI, EXPIRED

}
