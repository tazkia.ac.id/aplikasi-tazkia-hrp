package id.ac.tazkia.aplikasihr.entity.setting.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
//import jdk.jfr.Enabled;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollBenefit {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String code;

    private String name;

    private String rumus;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
