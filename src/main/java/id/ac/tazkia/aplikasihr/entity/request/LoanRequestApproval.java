package id.ac.tazkia.aplikasihr.entity.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.LoanApprovalSetting;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class LoanRequestApproval {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_loan_request")
    private LoanRequest loanRequest;

    @ManyToOne
    @JoinColumn(name = "id_loan_approval_setting")
    private LoanApprovalSetting loanApprovalSetting;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private LocalDateTime dateApprove;

    private String deskripsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove = StatusRecord.APPROVED;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
