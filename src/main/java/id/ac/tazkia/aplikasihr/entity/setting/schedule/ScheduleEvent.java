package id.ac.tazkia.aplikasihr.entity.setting.schedule;


import id.ac.tazkia.aplikasihr.entity.Days;
import id.ac.tazkia.aplikasihr.entity.ScheduleType;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.setting.DayOffPatternScheduleAllow;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class ScheduleEvent {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private LocalDate scheduleDate;

    private LocalTime scheduleTimeStart;

    private LocalTime scheduleTimeEnd;

    private String scheduleName;

    private String scheduleDescription;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private String scheduleDateString;

    private String scheduleTimeStartString;

    private String scheduleTimeEndString;

    @Enumerated(EnumType.STRING)
    private ScheduleType scheduleType;

    @Enumerated(EnumType.STRING)
    private StatusRecord uploadPicture = StatusRecord.TRUE;

    @Enumerated(EnumType.STRING)
    private StatusRecord mustMatch = StatusRecord.TRUE;

    @ManyToOne
    @JoinColumn(name = "id_day")
    private Days days;

    private BigDecimal reward;

}
