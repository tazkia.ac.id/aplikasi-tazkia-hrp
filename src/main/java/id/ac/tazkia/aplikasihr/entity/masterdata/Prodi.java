package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "department_prodi")
public class Prodi {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_fakultas")
    private Fakultas fakultas;

    @ManyToOne
    @JoinColumn(name = "id_jenjang")
    private Jenjang idJenjang;

    @NotNull
    private String kodeProdi;

    private String kodeBiaya;

    @NotEmpty
    private String namaProdi;

    @NotEmpty
    private String namaProdiEnglish;

    private String keterangan;

    private String jabatan;

    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

//    @ManyToOne
//    @JoinColumn(name = "id_dosen")
//    private Dosen dosen;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String noSk;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalSk;

    private String kodeSpmb;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @JoinColumn(name = "kordinator")
    private JobPosition kordinator;

}
