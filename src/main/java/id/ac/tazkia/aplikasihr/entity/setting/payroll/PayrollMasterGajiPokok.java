package id.ac.tazkia.aplikasihr.entity.setting.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JenjangPendidikan;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobLevel;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollMasterGajiPokok {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_job_level")
    private JobLevel jobLevel;

    @ManyToOne
    @JoinColumn(name = "id_jenjang_pendidikan")
    private JenjangPendidikan jenjangPendidikan;

    @NotNull
    private String description;

    @NotNull
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
