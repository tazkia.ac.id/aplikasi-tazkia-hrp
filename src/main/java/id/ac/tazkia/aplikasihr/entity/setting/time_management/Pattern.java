package id.ac.tazkia.aplikasihr.entity.setting.time_management;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class Pattern {

    @Id
//    @GeneratedValue(generator = "uuid" )
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String patternName;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
