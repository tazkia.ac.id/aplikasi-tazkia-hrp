package id.ac.tazkia.aplikasihr.entity.kpi;

import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class PengisianKpi {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_kpi")
    private PositionKpi positionKpi;

    private LocalDate tanggal;

    private LocalDate tanggalPenugasan;

    private LocalDateTime tanggalInput;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @Lob
    private String deskripsi;

    @Lob
    private String deskripsiPenugasan;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusKpi = StatusRecord.OPEN;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private String dateUpdate;

    private String bulan;

    private String tahun;
}
