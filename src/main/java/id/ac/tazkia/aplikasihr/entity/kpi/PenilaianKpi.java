package id.ac.tazkia.aplikasihr.entity.kpi;

import id.ac.tazkia.aplikasihr.entity.PositionKpi;
import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;

@Entity
@Data
public class PenilaianKpi {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_position_kpi")
    private PositionKpi positionKpi;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private String bulan;
    private String tahun;

    private String grade;

    private BigDecimal nilai;

    private BigDecimal nominal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
