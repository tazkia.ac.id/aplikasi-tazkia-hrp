package id.ac.tazkia.aplikasihr.entity.masterdata;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class LecturerClass {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String classs;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;



}
