package id.ac.tazkia.aplikasihr.entity.setting.kpi;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class KpiPeriode {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty
    @NotNull
    private String kodePeriode;

    @NotNull
    @NotEmpty
    private String namaPeriode;

    private LocalDate mulaiSetting;

    private LocalDate selesaiSetting;

    private LocalDate mulaiPeriode;

    private LocalDate selesaiPeriode;

    private LocalDate mulaiPenilaian;

    private LocalDate selesaiPenilaian;

    private LocalDate tanggalKalkulasi;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @NotNull
    @NotEmpty
    private String mulaiSettingString;

    @NotNull
    @NotEmpty
    private String selesaiSettingString;

    @NotNull
    @NotEmpty
    private String mulaiPeriodeString;

    @NotNull
    @NotEmpty
    private String selesaiPeriodeString;

    @NotNull
    @NotEmpty
    private String mulaiPenilaianString;

    @NotNull
    @NotEmpty
    private String selesaiPenilaianString;

    @NotNull
    @NotEmpty
    private String tanggalKalkulasiString;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

}
