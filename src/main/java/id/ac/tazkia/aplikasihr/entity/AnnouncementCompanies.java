package id.ac.tazkia.aplikasihr.entity;

import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@EqualsAndHashCode(of = {"id", "announcement"})
@Data
public class AnnouncementCompanies {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_announcement")
    private Announcement announcement;

    @ManyToOne
    @JoinColumn(name = "id_companies")
    private Companies companies;

}
