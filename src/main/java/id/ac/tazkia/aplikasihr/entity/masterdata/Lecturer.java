package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
public class Lecturer {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name="id")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_lecturer_class")
    private LecturerClass lecturerClass;


    private String name;

    private String nidn;

    private String email;
    private String userInsert;
    private String userUpdate;
    private String userDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateInsert;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateUpdate;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateDelete;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi ;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    @ManyToOne
    @JoinColumn(name = "id_jabatan_fungsional_dosen")
    private JabatanFungsionalDosen jabatanFungsionalDosen;

    private String fileJabatanFungsionalDosen;

}
