package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Companies;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class AttendanceLecturer {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String bulanDate;

    private String tahun;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Lecturer lecturer;

    private String idJadwal;

    private String dosen;

    private String tanggal;

    private String nama;

    private String kelas;

    private String hari;

    private BigDecimal sks;

    private String matkul;

    private String email;

    private BigDecimal totalSks;

    private BigDecimal totalSeluruhSks;

    private BigDecimal totalSeluruhSesi;

    private String bulan;

    private BigDecimal totalSesi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userRun;

    private LocalDateTime tanggalRun;

    @ManyToOne
    @JoinColumn(name = "id_attendance_lecturer_impor_process")
    private AttendanceLecturerImporProcess attendanceLecturerImporProcess;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private String jenjang;

    private String idProdi;

    private String idJenjang;

    private String prodi;

}
