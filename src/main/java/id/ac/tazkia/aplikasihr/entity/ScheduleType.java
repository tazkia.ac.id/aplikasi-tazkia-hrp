package id.ac.tazkia.aplikasihr.entity;

public enum ScheduleType {

    Daily, Custom, Once, Event

}
