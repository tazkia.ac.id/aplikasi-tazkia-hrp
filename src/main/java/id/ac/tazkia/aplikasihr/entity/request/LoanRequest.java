package id.ac.tazkia.aplikasihr.entity.request;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Loan;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class LoanRequest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_loan")
    private Loan loan;

    private LocalDateTime requestDate;

    private BigDecimal nominalEstimate;

    private Integer installment;

    private String loanFor;

    private String deskripsi;

    private BigDecimal estimatePersentase;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private BigDecimal nominalCicilan;

    private Integer sequence;

    private Integer totalSequence;

}
