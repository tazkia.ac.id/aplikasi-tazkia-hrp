package id.ac.tazkia.aplikasihr.entity.setting;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.JobPosition;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeJobPosition {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_position")
    private JobPosition position;

//    @NotNull
    private LocalDate startDate;

//    @NotNull
    private LocalDate endDate;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String file;

    @NotNull
    private String startDateString;

    @NotNull
    private String endDateString;

}
