package id.ac.tazkia.aplikasihr.entity.config;


import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.request.AttendanceRequestApproval;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "s_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
//@EqualsAndHashCode(of = "id")
public class User {

    //settingan lama
//    @Id
//    private String id;
//    private String username;
//    private Boolean active;
//
//    @NotNull
//    @ManyToOne
//    @JoinColumn(name = "id_role")
//    private Role role;
//


    @Id
    private String id;

    private String username;

    private Boolean active;

    private String user; // Field ini bisa diperbaiki atau dijelaskan lebih baik,

}
