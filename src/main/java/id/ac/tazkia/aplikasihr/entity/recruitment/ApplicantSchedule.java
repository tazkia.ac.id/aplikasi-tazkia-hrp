package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.StatusWawancara;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class ApplicantSchedule {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_applicant")
    private Applicant applicant;

    private String jenisUndangan;

    private LocalDate tanggal;

    private LocalTime waktu;

    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "id_employes")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "bertemu_dengan")
    private Employes bertemuDengan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusKirimEmail = StatusRecord.WAITING;

    @Enumerated(EnumType.STRING)
    private StatusWawancara statusWawancara;

    private String tanggalString;

    private LocalDateTime dateUpdate = LocalDateTime.now();

    private String userUpdate;

}
