package id.ac.tazkia.aplikasihr.entity;

public enum PeriodeEvaluasiKpi {
    HARIAN,BULANAN,SEMESTERAN,TAHUNAN
}
