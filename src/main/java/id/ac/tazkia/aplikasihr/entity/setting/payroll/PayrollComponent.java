package id.ac.tazkia.aplikasihr.entity.setting.payroll;


import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class PayrollComponent {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String jenisComponent;

    private String code;

    private String name;

    private String rumus;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord taxable;

    @Enumerated(EnumType.STRING)
    private StatusRecord zakat;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String kodeRumusDefault;

    @Enumerated(EnumType.STRING)
    private StatusRecord potonganAbsen;

    @Enumerated(EnumType.STRING)
    private StatusRecord potonganTerlambat;

    @Enumerated(EnumType.STRING)
    private StatusRecord thr;

    @Enumerated(EnumType.STRING)
    private StatusRecord thrPajak;

//    @NumberFormat
    private BigDecimal persentase;

    @Enumerated(EnumType.STRING)
    private StatusRecord perhitungan;

    private Integer maksimalHari;

}
