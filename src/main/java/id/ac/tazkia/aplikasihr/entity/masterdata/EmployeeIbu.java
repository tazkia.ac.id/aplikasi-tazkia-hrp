package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
//@EqualsAndHashCode(of = {"id","employes"})
@Entity
public class EmployeeIbu {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    private String namaIbu;

    private String tempatLahirIbu;

    private LocalDate tanggalLahirIbu;

    @ManyToOne
    @JoinColumn(name = "id_agama")
    private Agama agama;

    @ManyToOne
    @JoinColumn(name = "id_pendidikan_terakhir_ibu")
    private JenjangPendidikan pendidikanTerakhirIbu;

    private String jabatanTerakhirIbu;

//    @ManyToOne
//    @JoinColumn(name = "id_status_hidup_ibu")
    private String statusHidup;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    private String tanggalLahir;

}
