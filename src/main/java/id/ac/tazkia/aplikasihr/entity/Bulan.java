package id.ac.tazkia.aplikasihr.entity;

import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;

@Entity
@Data
public class Bulan {

    @Id
    private String id;

    private String nomor;

    private String nama;

    private String namaEnglish;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
