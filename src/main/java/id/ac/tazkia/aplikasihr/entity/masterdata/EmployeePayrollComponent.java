package id.ac.tazkia.aplikasihr.entity.masterdata;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.payroll.EmployeePayrollComponentHistoryDetail;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class EmployeePayrollComponent {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_component")
    private PayrollComponent payrollComponent;

    private String jenisComponent;

    private BigDecimal nominal;

    private String jenisNominal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    @OneToMany(mappedBy = "employeePayrollComponent", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<EmployeePayrollComponentHistoryDetail> employeePayrollComponentHistoryDetailss = new HashSet<>();

}
