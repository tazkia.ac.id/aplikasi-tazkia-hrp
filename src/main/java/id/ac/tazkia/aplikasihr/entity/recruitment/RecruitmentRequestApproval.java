package id.ac.tazkia.aplikasihr.entity.recruitment;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.setting.ApprovalRequest;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import java.time.LocalDate;

@Entity
@Data
public class RecruitmentRequestApproval {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_recruitment_request")
    private RecruitmentRequest recruitmentRequest;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NumberFormat
    @Min(0)
    private Integer number;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove;

    private LocalDate tanggalApprove  = LocalDate.now();

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDate dateUpdate = LocalDate.now();

    @ManyToOne
    @JoinColumn(name = "id_recruitment_approval_setting")
    private ApprovalRequest approvalRequest;

}
