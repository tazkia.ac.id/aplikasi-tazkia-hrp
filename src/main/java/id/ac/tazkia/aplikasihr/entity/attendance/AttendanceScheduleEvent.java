package id.ac.tazkia.aplikasihr.entity.attendance;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.Employes;
import id.ac.tazkia.aplikasihr.entity.masterdata.Lecturer;
import id.ac.tazkia.aplikasihr.entity.setting.schedule.ScheduleEvent;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.scheduling.quartz.LocalDataSourceJobStore;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class AttendanceScheduleEvent {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_schedule_event")
    private ScheduleEvent scheduleEvent;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private LocalDate dateAttendance;

    private LocalTime timeIn;

    private LocalDateTime DateTimeIn;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    private String description;

    private String fileUpload;

}
