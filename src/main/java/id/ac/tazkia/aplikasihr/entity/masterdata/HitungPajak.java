package id.ac.tazkia.aplikasihr.entity.masterdata;

import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
public class HitungPajak {

    @Id
    private Integer id;

    private BigDecimal syaratDari;

    private BigDecimal syaratSampai;

    private BigDecimal persentase;

    private BigDecimal persentaseNonNpwp;

    private String status;

}
