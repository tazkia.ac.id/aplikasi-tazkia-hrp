package id.ac.tazkia.aplikasihr.entity.document;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Data
public class Document {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty
    private String idTahun;

    @NotEmpty
    private String idDosen;

    private Integer nomor;

    @NotEmpty
    private String nomorString;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
