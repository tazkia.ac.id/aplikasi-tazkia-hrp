package id.ac.tazkia.aplikasihr.entity.payroll;

import id.ac.tazkia.aplikasihr.entity.StatusRecord;
import id.ac.tazkia.aplikasihr.entity.masterdata.EmployeePayrollComponent;
import id.ac.tazkia.aplikasihr.entity.setting.payroll.PayrollComponent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@EqualsAndHashCode(of = {"id","employeePayrollComponentHistory","employeePayrollComponent"})
@Data
public class EmployeePayrollComponentHistoryDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee_payroll_component")
    private EmployeePayrollComponent employeePayrollComponent;

    private BigDecimal currentAmount;

    private BigDecimal newAmount;

    @ManyToOne
    @JoinColumn(name = "id_employee_payroll_component_history")
    private EmployeePayrollComponentHistory employeePayrollComponentHistory;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;


}
